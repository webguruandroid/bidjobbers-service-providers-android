package com.bidjobbers.bidjobberspro.di.components;


import com.bidjobbers.bidjobberspro.di.PerActivity;
import com.bidjobbers.bidjobberspro.di.modules.ActivityModule;
import com.bidjobbers.bidjobberspro.features.Archive.ArchiveActivity;
import com.bidjobbers.bidjobberspro.features.BankDetails.BankDetailsActivity;
import com.bidjobbers.bidjobberspro.features.CustomerDetails.CustomerDetailsActivity;
import com.bidjobbers.bidjobberspro.features.CustomerProfile.CustomerProfileActivity;
import com.bidjobbers.bidjobberspro.features.DetailsReview.ReviewDetailsActivity;
import com.bidjobbers.bidjobberspro.features.ForgotOtpVeryfy.VerifyOtpActivity;
import com.bidjobbers.bidjobberspro.features.GetOtpAfterForgetPassword.GetOtpForForgetPasswordActivity;
import com.bidjobbers.bidjobberspro.features.InApp.InAppActivity;
import com.bidjobbers.bidjobberspro.features.LanguageChange.LanguageChangeActivity;
import com.bidjobbers.bidjobberspro.features.Map.MapActivity;
import com.bidjobbers.bidjobberspro.features.MapCustomerDetails.MapCustomerDetailsActivity;
import com.bidjobbers.bidjobberspro.features.NewArchive.NewArchiveActivity;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.AccountInformation.AccountInformationFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.AdvanceBookingTime.AdvanceTimeForCustomerBookingFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessInfo.BusinessInfoFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessLocatiion.BusinessLocationFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.FirstTimeAccountSettingsActivity;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.ImageDataFragment.ImagePickerFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.IntroBusinessDetails.IntroBusinessDetailsFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.IntroductionFragment.IntroFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.KindOfPropertyWorkOn.KindOfProPropertyWorkOnFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.PricesFor.PricesForFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.SocialArea.SocialFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TreeStrubWorkFragment.TreeStrubWorkFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TypeOfPropertyWorkOn.TypeOfPropertyWorkOnFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.personaldetails.PersonalDetailsFragment;
import com.bidjobbers.bidjobberspro.features.ScheduleYourJob.AvalableDateViewPagerHelper.AvalableDateBodyFragment;
import com.bidjobbers.bidjobberspro.features.ScheduleYourJob.OnlyActivity.ScheduleJobActivity;
import com.bidjobbers.bidjobberspro.features.TearmsandCondition.TearmsandConditionActivity;
import com.bidjobbers.bidjobberspro.features.TestMVP.TestActivity;
import com.bidjobbers.bidjobberspro.features.VerifySignup.VerifySignupActivity;
import com.bidjobbers.bidjobberspro.features.allreviews.AllReviewsActivity;
import com.bidjobbers.bidjobberspro.features.dashbord.Main2Activity;
import com.bidjobbers.bidjobberspro.features.dashbord.OnlyActivty.DashbordActivity;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.Cancelled.CancelledBodyFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.Completed.CompletedBodyFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.ScheduledTask.ScheduledBodyFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.TaskItemBodyFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.Contactus.ContactusFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.gallery.BlockCustomerFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.home.HomeFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.send.SettingsFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.share.TearmsandConditionFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.slideshow.NotificationFragmentFragment;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.features.prcdetails.PercDetailsUpdateActivity;
import com.bidjobbers.bidjobberspro.features.profiledetails.ProfileActivity;
import com.bidjobbers.bidjobberspro.features.resetpassword.ResetPasswordActivity;
import com.bidjobbers.bidjobberspro.features.select_language.SelectLanguageActivity;
import com.bidjobbers.bidjobberspro.features.settings.SettingsActivity;
import com.bidjobbers.bidjobberspro.features.signup.SignUpActivity;
import com.bidjobbers.bidjobberspro.features.splash.SplashActivity;
import com.bidjobbers.bidjobberspro.features.testfragment.QuestionImageGalaryFragment;
import com.bidjobbers.bidjobberspro.features.testfragment.TestFragmentActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent
{

    void inject(TestActivity activity);
    void inject(InAppActivity activity);
    void inject(BankDetailsActivity activity);
    void inject(LoginActivity activity);
    void inject(TearmsandConditionActivity activity);

    void inject(VerifySignupActivity activity);

    void inject(TestFragmentActivity activity);

    void inject(ArchiveActivity activity);

    void inject(SignUpActivity activity);

    void inject(VerifyOtpActivity activity);

    void inject(LanguageChangeActivity activity);

    void inject(GetOtpForForgetPasswordActivity activity);

    void inject(ResetPasswordActivity activity);
    void inject(SplashActivity activity);
    void inject(CustomerProfileActivity activity);

    void inject(com.bidjobbers.bidjobberspro.features.resetpasswordfromsettings.ResetPasswordActivity activity);
    void inject(DashbordActivity activity);

    void inject(FirstTimeAccountSettingsActivity activity);

    void inject(ProfileActivity activity);

    void inject(SelectLanguageActivity activity);
    void inject(NewArchiveActivity activity);

    void inject(SettingsActivity activity);
    void inject(PercDetailsUpdateActivity activity);
    void inject(MapActivity activity);
    void inject(Main2Activity activity);
    void inject(ReviewDetailsActivity activity);
    void inject(AllReviewsActivity activity);
    void inject(MapCustomerDetailsActivity activity);


    void inject(CompletedBodyFragment fragment);
    void inject(ContactusFragment fragment);
    void inject(TearmsandConditionFragment fragment);

    void inject(AccountInformationFragment fragment);

    void inject(QuestionImageGalaryFragment questionImageGalaryFragment);


    void inject(KindOfProPropertyWorkOnFragment kindOfProPropertyWorkOnFragment);

    void inject(PersonalDetailsFragment personalDetailsFragment);

    void inject(PricesForFragment pricesForFragment);

    void inject(TypeOfPropertyWorkOnFragment typeOfPropertyWorkOnFragment);

    void inject(TreeStrubWorkFragment treeStrubWorkFragment);

    void inject(AdvanceTimeForCustomerBookingFragment advanceTimeForCustomerBookingFragment);

    void inject(BusinessInfoFragment businessInfoFragment);

    void inject(BusinessLocationFragment businessLocationFragment);

    void inject(IntroFragment introFragment);


    void inject(IntroBusinessDetailsFragment introBusinessDetailsFragment);

    void inject(SocialFragment socialFragment);

    void inject(ImagePickerFragment fragment);

    void inject(HomeFragment fragment);
    void inject(NotificationFragmentFragment fragment);

    void inject(SettingsFragment fragment);
    void inject(TaskItemBodyFragment fragment);
    void inject(CustomerDetailsActivity activity);
    void inject(ScheduleJobActivity activity);
    void inject(AvalableDateBodyFragment fragment);

    void inject(ScheduledBodyFragment fragment);
    void inject(CancelledBodyFragment fragment);

    void inject(BlockCustomerFragment fragment);


}