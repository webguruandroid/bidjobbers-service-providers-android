package com.bidjobbers.bidjobberspro.di.components;

import com.bidjobbers.bidjobberspro.di.PerService;
import com.bidjobbers.bidjobberspro.di.modules.ServiceModule;

import dagger.Component;


@PerService
@Component(dependencies = ApplicationComponent.class, modules = ServiceModule.class)
public interface ServiceComponent {

    //void inject(MyFirebaseMessagingServices service);

}