package com.bidjobbers.bidjobberspro.di.modules;


import android.app.Application;
import android.content.Context;

import com.bidjobbers.bidjobberspro.data.AppDataManager;
import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.pref.AppPreferencesHelper;
import com.bidjobbers.bidjobberspro.data.pref.PreferencesHelper;
import com.bidjobbers.bidjobberspro.di.ApplicationContext;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Singleton
    @Provides
    PreferencesHelper providePrefHelper(AppPreferencesHelper mAppPreferencesHelper){
        return mAppPreferencesHelper;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }
}
