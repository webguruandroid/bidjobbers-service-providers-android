/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.bidjobbers.bidjobberspro.di.modules;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bidjobbers.bidjobberspro.data.network.models.Notification.NotificationResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ReviewList.ReviewResponse;
import com.bidjobbers.bidjobberspro.data.network.models.servicedetails.ServiceDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.subscription.SubscriptionResponse;
import com.bidjobbers.bidjobberspro.di.ActivityContext;
import com.bidjobbers.bidjobberspro.di.PerActivity;
import com.bidjobbers.bidjobberspro.features.Archive.ArchiveMvpPresenter;
import com.bidjobbers.bidjobberspro.features.Archive.ArchiveMvpView;
import com.bidjobbers.bidjobberspro.features.Archive.ArchivePresenter;
import com.bidjobbers.bidjobberspro.features.BankDetails.BankDetailsMvpPresenter;
import com.bidjobbers.bidjobberspro.features.BankDetails.BankDetailsMvpView;
import com.bidjobbers.bidjobberspro.features.BankDetails.BankDetailsPresenter;
import com.bidjobbers.bidjobberspro.features.CustomerDetails.CustomerDetailsMvpPresenter;
import com.bidjobbers.bidjobberspro.features.CustomerDetails.CustomerDetailsMvpView;
import com.bidjobbers.bidjobberspro.features.CustomerDetails.CustomerDetailsPresenter;
import com.bidjobbers.bidjobberspro.features.CustomerProfile.CustomerProfileMvpPresenter;
import com.bidjobbers.bidjobberspro.features.CustomerProfile.CustomerProfileMvpView;
import com.bidjobbers.bidjobberspro.features.CustomerProfile.CustomerProfilePresenter;
import com.bidjobbers.bidjobberspro.features.DetailsReview.ReviewDetailsMvpPresenter;
import com.bidjobbers.bidjobberspro.features.DetailsReview.ReviewDetailsMvpView;
import com.bidjobbers.bidjobberspro.features.DetailsReview.ReviewDetailsPresenter;
import com.bidjobbers.bidjobberspro.features.ForgotOtpVeryfy.VerifyOtpMvpPresenter;
import com.bidjobbers.bidjobberspro.features.ForgotOtpVeryfy.VerifyOtpMvpView;
import com.bidjobbers.bidjobberspro.features.ForgotOtpVeryfy.VerifyOtpPresenter;
import com.bidjobbers.bidjobberspro.features.GetOtpAfterForgetPassword.GetOptForgotPassPresenter;
import com.bidjobbers.bidjobberspro.features.GetOtpAfterForgetPassword.GetOtpForgotPassMvpPresenter;
import com.bidjobbers.bidjobberspro.features.GetOtpAfterForgetPassword.GetOtpForgotPassMvpView;
import com.bidjobbers.bidjobberspro.features.InApp.InAppMvpPresenter;
import com.bidjobbers.bidjobberspro.features.InApp.InAppMvpView;
import com.bidjobbers.bidjobberspro.features.InApp.InAppPresenter;
import com.bidjobbers.bidjobberspro.features.InApp.ScbscriptionAdapter;
import com.bidjobbers.bidjobberspro.features.LanguageChange.LanguageMvpPresenter;
import com.bidjobbers.bidjobberspro.features.LanguageChange.LanguageMvpView;
import com.bidjobbers.bidjobberspro.features.LanguageChange.LanguagePresenter;
import com.bidjobbers.bidjobberspro.features.Map.MapMvpPresenter;
import com.bidjobbers.bidjobberspro.features.Map.MapMvpView;
import com.bidjobbers.bidjobberspro.features.Map.MapPresenter;
import com.bidjobbers.bidjobberspro.features.MapCustomerDetails.MapCustomerDetailsMvpView;
import com.bidjobbers.bidjobberspro.features.MapCustomerDetails.MapCustomerDetailsPresenter;
import com.bidjobbers.bidjobberspro.features.NewArchive.NewArchiveMvpPresenter;
import com.bidjobbers.bidjobberspro.features.NewArchive.NewArchiveMvpView;
import com.bidjobbers.bidjobberspro.features.NewArchive.NewArchivePresenter;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.AdvanceBookingTime.AdvanceTimeForCustomerBookingFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessHours.BusinessHoursFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessInfo.BusinessInfoFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessLocatiion.BusinessLocationFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.FirstTimeAccountSettingsMvpPresenter;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.FirstTimeAccountSettingsMvpView;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.FirstTimeAccountSettingsPresenter;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.ImageDataFragment.ImagePickerFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.ImageDataFragment.ImagePickerHelper.TakeImageAdapterAC;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.IntroBusinessDetails.IntroBusinessDetailsFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.IntroductionFragment.IntroFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.KindOfPropertyWorkOn.KindOfProPropertyWorkOnFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.PricesFor.PricesForFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.SocialArea.SocialFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.SocialArea.SocialFragmentMvpPresenter;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.SocialArea.SocialFragmentMvpView;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TreeStrubWorkFragment.TreeStrubWorkFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TypeOfPropertyWorkOn.TypeOfPropertyWorkOnFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TypeOfPropertyWorkOn.TypeOfPropertyWorkOnFragmentMvpPresenter;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TypeOfPropertyWorkOn.TypeOfPropertyWorkOnFragmentMvpView;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.personaldetails.PersonalDetailsFragment;
import com.bidjobbers.bidjobberspro.features.ScheduleYourJob.AvalableDateViewPagerHelper.AvalableDateBodyFragment;
import com.bidjobbers.bidjobberspro.features.ScheduleYourJob.OnlyActivity.ScheduleJobMvpPresenter;
import com.bidjobbers.bidjobberspro.features.ScheduleYourJob.OnlyActivity.ScheduleJobMvpView;
import com.bidjobbers.bidjobberspro.features.ScheduleYourJob.OnlyActivity.ScheduleJobPresenter;
import com.bidjobbers.bidjobberspro.features.TestMVP.TestMvpPresenter;
import com.bidjobbers.bidjobberspro.features.TestMVP.TestMvpView;
import com.bidjobbers.bidjobberspro.features.TestMVP.TestPresenter;
import com.bidjobbers.bidjobberspro.features.allreviews.AllReviewsMvpPresenter;
import com.bidjobbers.bidjobberspro.features.allreviews.AllReviewsMvpView;
import com.bidjobbers.bidjobberspro.features.allreviews.AllReviewsPresenter;
import com.bidjobbers.bidjobberspro.features.dashbord.OnlyActivty.DashbordMvpPresenter;
import com.bidjobbers.bidjobberspro.features.dashbord.OnlyActivty.DashbordMvpView;
import com.bidjobbers.bidjobberspro.features.dashbord.OnlyActivty.DashbordPresenter;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.Cancelled.CancelledBodyFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.Completed.CompletedBodyFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.ScheduledTask.ScheduledBodyFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.TaskItemBodyFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.Contactus.ContactusFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.gallery.BlockCustomerFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.home.HomeFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.send.SettingsFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.share.TearmsandConditionFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.slideshow.NotificationAdapter;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.slideshow.NotificationFragmentFragment;
import com.bidjobbers.bidjobberspro.features.login.LoginPresenter;
import com.bidjobbers.bidjobberspro.features.login.LoginPresenterHelper;
import com.bidjobbers.bidjobberspro.features.login.LoginViewHelper;
import com.bidjobbers.bidjobberspro.features.prcdetails.PercDetailsUpdateMvpPresenter;
import com.bidjobbers.bidjobberspro.features.prcdetails.PercDetailsUpdateMvpView;
import com.bidjobbers.bidjobberspro.features.prcdetails.PercDetailsUpdatePresenter;
import com.bidjobbers.bidjobberspro.features.profiledetails.ProfileHelper.AllReviewAdapter;
import com.bidjobbers.bidjobberspro.features.profiledetails.ProfileHelper.BusinessHrAdapter;
import com.bidjobbers.bidjobberspro.features.profiledetails.ProfileHelper.ReviewAdapter;
import com.bidjobbers.bidjobberspro.features.profiledetails.ProfileMvpPresenter;
import com.bidjobbers.bidjobberspro.features.profiledetails.ProfileMvpView;
import com.bidjobbers.bidjobberspro.features.profiledetails.ProfilePresenter;
import com.bidjobbers.bidjobberspro.features.resetpassword.ResetPasswordMvpPresenter;
import com.bidjobbers.bidjobberspro.features.resetpassword.ResetPasswordMvpView;
import com.bidjobbers.bidjobberspro.features.resetpassword.ResetPasswordPresenter;
import com.bidjobbers.bidjobberspro.features.select_language.SelectLanguageMvpPresenter;
import com.bidjobbers.bidjobberspro.features.select_language.SelectLanguageMvpView;
import com.bidjobbers.bidjobberspro.features.select_language.SelectLanguagePresenter;
import com.bidjobbers.bidjobberspro.features.settings.SettingsMvpPresenter;
import com.bidjobbers.bidjobberspro.features.settings.SettingsMvpView;
import com.bidjobbers.bidjobberspro.features.settings.SettingsPresenter;
import com.bidjobbers.bidjobberspro.features.signup.SignUpMvpPresenter;
import com.bidjobbers.bidjobberspro.features.signup.SignUpPresenter;
import com.bidjobbers.bidjobberspro.features.signup.SignupMvpView;
import com.bidjobbers.bidjobberspro.features.splash.SplashMvpPresenter;
import com.bidjobbers.bidjobberspro.features.splash.SplashMvpView;
import com.bidjobbers.bidjobberspro.features.splash.SplashPresenter;
import com.bidjobbers.bidjobberspro.features.testfragment.QuestionImageGalaryFragment;
import com.bidjobbers.bidjobberspro.features.testfragment.QuestionImageGalaryFragmentMvpPresenter;
import com.bidjobbers.bidjobberspro.features.testfragment.QuestionImageGalaryFragmentMvpView;
import com.bidjobbers.bidjobberspro.features.testfragment.TestFragmentActivityMvpPresenter;
import com.bidjobbers.bidjobberspro.features.testfragment.TestFragmentActivityMvpView;
import com.bidjobbers.bidjobberspro.features.testfragment.TestFragmentActivityPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.AppSchedulerProvider;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }





    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }


    @Provides
    @PerActivity
    TestMvpPresenter<TestMvpView> provideLoginMvpPresenter(TestPresenter<TestMvpView> presenter)
    {
        return presenter;
    }

    @Provides
    @PerActivity
    SignUpMvpPresenter<SignupMvpView> provideSignUpMvpPresenter(SignUpPresenter<SignupMvpView> presenter)
    {
        return presenter;
    }


    @Provides
    @PerActivity
    SettingsMvpPresenter<SettingsMvpView> provideSettingMvpPresenter(SettingsPresenter<SettingsMvpView> presenter)
    {
        return presenter;
    }

    @Provides
    @PerActivity
    ArchiveMvpPresenter<ArchiveMvpView> provideArchiveMvpPresenter(ArchivePresenter<ArchiveMvpView> presenter)
    {
        return presenter;
    }


    @Provides
    @PerActivity
    SplashMvpPresenter<SplashMvpView> provideSplashMvpPresenter(SplashPresenter<SplashMvpView> presenter)
    {
        return presenter;
    }

    @Provides
    @PerActivity
    InAppMvpPresenter<InAppMvpView> provideInAppMvpPresenter(InAppPresenter<InAppMvpView> presenter)
    {
        return presenter;
    }

    @Provides
    @PerActivity
    PercDetailsUpdateMvpPresenter<PercDetailsUpdateMvpView> providePercDetailsMvpPresenter(PercDetailsUpdatePresenter<PercDetailsUpdateMvpView> presenter)
    {
        return presenter;
    }

    @Provides
    @PerActivity
    ReviewDetailsMvpPresenter<ReviewDetailsMvpView> provideReviewDetailsMvpPresenter(ReviewDetailsPresenter<ReviewDetailsMvpView> presenter)
    {
        return presenter;
    }

    @Provides
    @PerActivity
    BankDetailsMvpPresenter<BankDetailsMvpView> provideBankDetailsPresenter(BankDetailsPresenter<BankDetailsMvpView> presenter)
    {
        return presenter;
    }

    @Provides
    @PerActivity
    AllReviewsMvpPresenter<AllReviewsMvpView> provideAllReviewMvpPresenter(AllReviewsPresenter<AllReviewsMvpView> presenter)
    {
        return presenter;
    }
    @Provides
    @PerActivity
    MapMvpPresenter<MapMvpView> provideMapMvpPresenter(MapPresenter<MapMvpView> presenter)
    {
        return presenter;
    }


/*

    @Provides
    @PerActivity
    VerifySignupPresenter<VerifySignupMvpView> provideVerifySignupMvpPresenter(VerifySignupPresenter<VerifySignupMvpView> presenter)
    {
        return presenter;
    }
*/




    @Provides
    @PerActivity
    GetOtpForgotPassMvpPresenter<GetOtpForgotPassMvpView> provideGetOtpForgotPassMvpPresenter(GetOptForgotPassPresenter<GetOtpForgotPassMvpView> presenter)
    {
        return presenter;
    }


    @Provides
    @PerActivity
    VerifyOtpMvpPresenter<VerifyOtpMvpView> provideVerifyOtpMvpPresenter(VerifyOtpPresenter<VerifyOtpMvpView> presenter)
    {
        return presenter;
    }

    @Provides
    @PerActivity
    ResetPasswordMvpPresenter<ResetPasswordMvpView> provideResetPasswordMvpPresenter(ResetPasswordPresenter<ResetPasswordMvpView> presenter)
    {
        return presenter;
    }


    @Provides
    @PerActivity
    MapCustomerDetailsPresenter<MapCustomerDetailsMvpView> provideMapCustomerMvpPresenter(MapCustomerDetailsPresenter<MapCustomerDetailsMvpView> presenter)
    {
        return presenter;
    }

    @Provides
    @PerActivity
    CustomerProfileMvpPresenter<CustomerProfileMvpView> provideCustomerProfileMvpPresenter(CustomerProfilePresenter<CustomerProfileMvpView> presenter)
    {
        return presenter;
    }


    @Provides
    @PerActivity
    LanguageMvpPresenter<LanguageMvpView> provideLanguagePresenter(LanguagePresenter<LanguageMvpView> presenter)
    {
        return presenter;
    }

    @Provides
    @PerActivity
    CustomerDetailsMvpPresenter<CustomerDetailsMvpView> provideCustomerDetailsMvpPresenter(CustomerDetailsPresenter<CustomerDetailsMvpView> presenter)
    {
        return presenter;
    }


    @Provides
    @PerActivity
    DashbordMvpPresenter<DashbordMvpView> provideDashbordMvpPresenter(DashbordPresenter<DashbordMvpView> presenter)
    {
        return presenter;
    }


    @Provides
    @PerActivity
    ScheduleJobMvpPresenter<ScheduleJobMvpView> provideScheduleMvpPresenter(ScheduleJobPresenter<ScheduleJobMvpView> presenter)
    {
        return presenter;
    }

    @Provides
    @PerActivity
    FirstTimeAccountSettingsMvpPresenter<FirstTimeAccountSettingsMvpView> provideFirstTimeAccountSettingsMvpPresenter( FirstTimeAccountSettingsPresenter< FirstTimeAccountSettingsMvpView> presenter)
    {
        return presenter;
    }
    @Provides
    @PerActivity
    ProfileMvpPresenter<ProfileMvpView> provideProfileMvpPresenter(ProfilePresenter< ProfileMvpView> presenter)
    {
        return presenter;
    }



    @Provides
    @PerActivity
    LoginPresenterHelper<LoginViewHelper> provideTrueLoginMvpPresenter(LoginPresenter<LoginViewHelper> presenter)
    {
        return presenter;
    }


    @Provides
    @PerActivity
    LinearLayoutManager provideLinearLayoutManager()
    {
        return new LinearLayoutManager(mActivity);
    }






    @Provides
    @PerActivity
    TestFragmentActivityMvpPresenter provideTesFragmentActivityPresenter(TestFragmentActivityPresenter<TestFragmentActivityMvpView> presenter)
    {
        return presenter;
    }


/*

    @Provides
    @PerActivity
    QuestionImageGalaryFragment provideQuestionImageGalaryFragment()
    {
        return new QuestionImageGalaryFragment();
    }
*/

    @Provides
    @PerActivity
    AvalableDateBodyFragment provideKavalableDateBodyFragment()
    {
        return  new AvalableDateBodyFragment() ;
    }


    @Provides
    @PerActivity
    KindOfProPropertyWorkOnFragment provideKindOfProPropertyWorkOnFragment()
    {
        return  new KindOfProPropertyWorkOnFragment() ;
    }

    @Provides
    @PerActivity
    TaskItemBodyFragment provideTaskItemFragment()
    {
        return  new TaskItemBodyFragment() ;
    }

    @Provides
    @PerActivity
    ImagePickerFragment provideImagePickerFragment()
    {
        return  new ImagePickerFragment() ;
    }



    @Provides
    @PerActivity
    BusinessHoursFragment provideBusinessHoursFragment()
    {
        return  new BusinessHoursFragment();
    }

    @Provides
    @PerActivity
    PersonalDetailsFragment providePersonalDetailsFragment()
    {
        return  new PersonalDetailsFragment();
    }

    @Provides
    @PerActivity
    ScheduledBodyFragment provideScheduleFragment()
    {
        return  new ScheduledBodyFragment();
    }

    @Provides
    @PerActivity
    CompletedBodyFragment provideCompletedFragment()
    {
        return  new CompletedBodyFragment();
    }

    @Provides
    @PerActivity
    CancelledBodyFragment provideCancelledFragment()
    {
        return  new CancelledBodyFragment();
    }


    @Provides
    @PerActivity
    PricesForFragment providePricesForFragment()
    {
        return  new PricesForFragment();
    }

    @Provides
    @PerActivity
    TypeOfPropertyWorkOnFragment provideTypeOfPropertyWorkOnFragment()
    {
        return  new TypeOfPropertyWorkOnFragment();
    }

    @Provides
    @PerActivity
    TreeStrubWorkFragment provideTreeStrubWorkFragment()
    {
        return  new TreeStrubWorkFragment();
    }


    @Provides
    @PerActivity
    HomeFragment provideHomeFragment()
    {
        return  new HomeFragment();
    }
    @Provides
    @PerActivity
    ContactusFragment provideContactUsFragment()
    {
        return  new ContactusFragment();
    }

    @Provides
    @PerActivity
    BlockCustomerFragment provideBlockCustomerFragment()
    {
        return  new BlockCustomerFragment();
    }




    @Provides
    @PerActivity
    NotificationFragmentFragment provideNotificationFragment()
    {
        return  new NotificationFragmentFragment();
    }


    @Provides
    @PerActivity
    SettingsFragment provideSettingsFragment()
    {
        return  new SettingsFragment();
    }


    @Provides
    @PerActivity
    AdvanceTimeForCustomerBookingFragment provideAdvanceTimeForCustomerBookingFragment()
    {
        return  new AdvanceTimeForCustomerBookingFragment();
    }




    @Provides
    @PerActivity
    BusinessInfoFragment provideBusinessInfoFragment()
    {
        return  new BusinessInfoFragment();
    }


    @Provides
    @PerActivity
    BusinessLocationFragment provideBusinessLocationFragment()
    {
        return  new BusinessLocationFragment();
    }

    @Provides
    @PerActivity
    IntroFragment provideIntroFragment()
    {
        return  new IntroFragment();
    }

    @Provides
    @PerActivity
    QuestionImageGalaryFragment provideQuestionImageGalaryFragment()
    {
        return  new QuestionImageGalaryFragment();
    }


    @Provides
    @PerActivity
    IntroBusinessDetailsFragment provideIntroBusinessDetailsFragment()
    {
        return  new IntroBusinessDetailsFragment();
    }

    @Provides
    @PerActivity
    SocialFragment provideSocialFragment()
    {
        return  new SocialFragment();
    }

    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider()
    {
        return new AppSchedulerProvider();
    }


    @Provides
    @PerActivity
    TearmsandConditionFragment provideTearmsandConditionFragment()
    {
        return  new TearmsandConditionFragment();
    }

    @Provides
    @PerActivity
    SocialFragmentMvpPresenter<SocialFragmentMvpView> provideSocialFragmentMvpPresenter(SocialFragmentMvpPresenter<SocialFragmentMvpView> presenter)
    {
        return  presenter;
    }

    @Provides
    @PerActivity
    QuestionImageGalaryFragmentMvpPresenter<QuestionImageGalaryFragmentMvpView> provideQuestionImageGalaryFragmentMvpPresenter(QuestionImageGalaryFragmentMvpPresenter<QuestionImageGalaryFragmentMvpView> presenter)
    {
        return  presenter;
    }


    @Provides
    @PerActivity
    NewArchiveMvpPresenter<NewArchiveMvpView> provideNewArchiveMvpPresenter(NewArchivePresenter<NewArchiveMvpView> presenter)
    {
        return  presenter;
    }



    @Provides
    @PerActivity
    TypeOfPropertyWorkOnFragmentMvpPresenter<TypeOfPropertyWorkOnFragmentMvpView> provideTypeOfPropertyWorkOnFragmentMvpPresenter(TypeOfPropertyWorkOnFragmentMvpPresenter<TypeOfPropertyWorkOnFragmentMvpView> presenter)
    {
        return  presenter;
    }


    @Provides
    @PerActivity
    SelectLanguageMvpPresenter<SelectLanguageMvpView> provideSelectLanguageMvpPresenter(SelectLanguagePresenter< SelectLanguageMvpView> presenter)
    {
        return presenter;
    }

    @Provides
    BusinessHrAdapter provideBusinessHrAdapter() {
        return new BusinessHrAdapter(new ArrayList<ServiceDetailsResponse.ResponseDataBean.BuisnessHoursDetailsBean.BusinessHrDataBean>());
    }

//    @Provides
//    TaskListingAdapter provideReciveTaskAdapter() {
//        return new TaskListingAdapter(new ArrayList<ReciveTaskResponse.ResponseDataBean.TaskDataBean>());
//    }

  //  @Provides
   // ScheduleTaskListingAdapter provideScheduleTaskAdapter() {
    //    return new ScheduleTaskListingAdapter(new ArrayList<ScheduletaskResponse.ResponseDataBean.TaskDataBean>());
   // }

//    @Provides
//    CompletedTaskListingAdapter provideCompletedTaskAdapter() {
//        return new CompletedTaskListingAdapter(new ArrayList<CompletedTaskResponse.ResponseDataBean.TaskDataBean>());
//    }

//    @Provides
//    CancelledTaskListingAdapter provideCancelledTaskAdapter() {
//        return new CancelledTaskListingAdapter(new ArrayList<CancelledTaskResponse.ResponseDataBean.CancelledTaskBean>());
//    }
//    @Provides
//    BlockListingAdapter provideBlockListAdapter() {
//        return new BlockListingAdapter(new ArrayList<BlockCustomerListResponse.ResponseDataBean.BlocklistBean>());
//    }

    @Provides
    ReviewAdapter provideReviewListAdapter() {
        return new ReviewAdapter(new ArrayList<ReviewResponse.ResponseDataBean.ReviewListBean>(),0);
    }

    @Provides
    ScbscriptionAdapter provideSubscriptonAdapter() {
        return new ScbscriptionAdapter(new ArrayList<SubscriptionResponse.ResponseDataBean.SubscribePackageBean>(),0);
    }

    @Provides
    AllReviewAdapter provideAllReviewListAdapter() {
        return new AllReviewAdapter(new ArrayList<ReviewResponse.ResponseDataBean.ReviewListBean>(),0);
    }

//    @Provides
//    NewArchiveAdapter provideNewArchiveAdapter() {
//        return new NewArchiveAdapter(new ArrayList<ArchiveJobList>());
//    }

    @Provides
    NotificationAdapter provideNotificationListAdapter() {
        return new NotificationAdapter(new ArrayList<NotificationResponse.ResponseDataBean.NotificationListBean>(),0);
    }

    @Provides
    TakeImageAdapterAC provideTakeImageAdapter() {
        return new TakeImageAdapterAC(new ArrayList<String>());
    }
}
