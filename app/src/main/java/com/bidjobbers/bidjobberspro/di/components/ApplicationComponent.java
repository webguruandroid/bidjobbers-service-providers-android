package com.bidjobbers.bidjobberspro.di.components;


import android.app.Application;
import android.content.Context;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.di.ApplicationContext;
import com.bidjobbers.bidjobberspro.di.modules.ApplicationModule;
import com.bidjobbers.bidjobberspro.di.modules.NetworkModule;
import com.bidjobbers.bidjobberspro.shared.BidJobbersApp;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {

    void inject(BidJobbersApp app);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();
}