package com.bidjobbers.bidjobberspro.shared;

import android.app.Application;
import android.content.Context;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.di.components.ApplicationComponent;
import com.bidjobbers.bidjobberspro.di.components.DaggerApplicationComponent;
import com.bidjobbers.bidjobberspro.di.modules.ApplicationModule;
import com.bidjobbers.bidjobberspro.di.modules.NetworkModule;
import com.bidjobbers.bidjobberspro.utils.Constants;

import javax.inject.Inject;

public class BidJobbersApp extends Application {
    ApplicationComponent applicationComponent = DaggerApplicationComponent.builder()
            .applicationModule(new ApplicationModule(this))
            .networkModule(new NetworkModule(Constants.baseUrlLive)).build();

    @Inject
    DataManager mDataManager;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent.inject(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //  MultiDex.install(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    public DataManager getDataManager() {
        return mDataManager;
    }
}
