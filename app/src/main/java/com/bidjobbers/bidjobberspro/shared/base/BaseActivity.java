package com.bidjobbers.bidjobberspro.shared.base;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.di.components.DaggerActivityComponent;
import com.bidjobbers.bidjobberspro.di.modules.ActivityModule;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.shared.BidJobbersApp;
import com.bidjobbers.bidjobberspro.utils.CommonUtils;
import com.bidjobbers.bidjobberspro.utils.MessageEvent;
import com.bidjobbers.bidjobberspro.utils.NetworkUtils;
import com.google.android.material.snackbar.Snackbar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Unbinder;
import timber.log.Timber;

public abstract class BaseActivity extends AppCompatActivity implements MvpView {


    public static final String TAG = "MapActivity";
    private ProgressDialog mProgressDialog;

    private ActivityComponent mActivityComponent;
    private Unbinder mUnBinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);


       /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        */

        mActivityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .applicationComponent(((BidJobbersApp) getApplication()).getApplicationComponent())
                .build();
    }


    public ActivityComponent getActivityComponent()
    {
        return mActivityComponent;
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        String message = (String) event.toString();
        Log.e("fcmreceiver", "MESSAHe  :" + event.getMessage());

        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            if(event.getMessage().equals("deactivated")) {
                builder.setMessage(getResources().getString(R.string.deactivate));
            }
            else
            {
                builder.setMessage(getResources().getString(R.string.suspended));
            }
            builder.setCancelable(false);
            builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                    gotoLogin();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        } catch (Exception e){
            Timber.tag(TAG).e(e);
        }

    }

    private void gotoLogin()
    {

        Intent i=new Intent(this, LoginActivity.class);
        startActivity(i);
    }

    @Override
    protected void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();

    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission)
    {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void showLoading()
    {
        hideLoading();
        mProgressDialog = CommonUtils.showLoadingDialog(this);
    }

    @Override
    public void hideLoading()
    {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    public void showSnackBar(String message)
    {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(this, android.R.color.white));
        snackbar.show();
    }

    @Override
    public void onError(String message)
    {
        hideKeyboard();
        if (message != null) {
            showErrorToast(message);
        } else {
            showErrorToast("Error Message is null");
        }
    }

    @Override
    public void onError(@StringRes int resId)
    {
        onError(getString(resId));
    }

    @Override
    public void showMessage(String message)
    {
        hideKeyboard();
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Some error", Toast.LENGTH_LONG).show();
        }
    }

    public void showErrorToast(String message)
    {
        hideKeyboard();
        if (message != null)
        {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        }
        else
        {
            Toast.makeText(this, "Some error", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void showMessage(@StringRes int resId)
    {
        showMessage(getString(resId));
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isNetworkConnected(getApplicationContext());
    }

    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void logOut(){
      /*  Intent i = new Intent(this, SignInActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);*/

        /*LockManager<CustomPinActivity> lockManager = LockManager.getInstance();
        lockManager.getAppLock().setLogoId(R.mipmap.ic_launcher);
        lockManager.getAppLock().setShouldShowForgot(true);
        lockManager.getAppLock().setTimeout(Constants.TIMEOUT_MILLIES);
        lockManager.getAppLock().addIgnoredActivity(SplashActivity.class);
        lockManager.getAppLock().addIgnoredActivity(SignInActivity.class);
        lockManager.getAppLock().addIgnoredActivity(LoginEmailActivity.class);*/
    }


    public void setUnBinder(Unbinder unBinder) {
        mUnBinder = unBinder;
    }

    @Override
    protected void onDestroy() {

        if (mUnBinder != null) {
            mUnBinder.unbind();
        }
        super.onDestroy();
    }

    @Override
    public void showAlert(String message) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(message);
            builder.setCancelable(false);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        } catch (Exception e){
            Timber.tag(TAG).e(e);
        }
    }


    @Override
    public void showInactiveUserAlert(String message) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.app_name);
            builder.setIcon(R.mipmap.ic_launcher);
            builder.setMessage(message);
            builder.setCancelable(false);
            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    logOut();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }catch (Exception e){
            Timber.e(e);
        }
    }

    @Override
      public void onBackPressed() {
        super.onBackPressed();
        finish();
      //  overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    /*protected abstract void setUp();*/


    //App Lock related code
}
