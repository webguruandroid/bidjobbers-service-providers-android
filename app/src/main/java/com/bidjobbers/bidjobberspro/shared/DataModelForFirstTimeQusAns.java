package com.bidjobbers.bidjobberspro.shared;

public class DataModelForFirstTimeQusAns {

    public KindOfServiceDoProvide kindOfServiceDoProvide;//Done //ServiceTypeDetails
    public BusinessInfo businessInfo;//Done / BusinessInfo
    public BusinessLocation businessLocation;//Done // BusinessLocation
    public IntroduceYourBusiness introduceYourBusiness;//Done//BusinessDescription
    public BusinessHours businessHours; //Done//BusinessHrDetails
    public AdvanceTimeToBookAJob advanceTimeToBookAJob;//Done//AdvanceBooking
    public TypeOfPropertyWorkOn typeOfPropertyWorkOn;//Done//PropertyTypeDetails
    public BusinessPriceLawnSerce businessPriceLawnSerce;//Done//lawn & tree service
    public PhotoDescribeProject photoDescribeProject;
    public SocialMediaLink socialMediaLink;
    public WorkPhoto workPhoto;
    public BankDetails bankDetails;


    public BankDetails getBankDetails() {
        return bankDetails;
    }

    public void setBankDetails(BankDetails bankDetails) {
        this.bankDetails = bankDetails;
    }

    public WorkPhoto getWorkPhoto() {
        return workPhoto;
    }

    public void setWorkPhoto(WorkPhoto workPhoto) {
        this.workPhoto = workPhoto;
    }

    public KindOfServiceDoProvide getKindOfServiceDoProvide() {
        return kindOfServiceDoProvide;
    }

    public BusinessInfo getBusinessInfo() {
        return businessInfo;
    }

    public BusinessLocation getBusinessLocation() {
        return businessLocation;
    }

    public IntroduceYourBusiness getIntroduceYourBusiness() {
        return introduceYourBusiness;
    }

    public BusinessHours getBusinessHours() {
        return businessHours;
    }

    public AdvanceTimeToBookAJob getAdvanceTimeToBookAJob() {
        return advanceTimeToBookAJob;
    }

    public TypeOfPropertyWorkOn getTypeOfPropertyWorkOn() {
        return typeOfPropertyWorkOn;
    }

    public BusinessPriceLawnSerce getBusinessPriceLawnSerce() {
        return businessPriceLawnSerce;
    }

    public PhotoDescribeProject getPhotoDescribeProject() {
        return photoDescribeProject;
    }

    public SocialMediaLink getSocialMediaLink() {
        return socialMediaLink;
    }

    public void setSocialMediaLink(SocialMediaLink socialMediaLink) {
        this.socialMediaLink = socialMediaLink;
    }

    public void setKindOfService(KindOfServiceDoProvide kindOfServiceDoProvide) {
        this.kindOfServiceDoProvide = kindOfServiceDoProvide;
    }

    public void setKindOfServiceDoProvide(KindOfServiceDoProvide kindOfServiceDoProvide) {
        this.kindOfServiceDoProvide = kindOfServiceDoProvide;
    }

    public void setBusinessInfo(BusinessInfo businessInfo) {
        this.businessInfo = businessInfo;
    }

    public void setBusinessLocation(BusinessLocation businessLocation) {
        this.businessLocation = businessLocation;
    }

    public void setIntroduceYourBusiness(IntroduceYourBusiness introduceYourBusiness) {
        this.introduceYourBusiness = introduceYourBusiness;
    }

    public void setBusinessHours(BusinessHours businessHours) {
        this.businessHours = businessHours;
    }

    public void setAdvanceTimeToBookAJob(AdvanceTimeToBookAJob advanceTimeToBookAJob) {
        this.advanceTimeToBookAJob = advanceTimeToBookAJob;
    }

    public void setTypeOfPropertyWorkOn(TypeOfPropertyWorkOn typeOfPropertyWorkOn) {
        this.typeOfPropertyWorkOn = typeOfPropertyWorkOn;
    }

    public void setBusinessPriceLawnSerce(BusinessPriceLawnSerce businessPriceLawnSerce) {
        this.businessPriceLawnSerce = businessPriceLawnSerce;
    }

    public void setPhotoDescribeProject(PhotoDescribeProject photoDescribeProject) {
        this.photoDescribeProject = photoDescribeProject;
    }


    public static class KindOfServiceDoProvide {

        String lawnServiceId = "";
        String lawnServiceName = "";
        boolean isLawnServiceSelected = false;


        String mowALownId = "";
        String mowALawnName = "";
        String mowALawnDescription = "";
        String mowAMowALawnImage = "";
        String mowAMowPrice = "";
        boolean ismowALownSelected = false;

        String areateALawnId = "";
        String areateALawnName = "";
        String areateALawnDescription = "";
        String areateALawnImage = "";
        String areateALawnPrice = "";
        boolean isAreateALawn = false;

        String yardWasteId = "";
        String yearALawnName = "";
        String yearALawnDescription = "";
        String yearALawnImage = "";
        String yearALawnPrice = "";
        boolean isyardWaste = false;

        //=====================================

        String treeCuttingId = "";
        String treeCuttingServiceName = "";
        boolean isTreeCuttingSelected = false;


        String treeStrubsId = "";
        String treeStrubsName = "";
        String treeStrubsDescription = "";
        String treeStrubsImage = "";
        String treeStrubsPrice = "";
        boolean isTreeStrubsSelected = false;

        String treeStrumpsId = "";
        String treeStrumpsName = "";
        String treeStrumpsDescription = "";
        String treeStrumpsImage = "";
        String treeStrumpsPrice = "";
        boolean isTreeStrumps = false;

        String yardWasteTreeCuttingId = "";
        String yardWasteTreeName = "";
        String yardWasteTreeDescription = "";
        String yardWasteTreeImage = "";
        String yeardWastTreePrice = "";
        boolean isyardWasteTreeCutting = false;

        //==================================


        //===============================


        public String getMowAMowPrice() {
            return mowAMowPrice;
        }

        public void setMowAMowPrice(String mowAMowPrice) {
            this.mowAMowPrice = mowAMowPrice;
        }

        public String getAreateALawnPrice() {
            return areateALawnPrice;
        }

        public void setAreateALawnPrice(String areateALawnPrice) {
            this.areateALawnPrice = areateALawnPrice;
        }

        public String getYearALawnPrice() {
            return yearALawnPrice;
        }

        public void setYearALawnPrice(String yearALawnPrice) {
            this.yearALawnPrice = yearALawnPrice;
        }

        public String getTreeStrubsPrice() {
            return treeStrubsPrice;
        }

        public void setTreeStrubsPrice(String treeStrubsPrice) {
            this.treeStrubsPrice = treeStrubsPrice;
        }

        public String getTreeStrumpsPrice() {
            return treeStrumpsPrice;
        }

        public void setTreeStrumpsPrice(String treeStrumpsPrice) {
            this.treeStrumpsPrice = treeStrumpsPrice;
        }

        public String getYeardWastTreePrice() {
            return yeardWastTreePrice;
        }

        public void setYeardWastTreePrice(String yeardWastTreePrice) {
            this.yeardWastTreePrice = yeardWastTreePrice;
        }

        public String getLawnServiceId() {
            return lawnServiceId;
        }

        public void setLawnServiceId(String lawnServiceId) {
            this.lawnServiceId = lawnServiceId;
        }

        public String getLawnServiceName() {
            return lawnServiceName;
        }

        public void setLawnServiceName(String lawnServiceName) {
            this.lawnServiceName = lawnServiceName;
        }

        public boolean isLawnServiceSelected() {
            return isLawnServiceSelected;
        }

        public void setLawnServiceSelected(boolean lawnServiceSelected) {
            isLawnServiceSelected = lawnServiceSelected;
        }

        public String getMowALownId() {
            return mowALownId;
        }

        public void setMowALownId(String mowALownId) {
            this.mowALownId = mowALownId;
        }

        public String getMowALawnName() {
            return mowALawnName;
        }

        public void setMowALawnName(String mowALawnName) {
            this.mowALawnName = mowALawnName;
        }

        public String getMowALawnDescription() {
            return mowALawnDescription;
        }

        public void setMowALawnDescription(String mowALawnDescription) {
            this.mowALawnDescription = mowALawnDescription;
        }

        public String getMowAMowALawnImage() {
            return mowAMowALawnImage;
        }

        public void setMowAMowALawnImage(String mowAMowALawnImage) {
            this.mowAMowALawnImage = mowAMowALawnImage;
        }

        public boolean isIsmowALownSelected() {
            return ismowALownSelected;
        }

        public void setIsmowALownSelected(boolean ismowALownSelected) {
            this.ismowALownSelected = ismowALownSelected;
        }

        public String getAreateALawnId() {
            return areateALawnId;
        }

        public void setAreateALawnId(String areateALawnId) {
            this.areateALawnId = areateALawnId;
        }


        public String getAreateALawnName() {
            return areateALawnName;
        }

        public void setAreateALawnName(String areateALawnName) {
            this.areateALawnName = areateALawnName;
        }

        public String getAreateALawnDescription() {
            return areateALawnDescription;
        }

        public void setAreateALawnDescription(String areateALawnDescription) {
            this.areateALawnDescription = areateALawnDescription;
        }

        public String getAreateALawnImage() {
            return areateALawnImage;
        }

        public void setAreateALawnImage(String areateALawnImage) {
            this.areateALawnImage = areateALawnImage;
        }

        public boolean isAreateALawn() {
            return isAreateALawn;
        }

        public void setAreateALawn(boolean areateALawn) {
            isAreateALawn = areateALawn;
        }

        public String getYardWasteId() {
            return yardWasteId;
        }

        public void setYardWasteId(String yardWasteId) {
            this.yardWasteId = yardWasteId;
        }

        public String getYearALawnName() {
            return yearALawnName;
        }

        public void setYearALawnName(String yearALawnName) {
            this.yearALawnName = yearALawnName;
        }

        public String getYearALawnDescription() {
            return yearALawnDescription;
        }

        public void setYearALawnDescription(String yearALawnDescription) {
            this.yearALawnDescription = yearALawnDescription;
        }

        public String getYearALawnImage() {
            return yearALawnImage;
        }

        public void setYearALawnImage(String yearALawnImage) {
            this.yearALawnImage = yearALawnImage;
        }

        public boolean isIsyardWaste() {
            return isyardWaste;
        }

        public void setIsyardWaste(boolean isyardWaste) {
            this.isyardWaste = isyardWaste;
        }

        public String getTreeCuttingId() {
            return treeCuttingId;
        }

        public void setTreeCuttingId(String treeCuttingId) {
            this.treeCuttingId = treeCuttingId;
        }

        public String getTreeCuttingServiceName() {
            return treeCuttingServiceName;
        }

        public void setTreeCuttingServiceName(String treeCuttingServiceName) {
            this.treeCuttingServiceName = treeCuttingServiceName;
        }

        public boolean isTreeCuttingSelected() {
            return isTreeCuttingSelected;
        }

        public void setTreeCuttingSelected(boolean treeCuttingSelected) {
            isTreeCuttingSelected = treeCuttingSelected;
        }

        public String getTreeStrubsId() {
            return treeStrubsId;
        }

        public void setTreeStrubsId(String treeStrubsId) {
            this.treeStrubsId = treeStrubsId;
        }

        public String getTreeStrubsName() {
            return treeStrubsName;
        }

        public void setTreeStrubsName(String treeStrubsName) {
            this.treeStrubsName = treeStrubsName;
        }

        public String getTreeStrubsDescription() {
            return treeStrubsDescription;
        }

        public void setTreeStrubsDescription(String treeStrubsDescription) {
            this.treeStrubsDescription = treeStrubsDescription;
        }

        public String getTreeStrubsImage() {
            return treeStrubsImage;
        }

        public void setTreeStrubsImage(String treeStrubsImage) {
            this.treeStrubsImage = treeStrubsImage;
        }

        public boolean isTreeStrubsSelected() {
            return isTreeStrubsSelected;
        }

        public void setTreeStrubsSelected(boolean treeStrubsSelected) {
            isTreeStrubsSelected = treeStrubsSelected;
        }

        public String getTreeStrumpsId() {
            return treeStrumpsId;
        }

        public void setTreeStrumpsId(String treeStrumpsId) {
            this.treeStrumpsId = treeStrumpsId;
        }

        public String getTreeStrumpsName() {
            return treeStrumpsName;
        }

        public void setTreeStrumpsName(String treeStrumpsName) {
            this.treeStrumpsName = treeStrumpsName;
        }

        public String getTreeStrumpsDescription() {
            return treeStrumpsDescription;
        }

        public void setTreeStrumpsDescription(String treeStrumpsDescription) {
            this.treeStrumpsDescription = treeStrumpsDescription;
        }

        public String getTreeStrumpsImage() {
            return treeStrumpsImage;
        }

        public void setTreeStrumpsImage(String treeStrumpsImage) {
            this.treeStrumpsImage = treeStrumpsImage;
        }

        public boolean isTreeStrumps() {
            return isTreeStrumps;
        }

        public void setTreeStrumps(boolean treeStrumps) {
            isTreeStrumps = treeStrumps;
        }

        public String getYardWasteTreeCuttingId() {
            return yardWasteTreeCuttingId;
        }

        public void setYardWasteTreeCuttingId(String yardWasteTreeCuttingId) {
            this.yardWasteTreeCuttingId = yardWasteTreeCuttingId;
        }

        public String getYardWasteTreeName() {
            return yardWasteTreeName;
        }

        public void setYardWasteTreeName(String yardWasteTreeName) {
            this.yardWasteTreeName = yardWasteTreeName;
        }

        public String getYardWasteTreeDescription() {
            return yardWasteTreeDescription;
        }

        public void setYardWasteTreeDescription(String yardWasteTreeDescription) {
            this.yardWasteTreeDescription = yardWasteTreeDescription;
        }

        public String getYardWasteTreeImage() {
            return yardWasteTreeImage;
        }

        public void setYardWasteTreeImage(String yardWasteTreeImage) {
            this.yardWasteTreeImage = yardWasteTreeImage;
        }

        public boolean isIsyardWasteTreeCutting() {
            return isyardWasteTreeCutting;
        }

        public void setIsyardWasteTreeCutting(boolean isyardWasteTreeCutting) {
            this.isyardWasteTreeCutting = isyardWasteTreeCutting;
        }

        @Override
        public String toString() {
            return "KindOfServiceDoProvide{" +
                    "lawnServiceId='" + lawnServiceId + '\'' +
                    ", lawnServiceName='" + lawnServiceName + '\'' +
                    ", isLawnServiceSelected=" + isLawnServiceSelected +
                    ", mowALownId='" + mowALownId + '\'' +
                    ", mowALawnName='" + mowALawnName + '\'' +
                    ", mowALawnDescription='" + mowALawnDescription + '\'' +
                    ", mowAMowALawnImage='" + mowAMowALawnImage + '\'' +
                    ", mowAMowPrice='" + mowAMowPrice + '\'' +
                    ", ismowALownSelected=" + ismowALownSelected +
                    ", areateALawnId='" + areateALawnId + '\'' +
                    ", areateALawnName='" + areateALawnName + '\'' +
                    ", areateALawnDescription='" + areateALawnDescription + '\'' +
                    ", areateALawnImage='" + areateALawnImage + '\'' +
                    ", areateALawnPrice='" + areateALawnPrice + '\'' +
                    ", isAreateALawn=" + isAreateALawn +
                    ", yardWasteId='" + yardWasteId + '\'' +
                    ", yearALawnName='" + yearALawnName + '\'' +
                    ", yearALawnDescription='" + yearALawnDescription + '\'' +
                    ", yearALawnImage='" + yearALawnImage + '\'' +
                    ", yearALawnPrice='" + yearALawnPrice + '\'' +
                    ", isyardWaste=" + isyardWaste +
                    ", treeCuttingId='" + treeCuttingId + '\'' +
                    ", treeCuttingServiceName='" + treeCuttingServiceName + '\'' +
                    ", isTreeCuttingSelected=" + isTreeCuttingSelected +
                    ", treeStrubsId='" + treeStrubsId + '\'' +
                    ", treeStrubsName='" + treeStrubsName + '\'' +
                    ", treeStrubsDescription='" + treeStrubsDescription + '\'' +
                    ", treeStrubsImage='" + treeStrubsImage + '\'' +
                    ", treeStrubsPrice='" + treeStrubsPrice + '\'' +
                    ", isTreeStrubsSelected=" + isTreeStrubsSelected +
                    ", treeStrumpsId='" + treeStrumpsId + '\'' +
                    ", treeStrumpsName='" + treeStrumpsName + '\'' +
                    ", treeStrumpsDescription='" + treeStrumpsDescription + '\'' +
                    ", treeStrumpsImage='" + treeStrumpsImage + '\'' +
                    ", treeStrumpsPrice='" + treeStrumpsPrice + '\'' +
                    ", isTreeStrumps=" + isTreeStrumps +
                    ", yardWasteTreeCuttingId='" + yardWasteTreeCuttingId + '\'' +
                    ", yardWasteTreeName='" + yardWasteTreeName + '\'' +
                    ", yardWasteTreeDescription='" + yardWasteTreeDescription + '\'' +
                    ", yardWasteTreeImage='" + yardWasteTreeImage + '\'' +
                    ", yeardWastTreePrice='" + yeardWastTreePrice + '\'' +
                    ", isyardWasteTreeCutting=" + isyardWasteTreeCutting +
                    '}';
        }
    }

    public static class BusinessInfo {

        String companyName = "";
        String yearFounded = "";
        String noOfEmployeeId = "";
        String noOfEmployee = "";
        String compRegisterNumber = "";
        String NOEOneId = "";
        String NOEOneName = "";
        String NOETwoId = "";
        String NOETwoName = "";
        String NOEThreeId = "";
        String NOEThreeName = "";


        //============================


        //========================


        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getYearFounded() {
            return yearFounded;
        }

        public void setYearFounded(String yearFounded) {
            this.yearFounded = yearFounded;
        }

        public String getNoOfEmployeeId() {
            return noOfEmployeeId;
        }

        public void setNoOfEmployeeId(String noOfEmployeeId) {
            this.noOfEmployeeId = noOfEmployeeId;
        }

        public String getNoOfEmployee() {
            return noOfEmployee;
        }

        public void setNoOfEmployee(String noOfEmployee) {
            this.noOfEmployee = noOfEmployee;
        }

        public String getCompRegisterNumber() {
            return compRegisterNumber;
        }

        public void setCompRegisterNumber(String compRegisterNumber) {
            this.compRegisterNumber = compRegisterNumber;
        }

        public String getNOEOneId() {
            return NOEOneId;
        }

        public void setNOEOneId(String NOEOneId) {
            this.NOEOneId = NOEOneId;
        }

        public String getNOEOneName() {
            return NOEOneName;
        }

        public void setNOEOneName(String NOEOneName) {
            this.NOEOneName = NOEOneName;
        }

        public String getNOETwoId() {
            return NOETwoId;
        }

        public void setNOETwoId(String NOETwoId) {
            this.NOETwoId = NOETwoId;
        }

        public String getNOETwoName() {
            return NOETwoName;
        }

        public void setNOETwoName(String NOETwoName) {
            this.NOETwoName = NOETwoName;
        }

        public String getNOEThreeId() {
            return NOEThreeId;
        }

        public void setNOEThreeId(String NOEThreeId) {
            this.NOEThreeId = NOEThreeId;
        }

        public String getNOEThreeName() {
            return NOEThreeName;
        }

        public void setNOEThreeName(String NOEThreeName) {
            this.NOEThreeName = NOEThreeName;
        }

        @Override
        public String toString() {
            return "BusinessInfo{" +
                    "companyName='" + companyName + '\'' +
                    ", yearFounded='" + yearFounded + '\'' +
                    ", noOfEmployee='" + noOfEmployee + '\'' +
                    ", compRegisterNumber='" + compRegisterNumber + '\'' +
                    '}';
        }
    }

    public static class BusinessLocation {

        String businessAddress = "";
        String apartment = "";
        String city = "";
        String state = "";
        String country = "";
        String pinCode = "";
        String City_Id = "";
        String Country_Id = "";
        String latitude = "";
        String longitude = "";


        //================

        //=================


        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getCity_Id() {
            return City_Id;
        }

        public void setCity_Id(String city_Id) {
            City_Id = city_Id;
        }

        public String getCountry_Id() {
            return Country_Id;
        }

        public void setCountry_Id(String country_Id) {
            Country_Id = country_Id;
        }

        public String getBusinessAddress() {
            return businessAddress;
        }

        public void setBusinessAddress(String businessAddress) {
            this.businessAddress = businessAddress;
        }

        public String getPinCode() {
            return pinCode;
        }

        public void setPinCode(String pinCode) {
            this.pinCode = pinCode;
        }


        public String getApartment() {
            return apartment;
        }

        public void setApartment(String apartment) {
            this.apartment = apartment;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        @Override
        public String toString() {
            return "BusinessLocation{" +
                    "businessAddress='" + businessAddress + '\'' +
                    ", pinCode='" + pinCode + '\'' +
                    '}';
        }
    }

    public static class IntroduceYourBusiness {

        String details = "";


        public String getDetails() {
            return details;
        }

        public void setDetails(String details) {
            this.details = details;
        }


        @Override
        public String toString() {
            return "IntroduceYourBusiness{" +
                    "details='" + details + '\'' +
                    '}';
        }
    }

    public static class BusinessHours {

        String mondayId = "";
        String mondayName = "";
        String mondayFromTimeId = "";
        String mondayToTimeId = "";
        String mondayFromTime = "";
        String mondayToTime = "";
        boolean isMondaySelected = false;

        String tuesdayId = "";
        String tuesdayName = "";
        String tuesdayFromTimeId = "";
        String tuesdayToTimeId = "";
        String tuesdayFromTime = "";
        String tuesdayToTime = "";
        boolean isTuesdaySelected = false;

        String wednesdayId = "";
        String wednesdayName = "";
        String wednesdayFromTimeId = "";
        String wednesdayToTimeId = "";
        String wednesdayFromTime = "";
        String wednesdayToTime = "";
        boolean isWednesdaySelected = false;

        String thursdayId = "";
        String thrusdayName = "";
        String thursdayFromTimeId = "";
        String thursdayToTimeId = "";
        String thursdayFromTime = "";
        String thrusdayToTime = "";
        boolean isThursdaySelected = false;


        String fridayId = "";
        String fridayName = "";
        String fridayFromTimeId = "";
        String fridayToTimeId = "";
        String fridayFromTime = "";
        String fridayToTime = "";
        boolean isFridaySelected = false;


        String saturdayId = "";
        String saturdayName = "";
        String saturdayFromTimeId = "";
        String saturdayToTimeId = "";
        String saturdayFromTime = "";
        String saturdayToTime = "";
        boolean isSaturdaySelected = false;

        String sundayId = "";
        String sundayName = "";
        String sundayFromTimeId = "";
        String sundayToTimeId = "";
        String sundayFromTime = "";
        String sundayToTime = "";
        boolean isSundaySelected = false;


        String OneHourId = "";
        String OneHourTime = "";

        String TwoHourId = "";
        String TwoHourTime = "";

        String ThreeHourId = "";
        String ThreeHourTime = "";

        String FourHourId = "";
        String FourHourTime = "";

        String FiveHourId = "";
        String FiveHourTime = "";

        String SixHourId = "";
        String SixHourTime = "";

        String SevenHourId = "";
        String SevenHourTime = "";

        String EightHourId = "";
        String EightHourTime = "";

        String NineHourId = "";
        String NineHourTime = "";

        String TenHourId = "";
        String TenHourTime = "";

        String ElevenHourId = "";
        String ElevenHourTime = "";

        String TwellveHourId = "";
        String TwelveHourTime = "";


        @Override
        public String toString() {
            return "BusinessHoursRequest{" +
                    "mondayId='" + mondayId + '\'' +
                    ", mondayName='" + mondayName + '\'' +
                    ", mondayFromTimeId='" + mondayFromTimeId + '\'' +
                    ", mondayToTimeId='" + mondayToTimeId + '\'' +
                    ", mondayFromTime='" + mondayFromTime + '\'' +
                    ", mondayToTime='" + mondayToTime + '\'' +
                    ", isMondaySelected=" + isMondaySelected +
                    ", tuesdayId='" + tuesdayId + '\'' +
                    ", tuesdayName='" + tuesdayName + '\'' +
                    ", tuesdayFromTimeId='" + tuesdayFromTimeId + '\'' +
                    ", tuesdayToTimeId='" + tuesdayToTimeId + '\'' +
                    ", tuesdayFromTime='" + tuesdayFromTime + '\'' +
                    ", tuesdayToTime='" + tuesdayToTime + '\'' +
                    ", isTuesdaySelected=" + isTuesdaySelected +
                    ", wednesdayId='" + wednesdayId + '\'' +
                    ", wednesdayName='" + wednesdayName + '\'' +
                    ", wednesdayFromTimeId='" + wednesdayFromTimeId + '\'' +
                    ", wednesdayToTimeId='" + wednesdayToTimeId + '\'' +
                    ", wednesdayFromTime='" + wednesdayFromTime + '\'' +
                    ", wednesdayToTime='" + wednesdayToTime + '\'' +
                    ", isWednesdaySelected=" + isWednesdaySelected +
                    ", thursdayId='" + thursdayId + '\'' +
                    ", thrusdayName='" + thrusdayName + '\'' +
                    ", thursdayFromTimeId='" + thursdayFromTimeId + '\'' +
                    ", thursdayToTimeId='" + thursdayToTimeId + '\'' +
                    ", thursdayFromTime='" + thursdayFromTime + '\'' +
                    ", thrusdayToTime='" + thrusdayToTime + '\'' +
                    ", isThursdaySelected=" + isThursdaySelected +
                    ", fridayId='" + fridayId + '\'' +
                    ", fridayName='" + fridayName + '\'' +
                    ", fridayFromTimeId='" + fridayFromTimeId + '\'' +
                    ", fridayToTimeId='" + fridayToTimeId + '\'' +
                    ", fridayFromTime='" + fridayFromTime + '\'' +
                    ", fridayToTime='" + fridayToTime + '\'' +
                    ", isFridaySelected=" + isFridaySelected +
                    ", saturdayId='" + saturdayId + '\'' +
                    ", saturdayName='" + saturdayName + '\'' +
                    ", saturdayFromTimeId='" + saturdayFromTimeId + '\'' +
                    ", saturdayToTimeId='" + saturdayToTimeId + '\'' +
                    ", saturdayFromTime='" + saturdayFromTime + '\'' +
                    ", saturdayToTime='" + saturdayToTime + '\'' +
                    ", isSaturdaySelected=" + isSaturdaySelected +
                    ", sundayId='" + sundayId + '\'' +
                    ", sundayName='" + sundayName + '\'' +
                    ", sundayFromTimeId='" + sundayFromTimeId + '\'' +
                    ", sundayToTimeId='" + sundayToTimeId + '\'' +
                    ", sundayFromTime='" + sundayFromTime + '\'' +
                    ", sundayToTime='" + sundayToTime + '\'' +
                    ", isSundaySelected=" + isSundaySelected +
                    ", OneHourId='" + OneHourId + '\'' +
                    ", OneHourTime='" + OneHourTime + '\'' +
                    ", TwoHourId='" + TwoHourId + '\'' +
                    ", TwoHourTime='" + TwoHourTime + '\'' +
                    ", ThreeHourId='" + ThreeHourId + '\'' +
                    ", ThreeHourTime='" + ThreeHourTime + '\'' +
                    ", FourHourId='" + FourHourId + '\'' +
                    ", FourHourTime='" + FourHourTime + '\'' +
                    ", FiveHourId='" + FiveHourId + '\'' +
                    ", FiveHourTime='" + FiveHourTime + '\'' +
                    ", SixHourId='" + SixHourId + '\'' +
                    ", SixHourTime='" + SixHourTime + '\'' +
                    ", SevenHourId='" + SevenHourId + '\'' +
                    ", SevenHourTime='" + SevenHourTime + '\'' +
                    ", EightHourId='" + EightHourId + '\'' +
                    ", EightHourTime='" + EightHourTime + '\'' +
                    ", NineHourId='" + NineHourId + '\'' +
                    ", NineHourTime='" + NineHourTime + '\'' +
                    ", TenHourId='" + TenHourId + '\'' +
                    ", TenHourTime='" + TenHourTime + '\'' +
                    ", ElevenHourId='" + ElevenHourId + '\'' +
                    ", ElevenHourTime='" + ElevenHourTime + '\'' +
                    ", TwellveHourId='" + TwellveHourId + '\'' +
                    ", TwelveHourTime='" + TwelveHourTime + '\'' +
                    '}';
        }

        public String getOneHourId() {
            return OneHourId;
        }

        public void setOneHourId(String oneHourId) {
            OneHourId = oneHourId;
        }

        public String getOneHourTime() {
            return OneHourTime;
        }

        public void setOneHourTime(String oneHourTime) {
            OneHourTime = oneHourTime;
        }

        public String getTwoHourId() {
            return TwoHourId;
        }

        public void setTwoHourId(String twoHourId) {
            TwoHourId = twoHourId;
        }

        public String getTwoHourTime() {
            return TwoHourTime;
        }

        public void setTwoHourTime(String twoHourTime) {
            TwoHourTime = twoHourTime;
        }

        public String getThreeHourId() {
            return ThreeHourId;
        }

        public void setThreeHourId(String threeHourId) {
            ThreeHourId = threeHourId;
        }

        public String getThreeHourTime() {
            return ThreeHourTime;
        }

        public void setThreeHourTime(String threeHourTime) {
            ThreeHourTime = threeHourTime;
        }

        public String getFourHourId() {
            return FourHourId;
        }

        public void setFourHourId(String fourHourId) {
            FourHourId = fourHourId;
        }

        public String getFourHourTime() {
            return FourHourTime;
        }

        public void setFourHourTime(String fourHourTime) {
            FourHourTime = fourHourTime;
        }

        public String getFiveHourId() {
            return FiveHourId;
        }

        public void setFiveHourId(String fiveHourId) {
            FiveHourId = fiveHourId;
        }

        public String getFiveHourTime() {
            return FiveHourTime;
        }

        public void setFiveHourTime(String fiveHourTime) {
            FiveHourTime = fiveHourTime;
        }

        public String getSixHourId() {
            return SixHourId;
        }

        public void setSixHourId(String sixHourId) {
            SixHourId = sixHourId;
        }

        public String getSixHourTime() {
            return SixHourTime;
        }

        public void setSixHourTime(String sixHourTime) {
            SixHourTime = sixHourTime;
        }

        public String getSevenHourId() {
            return SevenHourId;
        }

        public void setSevenHourId(String sevenHourId) {
            SevenHourId = sevenHourId;
        }

        public String getSevenHourTime() {
            return SevenHourTime;
        }

        public void setSevenHourTime(String sevenHourTime) {
            SevenHourTime = sevenHourTime;
        }

        public String getEightHourId() {
            return EightHourId;
        }

        public void setEightHourId(String eightHourId) {
            EightHourId = eightHourId;
        }

        public String getEightHourTime() {
            return EightHourTime;
        }

        public void setEightHourTime(String eightHourTime) {
            EightHourTime = eightHourTime;
        }

        public String getNineHourId() {
            return NineHourId;
        }

        public void setNineHourId(String nineHourId) {
            NineHourId = nineHourId;
        }

        public String getNineHourTime() {
            return NineHourTime;
        }

        public void setNineHourTime(String nineHourTime) {
            NineHourTime = nineHourTime;
        }

        public String getTenHourId() {
            return TenHourId;
        }

        public void setTenHourId(String tenHourId) {
            TenHourId = tenHourId;
        }

        public String getTenHourTime() {
            return TenHourTime;
        }

        public void setTenHourTime(String tenHourTime) {
            TenHourTime = tenHourTime;
        }

        public String getElevenHourId() {
            return ElevenHourId;
        }

        public void setElevenHourId(String elevenHourId) {
            ElevenHourId = elevenHourId;
        }

        public String getElevenHourTime() {
            return ElevenHourTime;
        }

        public void setElevenHourTime(String elevenHourTime) {
            ElevenHourTime = elevenHourTime;
        }

        public String getTwellveHourId() {
            return TwellveHourId;
        }

        public void setTwellveHourId(String twellveHourId) {
            TwellveHourId = twellveHourId;
        }

        public String getTwelveHourTime() {
            return TwelveHourTime;
        }

        public void setTwelveHourTime(String twelveHourTime) {
            TwelveHourTime = twelveHourTime;
        }

        public String getMondayId() {
            return mondayId;
        }

        public void setMondayId(String mondayId) {
            this.mondayId = mondayId;
        }

        public String getMondayName() {
            return mondayName;
        }

        public void setMondayName(String mondayName) {
            this.mondayName = mondayName;
        }

        public String getMondayFromTimeId() {
            return mondayFromTimeId;
        }

        public void setMondayFromTimeId(String mondayFromTimeId) {
            this.mondayFromTimeId = mondayFromTimeId;
        }

        public String getMondayToTimeId() {
            return mondayToTimeId;
        }

        public void setMondayToTimeId(String mondayToTimeId) {
            this.mondayToTimeId = mondayToTimeId;
        }

        public String getMondayFromTime() {
            return mondayFromTime;
        }

        public void setMondayFromTime(String mondayFromTime) {
            this.mondayFromTime = mondayFromTime;
        }

        public String getMondayToTime() {
            return mondayToTime;
        }

        public void setMondayToTime(String mondayToTime) {
            this.mondayToTime = mondayToTime;
        }

        public boolean isMondaySelected() {
            return isMondaySelected;
        }

        public void setMondaySelected(boolean mondaySelected) {
            isMondaySelected = mondaySelected;
        }

        public String getTuesdayId() {
            return tuesdayId;
        }

        public void setTuesdayId(String tuesdayId) {
            this.tuesdayId = tuesdayId;
        }

        public String getTuesdayName() {
            return tuesdayName;
        }

        public void setTuesdayName(String tuesdayName) {
            this.tuesdayName = tuesdayName;
        }

        public String getTuesdayFromTimeId() {
            return tuesdayFromTimeId;
        }

        public void setTuesdayFromTimeId(String tuesdayFromTimeId) {
            this.tuesdayFromTimeId = tuesdayFromTimeId;
        }

        public String getTuesdayToTimeId() {
            return tuesdayToTimeId;
        }

        public void setTuesdayToTimeId(String tuesdayToTimeId) {
            this.tuesdayToTimeId = tuesdayToTimeId;
        }

        public String getTuesdayFromTime() {
            return tuesdayFromTime;
        }

        public void setTuesdayFromTime(String tuesdayFromTime) {
            this.tuesdayFromTime = tuesdayFromTime;
        }

        public String getTuesdayToTime() {
            return tuesdayToTime;
        }

        public void setTuesdayToTime(String tuesdayToTime) {
            this.tuesdayToTime = tuesdayToTime;
        }

        public boolean isTuesdaySelected() {
            return isTuesdaySelected;
        }

        public void setTuesdaySelected(boolean tuesdaySelected) {
            isTuesdaySelected = tuesdaySelected;
        }

        public String getWednesdayId() {
            return wednesdayId;
        }

        public void setWednesdayId(String wednesdayId) {
            this.wednesdayId = wednesdayId;
        }

        public String getWednesdayName() {
            return wednesdayName;
        }

        public void setWednesdayName(String wednesdayName) {
            this.wednesdayName = wednesdayName;
        }

        public String getWednesdayFromTimeId() {
            return wednesdayFromTimeId;
        }

        public void setWednesdayFromTimeId(String wednesdayFromTimeId) {
            this.wednesdayFromTimeId = wednesdayFromTimeId;
        }

        public String getWednesdayToTimeId() {
            return wednesdayToTimeId;
        }

        public void setWednesdayToTimeId(String wednesdayToTimeId) {
            this.wednesdayToTimeId = wednesdayToTimeId;
        }

        public String getWednesdayFromTime() {
            return wednesdayFromTime;
        }

        public void setWednesdayFromTime(String wednesdayFromTime) {
            this.wednesdayFromTime = wednesdayFromTime;
        }

        public String getWednesdayToTime() {
            return wednesdayToTime;
        }

        public void setWednesdayToTime(String wednesdayToTime) {
            this.wednesdayToTime = wednesdayToTime;
        }

        public boolean isWednesdaySelected() {
            return isWednesdaySelected;
        }

        public void setWednesdaySelected(boolean wednesdaySelected) {
            isWednesdaySelected = wednesdaySelected;
        }

        public String getThursdayId() {
            return thursdayId;
        }

        public void setThursdayId(String thursdayId) {
            this.thursdayId = thursdayId;
        }

        public String getThrusdayName() {
            return thrusdayName;
        }

        public void setThrusdayName(String thrusdayName) {
            this.thrusdayName = thrusdayName;
        }

        public String getThursdayFromTimeId() {
            return thursdayFromTimeId;
        }

        public void setThursdayFromTimeId(String thursdayFromTimeId) {
            this.thursdayFromTimeId = thursdayFromTimeId;
        }

        public String getThursdayToTimeId() {
            return thursdayToTimeId;
        }

        public void setThursdayToTimeId(String thursdayToTimeId) {
            this.thursdayToTimeId = thursdayToTimeId;
        }

        public String getThursdayFromTime() {
            return thursdayFromTime;
        }

        public void setThursdayFromTime(String thursdayFromTime) {
            this.thursdayFromTime = thursdayFromTime;
        }

        public String getThrusdayToTime() {
            return thrusdayToTime;
        }

        public void setThrusdayToTime(String thrusdayToTime) {
            this.thrusdayToTime = thrusdayToTime;
        }

        public boolean isThursdaySelected() {
            return isThursdaySelected;
        }

        public void setThursdaySelected(boolean thursdaySelected) {
            isThursdaySelected = thursdaySelected;
        }

        public String getFridayId() {
            return fridayId;
        }

        public void setFridayId(String fridayId) {
            this.fridayId = fridayId;
        }

        public String getFridayName() {
            return fridayName;
        }

        public void setFridayName(String fridayName) {
            this.fridayName = fridayName;
        }

        public String getFridayFromTimeId() {
            return fridayFromTimeId;
        }

        public void setFridayFromTimeId(String fridayFromTimeId) {
            this.fridayFromTimeId = fridayFromTimeId;
        }

        public String getFridayToTimeId() {
            return fridayToTimeId;
        }

        public void setFridayToTimeId(String fridayToTimeId) {
            this.fridayToTimeId = fridayToTimeId;
        }

        public String getFridayFromTime() {
            return fridayFromTime;
        }

        public void setFridayFromTime(String fridayFromTime) {
            this.fridayFromTime = fridayFromTime;
        }

        public String getFridayToTime() {
            return fridayToTime;
        }

        public void setFridayToTime(String fridayToTime) {
            this.fridayToTime = fridayToTime;
        }

        public boolean isFridaySelected() {
            return isFridaySelected;
        }

        public void setFridaySelected(boolean fridaySelected) {
            isFridaySelected = fridaySelected;
        }

        public String getSaturdayId() {
            return saturdayId;
        }

        public void setSaturdayId(String saturdayId) {
            this.saturdayId = saturdayId;
        }

        public String getSaturdayName() {
            return saturdayName;
        }

        public void setSaturdayName(String saturdayName) {
            this.saturdayName = saturdayName;
        }

        public String getSaturdayFromTimeId() {
            return saturdayFromTimeId;
        }

        public void setSaturdayFromTimeId(String saturdayFromTimeId) {
            this.saturdayFromTimeId = saturdayFromTimeId;
        }

        public String getSaturdayToTimeId() {
            return saturdayToTimeId;
        }

        public void setSaturdayToTimeId(String saturdayToTimeId) {
            this.saturdayToTimeId = saturdayToTimeId;
        }

        public String getSaturdayFromTime() {
            return saturdayFromTime;
        }

        public void setSaturdayFromTime(String saturdayFromTime) {
            this.saturdayFromTime = saturdayFromTime;
        }

        public String getSaturdayToTime() {
            return saturdayToTime;
        }

        public void setSaturdayToTime(String saturdayToTime) {
            this.saturdayToTime = saturdayToTime;
        }

        public boolean isSaturdaySelected() {
            return isSaturdaySelected;
        }

        public void setSaturdaySelected(boolean saturdaySelected) {
            isSaturdaySelected = saturdaySelected;
        }

        public String getSundayId() {
            return sundayId;
        }

        public void setSundayId(String sundayId) {
            this.sundayId = sundayId;
        }

        public String getSundayName() {
            return sundayName;
        }

        public void setSundayName(String sundayName) {
            this.sundayName = sundayName;
        }

        public String getSundayFromTimeId() {
            return sundayFromTimeId;
        }

        public void setSundayFromTimeId(String sundayFromTimeId) {
            this.sundayFromTimeId = sundayFromTimeId;
        }

        public String getSundayToTimeId() {
            return sundayToTimeId;
        }

        public void setSundayToTimeId(String sundayToTimeId) {
            this.sundayToTimeId = sundayToTimeId;
        }

        public String getSundayFromTime() {
            return sundayFromTime;
        }

        public void setSundayFromTime(String sundayFromTime) {
            this.sundayFromTime = sundayFromTime;
        }

        public String getSundayToTime() {
            return sundayToTime;
        }

        public void setSundayToTime(String sundayToTime) {
            this.sundayToTime = sundayToTime;
        }

        public boolean isSundaySelected() {
            return isSundaySelected;
        }

        public void setSundaySelected(boolean sundaySelected) {
            isSundaySelected = sundaySelected;
        }

    }

    public static class AdvanceTimeToBookAJob {


        String Count1Id = "";
        String Count1Count = "";

        String Count2Id = "";
        String Count2Count = "";

        String Count3Id = "";
        String Count3Count = "";

        String Count4Id = "";
        String Count4Count = "";

        String Count5Id = "";
        String Count5Count = "";

        String Count6Id = "";
        String Count6Count = "";

        String Count7Id = "";
        String Count7Count = "";

        String Count8Id = "";
        String Count8Count = "";

        String Count9Id = "";
        String Count9Count = "";

        String Count10Id = "";
        String Count10Count = "";

        String Count11Id = "";
        String Count11Count = "";

        String Count12Id = "";
        String Count12Count = "";

        String Count13Id = "";
        String Count13Count = "";

        String Count14Id = "";
        String Count14Count = "";

        String Count15Id = "";
        String Count15Count = "";

        String Count16Id = "";
        String Count16Count = "";

        String Count17Id = "";
        String Count17Count = "";


        String Count18Id = "";
        String Count18Count = "";


        String Count19Id = "";
        String Count19Count = "";

        String Count20Id = "";
        String Count20Count = "";

        String Count21Id = "";
        String Count21Count = "";

        String Count22Id = "";
        String Count22Count = "";


        String Count23Id = "";
        String Count23Count = "";

        String Count24Id = "";
        String Count24Count = "";

        String Count25Id = "";
        String Count25Count = "";

        String Count26Id = "";
        String Count26Count = "";

        String Count27Id = "";
        String Count27Count = "";

        String Count28Id = "";
        String Count28Count = "";

        String Count29Id = "";
        String Count29Count = "";


        String Count30Id = "";
        String Count30Count = "";


        String howFar1Id = "";
        String howFar1Unit = "";

        String howFar2Id = "";
        String howFar2Unit = "";

        String howFar3Id = "";
        String howFar3Unit = "";


        String notice1Id = "";
        String notice1Unite = "";

        String notice2Id = "";
        String notice2Unite = "";


        String FieldAdvanceValueId = "";
        String FieldAdvanceValue = "";
        String FieldAdvanceNameId = "";
        String FieldAdvanceName = "";
        String FieldNoticeValueId = "";
        String FieldNoticeValue = "";
        String FieldNoticeNameId = "";
        String FieldNoticeName = "";


        public String getFieldAdvanceValueId() {
            return FieldAdvanceValueId;
        }

        public void setFieldAdvanceValueId(String fieldAdvanceValueId) {
            FieldAdvanceValueId = fieldAdvanceValueId;
        }

        public String getFieldAdvanceValue() {
            return FieldAdvanceValue;
        }

        public void setFieldAdvanceValue(String fieldAdvanceValue) {
            FieldAdvanceValue = fieldAdvanceValue;
        }

        public String getFieldAdvanceNameId() {
            return FieldAdvanceNameId;
        }

        public void setFieldAdvanceNameId(String fieldAdvanceNameId) {
            FieldAdvanceNameId = fieldAdvanceNameId;
        }

        public String getFieldAdvanceName() {
            return FieldAdvanceName;
        }

        public void setFieldAdvanceName(String fieldAdvanceName) {
            FieldAdvanceName = fieldAdvanceName;
        }

        public String getFieldNoticeValueId() {
            return FieldNoticeValueId;
        }

        public void setFieldNoticeValueId(String fieldNoticeValueId) {
            FieldNoticeValueId = fieldNoticeValueId;
        }

        public String getFieldNoticeValue() {
            return FieldNoticeValue;
        }

        public void setFieldNoticeValue(String fieldNoticeValue) {
            FieldNoticeValue = fieldNoticeValue;
        }

        public String getFieldNoticeNameId() {
            return FieldNoticeNameId;
        }

        public void setFieldNoticeNameId(String fieldNoticeNameId) {
            FieldNoticeNameId = fieldNoticeNameId;
        }

        public String getFieldNoticeName() {
            return FieldNoticeName;
        }

        public void setFieldNoticeName(String fieldNoticeName) {
            FieldNoticeName = fieldNoticeName;
        }

        public String getNotice1Id() {
            return notice1Id;
        }

        public void setNotice1Id(String notice1Id) {
            this.notice1Id = notice1Id;
        }

        public String getNotice1Unite() {
            return notice1Unite;
        }

        public void setNotice1Unite(String notice1Unite) {
            this.notice1Unite = notice1Unite;
        }

        public String getNotice2Id() {
            return notice2Id;
        }

        public void setNotice2Id(String notice2Id) {
            this.notice2Id = notice2Id;
        }

        public String getNotice2Unite() {
            return notice2Unite;
        }

        public void setNotice2Unite(String notice2Unite) {
            this.notice2Unite = notice2Unite;
        }

        public String getHowFar1Id() {
            return howFar1Id;
        }

        public void setHowFar1Id(String howFar1Id) {
            this.howFar1Id = howFar1Id;
        }

        public String getHowFar1Unit() {
            return howFar1Unit;
        }

        public void setHowFar1Unit(String howFar1Unit) {
            this.howFar1Unit = howFar1Unit;
        }

        public String getHowFar2Id() {
            return howFar2Id;
        }

        public void setHowFar2Id(String howFar2Id) {
            this.howFar2Id = howFar2Id;
        }

        public String getHowFar2Unit() {
            return howFar2Unit;
        }

        public void setHowFar2Unit(String howFar2Unit) {
            this.howFar2Unit = howFar2Unit;
        }

        public String getHowFar3Id() {
            return howFar3Id;
        }

        public void setHowFar3Id(String howFar3Id) {
            this.howFar3Id = howFar3Id;
        }

        public String getHowFar3Unit() {
            return howFar3Unit;
        }

        public void setHowFar3Unit(String howFar3Unit) {
            this.howFar3Unit = howFar3Unit;
        }

        public String getCount1Id() {
            return Count1Id;
        }

        public void setCount1Id(String count1Id) {
            Count1Id = count1Id;
        }

        public String getCount1Count() {
            return Count1Count;
        }

        public void setCount1Count(String count1Count) {
            Count1Count = count1Count;
        }

        public String getCount2Id() {
            return Count2Id;
        }

        public void setCount2Id(String count2Id) {
            Count2Id = count2Id;
        }

        public String getCount2Count() {
            return Count2Count;
        }

        public void setCount2Count(String count2Count) {
            Count2Count = count2Count;
        }

        public String getCount3Id() {
            return Count3Id;
        }

        public void setCount3Id(String count3Id) {
            Count3Id = count3Id;
        }

        public String getCount3Count() {
            return Count3Count;
        }

        public void setCount3Count(String count3Count) {
            Count3Count = count3Count;
        }

        public String getCount4Id() {
            return Count4Id;
        }

        public void setCount4Id(String count4Id) {
            Count4Id = count4Id;
        }

        public String getCount4Count() {
            return Count4Count;
        }

        public void setCount4Count(String count4Count) {
            Count4Count = count4Count;
        }

        public String getCount5Id() {
            return Count5Id;
        }

        public void setCount5Id(String count5Id) {
            Count5Id = count5Id;
        }

        public String getCount5Count() {
            return Count5Count;
        }

        public void setCount5Count(String count5Count) {
            Count5Count = count5Count;
        }

        public String getCount6Id() {
            return Count6Id;
        }

        public void setCount6Id(String count6Id) {
            Count6Id = count6Id;
        }

        public String getCount6Count() {
            return Count6Count;
        }

        public void setCount6Count(String count6Count) {
            Count6Count = count6Count;
        }

        public String getCount7Id() {
            return Count7Id;
        }

        public void setCount7Id(String count7Id) {
            Count7Id = count7Id;
        }

        public String getCount7Count() {
            return Count7Count;
        }

        public void setCount7Count(String count7Count) {
            Count7Count = count7Count;
        }

        public String getCount8Id() {
            return Count8Id;
        }

        public void setCount8Id(String count8Id) {
            Count8Id = count8Id;
        }

        public String getCount8Count() {
            return Count8Count;
        }

        public void setCount8Count(String count8Count) {
            Count8Count = count8Count;
        }

        public String getCount9Id() {
            return Count9Id;
        }

        public void setCount9Id(String count9Id) {
            Count9Id = count9Id;
        }

        public String getCount9Count() {
            return Count9Count;
        }

        public void setCount9Count(String count9Count) {
            Count9Count = count9Count;
        }

        public String getCount10Id() {
            return Count10Id;
        }

        public void setCount10Id(String count10Id) {
            Count10Id = count10Id;
        }

        public String getCount10Count() {
            return Count10Count;
        }

        public void setCount10Count(String count10Count) {
            Count10Count = count10Count;
        }

        public String getCount11Id() {
            return Count11Id;
        }

        public void setCount11Id(String count11Id) {
            Count11Id = count11Id;
        }

        public String getCount11Count() {
            return Count11Count;
        }

        public void setCount11Count(String count11Count) {
            Count11Count = count11Count;
        }

        public String getCount12Id() {
            return Count12Id;
        }

        public void setCount12Id(String count12Id) {
            Count12Id = count12Id;
        }

        public String getCount12Count() {
            return Count12Count;
        }

        public void setCount12Count(String count12Count) {
            Count12Count = count12Count;
        }

        public String getCount13Id() {
            return Count13Id;
        }

        public void setCount13Id(String count13Id) {
            Count13Id = count13Id;
        }

        public String getCount13Count() {
            return Count13Count;
        }

        public void setCount13Count(String count13Count) {
            Count13Count = count13Count;
        }

        public String getCount14Id() {
            return Count14Id;
        }

        public void setCount14Id(String count14Id) {
            Count14Id = count14Id;
        }

        public String getCount14Count() {
            return Count14Count;
        }

        public void setCount14Count(String count14Count) {
            Count14Count = count14Count;
        }

        public String getCount15Id() {
            return Count15Id;
        }

        public void setCount15Id(String count15Id) {
            Count15Id = count15Id;
        }

        public String getCount15Count() {
            return Count15Count;
        }

        public void setCount15Count(String count15Count) {
            Count15Count = count15Count;
        }

        public String getCount16Id() {
            return Count16Id;
        }

        public void setCount16Id(String count16Id) {
            Count16Id = count16Id;
        }

        public String getCount16Count() {
            return Count16Count;
        }

        public void setCount16Count(String count16Count) {
            Count16Count = count16Count;
        }

        public String getCount17Id() {
            return Count17Id;
        }

        public void setCount17Id(String count17Id) {
            Count17Id = count17Id;
        }

        public String getCount17Count() {
            return Count17Count;
        }

        public void setCount17Count(String count17Count) {
            Count17Count = count17Count;
        }

        public String getCount18Id() {
            return Count18Id;
        }

        public void setCount18Id(String count18Id) {
            Count18Id = count18Id;
        }

        public String getCount18Count() {
            return Count18Count;
        }

        public void setCount18Count(String count18Count) {
            Count18Count = count18Count;
        }

        public String getCount19Id() {
            return Count19Id;
        }

        public void setCount19Id(String count19Id) {
            Count19Id = count19Id;
        }

        public String getCount19Count() {
            return Count19Count;
        }

        public void setCount19Count(String count19Count) {
            Count19Count = count19Count;
        }

        public String getCount20Id() {
            return Count20Id;
        }

        public void setCount20Id(String count20Id) {
            Count20Id = count20Id;
        }

        public String getCount20Count() {
            return Count20Count;
        }

        public void setCount20Count(String count20Count) {
            Count20Count = count20Count;
        }

        public String getCount21Id() {
            return Count21Id;
        }

        public void setCount21Id(String count21Id) {
            Count21Id = count21Id;
        }

        public String getCount21Count() {
            return Count21Count;
        }

        public void setCount21Count(String count21Count) {
            Count21Count = count21Count;
        }

        public String getCount22Id() {
            return Count22Id;
        }

        public void setCount22Id(String count22Id) {
            Count22Id = count22Id;
        }

        public String getCount22Count() {
            return Count22Count;
        }

        public void setCount22Count(String count22Count) {
            Count22Count = count22Count;
        }

        public String getCount23Id() {
            return Count23Id;
        }

        public void setCount23Id(String count23Id) {
            Count23Id = count23Id;
        }

        public String getCount23Count() {
            return Count23Count;
        }

        public void setCount23Count(String count23Count) {
            Count23Count = count23Count;
        }

        public String getCount24Id() {
            return Count24Id;
        }

        public void setCount24Id(String count24Id) {
            Count24Id = count24Id;
        }

        public String getCount24Count() {
            return Count24Count;
        }

        public void setCount24Count(String count24Count) {
            Count24Count = count24Count;
        }

        public String getCount25Id() {
            return Count25Id;
        }

        public void setCount25Id(String count25Id) {
            Count25Id = count25Id;
        }

        public String getCount25Count() {
            return Count25Count;
        }

        public void setCount25Count(String count25Count) {
            Count25Count = count25Count;
        }

        public String getCount26Id() {
            return Count26Id;
        }

        public void setCount26Id(String count26Id) {
            Count26Id = count26Id;
        }

        public String getCount26Count() {
            return Count26Count;
        }

        public void setCount26Count(String count26Count) {
            Count26Count = count26Count;
        }

        public String getCount27Id() {
            return Count27Id;
        }

        public void setCount27Id(String count27Id) {
            Count27Id = count27Id;
        }

        public String getCount27Count() {
            return Count27Count;
        }

        public void setCount27Count(String count27Count) {
            Count27Count = count27Count;
        }

        public String getCount28Id() {
            return Count28Id;
        }

        public void setCount28Id(String count28Id) {
            Count28Id = count28Id;
        }

        public String getCount28Count() {
            return Count28Count;
        }

        public void setCount28Count(String count28Count) {
            Count28Count = count28Count;
        }

        public String getCount29Id() {
            return Count29Id;
        }

        public void setCount29Id(String count29Id) {
            Count29Id = count29Id;
        }

        public String getCount29Count() {
            return Count29Count;
        }

        public void setCount29Count(String count29Count) {
            Count29Count = count29Count;
        }

        public String getCount30Id() {
            return Count30Id;
        }

        public void setCount30Id(String count30Id) {
            Count30Id = count30Id;
        }

        public String getCount30Count() {
            return Count30Count;
        }

        public void setCount30Count(String count30Count) {
            Count30Count = count30Count;
        }

        @Override
        public String toString() {
            return "AdvanceTimeToBookAJob{" +

                    '}';
        }
    }

    public static class TypeOfPropertyWorkOn {

        String residentialId = "";
        String residentialName = "";
        boolean isResidential = false;

        String commercialId = "";
        String commercialName = "";
        boolean isCommercial = false;

        String lincenceNumber = "";
        boolean isHaveALincence = false;

        public String getResidentialName() {
            return residentialName;
        }

        public void setResidentialName(String residentialName) {
            this.residentialName = residentialName;
        }

        public String getCommercialName() {
            return commercialName;
        }

        public void setCommercialName(String commercialName) {
            this.commercialName = commercialName;
        }

        public String getResidentialId() {
            return residentialId;
        }

        public void setResidentialId(String residentialId) {
            this.residentialId = residentialId;
        }

        public boolean isResidential() {
            return isResidential;
        }

        public void setResidential(boolean residential) {
            isResidential = residential;
        }

        public String getCommercialId() {
            return commercialId;
        }

        public void setCommercialId(String commercialId) {
            this.commercialId = commercialId;
        }

        public boolean isCommercial() {
            return isCommercial;
        }

        public void setCommercial(boolean commercial) {
            isCommercial = commercial;
        }

        public String getLincenceNumber() {
            return lincenceNumber;
        }

        public void setLincenceNumber(String lincenceNumber) {
            this.lincenceNumber = lincenceNumber;
        }

        public boolean isHaveALincence() {
            return isHaveALincence;
        }

        public void setHaveALincence(boolean haveALincence) {
            isHaveALincence = haveALincence;
        }

        @Override
        public String toString() {
            return "TypeOfPropertyWorkOn{" +
                    "residentialId='" + residentialId + '\'' +
                    ", residentialName='" + residentialName + '\'' +
                    ", isResidential=" + isResidential +
                    ", commercialId='" + commercialId + '\'' +
                    ", commercialName='" + commercialName + '\'' +
                    ", isCommercial=" + isCommercial +
                    ", lincenceNumber='" + lincenceNumber + '\'' +
                    ", isHaveALincence=" + isHaveALincence +
                    '}';
        }
    }

    public static class BusinessPriceLawnSerce {

        String LawnServceId = "";
        String LawnServiceName = "";

        String TreeCuttingServiceId = "";
        String TreeCuttingServiceName = "";

        String LsmallId = "";
        String LsmallName = "";
        String LsmallDescription = "";
        String LsmallImage = "";
        String LsmaillPrice = "";

        String LmedId = "";
        String LmedName = "";
        String LmedDescription = "";
        String LmedImage = "";
        String LmedPrice = "";

        String LlargeId = "";
        String LlargeName = "";
        String LlargeDescription = "";
        String LlargeImage = "";
        String LlargePrice = "";

        String LvryLargeId = "";
        String LveryLargeName = "";
        String LveryLargeDescription = "";
        String LveryLargeImage = "";
        String LvryLargePrice = "";

        String LunSureId = "";
        String LunSureName = "";
        String LunSureDescription = "";
        String LunSureImage = "";
        String LunSurePrice = "";


        String TOneTreeId = "";
        String TOneTreeName = "";
        String TOneTreeDescription = "";
        String TOneTreeImage = "";
        String TOneTreePrice = "";

        String TtwoTothreeId = "";
        String TtwoTothreeName = "";
        String TtwoTothreeDescription = "";
        String TtwoTothreeImage = "";
        String TtwoTothreePrice = "";

        String TfourTofiveId = "";
        String TfourTofiveName = "";
        String TfourTofiveDescription = "";
        String TfourTofiveImage = "";
        String TfourTofivePrice = "";

        String TmoreFiveId = "";
        String TmoreFiveName = "";
        String TmoreFiveDescription = "";
        String TmoreFiveImage = "";
        String TmoreFivePrice = "";


        @Override
        public String toString() {
            return "BusinessPriceLawnSerce{" +
                    "LawnServceId='" + LawnServceId + '\'' +
                    ", LawnServiceName='" + LawnServiceName + '\'' +
                    ", TreeCuttingServiceId='" + TreeCuttingServiceId + '\'' +
                    ", TreeCuttingServiceName='" + TreeCuttingServiceName + '\'' +
                    ", LsmallId='" + LsmallId + '\'' +
                    ", LsmallName='" + LsmallName + '\'' +
                    ", LsmallDescription='" + LsmallDescription + '\'' +
                    ", LsmallImage='" + LsmallImage + '\'' +
                    ", LsmaillPrice='" + LsmaillPrice + '\'' +
                    ", LmedId='" + LmedId + '\'' +
                    ", LmedName='" + LmedName + '\'' +
                    ", LmedDescription='" + LmedDescription + '\'' +
                    ", LmedImage='" + LmedImage + '\'' +
                    ", LmedPrice='" + LmedPrice + '\'' +
                    ", LlargeId='" + LlargeId + '\'' +
                    ", LlargeName='" + LlargeName + '\'' +
                    ", LlargeDescription='" + LlargeDescription + '\'' +
                    ", LlargeImage='" + LlargeImage + '\'' +
                    ", LlargePrice='" + LlargePrice + '\'' +
                    ", LvryLargeId='" + LvryLargeId + '\'' +
                    ", LveryLargeName='" + LveryLargeName + '\'' +
                    ", LveryLargeDescription='" + LveryLargeDescription + '\'' +
                    ", LveryLargeImage='" + LveryLargeImage + '\'' +
                    ", LvryLargePrice='" + LvryLargePrice + '\'' +
                    ", LunSureId='" + LunSureId + '\'' +
                    ", LunSureName='" + LunSureName + '\'' +
                    ", LunSureDescription='" + LunSureDescription + '\'' +
                    ", LunSureImage='" + LunSureImage + '\'' +
                    ", LunSurePrice='" + LunSurePrice + '\'' +
                    ", TOneTreeId='" + TOneTreeId + '\'' +
                    ", TOneTreeName='" + TOneTreeName + '\'' +
                    ", TOneTreeDescription='" + TOneTreeDescription + '\'' +
                    ", TOneTreeImage='" + TOneTreeImage + '\'' +
                    ", TOneTreePrice='" + TOneTreePrice + '\'' +
                    ", TtwoTothreeId='" + TtwoTothreeId + '\'' +
                    ", TtwoTothreeName='" + TtwoTothreeName + '\'' +
                    ", TtwoTothreeDescription='" + TtwoTothreeDescription + '\'' +
                    ", TtwoTothreeImage='" + TtwoTothreeImage + '\'' +
                    ", TtwoTothreePrice='" + TtwoTothreePrice + '\'' +
                    ", TfourTofiveId='" + TfourTofiveId + '\'' +
                    ", TfourTofiveName='" + TfourTofiveName + '\'' +
                    ", TfourTofiveDescription='" + TfourTofiveDescription + '\'' +
                    ", TfourTofiveImage='" + TfourTofiveImage + '\'' +
                    ", TfourTofivePrice='" + TfourTofivePrice + '\'' +
                    ", TmoreFiveId='" + TmoreFiveId + '\'' +
                    ", TmoreFiveName='" + TmoreFiveName + '\'' +
                    ", TmoreFiveDescription='" + TmoreFiveDescription + '\'' +
                    ", TmoreFiveImage='" + TmoreFiveImage + '\'' +
                    ", TmoreFivePrice='" + TmoreFivePrice + '\'' +
                    '}';
        }

        public String getLawnServceId() {
            return LawnServceId;
        }

        public void setLawnServceId(String lawnServceId) {
            LawnServceId = lawnServceId;
        }

        public String getLawnServiceName() {
            return LawnServiceName;
        }

        public void setLawnServiceName(String lawnServiceName) {
            LawnServiceName = lawnServiceName;
        }

        public String getTreeCuttingServiceId() {
            return TreeCuttingServiceId;
        }

        public void setTreeCuttingServiceId(String treeCuttingServiceId) {
            TreeCuttingServiceId = treeCuttingServiceId;
        }

        public String getTreeCuttingServiceName() {
            return TreeCuttingServiceName;
        }

        public void setTreeCuttingServiceName(String treeCuttingServiceName) {
            TreeCuttingServiceName = treeCuttingServiceName;
        }

        public String getLsmallId() {
            return LsmallId;
        }

        public void setLsmallId(String lsmallId) {
            LsmallId = lsmallId;
        }

        public String getLsmallName() {
            return LsmallName;
        }

        public void setLsmallName(String lsmallName) {
            LsmallName = lsmallName;
        }

        public String getLsmallDescription() {
            return LsmallDescription;
        }

        public void setLsmallDescription(String lsmallDescription) {
            LsmallDescription = lsmallDescription;
        }

        public String getLsmallImage() {
            return LsmallImage;
        }

        public void setLsmallImage(String lsmallImage) {
            LsmallImage = lsmallImage;
        }

        public String getLsmaillPrice() {
            return LsmaillPrice;
        }

        public void setLsmaillPrice(String lsmaillPrice) {
            LsmaillPrice = lsmaillPrice;
        }

        public String getLmedId() {
            return LmedId;
        }

        public void setLmedId(String lmedId) {
            LmedId = lmedId;
        }

        public String getLmedName() {
            return LmedName;
        }

        public void setLmedName(String lmedName) {
            LmedName = lmedName;
        }

        public String getLmedDescription() {
            return LmedDescription;
        }

        public void setLmedDescription(String lmedDescription) {
            LmedDescription = lmedDescription;
        }

        public String getLmedImage() {
            return LmedImage;
        }

        public void setLmedImage(String lmedImage) {
            LmedImage = lmedImage;
        }

        public String getLmedPrice() {
            return LmedPrice;
        }

        public void setLmedPrice(String lmedPrice) {
            LmedPrice = lmedPrice;
        }

        public String getLlargeId() {
            return LlargeId;
        }

        public void setLlargeId(String llargeId) {
            LlargeId = llargeId;
        }

        public String getLlargeName() {
            return LlargeName;
        }

        public void setLlargeName(String llargeName) {
            LlargeName = llargeName;
        }

        public String getLlargeDescription() {
            return LlargeDescription;
        }

        public void setLlargeDescription(String llargeDescription) {
            LlargeDescription = llargeDescription;
        }

        public String getLlargeImage() {
            return LlargeImage;
        }

        public void setLlargeImage(String llargeImage) {
            LlargeImage = llargeImage;
        }

        public String getLlargePrice() {
            return LlargePrice;
        }

        public void setLlargePrice(String llargePrice) {
            LlargePrice = llargePrice;
        }

        public String getLvryLargeId() {
            return LvryLargeId;
        }

        public void setLvryLargeId(String lvryLargeId) {
            LvryLargeId = lvryLargeId;
        }

        public String getLveryLargeName() {
            return LveryLargeName;
        }

        public void setLveryLargeName(String lveryLargeName) {
            LveryLargeName = lveryLargeName;
        }

        public String getLveryLargeDescription() {
            return LveryLargeDescription;
        }

        public void setLveryLargeDescription(String lveryLargeDescription) {
            LveryLargeDescription = lveryLargeDescription;
        }

        public String getLveryLargeImage() {
            return LveryLargeImage;
        }

        public void setLveryLargeImage(String lveryLargeImage) {
            LveryLargeImage = lveryLargeImage;
        }

        public String getLvryLargePrice() {
            return LvryLargePrice;
        }

        public void setLvryLargePrice(String lvryLargePrice) {
            LvryLargePrice = lvryLargePrice;
        }

        public String getLunSureId() {
            return LunSureId;
        }

        public void setLunSureId(String lunSureId) {
            LunSureId = lunSureId;
        }

        public String getLunSureName() {
            return LunSureName;
        }

        public void setLunSureName(String lunSureName) {
            LunSureName = lunSureName;
        }

        public String getLunSureDescription() {
            return LunSureDescription;
        }

        public void setLunSureDescription(String lunSureDescription) {
            LunSureDescription = lunSureDescription;
        }

        public String getLunSureImage() {
            return LunSureImage;
        }

        public void setLunSureImage(String lunSureImage) {
            LunSureImage = lunSureImage;
        }

        public String getLunSurePrice() {
            return LunSurePrice;
        }

        public void setLunSurePrice(String lunSurePrice) {
            LunSurePrice = lunSurePrice;
        }

        public String getTOneTreeId() {
            return TOneTreeId;
        }

        public void setTOneTreeId(String TOneTreeId) {
            this.TOneTreeId = TOneTreeId;
        }

        public String getTOneTreeName() {
            return TOneTreeName;
        }

        public void setTOneTreeName(String TOneTreeName) {
            this.TOneTreeName = TOneTreeName;
        }

        public String getTOneTreeDescription() {
            return TOneTreeDescription;
        }

        public void setTOneTreeDescription(String TOneTreeDescription) {
            this.TOneTreeDescription = TOneTreeDescription;
        }

        public String getTOneTreeImage() {
            return TOneTreeImage;
        }

        public void setTOneTreeImage(String TOneTreeImage) {
            this.TOneTreeImage = TOneTreeImage;
        }

        public String getTOneTreePrice() {
            return TOneTreePrice;
        }

        public void setTOneTreePrice(String TOneTreePrice) {
            this.TOneTreePrice = TOneTreePrice;
        }

        public String getTtwoTothreeId() {
            return TtwoTothreeId;
        }

        public void setTtwoTothreeId(String ttwoTothreeId) {
            TtwoTothreeId = ttwoTothreeId;
        }

        public String getTtwoTothreeName() {
            return TtwoTothreeName;
        }

        public void setTtwoTothreeName(String ttwoTothreeName) {
            TtwoTothreeName = ttwoTothreeName;
        }

        public String getTtwoTothreeDescription() {
            return TtwoTothreeDescription;
        }

        public void setTtwoTothreeDescription(String ttwoTothreeDescription) {
            TtwoTothreeDescription = ttwoTothreeDescription;
        }

        public String getTtwoTothreeImage() {
            return TtwoTothreeImage;
        }

        public void setTtwoTothreeImage(String ttwoTothreeImage) {
            TtwoTothreeImage = ttwoTothreeImage;
        }

        public String getTtwoTothreePrice() {
            return TtwoTothreePrice;
        }

        public void setTtwoTothreePrice(String ttwoTothreePrice) {
            TtwoTothreePrice = ttwoTothreePrice;
        }

        public String getTfourTofiveId() {
            return TfourTofiveId;
        }

        public void setTfourTofiveId(String tfourTofiveId) {
            TfourTofiveId = tfourTofiveId;
        }

        public String getTfourTofiveName() {
            return TfourTofiveName;
        }

        public void setTfourTofiveName(String tfourTofiveName) {
            TfourTofiveName = tfourTofiveName;
        }

        public String getTfourTofiveDescription() {
            return TfourTofiveDescription;
        }

        public void setTfourTofiveDescription(String tfourTofiveDescription) {
            TfourTofiveDescription = tfourTofiveDescription;
        }

        public String getTfourTofiveImage() {
            return TfourTofiveImage;
        }

        public void setTfourTofiveImage(String tfourTofiveImage) {
            TfourTofiveImage = tfourTofiveImage;
        }

        public String getTfourTofivePrice() {
            return TfourTofivePrice;
        }

        public void setTfourTofivePrice(String tfourTofivePrice) {
            TfourTofivePrice = tfourTofivePrice;
        }

        public String getTmoreFiveId() {
            return TmoreFiveId;
        }

        public void setTmoreFiveId(String tmoreFiveId) {
            TmoreFiveId = tmoreFiveId;
        }

        public String getTmoreFiveName() {
            return TmoreFiveName;
        }

        public void setTmoreFiveName(String tmoreFiveName) {
            TmoreFiveName = tmoreFiveName;
        }

        public String getTmoreFiveDescription() {
            return TmoreFiveDescription;
        }

        public void setTmoreFiveDescription(String tmoreFiveDescription) {
            TmoreFiveDescription = tmoreFiveDescription;
        }

        public String getTmoreFiveImage() {
            return TmoreFiveImage;
        }

        public void setTmoreFiveImage(String tmoreFiveImage) {
            TmoreFiveImage = tmoreFiveImage;
        }

        public String getTmoreFivePrice() {
            return TmoreFivePrice;
        }

        public void setTmoreFivePrice(String tmoreFivePrice) {
            TmoreFivePrice = tmoreFivePrice;
        }
    }

    public static class PhotoDescribeProject {

        String isAddad;
        String imageUrl;
        String imageOne;
        String imageTwo;
        String imageThree;
        String imageFour;
        String imageFive;

        public String getIsAddad() {
            return isAddad;
        }

        public void setIsAddad(String isAddad) {
            this.isAddad = isAddad;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getImageOne() {
            return imageOne;
        }

        public void setImageOne(String imageOne) {
            this.imageOne = imageOne;
        }

        public String getImageTwo() {
            return imageTwo;
        }

        public void setImageTwo(String imageTwo) {
            this.imageTwo = imageTwo;
        }

        public String getImageThree() {
            return imageThree;
        }

        public void setImageThree(String imageThree) {
            this.imageThree = imageThree;
        }

        public String getImageFour() {
            return imageFour;
        }

        public void setImageFour(String imageFour) {
            this.imageFour = imageFour;
        }

        public String getImageFive() {
            return imageFive;
        }

        public void setImageFive(String imageFive) {
            this.imageFive = imageFive;
        }

        @Override
        public String toString() {
            return "PhotoDescribeProject{" +
                    "isAddad='" + isAddad + '\'' +
                    ", imageUrl='" + imageUrl + '\'' +
                    ", imageOne='" + imageOne + '\'' +
                    ", imageTwo='" + imageTwo + '\'' +
                    ", imageThree='" + imageThree + '\'' +
                    ", imageFour='" + imageFour + '\'' +
                    ", imageFive='" + imageFive + '\'' +
                    '}';
        }
    }


    public static class SocialMediaLink {
        String sm_one;
        String sm_two;
        String sm_three;
        String sm_four;
        String sm_five;
        String website_Url;

        public String getSm_five() {
            return sm_five;
        }

        public void setSm_five(String sm_five) {
            this.sm_five = sm_five;
        }

        public String getSm_one() {
            return sm_one;
        }

        public void setSm_one(String sm_one) {
            this.sm_one = sm_one;
        }

        public String getSm_two() {
            return sm_two;
        }

        public void setSm_two(String sm_two) {
            this.sm_two = sm_two;
        }

        public String getSm_three() {
            return sm_three;
        }

        public void setSm_three(String sm_three) {
            this.sm_three = sm_three;
        }

        public String getSm_four() {
            return sm_four;
        }

        public void setSm_four(String sm_four) {
            this.sm_four = sm_four;
        }

        public String getWebsite_Url() {
            return website_Url;
        }

        public void setWebsite_Url(String website_Url) {
            this.website_Url = website_Url;
        }

        @Override
        public String toString() {
            return "SocialMediaLink{" +
                    "sm_one='" + sm_one + '\'' +
                    ", sm_two='" + sm_two + '\'' +
                    ", sm_three='" + sm_three + '\'' +
                    ", sm_four='" + sm_four + '\'' +
                    ", sm_five='" + sm_five + '\'' +
                    ", website_Url='" + website_Url + '\'' +
                    '}';
        }
    }


    public static class WorkPhoto {

    }

    public static class BankDetails {
        String transaction_account_id;
        String bank_name;
        String bank_account_holder_name;
        String bank_account_holder_last_name;
        String bank_account_number;
        String bank_routing_number;
        String bank_country;
        String bank_currency;
        String bank_swift;
        String bank_iban;
        String paypal_id;
        String paypal_currency;


        public String getBank_account_holder_last_name() {
            return bank_account_holder_last_name;
        }

        public void setBank_account_holder_last_name(String bank_account_holder_last_name) {
            this.bank_account_holder_last_name = bank_account_holder_last_name;
        }

        public String getTransaction_account_id() {
            return transaction_account_id;
        }

        public void setTransaction_account_id(String transaction_account_id) {
            this.transaction_account_id = transaction_account_id;
        }

        public String getBank_name() {
            return bank_name;
        }

        public void setBank_name(String bank_name) {
            this.bank_name = bank_name;
        }

        public String getBank_account_holder_name() {
            return bank_account_holder_name;
        }

        public void setBank_account_holder_name(String bank_account_holder_name) {
            this.bank_account_holder_name = bank_account_holder_name;
        }

        public String getBank_account_number() {
            return bank_account_number;
        }

        public void setBank_account_number(String bank_account_number) {
            this.bank_account_number = bank_account_number;
        }

        public String getBank_routing_number() {
            return bank_routing_number;
        }

        public void setBank_routing_number(String bank_routing_number) {
            this.bank_routing_number = bank_routing_number;
        }

        public String getBank_country() {
            return bank_country;
        }

        public void setBank_country(String bank_country) {
            this.bank_country = bank_country;
        }

        public String getBank_currency() {
            return bank_currency;
        }

        public void setBank_currency(String bank_currency) {
            this.bank_currency = bank_currency;
        }

        public String getBank_swift() {
            return bank_swift;
        }

        public void setBank_swift(String bank_swift) {
            this.bank_swift = bank_swift;
        }

        public String getBank_iban() {
            return bank_iban;
        }

        public void setBank_iban(String bank_iban) {
            this.bank_iban = bank_iban;
        }

        public String getPaypal_id() {
            return paypal_id;
        }

        public void setPaypal_id(String paypal_id) {
            this.paypal_id = paypal_id;
        }

        public String getPaypal_currency() {
            return paypal_currency;
        }

        public void setPaypal_currency(String paypal_currency) {
            this.paypal_currency = paypal_currency;
        }

        @Override
        public String toString() {
            return "BankDetails{" +
                    "transaction_account_id='" + transaction_account_id + '\'' +
                    ", bank_name='" + bank_name + '\'' +
                    ", bank_account_holder_name='" + bank_account_holder_name + '\'' +
                    ", bank_account_number='" + bank_account_number + '\'' +
                    ", bank_routing_number='" + bank_routing_number + '\'' +
                    ", bank_country='" + bank_country + '\'' +
                    ", bank_currency='" + bank_currency + '\'' +
                    ", bank_swift='" + bank_swift + '\'' +
                    ", bank_iban='" + bank_iban + '\'' +
                    ", paypal_id='" + paypal_id + '\'' +
                    ", paypal_currency='" + paypal_currency + '\'' +
                    '}';
        }
    }

}
