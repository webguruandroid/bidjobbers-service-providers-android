package com.bidjobbers.bidjobberspro.shared.base.fragment;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class BaseFragmentPresenter<V extends BaseFragmentMvpView> implements BaseFragmentMvpPresenter<V> {

    private static final String TAG = "BasePresenter";

    private final DataManager mDataManager;

    private V mMvpView;

    private final SchedulerProvider mSchedulerProvider;
    private final CompositeDisposable mCompositeDisposable;

    @Inject
    public BaseFragmentPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        this.mDataManager = mDataManager;
        this.mSchedulerProvider = mSchedulerProvider;
        this.mCompositeDisposable = mCompositeDisposable;
    }

   /* @Inject
    public BaseFragmentPresenter(DataManager dataManager) {
        this.mDataManager = dataManager;
    }*/

    @Override
    public void onAttach(V mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mMvpView = null;
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    public V getMvpView() {
        return mMvpView;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new BaseFragmentPresenter.MvpViewNotAttachedException();
    }

    public DataManager getDataManager() {
        return mDataManager;
    }


    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call Presenter.onAttach(MvpView) before" +
                    " requesting data to the Presenter");
        }
    }

    public SchedulerProvider getSchedulerProvider() {
        return mSchedulerProvider;
    }



    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

}
