package com.bidjobbers.bidjobberspro.shared.base.fragment;

public interface BaseFragmentMvpPresenter <V extends BaseFragmentMvpView> {

    void onAttach(V mvpView);

    void onDetach();

}
