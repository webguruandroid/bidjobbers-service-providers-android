package com.bidjobbers.bidjobberspro.features.OnTimeScreen.KindOfPropertyWorkOn;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class KindOfProPropertyWorkOnFragmentPresenter<V extends KindOfProPropertyWorkOnFragmentMvpView>
        extends BaseFragmentPresenter<V>
        implements  KindOfProPropertyWorkOnFragmentMvpPresenter<V>
{

    @Inject
    public KindOfProPropertyWorkOnFragmentPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }

    @Override
    public void onGetDataFromSP() {
        Gson gson = new Gson();
        String json = getDataManager().getMyObject();
        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);

        getMvpView().getDataFromSP(obj.kindOfServiceDoProvide.isLawnServiceSelected(),
                obj.kindOfServiceDoProvide.getLawnServiceId(),
                obj.kindOfServiceDoProvide.isIsmowALownSelected(),
                obj.kindOfServiceDoProvide.getMowALownId(),
                obj.kindOfServiceDoProvide.isAreateALawn(),
                obj.kindOfServiceDoProvide.getAreateALawnId(),
                obj.kindOfServiceDoProvide.isIsyardWaste(),
                obj.kindOfServiceDoProvide.getYardWasteId(),
                obj.kindOfServiceDoProvide.isTreeCuttingSelected(),
                obj.kindOfServiceDoProvide.getTreeCuttingId(),
                obj.kindOfServiceDoProvide.isTreeStrubsSelected(),
                obj.kindOfServiceDoProvide.getTreeStrubsId(),
                obj.kindOfServiceDoProvide.isTreeStrumps(),
                obj.kindOfServiceDoProvide.getTreeStrumpsId(),
                obj.kindOfServiceDoProvide.isIsyardWasteTreeCutting(),
                obj.kindOfServiceDoProvide.getYardWasteTreeCuttingId());
    }

    @Override
    public void onGetDataFromApi() {

        Gson gson = new Gson();
        String json = getDataManager().getMyObject();
        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
        getMvpView().setDataFromApi(obj.kindOfServiceDoProvide);
    }
}
