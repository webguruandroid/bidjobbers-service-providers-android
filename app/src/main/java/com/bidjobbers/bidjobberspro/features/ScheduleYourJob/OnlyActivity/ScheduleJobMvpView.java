package com.bidjobbers.bidjobberspro.features.ScheduleYourJob.OnlyActivity;

import com.bidjobbers.bidjobberspro.data.network.models.TimeSlot.TimeSlotResponse;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface ScheduleJobMvpView  extends MvpView {

    void onSuccessfullyGetTimeSlot(TimeSlotResponse.ResponseDataBean timeSlotResponse);
    void onDeactivate(String data);
}
