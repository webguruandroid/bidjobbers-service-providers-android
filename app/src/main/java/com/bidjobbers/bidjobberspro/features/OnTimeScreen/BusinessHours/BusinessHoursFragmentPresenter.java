package com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessHours;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class BusinessHoursFragmentPresenter<V extends BusinessHoursMvpView>
        extends BaseFragmentPresenter<V>
        implements BusinessHoursFragmentMvpPresenter<V> {

    @Inject
    public BusinessHoursFragmentPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }



}
