package com.bidjobbers.bidjobberspro.features.ForgotOtpVeryfy;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface VerifyOtpMvpPresenter <V extends VerifyOtpMvpView> extends MvpPresenter<V>   {

    public void onGetStarted(String emailId, String otp);

    public void actionResentOtp(String email);

}
