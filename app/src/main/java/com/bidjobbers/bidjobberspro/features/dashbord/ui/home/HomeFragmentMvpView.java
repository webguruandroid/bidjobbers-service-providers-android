package com.bidjobbers.bidjobberspro.features.dashbord.ui.home;

import com.bidjobbers.bidjobberspro.data.network.models.NotificationCount.NotificationCountResponse;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.TabBean;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

import java.util.ArrayList;

public interface HomeFragmentMvpView extends BaseFragmentMvpView {
    public void  allTasksName(ArrayList<TabBean> arrayList);
    void onSuccessfullyGetNotificationCount(NotificationCountResponse notificationCountResponse);
}
