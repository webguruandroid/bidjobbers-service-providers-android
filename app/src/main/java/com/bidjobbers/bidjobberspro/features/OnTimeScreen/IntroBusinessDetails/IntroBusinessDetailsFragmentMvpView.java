package com.bidjobbers.bidjobberspro.features.OnTimeScreen.IntroBusinessDetails;

import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface IntroBusinessDetailsFragmentMvpView extends BaseFragmentMvpView {

    void setDataFromApi(DataModelForFirstTimeQusAns.IntroduceYourBusiness introduceYourBusiness);
}
