package com.bidjobbers.bidjobberspro.features.TearmsandCondition;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.Allpages.AllPagesRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Allpages.AllPagesResponse;
import com.bidjobbers.bidjobberspro.features.allreviews.AllReviewsMvpPresenter;
import com.bidjobbers.bidjobberspro.features.allreviews.AllReviewsMvpView;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class TearmsandConditionPresenter <V extends TearmsandConditionMvpView> extends BasePresenter<V> implements TearmsandConditionMvpPresenter<V> {
    @Inject
    public TearmsandConditionPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void getAllPages(String sug) {
        if(getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();

            AllPagesRequest allPagesRequest = new AllPagesRequest(getDataManager().getLocalValue(),sug);

            getCompositeDisposable().add(
                    getDataManager().getAllPages("application/json",allPagesRequest)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(new DisposableSingleObserver<AllPagesResponse>() {
                                @Override
                                public void onSuccess(AllPagesResponse testResponse) {
                                    Log.v("onSuccess", testResponse.getResponseCode() + "");

                                    getMvpView().hideLoading();

                                    getMvpView().allTasksName(testResponse);

                                }

                                @Override
                                public void onError(Throwable e) {

                                    getMvpView().hideLoading();
                                    Log.e("ERROR", "onError: " + e.getMessage());

                                }

                            }));
        }
        else
        {
            getMvpView().showAlert("No internet Connection");
        }

    }
}
