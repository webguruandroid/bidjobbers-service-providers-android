package com.bidjobbers.bidjobberspro.features.DetailsReview;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface ReviewDetailsMvpPresenter <V extends ReviewDetailsMvpView> extends MvpPresenter<V> {
}
