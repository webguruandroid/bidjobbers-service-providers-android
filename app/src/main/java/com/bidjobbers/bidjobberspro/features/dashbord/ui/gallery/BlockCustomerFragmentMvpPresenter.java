package com.bidjobbers.bidjobberspro.features.dashbord.ui.gallery;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface BlockCustomerFragmentMvpPresenter <V extends BlockCustomerFragmentMvpView> extends BaseFragmentMvpPresenter<V> {

    void getBlockCustomer();
    void onUnBlockUser(String customerId);
}
