package com.bidjobbers.bidjobberspro.features.OnTimeScreen.TreeStrubWorkFragment;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TreeStrubWorkFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TreeStrubWorkFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TreeStrubWorkFragment extends BaseFragment implements TreeStrubWorkFragmentMvpView{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    @BindView(R.id.etOne)
    EditText etOne;
    @BindView(R.id.etTwo)
    EditText etTwo;
    @BindView(R.id.etThree)
    EditText etThree;
    @BindView(R.id.tv_one)
    TextView tv_one;
    @BindView(R.id.tv_2to3)
    TextView tv_2to3;
    @BindView(R.id.tv_4to5)
    TextView tv_4to5;
    @BindView(R.id.tv_more_than_5)
    TextView tv_more_than_5;
    @BindView(R.id.tv_heres_why)
    TextView tv_heres_why;

    Dialog dialog;
    String treeOneId,treeTwoId,treeThreeId,treeFourId;

    ArrayList<TreeStrubWorkObject.TreeClass> arrayList=new ArrayList<>();

    @Inject
    TreeStrubWorkFragmentPresenter<TreeStrubWorkFragmentMvpView>presenter;

    public TreeStrubWorkFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static TreeStrubWorkFragment newInstance(String param1, String param2) {
        TreeStrubWorkFragment fragment = new TreeStrubWorkFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            String str = bundle.getString("FROMG");
            Log.e("FROMG", str);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_tree_strub_work, container, false);
        setUnBinder(ButterKnife.bind(this,v));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }
        presenter.onAttach(this);
        presenter.onGetDataFromApi();

        return v;
    }




    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

      /*  oneTreePrice.setText("$");
        Selection.setSelection(oneTreePrice.getText(), oneTreePrice.getText().length());
        oneTreePrice.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("$")){
                    oneTreePrice.setText("$");
                    Selection.setSelection(oneTreePrice.getText(), oneTreePrice.getText().length());
                }

            }
        });

        twoThreeTreePrice.setText("$");
        Selection.setSelection(twoThreeTreePrice.getText(), twoThreeTreePrice.getText().length());
        twoThreeTreePrice.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("$")){
                    twoThreeTreePrice.setText("$");
                    Selection.setSelection(twoThreeTreePrice.getText(), twoThreeTreePrice.getText().length());
                }

            }
        });


        twoFourFivePrice.setText("$");
        Selection.setSelection(twoFourFivePrice.getText(), twoFourFivePrice.getText().length());
        twoFourFivePrice.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("$")){
                    twoFourFivePrice.setText("$");
                    Selection.setSelection(twoFourFivePrice.getText(), twoFourFivePrice.getText().length());
                }

            }
        });


    */
        tv_heres_why.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPriceDialog();
            }
        });

    }


    private void openPriceDialog()
    {
        dialog = new Dialog(getActivity(), R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_price_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView txt=dialog.findViewById(R.id.txt);
        Button btn_ok=dialog.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txt.setText(Html.fromHtml("<ul>\n" +
                    "  <li>"+getResources().getString(R.string.price_two)+"</li>\n" +
                    "  <li>"+getResources().getString(R.string.price_three)+"</li>\n" +
                    "  <li>"+getResources().getString(R.string.price_four)+"</li>\n" +
                    "  <li>"+getResources().getString(R.string.price_five)+"</li>\n" +
                    "</ul>", Html.FROM_HTML_MODE_COMPACT));
        } else {
            txt.setText(Html.fromHtml("<ul>\n" +
                    "  <li>"+getResources().getString(R.string.price_two)+"</li>\n" +
                    "  <li>"+getResources().getString(R.string.price_three)+"</li>\n" +
                    "  <li>"+getResources().getString(R.string.price_four)+"</li>\n" +
                    "  <li>"+getResources().getString(R.string.price_five)+"</li>\n" +
                    "</ul>"));
        }


        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();
    }
    @Override
    public void setDataFromApi(DataModelForFirstTimeQusAns.BusinessPriceLawnSerce businessPriceLawnSerce) {

        if(!businessPriceLawnSerce.getTOneTreePrice().equals("")) {
            etOne.setText(businessPriceLawnSerce.getTOneTreePrice().substring(1));
        }
        else
        {
            etOne.setText("");
        }
        if(!businessPriceLawnSerce.getTtwoTothreePrice().equals("")) {
            etTwo.setText(businessPriceLawnSerce.getTtwoTothreePrice().substring(1));
        }
        else
        {
            etTwo.setText("");
        }
        if(!businessPriceLawnSerce.getTfourTofivePrice().equals("")) {
            etThree.setText(businessPriceLawnSerce.getTfourTofivePrice().substring(1));
        }
        else
        {
            etThree.setText("");
        }
            treeOneId = businessPriceLawnSerce.getTOneTreeId();
            tv_one.setText(businessPriceLawnSerce.getTOneTreeName());
            treeTwoId = businessPriceLawnSerce.getTtwoTothreeId();
            tv_2to3.setText(businessPriceLawnSerce.getTtwoTothreeName());
            treeThreeId = businessPriceLawnSerce.getTfourTofiveId();
            tv_4to5.setText(businessPriceLawnSerce.getTfourTofiveName());
            treeFourId = businessPriceLawnSerce.getTmoreFiveId();
            // Toast.makeText(getActivity(),treeFourId+""+businessPriceLawnSerce.getTmoreFiveName(),Toast.LENGTH_SHORT).show();
            tv_more_than_5.setText(businessPriceLawnSerce.getTmoreFiveName());
        }


    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public TreeStrubWorkObject sendDataTreeStrubWork()
    {

        arrayList=new ArrayList<>();
        TreeStrubWorkObject.TreeClass  treeStrubWorkObjectOne=new TreeStrubWorkObject.TreeClass(treeOneId,"$"+etOne.getText().toString().trim());
        TreeStrubWorkObject.TreeClass  treeStrubWorkObjectTwo=new TreeStrubWorkObject.TreeClass(treeTwoId,"$"+etTwo.getText().toString().trim());
        TreeStrubWorkObject.TreeClass  treeStrubWorkObjectTree=new TreeStrubWorkObject.TreeClass(treeThreeId,"$"+etThree.getText().toString().trim());
        TreeStrubWorkObject.TreeClass  treeStrubWorkObjectFour=new TreeStrubWorkObject.TreeClass(treeFourId,"0");

      //  TreeStrubWorkObject.TreeClass  treeStrubWorkObjectOne=new TreeStrubWorkObject.TreeClass(treeOneId,"0");
      //  TreeStrubWorkObject.TreeClass  treeStrubWorkObjectTwo=new TreeStrubWorkObject.TreeClass(treeTwoId,"0");
       // TreeStrubWorkObject.TreeClass  treeStrubWorkObjectTree=new TreeStrubWorkObject.TreeClass(treeThreeId,"0");
      //  TreeStrubWorkObject.TreeClass  treeStrubWorkObjectFour=new TreeStrubWorkObject.TreeClass(treeFourId,"0");

        arrayList.add(treeStrubWorkObjectOne);
        arrayList.add(treeStrubWorkObjectTwo);
        arrayList.add(treeStrubWorkObjectTree);
        arrayList.add(treeStrubWorkObjectFour);

        TreeStrubWorkObject obj=new TreeStrubWorkObject(arrayList);

           return obj;
    }
}
