package com.bidjobbers.bidjobberspro.features.login;

import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface LoginViewHelper extends MvpView {

  public void loginAction(String Data);

  public void forgotPass();

  public void displayAuth(String auth);

  public void goVerificatioPage(String emailId);

  public void goTaskScreen();

  public void goFirstTimeLoginScreen();

  public void onChkValue(String value);

}
