package com.bidjobbers.bidjobberspro.features.profiledetails;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.Checkpassword.CheckpasswordResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ReviewList.ReviewResponse;
import com.bidjobbers.bidjobberspro.data.network.models.UpdateProfile.UpdateProfileResponse;
import com.bidjobbers.bidjobberspro.data.network.models.servicedetails.ServiceDetailsResponse;
import com.bidjobbers.bidjobberspro.features.BankDetails.BankDetailsActivity;
import com.bidjobbers.bidjobberspro.features.DetailsReview.ReviewDetailsActivity;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.AdvanceBookingTime.AdvanceTimeForCustomerBookingFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.AdvanceBookingTime.AdvanceTimeForCustomerObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessInfo.BusinessInfoFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessInfo.BusinessInfoObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessLocatiion.BusinessLocationFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessLocatiion.BusinessLocationObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.ImageDataFragment.ImagePickerFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.ImageDataFragment.ImagePickerHelper.TakeImageAdapterAC;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.ImageDataFragment.ImagePickerObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.IntroBusinessDetails.IntroBusinessDetailsFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.IntroBusinessDetails.IntroBusinessObject.IntroBusinessObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.IntroductionFragment.IntroFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.KindOfPropertyWorkOn.KindOfProPropertyWorkOnFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.KindOfPropertyWorkOn.kindofPropertyObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.PricesFor.PriceForObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.PricesFor.PricesForFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.SocialArea.SocialFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.SocialArea.SocialFragmentObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TreeStrubWorkFragment.TreeStrubWorkFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TreeStrubWorkFragment.TreeStrubWorkObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TypeOfPropertyWorkOn.TypeOfPropertyObject.TypeOfPropertyObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TypeOfPropertyWorkOn.TypeOfPropertyWorkOnFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.personaldetails.PersonalDetailsFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.personaldetails.PersonalDetailsObject;
import com.bidjobbers.bidjobberspro.features.allreviews.AllReviewsActivity;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.features.profiledetails.ProfileHelper.BusinessHrAdapter;
import com.bidjobbers.bidjobberspro.features.profiledetails.ProfileHelper.BusinesshrBean;
import com.bidjobbers.bidjobberspro.features.profiledetails.ProfileHelper.ReviewAdapter;
import com.bidjobbers.bidjobberspro.features.testfragment.QuestionImageGalaryFragment;
import com.bidjobbers.bidjobberspro.features.testfragment.QuestionImageGalaryObject;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.flexbox.FlexboxLayout;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.robertlevonyan.views.chip.Chip;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ProfileActivity extends BaseActivity implements ProfileMvpView {

    @BindView(R.id.allTags)
    FlexboxLayout allTags;
    @BindView(R.id.rv_work_photo)
    RecyclerView rv_work_photo;
    @BindView(R.id.rv_reviews)
    RecyclerView rv_reviews;
    @BindView(R.id.scrl)
    NestedScrollView scrl;
    @BindView(R.id.iv_edit_prof)
    ImageView iv_edit_prof;
    @BindView(R.id.fragment_containerholder)
    LinearLayout fragment_containerholder;
    @BindView(R.id.btn_bck)
    LinearLayout btn_bck;
    @BindView(R.id.btn_submit)
    LinearLayout btn_submit;
    @BindView(R.id.btn_all_review)
    LinearLayout btn_all_review;
    @BindView(R.id.openKindOfPropertyWorkOn)
    ImageView openKindOfPropertyWorkOn;
    @BindView(R.id.iv_open_image_fragment)
    ImageView iv_open_image_fragment;
    @BindView(R.id.iv_buseness_info_fragment)
    ImageView iv_buseness_info_fragment;
    @BindView(R.id.iv_open_buseness_location_fragment)
    ImageView iv_open_buseness_location_fragment;
    @BindView(R.id.iv_introbusinessdetailsfragment_open)
    ImageView iv_introbusinessdetailsfragment_open;
    @BindView(R.id.iv_open_buseness_hours_fragment)
    ImageView iv_open_buseness_hours_fragment;
    @BindView(R.id.iv_advance_booking_time_fragment)
    ImageView iv_advance_booking_time_fragment;
    @BindView(R.id.iv_type_of_property_work_for_fragment)
    ImageView iv_type_of_property_work_for_fragment;
    @BindView(R.id.iv_qst_image_galary_fragment)
    ImageView iv_qst_image_galary_fragment;
    final int PRC_DETAILS = 1, TAGS_DETAILS = 5, MY_WORK_PHOTO = 6, BUSINESS_INFO = 7;
    @BindView(R.id.iv_mnu)
    ImageView iv_mnu;
    @BindView(R.id.iv_edit_social)
    ImageView iv_edit_social;
    @BindView(R.id.iv_prc)
    ImageView iv_prc;
    @BindView(R.id.tv_submit)
    TextView tv_submit;

    @BindView(R.id.tvSPName)
    TextView tvSPName;
    @BindView(R.id.tvSPEmail)
    TextView tvSPEmail;
    @BindView(R.id.tvSPPhone)
    TextView tvSPPhone;
    @BindView(R.id.tvSPRating)
    TextView tvSPRating;
    @BindView(R.id.ll_fetch_location)
    LinearLayout ll_fetch_location;

    @BindView(R.id.tvYearFound)
    TextView tvYearFound;
    @BindView(R.id.tvNoofEmp)
    TextView tvNoofEmp;
    @BindView(R.id.tvRegistrationNo)
    TextView tvRegistrationNo;
    @BindView(R.id.tvNameOfCompany)
    TextView tvNameOfCompany;

    @BindView(R.id.tvBusinessAddress)
    TextView tvBusinessAddress;
    @BindView(R.id.tvBusinessPincode)
    TextView tvBusinessPincode;
    final int SOCIAL_WEB = 0, BUSINESS_LOC = 8, BUSINESS_DESC = 9, BUSINESS_HOURS = 10;
    final int BUSINESS_ADVANCE = 11, BUSINESS_OFFER_FOR = 2, PRICE_LAWN_SERVICE = 3, PRICE_TREE_CUTTING = 4;
    private final ArrayList<BusinesshrBean> mValues = new ArrayList<>();
    @BindView(R.id.iv_cng_tree)
    ImageView iv_cng_tree;
    @BindView(R.id.tvSMWeburl)
    TextView tvSMWeburl;
    @BindView(R.id.tvSMFour)
    TextView tvSMFour;

    @BindView(R.id.rvBusinessHr)
    RecyclerView rvBusinessHr;
    @BindView(R.id.tvAdvanceBooking)
    TextView tvAdvanceBooking;
    @BindView(R.id.tvNoticeBefore)
    TextView tvNoticeBefore;
    @BindView(R.id.tvResiCommercial)
    TextView tvResiCommercial;
    @BindView(R.id.tvHavingLicence)
    TextView tvHavingLicence;
    @BindView(R.id.tvLicenceNo)
    TextView tvLicenceNo;
    @BindView(R.id.imageOne)
    ImageView imageOne;
    @BindView(R.id.imageTwo)
    ImageView imageTwo;
    @BindView(R.id.imageFour)
    ImageView imageFour;
    @BindView(R.id.imageFive)
    ImageView imageFive;
    @BindView(R.id.imageThree)
    ImageView imageThree;
    @BindView(R.id.tvSMFive)
    TextView tvSMFive;
    @BindView(R.id.licenceText)
    TextView licenceText;


    @Inject
    BusinessHrAdapter businessHrAdapter;

    @Inject
    ReviewAdapter reviewAdapter;

    @Inject
    TakeImageAdapterAC adapter;
    @BindView(R.id.tvSMThree)
    TextView tvSMThree;


    TextView tv_add;
    LinearLayout ll_search_address;
    LinearLayout ll_type_address;
    ImageView iv_gal_img;

    ArrayList<String> arrayListPhoto = new ArrayList<String>();

    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    BaseFragment previousFragments;

    Context mContext;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE_DEST = 601;
    @BindView(R.id.tvSMTwo)
    TextView tvSMTwo;
    GetLocation mGetLocation;

    @Inject
    ProfilePresenter<ProfileMvpView> profilePresenter;
    @Inject
    KindOfProPropertyWorkOnFragment kindOfProPropertyWorkOnFragment;
    @Inject
    PersonalDetailsFragment personalDetailsFragment;
    @Inject
    PricesForFragment pricesForFragment;
    @Inject
    TreeStrubWorkFragment treeStrubWorkFragment;
    @Inject
    TypeOfPropertyWorkOnFragment typeOfPropertyWorkOnFragment;
    @Inject
    AdvanceTimeForCustomerBookingFragment advanceTimeForCustomerBookingFragment;
    @Inject
    BusinessInfoFragment businessInfoFragment;
    @Inject
    BusinessLocationFragment businessLocationFragment;
    @Inject
    IntroFragment introFragment;
    @Inject
    QuestionImageGalaryFragment questionImageGalaryFragment;
    @Inject
    IntroBusinessDetailsFragment introBusinessDetailsFragment;
    @Inject
    SocialFragment socialFragment;
    @Inject
    ImagePickerFragment imagePickerFragment;

    SharedPreferences mPrefs;
    @BindView(R.id.tvSMOne)
    TextView tvSMOne;
    @BindView(R.id.tvBusinessDescription)
    TextView tvBusinessDescription;
    String haveLicence;

    @BindView(R.id.tvVeryLarge)
    TextView tvVeryLarge;
    @BindView(R.id.tvLargePrice)
    TextView tvLargePrice;
    @BindView(R.id.tvMediumPrice)
    TextView tvMediumPrice;
    @BindView(R.id.tvSmallPrice)
    TextView tvSmallPrice;
    @BindView(R.id.FiveTreePrice)
    TextView FiveTreePrice;

    @BindView(R.id.OneTreePrice)
    TextView OneTreePrice;
    @BindView(R.id.TwoTreePrice)
    TextView TwoTreePrice;
    @BindView(R.id.FourTreePrice)
    TextView FourTreePrice;
    @BindView(R.id.tvBusinessCity)
    TextView tvBusinessCity;
    @BindView(R.id.tvBusinessApartment)
    TextView tvBusinessApartment;
    @BindView(R.id.tvBusinessState)
    TextView tvBusinessState;
    @BindView(R.id.tvBusinessCountry)
    TextView tvBusinessCountry;
    @BindView(R.id.ll_price_for_tree)
    LinearLayout ll_price_for_tree;
    @BindView(R.id.ll_price_for_lawn)
    LinearLayout ll_price_for_lawn;
    ArrayList<ServiceDetailsResponse.ResponseDataBean.BuisnessHoursDetailsBean.BusinessHrDataBean> mValuesHr = new ArrayList<>();
    Dialog dialog, dialogWhereDataTake, dialogTypeAddress;
    SharedPreferences mBank;
    String ImageOne, ImageTwo, ImageThree, ImageFour, ImageFive;
    @BindView(R.id.tv_paypal_currency)
    TextView tv_paypal_currency;
    @BindView(R.id.tv_paypal_id)
    TextView tv_paypal_id;
    @BindView(R.id.tv_bank_swift_number)
    TextView tv_bank_swift_number;
    @BindView(R.id.tv_bank_routing_number)
    TextView tv_bank_routing_number;
    @BindView(R.id.tv_bank_name)
    TextView tv_bank_name;
    @BindView(R.id.tv_bank_iban)
    TextView tv_bank_iban;
    @BindView(R.id.tv_bank_currency)
    TextView tv_bank_currency;
    @BindView(R.id.tv_bank_country)
    TextView tv_bank_country;
    @BindView(R.id.tv_bank_account_number)
    TextView tv_bank_account_number;
    @BindView(R.id.tv_bank_holder_name)
    TextView tv_bank_holder_name;
    @BindView(R.id.iv_edit_bank)
    ImageView iv_edit_bank;
    @BindView(R.id.ll_bank_deactive)
    LinearLayout ll_bank_deactive;

    int currentScreen = 888;
    @BindView(R.id.ll_bank_active)
    LinearLayout ll_bank_active;
    @BindView(R.id.tv_bank_holder_name_last)
            TextView tv_bank_holder_name_last;
    ArrayList<Integer> propertyId = new ArrayList<>();
    MultipartBody.Part[] workImage = new MultipartBody.Part[5];

    BusinesshrBean businesshrBeanOne, businesshrBeanTwo, businesshrBeanThree, businesshrBeanFour, businesshrBeanFive, businesshrBeanSix, businesshrBeanSeven;

    String isBankfillup="";

    public static String insertPeriodically(
            String text, String insert, int period)
    {
        StringBuilder builder = new StringBuilder(
                text.length() + insert.length() * (text.length()/period)+1);

        int index = 0;
        String prefix = "";
        while (index < text.length())
        {
            // Don't put the insert in the very first iteration.
            // This is easier than appending it *after* each substring
            builder.append(prefix);
            prefix = insert;
            builder.append(text.substring(index,
                    Math.min(index + period, text.length())));
            index += period;
        }
        return builder.toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        profilePresenter.onAttach(this);

        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        mBank = getSharedPreferences("Bidjobbers_Bank", Context.MODE_PRIVATE);
        mContext = this;
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        profilePresenter.doGetServiceDetails("en");
        profilePresenter.onAllDialogLoad();
        //  profilePresenter.onLoadTakeImageAdapter();

        profilePresenter.getReview();


        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getResources().getString(R.string.google_maps_key));
        }

        workImage[0] = null;
        workImage[1] = null;
        workImage[2] = null;
        workImage[3] = null;
        workImage[4] = null;


        rv_reviews.setLayoutManager(new LinearLayoutManager(this));
        rv_reviews.setAdapter(reviewAdapter);
        reviewAdapter.setAdapterListner(new ReviewAdapter.ReviewListner() {
            @Override
            public void onItemClick(String Image, String Desc, String Name, String Date, String Time, String Rating) {
                Intent i = new Intent(getApplicationContext(), ReviewDetailsActivity.class);
                i.putExtra("image", Image);
                i.putExtra("desc", Desc);
                i.putExtra("name", Name);
                i.putExtra("date", Date);
                i.putExtra("time", Time);
                i.putExtra("rating", Rating);
                startActivity(i);
            }
        });

        LinearLayoutManager layoutManagerMakeOverBrand = new LinearLayoutManager(this);
        layoutManagerMakeOverBrand.setOrientation(LinearLayoutManager.HORIZONTAL);
        rv_work_photo.setLayoutManager(layoutManagerMakeOverBrand);

        rv_work_photo.setAdapter(adapter);


        adapter.setAdapterListener(new TakeImageAdapterAC.CellListener() {
            @Override
            public void onItemClick(int pos, ArrayList<String> items) {
                String str = "";
                if (pos == 0) {
                    str = items.get(0);
                }
                if (pos == 1) {
                    str = items.get(1);
                }
                if (pos == 2) {
                    str = items.get(2);
                }
                if (pos == 3) {
                    str = items.get(3);
                }
                if (pos == 4) {
                    str = items.get(4);
                }

                Glide.with(mContext).
                        load(str)
                        //.placeholder(R.drawable.image_placeholder)
                        .into(iv_gal_img);
                dialog.show();
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (scrl.getVisibility() == View.VISIBLE) {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);

        } else {
            scrl.setVisibility(View.VISIBLE);
            fragment_containerholder.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case PLACE_AUTOCOMPLETE_REQUEST_CODE_DEST:
                try {

                    if (resultCode == RESULT_OK) {
                        Place sourceplace = Autocomplete.getPlaceFromIntent(data);
                        String source = sourceplace.getName().toString();
                        Toast.makeText(mContext, "Location:" + source, Toast.LENGTH_SHORT).show();
                        mGetLocation.getLocation(source);

                    } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                        Toast.makeText(mContext, "Error:" + "Happen", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
        }


    }

    @Override
    public void onDeactivate(String data) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(ProfileActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, 800);
    }

    @Override
    public void successfullyGetServiceDetails(ServiceDetailsResponse serviceDetailsResponse) {

        Log.e("firsttime", "Enter");
        mValuesHr.clear();
        arrayListPhoto.clear();

        Gson gson = new Gson();
        String json = mPrefs.getString("MyObject", "");
        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
        profilePresenter.onAllTagLoad();
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.ic_user);
        requestOptions.error(R.drawable.ic_user);
        Glide.with(this).load(serviceDetailsResponse.getResponseData().getServiceProviderDetails().getImage())
                .apply(requestOptions)
                .into(iv_prc);

        isBankfillup=serviceDetailsResponse.getResponseData().getIsTransAccountDetailsAvailable();

        tvSPName.setText(serviceDetailsResponse.getResponseData().getServiceProviderDetails().getName());
        tvSPEmail.setText(serviceDetailsResponse.getResponseData().getServiceProviderDetails().getEmail());
        tvSPPhone.setText(serviceDetailsResponse.getResponseData().getServiceProviderDetails().getPhone());
        tvSPRating.setText(serviceDetailsResponse.getResponseData().getServiceProviderDetails().getRating());


        tvNameOfCompany.setText(obj.businessInfo.getCompanyName());
        tvRegistrationNo.setText(obj.businessInfo.getCompRegisterNumber());
        tvNoofEmp.setText(obj.businessInfo.getNoOfEmployee());
        tvYearFound.setText(obj.businessInfo.getYearFounded());


        tvBusinessAddress.setText(serviceDetailsResponse.getResponseData().getBuisnessLocation().getBusiness_address());
        tvBusinessCity.setText(serviceDetailsResponse.getResponseData().getBuisnessLocation().getCity());
        tvBusinessApartment.setText(serviceDetailsResponse.getResponseData().getBuisnessLocation().getApartment());
        tvBusinessState.setText(serviceDetailsResponse.getResponseData().getBuisnessLocation().getState());
        tvBusinessCountry.setText(serviceDetailsResponse.getResponseData().getBuisnessLocation().getCountry());
        tvBusinessPincode.setText(serviceDetailsResponse.getResponseData().getBuisnessLocation().getZipcode());


        if (serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().size() == 1) {
            if (!serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(0).equals("")) {
                tvSMOne.setVisibility(View.VISIBLE);
                tvSMOne.setText(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(0));
                tvSMOne.setPaintFlags(tvSMOne.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            } else {
                tvSMOne.setVisibility(View.GONE);
            }
        }
        if (serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().size() == 2) {

            if (!serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(0).equals("")) {
                tvSMOne.setVisibility(View.VISIBLE);
                tvSMOne.setText(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(0));
                tvSMOne.setPaintFlags(tvSMOne.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            } else {
                tvSMOne.setVisibility(View.GONE);
            }
            if (!serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(1).equals("") || serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(1) != null) {
                tvSMTwo.setVisibility(View.VISIBLE);
                tvSMTwo.setText(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(1));
                tvSMTwo.setPaintFlags(tvSMTwo.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            } else {
                tvSMTwo.setVisibility(View.GONE);
            }
        }

        if (serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().size() == 3) {

            tvSMOne.setVisibility(View.VISIBLE);
            tvSMOne.setText(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(0));
            tvSMOne.setPaintFlags(tvSMOne.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            tvSMTwo.setVisibility(View.VISIBLE);
            tvSMTwo.setText(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(1));
            tvSMTwo.setPaintFlags(tvSMTwo.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            tvSMThree.setVisibility(View.VISIBLE);
            tvSMThree.setText(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(2));
            tvSMThree.setPaintFlags(tvSMThree.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }

        if (serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().size() == 4) {

            tvSMOne.setVisibility(View.VISIBLE);
            tvSMOne.setText(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(0));
            tvSMOne.setPaintFlags(tvSMOne.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            tvSMTwo.setVisibility(View.VISIBLE);
            tvSMTwo.setText(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(1));
            tvSMTwo.setPaintFlags(tvSMTwo.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            tvSMThree.setVisibility(View.VISIBLE);
            tvSMThree.setText(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(2));
            tvSMThree.setPaintFlags(tvSMThree.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            tvSMFour.setVisibility(View.VISIBLE);
            tvSMFour.setText(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(3));
            tvSMFour.setPaintFlags(tvSMFour.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }
        if (serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().size() == 5) {

            if (serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(0) != null) {
                tvSMOne.setVisibility(View.VISIBLE);
                tvSMOne.setText(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(0));
                tvSMOne.setPaintFlags(tvSMOne.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            } else {
                tvSMOne.setVisibility(View.GONE);
            }
            if (serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(1) != null) {
                tvSMTwo.setVisibility(View.VISIBLE);
                tvSMTwo.setText(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(1));
                tvSMTwo.setPaintFlags(tvSMTwo.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            } else {
                tvSMTwo.setVisibility(View.GONE);
            }
            if (serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(2) != null) {
                tvSMThree.setVisibility(View.VISIBLE);
                tvSMThree.setText(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(2));
                tvSMThree.setPaintFlags(tvSMThree.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            } else {
                tvSMThree.setVisibility(View.GONE);
            }
            if (serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(3) != null) {
                tvSMFour.setVisibility(View.VISIBLE);
                tvSMFour.setText(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(3));
                tvSMFour.setPaintFlags(tvSMFour.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            } else {
                tvSMFour.setVisibility(View.GONE);
            }
            if (serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(4) != null) {
                tvSMFive.setVisibility(View.VISIBLE);
                tvSMFive.setText(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(4));
                tvSMFive.setPaintFlags(tvSMFive.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            } else {
                tvSMFive.setVisibility(View.GONE);
            }
        }

        if (!serviceDetailsResponse.getResponseData().getSocialLink().getWebsite_url().equals("")) {
            tvSMWeburl.setVisibility(View.VISIBLE);
            tvSMWeburl.setText(serviceDetailsResponse.getResponseData().getSocialLink().getWebsite_url());
            tvSMWeburl.setPaintFlags(tvSMWeburl.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }

        tvSMOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tvSMOne.getText().toString().trim()));
                    startActivity(browserIntent);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Url Not valide", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tvSMTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tvSMTwo.getText().toString().trim()));
                    startActivity(browserIntent);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Url Not valide", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tvSMThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tvSMThree.getText().toString().trim()));
                    startActivity(browserIntent);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Url Not valide", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tvSMFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tvSMFour.getText().toString().trim()));
                    startActivity(browserIntent);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Url Not valide", Toast.LENGTH_SHORT).show();
                }
            }
        });

        tvSMFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tvSMFive.getText().toString().trim()));
                    startActivity(browserIntent);

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Url Not valide", Toast.LENGTH_SHORT).show();
                }
            }
        });
        tvSMWeburl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tvSMWeburl.getText().toString().trim()));
                    startActivity(browserIntent);

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Url Not valide", Toast.LENGTH_SHORT).show();
                }
            }
        });


        tvBusinessDescription.setText(serviceDetailsResponse.getResponseData().getBuisnessDesc().getBusiness_intro_desc());


        rvBusinessHr.setLayoutManager(new LinearLayoutManager(this));
        rvBusinessHr.setAdapter(businessHrAdapter);
        Log.e("Profile", obj.businessHours.toString());

        if (obj.businessHours.isMondaySelected()) {
            businesshrBeanOne = new BusinesshrBean();
            businesshrBeanOne.setDayName(obj.businessHours.getMondayName());
            businesshrBeanOne.setStartTime(obj.businessHours.getMondayFromTime());
            businesshrBeanOne.setEndTime(obj.businessHours.getMondayToTime());
        }
        if (obj.businessHours.isTuesdaySelected()) {
            businesshrBeanTwo = new BusinesshrBean();
            businesshrBeanTwo.setDayName(obj.businessHours.getTuesdayName());
            businesshrBeanTwo.setStartTime(obj.businessHours.getTuesdayFromTime());
            businesshrBeanTwo.setEndTime(obj.businessHours.getTuesdayToTime());

        }
        if (obj.businessHours.isWednesdaySelected()) {
            businesshrBeanThree = new BusinesshrBean();
            businesshrBeanThree.setDayName(obj.businessHours.getWednesdayName());
            businesshrBeanThree.setStartTime(obj.businessHours.getWednesdayFromTime());
            businesshrBeanThree.setEndTime(obj.businessHours.getWednesdayToTime());

        }
        if (obj.businessHours.isThursdaySelected()) {
            businesshrBeanFour = new BusinesshrBean();
            businesshrBeanFour.setDayName(obj.businessHours.getThrusdayName());
            businesshrBeanFour.setStartTime(obj.businessHours.getThursdayFromTime());
            businesshrBeanFour.setEndTime(obj.businessHours.getThrusdayToTime());

        }
        if (obj.businessHours.isFridaySelected()) {

            businesshrBeanFive = new BusinesshrBean();
            businesshrBeanFive.setDayName(obj.businessHours.getFridayName());
            businesshrBeanFive.setStartTime(obj.businessHours.getFridayFromTime());
            businesshrBeanFive.setEndTime(obj.businessHours.getFridayToTime());

        }
        if (obj.businessHours.isSaturdaySelected()) {

            businesshrBeanSix = new BusinesshrBean();
            businesshrBeanSix.setDayName(obj.businessHours.getSaturdayName());
            businesshrBeanSix.setStartTime(obj.businessHours.getSaturdayFromTime());
            businesshrBeanSix.setEndTime(obj.businessHours.getSaturdayToTime());

        }
        if (obj.businessHours.isSundaySelected()) {

            businesshrBeanSeven = new BusinesshrBean();
            businesshrBeanSeven.setDayName(obj.businessHours.getSundayName());
            businesshrBeanSeven.setStartTime(obj.businessHours.getSundayFromTime());
            businesshrBeanSeven.setEndTime(obj.businessHours.getSundayToTime());

        }

        mValues.add(businesshrBeanOne);
        mValues.add(businesshrBeanTwo);
        mValues.add(businesshrBeanThree);
        mValues.add(businesshrBeanFour);
        mValues.add(businesshrBeanFive);
        mValues.add(businesshrBeanSix);
        mValues.add(businesshrBeanSeven);


        tvNoticeBefore.setText(obj.advanceTimeToBookAJob.getFieldNoticeValue() + " " + obj.advanceTimeToBookAJob.getFieldNoticeName());
        tvAdvanceBooking.setText(obj.advanceTimeToBookAJob.getFieldAdvanceValue() + " " + obj.advanceTimeToBookAJob.getFieldAdvanceName());


        Log.e("Typeofproperty", obj.typeOfPropertyWorkOn.toString());
        if (serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getPropertyType().get(0).getIs_selected().equals("1") && serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getPropertyType().get(1).getIs_selected().equals("1")) {
            tvResiCommercial.setText(serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getPropertyType().get(0).getName() + " && " + serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getPropertyType().get(1).getName());
        } else if (serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getPropertyType().get(0).getIs_selected().equals("1")) {
            tvResiCommercial.setText(serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getPropertyType().get(0).getName());
        } else if (serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getPropertyType().get(1).getIs_selected().equals("1")) {
            tvResiCommercial.setText(serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getPropertyType().get(1).getName());
        } else {
            tvResiCommercial.setText("");
        }

        if (serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getIs_having_licence().equals("1")) {
            tvHavingLicence.setText("yes");
            tvLicenceNo.setVisibility(View.VISIBLE);
            licenceText.setVisibility(View.VISIBLE);
            tvLicenceNo.setText(serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getLicence_number());
        } else {
            tvHavingLicence.setText("no");
            tvLicenceNo.setVisibility(View.GONE);
            licenceText.setVisibility(View.GONE);
            tvLicenceNo.setText(serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getLicence_number());
        }

        for (int i = 0; i < serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().size(); i++) {
            ServiceDetailsResponse.ResponseDataBean.BuisnessHoursDetailsBean.BusinessHrDataBean mData = serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(i);
            if (mData.getIsSelected().equals("1")) {
                mValuesHr.add(mData);
            }
        }


        //  businessHrAdapter.loadList(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData());

        businessHrAdapter.loadList(mValuesHr);


        for (int i = 0; i < serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().size(); i++) {
            arrayListPhoto.add(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage_url() + "/" + serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().get(i));
        }
//        ImageOne=obj.photoDescribeProject.getImageUrl()+"/"+obj.photoDescribeProject.getImageOne();
//
//        ImageTwo=obj.photoDescribeProject.getImageUrl()+"/"+obj.photoDescribeProject.getImageTwo();
//
//        ImageThree=obj.photoDescribeProject.getImageUrl()+"/"+obj.photoDescribeProject.getImageThree();
//
//        ImageFour=obj.photoDescribeProject.getImageUrl()+"/"+obj.photoDescribeProject.getImageFour();
//
//        ImageFive=obj.photoDescribeProject.getImageUrl()+"/"+obj.photoDescribeProject.getImageFive();
//
//
//        arrayListPhoto.add(ImageOne);
//        arrayListPhoto.add(ImageTwo);
//        arrayListPhoto.add(ImageThree);
//        arrayListPhoto.add(ImageFour);
//        arrayListPhoto.add(ImageFive);
//        Log.e("firsttime", arrayListPhoto.get(0)+"-"+ arrayListPhoto.get(1)+"-"+ arrayListPhoto.get(2));


        adapter.loadList(arrayListPhoto);

        tvSmallPrice.setText(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(0).getPrice());
        tvMediumPrice.setText(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(1).getPrice());
        tvLargePrice.setText(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(2).getPrice());
        tvVeryLarge.setText(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(3).getPrice());

        OneTreePrice.setText(serviceDetailsResponse.getResponseData().getTreeServicesDetails().get(0).getPrice());
        TwoTreePrice.setText(serviceDetailsResponse.getResponseData().getTreeServicesDetails().get(1).getPrice());
        FourTreePrice.setText(serviceDetailsResponse.getResponseData().getTreeServicesDetails().get(2).getPrice());
        if (!serviceDetailsResponse.getResponseData().getTreeServicesDetails().get(3).getPrice().equals("")) {
            FiveTreePrice.setText(serviceDetailsResponse.getResponseData().getTreeServicesDetails().get(3).getPrice());
        } else {
            FiveTreePrice.setText(getResources().getString(R.string.addpricelater));
        }


        if (serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getIs_selected().equals("1") && serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getIs_selected().equals("1")) {
            ll_price_for_lawn.setVisibility(View.VISIBLE);
            ll_price_for_tree.setVisibility(View.VISIBLE);
        } else if (serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getIs_selected().equals("1") && serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getIs_selected().equals("0")) {
            ll_price_for_lawn.setVisibility(View.VISIBLE);
            ll_price_for_tree.setVisibility(View.GONE);
        } else if (serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getIs_selected().equals("0") && serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getIs_selected().equals("1")) {
            ll_price_for_lawn.setVisibility(View.GONE);
            ll_price_for_tree.setVisibility(View.VISIBLE);
        }


        //Toast.makeText(getApplicationContext(),a.get(0),Toast.LENGTH_SHORT).show();

        allTags.removeAllViews();

        FlexboxLayout.LayoutParams params
                = new FlexboxLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 10, 10, 10);


        for (int i = 0; i < serviceDetailsResponse.getResponseData().getServiceTypeDetails().size(); i++) {
            if (serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(i).getIs_selected().equals("1")) {
                final Chip chip = new Chip(this);
                chip.setChipText(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(i).getParent_name());

                chip.setTag(0);
                chip.setTextColor(ContextCompat.getColor(this, R.color.green_above_avg));
                chip.changeBackgroundColor(ContextCompat.getColor(this, R.color.white));
                chip.setStrokeSize(2);
                chip.setStrokeColor(ContextCompat.getColor(this, R.color.green_above_avg));

//                chip.setOnChipClickListener(new OnChipClickListener() {
//                    @Override
//                    public void onChipClick(View v) {
//                        String chipId = (String) v.getTag();
//                        String s = null;
//                        String id1 = String.valueOf(chipId);
//                    }
//                });

                allTags.addView(chip, params);
            }
            for (int j = 0; j < serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getChildren().size(); j++) {
                if (serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(i).getChildren().get(j).getIs_selected().equals("1")) {
                    final Chip chip = new Chip(this);
                    chip.setChipText(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(i).getChildren().get(j).getName());
                    chip.setTag(1);
                    chip.setTextColor(ContextCompat.getColor(this, R.color.green_above_avg));
                    chip.changeBackgroundColor(ContextCompat.getColor(this, R.color.white));
                    chip.setStrokeSize(2);
                    chip.setStrokeColor(ContextCompat.getColor(this, R.color.green_above_avg));

//                    chip.setOnChipClickListener(new OnChipClickListener() {
//                        @Override
//                        public void onChipClick(View v) {
//                            String chipId = (String) v.getTag();
//                            String s = null;
//                            String id1 = String.valueOf(chipId);
//                        }
//                    });

                    allTags.addView(chip, params);
                }
            }

        }


        Log.e("DATA", mBank.getString("BankInfo",""));

        if (mBank.getString("BankInfo","").equals("yes")) {
            SharedPreferences.Editor myEdit
                    = mBank.edit();

            myEdit.putString("BankInfo", "yes");
            myEdit.apply();
            myEdit.commit();
            ll_bank_active.setVisibility(View.VISIBLE);
            ll_bank_deactive.setVisibility(View.GONE);

            if (!serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_account_number().equals("")) {
                if(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_account_number().length()>5) {
                    String account_number = serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_account_number();
                    String finalA = "";
                    String account_number_first = account_number.substring(0, account_number.length() - 4);
                    String account_number_last = account_number.substring(account_number.length() - 4, account_number.length());
                    int length = account_number_first.length();
                    for (int i = 0; i < 10; i++) {
                        finalA = finalA + "x";
                    }
                    finalA = finalA + account_number_last;
                    tv_bank_account_number.setText(finalA);
                }
                else
                {
                    tv_bank_account_number.setText(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_account_number());
                }
                tv_bank_country.setText(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_country());
                tv_bank_holder_name_last.setText(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_account_holder_last_name());
                tv_bank_holder_name.setText(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_account_holder_first_name());
                tv_bank_currency.setText(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_currency());

                String ibanLength=serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_iban();
                StringBuilder s;
                s = new StringBuilder(ibanLength);
                for(int i = 4; i < ibanLength.length(); i += 5){
                    s.insert(i, " ");
                }
                String a=ibanLength.replaceAll(" ","");
                tv_bank_iban.setText(insertPeriodically(a," ",4));
                tv_bank_name.setText(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_name());
                tv_bank_swift_number.setText(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_swift());
              //  tv_bank_routing_number.setText(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_routing_number());
            } else {
                tv_bank_account_number.setText(getResources().getString(R.string.nodataavailable));
                tv_bank_country.setText(getResources().getString(R.string.nodataavailable));
                tv_bank_holder_name.setText(getResources().getString(R.string.nodataavailable));
                tv_bank_holder_name_last.setText(getResources().getString(R.string.nodataavailable));
                tv_bank_currency.setText(getResources().getString(R.string.nodataavailable));
                tv_bank_iban.setText(getResources().getString(R.string.nodataavailable));
                tv_bank_name.setText(getResources().getString(R.string.nodataavailable));
                tv_bank_swift_number.setText(getResources().getString(R.string.nodataavailable));
                tv_bank_routing_number.setText(getResources().getString(R.string.nodataavailable));
            }
            if (!serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getPaypal_id().equals("")) {
                if(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getPaypal_id().length()>5) {
                    String account_number = serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getPaypal_id();
                    String finalA = "";
                    String account_number_first = account_number.substring(0, account_number.length() - 4);
                    String account_number_last = account_number.substring(account_number.length() - 4, account_number.length());
                    int length = account_number_first.length();
                    for (int i = 0; i < 10; i++) {
                        finalA = finalA + "x";
                    }
                    finalA = finalA + account_number_last;
                    tv_paypal_id.setText(finalA);
                }
                else
                {
                    tv_paypal_id.setText(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getPaypal_id());
                }
                tv_paypal_currency.setText(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getPaypal_currency());

            } else {
                tv_paypal_currency.setText("No Data Available");
                tv_paypal_id.setText("No Data Available");
            }


        } else {

            SharedPreferences.Editor myEdit
                    = mBank.edit();

            myEdit.putString("BankInfo", "no");
            myEdit.apply();
            myEdit.commit();

            ll_bank_active.setVisibility(View.GONE);
            ll_bank_deactive.setVisibility(View.VISIBLE);

            tv_bank_account_number.setText("No Data Available");
            tv_bank_country.setText("No Data Available");
            tv_bank_holder_name.setText("No Data Available");
            tv_bank_currency.setText("No Data Available");
            tv_bank_iban.setText("No Data Available");
            tv_bank_name.setText("No Data Available");
            tv_bank_swift_number.setText("No Data Available");
            tv_bank_routing_number.setText("No Data Available");
            tv_paypal_currency.setText("No Data Available");
            tv_paypal_id.setText("No Data Available");

        }


    }

    @Override
    public void successfullySubmitPassword(CheckpasswordResponse checkpasswordResponse) {
        if(checkpasswordResponse.getResponseCode()==1) {
            Intent i = new Intent(ProfileActivity.this, BankDetailsActivity.class);
            i.putExtra("Tag", "Profile");
            startActivity(i);
        }
        else
        {
            Toast.makeText(getApplicationContext(),checkpasswordResponse.getResponseText(),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void displayAllReviews() {

        startActivity(new Intent(mContext, AllReviewsActivity.class));
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);

    }

    @Override
    public void backToPriviousScreen() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scrl.setVisibility(View.VISIBLE);
                fragment_containerholder.setVisibility(View.GONE);
            }
        }, 500);
    }

    @Override
    public void btnSubmitAction() {


        int whichEditBthPress = currentScreen;

        switch (whichEditBthPress) {

            case SOCIAL_WEB:
                SocialFragmentObject socialFragmentObject = socialFragment.setDataSocialFragment();
                profilePresenter.submitSocialLink(socialFragmentObject.getSocialList(), socialFragmentObject.getWebUrl());
                break;

            case BUSINESS_OFFER_FOR:
                propertyId = new ArrayList<>();
                TypeOfPropertyObject typeOfPropertyObject = typeOfPropertyWorkOnFragment.sendDataToActivity();


                if (typeOfPropertyObject.isCommertial()) {
                    propertyId.add(Integer.parseInt(typeOfPropertyObject.getCommertialId()));
                }
                if (typeOfPropertyObject.isResidential()) {
                    propertyId.add(Integer.parseInt(typeOfPropertyObject.getResidentialId()));
                }

                Log.e("Firsttime", typeOfPropertyObject.toString());

                if (typeOfPropertyObject.isHasLicence() == true) {
                    haveLicence = "1";
                } else {
                    haveLicence = "0";
                }

                if (typeOfPropertyObject.isCommertial() == false && typeOfPropertyObject.isResidential() == false) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseSelectOnePropertyType), Toast.LENGTH_SHORT).show();
                } else {


                    profilePresenter.submitPropertyTypeData(propertyId, haveLicence, typeOfPropertyObject.getLicenceNo());
                }

                break;

            case BUSINESS_INFO:
                BusinessInfoObject businessInfoObject = businessInfoFragment.setDataBusinessInfo();
                if (businessInfoObject.getCompanyName().equals("")) {
                    Toast.makeText(mContext, getResources().getString(R.string.PleaseEnterCompanyName), Toast.LENGTH_SHORT).show();
                } else if (businessInfoObject.getYearFounded().equals("")) {
                    Toast.makeText(mContext, getString(R.string.PleaseEnterYearofFound), Toast.LENGTH_SHORT).show();
                } else if (businessInfoObject.getNoOfEmployeeId().equals("0")) {
                    Toast.makeText(mContext, getResources().getString(R.string.PleaseSelectNoofEmployee), Toast.LENGTH_SHORT).show();
                } else if (businessInfoObject.getCompRegisterNumber().equals("")) {
                    Toast.makeText(mContext, getResources().getString(R.string.PleaseEnterCompanyRegistrationNumber), Toast.LENGTH_SHORT).show();
                } else {
                    profilePresenter.submitBusinessInfo(businessInfoObject.getCompanyName(), businessInfoObject.getYearFounded(), businessInfoObject.getNoOfEmployeeId(), businessInfoObject.getCompRegisterNumber());
                }

                break;
            case BUSINESS_LOC:

                BusinessLocationObject businessLocationObject = businessLocationFragment.setDataFunctionBusinessLocation();
                if (businessLocationObject.getBusinessAddress().equals("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseEnterBusinessAddress), Toast.LENGTH_SHORT).show();
                } else if (businessLocationObject.getApartment().equals("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseEnterApartmentStreet), Toast.LENGTH_SHORT).show();
                } else if (businessLocationObject.getCity().equals("0")) {

                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseSelectCity), Toast.LENGTH_SHORT).show();
                } else if (businessLocationObject.getCountry().equals("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleasenterCountry), Toast.LENGTH_SHORT).show();
                } else if (businessLocationObject.getPinCode().equals("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseEnterPinCode), Toast.LENGTH_SHORT).show();
                } else {
                    profilePresenter.submitBusinessLocation(businessLocationObject.getBusinessAddress(), businessLocationObject.getApartment(), businessLocationObject.getCity(), businessLocationObject.getState(), businessLocationObject.getCountry(), businessLocationObject.getPinCode(), businessLocationObject.getLat(), businessLocationObject.getLog());
                }
                break;
            case BUSINESS_DESC:
                IntroBusinessObject introBusinessObject = introBusinessDetailsFragment.sendDataToActvity();
                if (introBusinessObject.getDes().equals("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseEnterDescription), Toast.LENGTH_SHORT).show();
                } else {
                    profilePresenter.submitData(introBusinessObject.getDes());
                }
                break;
            case BUSINESS_HOURS:
                QuestionImageGalaryObject questionImageGalaryObject = questionImageGalaryFragment.setDataQuestionImageGalary();

                if (questionImageGalaryObject.getDayId().size() == 0 && questionImageGalaryObject.getStartTimeId().size() == 0 && questionImageGalaryObject.getStartTime().size() == 0 && questionImageGalaryObject.getEndTimeId().size() == 0 && questionImageGalaryObject.getEndTime().size() == 0) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseSelectOneBusinessHourAtleast), Toast.LENGTH_SHORT).show();
                } else {
                    profilePresenter.submitBusinessHours(questionImageGalaryObject.getDayId(), questionImageGalaryObject.getStartTimeId(), questionImageGalaryObject.getStartTime(), questionImageGalaryObject.getEndTimeId(), questionImageGalaryObject.getEndTime());

                }
                break;
            //===========Today=====================
            case PRICE_LAWN_SERVICE:
                PriceForObject object = pricesForFragment.setDataBusinessInfo();

                for (int i = 0; i < object.list.size(); i++) {
                    if (object.list.get(i).getTreePrice().equals("")) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseEnterPrice), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                profilePresenter.submitPriceForTypeData(object.list);

                break;
            case PRICE_TREE_CUTTING:
                TreeStrubWorkObject treeStrubWorkObject = treeStrubWorkFragment.sendDataTreeStrubWork();
                for (int i = 0; i < treeStrubWorkObject.list.size(); i++) {
                    if (treeStrubWorkObject.list.get(i).getPrice().equals("")) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseEnterPrice), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                profilePresenter.submitPriceForTree(treeStrubWorkObject.list);

                break;
            //=====Day After tomorrow=======
            case MY_WORK_PHOTO:
                // Toast.makeText(mContext, "MY_WORK_PHOTO", Toast.LENGTH_SHORT).show();

                ImagePickerObject imagePickerObject = imagePickerFragment.saveImagePicker();
//                if(imagePickerObject.getWork().equals("1"))
//                {
//                    if(imagePickerObject.getIsEmpty().equals("1")) {
//                        Toast.makeText(getApplicationContext(), "Please Select At least One  Image", Toast.LENGTH_SHORT).show();
//                        return;
//                    }
//
////                    if(imagePickerObject.getFileOne()==null&&imagePickerObject.getFileTwo()==null&&imagePickerObject.getFileThree()==null&&imagePickerObject.getFileFour()==null&&imagePickerObject.getFileFive()==null)
////                    {
////                        Toast.makeText(getApplicationContext(),"Please Select All  Image",Toast.LENGTH_SHORT).show();
////                        return;
////                    }
//                }

                if (imagePickerObject.getFileOne() != null) {
                    RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), imagePickerObject.getFileOne());
                    workImage[0] = MultipartBody.Part.createFormData("work_image[0]", imagePickerObject.getFileOne().getName(), reqFile);
                }
                if (imagePickerObject.getFileTwo() != null) {
                    RequestBody reqFileOne = RequestBody.create(MediaType.parse("multipart/form-data"), imagePickerObject.getFileTwo());
                    workImage[1] = MultipartBody.Part.createFormData("work_image[1]", imagePickerObject.getFileTwo().getName(), reqFileOne);
                }
                if (imagePickerObject.getFileThree() != null) {
                    RequestBody reqFileThree = RequestBody.create(MediaType.parse("multipart/form-data"), imagePickerObject.getFileThree());
                    workImage[2] = MultipartBody.Part.createFormData("work_image[2]", imagePickerObject.getFileThree().getName(), reqFileThree);
                }
                if (imagePickerObject.getFileFour() != null) {
                    RequestBody reqFileFour = RequestBody.create(MediaType.parse("multipart/form-data"), imagePickerObject.getFileFour());
                    workImage[3] = MultipartBody.Part.createFormData("work_image[3]", imagePickerObject.getFileFour().getName(), reqFileFour);
                }
                if (imagePickerObject.getFileFive() != null) {
                    RequestBody reqFileFive = RequestBody.create(MediaType.parse("multipart/form-data"), imagePickerObject.getFileFive());
                    workImage[4] = MultipartBody.Part.createFormData("work_image[4]", imagePickerObject.getFileFive().getName(), reqFileFive);
                }

                profilePresenter.submitImageData(workImage, imagePickerObject.getWork());


                break;
            //========Tomorrow=========
            case BUSINESS_ADVANCE:

                // Toast.makeText(mContext, "BUSINESS_ADVANCE", Toast.LENGTH_SHORT).show();
                //  AdvanceTimeForCustomerObject advanceTimeForCustomerObject=advanceTimeForCustomerBookingFragment.setAdvanceBooking();
                //  profilePresenter.submitAdvanceTypeData(advanceTimeForCustomerObject.getAdvanceValueId())

                AdvanceTimeForCustomerObject advanceTimeForCustomerObject = advanceTimeForCustomerBookingFragment.setAdvanceBooking();

                if (advanceTimeForCustomerObject.getAdvanceNameid().equals("0") && advanceTimeForCustomerObject.getAdvanceValueId().equals("0") && advanceTimeForCustomerObject.getNoticeNameId().equals("0") && advanceTimeForCustomerObject.getNoticeValueId().equals("0")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseSelectAllValue), Toast.LENGTH_SHORT).show();
                } else {

                    profilePresenter.submitAdvanceTypeData(advanceTimeForCustomerObject.getAdvanceValueId(), advanceTimeForCustomerObject.getAdvanceValue(), advanceTimeForCustomerObject.getAdvanceNameid(), advanceTimeForCustomerObject.getAdvanceName(), advanceTimeForCustomerObject.getNoticeValueId(), advanceTimeForCustomerObject.getNoticeValue(), advanceTimeForCustomerObject.getNoticeNameId(), advanceTimeForCustomerObject.getNoticeName());
                }
                break;
            case PRC_DETAILS:

                PersonalDetailsObject personalDetailsObject = personalDetailsFragment.sendDataforEdit();
                if (personalDetailsObject.getName().equals("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseEnterName), Toast.LENGTH_SHORT).show();
                } else if (personalDetailsObject.getPhone().equals("")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseEnterPhoneNumber), Toast.LENGTH_SHORT).show();
                } else {
                    profilePresenter.onUpdateProfile(personalDetailsObject.getImg_body(), personalDetailsObject.getName(), personalDetailsObject.getPhone(), "en");
                }
                break;
            case TAGS_DETAILS:
                kindofPropertyObject kindofPropertyObject = kindOfProPropertyWorkOnFragment.setDataFunctionKindofProperty();
                ArrayList<Integer> kindofId = new ArrayList<>();

                Gson gson = new Gson();
                String json = mPrefs.getString("MyObject", "");
                DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
                Log.e("FirstTime", obj.kindOfServiceDoProvide.toString());

                if (kindofPropertyObject.getLawnServiceId().equals("1")) {
                    kindofId.add(Integer.parseInt(obj.kindOfServiceDoProvide.getLawnServiceId()));
                }
                if (kindofPropertyObject.getYardWasteId().equals("1")) {
                    kindofId.add(Integer.parseInt(obj.kindOfServiceDoProvide.getYardWasteId()));
                }
                if (kindofPropertyObject.getMowALownId().equals("1")) {
                    kindofId.add(Integer.parseInt(obj.kindOfServiceDoProvide.getMowALownId()));
                }
                if (kindofPropertyObject.getAreateALawnId().equals("1")) {
                    kindofId.add(Integer.parseInt(obj.kindOfServiceDoProvide.getAreateALawnId()));
                }
                if (kindofPropertyObject.getTreeCuttingId().equals("1")) {
                    kindofId.add(Integer.parseInt(obj.kindOfServiceDoProvide.getTreeCuttingId()));
                }
                if (kindofPropertyObject.getTreeStrubsId().equals("1")) {
                    kindofId.add(Integer.parseInt(obj.kindOfServiceDoProvide.getTreeStrubsId()));
                }
                if (kindofPropertyObject.getTreeStrumpsId().equals("1")) {
                    kindofId.add(Integer.parseInt(obj.kindOfServiceDoProvide.getTreeStrumpsId()));
                }
                if (kindofPropertyObject.getYardWasteTreeCuttingId().equals("1")) {
                    kindofId.add(Integer.parseInt(obj.kindOfServiceDoProvide.getYardWasteTreeCuttingId()));
                }

                Log.e("FirstTime", kindofId.size() + "");


                if (kindofPropertyObject.getLawnServiceId().equals("0") && kindofPropertyObject.getTreeCuttingId().equals("0")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseSelecOneService), Toast.LENGTH_SHORT).show();
                } else if (kindofPropertyObject.getLawnServiceId().equals("1") && kindofPropertyObject.getYardWasteId().equals("0") && kindofPropertyObject.getMowALownId().equals("0") && kindofPropertyObject.getAreateALawnId().equals("0")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseSelectneLawnService), Toast.LENGTH_SHORT).show();
                } else if (kindofPropertyObject.getTreeCuttingId().equals("1") && kindofPropertyObject.getTreeStrubsId().equals("0") && kindofPropertyObject.getTreeStrumpsId().equals("0") && kindofPropertyObject.getYardWasteTreeCuttingId().equals("0")) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseSelectOneTreeCuttingService), Toast.LENGTH_SHORT).show();
                } else {
                    profilePresenter.submitKindOfPropertyData(kindofId);
                }


                break;

            default:
                showSnackBar("Action Not Detected");


        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        profilePresenter.doGetServiceDetails("en");

    }

    @Override
    public void editSocial() {
        scrl.setVisibility(View.GONE);
        fragment_containerholder.setVisibility(View.VISIBLE);
        if (previousFragments == null) {
            //loadFragment(PersonalDetailsFragment.newInstance("",""));
            loadFragment(socialFragment);
        }
    }

    @Override
    public void editProf() {
        scrl.setVisibility(View.GONE);
        fragment_containerholder.setVisibility(View.VISIBLE);
        if (previousFragments == null) {
            loadFragment(personalDetailsFragment);
        }
    }

    @Override
    public void editImageGalaryFragment() {
        scrl.setVisibility(View.GONE);
        fragment_containerholder.setVisibility(View.VISIBLE);
        if (previousFragments == null) {
            loadFragment(pricesForFragment);
        }
    }

    @Override
    public void editOpenKindOfPropertyWorkOn() {
        scrl.setVisibility(View.GONE);
        fragment_containerholder.setVisibility(View.VISIBLE);
        if (previousFragments == null) {
            loadFragment(kindOfProPropertyWorkOnFragment);
        }
    }

    @Override
    public void editTypeOfPropertyWorkFor() {
        scrl.setVisibility(View.GONE);
        fragment_containerholder.setVisibility(View.VISIBLE);
        if (previousFragments == null) {
            loadFragment(typeOfPropertyWorkOnFragment);
        }
    }

    @Override
    public void editCngTree() {
        scrl.setVisibility(View.GONE);
        fragment_containerholder.setVisibility(View.VISIBLE);
        if (previousFragments == null) {
            loadFragment(treeStrubWorkFragment);
        }
    }

    //Problem is here please have look in future
    @Override
    public void editOpenImageFragment() {
        scrl.setVisibility(View.GONE);
        fragment_containerholder.setVisibility(View.VISIBLE);
        if (previousFragments == null) {
            // loadFragment(ImagePickerFragment.newInstance("",""));
            loadFragment(imagePickerFragment);
        }
    }
    //End

    @Override
    public void editAdvanceBookingTime() {
        scrl.setVisibility(View.GONE);
        fragment_containerholder.setVisibility(View.VISIBLE);
        if (previousFragments == null) {
            loadFragment(advanceTimeForCustomerBookingFragment);
        }
    }

    @Override
    public void editBusinessInfo() {
        scrl.setVisibility(View.GONE);
        fragment_containerholder.setVisibility(View.VISIBLE);
        if (previousFragments == null) {
            loadFragment(businessInfoFragment);
        }
    }

    @Override
    public void editOpenBusinessLocation() {
        scrl.setVisibility(View.GONE);
        fragment_containerholder.setVisibility(View.VISIBLE);
        if (previousFragments == null) {
            loadFragment(businessLocationFragment);
        }
    }

    @Override
    public void editBusinessIntroDetails() {
        scrl.setVisibility(View.GONE);
        fragment_containerholder.setVisibility(View.VISIBLE);
        if (previousFragments == null) {
            loadFragment(introBusinessDetailsFragment);
        }
    }

    @Override
    public void editBusinessHours() {
        scrl.setVisibility(View.GONE);
        fragment_containerholder.setVisibility(View.VISIBLE);
        if (previousFragments == null) {
            loadFragment(questionImageGalaryFragment);
        }
    }

    @Override
    public void loadTakeImageAdapter() {


        for (int i = 0; i < arrayListPhoto.size(); i++) {
            Log.e("Workimage", arrayListPhoto.get(i));
        }

        adapter.loadList(arrayListPhoto);
        adapter.setAdapterListener(new TakeImageAdapterAC.CellListener() {
            @Override
            public void onItemClick(int pos, ArrayList<String> items) {
                String str = "";
                if (pos == 0) {
                    str = ImageOne;
                }
                if (pos == 1) {
                    str = ImageTwo;
                }
                if (pos == 2) {
                    str = ImageThree;
                }
                if (pos == 3) {
                    str = ImageFour;
                }
                if (pos == 4) {
                    str = ImageFive;
                }

                Glide.with(mContext).
                        load(str)
                        // .placeholder(R.drawable.image_placeholder)
                        .into(iv_gal_img);
                dialog.show();
            }
        });

        LinearLayoutManager layoutManagerMakeOverBrand = new LinearLayoutManager(this);
        layoutManagerMakeOverBrand.setOrientation(LinearLayoutManager.HORIZONTAL);
        rv_work_photo.setLayoutManager(layoutManagerMakeOverBrand);

        rv_work_photo.setAdapter(adapter);

    }

    @Override
    public void loadReviewAdapter() {


    }

    @Override
    public void allDialogLoad() {
        dialog = new Dialog(mContext, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_zoom_in_popup);

        iv_gal_img = dialog.findViewById(R.id.iv_gal_img);

        dialogTypeAddress = new Dialog(mContext, R.style.Theme_Dialog_popup);
        dialogTypeAddress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTypeAddress.setContentView(R.layout.layout_type_address);

        dialogWhereDataTake = new Dialog(mContext, R.style.Theme_Dialog_popup);
        dialogWhereDataTake.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogWhereDataTake.setContentView(R.layout.layout_select_loc_aria);

        ll_search_address = (LinearLayout) dialogWhereDataTake.findViewById(R.id.ll_search_address);
        ll_search_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogWhereDataTake.dismiss();
                // openAutoLocationFetch();
            }
        });
        ll_type_address = (LinearLayout) dialogWhereDataTake.findViewById(R.id.ll_type_address);
        ll_type_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogWhereDataTake.dismiss();
                dialogTypeAddress.show();
            }
        });
        tv_add = (TextView) dialogTypeAddress.findViewById(R.id.tv_add);
        tv_add.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          dialogTypeAddress.dismiss();
                                      }
                                  }
        );

    }

    @Override
    public void allTagLoad() {

    }

    @Override
    public void successfullyUpdateProfile(UpdateProfileResponse updateProfileResponse) {
        profilePresenter.doGetServiceDetails("en");
    }

    @Override
    public void successfullySaveKindOfPropertyData() {
        profilePresenter.doGetServiceDetails("en");
    }

    @Override
    public void successfullySaveSocialLink() {


        profilePresenter.doGetServiceDetails("en");
        // getSupportFragmentManager().beginTransaction().remove(this).commit();

    }

    @Override
    public void successfullySaveBusinessInfo() {
        profilePresenter.doGetServiceDetails("en");
    }

    @Override
    public void successfullySaveBusinessLocation() {
        profilePresenter.doGetServiceDetails("en");
    }

    @Override
    public void successfullySavaData() {
        profilePresenter.doGetServiceDetails("en");
    }

    @Override
    public void successfullySaveBusinessHour() {
        profilePresenter.doGetServiceDetails("en");
    }

    @Override
    public void successfullySaveAdvanceBooking() {
        profilePresenter.doGetServiceDetails("en");
    }

    @Override
    public void successfullyySavePropertyType() {
        profilePresenter.doGetServiceDetails("en");
    }

    @Override
    public void successfullySavePriceForLawn() {
        profilePresenter.doGetServiceDetails("en");
    }

    @Override
    public void successfullySavePriceForTree() {
        profilePresenter.doGetServiceDetails("en");
    }

    @Override
    public void successfullySaveImage() {
        profilePresenter.doGetServiceDetails("en");
    }

    @Override
    public void successfullyGetReview(ReviewResponse reviewResponse) {
        if (reviewResponse.getResponseCode() == 1) {
            if (reviewResponse.getResponseData().getReviewList().size() > 3) {
                ArrayList<ReviewResponse.ResponseDataBean.ReviewListBean> arrylist = new ArrayList<>();
                arrylist.add(reviewResponse.getResponseData().getReviewList().get(0));
                arrylist.add(reviewResponse.getResponseData().getReviewList().get(1));
                arrylist.add(reviewResponse.getResponseData().getReviewList().get(2));

                reviewAdapter.loadList(arrylist);
                reviewAdapter.notifyDataSetChanged();
                btn_all_review.setVisibility(View.VISIBLE);
            } else {
                reviewAdapter.loadList(reviewResponse.getResponseData().getReviewList());
                reviewAdapter.notifyDataSetChanged();
                btn_all_review.setVisibility(View.GONE);
            }
        } else {
            btn_all_review.setVisibility(View.GONE);
        }
    }

    @OnClick
            ({R.id.iv_edit_social, R.id.iv_edit_prof, R.id.iv_type_of_property_work_for_fragment, R.id.iv_qst_image_galary_fragment, R.id.iv_cng_tree,
                    R.id.openKindOfPropertyWorkOn, R.id.iv_open_image_fragment, R.id.iv_buseness_info_fragment,
                    R.id.iv_open_buseness_location_fragment, R.id.iv_introbusinessdetailsfragment_open, R.id.iv_open_buseness_hours_fragment,
                    R.id.iv_advance_booking_time_fragment, R.id.btn_all_review, R.id.btn_bck, R.id.btn_submit, R.id.iv_mnu,
                    R.id.btn_next, R.id.iv_edit_bank})
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_all_review:
                profilePresenter.onDisplayAllReviews();
                break;
            case R.id.btn_bck:
                profilePresenter.onBackClick();
                break;
            case R.id.btn_next:
                profilePresenter.onBtnSubmitAction();
                break;
            case R.id.iv_edit_social:
                currentScreen = SOCIAL_WEB;
                profilePresenter.onEditSocial();
                break;
            case R.id.iv_edit_prof:
                currentScreen = PRC_DETAILS;
                profilePresenter.onEditProf();
                break;
            case R.id.iv_type_of_property_work_for_fragment:
                currentScreen = BUSINESS_OFFER_FOR;
                profilePresenter.onEditTypeOfPropertyWorkFor();
                break;
            case R.id.iv_qst_image_galary_fragment:
                currentScreen = PRICE_LAWN_SERVICE;
                profilePresenter.onEditImageGalaryFragment();

                break;
            case R.id.iv_cng_tree:
                currentScreen = PRICE_TREE_CUTTING;
                profilePresenter.onEditCngTree();

                break;
            case R.id.openKindOfPropertyWorkOn:
                currentScreen = TAGS_DETAILS;
                profilePresenter.onKindOfProperty();

                break;
            case R.id.iv_open_image_fragment:
                currentScreen = MY_WORK_PHOTO;
                profilePresenter.onEditImageFragment();

                break;
            case R.id.iv_buseness_info_fragment:
                currentScreen = BUSINESS_INFO;
                profilePresenter.onBusinessInfoFragment();

                break;
            case R.id.iv_open_buseness_location_fragment:
                currentScreen = BUSINESS_LOC;
                profilePresenter.onBusinessLocationFragment();

                break;
            case R.id.iv_introbusinessdetailsfragment_open:
                currentScreen = BUSINESS_DESC;
                profilePresenter.onBusinessDetailsFragment();

                break;
            case R.id.iv_open_buseness_hours_fragment:
                currentScreen = BUSINESS_HOURS;
                profilePresenter.onBusinessHoursFragment();

                break;
            case R.id.iv_advance_booking_time_fragment:
                currentScreen = BUSINESS_ADVANCE;
                profilePresenter.onBusinessBookingTimeFragment();
                break;
            case R.id.iv_mnu:
                if (scrl.getVisibility() == View.VISIBLE) {
                    finish();
                    overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                } else {
                    scrl.setVisibility(View.VISIBLE);
                    fragment_containerholder.setVisibility(View.GONE);
                }
                break;
            case R.id.iv_edit_bank:
               // Toast.makeText(getApplicationContext(),isBankfillup,Toast.LENGTH_SHORT).show();
                if(isBankfillup.equals("1"))
                {
                    showDialogPassword(ProfileActivity.this);
                }
                else
                {
                    Intent i = new Intent(ProfileActivity.this, BankDetailsActivity.class);
                    i.putExtra("Tag", "Profile");
                    startActivity(i);
                }

                break;
            default:
                showSnackBar("Under development");
        }
    }

    public void showDialogPassword(Context activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dailog_password);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        EditText et_password=dialog.findViewById(R.id.et_password);
        TextView tv_Cancel=dialog.findViewById(R.id.tv_Cancel);
        TextView tv_Continue=dialog.findViewById(R.id.tv_Continue);

        tv_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        tv_Continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                if(et_password.getText().toString().trim().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"Please Enter Password",Toast.LENGTH_SHORT).show();
                    return;
                }
                else {
                    profilePresenter.onSubmitPassword(et_password.getText().toString().trim());
                }
            }
        });



        dialog.show();
    }

    private void loadFragment(Fragment fragment) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_righ_jitt, R.anim.exit_to_left_jit);
        transaction.replace(R.id.fragment_container, fragment);

        Bundle bundle = new Bundle();

        bundle.putSerializable("FROMG", "2");
        fragment.setArguments(bundle);


        transaction.addToBackStack(null);
        transaction.commit();
        tv_submit.setText("U P D A T E");
    }

    public void dialogOpen() {
        dialogWhereDataTake.show();
    }

    public void openAutoLocationFetch() {
        try {
            /*Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN).build(this);
            startActivityForResult(intent, req_val);*/

            List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);
            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE_DEST);
            //finish();

        } catch (Exception e) {
            Log.e(TAG, "findAddressAuto: " + e.getMessage());
        }
    }

    public void setmGetLocation(GetLocation mGetLocation) {
        this.mGetLocation = mGetLocation;
    }

    public interface GetLocation {
        void getLocation(String loc);
    }


}
