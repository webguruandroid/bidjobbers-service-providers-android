package com.bidjobbers.bidjobberspro.features.dashbord.ui.send;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface SettingsFragmentMvpPresenter <V extends SettingsFragmentMvpView> extends BaseFragmentMvpPresenter<V> {

    void onSuccessfullGetProfile(String language);
    void onLogoutClick();
}
