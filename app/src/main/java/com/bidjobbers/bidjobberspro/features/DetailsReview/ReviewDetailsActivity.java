package com.bidjobbers.bidjobberspro.features.DetailsReview;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;
import com.bumptech.glide.Glide;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ReviewDetailsActivity extends BaseActivity implements ReviewDetailsMvpView, View.OnClickListener {

    @BindView(R.id.iv_mnu)
    ImageView iv_mnu;
    @BindView(R.id.iv_prc)
    CircleImageView  iv_prc;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_date)
    TextView tv_date;
    @BindView(R.id.tv_time)
    TextView tv_time;
    @BindView(R.id.tv_rating)
    TextView tv_rating;
    @BindView(R.id.tv_desc)
    TextView tv_desc;
    @BindView(R.id.tv_heading)
    TextView tv_heading;

    String image,desc,name,date,time,rating;

    @Inject
    ReviewDetailsPresenter<ReviewDetailsMvpView> reviewDetailsPresenter;
    Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_details);
        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        mContext = this;
        reviewDetailsPresenter.onAttach(this);
        image=getIntent().getStringExtra("image");
        desc=getIntent().getStringExtra("desc");
        name=getIntent().getStringExtra("name");
        date=getIntent().getStringExtra("date");
        time=getIntent().getStringExtra("time");
        rating=getIntent().getStringExtra("rating");

        Glide.with(mContext).load(image).into(iv_prc);
        tv_name.setText(name);
        tv_date.setText(date);
        tv_time.setText(time);
        tv_rating.setText(rating);
        tv_desc.setText(desc);
        tv_heading.setText(name+""+getResources().getString(R.string.review));

    }

    @OnClick(R.id.iv_mnu)
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.iv_mnu:
                finish();
                overridePendingTransition( R.anim.slide_from_top,R.anim.slide_in_top);
                break;
        }
    }
}
