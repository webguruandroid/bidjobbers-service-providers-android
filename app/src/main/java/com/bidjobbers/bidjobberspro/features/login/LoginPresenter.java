package com.bidjobbers.bidjobberspro.features.login;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.login.LoginRequest;
import com.bidjobbers.bidjobberspro.data.network.models.login.LoginResponce;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.CommonUtils;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class LoginPresenter <V extends LoginViewHelper>
                    extends BasePresenter<V>
                   implements LoginPresenterHelper<V> {

    @Inject
    public LoginPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void onClickLogin(final String username, String password,String deviceType,String deviceToken)
    {
        if(username.toString().trim().isEmpty())
        {
            if(getDataManager().getLocalValue().trim().equals("fr"))
             getMvpView().showMessage("S'il vous plaît entrer UserName");
            else
             getMvpView().showMessage("Please Enter UserName");

            return;

        }
        if(password.toString().trim().isEmpty())
        {
            if(getDataManager().getLocalValue().trim().equals("fr"))
                getMvpView().showMessage("Veuillez entrer le mot de passe");
            else
                getMvpView().showMessage("Please Enter Password");

            return;
        }
        if(!CommonUtils.isEmailValid(username.toString()))
        {
            if(getDataManager().getLocalValue().trim().equals("fr"))
                getMvpView().showMessage("Email id n'est pas valide");
            else
                getMvpView().showMessage("Email Id is not valid");

            return;
        }

        if (password.length()<6)
        {
            if(getDataManager().getLocalValue().trim().equals("fr"))
                getMvpView().showMessage("La longueur du mot de passe doit être de 6");
            else
                getMvpView().showMessage("Password length should be 6");

            return;
        }

        if(getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            //getMvpView().loginAction(username+password);
            LoginRequest loginRequest = new LoginRequest(username, password, getDataManager().getLocalValue(),deviceType,deviceToken);

            getCompositeDisposable().add(
                    getDataManager().getLoginResponse(loginRequest)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(new DisposableSingleObserver<LoginResponce>() {
                                @Override
                                public void onSuccess(LoginResponce testResponse) {
                                    Log.v("onSuccess", testResponse.getResponseCode() + "");

                                    getMvpView().hideLoading();
                        /* ArrayList<DataBean> dataBeanArrayList = new ArrayList<>();
                        for (int r=0;r<testResponse.getResponseData().getData().size();r++) {
                            DataBean dataBean = new DataBean(testResponse.getResponseData().getData().get(r).getData(),
                                    testResponse.getResponseData().getData().get(r).getData());
                            dataBeanArrayList.add(dataBean);
                        }
                        TestPersistData testPersistData = new TestPersistData(dataBeanArrayList);
                        //getMvpView().onTestResult(testPersistData);
                        getMvpView().loginAction("asdasd"+dataBeanArrayList.size());*/

                                    if (testResponse.getResponseCode() == 1) {


                                        getDataManager().setAccess_token(testResponse.getResponseData().getAuthorization().getAccess_token());
                                        getDataManager().setIsQuestionFillup(testResponse.getResponseData().getHasBusinessDetails());
                                        getDataManager().setIsEmailVarified(testResponse.getResponseData().getIsEmailVerified());

                                      //  getMvpView().onError(testResponse.getResponseData().getHasBusinessDetails()+testResponse.getResponseData().getAuthorization().getAccess_token());
                                        if (testResponse.getResponseData().getIsEmailVerified().trim().equals("0")) {

                               /*
                               Intent intentHome = new Intent(mContext, VerifySignupActivity.class);
                               intentHome.putExtra("emailAddress",input_email.getText().toString().trim());
                               startActivity(intentHome);
                               */
                                            //Api call for resent otp
                                            //And show popup -> Email Verification Pending
                                            //Please verify your email address


                                            getMvpView().goVerificatioPage(username);
                                        } else {
                                            if (testResponse.getResponseData().getHasBusinessDetails().equals("1")) {

                                                getMvpView().goTaskScreen();
                                            } else {
                                                getMvpView().goFirstTimeLoginScreen();

                                            }
                                        }
                                    }else if(testResponse.getResponseCode() == 401)
                                    {
                                        getMvpView().showMessage(testResponse.getResponseText());
                                    }
                                    else {
                                        getMvpView().showMessage(testResponse.getResponseText());
                                    }

                                }

                                @Override
                                public void onError(Throwable e) {

                                    getMvpView().hideLoading();
                                    Log.e("ERROR", "onError: " + e.getMessage());

                                }

                            }));
        }
        else
        {
            getMvpView().showAlert("No internet Connection");
        }

    }




    public void callForResentOTP(String emailid){

    }



    @Override
    public void onForgotPass() {
        getMvpView().forgotPass();
    }

    @Override
    public void chckAuth() {
        getMvpView().displayAuth(getDataManager().getAccess_token());
    }

    @Override
    public void chekValue() {
         getMvpView().onChkValue(getDataManager().getLocalValue());
    }
}
