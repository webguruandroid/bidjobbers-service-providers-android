package com.bidjobbers.bidjobberspro.features.OnTimeScreen.personaldetails;

import okhttp3.MultipartBody;

public class PersonalDetailsObject {
    MultipartBody.Part img_body;
    String name;
    String phone;

    public PersonalDetailsObject(MultipartBody.Part img_body, String name, String phone) {
        this.img_body = img_body;
        this.name = name;
        this.phone = phone;
    }

    public MultipartBody.Part getImg_body() {
        return img_body;
    }

    public void setImg_body(MultipartBody.Part img_body) {
        this.img_body = img_body;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
