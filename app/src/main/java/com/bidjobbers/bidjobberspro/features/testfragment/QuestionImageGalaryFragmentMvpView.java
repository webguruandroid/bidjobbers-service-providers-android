package com.bidjobbers.bidjobberspro.features.testfragment;

import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface QuestionImageGalaryFragmentMvpView extends BaseFragmentMvpView {


    void setDataFromApi(DataModelForFirstTimeQusAns.BusinessHours businessHours);




    public void actionTestPresenter();

    //=======All Apply button action ==================

    public void actionApplyMonday();
    public void actionApplyTuesday();
    public void actionApplyWednesday();
    public void actionApplyThree();
    public void actionApplyFriday();
    public void actionApplySaturday();
    public void actionApplySunday();

    //=======All Edit button===========================

    public void actionEditOne();
    public void actionEditTwo();
    public void actionEditThree();
    public void actionEditFour();
    public void actionEditFive();
    public void actionEditSix();
    public void actionEditSeven();

    //=======All Done button ==========================

    public void actionDoneOne();
    public void actionDoneTwo();
    public void actionDoneThree();
    public void actionDoneFour();
    public void actionDoneFive();
    public void actionDoneSix();
    public void actionDoneSeven();


    //=======All Check box ============================

    public void actionChkMonday(boolean flag);
    public void actionChkTuesday(boolean flag);
    public void actionChkWednesday(boolean flag);
    public void actionChkThusday(boolean flag);
    public void actionChkFriday(boolean flag);
    public void actionChkSaturday(boolean flag);
    public void actionChkSunday(boolean flag);


    //===========Spinner Select Action =================

    public void actionSpin1(int pos);
    public void actionSpinEnd1(int pos);

    public void actionSpin2(int pos);
    public void actionSpinEnd2(int pos);

    public void actionSpin3(int pos);
    public void actionSpinEnd3(int pos);

    public void actionSpin4(int pos);
    public void actionSpinEnd4(int pos);

    public void actionSpin5(int pos);
    public void actionSpinEnd5(int pos);

    public void actionSpin6(int pos);
    public void actionSpinEnd6(int pos);

    public void actionSpin7(int pos);
    public void actionSpinEnd7(int pos);

}
