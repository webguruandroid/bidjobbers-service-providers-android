package com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessInfo;

public class BusinessInfoObject {

    String companyName="";
    String yearFounded="";
    String noOfEmployeeId="";
    String compRegisterNumber="";

    public BusinessInfoObject(String companyName, String yearFounded, String noOfEmployeeId, String compRegisterNumber) {
        this.companyName = companyName;
        this.yearFounded = yearFounded;
        this.noOfEmployeeId = noOfEmployeeId;
        this.compRegisterNumber = compRegisterNumber;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getYearFounded() {
        return yearFounded;
    }

    public void setYearFounded(String yearFounded) {
        this.yearFounded = yearFounded;
    }

    public String getNoOfEmployeeId() {
        return noOfEmployeeId;
    }

    public void setNoOfEmployeeId(String noOfEmployeeId) {
        this.noOfEmployeeId = noOfEmployeeId;
    }

    public String getCompRegisterNumber() {
        return compRegisterNumber;
    }

    public void setCompRegisterNumber(String compRegisterNumber) {
        this.compRegisterNumber = compRegisterNumber;
    }


    @Override
    public String toString() {
        return "BusinessInfoObject{" +
                "companyName='" + companyName + '\'' +
                ", yearFounded='" + yearFounded + '\'' +
                ", noOfEmployeeId='" + noOfEmployeeId + '\'' +
                ", compRegisterNumber='" + compRegisterNumber + '\'' +
                '}';
    }
}
