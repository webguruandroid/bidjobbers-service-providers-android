package com.bidjobbers.bidjobberspro.features.testfragment;

import java.util.ArrayList;

public class QuestionImageGalaryObject {

    ArrayList<String> dayId;
    ArrayList<String> startTimeId;
    ArrayList<String> startTime;
    ArrayList<String> endTimeId;
    ArrayList<String> endTime;

    public QuestionImageGalaryObject(ArrayList<String> dayId, ArrayList<String> startTimeId, ArrayList<String> startTime, ArrayList<String> endTimeId, ArrayList<String> endTime) {
        this.dayId = dayId;
        this.startTimeId = startTimeId;
        this.startTime = startTime;
        this.endTimeId = endTimeId;
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "QuestionImageGalaryObject{" +
                "dayId=" + dayId +
                ", startTimeId=" + startTimeId +
                ", startTime=" + startTime +
                ", endTimeId=" + endTimeId +
                ", endTime=" + endTime +
                '}';
    }

    public ArrayList<String> getDayId() {
        return dayId;
    }

    public void setDayId(ArrayList<String> dayId) {
        this.dayId = dayId;
    }

    public ArrayList<String> getStartTimeId() {
        return startTimeId;
    }

    public void setStartTimeId(ArrayList<String> startTimeId) {
        this.startTimeId = startTimeId;
    }

    public ArrayList<String> getStartTime() {
        return startTime;
    }

    public void setStartTime(ArrayList<String> startTime) {
        this.startTime = startTime;
    }

    public ArrayList<String> getEndTimeId() {
        return endTimeId;
    }

    public void setEndTimeId(ArrayList<String> endTimeId) {
        this.endTimeId = endTimeId;
    }

    public ArrayList<String> getEndTime() {
        return endTime;
    }

    public void setEndTime(ArrayList<String> endTime) {
        this.endTime = endTime;
    }
}
