package com.bidjobbers.bidjobberspro.features.ScheduleYourJob.AvalableDateViewPagerHelper;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.TimeSlot.TimeSlotRequest;
import com.bidjobbers.bidjobberspro.data.network.models.TimeSlot.TimeSlotResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

public class AvalableDateBodyPresenter <V extends AvalableDateBodyFragmentMvpView>
        extends BaseFragmentPresenter<V>
        implements  AvalableDateBodyMvpPresenter<V> {

    @Inject
    public AvalableDateBodyPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }

    @Override
    public void ongetSchedule() {
        TimeSlotRequest timeSlotRequests=new TimeSlotRequest(getDataManager().getLocalValue());

        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().postGetTimeSlot( "Bearer "+getDataManager().getAccess_token(),"application/json",timeSlotRequests)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<TimeSlotResponse>()
                {

                    @Override
                    public void onSuccess(TimeSlotResponse timeSlotResponse)
                    {
                        getMvpView().hideLoading();

                        if(timeSlotResponse.getResponseCode()==1 )
                        {
                            getMvpView().onSuccessfullyGetTimeSlot(timeSlotResponse.getResponseData());
                            // getMvpView().onError(timeSlotResponse.getResponseText());
                        }
                        else
                        {

                            getMvpView().onError(timeSlotResponse.getResponseText());
                            getMvpView().onSuccessfullyGetTimeSlot(timeSlotResponse.getResponseData());

                        }

                    }
                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));


    }
}
