package com.bidjobbers.bidjobberspro.features.OnTimeScreen.SocialArea;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface SocialFragmentMvpPresenter  <V extends SocialFragmentMvpView> extends BaseFragmentMvpPresenter<V> {

   public void ClickAddSocialLinkBtnOne();
   public void ClickAddSocialLinkBtnTwo();
   public void ClickAddSocialLinkBtnThree();

   void onGetDataFromApi();

}
