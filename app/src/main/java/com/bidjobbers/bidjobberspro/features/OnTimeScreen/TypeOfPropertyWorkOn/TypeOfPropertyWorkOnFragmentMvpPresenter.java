package com.bidjobbers.bidjobberspro.features.OnTimeScreen.TypeOfPropertyWorkOn;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface TypeOfPropertyWorkOnFragmentMvpPresenter
        <V extends TypeOfPropertyWorkOnFragmentMvpView>
        extends BaseFragmentMvpPresenter<V> {

    public void onYesHaveALicence();
    public void onNoHaveALicence();

    public void onInit(String from);

    void onGetDataFromSP();

    void onGetDataFromApi();

}
