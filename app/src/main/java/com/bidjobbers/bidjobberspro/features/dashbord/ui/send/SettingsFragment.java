package com.bidjobbers.bidjobberspro.features.dashbord.ui.send;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.features.Archive.ArchiveActivity;
import com.bidjobbers.bidjobberspro.features.LanguageChange.LanguageChangeActivity;
import com.bidjobbers.bidjobberspro.features.NewArchive.NewArchiveActivity;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.features.prcdetails.PercDetailsUpdateActivity;
import com.bidjobbers.bidjobberspro.features.profiledetails.ProfileActivity;
import com.bidjobbers.bidjobberspro.features.resetpasswordfromsettings.ResetPasswordActivity;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;
import com.bumptech.glide.Glide;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SettingsFragment extends BaseFragment implements SettingsFragmentMvpView,View.OnClickListener {


    @BindView(R.id.ivProfileImage)
    ImageView ivProfileImage;
    @BindView(R.id.ll_prs_dtls)
    LinearLayout ll_prs_dtls;
    @BindView(R.id.ll_bd_dtls)
    LinearLayout ll_bd_dtls;
    @BindView(R.id.ll_cng_pass)
    LinearLayout ll_cng_pass;
    @BindView(R.id.ll_log_out)
    LinearLayout ll_log_out;
    @BindView(R.id.ll_cng_language)
    LinearLayout ll_cng_language;
    @BindView(R.id.ll_blocked)
    LinearLayout ll_archive;
    @BindView(R.id.ll_archived)
    LinearLayout ll_archived;

    @BindView(R.id.iv_personal_details)
    ImageView iv_personal_details;
    @BindView(R.id.tv_personal_details)
    TextView tv_personal_details;

    @BindView(R.id.iv_business_details)
    ImageView iv_business_details;
    @BindView(R.id.tv_business_details)
    TextView tv_business_details;


    @Inject
    SettingsFragmentPresenter<SettingsFragmentMvpView> settingsFragmentPresenter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_settings, container, false);
        setUnBinder(ButterKnife.bind(this,root));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }
        settingsFragmentPresenter.onAttach(this);


        return root;
    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        settingsFragmentPresenter.onSuccessfullGetProfile(prefs.getString("LOCAL_KEY",""));

    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        settingsFragmentPresenter.onSuccessfullGetProfile(prefs.getString("LOCAL_KEY",""));
    }

    @Override
    public void successfullyGetProfile(getProfileResponse getProfileResponse) {
        Glide.with(getActivity())
                .load(getProfileResponse.getResponseData().getProfile_image())
                .into(ivProfileImage);
    }

    @Override
    public void successfullyLogout() {
        Intent i=new Intent(getActivity(), LoginActivity.class);
        startActivity(i);
        getActivity().finish();
    }

    @Override
    public void onDeactivate(String data) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
                getActivity().finish();
            }
        }, 800);
    }

    @OnClick({R.id.tv_logout,R.id.iv_logout,R.id.iv_change_pwd,R.id.tv_change_pwd,R.id.iv_cahnge_lang,R.id.tv_clanguage_lng,R.id.iv_archive,R.id.tv_archiveed,R.id.tv_block,R.id.iv_block,R.id.iv_business_details,R.id.tv_business_details,R.id.tv_personal_details,R.id.iv_personal_details,R.id.ll_prs_dtls,R.id.ll_bd_dtls,R.id.ll_cng_pass,R.id.ll_log_out,R.id.ll_cng_language,R.id.ll_blocked,R.id.ll_archived})
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.tv_personal_details:

                startActivity(new Intent(getActivity(), PercDetailsUpdateActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.iv_personal_details:

                startActivity(new Intent(getActivity(), PercDetailsUpdateActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;

            case R.id.ll_prs_dtls:

                startActivity(new Intent(getActivity(), PercDetailsUpdateActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.iv_business_details:

                startActivity(new Intent(getActivity(), ProfileActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.tv_business_details:

                startActivity(new Intent(getActivity(), ProfileActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.ll_bd_dtls:

                startActivity(new Intent(getActivity(), ProfileActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.ll_cng_pass:

                 startActivity(new Intent(getActivity(), ResetPasswordActivity.class));
                 getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.iv_change_pwd:

                startActivity(new Intent(getActivity(), ResetPasswordActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.tv_change_pwd:

                startActivity(new Intent(getActivity(), ResetPasswordActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.iv_logout:
                settingsFragmentPresenter.onLogoutClick();
                break;
            case R.id.tv_logout:
                settingsFragmentPresenter.onLogoutClick();
                break;
            case R.id.ll_log_out:
                settingsFragmentPresenter.onLogoutClick();
                break;
            case R.id.ll_cng_language:
                startActivity(new Intent(getActivity(), LanguageChangeActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.iv_cahnge_lang:
                startActivity(new Intent(getActivity(), LanguageChangeActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.tv_clanguage_lng:
                startActivity(new Intent(getActivity(), LanguageChangeActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.tv_block:
                startActivity(new Intent(getActivity(), ArchiveActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.iv_block:
                startActivity(new Intent(getActivity(), ArchiveActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.ll_blocked:
                startActivity(new Intent(getActivity(), ArchiveActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.ll_archived:
                startActivity(new Intent(getActivity(), NewArchiveActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.iv_archive:
                startActivity(new Intent(getActivity(), NewArchiveActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;
            case R.id.tv_archiveed:
                startActivity(new Intent(getActivity(), NewArchiveActivity.class));
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                break;

        }
    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }
}