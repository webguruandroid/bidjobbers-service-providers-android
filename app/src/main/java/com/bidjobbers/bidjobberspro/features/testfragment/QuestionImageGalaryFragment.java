package com.bidjobbers.bidjobberspro.features.testfragment;

import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;
import com.bidjobbers.bidjobberspro.utils.Validation.BusinessHoursDetaisResult;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link QuestionImageGalaryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link QuestionImageGalaryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class QuestionImageGalaryFragment extends BaseFragment implements AdapterView.OnItemSelectedListener,QuestionImageGalaryFragmentMvpView{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @BindView(R.id.ll_day1)LinearLayout ll_day1;@BindView(R.id.ll_edt_time_one) LinearLayout ll_edt_time_one;    @BindView(R.id.ll_time_done_one) LinearLayout ll_time_done_one; @BindView(R.id.ll_done_one) LinearLayout ll_done_one;    @BindView(R.id.ll_edit_one) LinearLayout ll_edit_one;
    @BindView(R.id.ll_day2) LinearLayout ll_day2;@BindView(R.id.ll_edt_time_two) LinearLayout ll_edt_time_two;@BindView(R.id.ll_time_done_two) LinearLayout ll_time_done_two;@BindView(R.id.ll_done_two)LinearLayout ll_done_two;@BindView(R.id.ll_edit_two)LinearLayout ll_edit_two;
    @BindView(R.id.ll_day3)LinearLayout ll_day3;@BindView(R.id.ll_edt_time_three)LinearLayout ll_edt_time_three;@BindView(R.id.ll_time_done_three)LinearLayout ll_time_done_three;@BindView(R.id.ll_done_three)LinearLayout ll_done_three;@BindView(R.id.ll_edit_three)LinearLayout ll_edit_three;
    @BindView(R.id.ll_day4)LinearLayout ll_day4;@BindView(R.id.ll_edt_time_four)LinearLayout ll_edt_time_four;@BindView(R.id.ll_time_done_four)LinearLayout ll_time_done_four;@BindView(R.id.ll_done_four)LinearLayout ll_done_four;@BindView(R.id.ll_edit_four)LinearLayout ll_edit_four;
    @BindView(R.id.ll_day5)LinearLayout ll_day5;@BindView(R.id.ll_edt_time_five)LinearLayout ll_edt_time_five;@BindView(R.id.ll_time_done_five)LinearLayout ll_time_done_five ;@BindView(R.id.ll_done_five)LinearLayout ll_done_five;@BindView(R.id.ll_edit_five)LinearLayout ll_edit_five;
    @BindView(R.id.ll_day6)LinearLayout ll_day6;@BindView(R.id.ll_edt_time_six)LinearLayout ll_edt_time_six;@BindView(R.id.ll_time_done_six)LinearLayout ll_time_done_six;@BindView(R.id.ll_done_six)LinearLayout ll_done_six;@BindView(R.id.ll_edit_six)LinearLayout ll_edit_six;
    @BindView(R.id.ll_day7)LinearLayout ll_day7;@BindView(R.id.ll_edt_time_seven)LinearLayout ll_edt_time_seven;@BindView(R.id.ll_time_done_seven)LinearLayout ll_time_done_seven;@BindView(R.id.ll_done_seven)LinearLayout ll_done_seven;@BindView(R.id.ll_edit_seven)LinearLayout ll_edit_seven;
    @BindView(R.id.ll_disable_monday)LinearLayout ll_disable_monday; @BindView(R.id.ll_disable_tusday)LinearLayout ll_disable_tusday; @BindView(R.id.ll_disable_wednesday)LinearLayout ll_disable_wednesday;@BindView(R.id.ll_disable_thusday)LinearLayout ll_disable_thusday;
    @BindView(R.id.ll_disable_friday)LinearLayout  ll_disable_friday; @BindView(R.id.ll_disable_saturday)LinearLayout ll_disable_saturday ; @BindView(R.id.ll_disable_sunday)LinearLayout ll_disable_sunday;
    @BindView(R.id.tv_apply_three)TextView tv_apply_three;
    @BindView(R.id.ll_first)LinearLayout ll_first;
    @BindView(R.id.chk_monday)CheckBox chk_monday; @BindView(R.id.chk_tusday)CheckBox chk_tusday; @BindView(R.id.chk_wednesday)CheckBox chk_wednesday; @BindView(R.id.chk_thusday)CheckBox chk_thusday;@BindView(R.id.chk_friday)CheckBox chk_friday;@BindView(R.id.chk_saturday)CheckBox chk_saturday;@BindView(R.id.chk_sunday)CheckBox chk_sunday;
    @BindView(R.id.tv_apply_monday)TextView tv_apply_monday; @BindView(R.id.tv_apply_tuesday)TextView  tv_apply_tuesday;@BindView(R.id.tv_apply_wednesday)TextView tv_apply_wednesday;@BindView(R.id.tv_apply_friday)TextView tv_apply_friday;@BindView(R.id.tv_apply_saturday)TextView  tv_apply_saturday;@BindView(R.id.tv_apply_sunday) TextView  tv_apply_sunday;
    @BindView(R.id.tv_monday_time)TextView  tv_monday_time;@BindView(R.id.tv_tuesday_time)TextView  tv_tuesday_time;@BindView(R.id.tv_wednesday_time)TextView  tv_wednesday_time;@BindView(R.id.tv_thursday_time)TextView  tv_thursday_time;@BindView(R.id.tv_friday_time)TextView  tv_friday_time;@BindView(R.id.tv_saturday_time)TextView  tv_saturday_time;@BindView(R.id.tv_sunday_time)TextView tv_sunday_time;
    @BindView(R.id.spin_end1) Spinner spin_end1; @BindView(R.id.spin_start1)    Spinner spin1;    @BindView(R.id.spin_start2)    Spinner spin2 ;    @BindView(R.id.spin_end2)    Spinner spin_end2 ;    @BindView(R.id.spin_start3)    Spinner spin3 ;    @BindView(R.id.spin_end3)    Spinner spin_end3 ;
    @BindView(R.id.spin_start4) Spinner spin4 ; @BindView(R.id.spin_end4)    Spinner spin_end4 ;    @BindView(R.id.spin_start5)    Spinner spin5 ;    @BindView(R.id.spin_end5)    Spinner spin_end5 ;    @BindView(R.id.spin_start6)    Spinner spin6;    @BindView(R.id.spin_end6)    Spinner spin_end6  ;
    @BindView(R.id.spin_start7) Spinner spin7 ;  @BindView(R.id.spin_end7)    Spinner spin_end7;
    @BindView(R.id.tvMonday)TextView tvMonday;
    @BindView(R.id.tvTuesday)TextView tvTuesday;
    @BindView(R.id.tvWednesday)TextView tvWednesday;
    @BindView(R.id.tvThrusday)TextView tvThrusday;
    @BindView(R.id.tvFriday)TextView tvFriday;
    @BindView(R.id.tvSaturday)TextView tvSaturday;
    @BindView(R.id.tvSunday)TextView tvSunday;
    @BindView(R.id.llMondayBg)LinearLayout llMondayBg;
    @BindView(R.id.llTuesDayBg)LinearLayout llTuesDayBg;
    @BindView(R.id.llWednesDayBg)LinearLayout llWednesDayBg;
    @BindView(R.id.llThrusdayBg)LinearLayout llThrusdayBg;
    @BindView(R.id.llFridayBg)LinearLayout llFridayBg;
    @BindView(R.id.llSaturdayBg)LinearLayout llSaturdayBg;
    @BindView(R.id.llSundayBg)LinearLayout llSundayBg;



    ArrayList<String>dayId=new ArrayList<>();
    ArrayList<String>startTimeId=new ArrayList<>();
    ArrayList<String>startTime=new ArrayList<>();
    ArrayList<String>endTimeId=new ArrayList<>();
    ArrayList<String>endTime=new ArrayList<>();
    ArrayList<String>dayName=new ArrayList<>();



    ArrayList<String>dayIdNew=new ArrayList<>();
    ArrayList<String>startTimeIdNew=new ArrayList<>();
    ArrayList<String>startTimeNew=new ArrayList<>();
    ArrayList<String>endTimeIdNew=new ArrayList<>();
    ArrayList<String>endTimeNew=new ArrayList<>();
    ArrayList<String>dayNameNew=new ArrayList<>();


    String MonId,TueId,WedId,ThrusId,FriId,SaturId,SunId;

    int pos=10;
    String  spin_start1_text="08:00",spin_end1_text="19:00"; int spin_start1_index=0; int spin_end1_index=0;
    String  spin_start2_text="08:00",spin_end2_text="19:00"; int spin_start2_index=0; int spin_end2_index=0;
    String  spin_start3_text="08:00",spin_end3_text="19:00"; int spin_start3_index=0; int spin_end3_index=0;
    String  spin_start4_text="08:00",spin_end4_text="19:00"; int spin_start4_index=0; int spin_end4_index=0;
    String  spin_start5_text="08:00",spin_end5_text="19:00"; int spin_start5_index=0; int spin_end5_index=0;
    String  spin_start6_text="08:00",spin_end6_text="19:00"; int spin_start6_index=0; int spin_end6_index=0;
    String  spin_start7_text="08:00",spin_end7_text="19:00"; int spin_start7_index=0; int spin_end7_index=0;

    @Inject
    QuestionImageGalaryFragmentPresenter<QuestionImageGalaryFragmentMvpView> questionImageGalaryFragmentPresenter;



    ArrayList<String> time = new ArrayList<String>();
    ArrayList<String>timeId=new ArrayList<>();
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public static QuestionImageGalaryFragment newInstance()
    {
        QuestionImageGalaryFragment fragment = new QuestionImageGalaryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            String str = bundle.getString("FROMG");
            Log.e("FROMG", str);

        }
    }

    @Override
    public void setDataFromApi(DataModelForFirstTimeQusAns.BusinessHours businessHours) {


        dayId=new ArrayList<>();
        startTimeId=new ArrayList<>();
        startTime=new ArrayList<>();
        endTimeId=new ArrayList<>();
        endTime=new ArrayList<>();


        dayIdNew=new ArrayList<>();
        startTimeIdNew=new ArrayList<>();
        startTimeNew=new ArrayList<>();
        endTimeIdNew=new ArrayList<>();
        endTimeNew=new ArrayList<>();
        dayNameNew=new ArrayList<>();


        Log.e("Question",businessHours.getOneHourTime());

        time.add(businessHours.getOneHourTime());
        time.add(businessHours.getTwoHourTime());
        time.add(businessHours.getThreeHourTime());
        time.add(businessHours.getFourHourTime());
        time.add(businessHours.getFiveHourTime());
        time.add(businessHours.getSixHourTime());
        time.add(businessHours.getSevenHourTime());
        time.add(businessHours.getEightHourTime());
        time.add(businessHours.getNineHourTime());
        time.add(businessHours.getTenHourTime());
        time.add(businessHours.getElevenHourTime());
        time.add(businessHours.getTwelveHourTime());

        timeId.add(businessHours.getOneHourId());
        timeId.add(businessHours.getTwoHourId());
        timeId.add(businessHours.getThreeHourId());
        timeId.add(businessHours.getFourHourId());
        timeId.add(businessHours.getFiveHourId());
        timeId.add(businessHours.getSixHourId());
        timeId.add(businessHours.getSevenHourId());
        timeId.add(businessHours.getEightHourId());
        timeId.add(businessHours.getNineHourId());
        timeId.add(businessHours.getTenHourId());
        timeId.add(businessHours.getElevenHourId());
        timeId.add(businessHours.getTwoHourId());





       MonId=businessHours.getMondayId();
       TueId=businessHours.getTuesdayId();
       WedId=businessHours.getWednesdayId();
       ThrusId=businessHours.getThursdayId();
       FriId=businessHours.getFridayId();
       SaturId=businessHours.getSaturdayId();
       SunId=businessHours.getSundayId();






        tvMonday.setText(businessHours.getMondayName());
        if(businessHours.isMondaySelected())
        {
            chk_monday.setChecked(true);

            spin_start1_index=Integer.parseInt(businessHours.getMondayFromTimeId());
            spin_start1_text=businessHours.getMondayFromTime();
            spin_end1_index=Integer.parseInt(businessHours.getMondayToTimeId());
            spin_end1_text=businessHours.getMondayToTime();

            questionImageGalaryFragmentPresenter.onChkMonday(true);
            tv_monday_time.setText(businessHours.getMondayFromTime()+"-"+businessHours.getMondayToTime());


        }
        else
        {



            chk_monday.setChecked(false);
            questionImageGalaryFragmentPresenter.onChkMonday(false);
            tv_monday_time.setText("08:00"+"-"+"08:00");

        }

        tvTuesday.setText(businessHours.getTuesdayName());
        if(businessHours.isTuesdaySelected())
        {
            chk_tusday.setChecked(true);

            spin_start2_index=Integer.parseInt(businessHours.getTuesdayFromTimeId());
            spin_start2_text=businessHours.getTuesdayFromTime();
            spin_end2_index=Integer.parseInt(businessHours.getTuesdayToTimeId());
            spin_end2_text=businessHours.getTuesdayToTime();
            questionImageGalaryFragmentPresenter.onChkTuesday(true);
            tv_tuesday_time.setText(businessHours.getTuesdayFromTime()+"-"+businessHours.getTuesdayToTime());

            questionImageGalaryFragmentPresenter.onDoneTwo();
        }
        else
        {
            chk_tusday.setChecked(false);
            tv_tuesday_time.setText("08:00"+"-"+"08:00");
            questionImageGalaryFragmentPresenter.onChkTuesday(false);

        }
        tvWednesday.setText(businessHours.getWednesdayName());
        if(businessHours.isWednesdaySelected())
        {
            chk_wednesday.setChecked(true);
            spin_start3_index=Integer.parseInt(businessHours.getWednesdayFromTimeId());
            spin_start3_text=businessHours.getWednesdayFromTime();
            spin_end3_index=Integer.parseInt(businessHours.getWednesdayToTimeId());
            spin_end3_text=businessHours.getWednesdayToTime();
            questionImageGalaryFragmentPresenter.onChkWednesday(true);
            tv_wednesday_time.setText(businessHours.getWednesdayFromTime()+"-"+businessHours.getWednesdayToTime());
            questionImageGalaryFragmentPresenter.onDoneThree();
        }
        else
        {
            chk_wednesday.setChecked(false);
            questionImageGalaryFragmentPresenter.onChkWednesday(false);
            tv_wednesday_time.setText("08:00"+"-"+"08:00");
        }
        tvThrusday.setText(businessHours.getThrusdayName());
        if(businessHours.isThursdaySelected())
        {
            chk_thusday.setChecked(true);
            spin_start4_index=Integer.parseInt(businessHours.getThursdayFromTimeId());
            spin_start4_text=businessHours.getThursdayFromTime();
            spin_end4_index=Integer.parseInt(businessHours.getThursdayToTimeId());
            spin_end4_text=businessHours.getThrusdayToTime();
            questionImageGalaryFragmentPresenter.onChkThusday(true);
            tv_thursday_time.setText(businessHours.getThursdayFromTime()+"-"+businessHours.getThrusdayToTime());
            questionImageGalaryFragmentPresenter.onDoneFour();
        }
        else
        {
            chk_thusday.setChecked(false);
            questionImageGalaryFragmentPresenter.onChkThusday(false);
            tv_thursday_time.setText("08:00"+"-"+"08:00");
        }
        tvFriday.setText(businessHours.getFridayName());
        if(businessHours.isFridaySelected())
        {
            chk_friday.setChecked(true);
            spin_start5_index=Integer.parseInt(businessHours.getFridayFromTimeId());
            spin_start5_text=businessHours.getFridayFromTime();
            spin_end5_index=Integer.parseInt(businessHours.getFridayToTimeId());
            spin_end5_text=businessHours.getFridayToTime();
            questionImageGalaryFragmentPresenter.onChkFriday(true);
            tv_friday_time.setText(businessHours.getFridayFromTime()+"-"+businessHours.getFridayToTime());
            questionImageGalaryFragmentPresenter.onDoneFive();
        }
        else
        {
            chk_friday.setChecked(false);
            questionImageGalaryFragmentPresenter.onChkFriday(false);
            tv_friday_time.setText("08:00"+"-"+"08:00");
        }
        tvSaturday.setText(businessHours.getSaturdayName());
        if(businessHours.isSaturdaySelected())
        {
            chk_saturday.setChecked(true);

            spin_start6_index=Integer.parseInt(businessHours.getSaturdayFromTimeId());
            spin_start6_text=businessHours.getSaturdayFromTime();
            spin_end6_index=Integer.parseInt(businessHours.getSaturdayToTimeId());
            spin_end6_text=businessHours.getSaturdayToTime();
            questionImageGalaryFragmentPresenter.onChkSaturday(true);
            tv_saturday_time.setText(businessHours.getSaturdayFromTime()+"-"+businessHours.getSaturdayToTime());
            questionImageGalaryFragmentPresenter.onDoneSix();
        }
        else
        {
            chk_saturday.setChecked(false);
            questionImageGalaryFragmentPresenter.onChkSaturday(false);
            tv_saturday_time.setText("08:00"+"-"+"08:00");
        }

        tvSunday.setText(businessHours.getSundayName());
        if(businessHours.isSundaySelected())
        {
            chk_sunday.setChecked(true);
            spin_start7_index=Integer.parseInt(businessHours.getSundayFromTimeId());
            spin_start7_text=businessHours.getSundayFromTime();
            spin_end7_index=Integer.parseInt(businessHours.getSundayToTimeId());
            spin_end7_text=businessHours.getSundayToTime();
            questionImageGalaryFragmentPresenter.onChkSunday(true);
            tv_sunday_time.setText(businessHours.getSundayFromTime()+"-"+businessHours.getSundayToTime());
            questionImageGalaryFragmentPresenter.onDoneSeven();
        }
        else
        {
            chk_sunday.setChecked(false);
            questionImageGalaryFragmentPresenter.onChkSunday(false);
            tv_sunday_time.setText("08:00"+"-"+"08:00");
        }




    }

    @Override
    protected void setUp(View view) {




    }

    public int getIndexOftimining(String timing)
    {
        return time.indexOf(timing);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

      /*  spin_start1_text="08:00";spin_end1_text="19:00";

        spin1.setSelection(getIndexOftimining(spin_start1_text));
        spin_end1.setSelection(getIndexOftimining(spin_end1_text));


        spin_start2_text="08:00";spin_end2_text="19:00";

        spin2.setSelection(getIndexOftimining(spin_start2_text));
        spin_end2.setSelection(getIndexOftimining(spin_end2_text));

        spin_start3_text="08:00";spin_end3_text="19:00";

        spin3.setSelection(getIndexOftimining(spin_start3_text));
        spin_end3.setSelection(getIndexOftimining(spin_end3_text));

        spin_start4_text="08:00";spin_end4_text="19:00";

        spin4.setSelection(getIndexOftimining(spin_start4_text));
        spin_end4.setSelection(getIndexOftimining(spin_end4_text));

        spin_start5_text="08:00";spin_end5_text="19:00";

        spin5.setSelection(getIndexOftimining(spin_start5_text));
        spin_end5.setSelection(getIndexOftimining(spin_end5_text));

        spin_start6_text="08:00";spin_end6_text="19:00";

        spin6.setSelection(getIndexOftimining(spin_start6_text));
        spin_end6.setSelection(getIndexOftimining(spin_end6_text));

        spin_start7_text="08:00";spin_end7_text="19:00";

        spin7.setSelection(getIndexOftimining(spin_start7_text));
        spin_end7.setSelection(getIndexOftimining(spin_end7_text));*/


      //  questionImageGalaryFragmentPresenter.onInit("");


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_question_image_galary, container, false);
        setUnBinder(ButterKnife.bind(this,v));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }
        questionImageGalaryFragmentPresenter.onAttach(this);
        questionImageGalaryFragmentPresenter.onGetDataFromApi();

        return v;
    }




    public void allEditClose()
    {
        ll_time_done_one.setVisibility(View.VISIBLE);
        ll_edt_time_one.setVisibility(View.GONE);
        ll_day1.setVisibility(View.VISIBLE);

        ll_time_done_two.setVisibility(View.VISIBLE);
        ll_edt_time_two.setVisibility(View.GONE);
        ll_day2.setVisibility(View.VISIBLE);

        ll_time_done_three.setVisibility(View.VISIBLE);
        ll_edt_time_three.setVisibility(View.GONE);
        ll_day3.setVisibility(View.VISIBLE);


        ll_time_done_four.setVisibility(View.VISIBLE);
        ll_edt_time_four.setVisibility(View.GONE);
        ll_day4.setVisibility(View.VISIBLE);

        ll_time_done_five.setVisibility(View.VISIBLE);
        ll_edt_time_five.setVisibility(View.GONE);
        ll_day5.setVisibility(View.VISIBLE);

        ll_time_done_six.setVisibility(View.VISIBLE);
        ll_edt_time_six.setVisibility(View.GONE);
        ll_day6.setVisibility(View.VISIBLE);

        ll_time_done_seven.setVisibility(View.VISIBLE);
        ll_edt_time_seven.setVisibility(View.GONE);
        ll_day7.setVisibility(View.VISIBLE);

        ll_time_done_one.setVisibility(View.VISIBLE);
        ll_edt_time_one.setVisibility(View.GONE);
        ll_day1.setVisibility(View.VISIBLE);
    }

    public void allTimingSet(String timing)
    {
        tv_monday_time.setText(timing);
        tv_tuesday_time.setText(timing);
        tv_wednesday_time.setText(timing);
        tv_thursday_time.setText(timing);
        tv_friday_time.setText(timing);
        tv_saturday_time.setText(timing);
        tv_sunday_time.setText(timing);
    }

    public void allSpinnerBindAtPosition(int start_pos, int end_pos)
    {
        spin1.setSelection(start_pos);
        spin_end1.setSelection(end_pos);

        spin2.setSelection(start_pos);
        spin_end2.setSelection(end_pos);

        spin3.setSelection(start_pos);
        spin_end3.setSelection(end_pos);

        spin4.setSelection(start_pos);
        spin_end4.setSelection(end_pos);

        spin5.setSelection(start_pos);
        spin_end5.setSelection(end_pos);

        spin6.setSelection(start_pos);
        spin_end6.setSelection(end_pos);

        spin7.setSelection(start_pos);
        spin_end7.setSelection(end_pos);


    }


    @OnClick
            ({R.id.tv_apply_monday,R.id.tv_apply_tuesday,R.id.tv_apply_wednesday,R.id.tv_apply_three,R.id.tv_apply_friday,R.id.tv_apply_saturday,R.id.tv_apply_sunday,
              R.id.ll_edit_one,R.id.ll_edit_two,R.id.ll_edit_three,R.id.ll_edit_four,R.id.ll_edit_five,R.id.ll_edit_six,R.id.ll_edit_seven,
              R.id.ll_done_one,R.id.ll_done_two,R.id.ll_done_three,R.id.ll_done_four,R.id.ll_done_five,R.id.ll_done_six,R.id.ll_done_seven
             })
    public void onClick(View v)
    {

        switch (v.getId()){
            case R.id.tv_apply_monday :
                questionImageGalaryFragmentPresenter.onApplyMonday();
                break;
            case R.id.tv_apply_tuesday :
                questionImageGalaryFragmentPresenter.onApplyTuesday();
                break;
            case R.id.tv_apply_wednesday :
                questionImageGalaryFragmentPresenter.onApplyWednesday();
                break;
            case R.id.tv_apply_three :
                questionImageGalaryFragmentPresenter.onApplyThree();
                break;
            case R.id.tv_apply_friday :
                questionImageGalaryFragmentPresenter.onApplyFriday();
                break;
            case R.id.tv_apply_saturday :
                questionImageGalaryFragmentPresenter.onApplySaturday();
                break;
            case R.id.tv_apply_sunday :
                questionImageGalaryFragmentPresenter.onApplySunday();
                break;

            case R.id.ll_edit_one :
                questionImageGalaryFragmentPresenter.onEditOne();
                break;
            case R.id.ll_edit_two :
                questionImageGalaryFragmentPresenter.onEditTwo();
                break;
            case R.id.ll_edit_three :
                questionImageGalaryFragmentPresenter.onEditThree();
                break;
            case R.id.ll_edit_four :
                questionImageGalaryFragmentPresenter.onEditFour();
                break;
            case R.id.ll_edit_five :
                questionImageGalaryFragmentPresenter.onEditFive();
                break;
            case R.id.ll_edit_six :
                questionImageGalaryFragmentPresenter.onEditSix();
                break;
            case R.id.ll_edit_seven :
                questionImageGalaryFragmentPresenter.onEditSeven();
                break;
            case R.id.ll_done_one :
                questionImageGalaryFragmentPresenter.onDoneOne();
                break;
            case R.id.ll_done_two :
                questionImageGalaryFragmentPresenter.onDoneTwo();
                break;
            case R.id.ll_done_three :
                questionImageGalaryFragmentPresenter.onDoneThree();
                break;
            case R.id.ll_done_four :
                questionImageGalaryFragmentPresenter.onDoneFour();
                break;
            case R.id.ll_done_five :
                questionImageGalaryFragmentPresenter.onDoneFive();
                break;
            case R.id.ll_done_six :
                questionImageGalaryFragmentPresenter.onDoneSix();
                break;
            case R.id.ll_done_seven :
                questionImageGalaryFragmentPresenter.onDoneSeven();
                break;
        }

    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        time.add("08:00");time.add("09:00");time.add("10:00");time.add("11:00");time.add("12:00");time.add("13:00");
//        time.add("14:00");time.add("15:00");time.add("16:00");time.add("17:00");time.add("18:00");time.add("19:00");
//        time.add("20:00");

        SpannableString content = new SpannableString(getResources().getString(R.string.apply));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tv_apply_three.setText(content);

        spin1.setOnItemSelectedListener(this);
        spin1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getContext(), time.get(position), Toast.LENGTH_SHORT).show();
                questionImageGalaryFragmentPresenter.onSpin1(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spin_end1.setOnItemSelectedListener(this);
        spin_end1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getContext(), time.get(position), Toast.LENGTH_SHORT).show();

                questionImageGalaryFragmentPresenter.onSpinEnd1(position);


            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spin2.setOnItemSelectedListener(this);
        spin2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getContext(), time.get(position), Toast.LENGTH_SHORT).show();
                questionImageGalaryFragmentPresenter.onSpin2(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_end2.setOnItemSelectedListener(this);
        spin_end2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(getContext(), time.get(position), Toast.LENGTH_SHORT).show();
                questionImageGalaryFragmentPresenter.onSpinEnd2(position);

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin3.setOnItemSelectedListener(this);
        spin3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(getContext(), time.get(position), Toast.LENGTH_SHORT).show();
                questionImageGalaryFragmentPresenter.onSpin3(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_end3.setOnItemSelectedListener(this);
        spin_end3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(getContext(), time.get(position), Toast.LENGTH_SHORT).show();
                questionImageGalaryFragmentPresenter.onSpinEnd3(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin4.setOnItemSelectedListener(this);
        spin4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(getContext(), time.get(position), Toast.LENGTH_SHORT).show();
                questionImageGalaryFragmentPresenter.onSpin4(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_end4.setOnItemSelectedListener(this);
        spin_end4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(getContext(), time.get(position), Toast.LENGTH_SHORT).show();
                questionImageGalaryFragmentPresenter.onSpinEnd4(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin5.setOnItemSelectedListener(this);
        spin5.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(getContext(), time.get(position), Toast.LENGTH_SHORT).show();
                questionImageGalaryFragmentPresenter.onSpin5(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_end5.setOnItemSelectedListener(this);
        spin_end5.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(getContext(), time.get(position), Toast.LENGTH_SHORT).show();
                questionImageGalaryFragmentPresenter.onSpinEnd5(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin6.setOnItemSelectedListener(this);
        spin6.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(getContext(), time.get(position), Toast.LENGTH_SHORT).show();
                questionImageGalaryFragmentPresenter.onSpin6(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_end6.setOnItemSelectedListener(this);
        spin_end6.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(getContext(), time.get(position), Toast.LENGTH_SHORT).show();
                questionImageGalaryFragmentPresenter.onSpinEnd6(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin7.setOnItemSelectedListener(this);
        spin7.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(getContext(), time.get(position), Toast.LENGTH_SHORT).show();
                questionImageGalaryFragmentPresenter.onSpin7(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_end7.setOnItemSelectedListener(this);
        spin_end7.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(getContext(), time.get(position), Toast.LENGTH_SHORT).show();
                questionImageGalaryFragmentPresenter.onSpinEnd7(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        TimingAdapter timeTypeAdapter = new TimingAdapter(getContext(),time);

        spin1.setAdapter(timeTypeAdapter);
        spin2.setAdapter(timeTypeAdapter);
        spin3.setAdapter(timeTypeAdapter);
        spin4.setAdapter(timeTypeAdapter);
        spin5.setAdapter(timeTypeAdapter);
        spin6.setAdapter(timeTypeAdapter);
        spin7.setAdapter(timeTypeAdapter);

        spin_end1.setAdapter(timeTypeAdapter);
        spin_end2.setAdapter(timeTypeAdapter);
        spin_end3.setAdapter(timeTypeAdapter);
        spin_end4.setAdapter(timeTypeAdapter);
        spin_end5.setAdapter(timeTypeAdapter);
        spin_end6.setAdapter(timeTypeAdapter);
        spin_end7.setAdapter(timeTypeAdapter);


        chk_monday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                questionImageGalaryFragmentPresenter.onChkMonday(isChecked);


            }
        });
        chk_tusday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                questionImageGalaryFragmentPresenter.onChkTuesday(isChecked);


            }
        });
        chk_wednesday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                questionImageGalaryFragmentPresenter.onChkWednesday(isChecked);

            }
        });
        chk_thusday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                questionImageGalaryFragmentPresenter.onChkThusday(isChecked);

            }
        });
        chk_friday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                questionImageGalaryFragmentPresenter.onChkFriday(isChecked);

            }
        });
        chk_saturday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                questionImageGalaryFragmentPresenter.onChkSaturday(isChecked);

            }
        });
        chk_sunday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                questionImageGalaryFragmentPresenter.onChkSunday(isChecked);

            }
        });

    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }



    @Override
    public void actionTestPresenter() {

        Toast.makeText(getContext(), "Test", Toast.LENGTH_SHORT).show();

    }


    @Override
    public void actionApplyMonday() {
        String temp_start = spin_start1_text.replace(":", ".");
        String temp_end = spin_end1_text.replace(":", ".");
        double start = Double.valueOf(temp_start);
        double end = Double.valueOf(temp_end);
        if ((end - start) >= 1)
        {
            tv_monday_time.setText(spin_start1_text+" - "+spin_end1_text);
            allEditClose();
            allTimingSet(spin_start1_text+" - "+spin_end1_text);
            allSpinnerBindAtPosition(spin_start1_index,spin_end1_index);
        }
        else
        {
            Toast.makeText(getContext(), "End time start time should have a difference of 1 hour", Toast.LENGTH_SHORT).show();
        }

        setApplyAll(spin_start1_index+"",spin_start1_text,spin_end1_index+"",spin_end1_text);


    }

    private void setApplyAll(String spin_start1_index,String spin_start1_text,String spin_end1_index,String spin_end1_text)
    {
        dayName.add("Monday");
        dayId.add(MonId);
        startTimeId.add(spin_start1_index+"");
        startTime.add(spin_start1_text);
        endTimeId.add(spin_end1_index+"");
        endTime.add(spin_end1_text);

        dayName.add("Tuesday");
        dayId.add(TueId);
        startTimeId.add(spin_start1_index+"");
        startTime.add(spin_start1_text);
        endTimeId.add(spin_end1_index+"");
        endTime.add(spin_end1_text);

        dayName.add("Wednesday");
        dayId.add(WedId);
        startTimeId.add(spin_start1_index+"");
        startTime.add(spin_start1_text);
        endTimeId.add(spin_end1_index+"");
        endTime.add(spin_end1_text);

        dayName.add("Thursday");
        dayId.add(ThrusId);
        startTimeId.add(spin_start1_index+"");
        startTime.add(spin_start1_text);
        endTimeId.add(spin_end1_index+"");
        endTime.add(spin_end1_text);

        dayName.add("Friday");
        dayId.add(FriId);
        startTimeId.add(spin_start1_index+"");
        startTime.add(spin_start1_text);
        endTimeId.add(spin_end1_index+"");
        endTime.add(spin_end1_text);



        dayName.add("Saturday");
        dayId.add(SaturId);
        startTimeId.add(spin_start1_index+"");
        startTime.add(spin_start1_text);
        endTimeId.add(spin_end1_index+"");
        endTime.add(spin_end1_text);

        dayName.add("Sunday");
        dayId.add(SunId);
        startTimeId.add(spin_start1_index+"");
        startTime.add(spin_start1_text);
        endTimeId.add(spin_end1_index+"");
        endTime.add(spin_end1_text);
    }
    @Override
    public void actionApplyTuesday() {
        String temp_start = spin_start2_text.replace(":", ".");
        String temp_end = spin_end2_text.replace(":", ".");
        double start = Double.valueOf(temp_start);
        double end = Double.valueOf(temp_end);
        if ((end - start) >= 1) {
            allEditClose();
            allTimingSet(spin_start2_text + " - " + spin_end2_text);
            allSpinnerBindAtPosition(spin_start2_index, spin_end2_index);
        }
        else
        {
            Toast.makeText(getContext(), "End time start time should have a difference of 1 hour", Toast.LENGTH_SHORT).show();

        }


        setApplyAll(spin_start2_index+"",spin_start2_text,spin_end2_index+"",spin_end2_text);
    }
    @Override
    public void actionApplyWednesday() {
        String temp_start = spin_start3_text.replace(":", ".");
        String temp_end = spin_end3_text.replace(":", ".");
        double start = Double.valueOf(temp_start);
        double end = Double.valueOf(temp_end);
        if ((end - start) >= 1) {
            allEditClose();
            allTimingSet(spin_start3_text+" - "+spin_end3_text);
            allSpinnerBindAtPosition(spin_start3_index,spin_end3_index);
        }
        else
        {
            Toast.makeText(getContext(), "End time start time should have a difference of 1 hour", Toast.LENGTH_SHORT).show();

        }

        setApplyAll(spin_start3_index+"",spin_start3_text,spin_end3_index+"",spin_end3_text);
    }
    @Override
    public void actionApplyThree() {
        //Toast.makeText(getContext(), "tv_apply_thursday", Toast.LENGTH_SHORT).show();
        String temp_start = spin_start4_text.replace(":", ".");
        String temp_end = spin_end4_text.replace(":", ".");
        double start = Double.valueOf(temp_start);
        double end = Double.valueOf(temp_end);
        if ((end - start) >= 1) {
            allEditClose();
            allTimingSet(spin_start4_text+" - "+spin_end4_text);
            allSpinnerBindAtPosition(spin_start4_index,spin_end4_index);
        }
        else
        {
            Toast.makeText(getContext(), "End time start time should have a difference of 1 hour", Toast.LENGTH_SHORT).show();

        }
        setApplyAll(spin_start4_index+"",spin_start4_text,spin_end4_index+"",spin_end3_text);
    }
    @Override
    public void actionApplyFriday() {
        String temp_start = spin_start5_text.replace(":", ".");
        String temp_end = spin_end5_text.replace(":", ".");
        double start = Double.valueOf(temp_start);
        double end = Double.valueOf(temp_end);
        if ((end - start) >= 1) {
            allEditClose();
            allTimingSet(spin_start5_text+" - "+spin_end5_text);
            allSpinnerBindAtPosition(spin_start5_index,spin_end5_index);
        }
        else
        {
            Toast.makeText(getContext(), "End time start time should have a difference of 1 hour", Toast.LENGTH_SHORT).show();
        }
        setApplyAll(spin_start5_index+"",spin_start5_text,spin_end5_index+"",spin_end5_text);
    }
    @Override
    public void actionApplySaturday() {
        String temp_start = spin_start6_text.replace(":", ".");
        String temp_end = spin_end6_text.replace(":", ".");
        double start = Double.valueOf(temp_start);
        double end = Double.valueOf(temp_end);
        if ((end - start) >= 1) {
            allEditClose();
            allTimingSet(spin_start6_text+" - "+spin_end6_text);
            allSpinnerBindAtPosition(spin_start6_index,spin_end6_index);
        }
        else
        {
            Toast.makeText(getContext(), "End time start time should have a difference of 1 hour", Toast.LENGTH_SHORT).show();
        }
        setApplyAll(spin_start6_index+"",spin_start6_text,spin_end6_index+"",spin_end6_text);
    }
    @Override
    public void actionApplySunday() {
        String temp_start = spin_start7_text.replace(":", ".");
        String temp_end = spin_end7_text.replace(":", ".");
        double start = Double.valueOf(temp_start);
        double end = Double.valueOf(temp_end);
        if ((end - start) >= 1) {
            allEditClose();
            allTimingSet(spin_start7_text+" - "+spin_end7_text);
            allSpinnerBindAtPosition(spin_start7_index,spin_end7_index);
        }
        else
        {
            Toast.makeText(getContext(), "End time start time should have a difference of 1 hour", Toast.LENGTH_SHORT).show();
        }
        setApplyAll(spin_start7_index+"",spin_start7_text,spin_end7_index+"",spin_end7_text);
    }


    @Override
    public void actionEditOne() {
        ll_day1.setVisibility(View.GONE);
        ll_time_done_one.setVisibility(View.GONE);
        ll_edt_time_one.setVisibility(View.VISIBLE);
    }
    @Override
    public void actionEditTwo() {
        ll_day2.setVisibility(View.GONE);
        ll_time_done_two.setVisibility(View.GONE);
        ll_edt_time_two.setVisibility(View.VISIBLE);
    }
    @Override
    public void actionEditThree() {
        ll_day3.setVisibility(View.GONE);
        ll_time_done_three.setVisibility(View.GONE);
        ll_edt_time_three.setVisibility(View.VISIBLE);

    }
    @Override
    public void actionEditFour() {
        ll_day4.setVisibility(View.GONE);
        ll_time_done_four.setVisibility(View.GONE);
        ll_edt_time_four.setVisibility(View.VISIBLE);
    }
    @Override
    public void actionEditFive() {
        ll_day5.setVisibility(View.GONE);
        ll_time_done_five.setVisibility(View.GONE);
        ll_edt_time_five.setVisibility(View.VISIBLE);
    }
    @Override
    public void actionEditSix() {
        ll_day6.setVisibility(View.GONE);
        ll_time_done_six.setVisibility(View.GONE);
        ll_edt_time_six.setVisibility(View.VISIBLE);
    }
    @Override
    public void actionEditSeven() {
        ll_day7.setVisibility(View.GONE);
        ll_time_done_seven.setVisibility(View.GONE);
        ll_edt_time_seven.setVisibility(View.VISIBLE);
    }







    public QuestionImageGalaryObject setDataQuestionImageGalary()
    {



        for(int i=0;i<dayId.size();i++)
        {


            dayIdNew.add(dayId.get(i));
            startTimeIdNew.add(startTimeId.get(i));
            startTimeNew.add(startTime.get(i));
            endTimeIdNew.add(endTimeId.get(i));
            endTimeNew.add(endTime.get(i));
            dayNameNew.add(dayName.get(i));
            Log.e("Question",dayIdNew.get(i)+" "+startTimeIdNew.get(i)+""+dayNameNew.get(i));
        }

        QuestionImageGalaryObject object=new QuestionImageGalaryObject(dayIdNew,startTimeIdNew,startTimeNew,endTimeIdNew,endTimeNew);
        return object;

    }

    @Override
    public void actionDoneOne() {
        String temp_start = spin_start1_text.replace(":", ".");
        String temp_end = spin_end1_text.replace(":", ".");
        double start = Double.valueOf(temp_start);
        double end = Double.valueOf(temp_end);

        if ((end - start) >= 1)
        {
            tv_monday_time.setText(spin_start1_text + " - " + spin_end1_text);
            ll_day1.setVisibility(View.VISIBLE);
            ll_time_done_one.setVisibility(View.VISIBLE);
            ll_edt_time_one.setVisibility(View.GONE);
        }
        else
        {
            Toast.makeText(getContext(), "End time start time should have a difference of 1 hour", Toast.LENGTH_SHORT).show();
        }

        Log.e("Question",MonId+spin_start1_text+spin_start1_index+spin_end1_text+spin_end1_index+MonId);

        dayName.add("Monday");
        dayId.add(MonId);
        startTimeId.add(spin_start1_index+"");
        startTime.add(spin_start1_text);
        endTimeId.add(spin_end1_index+"");
        endTime.add(spin_end1_text);

    }
    @Override
    public void actionDoneTwo() {
        String temp_start = spin_start2_text.replace(":", ".");
        String temp_end = spin_end2_text.replace(":", ".");
        double start = Double.valueOf(temp_start);
        double end = Double.valueOf(temp_end);
        if ((end - start) >= 1)
        {
            ll_day2.setVisibility(View.VISIBLE);
            ll_time_done_two.setVisibility(View.VISIBLE);
            ll_edt_time_two.setVisibility(View.GONE);

            tv_tuesday_time.setText(spin_start2_text+" - "+spin_end2_text);
        }
        else
        {
            Toast.makeText(getContext(), "End time start time should have a difference of 1 hour", Toast.LENGTH_SHORT).show();
        }

        dayName.add("Tuesday");
        dayId.add(TueId);
        startTimeId.add(spin_start2_index+"");
        startTime.add(spin_start2_text);
        endTimeId.add(spin_end2_index+"");
        endTime.add(spin_end2_text);
    }
    @Override
    public void actionDoneThree() {

        String temp_start = spin_start3_text.replace(":", ".");
        String temp_end = spin_end3_text.replace(":", ".");
        double start = Double.valueOf(temp_start);
        double end = Double.valueOf(temp_end);
        if ((end - start) >= 1)
        {
            ll_day3.setVisibility(View.VISIBLE);
            ll_time_done_three.setVisibility(View.VISIBLE);
            ll_edt_time_three.setVisibility(View.GONE);

            tv_wednesday_time.setText(spin_start3_text+" - "+spin_end3_text);
        }
        else
        {
            Toast.makeText(getContext(), "End time start time should have a difference of 1 hour", Toast.LENGTH_SHORT).show();
        }


        dayName.add("Wednesday");
        dayId.add(WedId);
        startTimeId.add(spin_start3_index+"");
        startTime.add(spin_start3_text);
        endTimeId.add(spin_end3_index+"");
        endTime.add(spin_end3_text);

    }
    @Override
    public void actionDoneFour() {

        String temp_start = spin_start4_text.replace(":", ".");
        String temp_end = spin_end4_text.replace(":", ".");
        double start = Double.valueOf(temp_start);
        double end = Double.valueOf(temp_end);
        if ((end - start) >= 1)
        {
            ll_day4.setVisibility(View.VISIBLE);
            ll_time_done_four.setVisibility(View.VISIBLE);
            ll_edt_time_four.setVisibility(View.GONE);
            tv_thursday_time.setText(spin_start4_text+" - "+spin_end4_text);
        }
        else
        {
            Toast.makeText(getContext(), "End time start time should have a difference of 1 hour", Toast.LENGTH_SHORT).show();
        }

        dayName.add("Thursday");
        dayId.add(ThrusId);
        startTimeId.add(spin_start4_index+"");
        startTime.add(spin_start4_text);
        endTimeId.add(spin_end4_index+"");
        endTime.add(spin_end4_text);

    }
    @Override
    public void actionDoneFive() {

        String temp_start = spin_start5_text.replace(":", ".");
        String temp_end = spin_end5_text.replace(":", ".");
        double start = Double.valueOf(temp_start);
        double end = Double.valueOf(temp_end);
        if ((end - start) >= 1)
        {
            ll_day5.setVisibility(View.VISIBLE);
            ll_time_done_five.setVisibility(View.VISIBLE);
            ll_edt_time_five.setVisibility(View.GONE);
            tv_friday_time.setText(spin_start5_text+" - "+spin_end5_text);
        }
        else
        {
            Toast.makeText(getContext(), "End time start time should have a difference of 1 hour", Toast.LENGTH_SHORT).show();
        }

        dayName.add("Friday");
        dayId.add(FriId);
        startTimeId.add(spin_start5_index+"");
        startTime.add(spin_start5_text);
        endTimeId.add(spin_end5_index+"");
        endTime.add(spin_end5_text);

    }
    @Override
    public void actionDoneSix() {

        String temp_start = spin_start6_text.replace(":", ".");
        String temp_end = spin_end6_text.replace(":", ".");
        double start = Double.valueOf(temp_start);
        double end = Double.valueOf(temp_end);
        if ((end - start) >= 1)
        {
            ll_day6.setVisibility(View.VISIBLE);
            ll_time_done_six.setVisibility(View.VISIBLE);
            ll_edt_time_six.setVisibility(View.GONE);
            tv_saturday_time.setText(spin_start6_text+" - "+spin_end6_text);
        }
        else
        {
            Toast.makeText(getContext(), "End time start time should have a difference of 1 hour", Toast.LENGTH_SHORT).show();
        }

        dayName.add("Saturday");
        dayId.add(SaturId);
        startTimeId.add(spin_start6_index+"");
        startTime.add(spin_start6_text);
        endTimeId.add(spin_end6_index+"");
        endTime.add(spin_end6_text);
    }
    @Override
    public void actionDoneSeven() {

        String temp_start = spin_start7_text.replace(":", ".");
        String temp_end = spin_end7_text.replace(":", ".");
        double start = Double.valueOf(temp_start);
        double end = Double.valueOf(temp_end);
        if ((end - start) >= 1)
        {
            ll_day7.setVisibility(View.VISIBLE);
            ll_time_done_seven.setVisibility(View.VISIBLE);
            ll_edt_time_seven.setVisibility(View.GONE);
            tv_sunday_time.setText(spin_start7_text+" - "+spin_end7_text);
        }
        else
        {
            Toast.makeText(getContext(), "End time start time should have a difference of 1 hour", Toast.LENGTH_SHORT).show();
        }

        dayName.add("Sunday");
        dayId.add(SunId);
        startTimeId.add(spin_start7_index+"");
        startTime.add(spin_start7_text);
        endTimeId.add(spin_end7_index+"");
        endTime.add(spin_end7_text);

    }


    @Override
    public void actionChkMonday(boolean isChecked) {

        if(isChecked)
        {
            ll_disable_monday.setVisibility(View.GONE);
        }
        else
        {
            ll_disable_monday.setVisibility(View.VISIBLE);
            ll_time_done_one.setVisibility(View.VISIBLE);
            ll_edt_time_one.setVisibility(View.GONE);
            ll_day1.setVisibility(View.VISIBLE);


            for(int i=0;i<dayName.size();i++)
            {
                if(dayName.get(i).equals("Monday"))
                {
                    pos=i;
                }
            }

            Log.e("Hour",pos+"");
            if(pos!=10) {
                try {


                dayId.remove(pos);
                startTimeId.remove(pos);
                startTime.remove(pos);
                endTimeId.remove(pos);
                endTime.remove(pos);
                dayName.remove(pos);
                }catch(Exception e)
                {

                }
            }

        }





    }
    @Override
    public void actionChkTuesday(boolean isChecked) {
        if(isChecked)
        {
            ll_disable_tusday.setVisibility(View.GONE);
        }
        else
        {
            ll_disable_tusday.setVisibility(View.VISIBLE);

            ll_time_done_two.setVisibility(View.VISIBLE);
            ll_edt_time_two.setVisibility(View.GONE);
            ll_day2.setVisibility(View.VISIBLE);
            for(int i=0;i<dayName.size();i++)
            {
                if(dayName.get(i).equals("Tuesday"))
                {
                    Toast.makeText(getActivity(),"pos"+i,Toast.LENGTH_SHORT).show();
                    pos=i;
                }
            }

            if(pos!=10) {
                try {
                    dayId.remove(pos);
                    startTimeId.remove(pos);
                    startTime.remove(pos);
                    endTimeId.remove(pos);
                    endTime.remove(pos);
                    dayName.remove(pos);
                }catch(Exception e)
                {

                }

            }

        }
    }
    @Override
    public void actionChkWednesday(boolean isChecked) {
        if(isChecked)
        {
            ll_disable_wednesday.setVisibility(View.GONE);
        }
        else
        {
            ll_disable_wednesday.setVisibility(View.VISIBLE);
            ll_time_done_three.setVisibility(View.VISIBLE);
            ll_edt_time_three.setVisibility(View.GONE);
            ll_day3.setVisibility(View.VISIBLE);
            for(int i=0;i<dayName.size();i++)
            {
                if(dayName.get(i).equals("Wednesday"))
                {
                    pos=i;
                }
            }

            if(pos!=10) {
                try {
                dayId.remove(pos);
                startTimeId.remove(pos);
                startTime.remove(pos);
                endTimeId.remove(pos);
                endTime.remove(pos);
                dayName.remove(pos);
                }catch(Exception e)
                {

                }

            }

        }
    }
    @Override
    public void actionChkThusday(boolean isChecked) {
        if(isChecked)
        {
            ll_disable_thusday.setVisibility(View.GONE);
        }
        else
        {
            ll_disable_thusday.setVisibility(View.VISIBLE);
            ll_time_done_four.setVisibility(View.VISIBLE);
            ll_edt_time_four.setVisibility(View.GONE);
            ll_day4.setVisibility(View.VISIBLE);


            for(int i=0;i<dayName.size();i++)
            {
                if(dayName.get(i).equals("Thursday"))
                {
                    pos=i;
                }
            }

            if(pos!=10) {
                try {
                dayId.remove(pos);
                startTimeId.remove(pos);
                startTime.remove(pos);
                endTimeId.remove(pos);
                endTime.remove(pos);
                dayName.remove(pos);
                }catch(Exception e)
                {

                }
            }
        }
    }
    @Override
    public void actionChkFriday(boolean isChecked) {
        if(isChecked)
        {
            ll_disable_friday.setVisibility(View.GONE);
        }
        else
        {
            ll_disable_friday.setVisibility(View.VISIBLE);
            ll_time_done_five.setVisibility(View.VISIBLE);
            ll_edt_time_five.setVisibility(View.GONE);
            ll_day5.setVisibility(View.VISIBLE);


            for(int i=0;i<dayName.size();i++)
            {
                if(dayName.get(i).equals("Friday"))
                {
                    pos=i;
                }
            }

            if(pos!=10) {
                try {
                dayId.remove(pos);
                startTimeId.remove(pos);
                startTime.remove(pos);
                endTimeId.remove(pos);
                endTime.remove(pos);
                dayName.remove(pos);
                }catch(Exception e)
                {

                }
            }

        }

    }
    @Override
    public void actionChkSaturday(boolean isChecked) {
        if(isChecked)
        {
            ll_disable_saturday.setVisibility(View.GONE);

        }
        else
        {
            ll_disable_saturday.setVisibility(View.VISIBLE);
            ll_time_done_six.setVisibility(View.VISIBLE);
            ll_edt_time_six.setVisibility(View.GONE);
            ll_day6.setVisibility(View.VISIBLE);
            for(int i=0;i<dayName.size();i++)
            {
                if(dayName.get(i).equals("Saturday"))
                {
                    pos=i;
                }
            }

            if(pos!=10) {
                try {
                dayId.remove(pos);
                startTimeId.remove(pos);
                startTime.remove(pos);
                endTimeId.remove(pos);
                endTime.remove(pos);
                dayName.remove(pos);
                }catch(Exception e)
                {

                }
            }

        }

    }
    @Override
    public void actionChkSunday(boolean isChecked) {
        if(isChecked)
        {
            ll_disable_sunday.setVisibility(View.GONE);

        }
        else
        {
            ll_disable_sunday.setVisibility(View.VISIBLE);
            ll_time_done_seven.setVisibility(View.VISIBLE);
            ll_edt_time_seven.setVisibility(View.GONE);
            ll_day7.setVisibility(View.VISIBLE);

            for(int i=0;i<dayName.size();i++)
            {
                if(dayName.get(i).equals("Sunday"))
                {
                    pos=i;
                }
            }

            if(pos!=10) {
                try {
                dayId.remove(pos);
                startTimeId.remove(pos);
                startTime.remove(pos);
                endTimeId.remove(pos);
                endTime.remove(pos);
                dayName.remove(pos);
                }catch(Exception e)
                {

                }
            }

        }
    }



    @Override
    public void actionSpin1(int pos) {
        spin_start1_text =  time.get(pos);
        spin_start1_index= pos;


    }
    @Override
    public void actionSpinEnd1(int pos) {
        spin_end1_text =  time.get(pos);
        spin_end1_index= pos;
    }
    @Override
    public void actionSpin2(int pos) {
        spin_start2_text =  time.get(pos); spin_start2_index = pos;
     }
    @Override
    public void actionSpinEnd2(int pos) {
        spin_end2_text =  time.get(pos); spin_end2_index = pos;
    }
    @Override
    public void actionSpin3(int pos) {
        spin_start3_text =  time.get(pos); spin_start3_index = pos;
    }
    @Override
    public void actionSpinEnd3(int pos) {
        spin_end3_text =  time.get(pos); spin_end3_index = pos;
    }
    @Override
    public void actionSpin4(int pos) {
        spin_start4_text =  time.get(pos);   spin_start4_index = pos;
    }
    @Override
    public void actionSpinEnd4(int pos) {
        spin_end4_text =  time.get(pos); spin_end4_index = pos;

    }
    @Override
    public void actionSpin5(int pos) {
        spin_start5_text =  time.get(pos); spin_start5_index = pos;

    }
    @Override
    public void actionSpinEnd5(int pos) {
        spin_end5_text =  time.get(pos); spin_end5_index = pos;

    }
    @Override
    public void actionSpin6(int pos) {
        spin_start6_text =  time.get(pos); spin_start6_index = pos;
    }
    @Override
    public void actionSpinEnd6(int pos) {
        spin_end6_text =  time.get(pos);spin_end6_index = pos;
    }
    @Override
    public void actionSpin7(int pos) {
        spin_start7_text =  time.get(pos);spin_start7_index = pos;
    }
    @Override
    public void actionSpinEnd7(int pos) {
        spin_end7_text =  time.get(pos); spin_end7_index = pos;
    }
   // CheckBox chk_monday;chk_tusday; chk_wednesday;chk_thusday;chk_friday;chk_saturday;chk_sunday;
    public BusinessHoursDetaisResult getValueFromBusinessHours()
    {
        String temp_start1 = spin_start1_text.replace(":", ".");
        String temp_end1 = spin_end1_text.replace(":", ".");
        String mondayTiming = ""; String tuesdayTiming = ""; String wednesdayTiming = "";
        String thursdayTiming = ""; String fridayTiming = "";String saturdayTiming = "";
        String sundayTiming = "";
        boolean mondayFlag = false;boolean tuesdayFlag = false;  boolean wednesdayFlag = false;
        boolean thursdayFlag = false;  boolean fridayFlag = false;boolean saturdayFlag = false;
        boolean sundayFlag = false;

        boolean error = false; String error_text="";


        double start = Double.valueOf(temp_start1);
        double end = Double.valueOf(temp_end1);

        if(chk_monday.isChecked())
        {
            mondayFlag = true;
            if ((end - start) >= 1)
            {
                mondayTiming = tv_monday_time.getText().toString().trim();
            }
            else
            {
                error = true;
                error_text ="Monday End time start time should have a difference of 1 hour";
                mondayTiming = "nodata";
            }
        }
        else{
            mondayFlag = false;
            mondayTiming = "nodata";

        }

        //==========================================================================

        String temp_start2 = spin_start2_text.replace(":", ".");
        String temp_end2 = spin_end2_text.replace(":", ".");



         double start2 = Double.valueOf(temp_start2);
         double end2 = Double.valueOf(temp_end2);


         if(chk_tusday.isChecked())
         {
            tuesdayFlag = true;
            if ((end2 - start2) >= 1)
            {
                tuesdayTiming = tv_tuesday_time.getText().toString().trim();
            }
            else
            {
                error = true;
                error_text ="Tuesday End time start time should have a difference of 1 hour";
                tuesdayTiming = "nodata";
            }
         }
         else{
            tuesdayFlag = false;
            tuesdayTiming = "nodata";

         }

         //=======================================================================

        String temp_start3 = spin_start3_text.replace(":", ".");
        String temp_end3 = spin_end3_text.replace(":", ".");


        double start3 = Double.valueOf(temp_start3);
        double end3 = Double.valueOf(temp_end3);

        if(chk_wednesday.isChecked())
        {
            wednesdayFlag = true;
            if ((end3 - start3) >= 1)
            {
                wednesdayTiming = tv_wednesday_time.getText().toString().trim();
            }
            else
            {
                error = true;
                error_text ="Wednesday End time start time should have a difference of 1 hour";
                wednesdayTiming = "nodata";
            }
        }
        else{
            wednesdayFlag = false;
            wednesdayTiming = "nodata";

        }

        //=============================================================================

        String temp_start4 = spin_start4_text.replace(":", ".");
        String temp_end4 = spin_end4_text.replace(":", ".");

        double start4 = Double.valueOf(temp_start4);
        double end4 = Double.valueOf(temp_end4);


        if(chk_thusday.isChecked())
        {
            thursdayFlag = true;
            if ((end4 - start4) >= 1)
            {
                thursdayTiming = tv_thursday_time.getText().toString().trim();
            }
            else
            {
                error = true;
                error_text ="Thursday End time start time should have a difference of 1 hour";
                thursdayTiming = "nodata";
            }
        }
        else{
            thursdayFlag = false;
            thursdayTiming = "nodata";
        }

        //==========================================================

        String temp_start5 = spin_start5_text.replace(":", ".");
        String temp_end5 = spin_end5_text.replace(":", ".");

        double start5 = Double.valueOf(temp_start5);
        double end5 = Double.valueOf(temp_end5);

        if(chk_friday.isChecked())
        {
            fridayFlag = true;
            if ((end5 - start5) >= 1)
            {
                fridayTiming = tv_friday_time.getText().toString().trim();
            }
            else
            {
                error = true;
                error_text ="Friday End time start time should have a difference of 1 hour";
                fridayTiming = "nodata";
            }
        }
        else{
            fridayFlag = false;
            fridayTiming = "nodata";
        }


        //=========================================================================

        String temp_start6 = spin_start6_text.replace(":", ".");
        String temp_end6 = spin_end6_text.replace(":", ".");

        double start6 = Double.valueOf(temp_start6);
        double end6 = Double.valueOf(temp_end6);


        if(chk_saturday.isChecked())
        {
            saturdayFlag = true;
            if ((end6 - start6) >= 1)
            {
                saturdayTiming = tv_saturday_time.getText().toString().trim();
            }
            else
            {
                error = true;
                error_text ="Saturday End time start time should have a difference of 1 hour";
                saturdayTiming = "nodata";
            }
        }
        else{
            saturdayFlag = false;
            saturdayTiming = "nodata";
        }
        //==========================================================================


        String temp_start7 = spin_start7_text.replace(":", ".");
        String temp_end7 = spin_end7_text.replace(":", ".");

        double start7 = Double.valueOf(temp_start7);
        double end7 = Double.valueOf(temp_end7);

        if(chk_saturday.isChecked())
        {
            sundayFlag = true;
            if ((end7 - start7) >= 1)
            {
                sundayTiming = tv_sunday_time.getText().toString().trim();
            }
            else
            {
                error = true;
                error_text ="Sunday End time start time should have a difference of 1 hour";
                sundayTiming = "nodata";
            }
        }
        else{

            sundayFlag = false;
            sundayTiming = "nodata";
        }


        /*Toast.makeText(getContext(), error_text, Toast.LENGTH_SHORT).show();

        Toast.makeText(getContext(), mondayTiming, Toast.LENGTH_SHORT).show();
        Toast.makeText(getContext(), tuesdayTiming, Toast.LENGTH_SHORT).show();
        Toast.makeText(getContext(), wednesdayTiming, Toast.LENGTH_SHORT).show();
        Toast.makeText(getContext(), thursdayTiming, Toast.LENGTH_SHORT).show();
        Toast.makeText(getContext(), fridayTiming, Toast.LENGTH_SHORT).show();
        Toast.makeText(getContext(), saturdayTiming, Toast.LENGTH_SHORT).show();
        Toast.makeText(getContext(), sundayTiming, Toast.LENGTH_SHORT).show();*/


        BusinessHoursDetaisResult businessHoursDetaisResult = new BusinessHoursDetaisResult(error, error_text,
         mondayFlag,  mondayTiming,  tuesdayFlag,  tuesdayTiming,  wednesdayFlag,  wednesdayTiming,  thursdayFlag,
                 thursdayTiming,  fridayFlag,  fridayTiming,  saturdayFlag,  saturdayTiming,  sundayFlag,
                 sundayTiming) ;




       return  businessHoursDetaisResult;



    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
