package com.bidjobbers.bidjobberspro.features.OnTimeScreen.SocialArea;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class SocialFragmentPresenter<V extends SocialFragmentMvpView>
        extends BaseFragmentPresenter<V>
        implements  SocialFragmentMvpPresenter<V>  {

    @Inject
    public SocialFragmentPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }

    @Override
    public void ClickAddSocialLinkBtnOne() {

      getMvpView().resultAddSocialLinkOne();

    }

    @Override
    public void ClickAddSocialLinkBtnTwo() {
        getMvpView().resultAddSocialLinkTwo();
    }

    @Override
    public void ClickAddSocialLinkBtnThree() {
        getMvpView().resultAddSocialLinkThree();
    }

    @Override
    public void onGetDataFromApi() {

        Gson gson = new Gson();
        String json = getDataManager().getMyObject();
        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
        getMvpView().setDataFromApi(obj.socialMediaLink);
    }
}
