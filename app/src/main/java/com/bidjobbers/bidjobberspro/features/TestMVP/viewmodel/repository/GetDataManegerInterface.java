package com.bidjobbers.bidjobberspro.features.TestMVP.viewmodel.repository;

import com.bidjobbers.bidjobberspro.data.DataManager;

//@Component(modules = NetworkModule.class)
public interface GetDataManegerInterface {

    DataManager getDataManager();
}
