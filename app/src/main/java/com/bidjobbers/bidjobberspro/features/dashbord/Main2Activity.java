package com.bidjobbers.bidjobberspro.features.dashbord;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.DeviceToken.DeviceTokenResponse;
import com.bidjobbers.bidjobberspro.data.network.models.NotificationCount.NotificationCountResponse;
import com.bidjobbers.bidjobberspro.data.network.models.SetLocal.SetLocalResponse;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.features.InApp.InAppActivity;
import com.bidjobbers.bidjobberspro.features.dashbord.OnlyActivty.DashbordMvpView;
import com.bidjobbers.bidjobberspro.features.dashbord.OnlyActivty.DashbordPresenter;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.Contactus.ContactusFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.gallery.BlockCustomerFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.home.HomeFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.send.SettingsFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.share.TearmsandConditionFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.slideshow.NotificationFragmentFragment;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.features.prcdetails.PercDetailsUpdateActivity;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Main2Activity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, DashbordMvpView {




    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;
    @BindView(R.id.nav_view)
    NavigationView nav_view;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.llContainer)
    FrameLayout llContainer;
    @BindView(R.id.tvHeader)
    TextView tvHeader;
    @BindView(R.id.fl_notification)
     FrameLayout fl_notification;
    @BindView(R.id.tv_count)
            TextView tv_count;

    ImageView ivProfile;

    TextView tvName;

    TextView ivEmail;

    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    Fragment fragment = null;

    boolean doubleBackToExitPressedOnce = false;
    Context mContext;
    String device_token,Language;
    SharedPreferences prefs;
    SharedPreferences mBank;
    @Inject
    DashbordPresenter<DashbordMvpView> dashbordPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mContext = this;
        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        dashbordPresenter.onAttach(this);

        prefs = PreferenceManager.getDefaultSharedPreferences(Main2Activity.this);
        Language=prefs.getString("LOCAL_KEY","");
        Log.e("Local",Language);
        mBank=getSharedPreferences("Bidjobbers_Bank", Context.MODE_PRIVATE);
        setUi();
        tvHeader.setText(getResources().getString(R.string.menue_task));
        replaceFragment(new HomeFragment());

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( Main2Activity .this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                device_token = instanceIdResult.getToken();
                Log.e("Token",device_token);

                dashbordPresenter.onUpdateDeviceToken("A",device_token);

            }
        });
        dashbordPresenter.onGetNotificationCount();
        dashbordPresenter.getSetLocal("");

    }


    @Override
    protected void onResume() {
        super.onResume();
         prefs = PreferenceManager.getDefaultSharedPreferences(Main2Activity.this);
        Language=prefs.getString("LOCAL_KEY","");
        Log.e("Local",Language);
        dashbordPresenter.onSuccessfullGetProfile(Language);
        dashbordPresenter.onGetNotificationCount();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
     //   getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }



    private void setUi()
    {

//       // getSupportActionBar().setTitle();
//        toolbar.setTitleTextColor(101010);
//        toolbar.setTitleMarginStart(60);
//        toolbar.setTitle("Jobs");
        setSupportActionBar(toolbar);
        tvHeader.setText("Jobs");
       // getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#070707\"; align=\"center\">" +"Jobs"+ "</font>")));
       // getSupportActionBar().setTitle((Html.fromHtml("<h1 style=\"color:black;text-align:center\">Jobs</h1>")));
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer_layout, toolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close );
        drawer_layout.addDrawerListener(toggle);
        toggle.syncState();
        toolbar.setNavigationIcon(R.drawable.ic_hamburger);

        nav_view.setNavigationItemSelectedListener(this);


        View headerView = nav_view.getHeaderView(0);
        tvName = (TextView) headerView.findViewById(R.id.tvName);
        ivEmail=(TextView)headerView.findViewById(R.id.ivEmail);
        ivProfile=(ImageView)headerView.findViewById(R.id.ivProfile);

        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.closeDrawer(GravityCompat.START);
                startActivity(new Intent(getApplicationContext(), PercDetailsUpdateActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

        fl_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvHeader.setText("Notifications");
                fragment=new NotificationFragmentFragment();
                Bundle bundleThree = new Bundle();
                bundleThree.putString("Sug", "contact-us");
                fragment.setArguments(bundleThree);
                replaceFragment(fragment);
            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {

            case R.id.action_logout:

                dashbordPresenter.onLogoutClick();
                break;

            case R.id.action_settings:
               // getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#070707\"; align=\"center\">" +"Settings"+ "</font>")));
                tvHeader.setText(getResources().getString(R.string.menue_prof));
                fragment=new SettingsFragment();
                replaceFragment(fragment);
                break;
            case R.id.action_task:
               // getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#070707\"; align=\"center\">" +"Jobs"+ "</font>")));
                tvHeader.setText(getResources().getString(R.string.menue_task));
                fragment=new HomeFragment();
                replaceFragment(fragment);
                break;
            case R.id.action_blockcustomer:
                // getSupportActionBar().setTitle((Html.fromHtml("<font color=\"#070707\"; align=\"center\">" +"Jobs"+ "</font>")));
                tvHeader.setText("Block Customers");
                fragment=new BlockCustomerFragment();
                replaceFragment(fragment);
                break;
            case R.id.action_tearm_condition:
                tvHeader.setText(getResources().getString(R.string.menue_terms));
                fragment=new TearmsandConditionFragment();
                Bundle bundle = new Bundle();
                bundle.putString("Sug", "terms-and-condition");
                fragment.setArguments(bundle);
                replaceFragment(fragment);
                break;
            case R.id.action_privacy:
                tvHeader.setText(getResources().getString(R.string.menue_pp));
                fragment=new TearmsandConditionFragment();
                Bundle bundleOne = new Bundle();
                bundleOne.putString("Sug", "privacy-policy");
                fragment.setArguments(bundleOne);
                replaceFragment(fragment);
                break;
            case R.id.action_contactus:
                tvHeader.setText(getResources().getString(R.string.menue_cu));
                fragment=new ContactusFragment();
                Bundle bundleTwo = new Bundle();
                bundleTwo.putString("Sug", "contact-us");
                fragment.setArguments(bundleTwo);
                replaceFragment(fragment);
                break;
            case R.id.action_inapp:
                Intent inapp=new Intent(getApplicationContext(), InAppActivity.class);
                startActivity(inapp);
                break;

            case R.id.action_notification:
               // showMessage("Under development");
                tvHeader.setText(getResources().getString(R.string.menue_noti));
                fragment=new NotificationFragmentFragment();
                Bundle bundleThree = new Bundle();
                bundleThree.putString("Sug", "contact-us");
                fragment.setArguments(bundleThree);
                replaceFragment(fragment);
                break;


            default:
                showMessage("Under development");

        }

        drawer_layout.closeDrawer(GravityCompat.START);
        return true;
    }

    void replaceFragment(Fragment fragment)
    {
        if (fragment != null) {
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.replace(R.id.llContainer, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    public void onDeactivate(String data) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(Main2Activity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, 800);
    }

    @Override
    public void successfullySEtLocal(SetLocalResponse setLocalResponse) {

    }

    @Override
    public void successfullyGetProfile(getProfileResponse getProfileResponse) {
        if(getProfileResponse.getResponseCode()==1)
        {

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.ic_user);
            requestOptions.error(R.drawable.ic_user);
            Glide.with(mContext)
                    .load(getProfileResponse.getResponseData().getProfile_image())
                    .apply(requestOptions)
                    .into(ivProfile);

            ivEmail.setText(getProfileResponse.getResponseData().getEmail());
            tvName.setText(getProfileResponse.getResponseData().getName());

        }
    }

    @Override
    public void successfullyLogout() {
        Intent i=new Intent(Main2Activity.this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onSuccessfullyGetNotificationCount(NotificationCountResponse notificationCountResponse) {

        if(notificationCountResponse.getResponseCode()==1)
        {
            tv_count.setText(notificationCountResponse.getResponseData().getNotificationCount());

            if(notificationCountResponse.getResponseData().getHasTransAccDetails().equals("1")) {
                SharedPreferences.Editor myEdit
                        = mBank.edit();

                myEdit.putString("BankInfo", "yes");
                myEdit.apply();
                myEdit.commit();
            }
            else
            {
                SharedPreferences.Editor myEdit
                        = mBank.edit();

                myEdit.putString("BankInfo", "no");
                myEdit.apply();
                myEdit.commit();
            }

        }
        else
        {
            tv_count.setText("0");
            SharedPreferences.Editor myEdit
                    = mBank.edit();

            myEdit.putString("BankInfo", "no");
            myEdit.apply();
            myEdit.commit();

        }
    }

    @Override
    public void onSuccessfullyUpdateToken(DeviceTokenResponse response) {

    }


    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
