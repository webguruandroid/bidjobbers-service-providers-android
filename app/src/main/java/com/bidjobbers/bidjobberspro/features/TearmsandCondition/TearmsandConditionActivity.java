package com.bidjobbers.bidjobberspro.features.TearmsandCondition;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.Allpages.AllPagesResponse;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TearmsandConditionActivity extends BaseActivity implements TearmsandConditionMvpView {

    @BindView(R.id.tvContent)
    TextView tvContent;
    @BindView(R.id.tvHeader)
    TextView tvHeader;
    @BindView(R.id.iv_mnu)
    ImageView iv_mnu;
    @BindView(R.id.webView)
    WebView webView;

    Context mContext;
    @Inject
    TearmsandConditionPresenter<TearmsandConditionMvpView> allpresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tearmsand_condition);
        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        tvHeader.setText(getResources().getString(R.string.menue_terms));
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        //webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        // webView.setWebViewClient(new MyBrowser());
        webView.getSettings().setDefaultFontSize((int) getResources().getDimension(R.dimen._11sdp));

        allpresenter.onAttach(this);
        allpresenter.getAllPages("terms-and-condition");


        iv_mnu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void allTasksName(AllPagesResponse response) {
        if(response.getResponseCode()==1)
        {
            webView.loadDataWithBaseURL(null,response.getResponseData().getPage_content(),"text/html", "utf-8", null);
           // String strArray = response.getResponseData().getPage_content().replace("\n", "<br>");;

           // tvContent.setText(Html.fromHtml(strArray));
        }
    }

    @Override
    public void onDeactive(String data) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(TearmsandConditionActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, 800);
    }
}
