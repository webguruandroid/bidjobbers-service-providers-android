package com.bidjobbers.bidjobberspro.features.OnTimeScreen.TypeOfPropertyWorkOn;

import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface TypeOfPropertyWorkOnFragmentMvpView extends BaseFragmentMvpView {
    public void actionYesHaveALicence();
    public void actionNoHaveALicence();
    void  getDataFromSP(boolean isResidential,String residentialId,boolean isCommertial,String commertialId,boolean hasLicence,String licenceNo);

    void setDataFromApi(DataModelForFirstTimeQusAns.TypeOfPropertyWorkOn typeOfPropertyWorkOn);


}
