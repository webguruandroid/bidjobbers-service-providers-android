package com.bidjobbers.bidjobberspro.features.splash;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.features.CustomerDetails.CustomerDetailsActivity;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.FirstTimeAccountSettingsActivity;
import com.bidjobbers.bidjobberspro.features.dashbord.Main2Activity;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.features.select_language.SelectLanguageActivity;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;
import com.google.gson.Gson;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class SplashActivity extends BaseActivity implements SplashMvpView {

    SharedPreferences mPrefs;

    @Inject
    SplashPresenter<SplashMvpView> splashPresenter;

    String currentLanguage = "",Language;
    Locale myLocale;
    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        splashPresenter.onAttach(this);


         mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        Language=mPrefs.getString("LOCAL_KEY","");
        Log.e("Local",Language);

        Gson gson = new Gson();
        String json = mPrefs.getString("MyObject", "");
        if(getIntent().getExtras()!=null) {

            if (getIntent().getExtras().getString("status_id") != null) {
                Log.e("enter", getIntent().getExtras().getString("status_id") + "");

                Intent intent = new Intent(this, CustomerDetailsActivity.class);
                intent.putExtra("status", getIntent().getExtras().getString("status_id"));
                intent.putExtra("taskId", getIntent().getExtras().getString("task_id"));
                intent.putExtra("bidId", "");
                intent.putExtra("Tag","Background");
                startActivity(intent);
                finish();
            }
            else
            {
                setLocale(Language);
            }
        }
        else
        {
            setLocale(Language);
        }


        if(json.trim().equals(""))
        {
            testForDataModelForBlankData();

        }
        else
        {

        }


    }



    public void setLocale(String localeName) {

        myLocale = new Locale(localeName);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();

        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    if(!mPrefs.getString("LOCAL_KEY","").equals("")) {

                        Log.e("dataJit","1");
//
//                        Intent i = new Intent(SplashActivity.this, LoginActivity.class);
//                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(i);
//                        finish();

                        splashPresenter.onNavigateToLogin();
                    }
                    else
                    {

                        Log.e("dataJit","2");

                        Intent i = new Intent(SplashActivity.this, SelectLanguageActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.putExtra("tag","Insert");
                        startActivity(i);
                        finish();
                    }
                }
            }, 2000);
//
//        } else {
//            Toast.makeText(this, "Language already selected!", Toast.LENGTH_SHORT).show();
//        }
    }
    public void testForDataModelForBlankData() {
        DataModelForFirstTimeQusAns dataModelForFirstTimeQusAns =
                new DataModelForFirstTimeQusAns();


        DataModelForFirstTimeQusAns.KindOfServiceDoProvide kindOfServiceDoProvide
                = new DataModelForFirstTimeQusAns.KindOfServiceDoProvide();

        kindOfServiceDoProvide.setLawnServiceId("");
        kindOfServiceDoProvide.setLawnServiceSelected(false);
        kindOfServiceDoProvide.setMowALownId("");
        kindOfServiceDoProvide.setIsmowALownSelected(false);
        kindOfServiceDoProvide.setAreateALawnId("");
        kindOfServiceDoProvide.setAreateALawn(false);
        kindOfServiceDoProvide.setYardWasteId("");
        kindOfServiceDoProvide.setIsyardWaste(false);
        kindOfServiceDoProvide.setTreeCuttingId("");
        kindOfServiceDoProvide.setTreeCuttingSelected(false);
        kindOfServiceDoProvide.setTreeStrubsId("");
        kindOfServiceDoProvide.setTreeStrubsSelected(false);
        kindOfServiceDoProvide.setTreeStrumpsId("");
        kindOfServiceDoProvide.setTreeStrumps(false);
        kindOfServiceDoProvide.setYardWasteTreeCuttingId("");
        kindOfServiceDoProvide.setIsyardWasteTreeCutting(false);


        dataModelForFirstTimeQusAns.setKindOfService(kindOfServiceDoProvide);


        DataModelForFirstTimeQusAns.BusinessInfo businessInfo
                = new DataModelForFirstTimeQusAns.BusinessInfo();

        businessInfo.setCompanyName("");
        businessInfo.setYearFounded("");
        businessInfo.setNoOfEmployee("");
        businessInfo.setCompRegisterNumber("");


        dataModelForFirstTimeQusAns.setBusinessInfo(businessInfo);


        DataModelForFirstTimeQusAns.BusinessLocation businessLocation
                = new DataModelForFirstTimeQusAns.BusinessLocation();

        businessLocation.setBusinessAddress("");
        businessLocation.setPinCode("");


        dataModelForFirstTimeQusAns.setBusinessLocation(businessLocation);


        DataModelForFirstTimeQusAns.IntroduceYourBusiness introduceYourBusiness
                = new DataModelForFirstTimeQusAns.IntroduceYourBusiness();

        introduceYourBusiness.setDetails("");


        dataModelForFirstTimeQusAns.setIntroduceYourBusiness(introduceYourBusiness);


        DataModelForFirstTimeQusAns.BusinessHours businessHours
                = new DataModelForFirstTimeQusAns.BusinessHours();

        businessHours.setMondayId("");

        businessHours.setMondayFromTime("");
        businessHours.setMondayToTime("");
        businessHours.setMondaySelected(false);


        businessHours.setTuesdayId("");

        businessHours.setTuesdayFromTime("");
        businessHours.setTuesdayToTime("");
        businessHours.setTuesdaySelected(false);


        businessHours.setWednesdayId("");
        businessHours.setWednesdayFromTime("");
        businessHours.setWednesdayToTime("");
        businessHours.setWednesdaySelected(false);

        businessHours.setThursdayId("");
        businessHours.setThursdayFromTime("");
        businessHours.setThursdaySelected(false);

        businessHours.setFridayId("");
        businessHours.setFridayFromTime("");
        businessHours.setFridayToTime("");
        businessHours.setFridaySelected(false);

        businessHours.setSaturdayId("");
        businessHours.setSaturdayFromTime("");
        businessHours.setSaturdayToTime("");
        businessHours.setSaturdaySelected(false);

        businessHours.setSundayId("");
        businessHours.setSundayFromTime("");
        businessHours.setSundayToTime("");
        businessHours.setSundaySelected(false);


        dataModelForFirstTimeQusAns.setBusinessHours(businessHours);



        DataModelForFirstTimeQusAns.BankDetails bankDetails
                = new DataModelForFirstTimeQusAns.BankDetails();
        bankDetails.setPaypal_id("");
        bankDetails.setPaypal_currency("");
        bankDetails.setBank_routing_number("");
        bankDetails.setBank_name("");
        bankDetails.setBank_iban("");
        bankDetails.setBank_country("");
        bankDetails.setBank_currency("");
        bankDetails.setBank_account_number("");
        bankDetails.setTransaction_account_id("");
        bankDetails.setBank_account_holder_name("");
        bankDetails.setBank_swift("");


        dataModelForFirstTimeQusAns.setBankDetails(bankDetails);


        DataModelForFirstTimeQusAns.AdvanceTimeToBookAJob advanceTimeToBookAJob
                = new DataModelForFirstTimeQusAns.AdvanceTimeToBookAJob();




        dataModelForFirstTimeQusAns.setAdvanceTimeToBookAJob(advanceTimeToBookAJob);


        DataModelForFirstTimeQusAns.TypeOfPropertyWorkOn typeOfPropertyWorkOn
                = new DataModelForFirstTimeQusAns.TypeOfPropertyWorkOn();

        typeOfPropertyWorkOn.setResidentialId("");
        typeOfPropertyWorkOn.setResidential(false);
        typeOfPropertyWorkOn.setCommercialId("");
        typeOfPropertyWorkOn.setCommercial(false);

        dataModelForFirstTimeQusAns.setTypeOfPropertyWorkOn(typeOfPropertyWorkOn);


        DataModelForFirstTimeQusAns.BusinessPriceLawnSerce businessPriceLawnSerce
                = new DataModelForFirstTimeQusAns.BusinessPriceLawnSerce();

//        businessPriceLawnSerce.set("");
//        businessPriceLawnSerce.setSmaillPrice("");
//        businessPriceLawnSerce.setMedId("");
//        businessPriceLawnSerce.setMedPrice("");
//        businessPriceLawnSerce.setLargeId("");
//        businessPriceLawnSerce.setLargePrice("");
//        businessPriceLawnSerce.setVryLargeId("");
//        businessPriceLawnSerce.setVryLargePrice("");
//        businessPriceLawnSerce.setUnSureId("");
//        businessPriceLawnSerce.setUnSurePrice("");


        dataModelForFirstTimeQusAns.setBusinessPriceLawnSerce(businessPriceLawnSerce);


        DataModelForFirstTimeQusAns.PhotoDescribeProject photoDescribeProject
                = new DataModelForFirstTimeQusAns.PhotoDescribeProject();

        photoDescribeProject.setImageOne("");
        photoDescribeProject.setImageTwo("");
        photoDescribeProject.setImageThree("");
        photoDescribeProject.setImageFour("");
        photoDescribeProject.setImageFive("");

        dataModelForFirstTimeQusAns.setPhotoDescribeProject(photoDescribeProject);

        DataModelForFirstTimeQusAns.SocialMediaLink socialMediaLink=new DataModelForFirstTimeQusAns.SocialMediaLink();

        socialMediaLink.setSm_one("");
        socialMediaLink.setSm_two("");
        socialMediaLink.setSm_three("");
        socialMediaLink.setSm_four("");
        socialMediaLink.setWebsite_Url("");
        dataModelForFirstTimeQusAns.setSocialMediaLink(socialMediaLink);
        Log.e("social","-"+socialMediaLink.toString());



        Gson gson = new Gson();
        String json = gson.toJson(dataModelForFirstTimeQusAns);

        Log.e("json:--", "json--" + json);

        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        mPrefs.edit().putString("MyObject", json.toString()).commit();
        mPrefs.edit().putString("LOCAL_KEY", "").commit();


        //obj
        //To fetch data

       /* SharedPreferences mPrefss = PreferenceManager.getDefaultSharedPreferences(this);
        Gson gsonn = new Gson();
        String jsonn = mPrefss.getString("MyObject", "");
        DataModelForFirstTimeQusAns obj = gsonn.fromJson(jsonn, DataModelForFirstTimeQusAns.class);
        Log.e("TESTJIT",obj.g)*/


    }


    @Override
    public void onSuccessfullyDecideNavigation(String flag) {
      //  Toast.makeText(getApplicationContext(),flag,Toast.LENGTH_SHORT).show();
        if(flag.equals("Main"))
        {
            Intent i = new Intent(SplashActivity.this, Main2Activity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra("tag","Insert");
            startActivity(i);
            finish();
        }
        else if(flag.equals("Login"))
        {
            Intent i = new Intent(SplashActivity.this, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra("tag","Insert");
            startActivity(i);
            finish();
        }
        else if(flag.equals("Verify"))
        {

            Intent intentHome = new Intent(SplashActivity.this, LoginActivity.class);
            intentHome.putExtra("emailAddress",dataManager.getCurrentUserEmail());
            intentHome.putExtra("otp_for","EVAR");
            startActivity(intentHome);
            finish();
        }
        else if(flag.equals("FirstTime"))
        {

            Intent i = new Intent(SplashActivity.this, FirstTimeAccountSettingsActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            i.putExtra("tag","Insert");
            startActivity(i);
            finish();
        }

    }
}
