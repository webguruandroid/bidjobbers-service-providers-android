package com.bidjobbers.bidjobberspro.features.Map;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class MapActivity extends BaseActivity implements   MapMvpView,
    OnMapReadyCallback, LocationListener, GoogleMap.OnCameraMoveStartedListener,
    GoogleMap.OnCameraMoveListener,
    GoogleMap.OnCameraMoveCanceledListener,
    GoogleMap.OnCameraIdleListener,
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener,
    EasyPermissions.PermissionCallbacks, View.OnClickListener {



    public static final int RC_LOC_PERM = 101;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final int REQUEST_CHECK_SETTINGS = 1452;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    Context mContext;
    Marker mCurrLocationMarker;
    SupportMapFragment mapFragment;
    Bundle bundle=null;
    String search="1",Latitude,Longitude;
    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.btnContinue)
    Button btnContinue;
    @BindView(R.id.tvHeader)
    TextView tvHeader;
    String addresss ,city, state, country, knownName ,postalCode,latitudeCenter,longitudeCenter;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        bundle=getIntent().getExtras();
        if (bundle!=null){



                btnContinue.setVisibility(View.VISIBLE);
                etSearch.setVisibility(View.VISIBLE);
                tvHeader.setText("Find Address");

        }
        mContext = this;
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Places.initialize(getApplicationContext(), getString(R.string.map_key));
        setUp();





    }

    private void setUp()
    {

        buildGoogleApiClient();

        loadWithLocationTask();

    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                //finish();
            }
            return false;
        }
        return true;
    }



    public void settingsrequest() {
        Log.e(TAG, "settingsrequest ");
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000);
        locationRequest.setFastestInterval(3 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        Log.e(TAG, "onsetiing Result success: ");
                        startLocationUpdates();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(MapActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        Log.e(TAG, "onResult: " + " SETTINGS_CHANGE_UNAVAILABLE");
                        break;
                }
            }
        });
    }

    public void startLocationUpdates() {
        Log.e(TAG, "Onmapready-0");
        if (ActivityCompat.checkSelfPermission(MapActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MapActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "Onmapready-1");
             Toast.makeText(mContext,"No location permission",Toast.LENGTH_SHORT).show();
            return;
        }
        Log.e(TAG, "Onmapready-2");
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLastLocation != null) {
            Log.e(TAG, "startLocationUpdates: ");
            changeMapToCentre(mLastLocation);
            return;
        }
        else
        {
            Log.e(TAG, "startLocationUpdates: NULL");
        }

        try {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10 * 1000);
            mLocationRequest.setFastestInterval(5 * 1000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @AfterPermissionGranted(RC_LOC_PERM)
    public void loadWithLocationTask() {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

            if (checkPlayServices()) {
                  Toast.makeText(mContext, "Google play services  found", Toast.LENGTH_SHORT).show();
                settingsrequest();
            } else {
                Toast.makeText(mContext, "Google play services not found", Toast.LENGTH_SHORT).show();
                finish();
            }

        } else {
            // Ask for one permission
            EasyPermissions.requestPermissions(this, "You need to enable location permission to use this app",
                    RC_LOC_PERM, Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "onConnectionFailed: ");
    }



    public void findPlace() {
        List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG, Place.Field.ADDRESS_COMPONENTS);
        Intent autocompleteIntent =
                new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                        .build(this);
        startActivityForResult(autocompleteIntent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

    }


    @Override
    public void onCameraIdle() {


        try {
            CameraPosition camPosition = mMap.getCameraPosition();


            Location mLocation = new Location("");
            mLocation.setLatitude(camPosition.target.latitude);
            mLocation.setLongitude(camPosition.target.longitude);
            String address = "";
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(mLocation.getLatitude(),mLocation.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                 addresss = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                 city = addresses.get(0).getLocality();
                 state = addresses.get(0).getAdminArea();
                 country = addresses.get(0).getCountryName();
                 postalCode = addresses.get(0).getPostalCode();
                 knownName = addresses.get(0).getFeatureName();
                if (postalCode == null)
                {
                    address = addresss + "," + city + "," + state + "," + country + "," + "";
                }
                else
                {
                    address = addresss + "," + city + "," + state + "," + country + "," + postalCode;
                    Log.e(TAG, address+"--"+postalCode);
                }
                etSearch.setText(address);
            } catch (IOException e) {
                e.printStackTrace();
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onCameraMoveCanceled() {

    }

    @Override
    public void onCameraMove() {

    }

    @Override
    public void onCameraMoveStarted(int i) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;


        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveStartedListener(this);
        mMap.setOnCameraMoveListener(this);
        mMap.setOnCameraMoveCanceledListener(this);
        mMap.setPadding(0,150,10,0);




    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            if (location != null)
                Log.e(TAG, "onLocationChanged: ");
            changeMapToCentre(location);
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void changeMapToCentre(Location location) {
        Log.e(TAG, "changing map" + location.getLatitude() + "lng" + location.getLongitude());
        // Toast.makeText(mContext,"inside onloationchange",Toast.LENGTH_LONG).show();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "changing map" + "no permission");
            return;
        }
        mMap.clear();


        latitudeCenter=location.getLatitude()+"";
        longitudeCenter=location.getLongitude()+"";

        // check if map is created successfully or not
        if (mMap != null) {
            //map.getUiSettings().setZoomControlsEnabled(false);

            try {
                if (search.equals("0")){
                    mMap.setMyLocationEnabled(false);
                }
                else {
                    mMap.setMyLocationEnabled(true);
                }

                mMap.getUiSettings().setZoomControlsEnabled(false);
                mMap.getUiSettings().setCompassEnabled(true);
                mMap.getUiSettings().setZoomGesturesEnabled(true);
                mMap.getUiSettings().setMapToolbarEnabled(false);

            } catch (SecurityException e) {
                e.printStackTrace();
            }

            LatLng latLong;
            latLong = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLong, 16));



            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLong);
            markerOptions.title("Current Position");

            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));


         //   mCurrLocationMarker = mMap.addMarker(markerOptions);

            //move map camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLong));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(16));
            String address = "";
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(location.getLatitude(),location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                String addresss = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                if (postalCode == null)
                {
                    address = addresss + "," + city + "," + state + "," + country + "," + "";
                }
                else
                {
                    address = addresss + "," + city + "," + state + "," + country + "," + postalCode;
                    Log.e(TAG, address+"--"+postalCode);
                }
                etSearch.setText(address);
            } catch (IOException e) {
                e.printStackTrace();
            }




        } else {
            Log.e(TAG, "No Map is there");
        }



        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                changeMapToCentre(location);
                return false;
            }
        });
    }



    @Override
    protected void onStart() {
        super.onStart();

        try {
            mGoogleApiClient.connect();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        loadWithLocationTask();
    }


    @Override
    protected void onStop() {
        //Toast.makeText(mContext, "onStop", Toast.LENGTH_SHORT).show();
        Log.e(TAG, "onStop: ");
        try {
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        super.onStop();
        /*if((getIntent().getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY)!= 0)
        {
            finishAffinity();
        }*/
    }

    @OnClick({R.id.iv_back,R.id.etSearch,R.id.btnContinue})
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.iv_back:
                finish();
                break;
            case R.id.etSearch:
                findPlace();
                break;
            case R.id.btnContinue:
                if(!etSearch.getText().toString().equals("")) {
                    String address = "",finaladdress="";
                    Intent returnIntent = new Intent();
                    if (postalCode == null)
                    {
                        address = addresss + "-" + city + "-" + state + "-" + country + "-" + "";
                        Log.e(TAG, address+"--"+postalCode);
                    }
                    else
                    {
                        address = addresss + "-" + city + "-" + state + "-" + country + "-" + postalCode;
                        Log.e(TAG, address+"--"+postalCode);
                    }
                    finaladdress=address.replace("null","");
                    returnIntent.putExtra("result", finaladdress);
                    returnIntent.putExtra("latitude", latitudeCenter);
                    returnIntent.putExtra("longitude", longitudeCenter);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"Please Enter Location",Toast.LENGTH_SHORT).show();
                }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                Place place = Autocomplete.getPlaceFromIntent(data);
                String address = "";

                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(place.getLatLng().latitude, place.getLatLng().longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    addresss = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    city = addresses.get(0).getLocality();
                    state = addresses.get(0).getAdminArea();
                    country = addresses.get(0).getCountryName();
                    postalCode = addresses.get(0).getPostalCode();
                    knownName = addresses.get(0).getFeatureName();
                    if (postalCode == null)
                    {
                        address = addresss + "," + city + "," + state + "," + country + "," + "";
                        Log.e(TAG, address+"--");
                    }
                    else
                    {
                        address = addresss + "," + city + "," + state + "," + country + "," + postalCode;
                        //address=addresss.replace("null","");
                        Log.e(TAG, address+"--");
                    }


                    etSearch.setText(address);
                } catch (IOException e) {
                    e.printStackTrace();
                }


                Location location = new Location("");
                location.setLatitude(place.getLatLng().latitude);
                location.setLongitude(place.getLatLng().longitude);

                changeMapToCentre(location);


            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                showMessage("Some error occoured");
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.e(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {

            }
        }
    }

}
