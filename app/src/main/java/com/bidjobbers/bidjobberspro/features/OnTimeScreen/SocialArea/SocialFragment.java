package com.bidjobbers.bidjobberspro.features.OnTimeScreen.SocialArea;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the

 * to handle interaction events.
 * Use the {@link SocialFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SocialFragment extends BaseFragment implements SocialFragmentMvpView {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @BindView(R.id.ll_forth)
    LinearLayout ll_forth;
    @BindView(R.id.ll_third)
    LinearLayout ll_third;
    @BindView(R.id.ll_second)
    LinearLayout ll_second;
    @BindView(R.id.iv_open_second_field)
    ImageView iv_open_second_field;
    @BindView(R.id.iv_open_third_field)
    ImageView iv_open_third_field;
    @BindView(R.id.iv_open_forth_field)
    ImageView iv_open_forth_field;

    @BindView(R.id.edt_social_one)
    EditText edt_social_one;
    @BindView(R.id.edt_social_two)
    EditText edt_social_two;
    @BindView(R.id.edt_social_three)
    EditText edt_social_three;
    @BindView(R.id.edt_social_four)
    EditText edt_social_four;
    @BindView(R.id.edt_website_url)
    EditText edt_website_url;
    @BindView(R.id.edt_social_five)
    EditText edt_social_five;

    //   edt_social_one , edt_social_two , edt_social_three , edt_social_four , edt_website_url
    Context mContext;
    ArrayList<String>socialArray=new ArrayList<>();

    @Inject
    SocialFragmentPresenter<SocialFragmentMvpView> socialFragmentPresenter;

    public SocialFragment() {

    }

    // TODO: Rename and change types and number of parameters
    public static SocialFragment newInstance(String param1, String param2) {
        SocialFragment fragment = new SocialFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {

            String str = bundle.getString("FROMG");
            Log.e("FROMG", str);

        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = getContext();
    }

    @OnClick
    ({ R.id.iv_open_second_field,R.id.iv_open_third_field,R.id.iv_open_forth_field })
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.iv_open_second_field :
                socialFragmentPresenter.ClickAddSocialLinkBtnOne();
                break;
            case R.id.iv_open_third_field :
                socialFragmentPresenter.ClickAddSocialLinkBtnTwo();
                break;
            case R.id.iv_open_forth_field :
                socialFragmentPresenter.ClickAddSocialLinkBtnThree();
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v= inflater.inflate(R.layout.fragment_social, container, false);
        setUnBinder(ButterKnife.bind(this,v));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }
        socialFragmentPresenter.onAttach(this);
        socialFragmentPresenter.onGetDataFromApi();
        Log.e("social","true");
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void onDetach() {
        super.onDetach();
        socialFragmentPresenter.onDetach();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        socialFragmentPresenter.onDetach();
    }

    @Override
    public void resultAddSocialLinkOne() {
        ll_second.setVisibility(View.VISIBLE);
        iv_open_second_field.setVisibility(View.GONE);
    }

    @Override
    public void resultAddSocialLinkTwo() {
        ll_third.setVisibility(View.VISIBLE);
        iv_open_third_field.setVisibility(View.GONE);
    }

    @Override
    public void resultAddSocialLinkThree() {
        ll_forth.setVisibility(View.VISIBLE);
        iv_open_forth_field.setVisibility(View.GONE);
    }

    @Override
    public void setDataFromApi(DataModelForFirstTimeQusAns.SocialMediaLink socialMediaLink) {

        Log.e("social",socialMediaLink.toString());

        if(socialMediaLink!=null){
     //   edt_social_one.setText(socialMediaLink.getSm_one());
     //   edt_social_two.setText(socialMediaLink.getSm_two());
    //    edt_social_three.setText(socialMediaLink.getSm_three());
    //    edt_social_four.setText(socialMediaLink.getSm_four());
    //    edt_social_five.setText(socialMediaLink.getSm_five());
     //   edt_website_url.setText(socialMediaLink.getWebsite_Url());
//
//        if(!socialMediaLink.getSm_two().equals(""))
//        {
//            socialFragmentPresenter.ClickAddSocialLinkBtnOne();
//        }
//        if(!socialMediaLink.getSm_three().equals(""))
//        {
//            socialFragmentPresenter.ClickAddSocialLinkBtnTwo();
//        }
//        if(!socialMediaLink.getSm_four().equals(""))
//        {
//            socialFragmentPresenter.ClickAddSocialLinkBtnThree();
//        }

        }
        else
        {
           // Toast.makeText(getActivity(),"",Toast.LENGTH_SHORT).show();
        }

    }



    public SocialFragmentObject setDataSocialFragment()
    {
        socialArray=new ArrayList<>();
        Log.e("social",edt_social_one.getText().toString().trim()+edt_social_two.getText().toString().trim());

        if(!edt_social_one.getText().toString().trim().equals(""))
        {
            socialArray.add(edt_social_one.getText().toString().trim());
        }
         if(!edt_social_two.getText().toString().equals(""))
        {
            socialArray.add(edt_social_two.getText().toString().trim());
        }
         if(!edt_social_three.getText().toString().equals(""))
        {
            socialArray.add(edt_social_three.getText().toString().trim());
        }
         if(!edt_social_four.getText().toString().equals(""))
        {
            socialArray.add(edt_social_four.getText().toString().trim());
        }
         if(!edt_social_five.getText().toString().equals(""))
         {
             socialArray.add(edt_social_five.getText().toString().trim());
         }

       // Log.e("social",socialArray.get(0)+socialArray.get(1)+socialArray.get(2)+socialArray.get(3)+socialArray.get(4));

        SocialFragmentObject social=new SocialFragmentObject(socialArray,edt_website_url.getText().toString().trim());
        return social;
    }


    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }
}
