package com.bidjobbers.bidjobberspro.features.dashbord.ui.share;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.Allpages.AllPagesResponse;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TearmsandConditionFragment extends BaseFragment implements TearmsandConditionFragmentMvpView {

    @BindView(R.id.tvContent)
    TextView tvContent;
    @BindView(R.id.webView)
    WebView webView;
    @Inject
    TearmsandConditionFragmentPresenter<TearmsandConditionFragmentMvpView> tearmsandConditionFragmentPresenter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_tearmandcondition, container, false);
        setUnBinder(ButterKnife.bind(this,root));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        //webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
       // webView.setWebViewClient(new MyBrowser());
        webView.getSettings().setDefaultFontSize((int) getResources().getDimension(R.dimen._11sdp));
        String strtext = getArguments().getString("Sug");
       // Toast.makeText(getActivity(),strtext,Toast.LENGTH_SHORT).show();
        tearmsandConditionFragmentPresenter.onAttach(this);
        tearmsandConditionFragmentPresenter.getAllPages(strtext);


        return root;
    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }

    @Override
    public void allTasksName(AllPagesResponse response) {
        if(response.getResponseCode()==1)
        {
            webView.loadDataWithBaseURL(null,response.getResponseData().getPage_content(),"text/html", "utf-8", null);
          //  String strArray = response.getResponseData().getPage_content().replace("\n", "<br>");;

         //   tvContent.setText(Html.fromHtml(strArray));
        }
    }

    @Override
    public void onDeactivate(String data) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
                getActivity().finish();
            }
        }, 800);
    }
}