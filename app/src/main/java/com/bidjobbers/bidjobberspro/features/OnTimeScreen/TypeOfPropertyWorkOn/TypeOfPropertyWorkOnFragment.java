package com.bidjobbers.bidjobberspro.features.OnTimeScreen.TypeOfPropertyWorkOn;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TypeOfPropertyWorkOn.TypeOfPropertyObject.TypeOfPropertyObject;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;
import com.bidjobbers.bidjobberspro.utils.Validation.TypeOfPropertyResult;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TypeOfPropertyWorkOnFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TypeOfPropertyWorkOnFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TypeOfPropertyWorkOnFragment extends BaseFragment implements  TypeOfPropertyWorkOnFragmentMvpView {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @BindView(R.id.rb_no)
    RadioButton rb_no;
    @BindView(R.id.rb_ys)
    RadioButton rb_ys;
    @BindView(R.id.td_lincence)
    EditText td_lincence;
    @BindView(R.id.cb_resident)
    CheckBox cb_resident;
    @BindView(R.id.cb_commer)
    CheckBox cb_commer;

    Boolean residential,commertial,havelicence=false;
      String  licenceno,commertialId="0",residentialId="0";

    boolean residentialFlag = false;
    boolean commercialFlag= false;


    @Inject
    TypeOfPropertyWorkOnFragmentPresenter<TypeOfPropertyWorkOnFragmentMvpView> typeOfPropertyWorkOnFragmentPresenter;

    public TypeOfPropertyWorkOnFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static TypeOfPropertyWorkOnFragment newInstance() {
        TypeOfPropertyWorkOnFragment fragment = new TypeOfPropertyWorkOnFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            String str = bundle.getString("FROMG");
            Log.e("FROMG", str);

        }



    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        rb_no.setOnCheckedChangeListener(new Radio_check());
        rb_ys.setOnCheckedChangeListener(new Radio_check());


    }

    @Override
    public void actionYesHaveALicence() {
        td_lincence.setVisibility(View.VISIBLE);
    }

    @Override
    public void actionNoHaveALicence() {
        td_lincence.setVisibility(View.GONE);
    }

    @Override
    public void getDataFromSP(boolean isResidential, String residentialId, boolean isCommertial, String commertialId, boolean hasLicence, String licenceNo) {


        if(isResidential)
        {

            cb_resident.setChecked(true);
        }
        if(isCommertial)
        {
            cb_commer.setChecked(true);
        }
        if(hasLicence)
        {
            Toast.makeText(getActivity(),licenceNo+"-"+hasLicence,Toast.LENGTH_SHORT).show();
            rb_ys.setChecked(true);
            rb_no.setChecked(false);
            td_lincence.setVisibility(View.VISIBLE);
        }
        else
        {
            rb_ys.setChecked(false);
            rb_no.setChecked(true);
            td_lincence.setVisibility(View.GONE);
        }
        td_lincence.setText(licenceNo+"");

    }

    @Override
    public void setDataFromApi(DataModelForFirstTimeQusAns.TypeOfPropertyWorkOn typeOfPropertyWorkOn) {


        Log.e("TypeofPropertity",typeOfPropertyWorkOn.toString());
        cb_resident.setText(typeOfPropertyWorkOn.getResidentialName());
        cb_commer.setText(typeOfPropertyWorkOn.getCommercialName());

        commertialId=typeOfPropertyWorkOn.getCommercialId();
        residentialId=typeOfPropertyWorkOn.getResidentialId();

        if(typeOfPropertyWorkOn.isCommercial())
        {
            cb_commer.setChecked(true);
            commercialFlag = true;
        }
        else
        {
            cb_commer.setChecked(false);
            commercialFlag = false;
        }
        if(typeOfPropertyWorkOn.isResidential())
        {
            cb_resident.setChecked(true);
            residentialFlag = true;
        }
        else
        {
            cb_resident.setChecked(false);
            residentialFlag = false;
        }
        if(typeOfPropertyWorkOn.isHaveALincence())
        {
            rb_ys.setChecked(true);
            rb_no.setChecked(false);
            td_lincence.setVisibility(View.VISIBLE);
            havelicence=true;

        }
        else
        {
            rb_ys.setChecked(false);
            rb_no.setChecked(true);
            td_lincence.setVisibility(View.GONE);
            havelicence=false;
        }
        td_lincence.setText(typeOfPropertyWorkOn.getLincenceNumber());


    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }


    class Radio_check implements  CompoundButton.OnCheckedChangeListener
    {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(rb_no.isChecked())
            {
                typeOfPropertyWorkOnFragmentPresenter.onNoHaveALicence();
                havelicence=false;
                td_lincence.setText("");


            }
            else if(rb_ys.isChecked())
            {
                typeOfPropertyWorkOnFragmentPresenter.onYesHaveALicence();
                havelicence=true;
            }
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v= inflater.inflate(R.layout.fragment_type_of_property_work_on, container, false);
        setUnBinder(ButterKnife.bind(this,v));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }


        typeOfPropertyWorkOnFragmentPresenter.onAttach(this);


        typeOfPropertyWorkOnFragmentPresenter.onGetDataFromApi();

        cb_resident.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                residentialFlag=isChecked;
            }
        });

        cb_commer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                commercialFlag=isChecked;
            }
        });


        return v;
    }

    public void setEditTextchangeListnet()
    {
        td_lincence.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                Toast.makeText(getActivity(),"before text change",Toast.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                Toast.makeText(getActivity(),"after text change",Toast.LENGTH_LONG).show();
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    protected void setUp(View view) {

    }




    @Override
    public void onDetach() {
        super.onDetach();

    }




    public TypeOfPropertyResult getDataTypeOfWorkOn()
    {
         //Toast.makeText(getContext(), ""+rb_no.isChecked(), Toast.LENGTH_SHORT).show();
         //Toast.makeText(getContext(), ""+rb_ys.isChecked(), Toast.LENGTH_SHORT).show();
         //Toast.makeText(getContext(), ""+cb_resident.isChecked(), Toast.LENGTH_SHORT).show();
         //Toast.makeText(getContext(), ""+cb_commer.isChecked(), Toast.LENGTH_SHORT).show();

        boolean error= false;
        String errorText="";

        boolean haveALincece= false;
        String licenceText="";



         if(rb_ys.isChecked())
         {
             haveALincece = true;
             if(td_lincence.getText().toString().isEmpty())
             {
                 //Toast.makeText(getContext(), "Please Enter the Licence number", Toast.LENGTH_SHORT).show();
                 error = true;
                 errorText = "Please Enter the Licence number";
                 licenceText="";
             }
             else
             {
                 //Toast.makeText(getContext(), "Licence Number:"+td_lincence.getText().toString(), Toast.LENGTH_SHORT).show();
                 licenceText=td_lincence.getText().toString();
             }
         }
         else
         {
             //No licence number
             haveALincece = false;
         }

         if(!cb_resident.isChecked()&&!cb_commer.isChecked())
         {
             //Toast.makeText(getContext(), "Choose any type of property.", Toast.LENGTH_SHORT).show();

             error = true;
             errorText = "Choose any type of property.";

             residentialFlag = false;
             commercialFlag = false;

         }
         else
         {
             if(cb_resident.isChecked())
             {
                 residentialFlag = true;
             }
             if(cb_resident.isChecked()==false)
             {
                 residentialFlag= false;
             }
             if(cb_commer.isChecked()==true)
             {
                 commercialFlag = true;
             }
             if(cb_commer.isChecked()==false)
             {
                 commercialFlag = false;
             }

         }




        TypeOfPropertyResult typeOfPropertyResult  = new  TypeOfPropertyResult( error,  errorText,  residentialFlag,  commercialFlag,  haveALincece,  licenceText);
       return typeOfPropertyResult;



         /*if(!rb_no.isChecked() && !rb_ys.isChecked())
        {
            Toast.makeText(getContext(), "False", Toast.LENGTH_SHORT).show();
        }
        */

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public TypeOfPropertyObject sendDataToActivity()
    {


        Log.e("TypeofProperty",commercialFlag+"-"+residentialFlag+"-"+havelicence+"-"+residentialId);

        TypeOfPropertyObject  typeOfPropertyObject=new TypeOfPropertyObject( commercialFlag,  residentialFlag,  havelicence,  td_lincence.getText().toString().trim(),  commertialId,  residentialId);
        return typeOfPropertyObject;

    }
}
