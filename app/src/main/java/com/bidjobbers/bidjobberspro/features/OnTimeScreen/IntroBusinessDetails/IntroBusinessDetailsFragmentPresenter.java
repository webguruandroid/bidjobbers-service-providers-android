package com.bidjobbers.bidjobberspro.features.OnTimeScreen.IntroBusinessDetails;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class IntroBusinessDetailsFragmentPresenter <V extends IntroBusinessDetailsFragmentMvpView>
        extends BaseFragmentPresenter<V>
        implements IntroBusinessDetailsFragmentMvpPresenter<V> {

    @Inject
    public IntroBusinessDetailsFragmentPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }


    @Override
    public void onGetDataFromApi() {
        Gson gson = new Gson();
        String json = getDataManager().getMyObject();
        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
        getMvpView().setDataFromApi(obj.introduceYourBusiness);
    }
}
