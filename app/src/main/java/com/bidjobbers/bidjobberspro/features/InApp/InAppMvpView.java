package com.bidjobbers.bidjobberspro.features.InApp;


import com.bidjobbers.bidjobberspro.data.network.models.subscription.SubscriptionResponse;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface InAppMvpView extends MvpView {
    void successfullyFetchInAppList(SubscriptionResponse subscriptionResponse);
}
