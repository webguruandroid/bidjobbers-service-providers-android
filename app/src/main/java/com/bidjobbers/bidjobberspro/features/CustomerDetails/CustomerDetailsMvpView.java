package com.bidjobbers.bidjobberspro.features.CustomerDetails;

import com.bidjobbers.bidjobberspro.data.network.models.BidCreate.BidCreateResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomer.BlockCustomerResponse;
import com.bidjobbers.bidjobberspro.data.network.models.CacelledTask.CancelledTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.CompletedTask.CompletedTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Customer.CustomerDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.EndTask.EndTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ReSchedule.RescheduleResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Recivetask.ReciveTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ScheduleTask.ScheduletaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.StartTask.StartTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.UnBlockUser.UnBlockUserResponse;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

import java.util.List;

public interface CustomerDetailsMvpView extends MvpView {

    void onDeactivate(String data);
    void  allTasksName(List<ReciveTaskResponse.ResponseDataBean.TaskDataBean> arrayList);
    void  allScheduleTasksName(List<ScheduletaskResponse.ResponseDataBean.TaskDataBean> arrayList);
    void allCompletedTaskName(List<CompletedTaskResponse.ResponseDataBean.TaskDataBean> arrayList);
    void  allCancelledTasksName(List<CancelledTaskResponse.ResponseDataBean.CancelledTaskBean> arrayList);
    void onSuccessfillyBid(BidCreateResponse bidCreateResponse);
    void onSuccessfullyRescheduleTask(RescheduleResponse rescheduleResponse);
    void onStartTask(StartTaskResponse startTaskResponse);
    void onEndTask(EndTaskResponse endTaskResponse);
    void onSuccessfullyBlockUser(BlockCustomerResponse blockCustomerResponse);
    void onSuccessfullyUnBlockUser(UnBlockUserResponse unBlockUserResponse);
    void onSuccessfullyGetCustomer(CustomerDetailsResponse customerDetailsResponse);

    void  allTasksNameSchdeule(ScheduletaskResponse arrayList);
}
