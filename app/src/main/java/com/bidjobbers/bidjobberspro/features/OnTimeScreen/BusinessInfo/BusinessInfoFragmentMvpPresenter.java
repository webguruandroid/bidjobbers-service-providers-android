package com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessInfo;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface BusinessInfoFragmentMvpPresenter <V extends BusinessInfoFragmentMvpView> extends BaseFragmentMvpPresenter<V> {
    void onGetDataFromApi();
}
