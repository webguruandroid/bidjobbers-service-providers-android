package com.bidjobbers.bidjobberspro.features.profiledetails;

import com.bidjobbers.bidjobberspro.features.OnTimeScreen.PricesFor.PriceForObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TreeStrubWorkFragment.TreeStrubWorkObject;
import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;

public interface ProfileMvpPresenter <V extends ProfileMvpView> extends MvpPresenter<V> {



    void doGetServiceDetails(String language);
    public void onBackClick();
    public void onDisplayAllReviews();
    public void onBtnSubmitAction();
    void onSubmitPassword(String password);

    public void onEditSocial();
    public void onEditProf();
    public void onEditTypeOfPropertyWorkFor();
    public void onEditImageGalaryFragment();
    public void onEditCngTree();
    public void onKindOfProperty();
    public void onEditImageFragment();

    public void onBusinessInfoFragment();
    public void onBusinessLocationFragment();
    public void onBusinessDetailsFragment();
    public void onBusinessHoursFragment();
    public void onBusinessBookingTimeFragment();

    public void onLoadTakeImageAdapter();
    public void onLoadReviewAdapter();

    public void onAllDialogLoad();
    public void onAllTagLoad();

    //API

    void onUpdateProfile(MultipartBody.Part img_body, String name, String phone, String local);
    void submitKindOfPropertyData(List<Integer> service_type);
    void submitSocialLink(List<String> socialLinks,String webUrl);
    void submitBusinessInfo(String companyName,String yearofFound,String noofEmpId,String comapnyRegNo);
    void submitBusinessLocation(String businessaddress,String apartment,String city,String state,String country,String zipcode, String lat, String lng);
    void submitData(String desc);
    void submitBusinessHours(ArrayList<String> dayId,
                             ArrayList<String>startTimeId,
                             ArrayList<String>startTime,
                             ArrayList<String>endTimeId,
                             ArrayList<String>endTime);
    void submitAdvanceTypeData(String advanceValueId,String advanceValue,String advanceNameid,String advanceName,String noticeValueId,String noticeValue,String noticeNameId,String noticeName);
    void submitPropertyTypeData(ArrayList<Integer> listNo,String isHaveLicense,String licenceNo);

    void submitPriceForTypeData(ArrayList<PriceForObject.PriceBean> list);

    void submitPriceForTree(ArrayList<TreeStrubWorkObject.TreeClass>list);
    void submitImageData(MultipartBody.Part[] workImage,String isWorkImage);

    void getReview();
}
