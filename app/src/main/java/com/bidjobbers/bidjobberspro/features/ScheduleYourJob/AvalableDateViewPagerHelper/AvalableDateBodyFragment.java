package com.bidjobbers.bidjobberspro.features.ScheduleYourJob.AvalableDateViewPagerHelper;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.TimeSlot.TimeSlotResponse;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;
import com.bidjobbers.bidjobberspro.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AvalableDateBodyFragment extends BaseFragment implements AvalableDateBodyFragmentMvpView {

    Context mContext;

    @BindView(R.id.recycler_task_list)
    RecyclerView recycler_product_list;
    @BindView(R.id.rlEmptySlot)
    RelativeLayout rlEmptySlot;


    String cat_id = "0";
    String cat_name="";





    AvalableTimeListAdapter avalableTimeListAdapter;
    ArrayList<AvalableDateBean>  avalableDateBeans;
    ArrayList<String>soltarray;
    @Inject
    AvalableDateBodyPresenter<AvalableDateBodyFragmentMvpView> avalableDateBodyPresenter;

    public AvalableDateBodyFragment()
    {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            //cartUpdateListener = (CartUpdateListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement onFragmentChangeListener");
        }


    }

    @Override
    protected void setUp(View view) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v= inflater.inflate(R.layout.layout_fragment_task_listing_new, container, false);
        setUnBinder(ButterKnife.bind(this,v));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }

        mContext = getContext();
        avalableDateBodyPresenter.onAttach(this);
        cat_id =  getArguments().getString("id");
        cat_name=getArguments().getString("name");

        LinearLayoutManager layoutManagerBest = new LinearLayoutManager(mContext);
        layoutManagerBest.setOrientation(LinearLayoutManager.VERTICAL);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recycler_product_list.getContext(),
                layoutManagerBest.getOrientation());
        recycler_product_list.addItemDecoration(dividerItemDecoration);
        recycler_product_list.setLayoutManager(layoutManagerBest);
        avalableDateBodyPresenter.ongetSchedule();

        return v;

    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }


    @Override
    public void onSuccessfullyGetTimeSlot(TimeSlotResponse.ResponseDataBean timeSlotResponse) {

        avalableDateBeans = new ArrayList<AvalableDateBean>();
        soltarray=new ArrayList<>();
        if(timeSlotResponse.getAvalableSlots().size()>0) {

            for (int i = 0; i < timeSlotResponse.getAvalableSlots().size(); i++) {

                final TimeSlotResponse.ResponseDataBean.AvalableSlotsBean avalableSlotsBean = timeSlotResponse.getAvalableSlots().get(i);

              //  Log.e("Slot",avalableSlotsBean.getDayName()+"-"+cat_name);
                if(avalableSlotsBean.getDayName().equals(cat_name))
                {
                    List<TimeSlotResponse.ResponseDataBean.AvalableSlotsBean.SlotsBean> list=avalableSlotsBean.getSlots();
                    Log.e("Slot","size"+list.size());
                    if(list.size()>0) {
                        rlEmptySlot.setVisibility(View.GONE);
                        recycler_product_list.setVisibility(View.VISIBLE);
                        for (int j = 0; j < list.size(); j++) {
                            TimeSlotResponse.ResponseDataBean.AvalableSlotsBean.SlotsBean sloatbean = list.get(j);
                            avalableDateBeans.add(new AvalableDateBean(sloatbean.getSlots(), sloatbean.getSlot_id(),sloatbean.getJobberSlot()));
                            if(sloatbean.getJobberSlot().equals("1")) {
                                soltarray.add("1");
                            }
                            else
                            {
                                soltarray.add("0");
                            }

                        }

                        avalableTimeListAdapter = new AvalableTimeListAdapter(getActivity(),avalableDateBeans,soltarray);

                        recycler_product_list.setAdapter(avalableTimeListAdapter);
                        avalableTimeListAdapter.notifyDataSetChanged();



                        avalableTimeListAdapter.setmListener(new AvalableTimeListAdapter.SelectItemListener() {
                            @Override
                            public void onItemClick(int pos) {

                                avalableTimeListAdapter.checkPosition(pos);

                                avalableTimeListAdapter.notifyDataSetChanged();
                                Constants.select_slot_time=avalableDateBeans.get(pos).getId();
                                Constants.select_slot_time_name=avalableDateBeans.get(pos).getDate();
                            }
                        });
                    }
                    else
                    {
                        rlEmptySlot.setVisibility(View.VISIBLE);
                        recycler_product_list.setVisibility(View.GONE);
                        Constants.select_slot_time="";
                        Constants.select_slot_time_name="";
                    }
                }

            }


        }











    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }
}