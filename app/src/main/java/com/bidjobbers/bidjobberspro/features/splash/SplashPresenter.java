package com.bidjobbers.bidjobberspro.features.splash;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class SplashPresenter <V extends SplashMvpView> extends BasePresenter<V> implements SplashMvpPresenter<V> {

    @Inject
    public SplashPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onNavigateToLogin() {
      //  getMvpView().onError(getDataManager().getIsQuestionFillup());
        Log.e("Splash",getDataManager().getIsQuestionFillup());
        if(!getDataManager().getAccess_token().equals("")&& getDataManager().getIsQuestionFillup().equals("1")&& getDataManager().getIsEmailVarified().equals("1"))
        {
            getMvpView().onSuccessfullyDecideNavigation("Main");
        }
        else if(getDataManager().getAccess_token().equals(""))
        {
            getMvpView().onSuccessfullyDecideNavigation("Login");
        }
        else if(!getDataManager().getIsEmailVarified().equals("1"))
        {
            getMvpView().onSuccessfullyDecideNavigation("Verify");
        }
        else if(!getDataManager().getIsQuestionFillup().equals("1"))
        {
            getMvpView().onSuccessfullyDecideNavigation("FirstTime");
        }

    }
}
