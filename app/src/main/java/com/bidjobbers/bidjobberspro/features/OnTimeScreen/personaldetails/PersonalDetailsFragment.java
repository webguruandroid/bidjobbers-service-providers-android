package com.bidjobbers.bidjobberspro.features.OnTimeScreen.personaldetails;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.UpdateProfile.UpdateProfileResponse;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;
import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PersonalDetailsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PersonalDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PersonalDetailsFragment extends BaseFragment implements PersonalDetailsFragmentMvpView ,View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    @BindView(R.id.ivProfile)
    ImageView ivProfile;
    @BindView(R.id.input_name)
    EditText input_name;
    @BindView(R.id.input_email)
    EditText input_email;
    @BindView(R.id.input_phone)
    EditText input_phone;
    @BindView(R.id.btn_update)
    Button btn_update;
    @BindView(R.id.ivTakePhoto)
    ImageView ivTakePhoto;

    Context mContext;

    ArrayList<Image> imagesArrayList1=new ArrayList<>();
    File upload_file_me;
    MultipartBody.Part img_body = null;

    @Inject
    PersonalDetailsFragmentPresenter<PersonalDetailsFragmentMvpView> personalDetailsFragmentPresenter;

    public PersonalDetailsFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static PersonalDetailsFragment newInstance(String param1, String param2) {
        PersonalDetailsFragment fragment = new PersonalDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            String str = bundle.getString("FROMG");
            Log.e("FROMG", str);

        }
    }


    @Override
    public void onDeactivate(String data) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
                getActivity().finish();
            }
        }, 800);
    }

    @Override
    public void successfullyGetProfile(getProfileResponse getProfileResponse) {

        if(getProfileResponse.getResponseCode()==1)
        {
            Glide.with(getActivity())
                    .load(getProfileResponse.getResponseData().getProfile_image())
                    // .placeholder(R.drawable.ic_launcher_background)
                    .into(ivProfile);

            input_email.setText(getProfileResponse.getResponseData().getEmail());
            input_name.setText(getProfileResponse.getResponseData().getName());
            input_phone.setText(getProfileResponse.getResponseData().getPhone());
        }
    }

    @Override
    public void successfullyUpdateProfile(UpdateProfileResponse updateProfileResponse) {
        Toast.makeText(getBaseActivity(),updateProfileResponse.getResponseText(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

      //  return inflater.inflate(R.layout.fragment_personal_details, container, false);
        View v= inflater.inflate(R.layout.fragment_personal_details, container, false);
        setUnBinder(ButterKnife.bind(this,v));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }


        personalDetailsFragmentPresenter.onAttach(this);

        personalDetailsFragmentPresenter.onSuccessfullGetProfile("en");

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @OnClick({R.id.ivTakePhoto,R.id.btn_update})
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ivTakePhoto:
                ImagePicker.create(this)
                        .returnMode(ReturnMode.CAMERA_ONLY) // set whether pick and / or camera action should return immediate result or not.
                        .folderMode(true) // folder mode (false by default)
                        .toolbarFolderTitle("Folder") // folder selection title
                        .toolbarImageTitle("Tap to select") // image selection title
                        .toolbarArrowColor(Color.BLACK) // Toolbar 'up' arrow color
                        .includeVideo(false) // Show video on image picker
                        .multi() // multi mode (default mode)
                        .limit(1) // max images can be selected (99 by default)
                        .showCamera(true) // show camera or not (true by default)
                        .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                        .origin(imagesArrayList1) // original selected images, used in multi mode
                        .exclude(imagesArrayList1) // exclude anything that in image.getPath()
                        .theme(R.style.ImagePickerTheme) // must inherit ef_BaseTheme. please refer to sample
                        .enableLog(false) // disabling log
                        .start(); // start image

                break;

            case R.id.btn_update:

                if(input_name.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(mContext, "Please Enter  Name.", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(input_phone.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(mContext, "Please Enter Phone Number", Toast.LENGTH_SHORT).show();
                    return;
                }
                else {
                    if (upload_file_me != null) {
                        RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), upload_file_me);
                        img_body = MultipartBody.Part.createFormData("profile_image", upload_file_me.getName(), reqFile);
                    }
                    personalDetailsFragmentPresenter.onUpdateProfile(img_body, input_name.getText().toString().trim(), input_phone.getText().toString().trim(), "");
                }
                break;
        }
    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public PersonalDetailsObject sendDataforEdit()
    {

        if (upload_file_me != null) {
            RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), upload_file_me);
            img_body = MultipartBody.Part.createFormData("profile_image", upload_file_me.getName(), reqFile);
        }
        PersonalDetailsObject personalDetailsObject=new PersonalDetailsObject(img_body, input_name.getText().toString().trim(), input_phone.getText().toString().trim());
       return personalDetailsObject;
    }

    @Override
    public void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            List<Image> images = ImagePicker.getImages(data);
            Log.e("File Size",ImagePicker.getImages(data).get(0).getPath());


            Image image = ImagePicker.getFirstImageOrNull(data);



            Glide.with(getActivity())
                    .load(images.get(0).getPath())
                    //.placeholder(R.drawable.ic_launcher_background)
                    .into(ivProfile);

            Log.e("File Size",images.get(0).getPath());


            File file = new File(images.get(0).getPath());

            String dir = file.getParent();

            File dirAsFile = file.getParentFile();

            upload_file_me = new File(images.get(0).getPath());

            Log.e("dirAsFile",dir);

        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
