package com.bidjobbers.bidjobberspro.features.dashbord.OnlyActivty;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.DeviceToken.DeviceTokenRequest;
import com.bidjobbers.bidjobberspro.data.network.models.DeviceToken.DeviceTokenResponse;
import com.bidjobbers.bidjobberspro.data.network.models.NotificationCount.NotificationCountRequest;
import com.bidjobbers.bidjobberspro.data.network.models.NotificationCount.NotificationCountResponse;
import com.bidjobbers.bidjobberspro.data.network.models.SetLocal.SetLocalRequest;
import com.bidjobbers.bidjobberspro.data.network.models.SetLocal.SetLocalResponse;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileRequest;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.TabBean;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

public class DashbordPresenter<V extends DashbordMvpView > extends BasePresenter<V> implements DashbordMvpPresenter<V> {

    ArrayList<TabBean> tabBeans= new ArrayList<>();



    /*@Inject @Named("foractivity")
    Context context;
*/
    @Inject
    public DashbordPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }
    @Override
    public void getSetLocal(String local) {

        SetLocalRequest setLocalRequest=new SetLocalRequest(getDataManager().getLocalValue());
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().setLocal("Bearer "+getDataManager().getAccess_token(),"application/json",setLocalRequest)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<SetLocalResponse>()
                {

                    @Override
                    public void onSuccess(SetLocalResponse subscriptionResponse)
                    {
                        getMvpView().hideLoading();

                        if(subscriptionResponse.getResponseCode()==1 )
                        {
                            getMvpView().successfullySEtLocal(subscriptionResponse);
                        }
                        else
                        {
                            getMvpView().onError(subscriptionResponse.getResponseText());
                        }

                    }
                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));

    }

//    @Override
//    public void getAllTasks()
//    {
//
//        ArrayList<TabBean> tabBeans = new ArrayList<TabBean>();
//
//        for (int i = 1; i<=getDataManager().getTypesOfTabs().size();i++)
//        {
//            tabBeans.add(new TabBean(String.valueOf(i),getDataManager().getTypesOfTabs().get(i-1)));
//        }
//
//        getMvpView().allTasksName(tabBeans);
//    }


    @Override
    public void onSuccessfullGetProfile(String language) {
        getProfileRequest getProfileRequest1=new getProfileRequest(language);

        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().getProfileDetails("Bearer "+getDataManager().getAccess_token(),"application/json","application/json",getProfileRequest1)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<getProfileResponse>()
                {

                    @Override
                    public void onSuccess(getProfileResponse testResponse)
                    {

                        getMvpView().hideLoading();
                        if(testResponse.getResponseCode()==1 )
                        {
                            getMvpView().successfullyGetProfile(testResponse);
                        }
                        if(testResponse.getResponseCode()==401)
                        {
                            getMvpView().onError(testResponse.getResponseText());
                            getMvpView().onDeactivate(testResponse.getResponseText());
                        }
                        else
                        {

                            getMvpView().successfullyGetProfile(testResponse);


                            //Shared prefrence value save access_token.
                        }

                    }
                    @Override
                    public void onError(Throwable e) {

                        getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));


    }

    @Override
    public void onLogoutClick() {

      //  getDataManager().destroyPref();
        getDataManager().setAccess_token("");
        getMvpView().successfullyLogout();

    }

    @Override
    public void onGetNotificationCount() {
        NotificationCountRequest notificationCountRequest=new NotificationCountRequest(getDataManager().getLocalValue());

        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().getNotificationCount("Bearer "+getDataManager().getAccess_token(),"application/json",notificationCountRequest)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<NotificationCountResponse>()
                {

                    @Override
                    public void onSuccess(NotificationCountResponse notificationCountResponse)
                    {

                        getMvpView().hideLoading();
                        if(notificationCountResponse.getResponseCode()==1 )
                        {
                            getMvpView().onSuccessfullyGetNotificationCount(notificationCountResponse);
                        }
                        else
                        {

                            getMvpView().onSuccessfullyGetNotificationCount(notificationCountResponse);


                            //Shared prefrence value save access_token.
                        }

                    }
                    @Override
                    public void onError(Throwable e) {

                        getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));
    }

    @Override
    public void onUpdateDeviceToken(String deviceType,String deviceToken) {
     //   getMvpView().onError(deviceToken);
        DeviceTokenRequest deviceTokenRequest=new DeviceTokenRequest("en",deviceType,deviceToken);

      //  getMvpView().onError(deviceTokenRequest.toString());
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().getDeviceToken("Bearer "+getDataManager().getAccess_token(),"application/json",deviceTokenRequest)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<DeviceTokenResponse>()
                {

                    @Override
                    public void onSuccess(DeviceTokenResponse notificationCountResponse)
                    {

                        getMvpView().hideLoading();
                        if(notificationCountResponse.getResponseCode()==1 )
                        {
                            getMvpView().onSuccessfullyUpdateToken(notificationCountResponse);
                        }
                        else
                        {

                            getMvpView().onSuccessfullyUpdateToken(notificationCountResponse);


                            //Shared prefrence value save access_token.
                        }

                    }
                    @Override
                    public void onError(Throwable e) {

                        getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));
    }


}
