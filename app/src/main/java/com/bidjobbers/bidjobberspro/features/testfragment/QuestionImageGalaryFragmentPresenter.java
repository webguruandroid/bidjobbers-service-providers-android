package com.bidjobbers.bidjobberspro.features.testfragment;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.businesshours.BusinessHoursRespose;
import com.bidjobbers.bidjobberspro.data.network.models.typeofpropertyresponse.TypeOfPropertyRequest;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

public class QuestionImageGalaryFragmentPresenter <V extends QuestionImageGalaryFragmentMvpView>
        extends BaseFragmentPresenter<V>
        implements  QuestionImageGalaryFragmentMvpPresenter<V>{

    @Inject
    public QuestionImageGalaryFragmentPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }

    @Override
    public void onGetDataFromApi() {

        Gson gson = new Gson();
        String json = getDataManager().getMyObject();
        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
        getMvpView().setDataFromApi(obj.businessHours);
    }

    @Override
    public void onTestPresenter() {
        getMvpView().actionTestPresenter();
    }

    @Override
    public void onApplyMonday() {
        getMvpView().actionApplyMonday();
    }

    @Override
    public void onApplyTuesday() {
        getMvpView().actionApplyTuesday();
    }

    @Override
    public void onApplyWednesday() {
        getMvpView().actionApplyWednesday();
    }

    @Override
    public void onApplyThree() {
        getMvpView().actionApplyThree();
    }

    @Override
    public void onApplyFriday() {
        getMvpView().actionApplyFriday();
    }

    @Override
    public void onApplySaturday() {
        getMvpView().actionApplySaturday();
    }

    @Override
    public void onApplySunday() {
        getMvpView().actionApplySunday();
    }

    @Override
    public void onEditOne() {
        getMvpView().actionEditOne();
    }

    @Override
    public void onEditTwo() {
        getMvpView().actionEditTwo();
    }

    @Override
    public void onEditThree() {
        getMvpView().actionEditThree();
    }

    @Override
    public void onEditFour() {
        getMvpView().actionEditFour();
    }

    @Override
    public void onEditFive() {
        getMvpView().actionEditFive();
    }

    @Override
    public void onEditSix() {
        getMvpView().actionEditSix();
    }

    @Override
    public void onEditSeven() {
        getMvpView().actionEditSeven();
    }

    @Override
    public void onDoneOne() {
        getMvpView().actionDoneOne();
    }

    @Override
    public void onDoneTwo() {
        getMvpView().actionDoneTwo();
    }

    @Override
    public void onDoneThree() {
        getMvpView().actionDoneThree();
    }

    @Override
    public void onDoneFour() {
        getMvpView().actionDoneFour();
    }

    @Override
    public void onDoneFive() {
        getMvpView().actionDoneFive();
    }

    @Override
    public void onDoneSix() {
        getMvpView().actionDoneSix();
    }

    @Override
    public void onDoneSeven() {
        getMvpView().actionDoneSeven();
    }

    @Override
    public void onChkMonday(boolean flag) {
        getMvpView().actionChkMonday(flag);
    }

    @Override
    public void onChkTuesday(boolean flag) {
        getMvpView().actionChkTuesday(flag);
    }

    @Override
    public void onChkWednesday(boolean flag) {
        getMvpView().actionChkWednesday(flag);
    }

    @Override
    public void onChkThusday(boolean flag) {
        getMvpView().actionChkThusday(flag);
    }

    @Override
    public void onChkFriday(boolean flag) {
        getMvpView().actionChkFriday(flag);
    }

    @Override
    public void onChkSaturday(boolean flag) {
        getMvpView().actionChkSaturday(flag);
    }

    @Override
    public void onChkSunday(boolean flag) {
        getMvpView().actionChkSunday(flag);
    }

    @Override
    public void onSpin1(int pos) {
        getMvpView().actionSpin1(pos);
    }

    @Override
    public void onSpinEnd1(int pos) {
        getMvpView().actionSpinEnd1(pos);
    }

    @Override
    public void onSpin2(int pos) {
        getMvpView().actionSpin2(pos);
    }

    @Override
    public void onSpinEnd2(int pos) {
        getMvpView().actionSpinEnd2(pos);
    }

    @Override
    public void onSpin3(int pos) {
        getMvpView().actionSpin3(pos);
    }

    @Override
    public void onSpinEnd3(int pos) {
        getMvpView().actionSpinEnd3(pos);
    }

    @Override
    public void onSpin4(int pos) {
        getMvpView().actionSpin4(pos);
    }

    @Override
    public void onSpinEnd4(int pos) {
        getMvpView().actionSpinEnd4(pos);
    }

    @Override
    public void onSpin5(int pos) {
        getMvpView().actionSpin5(pos);
    }

    @Override
    public void onSpinEnd5(int pos) {
        getMvpView().actionSpinEnd5(pos);
    }

    @Override
    public void onSpin6(int pos) {
        getMvpView().actionSpin6(pos);
    }

    @Override
    public void onSpinEnd6(int pos) {
        getMvpView().actionSpinEnd6(pos);
    }

    @Override
    public void onSpin7(int pos) {
        getMvpView().actionSpin7(pos);
    }

    @Override
    public void onSpinEnd7(int pos)
    {
        getMvpView().actionSpinEnd7(pos);
    }

    @Override
    public void onInit(String from)
    {
        String authorization= getDataManager().getAccess_token();
        TypeOfPropertyRequest typeOfPropertyRequest = new TypeOfPropertyRequest(authorization);

        getCompositeDisposable().add(getDataManager()
                .getBusinessHours( typeOfPropertyRequest)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<BusinessHoursRespose>()
                {
                    @Override
                    public void onSuccess(BusinessHoursRespose testResponse) {
                        Log.v("data_jit",testResponse.getResponseText());
                    }
                    @Override
                    public void onError(Throwable e)
                    {
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));
    }
}
