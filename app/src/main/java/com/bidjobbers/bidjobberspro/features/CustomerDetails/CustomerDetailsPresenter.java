package com.bidjobbers.bidjobberspro.features.CustomerDetails;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.BidCreate.BidCreateRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BidCreate.BidCreateResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomer.BlockCustomerRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomer.BlockCustomerResponse;
import com.bidjobbers.bidjobberspro.data.network.models.CacelledTask.CancelledTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.CacelledTask.CancelledTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.CompletedTask.CompletedTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.CompletedTask.CompletedTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Customer.CustomerDetailsRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Customer.CustomerDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.EndTask.EndTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ReSchedule.RescheduleRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ReSchedule.RescheduleResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Recivetask.ReciveTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Recivetask.ReciveTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ScheduleTask.ScheduletaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ScheduleTask.ScheduletaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.StartTask.StartTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.StartTask.StartTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.UnBlockUser.UnBlockUserRequest;
import com.bidjobbers.bidjobberspro.data.network.models.UnBlockUser.UnBlockUserResponse;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class CustomerDetailsPresenter <V extends CustomerDetailsMvpView>
        extends BasePresenter<V>
        implements CustomerDetailsMvpPresenter<V> {

    @Inject
    public CustomerDetailsPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void getAllTasks() {
        if(getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();

        ReciveTaskRequest reciveTaskRequest=new ReciveTaskRequest(getDataManager().getLocalValue(),"1");


        getCompositeDisposable().add(getDataManager().getReciveTask("Bearer "+getDataManager().getAccess_token(),"application/json",reciveTaskRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ReciveTaskResponse>() {
                    @Override
                    public void onSuccess(ReciveTaskResponse reciveTaskResponse) {


                        getMvpView().hideLoading();
                       // getMvpView().onError(reciveTaskResponse.getResponseText());
                        Log.e("Task",reciveTaskResponse.getResponseText());
                        getMvpView().allTasksName(reciveTaskResponse.getResponseData().getTaskData());

                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));
        }
        else
        {
            getMvpView().hideLoading();
            getMvpView().showAlert("No internet Connection");
        }

    }

    @Override
    public void getScheduleTask() {

        if(getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
        ScheduletaskRequest scheduletaskRequest=new ScheduletaskRequest(getDataManager().getLocalValue(),"1");


        getCompositeDisposable().add(getDataManager().getScheduledTask("Bearer "+getDataManager().getAccess_token(),"application/json",scheduletaskRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ScheduletaskResponse>() {
                    @Override
                    public void onSuccess(ScheduletaskResponse scheduletaskResponse) {



                        getMvpView().hideLoading();
                        if(scheduletaskResponse.getResponseCode()==1) {
                            getMvpView().allScheduleTasksName(scheduletaskResponse.getResponseData().getTaskData());
                        }
                        else
                        {
                            getMvpView().allScheduleTasksName(scheduletaskResponse.getResponseData().getTaskData());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));
        }
        else
        {
            getMvpView().hideLoading();
            getMvpView().showAlert("No internet Connection");
        }
    }

    @Override
    public void getCompletedTask() {
        if(getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
        CompletedTaskRequest completedTaskRequest=new CompletedTaskRequest(getDataManager().getLocalValue(),"1");
        getCompositeDisposable().add(getDataManager().postCompleteTask("Bearer "+getDataManager().getAccess_token(),"application/json",completedTaskRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CompletedTaskResponse>() {
                    @Override
                    public void onSuccess(CompletedTaskResponse completedTaskResponse) {



                        getMvpView().hideLoading();
                        if(completedTaskResponse.getResponseCode()==1) {
                            getMvpView().allCompletedTaskName(completedTaskResponse.getResponseData().getTaskData());
                        }
                        else
                        {
                            getMvpView().allCompletedTaskName(completedTaskResponse.getResponseData().getTaskData());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));
        }
        else
        {
            getMvpView().hideLoading();
            getMvpView().showAlert("No internet Connection");
        }

    }

    @Override
    public void getCancelledTasks() {

        if(getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
        CancelledTaskRequest cancelledTaskRequest=new CancelledTaskRequest(getDataManager().getLocalValue(),"1");


        getCompositeDisposable().add(getDataManager().postCancelledTask("Bearer "+getDataManager().getAccess_token(),"application/json",cancelledTaskRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CancelledTaskResponse>() {
                    @Override
                    public void onSuccess(CancelledTaskResponse scheduletaskResponse) {



                        getMvpView().hideLoading();
                        if(scheduletaskResponse.getResponseCode()==1) {
                            getMvpView().allCancelledTasksName(scheduletaskResponse.getResponseData().getCancelledTask());
                        }
                        else
                        {
                            getMvpView().allCancelledTasksName(scheduletaskResponse.getResponseData().getCancelledTask());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));
        }
        else
        {
            getMvpView().hideLoading();
            getMvpView().showAlert("No internet Connection");
        }

    }

    @Override
    public void onPostBid(String taskId, String dayId, String slotId, String price, String date,String customer_id) {

        if(getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
        BidCreateRequest bidCreateRequest=new BidCreateRequest(getDataManager().getLocalValue(),taskId,dayId,slotId,"$"+price,date,customer_id);


        getCompositeDisposable().add(getDataManager().postBid("Bearer "+getDataManager().getAccess_token(),"application/json",bidCreateRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<BidCreateResponse>() {
                    @Override
                    public void onSuccess(BidCreateResponse reciveTaskResponse) {


                        getMvpView().hideLoading();
                        // getMvpView().onError(reciveTaskResponse.getResponseText());
                        Log.e("Task",reciveTaskResponse.getResponseText());
                        if(reciveTaskResponse.getResponseCode()==1) {
                            getMvpView().onSuccessfillyBid(reciveTaskResponse);
                        }
                        else if(reciveTaskResponse.getResponseCode()==401)
                        {
                            getMvpView().onError(reciveTaskResponse.getResponseText());
                            getMvpView().onDeactivate(reciveTaskResponse.getResponseText());
                        }
                        else
                        {
                            getMvpView().onError(reciveTaskResponse.getResponseText());
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));
        }
        else
        {
            getMvpView().hideLoading();
            getMvpView().showAlert("No internet Connection");
        }
    }

    @Override
    public void onScheduleTask(String bidId,String taskId, String dayId, String slotId, String date, String comment) {

        if(getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
        RescheduleRequest rescheduleRequest=new RescheduleRequest(getDataManager().getLocalValue(),bidId,taskId,dayId,slotId,date,comment);


        getCompositeDisposable().add(getDataManager().postRescheduleTask("Bearer "+getDataManager().getAccess_token(),"application/json",rescheduleRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<RescheduleResponse>() {
                    @Override
                    public void onSuccess(RescheduleResponse reciveTaskResponse) {


                        getMvpView().hideLoading();
                        if(reciveTaskResponse.getResponseCode()==1) {
                           // getMvpView().onError(reciveTaskResponse.getResponseText());
                            Log.e("Task", reciveTaskResponse.getResponseText());
                            getMvpView().onSuccessfullyRescheduleTask(reciveTaskResponse);
                        }
                       else if(reciveTaskResponse.getResponseCode()==401)
                        {
                            getMvpView().onError(reciveTaskResponse.getResponseText());
                            getMvpView().onDeactivate(reciveTaskResponse.getResponseText());
                        }
                       else
                        {

                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));
        }
        else
        {
            getMvpView().hideLoading();
            getMvpView().showAlert("No internet Connection");
        }
    }

    @Override
    public void onStartTask(String customerId, String taskId, String bidId, String local) {

        if(getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
        StartTaskRequest startTaskRequest=new StartTaskRequest(customerId,taskId,bidId,getDataManager().getLocalValue());

        getCompositeDisposable().add(getDataManager().postStartTask("Bearer "+getDataManager().getAccess_token(),"application/json",startTaskRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<StartTaskResponse>() {
                    @Override
                    public void onSuccess(StartTaskResponse reciveTaskResponse) {


                        getMvpView().hideLoading();
                        if(reciveTaskResponse.getResponseCode()==1) {
                            getMvpView().onError(reciveTaskResponse.getResponseText());
                            Log.e("Task", reciveTaskResponse.getResponseText());
                            getMvpView().onStartTask(reciveTaskResponse);
                        }
                        else if(reciveTaskResponse.getResponseCode()==401)
                        {
                            getMvpView().onDeactivate(reciveTaskResponse.getResponseText());
                        }
                        else
                        {
                            getMvpView().onError(reciveTaskResponse.getResponseText());
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));
        }
        else
        {
            getMvpView().hideLoading();
            getMvpView().showAlert("No internet Connection");
        }
    }

    @Override
    public void onEndTask(String customerId, String taskId, String bidId, String local, MultipartBody.Part[] task_images,String note) {
        if(getMvpView().isNetworkConnected()) {
       getMvpView().showLoading();
        RequestBody customerIdS = RequestBody.create(MediaType.parse("multipart/form-data"), customerId);
        RequestBody taskIdS = RequestBody.create(MediaType.parse("multipart/form-data"), taskId);
        RequestBody bidIdS = RequestBody.create(MediaType.parse("multipart/form-data"), bidId);
        RequestBody localData = RequestBody.create(MediaType.parse("multipart/form-data"), getDataManager().getLocalValue());
        RequestBody notes=RequestBody.create(MediaType.parse("multipart/form-data"), note);

        getCompositeDisposable().add(getDataManager().postEndTask("Bearer "+getDataManager().getAccess_token(),
                localData,customerIdS,taskIdS,bidIdS,notes, task_images)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<EndTaskResponse>() {
                    @Override
                    public void onSuccess(EndTaskResponse reciveTaskResponse) {


                        getMvpView().hideLoading();
                        if(reciveTaskResponse.getResponseCode()==1) {
                            getMvpView().onError(reciveTaskResponse.getResponseText());
                            Log.e("Task", reciveTaskResponse.getResponseText());
                            getMvpView().onEndTask(reciveTaskResponse);
                        }
                        else if(reciveTaskResponse.getResponseCode()==401)
                        {
                            getMvpView().onError(reciveTaskResponse.getResponseText());
                            getMvpView().onDeactivate(reciveTaskResponse.getResponseText());
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));
        }
        else
        {
            getMvpView().hideLoading();
            getMvpView().showAlert("No internet Connection");
        }
    }

    @Override
    public void onBlockUser(String customerId) {
        if(getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
        BlockCustomerRequest blockCustomerRequest=new BlockCustomerRequest(getDataManager().getLocalValue(),customerId);

        getCompositeDisposable().add(getDataManager().postBlockUser("Bearer "+getDataManager().getAccess_token(),"application/json",blockCustomerRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<BlockCustomerResponse>() {
                    @Override
                    public void onSuccess(BlockCustomerResponse reciveTaskResponse) {


                        getMvpView().hideLoading();
                        getMvpView().onError(reciveTaskResponse.getResponseText());
                        Log.e("Task",reciveTaskResponse.getResponseText());
                        getMvpView().onSuccessfullyBlockUser(reciveTaskResponse);

                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));
        }
        else
        {
            getMvpView().hideLoading();
            getMvpView().showAlert("No internet Connection");
        }

    }

    @Override
    public void onUnBlockUser(String customerId) {

        if(getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
        UnBlockUserRequest blockCustomerRequest=new UnBlockUserRequest(getDataManager().getLocalValue(),customerId);

        getCompositeDisposable().add(getDataManager().postUnBlockUser("Bearer "+getDataManager().getAccess_token(),"application/json",blockCustomerRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UnBlockUserResponse>() {
                    @Override
                    public void onSuccess(UnBlockUserResponse unBlockUserResponse) {


                        getMvpView().hideLoading();
                        getMvpView().onError(unBlockUserResponse.getResponseText());
                        Log.e("Task",unBlockUserResponse.getResponseText());
                        getMvpView().onSuccessfullyUnBlockUser(unBlockUserResponse);

                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));

        }
        else
        {
            getMvpView().hideLoading();
            getMvpView().showAlert("No internet Connection");
        }


    }

    @Override
    public void onGetCustomer(String taskid) {
        if(getMvpView().isNetworkConnected()) {
        getMvpView().showLoading();
        CustomerDetailsRequest customerDetailsRequest=new CustomerDetailsRequest(getDataManager().getLocalValue(),taskid);
        getCompositeDisposable().add(getDataManager().getCustomerDetails("Bearer "+getDataManager().getAccess_token(),"application/json",customerDetailsRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CustomerDetailsResponse>() {
                    @Override
                    public void onSuccess(CustomerDetailsResponse CustomerDetailsResponse) {


                        getMvpView().hideLoading();
                        if(CustomerDetailsResponse.getResponseCode()==1) {

                            getMvpView().onSuccessfullyGetCustomer(CustomerDetailsResponse);
                        }
                        else
                        {
                            getMvpView().onSuccessfullyGetCustomer(CustomerDetailsResponse);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));
        }
        else
        {
            getMvpView().hideLoading();
            getMvpView().showAlert("No internet Connection");
        }
    }

    @Override
    public void getAllTasksSchedule(String currentpage) {
        if(getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();
            ScheduletaskRequest scheduletaskRequest=new ScheduletaskRequest(getDataManager().getLocalValue(),currentpage);


            getCompositeDisposable().add(getDataManager().getScheduledTask("Bearer "+getDataManager().getAccess_token(),"application/json",scheduletaskRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<ScheduletaskResponse>() {
                        @Override
                        public void onSuccess(ScheduletaskResponse scheduletaskResponse) {



                            getMvpView().hideLoading();
                            if(scheduletaskResponse.getResponseCode()==1) {
                                getMvpView().allTasksNameSchdeule(scheduletaskResponse);
                            }
                            else
                            {
                                getMvpView().allTasksNameSchdeule(scheduletaskResponse);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            getMvpView().hideLoading();
                        }
                    }));
        }
        else
        {
            getMvpView().hideLoading();
            getMvpView().showAlert("No internet Connection");
        }

    }

}
