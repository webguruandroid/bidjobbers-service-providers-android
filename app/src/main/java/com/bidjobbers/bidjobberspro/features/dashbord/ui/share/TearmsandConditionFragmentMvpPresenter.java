package com.bidjobbers.bidjobberspro.features.dashbord.ui.share;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface TearmsandConditionFragmentMvpPresenter<V extends TearmsandConditionFragmentMvpView> extends BaseFragmentMvpPresenter<V> {

     void getAllPages(String sug);
}
