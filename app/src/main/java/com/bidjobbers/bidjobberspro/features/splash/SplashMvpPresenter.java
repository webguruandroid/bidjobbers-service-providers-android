package com.bidjobbers.bidjobberspro.features.splash;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface SplashMvpPresenter <V extends SplashMvpView> extends MvpPresenter<V> {

  void onNavigateToLogin();
}
