package com.bidjobbers.bidjobberspro.features.dashbord.ui.slideshow;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.JobStatus.JobStatusRequest;
import com.bidjobbers.bidjobberspro.data.network.models.JobStatus.JobStatusResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Notification.NotificationRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Notification.NotificationResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

public class NotificationFragmentPresent<V extends NotificationFragmentMvpView>
        extends BaseFragmentPresenter<V>
        implements NotificationFragmentMvpPresenter<V> {

    @Inject
    public NotificationFragmentPresent(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }

    @Override
    public void getNotificationList() {

        if(getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();

            NotificationRequest notificationRequest = new NotificationRequest( getDataManager().getLocalValue());

            getMvpView().showLoading();
            getCompositeDisposable().add(getDataManager().getNotificationList("Bearer " + getDataManager().getAccess_token(), "application/json",
                    notificationRequest)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribeWith(new DisposableSingleObserver<NotificationResponse>() {

                        @Override
                        public void onSuccess(NotificationResponse notificationResponse) {
                            getMvpView().hideLoading();

                            if (notificationResponse.getResponseCode() == 1) {

                                getMvpView().successfullyFetchNotificationList(notificationResponse);
                            } else {

                                getMvpView().onError(notificationResponse.getResponseText());

                            }

                        }

                        @Override
                        public void onError(Throwable e) {
                            getMvpView().hideLoading();
                            Log.e("ERROR", "onError: " + e.getMessage());
                        }

                    }));
          }
         else
            {
                getMvpView().showAlert("No internet Connection");
            }



        }

    @Override
    public void getJobStatus(String local, String taskId) {
        JobStatusRequest jobStatusRequest=new JobStatusRequest(taskId,getDataManager().getLocalValue());

        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().getJobStatus( "Bearer "+getDataManager().getAccess_token(),"application/json",
                jobStatusRequest)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<JobStatusResponse>()
                {

                    @Override
                    public void onSuccess(JobStatusResponse jobStatusResponse)
                    {
                        getMvpView().hideLoading();

                        if(jobStatusResponse.getResponseCode()==1 )
                        {

                            getMvpView().successfullyFetchStatus(jobStatusResponse);
                        }
                        else
                        {
                           // getMvpView().successfullyFetchStatus(jobStatusResponse);
                            getMvpView().onError(jobStatusResponse.getResponseText());

                        }

                    }
                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));


    }
}
