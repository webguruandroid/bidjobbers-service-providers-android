package com.bidjobbers.bidjobberspro.features.OnTimeScreen.KindOfPropertyWorkOn;

import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface KindOfProPropertyWorkOnFragmentMvpView extends BaseFragmentMvpView {

    void  getDataFromSP(boolean isLawnServiceSelected,String lawnServiceId,boolean  ismowALownSelected,String mowALownId,boolean isAreateALawn,String areateALawnId,boolean isyardWaste,String yardWasteId,boolean isTreeCuttingSelected,String treeCuttingId,boolean isTreeStrubsSelected,String treeStrubsId,boolean isTreeStrumps,String treeStrumpsId,boolean isyardWasteTreeCutting,String yardWasteTreeCuttingId);

     void setDataFromApi(DataModelForFirstTimeQusAns.KindOfServiceDoProvide kindOfServiceDoProvide);

}
