package com.bidjobbers.bidjobberspro.features.ScheduleYourJob.AvalableDateViewPagerHelper;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface AvalableDateBodyMvpPresenter <V extends AvalableDateBodyFragmentMvpView> extends BaseFragmentMvpPresenter<V> {
    void ongetSchedule();
}
