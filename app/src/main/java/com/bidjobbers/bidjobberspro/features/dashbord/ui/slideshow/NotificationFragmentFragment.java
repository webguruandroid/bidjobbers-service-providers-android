package com.bidjobbers.bidjobberspro.features.dashbord.ui.slideshow;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.JobStatus.JobStatusResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Notification.NotificationResponse;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.features.CustomerDetails.CustomerDetailsActivity;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class NotificationFragmentFragment extends BaseFragment implements NotificationFragmentMvpView {




    @Inject
    NotificationAdapter adapter;
    @BindView(R.id.rv_notification)
    RecyclerView rv_notification;
    @BindView(R.id.rl_empty)
    RelativeLayout rl_empty;

    private Context mContext;


    @Inject
    NotificationFragmentPresent<NotificationFragmentMvpView > present;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_slideshow, container, false);
        setUnBinder(ButterKnife.bind(this,root));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }
        present.onAttach(this);
        setRvAdapter();
        present.getNotificationList();

        return root;
    }


    @Override
    protected void setUp(View view) {

    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }

    public void setRvAdapter()
    {
        rv_notification.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_notification.setAdapter(adapter);
        adapter.setAdapterListner(new NotificationAdapter.NotificationListner() {
            @Override
            public void onItemClick(String taskId, String bidId, String statusId, String status) {
                if(!statusId.equals("9")) {
                    present.getJobStatus("en", taskId);
                }
                else
                {
                    Intent intent=new Intent(getActivity(), CustomerDetailsActivity.class);
                    intent.putExtra("status",statusId);
                    intent.putExtra("taskId",taskId);
                    intent.putExtra("bidId","");
                    intent.putExtra("Tag","Foreground");
                    startActivity(intent);
                }

            }
        });
    }

    @Override
    public void successfullyFetchNotificationList(NotificationResponse notificationResponse) {

        if(notificationResponse.getResponseCode()==1) {
            rv_notification.setVisibility(View.VISIBLE);
            rl_empty.setVisibility(View.GONE);
            adapter.loadList(notificationResponse.getResponseData().getNotificationList());
        }
        else
        {
            rv_notification.setVisibility(View.GONE);
            rl_empty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void successfullyFetchStatus(JobStatusResponse jobStatusResponse) {
        Intent intent=new Intent(getActivity(), CustomerDetailsActivity.class);
        intent.putExtra("status",jobStatusResponse.getResponseData().getStatus_id());
        intent.putExtra("taskId",jobStatusResponse.getResponseData().getTask_id());
        intent.putExtra("bidId","");
        intent.putExtra("Tag","Foreground");
        startActivity(intent);
    }
}