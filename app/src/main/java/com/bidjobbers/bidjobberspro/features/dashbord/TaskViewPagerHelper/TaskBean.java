package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper;

public class TaskBean {

    String imageUrl="";
    String time="";
    String amault="";
    String status="";
    String bookFor="";

    public TaskBean(String imageUrl, String time, String amault, String status, String bookFor) {
        this.imageUrl = imageUrl;
        this.time = time;
        this.amault = amault;
        this.status = status;
        this.bookFor = bookFor;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAmault() {
        return amault;
    }

    public void setAmault(String amault) {
        this.amault = amault;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBookFor() {
        return bookFor;
    }

    public void setBookFor(String bookFor) {
        this.bookFor = bookFor;
    }
}
