package com.bidjobbers.bidjobberspro.features.BankDetails;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.BankDetails.BankDetailsRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BankDetails.BankDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList.BlockCustomerListRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList.BlockCustomerListResponse;
import com.bidjobbers.bidjobberspro.features.Archive.ArchiveMvpPresenter;
import com.bidjobbers.bidjobberspro.features.Archive.ArchiveMvpView;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class BankDetailsPresenter <V extends BankDetailsMvpView> extends BasePresenter<V> implements BankDetailsMvpPresenter<V> {
    @Inject
    public BankDetailsPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onGetDataFromApi() {
        Gson gson = new Gson();
        String json = getDataManager().getMyObject();
        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
        getMvpView().setDataFromApi(obj.bankDetails);
    }

    @Override
    public void submitBankDetails(String bankName, String accountHolderName,String accountHolderLastName, String bankAccountNumber, String routingNumber, String bankCountry, String bankCurrency, String bankSwift, String bankIban, String paypalId, String paypalCurrency,String transaction_acoount_id) {


        BankDetailsRequest bankDetailsRequest=new BankDetailsRequest( accountHolderName,accountHolderLastName,bankAccountNumber,bankCountry,bankCurrency,bankIban,bankName,bankSwift,getDataManager().getLocalValue(),paypalCurrency,paypalId,transaction_acoount_id);

        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().submitBankDetails("Bearer "+getDataManager().getAccess_token(),"application/json",bankDetailsRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<BankDetailsResponse>() {
                    @Override
                    public void onSuccess(BankDetailsResponse bankDetailsResponse) {

                        getMvpView().hideLoading();
                        if(bankDetailsResponse.getResponseCode()==1) {
                            getMvpView().successfullyUpdateBankDetails(bankDetailsResponse);
                            getMvpView().showAlert(bankDetailsResponse.getResponseText());
                        }
                        else
                        {
                            getMvpView().successfullyUpdateBankDetails(bankDetailsResponse);
                            getMvpView().showAlert(bankDetailsResponse.getResponseText());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));

    }
}
