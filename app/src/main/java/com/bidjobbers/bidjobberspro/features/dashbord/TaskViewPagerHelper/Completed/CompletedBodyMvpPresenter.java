package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.Completed;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface CompletedBodyMvpPresenter<V extends CompletedBodyMvpView> extends BaseFragmentMvpPresenter<V> {
     void getAllTasks(String currentpage);
}
