package com.bidjobbers.bidjobberspro.features.OnTimeScreen.TypeOfPropertyWorkOn;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.typeofpropertyresponse.TypeOfProperty;
import com.bidjobbers.bidjobberspro.data.network.models.typeofpropertyresponse.TypeOfPropertyRequest;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

public class TypeOfPropertyWorkOnFragmentPresenter<V extends TypeOfPropertyWorkOnFragmentMvpView>
        extends BaseFragmentPresenter<V>
        implements  TypeOfPropertyWorkOnFragmentMvpPresenter<V> {

    @Inject
    public TypeOfPropertyWorkOnFragmentPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }

    @Override
    public void onYesHaveALicence() {
       getMvpView().actionYesHaveALicence();
    }

    @Override
    public void onNoHaveALicence() {
        getMvpView().actionNoHaveALicence();
    }

    @Override
    public void onInit(String from) {


        //Check if data present in SP
        //Yes no need to call api set ui data from SP.


        String authorization= getDataManager().getAccess_token();

        TypeOfPropertyRequest typeOfPropertyRequest = new TypeOfPropertyRequest(authorization);

        getCompositeDisposable().add(getDataManager().getTypesOfProperty( typeOfPropertyRequest)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<TypeOfProperty>()
                {
                    @Override
                    public void onSuccess(TypeOfProperty testResponse) {

                        String residentId="";
                        String commerId="";

                        residentId= testResponse.getResponseData().get(0).getId();
                        commerId= testResponse.getResponseData().get(1).getId();


                       // SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
                       // Gson gson = new Gson();

                        Gson gson = new Gson();
                        String json = getDataManager().getMyObject();
                        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);

                        obj.typeOfPropertyWorkOn.setCommercialId(commerId);
                        obj.typeOfPropertyWorkOn.setResidentialId(residentId);


                        String jsonString = gson.toJson(obj);
                        getDataManager().setMyObject(jsonString);

                        Log.e("ERROR",obj.getTypeOfPropertyWorkOn().toString());

                    }
                    @Override
                    public void onError(Throwable e)
                    {
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));

    }

    @Override
    public void onGetDataFromSP() {
        Gson gson = new Gson();
        String json = getDataManager().getMyObject();
        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);

           getMvpView().getDataFromSP(obj.typeOfPropertyWorkOn.isResidential()
                ,obj.typeOfPropertyWorkOn.getResidentialId(),
                obj.typeOfPropertyWorkOn.isCommercial(),
                obj.typeOfPropertyWorkOn.getCommercialId(),
                obj.typeOfPropertyWorkOn.isHaveALincence(),
                obj.typeOfPropertyWorkOn.getLincenceNumber()
        );

    }

    @Override
    public void onGetDataFromApi() {
        Gson gson = new Gson();
        String json = getDataManager().getMyObject();
        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
        getMvpView().setDataFromApi(obj.typeOfPropertyWorkOn);
    }

//    @Override
//    public void actionNextClick(boolean isResidential, boolean isCommestial, boolean isLicence, String licenceNo) {
//        Gson gson = new Gson();
//        String json = getDataManager().getMyObject();
//        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
//        obj.getTypeOfPropertyWorkOn().setResidential(isResidential);
//        obj.getTypeOfPropertyWorkOn().setCommercial(isCommestial);
//        obj.getTypeOfPropertyWorkOn().setHaveALincence(isLicence);
//        obj.getTypeOfPropertyWorkOn().setLincenceNumber(licenceNo);
//        String jsonString = gson.toJson(obj);
//        getDataManager().setMyObject(jsonString);
//    }



}
