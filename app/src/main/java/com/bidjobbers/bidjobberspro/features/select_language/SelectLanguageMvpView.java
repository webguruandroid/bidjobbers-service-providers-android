package com.bidjobbers.bidjobberspro.features.select_language;

import com.bidjobbers.bidjobberspro.data.network.models.SetLocal.SetLocalResponse;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface SelectLanguageMvpView extends MvpView {
    void successfullySEtLocal(SetLocalResponse setLocalResponse);
    void getLanguage();
}
