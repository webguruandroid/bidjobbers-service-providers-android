package com.bidjobbers.bidjobberspro.features.dashbord.ui.Contactus;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.ContactUs.ContactUsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactusFragment extends BaseFragment implements ContactusFragmentMvpView{


    @BindView(R.id.sp_reason)
    Spinner sp_reason;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tv_email)
    TextView tv_email;
    @BindView(R.id.edt_message)
    EditText edt_message;
    @BindView(R.id.btn_submit_contact)
    Button btn_submit_contact;


    ArrayList<String> type= new ArrayList<String>();

    @Inject
    DataManager dataManager;
    @Inject
    ContactusFragmentPresenter<ContactusFragmentMvpView> homeFragmentPresenter;
    String reason="";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_contactus, container, false);
        setUnBinder(ButterKnife.bind(this,root));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }
        homeFragmentPresenter.onAttach(this);

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        type= new ArrayList<String>();
        type.add(getResources().getString(R.string.contact_reason_one));
        type.add(getResources().getString(R.string.contact_reason_two));
        homeFragmentPresenter.onSuccessfullGetProfile("");



        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, type);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_reason.setAdapter(adapter);

        sp_reason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                reason=type.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                reason=(getResources().getString(R.string.contact_reason_one));
            }
        });
        btn_submit_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(edt_message.getText().toString().trim().equals(""))
                {
                    Toast.makeText(getActivity(),"Please enter message",Toast.LENGTH_SHORT).show();
                    return;
                }
                else {
                    homeFragmentPresenter.onContactUs(tvName.getText().toString().trim(), tv_email.getText().toString().trim(), reason, edt_message.getText().toString().trim());
                }
            }
        });
    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }

    @Override
    public void successfullyGetProfile(getProfileResponse getProfileResponse) {
        tvName.setText(getProfileResponse.getResponseData().getName());
        tv_email.setText(getProfileResponse.getResponseData().getEmail());
    }

    @Override
    public void onDeactivate(String data) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
                getActivity().finish();
            }
        }, 800);
    }

    @Override
    public void successfullyContactUs(ContactUsResponse contactUsResponse) {

        if(contactUsResponse.getResponseCode()==1)
        {
            Toast.makeText(getActivity(),contactUsResponse.getResponseText(),Toast.LENGTH_SHORT).show();
            edt_message.setText("");
            ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, type);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_reason.setAdapter(adapter);
        }
    }
}
