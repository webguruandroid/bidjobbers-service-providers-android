package com.bidjobbers.bidjobberspro.features.OnTimeScreen.IntroBusinessDetails;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface IntroBusinessDetailsFragmentMvpPresenter <V extends IntroBusinessDetailsFragmentMvpView> extends BaseFragmentMvpPresenter<V> {
    void onGetDataFromApi();
}
