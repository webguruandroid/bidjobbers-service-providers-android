package com.bidjobbers.bidjobberspro.features.select_language;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.SetLocal.SetLocalRequest;
import com.bidjobbers.bidjobberspro.data.network.models.SetLocal.SetLocalResponse;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

public class SelectLanguagePresenter<V extends SelectLanguageMvpView> extends BasePresenter<V> implements SelectLanguageMvpPresenter<V> {
    @Inject
    public SelectLanguagePresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void getSetLocal(String local) {

        SetLocalRequest setLocalRequest=new SetLocalRequest(local);
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().setLocal("Bearer "+getDataManager().getAccess_token(),"application/json",setLocalRequest)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<SetLocalResponse>()
                {

                    @Override
                    public void onSuccess(SetLocalResponse subscriptionResponse)
                    {
                        getMvpView().hideLoading();

                        if(subscriptionResponse.getResponseCode()==1 )
                        {

                            getMvpView().successfullySEtLocal(subscriptionResponse);
                        }
                        else
                        {

                            getMvpView().onError(subscriptionResponse.getResponseText());

                        }

                    }
                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));

    }
    @Override
    public void setLanguage(String language) {
        getDataManager().setLocalValue(language);
    }
}
