package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper;

import com.bidjobbers.bidjobberspro.data.network.models.Recivetask.ReciveTaskResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface TaskItemBodyMvpView extends BaseFragmentMvpView {
     void  allTasksName(ReciveTaskResponse arrayList);
     void onDeActive(String data);
}
