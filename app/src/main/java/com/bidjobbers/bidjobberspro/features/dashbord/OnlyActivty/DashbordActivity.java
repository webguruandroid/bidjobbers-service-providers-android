package com.bidjobbers.bidjobberspro.features.dashbord.OnlyActivty;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.DeviceToken.DeviceTokenResponse;
import com.bidjobbers.bidjobberspro.data.network.models.NotificationCount.NotificationCountResponse;
import com.bidjobbers.bidjobberspro.data.network.models.SetLocal.SetLocalResponse;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPager.ViewPagerTaskAdapter;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.TabBean;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.home.HomeFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.send.SettingsFragment;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;
import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashbordActivity extends BaseActivity implements  DashbordMvpView{


    ViewPagerTaskAdapter viewPagerTaskyAdapter;
    TabLayout sliding_tabs;
    ViewPager viewpager;
    DrawerLayout drawer_layout;
    ImageView iv_mnu;
    NavigationView navigationView;


    ImageView ivProfile;

    TextView tvName;

    TextView ivEmail;
    View headerView;

    @BindView(R.id.llContainer)
    LinearLayout llContainer;

    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;

    Context mContext;
    ArrayList<TabBean> tabBeans= new ArrayList<>();
    Fragment fragment = null;

    @Inject
    DashbordPresenter<DashbordMvpView> dashbordPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashbord);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        dashbordPresenter.onAttach(this);

        mContext = this;

        sliding_tabs = (TabLayout) findViewById(R.id.sliding_tabs);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
        iv_mnu = (ImageView) findViewById(R.id.iv_mnu);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

      //  dashbordPresenter.getAllTasks();

        setUpNavigationView();


        sliding_tabs.setupWithViewPager(viewpager);
        viewpager.setOffscreenPageLimit(1);

        iv_mnu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.openDrawer(Gravity.LEFT);
            }
        });
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(DashbordActivity.this);
        dashbordPresenter.onSuccessfullGetProfile(prefs.getString("LOCAL_KEY",""));

        View headerView = navigationView.getHeaderView(0);
        tvName = (TextView) headerView.findViewById(R.id.tvName);
        ivEmail=(TextView)headerView.findViewById(R.id.ivEmail);
        ivProfile=(ImageView)headerView.findViewById(R.id.ivProfile);

        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment=new SettingsFragment();
                replaceFragment(fragment);

            }
        });
        replaceFragment(new HomeFragment());
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(DashbordActivity.this);
        dashbordPresenter.onSuccessfullGetProfile(prefs.getString("LOCAL_KEY",""));
    }

    private void setUpNavigationView() {

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.action_logout:

                        dashbordPresenter.onLogoutClick();
                        drawer_layout.closeDrawer(Gravity.LEFT);
                        break;

                     case R.id.action_settings:
//                        startActivity(new Intent(mContext, SettingsActivity.class));
//                        overridePendingTransition(R.anim.enter, R.anim.exit);
//                        drawer_layout.closeDrawer(Gravity.LEFT);
                         fragment=new SettingsFragment();
                         replaceFragment(fragment);
                         drawer_layout.closeDrawer(Gravity.LEFT);
                        break;
                     default:
                        showMessage("Under development");
                         drawer_layout.closeDrawer(Gravity.LEFT);
                }
//                if (menuItem.isChecked()) {
//                    menuItem.setChecked(false);
//                } else {
//                    menuItem.setChecked(true);
//                }
//                menuItem.setChecked(true);
                return true;
            }
        });

    }


    void replaceFragment(Fragment fragment)
    {
        if (fragment != null) {
            fragmentManager = getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.replace(R.id.llContainer, fragment);
            fragmentTransaction.commit();
        }
    }

//    @Override
//    public void allTasksName(ArrayList<TabBean> arrayList) {
//        tabBeans = arrayList;
//        viewPagerTaskyAdapter = new ViewPagerTaskAdapter(getSupportFragmentManager(),tabBeans);
//        viewpager.setAdapter(viewPagerTaskyAdapter);
//        viewPagerTaskyAdapter.notifyDataSetChanged();
//    }

    @Override
    public void onDeactivate(String data) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(DashbordActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, 800);


    }

    @Override
    public void successfullySEtLocal(SetLocalResponse setLocalResponse) {

    }

    @Override
    public void successfullyGetProfile(getProfileResponse getProfileResponse) {
        if(getProfileResponse.getResponseCode()==1)
        {
            Glide.with(mContext)
                    .load(getProfileResponse.getResponseData().getProfile_image())
                    // .placeholder(R.drawable.ic_launcher_background)
                    .into(ivProfile);

            ivEmail.setText(getProfileResponse.getResponseData().getEmail());
            tvName.setText(getProfileResponse.getResponseData().getName());

        }
    }

    @Override
    public void successfullyLogout() {
        Intent i=new Intent(DashbordActivity.this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onSuccessfullyGetNotificationCount(NotificationCountResponse notificationCountResponse) {

    }

    @Override
    public void onSuccessfullyUpdateToken(DeviceTokenResponse response) {

    }


}
