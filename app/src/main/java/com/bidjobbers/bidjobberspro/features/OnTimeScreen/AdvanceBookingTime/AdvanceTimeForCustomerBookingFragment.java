package com.bidjobbers.bidjobberspro.features.OnTimeScreen.AdvanceBookingTime;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import androidx.annotation.Nullable;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.AdvanceBookingTime.AdvanceBookingTimeHelper.DayWeekMonthAdapter;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.AdvanceBookingTime.AdvanceBookingTimeHelper.DayWeekMonthValueAdapter;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.AdvanceBookingTime.AdvanceBookingTimeHelper.HourDayAdapter;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.AdvanceBookingTime.AdvanceBookingTimeHelper.HourDayValueAdapter;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AdvanceTimeForCustomerBookingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AdvanceTimeForCustomerBookingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AdvanceTimeForCustomerBookingFragment extends BaseFragment implements AdapterView.OnItemSelectedListener,AdvanceTimeForCustomerBookingFragmentMvpView{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    @BindView(R.id.spin_left_first)
    Spinner spin_left_first;

    @BindView(R.id.spin_right_first)
    Spinner spin_right_first;

    @BindView(R.id.spin_one)
    Spinner spin_one;

    @BindView(R.id.spin_two)
    Spinner spin_two;


    ArrayList<String> dayWeekMonthValuesArray = new ArrayList<String>();
    ArrayList<String> dayWeekMonthValuesArrayId = new ArrayList<String>();
    ArrayList<String> dayWeekMonthArray = new ArrayList<String>();
    ArrayList<String> dayWeekMonthArrayId = new ArrayList<String>();

    ArrayList<String> hourDayArray = new ArrayList<String>();
    ArrayList<String> hourDayArrayId = new ArrayList<String>();

    ArrayList<String> hourDayValueArray = new ArrayList<String>();
    ArrayList<String> hourDayValueArrayId = new ArrayList<String>();

   String advanceValueId,
           advanceValue,
           advanceNameid,
           advanceName,
           noticeValueId,
           noticeValue,
           noticeNameId,
           noticeName;

    @Inject
    AdvanceTimeForCustomerBookingFragmentPresenter<AdvanceTimeForCustomerBookingFragmentMvpView> presenter;

    public AdvanceTimeForCustomerBookingFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static AdvanceTimeForCustomerBookingFragment newInstance() {
        AdvanceTimeForCustomerBookingFragment fragment = new AdvanceTimeForCustomerBookingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            String str = bundle.getString("FROMG");
            Log.e("FROMG", str);

        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v= inflater.inflate(R.layout.fragment_advance_time_for_customer_booking, container, false);
        setUnBinder(ButterKnife.bind(this,v));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }
        presenter.onAttach(this);
        presenter.onGetDataFromApi();

        return v;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        DayWeekMonthValueAdapter dayWeekMonthValueAdapter = new DayWeekMonthValueAdapter(getContext(),dayWeekMonthValuesArray);
        DayWeekMonthAdapter dayWeekMonthAdapter = new DayWeekMonthAdapter(getContext(),dayWeekMonthArray);

        HourDayAdapter hourDayAdapter = new HourDayAdapter(getContext(),hourDayArray);
        HourDayValueAdapter hourDayValueAdapter = new HourDayValueAdapter(getContext(),hourDayValueArray);


        spin_left_first.setOnItemSelectedListener(this);


        spin_left_first.setAdapter(dayWeekMonthValueAdapter);
        spin_right_first.setAdapter(dayWeekMonthAdapter);//hourDayValueAdapter

        spin_one.setAdapter(hourDayValueAdapter);
        spin_two.setAdapter(hourDayAdapter);

        getSpinnerListner();


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onGetDataFromApi();
    }

    @Override
    public void setDataFromApi(DataModelForFirstTimeQusAns.AdvanceTimeToBookAJob advanceTimeToBookAJob) {



         dayWeekMonthValuesArray = new ArrayList<String>();
         dayWeekMonthValuesArrayId = new ArrayList<String>();
         dayWeekMonthArray = new ArrayList<String>();
         dayWeekMonthArrayId = new ArrayList<String>();

         hourDayArray = new ArrayList<String>();
         hourDayArrayId = new ArrayList<String>();

         hourDayValueArray = new ArrayList<String>();
         hourDayValueArrayId = new ArrayList<String>();






   //     Log.e("Advance",Integer.parseInt(advanceTimeToBookAJob.getFieldAdvanceValueId())+"-"+Integer.parseInt(advanceTimeToBookAJob.getFieldAdvanceNameId())+"-"+Integer.parseInt(advanceTimeToBookAJob.getFieldNoticeValueId())+"-"+Integer.parseInt(advanceTimeToBookAJob.getFieldNoticeNameId())+"");


//
//        for(int i=0;i<dayWeekMonthValuesArray.size();i++)
//        {
//            if(advanceTimeToBookAJob.getFieldAdvanceValue().equals(dayWeekMonthValuesArray.get(i)))
//            {
//                spin_left_first.setSelection(i);
//            }
//        }


        if(!advanceTimeToBookAJob.getFieldAdvanceValueId().equals("")) {
            spin_left_first.setSelection(Integer.parseInt(advanceTimeToBookAJob.getFieldAdvanceValueId()));

        }
        if(!advanceTimeToBookAJob.getFieldAdvanceNameId().equals("")) {
            spin_right_first.setSelection(Integer.parseInt(advanceTimeToBookAJob.getFieldAdvanceNameId()));
        }
        if(!advanceTimeToBookAJob.getFieldNoticeValueId().equals("")) {
            spin_one.setSelection(Integer.parseInt(advanceTimeToBookAJob.getFieldNoticeValueId()));
        }
       if(advanceTimeToBookAJob.getFieldNoticeNameId().equals("1"))
       { spin_two.setSelection(1);}
       else if(advanceTimeToBookAJob.getFieldNoticeNameId().equals("4"))
       {  spin_two.setSelection(2);}
       else
       {   spin_two.setSelection(0);}

        dayWeekMonthValuesArray.add("Select");
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount1Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount2Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount3Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount4Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount5Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount6Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount7Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount8Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount9Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount10Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount11Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount12Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount13Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount14Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount15Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount16Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount17Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount18Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount19Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount20Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount21Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount22Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount23Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount24Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount25Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount26Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount27Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount28Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount29Count());
        dayWeekMonthValuesArray.add(advanceTimeToBookAJob.getCount30Count());


        dayWeekMonthValuesArrayId.add("0");
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount1Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount2Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount3Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount4Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount5Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount6Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount7Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount8Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount9Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount10Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount11Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount12Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount13Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount14Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount15Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount16Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount17Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount18Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount19Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount20Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount21Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount22Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount23Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount24Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount25Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount26Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount27Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount28Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount29Id());
        dayWeekMonthValuesArrayId.add(advanceTimeToBookAJob.getCount30Id());




        dayWeekMonthArray.add("Select");
        dayWeekMonthArray.add(advanceTimeToBookAJob.getHowFar1Unit());
        dayWeekMonthArray.add(advanceTimeToBookAJob.getHowFar2Unit());
        dayWeekMonthArray.add(advanceTimeToBookAJob.getHowFar3Unit());


        dayWeekMonthArrayId.add("0");
        dayWeekMonthArrayId.add(advanceTimeToBookAJob.getHowFar1Id());
        dayWeekMonthArrayId.add(advanceTimeToBookAJob.getHowFar2Id());
        dayWeekMonthArrayId.add(advanceTimeToBookAJob.getHowFar3Id());



        hourDayValueArray.add("Select");
        hourDayValueArray.add(advanceTimeToBookAJob.getCount1Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount2Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount3Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount4Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount5Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount6Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount7Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount8Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount9Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount10Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount11Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount12Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount13Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount14Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount15Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount16Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount17Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount18Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount19Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount20Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount21Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount22Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount23Count());
        hourDayValueArray.add(advanceTimeToBookAJob.getCount24Count());


        hourDayValueArrayId.add("0");
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount1Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount2Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount3Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount4Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount5Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount6Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount7Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount8Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount9Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount10Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount11Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount12Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount13Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount14Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount15Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount16Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount17Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount18Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount19Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount20Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount21Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount22Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount23Id());
        hourDayValueArrayId.add(advanceTimeToBookAJob.getCount24Id());



        hourDayArray.add("Select");
        hourDayArray.add(advanceTimeToBookAJob.getNotice1Unite());
        hourDayArray.add(advanceTimeToBookAJob.getNotice2Unite());

        hourDayArrayId.add("0");
        hourDayArrayId.add(advanceTimeToBookAJob.getNotice1Id());
        hourDayArrayId.add(advanceTimeToBookAJob.getNotice2Id());



    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public void getSpinnerListner()
    {

        spin_left_first.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                advanceValue=dayWeekMonthValuesArray.get(position);
                advanceValueId=dayWeekMonthValuesArrayId.get(position);

                Log.e("Advanceddd",advanceValue+"-"+advanceValueId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                advanceValueId=dayWeekMonthValuesArrayId.get(0);
                advanceValue=dayWeekMonthValuesArray.get(0);
            }
        });

        spin_right_first.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                advanceName=dayWeekMonthArray.get(position);
                advanceNameid=dayWeekMonthArrayId.get(position);

                Log.e("Advanceddd",advanceName+"-"+advanceNameid);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                advanceNameid=dayWeekMonthArrayId.get(0);
                advanceName=dayWeekMonthArray.get(0);
            }
        });

        spin_one.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                noticeValue=hourDayValueArray.get(position);
                noticeValueId=hourDayValueArrayId.get(position);

                Log.e("Advanceddd",noticeValue+"-"+noticeValueId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                noticeValueId=hourDayValueArrayId.get(0);
                noticeValue=hourDayValueArray.get(0);
            }
        });

        spin_two.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                noticeName=hourDayArray.get(position);
                noticeNameId=hourDayArrayId.get(position);

                Log.e("Advanceddd",noticeValue+"-"+noticeValueId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                noticeNameId=hourDayArrayId.get(0);
                noticeName=hourDayArray.get(0);
            }
        });

    }


    public AdvanceTimeForCustomerObject setAdvanceBooking()
    {


        AdvanceTimeForCustomerObject object=new AdvanceTimeForCustomerObject(advanceValueId,advanceValue,advanceNameid,advanceName,noticeValueId,noticeValue,noticeNameId,noticeName);
        return object;

    }

}
