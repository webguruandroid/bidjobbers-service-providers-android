package com.bidjobbers.bidjobberspro.features.dashbord.ui.gallery;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList.BlockCustomerListResponse;
import com.bidjobbers.bidjobberspro.data.network.models.UnBlockUser.UnBlockUserResponse;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BlockCustomerFragment extends BaseFragment  implements BlockCustomerFragmentMvpView  {


    Context mContext;
    @BindView(R.id.rvBlockList)
    RecyclerView rvBlockList;

    @BindView(R.id.llContainer)
    LinearLayout llContainer;
    @BindView(R.id.rlEmpty)
    RelativeLayout rlEmpty;


    BlockListingAdapter blockListingAdapter;

    @Inject
    BlockCustomerFragmentPresenter<BlockCustomerFragmentMvpView> blockCustomerFragmentPresenter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_blockcustomer, container, false);
        setUnBinder(ButterKnife.bind(this,root));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }
        mContext = getContext();
        blockCustomerFragmentPresenter.onAttach(this);
        rvBlockList.setLayoutManager(new LinearLayoutManager(mContext));
        rvBlockList.setAdapter(blockListingAdapter);


        blockCustomerFragmentPresenter.getBlockCustomer();

        return root;
    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }





    @Override
    public void successfullyGetBlockCustomer(BlockCustomerListResponse blockCustomerListResponse) {


        if(blockCustomerListResponse.getResponseCode()==1)
        {

            llContainer.setVisibility(View.VISIBLE);
            rlEmpty.setVisibility(View.GONE);

            if(blockCustomerListResponse.getResponseData().getBlocklist().size()>0) {

                blockListingAdapter.loadList(blockCustomerListResponse.getResponseData().getBlocklist());
                blockListingAdapter.setmListener(new BlockListingAdapter.AddButtonListener() {
                    @Override
                    public void onItemClick(int pos, final String taskId) {

                        try {
                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setTitle("Confirm Unblock");
                            builder.setMessage("Are you sure to unblock this customer for you?");
                            builder.setCancelable(false);
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    blockCustomerFragmentPresenter.onUnBlockUser(taskId);
                                }
                            });
                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog alert = builder.create();
                            alert.show();
                        } catch (Exception e){

                        }
                    }
                });

            }
        }
        else
        {

            llContainer.setVisibility(View.GONE);
            rlEmpty.setVisibility(View.VISIBLE);
            //!
        }
    }

    @Override
    public void onSuccessfullyUnBlockUser(UnBlockUserResponse unBlockUserResponse) {
        blockCustomerFragmentPresenter.getBlockCustomer();
    }
}