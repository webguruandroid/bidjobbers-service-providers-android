package com.bidjobbers.bidjobberspro.features.OnTimeScreen.PricesFor;

import java.util.ArrayList;

public class PriceForObject {


   public ArrayList<PriceBean> list;


    public PriceForObject(ArrayList<PriceBean> list) {
        this.list = list;
    }

    public ArrayList<PriceBean> getList() {
        return list;
    }

    public void setList(ArrayList<PriceBean> list) {
        this.list = list;
    }

    public static class PriceBean
    {
        String treeId;
        String treePrice;


        public PriceBean(String treeId, String treePrice) {
            this.treeId = treeId;
            this.treePrice = treePrice;
        }

        public String getTreeId() {
            return treeId;
        }

        public void setTreeId(String treeId) {
            this.treeId = treeId;
        }

        public String getTreePrice() {
            return treePrice;
        }

        public void setTreePrice(String treePrice) {
            this.treePrice = treePrice;


        }

        @Override
        public String toString() {
            return "PriceBean{" +
                    "treeId='" + treeId + '\'' +
                    ", treePrice='" + treePrice + '\'' +
                    '}';
        }
    }


    @Override
    public String toString() {
        return "PriceForObject{" +
                "list=" + list +
                '}';
    }
}
