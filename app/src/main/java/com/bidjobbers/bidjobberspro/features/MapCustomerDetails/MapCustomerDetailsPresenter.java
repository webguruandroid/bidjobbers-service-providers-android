package com.bidjobbers.bidjobberspro.features.MapCustomerDetails;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class MapCustomerDetailsPresenter <V extends MapCustomerDetailsMvpView> extends BasePresenter<V> implements MapCustomerDeatilsMvpPresenter<V> {

    @Inject
    public MapCustomerDetailsPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


}
