package com.bidjobbers.bidjobberspro.features.prcdetails;

import com.bidjobbers.bidjobberspro.data.network.models.UpdateProfile.UpdateProfileResponse;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface PercDetailsUpdateMvpView extends MvpView {

    void onDeactivate(String data);
    void successfullyGetProfile(getProfileResponse getProfileResponse);
    void successfullyUpdateProfile(UpdateProfileResponse updateProfileResponse);
}
