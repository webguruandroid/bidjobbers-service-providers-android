package com.bidjobbers.bidjobberspro.features.NewArchive;

import com.bidjobbers.bidjobberspro.features.MapCustomerDetails.MapCustomerDetailsMvpView;
import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface NewArchiveMvpPresenter  <V extends NewArchiveMvpView> extends MvpPresenter<V> {
    void getArchiveList(String timePeriod,String currentpag);
}
