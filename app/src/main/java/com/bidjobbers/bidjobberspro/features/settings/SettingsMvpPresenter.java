package com.bidjobbers.bidjobberspro.features.settings;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface SettingsMvpPresenter<V extends SettingsMvpView> extends MvpPresenter<V> {

    void onSuccessfullGetProfile(String language);
    void onLogoutClick();
}
