package com.bidjobbers.bidjobberspro.features.OnTimeScreen.AdvanceBookingTime;

import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface AdvanceTimeForCustomerBookingFragmentMvpView extends BaseFragmentMvpView {

    void setDataFromApi(DataModelForFirstTimeQusAns.AdvanceTimeToBookAJob advanceTimeToBookAJob );

}
