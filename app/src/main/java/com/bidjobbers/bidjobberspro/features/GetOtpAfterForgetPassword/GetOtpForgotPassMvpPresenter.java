package com.bidjobbers.bidjobberspro.features.GetOtpAfterForgetPassword;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface GetOtpForgotPassMvpPresenter <V extends GetOtpForgotPassMvpView> extends MvpPresenter<V> {

  public void onGetOtp(String email);

}
