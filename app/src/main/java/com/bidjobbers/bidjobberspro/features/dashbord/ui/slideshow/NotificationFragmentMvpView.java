package com.bidjobbers.bidjobberspro.features.dashbord.ui.slideshow;

import com.bidjobbers.bidjobberspro.data.network.models.JobStatus.JobStatusResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Notification.NotificationResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface NotificationFragmentMvpView extends BaseFragmentMvpView {

    void successfullyFetchNotificationList(NotificationResponse notificationResponse);
    void successfullyFetchStatus(JobStatusResponse jobStatusResponse);
}
