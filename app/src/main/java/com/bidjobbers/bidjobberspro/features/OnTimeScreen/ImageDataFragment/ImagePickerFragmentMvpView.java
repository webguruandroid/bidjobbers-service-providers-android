package com.bidjobbers.bidjobberspro.features.OnTimeScreen.ImageDataFragment;

import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface ImagePickerFragmentMvpView extends BaseFragmentMvpView {

    void setDataFromApi(DataModelForFirstTimeQusAns.PhotoDescribeProject workPhoto);
}
