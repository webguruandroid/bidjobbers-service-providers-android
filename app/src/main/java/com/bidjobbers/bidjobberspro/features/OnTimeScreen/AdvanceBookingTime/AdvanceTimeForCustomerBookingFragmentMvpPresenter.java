package com.bidjobbers.bidjobberspro.features.OnTimeScreen.AdvanceBookingTime;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface AdvanceTimeForCustomerBookingFragmentMvpPresenter
     <V extends AdvanceTimeForCustomerBookingFragmentMvpView>
        extends BaseFragmentMvpPresenter<V>
{

    void onGetDataFromApi();

}
