package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface TaskItemBodyMvpPresenter <V extends TaskItemBodyMvpView> extends BaseFragmentMvpPresenter<V> {
     void getAllTasks(String currentpage);
}
