package com.bidjobbers.bidjobberspro.features.VerifySignup;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.FirstTimeAccountSettingsActivity;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerifySignupActivity extends BaseActivity implements VerifySignupMvpView {


    //input_houseAddress btn_signUp img_resend

    @Inject
    VerifySignupPresenter<VerifySignupMvpView> verifySignupPresenter;

    @BindView(R.id.input_houseAddress)
    EditText input_houseAddress;

    String emailAddress ="";
    Dialog dialog;
    Context mContext;

    LinearLayout ll_go_veryfy;
    TextView txt;
    String otp_for ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_signup);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        mContext=this;

        Intent intent = getIntent();
        emailAddress = intent.getStringExtra("emailAddress");
        otp_for = intent.getStringExtra("otp_for");

     //   Toast.makeText(this, "otp_for:"+otp_for, Toast.LENGTH_SHORT).show();

        verifySignupPresenter.onAttach(this);

        dialog = new Dialog(mContext, R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_email_verify_sucess);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        txt = (TextView)dialog.findViewById(R.id.txt);


        ll_go_veryfy = dialog.findViewById(R.id.ll_go_veryfy);
        ll_go_veryfy.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(mContext, FirstTimeAccountSettingsActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

    }



    //img_resend

    @OnClick({R.id.img_resend,R.id.btn_signUp })
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_resend:
               //  Toast.makeText(this, "Resend Api", Toast.LENGTH_SHORT).show();
                 verifySignupPresenter.actionResentOtp(emailAddress,
                                                "EVAR","");
                 break;
            case R.id.btn_signUp:
                if(input_houseAddress.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(this, "Please Enter Otp", Toast.LENGTH_SHORT).show();
                }
                else
                {
                  //  Toast.makeText(this, "The Otp:"+input_houseAddress.getText().toString(), Toast.LENGTH_SHORT).show();
                    verifySignupPresenter.actionVerifyOtp(input_houseAddress.getText().toString().trim(), emailAddress, otp_for);
                }
                break;

        }}

    @Override
    public void onOtpValid(boolean error,String data,String auth)
    {

        if(error)
        {
            Toast.makeText(this, data, Toast.LENGTH_SHORT).show();
        }
        else
        {
            if((auth==null)||(auth.trim().equals("")))
            {
                Toast.makeText(this, "Enter Username Password to login.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(mContext, LoginActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
            else
            {
                txt.setText(data);
                dialog.show();
            }


        }

    }

    @Override
    public void onDeactivate(String text) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(VerifySignupActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, 800);
    }
}
