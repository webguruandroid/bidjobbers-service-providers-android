package com.bidjobbers.bidjobberspro.features.profiledetails.ProfileHelper;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.ReviewList.ReviewResponse;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AllReviewAdapter extends RecyclerView.Adapter<AllReviewAdapter.ViewHolder> {

    Context mContext;
    ArrayList<ReviewResponse.ResponseDataBean.ReviewListBean> mValues;
    int size;
    private AllReviewAdapter.ReviewListner listListner;
    public AllReviewAdapter(ArrayList<ReviewResponse.ResponseDataBean.ReviewListBean> item, int size) {

        this.mValues=item;
        this.size=size;
    }

    public void loadList(List<ReviewResponse.ResponseDataBean.ReviewListBean> items) {
        Log.e("Task",items.size()+"");
        mValues.clear();
        mValues.addAll(items);
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mContext = viewGroup.getContext();
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout_review_item, viewGroup, false);
        return new ViewHolder(view);
    }

    public void setAdapterListner(AllReviewAdapter.ReviewListner listner)
    {
        this.listListner=listner;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final ReviewResponse.ResponseDataBean.ReviewListBean mDataBean = mValues.get(i);
        viewHolder.tv_rv_date.setText(mDataBean.getReview_date());
        viewHolder.tv_rv_time.setText(mDataBean.getReview_time());
        viewHolder.tv_rv_desc.setText(mDataBean.getReview());
        viewHolder.tv_rv_name.setText(mDataBean.getCustomer_name());
        viewHolder.tv_rv_rating.setText(mDataBean.getRating());
        viewHolder.tv_rv_image.setText(mDataBean.getCustomer_profile_image());

        Glide.with(mContext).load(mDataBean.getCustomer_profile_image()).into(viewHolder.iv_prc);
    }

    @Override
    public int getItemCount() {
        return mValues.size();

    }

    public interface ReviewListner{
        void onItemClick(String Image,String Desc,String Name,String Date,String Time,String Rating);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.llContainer)
        LinearLayout llContainer;
        @BindView(R.id.tv_rv_desc)
        TextView tv_rv_desc;
        @BindView(R.id.tv_rv_rating)
        TextView tv_rv_rating;
        @BindView(R.id.tv_rv_time)
        TextView tv_rv_time;
        @BindView(R.id.tv_rv_date)
        TextView tv_rv_date;
        @BindView(R.id.tv_rv_name)
        TextView tv_rv_name;
        @BindView(R.id.iv_prc)
        CircleImageView iv_prc;
        @BindView(R.id.tv_rv_image)
        TextView tv_rv_image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            llContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listListner.onItemClick(tv_rv_image.getText().toString().trim(),tv_rv_desc.getText().toString().trim(),tv_rv_name.getText().toString().trim(),tv_rv_date.getText().toString().trim(),tv_rv_time.getText().toString().trim(),tv_rv_rating.getText().toString().trim());
                }
            });
        }
    }


}
