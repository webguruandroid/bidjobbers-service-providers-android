package com.bidjobbers.bidjobberspro.features.login;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface LoginPresenterHelper <V extends LoginViewHelper> extends MvpPresenter<V> {


    public void onClickLogin(String username, String password,String deviceType,String deviceToken);

    public void onForgotPass();

    public void chckAuth();

    public void chekValue();

}
