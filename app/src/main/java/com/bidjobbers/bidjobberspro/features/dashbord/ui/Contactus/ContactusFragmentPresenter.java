package com.bidjobbers.bidjobberspro.features.dashbord.ui.Contactus;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.ContactUs.ContactUsRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ContactUs.ContactUsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileRequest;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

public class ContactusFragmentPresenter <V extends ContactusFragmentMvpView>
        extends BaseFragmentPresenter<V>
        implements ContactusFragmentMvpPresenter<V> {

    @Inject
    public ContactusFragmentPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }

    @Override
    public void onSuccessfullGetProfile(String language) {
        getProfileRequest getProfileRequest1=new getProfileRequest(getDataManager().getLocalValue());

        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().getProfileDetails("Bearer "+getDataManager().getAccess_token(),"application/json","application/json",getProfileRequest1)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<getProfileResponse>()
                {

                    @Override
                    public void onSuccess(getProfileResponse testResponse)
                    {

                        getMvpView().hideLoading();
                        if(testResponse.getResponseCode()==1 )
                        {
                            getMvpView().successfullyGetProfile(testResponse);
                            getDataManager().setCurrentFirstName(testResponse.getResponseData().getName());
                            getDataManager().setCurrentUserEmail(testResponse.getResponseData()
                                    .getEmail());
                        }
                        else if(testResponse.getResponseCode()==401)
                        {
                            getMvpView().onError(testResponse.getResponseText());
                            getMvpView().onDeactivate(testResponse.getResponseText());
                        }
                        else
                        {

                            getMvpView().successfullyGetProfile(testResponse);


                            //Shared prefrence value save access_token.
                        }

                    }
                    @Override
                    public void onError(Throwable e) {

                        getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));

    }

    @Override
    public void onContactUs(String name, String email, String reason, String msg) {
        ContactUsRequest contactUsRequest=new ContactUsRequest(name,email,reason,msg,getDataManager().getLocalValue());

        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().postContactUs("application/json",contactUsRequest)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<ContactUsResponse>()
                {

                    @Override
                    public void onSuccess(ContactUsResponse testResponse)
                    {

                        getMvpView().hideLoading();
                        if(testResponse.getResponseCode()==1 )
                        {
                            getMvpView().onError(testResponse.getResponseText());
                            getMvpView().successfullyContactUs(testResponse);

                        }
                        else if(testResponse.getResponseCode()==401)
                        {
                            getMvpView().onError(testResponse.getResponseText());
                            getMvpView().onDeactivate(testResponse.getResponseText());
                        }
                        else
                        {




                            //Shared prefrence value save access_token.
                        }

                    }
                    @Override
                    public void onError(Throwable e) {

                        getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));

    }
}
