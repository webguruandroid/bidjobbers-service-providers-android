package com.bidjobbers.bidjobberspro.features.BankDetails;

import com.bidjobbers.bidjobberspro.data.network.models.BankDetails.BankDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList.BlockCustomerListResponse;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface BankDetailsMvpView extends MvpView {

    void successfullyUpdateBankDetails(BankDetailsResponse bankDetailsResponse);
    void setDataFromApi(DataModelForFirstTimeQusAns.BankDetails bankDetails);
}
