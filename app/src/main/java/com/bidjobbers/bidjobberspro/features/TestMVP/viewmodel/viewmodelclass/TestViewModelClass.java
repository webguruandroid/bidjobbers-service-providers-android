package com.bidjobbers.bidjobberspro.features.TestMVP.viewmodel.viewmodelclass;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.bidjobbers.bidjobberspro.data.network.models.test.TestResponse;
import com.bidjobbers.bidjobberspro.features.TestMVP.viewmodel.repository.TestResponseRepository;

public class TestViewModelClass extends ViewModel {

    private MutableLiveData<TestResponse> mutableLiveData;
    private TestResponseRepository newsRepository;

    public void init(){
        if (mutableLiveData != null){
            return;
        }
        newsRepository = TestResponseRepository.getInstance();
        mutableLiveData = newsRepository.getResponse();
    }

    public LiveData<TestResponse> getNewsRepository() {
        return mutableLiveData;
    }
}
