package com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessLocatiion;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface BusinessLocationFragmentMvpPresenter <V extends BusinessLocationFragmentMvpView> extends BaseFragmentMvpPresenter<V> {
    void onGetDataFromApi();
    void onGetCityCountry();
    void onGetCity(String ciuntryId);
}
