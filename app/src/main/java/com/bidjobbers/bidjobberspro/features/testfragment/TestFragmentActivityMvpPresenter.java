package com.bidjobbers.bidjobberspro.features.testfragment;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface TestFragmentActivityMvpPresenter <V extends TestFragmentActivityMvpView> extends MvpPresenter<V> {


    public void init(String from);




}
