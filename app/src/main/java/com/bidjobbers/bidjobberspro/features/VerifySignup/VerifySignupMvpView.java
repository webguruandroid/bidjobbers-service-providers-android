package com.bidjobbers.bidjobberspro.features.VerifySignup;

import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface VerifySignupMvpView extends MvpView {

  public void onOtpValid(boolean error,String data,String auth);
  void onDeactivate(String text);

}
