package com.bidjobbers.bidjobberspro.features.InApp;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.subscription.SubscriptionRequest;
import com.bidjobbers.bidjobberspro.data.network.models.subscription.SubscriptionResponse;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

public class InAppPresenter <V extends InAppMvpView> extends BasePresenter<V> implements InAppMvpPresenter<V> {
    @Inject
    public InAppPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void getSubscriptionList() {


        SubscriptionRequest subscriptionRequest=new SubscriptionRequest(getDataManager().getLocalValue());
        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().getSubscription("Bearer "+getDataManager().getAccess_token(),subscriptionRequest)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<SubscriptionResponse>()
                {

                    @Override
                    public void onSuccess(SubscriptionResponse subscriptionResponse)
                    {
                        getMvpView().hideLoading();

                        if(subscriptionResponse.getResponseCode()==1 )
                        {

                            getMvpView().successfullyFetchInAppList(subscriptionResponse);
                        }
                        else
                        {

                            getMvpView().onError(subscriptionResponse.getResponseText());

                        }

                    }
                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));
    }
}
