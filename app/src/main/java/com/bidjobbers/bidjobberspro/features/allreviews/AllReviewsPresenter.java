package com.bidjobbers.bidjobberspro.features.allreviews;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.ReviewList.ReviewRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ReviewList.ReviewResponse;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

public class AllReviewsPresenter  <V extends AllReviewsMvpView> extends BasePresenter<V> implements AllReviewsMvpPresenter<V> {

    @Inject
    public AllReviewsPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void getReview() {

        ReviewRequest reviewRequest=new ReviewRequest(getDataManager().getLocalValue());
        getCompositeDisposable().add(getDataManager().getReviewList( "Bearer "+getDataManager().getAccess_token(),"application/json",
                reviewRequest)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<ReviewResponse>()
                {

                    @Override
                    public void onSuccess(ReviewResponse testResponse)
                    {
                        getMvpView().hideLoading();

                        if(testResponse.getResponseCode()==1 )
                        {
                            getMvpView().successfullyGetReview(testResponse);
                            // getMvpView().onError(testResponse.getResponseText());
                        }
                        else
                        {

                            getMvpView().onError(testResponse.getResponseText());

                        }

                    }
                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));
    }
}
