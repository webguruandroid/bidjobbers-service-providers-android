package com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessLocatiion;

import com.bidjobbers.bidjobberspro.data.network.models.CityCountry.CityCountryResponse;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

import java.util.List;

public interface BusinessLocationFragmentMvpView extends BaseFragmentMvpView {

    void setDataFromApi(DataModelForFirstTimeQusAns.BusinessLocation businessLocation);
    void successfullygetCity(CityCountryResponse cityCountryResponse);
    void successCity(List<CityCountryResponse.ResponseDataBean.MasterDataBean.CityBean> list);
}
