package com.bidjobbers.bidjobberspro.features.NewArchive;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.NewArchive.ArchiveJobList;
import com.bidjobbers.bidjobberspro.data.network.models.NewArchive.NewArchiveList;
import com.bidjobbers.bidjobberspro.features.CustomerProfile.CustomerProfileActivity;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.OnLoadMoreListener;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;

import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewArchiveActivity extends BaseActivity implements NewArchiveMvpView {

    @BindView(R.id.rvArchiveList)
    RecyclerView rvArchiveList;

    @BindView(R.id.llContainer)
    LinearLayout llContainer;
    @BindView(R.id.rlEmpty)
    RelativeLayout rlEmpty;
    @BindView(R.id.iv_mnu)
    ImageView iv_mnu;

    NewArchiveAdapter newArchiveAdapter;
    Context mContext;
    @BindView(R.id.sp_block)
    Spinner sp_block;
    ArrayList<String> list=new ArrayList<>();
    @Inject
    NewArchivePresenter<NewArchiveMvpView> archivePresenter;
    @BindView(R.id.idSwip)
    SwipeRefreshLayout idSwip;
    String currentPage="1";
    ArrayList<ArchiveJobList> productItemList;
    private String selectedYear="";
    private int yr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_archive);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        mContext = this;
        archivePresenter.onAttach(this);
        rvArchiveList.setLayoutManager(new LinearLayoutManager(mContext));


        productItemList = new ArrayList<ArchiveJobList>();
        productItemList.clear();
        archivePresenter.getArchiveList(selectedYear,currentPage);
        newArchiveAdapter = new NewArchiveAdapter(productItemList,mContext,rvArchiveList);
        rvArchiveList.setAdapter(newArchiveAdapter);


        yr= Calendar.getInstance().get(Calendar.YEAR);
        Log.e("Year",yr+"");


        list.add(getResources().getString(R.string.pastsix));
        list.add(yr-1+"");
        list.add(yr-2+"");
        list.add(yr-3+"");
        list.add(yr-4+"");
        list.add(yr-5+"");
        setSpinner();
        iv_mnu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                //  overridePendingTransition( R.anim.slide_from_top,R.anim.slide_in_top);
            }
        });
        newArchiveAdapter.setmListener(new NewArchiveAdapter.AddButtonListener() {
            @Override
            public void onItemClick(int pos, String StatusId, String taskId) {
                Intent i = new Intent(mContext, CustomerProfileActivity.class);
                i.putExtra("status", StatusId);
                i.putExtra("taskId", taskId);
                i.putExtra("bidId", "0");
                i.putExtra("Tag","Foreground");
                 startActivity(i);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        newArchiveAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                try {
                    Log.e("load", "onLoadMore: "+ "called " );
                    if(!currentPage.equals("")){

                        productItemList.add(null);
                        //productItemList = new ArrayList<ShopProductResponse.ResponseDataBean.ProductListBean>();
                        rvArchiveList.post(new Runnable() {
                            @Override
                            public void run() {
                                newArchiveAdapter.notifyItemInserted(productItemList.size() -1);
                                archivePresenter.getArchiveList(selectedYear,currentPage);
                            }
                        });

                    }else {

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        if (idSwip.isRefreshing()) {
            idSwip.setRefreshing(false);
        }

        idSwip.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.e("tag","referesh");
                currentPage="1";
                productItemList.clear();
                archivePresenter.getArchiveList(selectedYear,currentPage);
            }
        });
    }

    private void setSpinner()
    {

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        //set the view for the Drop down list
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the ArrayAdapter to the spinner
        sp_block.setAdapter(dataAdapter);
        sp_block.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==0){
                    selectedYear = "Past 6 months";
                }
                else {
                    selectedYear = list.get(i);
                }
                archivePresenter.getArchiveList(selectedYear,currentPage);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                selectedYear=list.get(0);
                archivePresenter.getArchiveList(selectedYear,currentPage);
            }

        });
    }

    @Override
    public void successfullyGetArchiveCustomer(NewArchiveList archiveListResponse) {

        if(archiveListResponse.getResponseCode()==1)
        {
            if (idSwip.isRefreshing()) {
                idSwip.setRefreshing(false);
            }
            llContainer.setVisibility(View.VISIBLE);
            rlEmpty.setVisibility(View.GONE);
           // newArchiveAdapter.loadList(archiveListResponse.getResponseData().getArchiveJobList());

            if(currentPage.equals("1")){

                productItemList.clear();
                productItemList.addAll(archiveListResponse.getResponseData().getArchiveJobList());
            }else{

                productItemList.remove(productItemList.size() - 1);
                newArchiveAdapter.notifyItemRemoved(productItemList.size());

                productItemList.addAll(archiveListResponse.getResponseData().getArchiveJobList());
                //mAdapter.notifyDataSetChanged();
            }

            newArchiveAdapter.notifyDataSetChanged();
            newArchiveAdapter.setLoaded();
            currentPage =archiveListResponse.getResponseData().getPagination().getNextPage();

        }
        else
        {
            llContainer.setVisibility(View.GONE);
            rlEmpty.setVisibility(View.VISIBLE);
        }
    }
}
