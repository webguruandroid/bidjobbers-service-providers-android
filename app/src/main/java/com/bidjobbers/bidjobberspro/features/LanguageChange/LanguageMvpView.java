package com.bidjobbers.bidjobberspro.features.LanguageChange;

import com.bidjobbers.bidjobberspro.data.network.models.SetLocal.SetLocalResponse;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface LanguageMvpView extends MvpView {

    void successfullySEtLocal(SetLocalResponse setLocalResponse);
}
