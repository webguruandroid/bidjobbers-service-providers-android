package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.Cancelled;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.CacelledTask.CancelledTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.CacelledTask.CancelledTaskResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class CancelledBodyPresenter<V extends CancelledBodyMvpView>
        extends BaseFragmentPresenter<V>
        implements CancelledBodyMvpPresenter<V> {


    @Inject
    public CancelledBodyPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }

    @Override
    public void getAllTasks(String currentpage) {


        CancelledTaskRequest cancelledTaskRequest=new CancelledTaskRequest(getDataManager().getLocalValue(),currentpage);


        getCompositeDisposable().add(getDataManager().postCancelledTask("Bearer "+getDataManager().getAccess_token(),"application/json",cancelledTaskRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CancelledTaskResponse>() {
                    @Override
                    public void onSuccess(CancelledTaskResponse scheduletaskResponse) {



                        getMvpView().hideLoading();
                        if(scheduletaskResponse.getResponseCode()==1) {
                            getMvpView().allTasksName(scheduletaskResponse);
                        }
                        if(scheduletaskResponse.getResponseCode()==401) {
                            getMvpView().onError(scheduletaskResponse.getResponseText());
                            getMvpView().onDeactivate(scheduletaskResponse.getResponseText());
                        }
                        else
                        {
                            getMvpView().allTasksName(scheduletaskResponse);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));

    }
}
