package com.bidjobbers.bidjobberspro.features.resetpassword;

import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface ResetPasswordMvpView extends MvpView {

    public void goToLoginScreen();
    void doDeActivate(String data);
}
