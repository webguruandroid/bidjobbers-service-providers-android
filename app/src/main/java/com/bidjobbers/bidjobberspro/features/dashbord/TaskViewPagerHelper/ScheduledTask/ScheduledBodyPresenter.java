package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.ScheduledTask;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.ScheduleTask.ScheduletaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ScheduleTask.ScheduletaskResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ScheduledBodyPresenter<V extends ScheduledBodyMvpView>
        extends BaseFragmentPresenter<V>
        implements ScheduledBodyMvpPresenter<V> {


    @Inject
    public ScheduledBodyPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }

    @Override
    public void getAllTasks(String currentpage) {

        if(getMvpView().isNetworkConnected()) {
        getMvpView().showLoading();
        ScheduletaskRequest scheduletaskRequest=new ScheduletaskRequest(getDataManager().getLocalValue(),currentpage);


        getCompositeDisposable().add(getDataManager().getScheduledTask("Bearer "+getDataManager().getAccess_token(),"application/json",scheduletaskRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ScheduletaskResponse>() {
                    @Override
                    public void onSuccess(ScheduletaskResponse scheduletaskResponse) {

                        getMvpView().hideLoading();
                        if(scheduletaskResponse.getResponseCode()==1) {
                            getMvpView().allTasksName(scheduletaskResponse);
                        }
                        else if(scheduletaskResponse.getResponseCode()==401)
                        {
                            getMvpView().onError(scheduletaskResponse.getResponseText());
                            getMvpView().onDeactivate(scheduletaskResponse.getResponseText());
                        }
                        else
                        {
                            getMvpView().allTasksName(scheduletaskResponse);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));
        }
        else
        {
            getMvpView().hideLoading();
            getMvpView().showAlert("No internet Connection");
        }

    }
}
