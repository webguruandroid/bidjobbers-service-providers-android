package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.Completed;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.CompletedTask.CompletedTaskResponse;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.features.CustomerDetails.CustomerDetailsActivity;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.OnLoadMoreListener;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.TaskBean;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CompletedBodyFragment extends BaseFragment implements CompletedBodyMvpView {

    Context mContext;
    @BindView(R.id.recycler_task_list)
    RecyclerView recycler_product_list;


    @BindView(R.id.rlEmpty)
    SwipeRefreshLayout rlEmpty;
    @BindView(R.id.llContainer)
    LinearLayout llContainer;
    @BindView(R.id.tv_notask)
    TextView tv_notask;

    String id="";

    String cat_id = "0";
    String page = "1";
    String next_page="";

    LinearLayout ll_footer,loader;
    GridLayoutManager gridLayoutManager;
    Handler handler;

    String userID="186",currentPage="1";
    String quoteId= "";

    CompletedTaskListingAdapter taskListingAdapter;
    ArrayList<CompletedTaskResponse.ResponseDataBean.TaskDataBean> productItemList;
    ArrayList<TaskBean>  taskBeans;
    @BindView(R.id.idSwip)
    SwipeRefreshLayout idSwip;
    @Inject
    CompletedBodyPresenter<CompletedBodyMvpView> taskItemBodyPresenter;
    private ProgressDialog mProgressDialog;

    public CompletedBodyFragment()
    {

    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        try {
            //cartUpdateListener = (CartUpdateListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement onFragmentChangeListener");
        }
    }

    @Override
    protected void setUp(View view) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v= inflater.inflate(R.layout.layout_fragment_task_listing, container, false);
        setUnBinder(ButterKnife.bind(this,v));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }


        return v;

    }
    @Override
    public void onResume() {
        super.onResume();
        //taskItemBodyPresenter.getAllTasks();
    }



    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String  status ="";
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            status = bundle.getString("id", "1");
        }

        mContext = getContext();
        cat_id =  getArguments().getString("id");
        tv_notask.setText(getResources().getString(R.string.nojobcompleted));
        recycler_product_list.setLayoutManager(new LinearLayoutManager(getActivity()));
        taskItemBodyPresenter.onAttach(this);

        productItemList = new ArrayList<CompletedTaskResponse.ResponseDataBean.TaskDataBean>();
        currentPage="1";
        productItemList.clear();
        taskItemBodyPresenter.getAllTasks(currentPage);
        taskListingAdapter = new CompletedTaskListingAdapter(productItemList,mContext,recycler_product_list);
        recycler_product_list.setAdapter(taskListingAdapter);


        taskListingAdapter.setmListener(new CompletedTaskListingAdapter.AddButtonListener() {
            @Override
            public void onItemClick(int pos,String StatusId, String taskId) {

                Intent i = new Intent(getActivity(), CustomerDetailsActivity.class);
                i.putExtra("status", StatusId);
                i.putExtra("taskId", taskId);
                i.putExtra("Tag","Foreground");
                getActivity().startActivity(i);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        taskListingAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                try {
                    Log.e("load", "onLoadMore: "+ "called " );
                    if(!currentPage.equals("")){

                        productItemList.add(null);
                        //productItemList = new ArrayList<ShopProductResponse.ResponseDataBean.ProductListBean>();
                        recycler_product_list.post(new Runnable() {
                            @Override
                            public void run() {
                                taskListingAdapter.notifyItemInserted(productItemList.size() -1);
                                taskItemBodyPresenter.getAllTasks(currentPage);
                            }
                        });

                    }else {

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        idSwip.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.e("tag","referesh");
                currentPage="1";
                productItemList.clear();
                taskItemBodyPresenter.getAllTasks(currentPage);
            }
        });


        rlEmpty.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.e("tag","referesh");
                currentPage="1";
                productItemList.clear();
                taskItemBodyPresenter.getAllTasks(currentPage);
            }
        });

    }


    @Override
    public void allTasksName(CompletedTaskResponse completedTaskResponse) {


        if(completedTaskResponse.getResponseCode()==1) {
            if (completedTaskResponse.getResponseData().getTaskData().size() > 0) {
                if (idSwip.isRefreshing()) {
                    idSwip.setRefreshing(false);
                }
                if (rlEmpty.isRefreshing()) {
                    rlEmpty.setRefreshing(false);
                }
                llContainer.setVisibility(View.VISIBLE);
                rlEmpty.setVisibility(View.GONE);
                if(currentPage.equals("1")){
                    Log.e("Task", completedTaskResponse.getResponseData().getTaskData().size() + "");
                    productItemList.clear();
                    productItemList.addAll(completedTaskResponse.getResponseData().getTaskData());
                }else{

                    productItemList.remove(productItemList.size() - 1);
                    taskListingAdapter.notifyItemRemoved(productItemList.size());

                    productItemList.addAll(completedTaskResponse.getResponseData().getTaskData());
                    //mAdapter.notifyDataSetChanged();
                }

                taskListingAdapter.notifyDataSetChanged();
                taskListingAdapter.setLoaded();
                currentPage = completedTaskResponse.getResponseData().getPagination().getNext_page();

            } else {
                if (idSwip.isRefreshing()) {
                    idSwip.setRefreshing(false);
                }
                if (rlEmpty.isRefreshing()) {
                    rlEmpty.setRefreshing(false);
                }
                llContainer.setVisibility(View.GONE);
                rlEmpty.setVisibility(View.VISIBLE);
                productItemList.clear();
                taskListingAdapter.notifyDataSetChanged();
            }


        }
        else if(completedTaskResponse.getResponseCode()==401)
        {

            if(idSwip!=null&&rlEmpty!=null) {
                if (idSwip.isRefreshing()) {
                    idSwip.setRefreshing(false);
                }
                if (rlEmpty.isRefreshing()) {
                    rlEmpty.setRefreshing(false);
                }

            }
            Toast.makeText(getActivity(),completedTaskResponse.getResponseText(),Toast.LENGTH_LONG).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    Intent i = new Intent(getActivity(), LoginActivity.class);
                    startActivity(i);
                    getActivity().finish();
                }
            }, 800);

        }
        else
        {


            if (idSwip.isRefreshing()) {
                idSwip.setRefreshing(false);
            }
            if (rlEmpty.isRefreshing()) {
                rlEmpty.setRefreshing(false);
            }
            llContainer.setVisibility(View.GONE);
            rlEmpty.setVisibility(View.VISIBLE);
            productItemList.clear();
            taskListingAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onDeactivate(String data) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
                getActivity().finish();
            }
        }, 800);
    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        idSwip.removeAllViews();
    }
}