package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.Completed;

import com.bidjobbers.bidjobberspro.data.network.models.CompletedTask.CompletedTaskResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface CompletedBodyMvpView extends BaseFragmentMvpView {
     void  allTasksName(CompletedTaskResponse arrayList);
     void onDeactivate(String data);

}
