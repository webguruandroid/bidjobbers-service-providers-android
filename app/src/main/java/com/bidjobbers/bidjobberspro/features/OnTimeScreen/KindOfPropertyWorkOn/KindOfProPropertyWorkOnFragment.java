package com.bidjobbers.bidjobberspro.features.OnTimeScreen.KindOfPropertyWorkOn;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.features.testfragment.LawnAdapter;
import com.bidjobbers.bidjobberspro.features.testfragment.LawnBean;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link KindOfProPropertyWorkOnFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link KindOfProPropertyWorkOnFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class KindOfProPropertyWorkOnFragment extends BaseFragment implements KindOfProPropertyWorkOnFragmentMvpView {


    @BindView(R.id.rv_jobs_images)
    RecyclerView rv_jobs_images;
    @BindView(R.id.cbLawnService)
    CheckBox cbLawnService;
    @BindView(R.id.cbTreeCutting)
    CheckBox cbTreeCutting;


    String lawnServiceId="0",OneId;
    String mowALownId="0",TwoId;
    String areateALawnId="0",ThreeAId;
    String yardWasteId="0",FourId;

    String treeCuttingId="0",FiveId;
    String treeStrubsId="0",SixId;
    String treeStrumpsId="0",SevenId;
    String yardWasteTreeCuttingId="0",EightId;
    String str;

    ArrayList<LawnBean> listdata= new ArrayList<>();
    LawnAdapter lawnAdapter;

    boolean isLawn,isTree;

    @Inject
    KindOfProPropertyWorkOnFragmentPresenter<KindOfProPropertyWorkOnFragmentMvpView> kindOfProPropertyWorkOnFragmentPresenter;

    public KindOfProPropertyWorkOnFragment(){

    }

    public static KindOfProPropertyWorkOnFragment newInstance(/*Context context*/) {
        KindOfProPropertyWorkOnFragment fragment = new KindOfProPropertyWorkOnFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {

             str = bundle.getString("FROMG");
            Log.e("FROMG", str);

        }


    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_lawn, container, false);
        setUnBinder(ButterKnife.bind(this,v));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }

        kindOfProPropertyWorkOnFragmentPresenter.onAttach(this);

        lawnAdapter = new LawnAdapter(listdata);
        LinearLayoutManager layoutManagerBrand = new LinearLayoutManager(v.getContext());
        layoutManagerBrand.setOrientation(LinearLayoutManager.HORIZONTAL);
        rv_jobs_images.setLayoutManager(layoutManagerBrand);
        rv_jobs_images.setAdapter(lawnAdapter);

        kindOfProPropertyWorkOnFragmentPresenter.onGetDataFromApi();

        setDataFunctionKindofProperty();
        if(str.equals("2"))
        {

        }
        return v;
    }




    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDetach()
    {
        super.onDetach();

    }

    @Override
    public void getDataFromSP(boolean isLawnServiceSelected, String lawnServiceId, boolean ismowALownSelected, String mowALownId, boolean isAreateALawn, String areateALawnId, boolean isyardWaste, String yardWasteId, boolean isTreeCuttingSelected, String treeCuttingId, boolean isTreeStrubsSelected, String treeStrubsId, boolean isTreeStrumps, String treeStrumpsId, boolean isyardWasteTreeCutting, String yardWasteTreeCuttingId) {

    }

    @Override
    public void setDataFromApi(final DataModelForFirstTimeQusAns.KindOfServiceDoProvide kindOfServiceDoProvide) {
       // Toast.makeText(getActivity(),kindOfServiceDoProvide.isLawnServiceSelected()+"-"+kindOfServiceDoProvide.isTreeCuttingSelected(),Toast.LENGTH_SHORT).show();


        Log.e("KINDofPropertyWorkOn",kindOfServiceDoProvide.toString());
        listdata.clear();

        rv_jobs_images.setVisibility(View.GONE);

        if(kindOfServiceDoProvide.isLawnServiceSelected()==true&&kindOfServiceDoProvide.isTreeCuttingSelected()==false)
        {

            lawnServiceId="1";
            OneId=kindOfServiceDoProvide.getLawnServiceId();
            cbLawnService.setChecked(true);
            rv_jobs_images.setVisibility(View.VISIBLE);
            listdata.add(new LawnBean(kindOfServiceDoProvide.getYearALawnImage(),kindOfServiceDoProvide.getYearALawnName(),kindOfServiceDoProvide.getYearALawnDescription(),kindOfServiceDoProvide.getYardWasteId()));
            listdata.add(new LawnBean(kindOfServiceDoProvide.getMowAMowALawnImage(),kindOfServiceDoProvide.getMowALawnName(),kindOfServiceDoProvide.getMowALawnDescription(),kindOfServiceDoProvide.getMowALownId()));
            listdata.add(new LawnBean(kindOfServiceDoProvide.getAreateALawnImage(),kindOfServiceDoProvide.getAreateALawnName(),kindOfServiceDoProvide.getAreateALawnDescription(),kindOfServiceDoProvide.getAreateALawnId()));





            lawnAdapter.notifyDataSetChanged();
        }


       else if(kindOfServiceDoProvide.isTreeCuttingSelected()==true&&kindOfServiceDoProvide.isLawnServiceSelected()==false)
        {

            treeCuttingId="1";
            TwoId=kindOfServiceDoProvide.getTreeCuttingId();
            cbTreeCutting.setChecked(true);
            rv_jobs_images.setVisibility(View.VISIBLE);
            listdata.add(new LawnBean(kindOfServiceDoProvide.getTreeStrubsImage(),kindOfServiceDoProvide.getTreeStrubsName(),kindOfServiceDoProvide.getTreeStrubsDescription(),kindOfServiceDoProvide.getTreeStrubsId()));
            listdata.add(new LawnBean(kindOfServiceDoProvide.getTreeStrumpsImage(),kindOfServiceDoProvide.getTreeStrumpsName(),kindOfServiceDoProvide.getTreeStrumpsDescription(),kindOfServiceDoProvide.getTreeStrumpsId()));
            listdata.add(new LawnBean(kindOfServiceDoProvide.getYardWasteTreeImage(),kindOfServiceDoProvide.getYardWasteTreeName(),kindOfServiceDoProvide.getYardWasteTreeDescription(),kindOfServiceDoProvide.getYardWasteTreeCuttingId()));
            lawnAdapter.notifyDataSetChanged();
        }
        else if(kindOfServiceDoProvide.isTreeCuttingSelected()==true&&kindOfServiceDoProvide.isLawnServiceSelected()==true)
        {



            lawnServiceId="1";
            treeCuttingId="1";
            OneId=kindOfServiceDoProvide.getLawnServiceId();
            TwoId=kindOfServiceDoProvide.getTreeCuttingId();
            cbLawnService.setChecked(true);
            rv_jobs_images.setVisibility(View.VISIBLE);
            listdata.add(new LawnBean(kindOfServiceDoProvide.getYearALawnImage(),kindOfServiceDoProvide.getYearALawnName(),kindOfServiceDoProvide.getYearALawnDescription(),kindOfServiceDoProvide.getYardWasteId()));
            listdata.add(new LawnBean(kindOfServiceDoProvide.getMowAMowALawnImage(),kindOfServiceDoProvide.getMowALawnName(),kindOfServiceDoProvide.getMowALawnDescription(),kindOfServiceDoProvide.getMowALownId()));
            listdata.add(new LawnBean(kindOfServiceDoProvide.getAreateALawnImage(),kindOfServiceDoProvide.getAreateALawnName(),kindOfServiceDoProvide.getAreateALawnDescription(),kindOfServiceDoProvide.getAreateALawnId()));


            cbTreeCutting.setChecked(true);

            listdata.add(new LawnBean(kindOfServiceDoProvide.getTreeStrubsImage(),kindOfServiceDoProvide.getTreeStrubsName(),kindOfServiceDoProvide.getTreeStrubsDescription(),kindOfServiceDoProvide.getTreeStrubsId()));
            listdata.add(new LawnBean(kindOfServiceDoProvide.getTreeStrumpsImage(),kindOfServiceDoProvide.getTreeStrumpsName(),kindOfServiceDoProvide.getTreeStrumpsDescription(),kindOfServiceDoProvide.getTreeStrumpsId()));
            listdata.add(new LawnBean(kindOfServiceDoProvide.getYardWasteTreeImage(),kindOfServiceDoProvide.getYardWasteTreeName(),kindOfServiceDoProvide.getYardWasteTreeDescription(),kindOfServiceDoProvide.getYardWasteTreeCuttingId()));
            lawnAdapter.notifyDataSetChanged();
        }
        else
        {


            OneId="";
            TwoId="";
            lawnServiceId="0";
            treeCuttingId="0";
            cbTreeCutting.setChecked(false);
            listdata.clear();
            lawnAdapter.notifyDataSetChanged();
        }
        cbLawnService.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isLawn=isChecked;
                if(isChecked) {

                   // Toast.makeText(getActivity(), kindOfServiceDoProvide.getAreateALawnName()+"-"+kindOfServiceDoProvide.getMowALawnName()+"-"+kindOfServiceDoProvide.getYearALawnName(), Toast.LENGTH_SHORT).show();
                    rv_jobs_images.setVisibility(View.VISIBLE);
                    listdata.add(new LawnBean(kindOfServiceDoProvide.getYearALawnImage(),kindOfServiceDoProvide.getYearALawnName(),kindOfServiceDoProvide.getYearALawnDescription(),kindOfServiceDoProvide.getYardWasteId()));
                    listdata.add(new LawnBean(kindOfServiceDoProvide.getMowAMowALawnImage(),kindOfServiceDoProvide.getMowALawnName(),kindOfServiceDoProvide.getMowALawnDescription(),kindOfServiceDoProvide.getMowALownId()));
                    listdata.add(new LawnBean(kindOfServiceDoProvide.getAreateALawnImage(),kindOfServiceDoProvide.getAreateALawnName(),kindOfServiceDoProvide.getAreateALawnDescription(),kindOfServiceDoProvide.getAreateALawnId()));
                    lawnAdapter.notifyDataSetChanged();

                    lawnServiceId="1";
                    OneId=kindOfServiceDoProvide.getLawnServiceId();
                }
                else
                {

                    rv_jobs_images.setVisibility(View.VISIBLE);

                    if(isTree)
                    {
                        listdata.clear();
                        listdata.add(new LawnBean(kindOfServiceDoProvide.getTreeStrubsImage(),kindOfServiceDoProvide.getTreeStrubsName(),kindOfServiceDoProvide.getTreeStrubsDescription(),kindOfServiceDoProvide.getTreeStrubsId()));
                        listdata.add(new LawnBean(kindOfServiceDoProvide.getTreeStrumpsImage(),kindOfServiceDoProvide.getTreeStrumpsName(),kindOfServiceDoProvide.getTreeStrumpsDescription(),kindOfServiceDoProvide.getTreeStrumpsId()));
                        listdata.add(new LawnBean(kindOfServiceDoProvide.getYardWasteTreeImage(),kindOfServiceDoProvide.getYardWasteTreeName(),kindOfServiceDoProvide.getYardWasteTreeDescription(),kindOfServiceDoProvide.getYardWasteTreeCuttingId()));
                        lawnAdapter.notifyDataSetChanged();


                        treeCuttingId="1";
                        TwoId=kindOfServiceDoProvide.getTreeCuttingId();
                    }
                    else {
                        listdata.clear();
                        treeCuttingId="0";
                        TwoId="";
                    }
                    lawnAdapter.notifyDataSetChanged();
                }
            }
        });
        cbTreeCutting.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isTree=isChecked;
                if(isChecked) {

                    treeCuttingId="1";
                    rv_jobs_images.setVisibility(View.VISIBLE);

                    listdata.add(new LawnBean(kindOfServiceDoProvide.getTreeStrubsImage(),kindOfServiceDoProvide.getTreeStrubsName(),kindOfServiceDoProvide.getTreeStrubsDescription(),kindOfServiceDoProvide.getTreeStrubsId()));
                    listdata.add(new LawnBean(kindOfServiceDoProvide.getTreeStrumpsImage(),kindOfServiceDoProvide.getTreeStrumpsName(),kindOfServiceDoProvide.getTreeStrumpsDescription(),kindOfServiceDoProvide.getTreeStrumpsId()));
                    listdata.add(new LawnBean(kindOfServiceDoProvide.getYardWasteTreeImage(),kindOfServiceDoProvide.getYardWasteTreeName(),kindOfServiceDoProvide.getYardWasteTreeDescription(),kindOfServiceDoProvide.getYardWasteTreeCuttingId()));
                    lawnAdapter.notifyDataSetChanged();

                }
                else
                {
                    treeCuttingId="0";
                    rv_jobs_images.setVisibility(View.VISIBLE);
                    if(isLawn){
                        rv_jobs_images.setVisibility(View.VISIBLE);
                        listdata.clear();
                        listdata.add(new LawnBean(kindOfServiceDoProvide.getYearALawnImage(),kindOfServiceDoProvide.getYearALawnName(),kindOfServiceDoProvide.getYearALawnDescription(),kindOfServiceDoProvide.getYardWasteId()));
                        listdata.add(new LawnBean(kindOfServiceDoProvide.getMowAMowALawnImage(),kindOfServiceDoProvide.getMowALawnName(),kindOfServiceDoProvide.getMowALawnDescription(),kindOfServiceDoProvide.getMowALownId()));
                        listdata.add(new LawnBean(kindOfServiceDoProvide.getAreateALawnImage(),kindOfServiceDoProvide.getAreateALawnName(),kindOfServiceDoProvide.getAreateALawnDescription(),kindOfServiceDoProvide.getAreateALawnId()));
                        lawnAdapter.notifyDataSetChanged();
                    }
                    else
                    {
                        listdata.clear();
                    }

                    lawnAdapter.notifyDataSetChanged();
                }
            }
        });

        if(kindOfServiceDoProvide.isLawnServiceSelected()==true&&kindOfServiceDoProvide.isTreeCuttingSelected()==false) {


            if (kindOfServiceDoProvide.isIsyardWaste()) {
                listdata.get(0).setCheck(true);
                yardWasteId="1";
            } else {
                listdata.get(0).setCheck(false);
                yardWasteId="0";
            }

            if (kindOfServiceDoProvide.isIsmowALownSelected()) {
                listdata.get(1).setCheck(true);
                mowALownId="1";

            } else {
                listdata.get(1).setCheck(false);
                mowALownId="0";
            }

            if (kindOfServiceDoProvide.isAreateALawn()) {
                listdata.get(2).setCheck(true);
                areateALawnId="1";

            } else {
                listdata.get(2).setCheck(false);
                areateALawnId="0";
            }

        }
        if(kindOfServiceDoProvide.isTreeCuttingSelected()==true&&kindOfServiceDoProvide.isLawnServiceSelected()==false) {

            if (kindOfServiceDoProvide.isTreeStrubsSelected()) {
                listdata.get(0).setCheck(true);
                treeStrubsId="1";
            } else {
                listdata.get(0).setCheck(false);
                treeStrubsId="0";
            }
            if (kindOfServiceDoProvide.isTreeStrumps()) {
                listdata.get(1).setCheck(true);
                treeStrumpsId="1";
            } else {
                listdata.get(1).setCheck(false);
                treeStrumpsId="0";
            }
            if (kindOfServiceDoProvide.isIsyardWasteTreeCutting()) {
                listdata.get(2).setCheck(true);
                yardWasteTreeCuttingId="1";
            } else {
                listdata.get(2).setCheck(false);
                yardWasteTreeCuttingId="0";
            }
          }

            if(kindOfServiceDoProvide.isTreeCuttingSelected()==true&&kindOfServiceDoProvide.isLawnServiceSelected()==true)
            {


                if (kindOfServiceDoProvide.isIsyardWaste()) {
                    listdata.get(0).setCheck(true);
                    yardWasteId="1";
                } else {
                    listdata.get(0).setCheck(false);
                    yardWasteId="0";
                }

                if (kindOfServiceDoProvide.isIsmowALownSelected()) {
                    listdata.get(1).setCheck(true);
                    mowALownId="1";
                } else {
                    listdata.get(1).setCheck(false);
                    mowALownId="0";
                }

                if (kindOfServiceDoProvide.isAreateALawn()) {
                    listdata.get(2).setCheck(true);
                    areateALawnId="1";
                } else {
                    listdata.get(2).setCheck(false);
                    areateALawnId="0";
                }
                if (kindOfServiceDoProvide.isTreeStrubsSelected()) {
                    listdata.get(3).setCheck(true);
                    treeStrubsId="1";
                } else {
                    listdata.get(3).setCheck(false);
                    treeStrubsId="0";
                }
                if (kindOfServiceDoProvide.isTreeStrumps()) {
                    listdata.get(4).setCheck(true);
                    treeStrumpsId="1";
                } else {
                    listdata.get(4).setCheck(false);
                    treeStrumpsId="0";
                }
                if (kindOfServiceDoProvide.isIsyardWasteTreeCutting()) {
                    listdata.get(5).setCheck(true);
                    yardWasteTreeCuttingId="1";
                } else {
                    listdata.get(5).setCheck(false);
                    yardWasteTreeCuttingId="0";
                }


            }


        lawnAdapter.setLawnListener(new LawnAdapter.LawnListener() {
            @Override
            public void chengeCheck(int pos, boolean chk) {
                listdata.get(pos).setCheck(!chk);

                if(pos==0)
                {
                    if(!chk) {
                        yardWasteId="1";
                    }
                    else
                    {
                        yardWasteId="0";
                    }
                  //  Toast.makeText(getActivity(),"--"+pos+"--"+chk+"-"+yardWasteId,Toast.LENGTH_SHORT).show();
                }
                else if(pos==1)
                {
                    if(!chk){
                        mowALownId="1";
                    }
                    else {
                        mowALownId ="0";
                    }

                }
                else if(pos==2)
                {
                    if(!chk){
                        areateALawnId="1";
                    }else
                    {
                        areateALawnId="0";
                    }

                }
                else if(pos==3)
                {
                    if(!chk){
                        treeStrubsId="1";
                    }
                    else
                    {
                        treeStrubsId="0";
                    }

                }
                else if(pos==4)
                {
                    if(!chk){
                        treeStrumpsId="1";
                    }
                    else {
                        treeStrumpsId="0";
                    }

                }
                else if(pos==5)
                {
                    if(!chk){
                        yardWasteTreeCuttingId="1";
                    }
                    else {
                        yardWasteTreeCuttingId="0";
                    }

                }

                lawnAdapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



    public  kindofPropertyObject setDataFunctionKindofProperty()
    {

        kindofPropertyObject object = new kindofPropertyObject(lawnServiceId, mowALownId, areateALawnId, yardWasteId, treeCuttingId, treeStrubsId, treeStrumpsId, yardWasteTreeCuttingId);
        Log.e("KINDOF",object.toString());
            return object;

    }


}
