package com.bidjobbers.bidjobberspro.features.OnTimeScreen.ImageDataFragment.ImagePickerHelper;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class TakeImageAdapterAC extends RecyclerView.Adapter<TakeImageAdapterAC.ViewHolder>{

    ArrayList<String> a;
    Context context;
    CellListener cellListener;
    List<PlaceWorkModel> picturesList;
    Uri photo;
    int pos=77;

    private final ArrayList<String> mValues;
    Context mContext;

    public TakeImageAdapterAC(ArrayList<String> itemsData) {
        this.mValues = itemsData;

    }

    public void setAdapterListener(CellListener mListener) {
        this.cellListener = mListener;
    }

    public void sendImage(Uri photo, int pos) {
        this.photo = photo;
        this.pos=pos;
    }

    public interface CellListener {
        public void onItemClick(int pos, ArrayList<String> items);
    }

    public void updateList(List<PlaceWorkModel> picturesList) {
        this.picturesList = picturesList;
        notifyDataSetChanged();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        mContext = parent.getContext();
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_take_image_ac, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }


    public void loadList(List<String> items) {
        mValues.clear();
        mValues.addAll(items);
        notifyDataSetChanged();
    }
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {


     //   Toast.makeText(context,a.get(position),Toast.LENGTH_SHORT).show();

        Glide.with(mContext).
                load(mValues.get(position))
               // .placeholder(R.drawable.image_placeholder)
                .into(viewHolder.imgTourismHr);


    }

    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView imgTourismHr;
        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);

            imgTourismHr=(ImageView)itemLayoutView.findViewById(R.id.imgTourismHr);

            itemLayoutView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cellListener.onItemClick(getAdapterPosition(),mValues);
                }
            });
        }
    }


}
