package com.bidjobbers.bidjobberspro.features.prcdetails;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.UpdateProfile.UpdateProfileResponse;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileRequest;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class PercDetailsUpdatePresenter <V extends PercDetailsUpdateMvpView> extends BasePresenter<V> implements PercDetailsUpdateMvpPresenter<V> {

    @Inject
    public PercDetailsUpdatePresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onSuccessfullGetProfile(String language) {
        getProfileRequest getProfileRequest1=new getProfileRequest(language);

        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().getProfileDetails("Bearer "+getDataManager().getAccess_token(),"application/json","application/json",getProfileRequest1)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<getProfileResponse>()
                {

                    @Override
                    public void onSuccess(getProfileResponse testResponse)
                    {

                        getMvpView().hideLoading();
                        if(testResponse.getResponseCode()==0 )
                        {
                            getMvpView().successfullyGetProfile(testResponse);
                            getDataManager().setCurrentFirstName(testResponse.getResponseData().getName());
                            getDataManager().setCurrentUserEmail(testResponse.getResponseData()
                            .getEmail());
                        }
                        else if(testResponse.getResponseCode()==401)
                        {
                            getMvpView().onError(testResponse.getResponseText());
                            getMvpView().onDeactivate(testResponse.getResponseText());
                        }
                        else
                        {

                            getMvpView().successfullyGetProfile(testResponse);


                            //Shared prefrence value save access_token.
                        }

                    }
                    @Override
                    public void onError(Throwable e) {

                        getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));


    }

    @Override
    public void onUpdateProfile(MultipartBody.Part img_body, String name, String phone, String local) {




        RequestBody firstAndLastNameData = RequestBody.create(MediaType.parse("multipart/form-data"), name);
        RequestBody phoneNumberData = RequestBody.create(MediaType.parse("multipart/form-data"), phone);
        RequestBody localData = RequestBody.create(MediaType.parse("multipart/form-data"),  getDataManager().getLocalValue());




        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().postProfileUpdate( "Bearer "+getDataManager().getAccess_token(),
               firstAndLastNameData,
               phoneNumberData,localData,img_body)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<UpdateProfileResponse>()
                {

                    @Override
                    public void onSuccess(UpdateProfileResponse testResponse)
                    {
                        getMvpView().hideLoading();

                        if(testResponse.getResponseCode()==1 )
                        {
                            getMvpView().successfullyUpdateProfile(testResponse);
                        }
                        else
                        {

                            getMvpView().successfullyUpdateProfile(testResponse);

                        }

                    }
                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));





    }
}
