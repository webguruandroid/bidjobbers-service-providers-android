package com.bidjobbers.bidjobberspro.features.dashbord.ui.share;

import com.bidjobbers.bidjobberspro.data.network.models.Allpages.AllPagesResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface TearmsandConditionFragmentMvpView extends BaseFragmentMvpView {

     void  allTasksName(AllPagesResponse response);
     void onDeactivate(String data);
}
