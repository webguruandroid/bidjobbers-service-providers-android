package com.bidjobbers.bidjobberspro.features.OnTimeScreen;

import com.bidjobbers.bidjobberspro.data.network.models.servicedetails.ServiceDetailsResponse;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface FirstTimeAccountSettingsMvpView extends MvpView {

    public void actionTesting();
    void actionGetPropertyData();

    void successfullyGetServiceDetails(ServiceDetailsResponse serviceDetailsResponse);


    void successfullySavaData();

    void successfullySaveKindOfPropertyData();

    void successfullySaveBusinessInfo();

    void successfullySaveBusinessLocation();

    void successfullySaveBusinessHour();

    void successfullySaveSocialLink();

    void successfullyySavePropertyType();

    void successfullySaveAdvanceBooking();

    void successfullySavePriceForLawn();
    void successfullySavePriceForTree();

    void successfullySaveImage();

}
