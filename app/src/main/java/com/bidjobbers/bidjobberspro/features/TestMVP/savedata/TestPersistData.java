package com.bidjobbers.bidjobberspro.features.TestMVP.savedata;

import java.util.ArrayList;

public class TestPersistData {

   ArrayList<DataBean> dataBeanArrayList;

    public TestPersistData(ArrayList<DataBean> dataBeanArrayList) {
        this.dataBeanArrayList = dataBeanArrayList;
    }

    public void setDataBeanArrayList(ArrayList<DataBean> dataBeanArrayList) {
        this.dataBeanArrayList = dataBeanArrayList;
    }

    public ArrayList<DataBean> getDataBeanArrayList() {
        return dataBeanArrayList;
    }
}
