package com.bidjobbers.bidjobberspro.features.OnTimeScreen.TypeOfPropertyWorkOn.TypeOfPropertyObject;

public class TypeOfPropertyObject {

    boolean isCommertial;
    boolean isResidential;
    boolean hasLicence;
    String licenceNo;
    String commertialId;
    String residentialId;

    public TypeOfPropertyObject(boolean isCommertial, boolean isResidential, boolean hasLicence, String licenceNo, String commertialId, String residentialId) {
        this.isCommertial = isCommertial;
        this.isResidential = isResidential;
        this.hasLicence = hasLicence;
        this.licenceNo = licenceNo;
        this.commertialId = commertialId;
        this.residentialId = residentialId;
    }

    @Override
    public String toString() {
        return "TypeOfPropertyObject{" +
                "isCommertial=" + isCommertial +
                ", isResidential=" + isResidential +
                ", hasLicence=" + hasLicence +
                ", licenceNo='" + licenceNo + '\'' +
                ", commertialId='" + commertialId + '\'' +
                ", residentialId='" + residentialId + '\'' +
                '}';
    }

    public boolean isCommertial() {
        return isCommertial;
    }

    public void setCommertial(boolean commertial) {
        isCommertial = commertial;
    }

    public boolean isResidential() {
        return isResidential;
    }

    public void setResidential(boolean residential) {
        isResidential = residential;
    }

    public boolean isHasLicence() {
        return hasLicence;
    }

    public void setHasLicence(boolean hasLicence) {
        this.hasLicence = hasLicence;
    }

    public String getLicenceNo() {
        return licenceNo;
    }

    public void setLicenceNo(String licenceNo) {
        this.licenceNo = licenceNo;
    }

    public String getCommertialId() {
        return commertialId;
    }

    public void setCommertialId(String commertialId) {
        this.commertialId = commertialId;
    }

    public String getResidentialId() {
        return residentialId;
    }

    public void setResidentialId(String residentialId) {
        this.residentialId = residentialId;
    }
}
