package com.bidjobbers.bidjobberspro.features.CustomerDetails;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

import okhttp3.MultipartBody;

public interface CustomerDetailsMvpPresenter <V extends CustomerDetailsMvpView> extends MvpPresenter<V> {
    void getAllTasks();
    void getScheduleTask();
    void getCompletedTask();
    void getCancelledTasks();
    void onPostBid(String taskId,String dayId,String slotId,String price,String date,String customer_id);
    void onScheduleTask(String bidId,String taskId,String dayId,String slotId,String date,String comment);
    void onStartTask(String customerId,String taskId,String bidId,String local);
    void onEndTask(String customerId,String taskId,String bidId,String local, MultipartBody.Part[] task_images,String note);
    void onBlockUser(String customerId);
    void onUnBlockUser(String customerId);
    void onGetCustomer(String taskid);

    void getAllTasksSchedule(String currentpage);
}
