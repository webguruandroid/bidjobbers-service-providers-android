package com.bidjobbers.bidjobberspro.features.OnTimeScreen.PricesFor;

import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface PricesForFragmentMvpView  extends BaseFragmentMvpView {
    void setDataFromApi(DataModelForFirstTimeQusAns.BusinessPriceLawnSerce businessPriceLawnSerce);
}
