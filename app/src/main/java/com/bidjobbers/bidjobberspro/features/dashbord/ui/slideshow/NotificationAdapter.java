package com.bidjobbers.bidjobberspro.features.dashbord.ui.slideshow;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.Notification.NotificationResponse;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    Context mContext;
    ArrayList<NotificationResponse.ResponseDataBean.NotificationListBean> mValues;
    int size;
    private NotificationAdapter.NotificationListner listListner;
    public NotificationAdapter(ArrayList<NotificationResponse.ResponseDataBean.NotificationListBean> item, int size) {

        this.mValues=item;
        this.size=size;
    }

    public void loadList(List<NotificationResponse.ResponseDataBean.NotificationListBean> items) {
        Log.e("Task",items.size()+"");
        mValues.clear();
        mValues.addAll(items);
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mContext = viewGroup.getContext();
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout_notification_item, viewGroup, false);
        return new ViewHolder(view);
    }

    public void setAdapterListner(NotificationAdapter.NotificationListner listner)
    {
        this.listListner=listner;
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final NotificationResponse.ResponseDataBean.NotificationListBean mDataBean = mValues.get(i);
        viewHolder.tv_rv_date.setText(mDataBean.getNotification_date());
        viewHolder.tv_rv_time.setText(mDataBean.getNotification_time());
        viewHolder.tv_rv_desc.setText(mDataBean.getNotification_message());
        viewHolder.tv_rv_name.setText(mDataBean.getCustomer_name());
        viewHolder.tv_service_name.setText(mDataBean.getService_type());
        viewHolder.tv_status.setText(mDataBean.getStatus());


        if(mDataBean.getStatus_id()==1)
        {
            viewHolder.tv_status.setTextColor(ContextCompat.getColor(mContext,R.color.orange));
        }
        else if(mDataBean.getStatus_id()==2)
        {
            viewHolder.tv_status.setTextColor(ContextCompat.getColor(mContext,R.color.green_above_avg));
        }

        else if(mDataBean.getStatus_id()==3)
        {
            viewHolder.tv_status.setTextColor(ContextCompat.getColor(mContext,R.color.pending));
        }
        else if(mDataBean.getStatus_id()==4)
        {
            viewHolder.tv_status.setTextColor(ContextCompat.getColor(mContext,R.color.green_above_avg));
        }
        else if(mDataBean.getStatus_id()==5)
        {
            viewHolder.tv_status.setTextColor(ContextCompat.getColor(mContext,R.color.red));
        }
        else if(mDataBean.getStatus_id()==6)
        {
            viewHolder.tv_status.setTextColor(ContextCompat.getColor(mContext,R.color.green_above_avg));
        }
        else if(mDataBean.getStatus_id()==7)
        {
            viewHolder.tv_status.setTextColor(ContextCompat.getColor(mContext,R.color.colorRescheduled));
        }
        else if(mDataBean.getStatus_id()==8)
        {
            viewHolder.tv_status.setTextColor(ContextCompat.getColor(mContext,R.color.green_above_avg));
        }
        else if(mDataBean.getStatus_id()==9)
        {
            viewHolder.tv_status.setTextColor(ContextCompat.getColor(mContext,R.color.orange));
        }
        else if(mDataBean.getStatus_id()==10)
        {
            viewHolder.tv_status.setTextColor(ContextCompat.getColor(mContext,R.color.red));
        }
        Glide.with(mContext).load(mDataBean.getService_image()).into(viewHolder.iv_prc);

       viewHolder.llContainer.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               listListner.onItemClick(mDataBean.getTask_id(),mDataBean.getBid_id(),mDataBean.getStatus_id()+"",mDataBean.getStatus());
           }
       });
    }

    @Override
    public int getItemCount() {
        return mValues.size();

    }

    public interface NotificationListner{
        void onItemClick(String taskId, String bidId, String statusId, String status);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.llContainer)
        LinearLayout llContainer;
        @BindView(R.id.tv_rv_date)
        TextView tv_rv_date;
        @BindView(R.id.tv_rv_time)
        TextView tv_rv_time;
        @BindView(R.id.tv_service_name)
        TextView tv_service_name;
        @BindView(R.id.tv_status)
        TextView tv_status;
        @BindView(R.id.tv_rv_name)
        TextView tv_rv_name;
        @BindView(R.id.tv_rv_desc)
        TextView tv_rv_desc;
        @BindView(R.id.iv_prc)
        ImageView iv_prc;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);


        }
    }


}
