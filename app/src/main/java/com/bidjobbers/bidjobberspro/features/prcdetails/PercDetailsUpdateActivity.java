package com.bidjobbers.bidjobberspro.features.prcdetails;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.UpdateProfile.UpdateProfileResponse;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class PercDetailsUpdateActivity extends BaseActivity implements PercDetailsUpdateMvpView,View.OnClickListener{

    ImageView iv_mnu;


    @BindView(R.id.ivProfile)
    ImageView ivProfile;
    @BindView(R.id.input_name)
    EditText input_name;
    @BindView(R.id.input_email)
    EditText input_email;
    @BindView(R.id.input_phone)
    EditText input_phone;
    @BindView(R.id.btn_update)
    Button btn_update;
    @BindView(R.id.ivTakePhoto)
    ImageView ivTakePhoto;

    Context mContext;

    ArrayList<Image> imagesArrayList1=new ArrayList<>();
    File upload_file_me;
    MultipartBody.Part img_body = null;

    @Inject
    PercDetailsUpdatePresenter<PercDetailsUpdateMvpView> percDetailsUpdatePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perc_details_update);


        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        mContext=this;

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure();
        }
        percDetailsUpdatePresenter.onAttach(this);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(PercDetailsUpdateActivity.this);
        percDetailsUpdatePresenter.onSuccessfullGetProfile(prefs.getString("LOCAL_KEY",""));

        iv_mnu = (ImageView) findViewById(R.id.iv_mnu);

        iv_mnu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();

       // SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(PercDetailsUpdateActivity.this);
      //  percDetailsUpdatePresenter.onSuccessfullGetProfile(prefs.getString("LOCAL_KEY",""));
    }

    @Override
    public void onDeactivate(String data) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(PercDetailsUpdateActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, 800);
    }

    @Override
    public void successfullyGetProfile(getProfileResponse getProfileResponse) {

        if(getProfileResponse.getResponseCode()==1)
        {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.ic_user);
            requestOptions.error(R.drawable.ic_user);
            Glide.with(mContext)
                    .load(getProfileResponse.getResponseData().getProfile_image())
                    .apply(requestOptions)
                    .into(ivProfile);

            input_email.setText(getProfileResponse.getResponseData().getEmail());
            input_name.setText(getProfileResponse.getResponseData().getName());
            input_phone.setText(getProfileResponse.getResponseData().getPhone());
        }
    }

    @Override
    public void successfullyUpdateProfile(UpdateProfileResponse updateProfileResponse) {
        Toast.makeText(getApplicationContext(),updateProfileResponse.getResponseText(),Toast.LENGTH_SHORT).show();
    }

    @OnClick({R.id.ivTakePhoto,R.id.btn_update})
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ivTakePhoto:
                ImagePicker.create(this)
                        .returnMode(ReturnMode.CAMERA_ONLY) // set whether pick and / or camera action should return immediate result or not.
                        .folderMode(true) // folder mode (false by default)
                        .toolbarFolderTitle("Folder") // folder selection title
                        .toolbarImageTitle("Tap to select") // image selection title
                        .toolbarArrowColor(Color.BLACK) // Toolbar 'up' arrow color
                        .includeVideo(false) // Show video on image picker
                        .multi() // multi mode (default mode)
                        .limit(1) // max images can be selected (99 by default)
                        .showCamera(true) // show camera or not (true by default)
                        .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                        .origin(imagesArrayList1) // original selected images, used in multi mode
                        .exclude(imagesArrayList1) // exclude anything that in image.getPath()
                        .theme(R.style.ImagePickerTheme) // must inherit ef_BaseTheme. please refer to sample
                        .enableLog(false) // disabling log
                        .start(); // start image

                break;

            case R.id.btn_update:

                if(input_name.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(mContext, getResources().getString(R.string.PleaseEnterName), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(input_phone.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(mContext, getResources().getString(R.string.PleaseEnterPhoneNumber), Toast.LENGTH_SHORT).show();
                    return;
                }
                else {
                    if (upload_file_me != null) {
                        RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), upload_file_me);
                        img_body = MultipartBody.Part.createFormData("profile_image", upload_file_me.getName(), reqFile);
                    }
                    percDetailsUpdatePresenter.onUpdateProfile(img_body, input_name.getText().toString().trim(), input_phone.getText().toString().trim(), "");
                }
                break;
        }
    }



    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            List<Image> images = ImagePicker.getImages(data);
            Log.e("File Size",ImagePicker.getImages(data).get(0).getPath());
            /*
               for (int i = 0; i < images.size(); i++) {
                files_send_server.add(images.get(i));
            }*/

            // or get a single image only

            Image image = ImagePicker.getFirstImageOrNull(data);

            // iv_prc.setI

            Glide.with(mContext)
                    .load(images.get(0).getPath())
                    //.placeholder(R.drawable.ic_launcher_background)
                    .into(ivProfile);

            Log.e("File Size",images.get(0).getPath());


            File file = new File(images.get(0).getPath());

            String dir = file.getParent();

            File dirAsFile = file.getParentFile();

            upload_file_me = new File(images.get(0).getPath());

            Log.e("dirAsFile",dir);

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
