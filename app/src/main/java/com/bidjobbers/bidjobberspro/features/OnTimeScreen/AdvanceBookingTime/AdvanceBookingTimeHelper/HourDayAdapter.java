package com.bidjobbers.bidjobberspro.features.OnTimeScreen.AdvanceBookingTime.AdvanceBookingTimeHelper;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bidjobbers.bidjobberspro.R;

import java.util.ArrayList;

public class HourDayAdapter extends BaseAdapter {


    Context mContext;
    ArrayList<String> mLsits;
    int pos;

    public HourDayAdapter(Context mContext, ArrayList<String> gList) {
        this.mContext = mContext;
        this.mLsits = gList;
    }

    @Override
    public int getCount() {
        return mLsits.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static final class ViewHolder {
        TextView tv_time_slot;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return initView(position, convertView);
    }

    private View initView(int position, View convertView) {
        if(convertView == null)
            convertView = View.inflate(mContext,
                    android.R.layout.simple_list_item_1,
                    null);
        TextView tvText1 = convertView.findViewById(android.R.id.text1);
        tvText1.setText(mLsits.get(position));
        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final DayWeekMonthAdapter.ViewHolder viewHolder;
        pos = position;

        if(convertView==null){
            convertView = View.inflate(mContext,
                    R.layout.spin_right_box_gray_layout,
                    null);

            viewHolder = new DayWeekMonthAdapter.ViewHolder();
            viewHolder.tv_time_slot = (TextView) convertView.findViewById(R.id.tv_time_slot);
            convertView.setTag(viewHolder);
        }else{
            // just re-use the viewHolder
            viewHolder = (DayWeekMonthAdapter.ViewHolder) convertView.getTag();
        }

        viewHolder.tv_time_slot.setText(mLsits.get(position));

        return convertView;
    }
}