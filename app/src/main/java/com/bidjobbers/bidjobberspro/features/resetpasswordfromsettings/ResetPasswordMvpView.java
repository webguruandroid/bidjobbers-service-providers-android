package com.bidjobbers.bidjobberspro.features.resetpasswordfromsettings;

import com.bidjobbers.bidjobberspro.data.network.models.ChangeaPassword.ChangePasswordResponse;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface ResetPasswordMvpView extends MvpView {




    void successfullChangePassword(ChangePasswordResponse changePasswordResponse);
}
