package com.bidjobbers.bidjobberspro.features.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.features.GetOtpAfterForgetPassword.GetOtpForForgetPasswordActivity;
import com.bidjobbers.bidjobberspro.features.LanguageChange.LanguageChangeActivity;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.FirstTimeAccountSettingsActivity;
import com.bidjobbers.bidjobberspro.features.VerifySignup.VerifySignupActivity;
import com.bidjobbers.bidjobberspro.features.dashbord.Main2Activity;
import com.bidjobbers.bidjobberspro.features.signup.SignUpActivity;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements LoginViewHelper,View.OnClickListener {

    @BindView(R.id.input_houseAddress)
    EditText edtUserName;
    @BindView(R.id.edt_pass)
    EditText edtPass;
    @BindView(R.id.ivChangeLanguage)
    ImageView ivChangeLanguage;
    @BindView(R.id.ivBack)
    ImageView ivBack;

    @Inject
    LoginPresenter<LoginViewHelper> loginPresenter;

    String tag,lang;

     Context mContext;

    String currentLanguage = "";
    Locale myLocale;
    String device_token;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
        lang=prefs.getString("LOCAL_KEY","");
        setLocale(lang);
        setContentView(R.layout.activity_main);


       // currentLanguage = getIntent().getStringExtra(currentLang);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        loginPresenter.onAttach(this);


        Log.e("Language","OnCreate"+lang);
        dataManager.setLocalValue(lang);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( LoginActivity .this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                device_token = instanceIdResult.getToken();
                Log.e("Token",device_token);
                // Toast.makeText(getApplicationContext(),"Token"+device_token,Toast.LENGTH_SHORT).show();

            }
        });
    }

    //This is for testing you may omit this

    public void setLocale(String localeName) {

        if (!localeName.equals(currentLanguage)) {


           // LocaleHelper.setLocale(LoginActivity.this, localeName);
           // recreate();

            Log.e("Language--under",localeName);
//
//            myLocale = new Locale(localeName);
//            Resources res = getResources();
//            DisplayMetrics dm = res.getDisplayMetrics();
//            Configuration conf = res.getConfiguration();
//
//            conf.locale = myLocale;
//            res.updateConfiguration(conf, dm);

            Resources res = mContext.getResources();
// Change locale settings in the app.
            DisplayMetrics dm = res.getDisplayMetrics();
            android.content.res.Configuration conf = res.getConfiguration();
            conf.setLocale(new Locale(localeName.toLowerCase())); // API 17+ only.
            res.updateConfiguration(conf, dm);



        } else {
           // Toast.makeText(this, "Language already selected!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        loginPresenter.onDetach();
    }

    public void login(View view)
   {
       loginPresenter.onClickLogin(edtUserName.getText().toString().trim(),
       edtPass.getText().toString().trim(),"A",device_token);

   }

    public void forgotPass(View view)
    {
       loginPresenter.onForgotPass();
       //Intent animActivity = new Intent(this, SignUpActivity.class);
       //startActivity(animActivity);
       //overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public void signUp(View view)
    {
        startActivity(new Intent(this, SignUpActivity.class));
        overridePendingTransition(R.anim.enter, R.anim.exit);
        //Intent animActivity = new Intent(this, SignUpActivity.class);
        //startActivity(animActivity);
        //overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    @Override
    public void loginAction(String Data)
    {

        startActivity(new Intent(this, FirstTimeAccountSettingsActivity.class));
        //startActivity(new Intent(this, DashbordActivity.class));
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void forgotPass() {
        startActivity(new Intent(this, GetOtpForForgetPasswordActivity.class));
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @Override
    public void displayAuth(String auth) {

      //  Toast.makeText(this, auth, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void goVerificatioPage(String emailId) {
        Intent intentHome = new Intent(mContext, VerifySignupActivity.class);
        intentHome.putExtra("emailAddress",emailId.trim());
        intentHome.putExtra("otp_for","EVAR");
        startActivity(intentHome);
        finish();
    }

    @Override
    public void goTaskScreen()
    {
        Intent intentHome = new Intent(mContext, Main2Activity.class);
        startActivity(intentHome);
        finish();
    }

    @Override
    public void goFirstTimeLoginScreen() {
        startActivity(new Intent(this, FirstTimeAccountSettingsActivity.class));
    }

    @Override
    public void onChkValue(String s) {
     //   Toast.makeText(mContext, ""+s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }


    @OnClick({R.id.ivChangeLanguage,R.id.ivBack })
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.ivChangeLanguage:

                Intent i=new Intent(LoginActivity.this, LanguageChangeActivity.class);
                i.putExtra("tag","Update");
                startActivity(i);
               // finish();
                break;
            case R.id.ivBack:
                onBackPressed();
                break;
        }
    }
}
