package com.bidjobbers.bidjobberspro.features.VerifySignup;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface VerifySignupMvpPresenter <V extends VerifySignupMvpView> extends MvpPresenter<V> {


    public void actionVerifyOtp(String otp, String emailId ,String otp_for);

    public void actionResentOtp(String email, String otp_for, String locale);



}
