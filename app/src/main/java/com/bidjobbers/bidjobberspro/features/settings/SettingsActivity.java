package com.bidjobbers.bidjobberspro.features.settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.drawerlayout.widget.DrawerLayout;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.features.dashbord.OnlyActivty.DashbordActivity;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.features.prcdetails.PercDetailsUpdateActivity;
import com.bidjobbers.bidjobberspro.features.profiledetails.ProfileActivity;
import com.bidjobbers.bidjobberspro.features.resetpasswordfromsettings.ResetPasswordActivity;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;
import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsActivity extends BaseActivity implements SettingsMvpView {


    LinearLayout ll_prs_dtls,ll_bd_dtls,ll_cng_pass, ll_log_out;
    ImageView iv_mnu;
    DrawerLayout drawer_layout;
    NavigationView navigationView;
    Context mContext;
    TextView input_houseAddress, tv_bd, tv_cp, tv_lo;

    @BindView(R.id.ivProfileImage)
    ImageView ivProfileImage;




    ImageView ivProfile;

    TextView tvName;

    TextView ivEmail;
    View headerView;


    @Inject
    SettingsPresenter<SettingsMvpView> settingsPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        settingsPresenter.onAttach(this);
        input_houseAddress=(TextView) findViewById(R.id.input_houseAddress);

        input_houseAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_prs_dtls.performClick();
            }
        });

        tv_bd=(TextView) findViewById(R.id.tv_bd);
        tv_bd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_bd_dtls.performClick();
            }
        });

         tv_cp=(TextView) findViewById(R.id.tv_cp);
         tv_cp.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 ll_cng_pass.performClick();
             }
         });

                tv_lo=(TextView) findViewById(R.id.tv_lo);
         tv_lo.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 ll_log_out.performClick();
             }
         });



        ll_prs_dtls = (LinearLayout) findViewById(R.id.ll_prs_dtls);
        ll_bd_dtls = (LinearLayout) findViewById(R.id.ll_bd_dtls);
        ll_cng_pass = (LinearLayout) findViewById(R.id.ll_cng_pass);
        ll_log_out = (LinearLayout) findViewById(R.id.ll_log_out);
        iv_mnu = (ImageView) findViewById(R.id.iv_mnu);
        drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        mContext = this;

        ll_prs_dtls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(SettingsActivity.this, PercDetailsUpdateActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });
        ll_bd_dtls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingsActivity.this, ProfileActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        ll_cng_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingsActivity.this, ResetPasswordActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        ll_log_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             /*   startActivity(new Intent(SettingsActivity.this, SignUpActivity.class));
                overridePendingTransition(R.anim.enter, R.anim.exit);*/
            }
        });

        iv_mnu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.openDrawer(Gravity.LEFT);
            }
        });




        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this);
        settingsPresenter.onSuccessfullGetProfile(prefs.getString("LOCAL_KEY",""));
        setUpNavigationView();
    }

    private void setUpNavigationView() {

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {


            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.action_logout:

                        settingsPresenter.onLogoutClick();
                        break;
                    case R.id.action_settings:
                        startActivity(new Intent(mContext, DashbordActivity.class));
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        drawer_layout.closeDrawer(Gravity.LEFT);
                        return true;
                    default:
                        showMessage("Under development");
                }

                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                return true;
            }
        });

        View headerView = navigationView.getHeaderView(0);
        tvName = (TextView) headerView.findViewById(R.id.tvName);
        ivEmail=(TextView)headerView.findViewById(R.id.ivEmail);
        ivProfile=(ImageView)headerView.findViewById(R.id.ivProfile);
    }


    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this);
        settingsPresenter.onSuccessfullGetProfile(prefs.getString("LOCAL_KEY",""));
    }

    @Override
    public void successfullyGetProfile(getProfileResponse getProfileResponse) {


        Glide.with(mContext)
                .load(getProfileResponse.getResponseData().getProfile_image())
                // .placeholder(R.drawable.ic_launcher_background)
                .into(ivProfileImage);
        Glide.with(mContext)
                .load(getProfileResponse.getResponseData().getProfile_image())
                // .placeholder(R.drawable.ic_launcher_background)
                .into(ivProfile);

        ivEmail.setText(getProfileResponse.getResponseData().getEmail());
        tvName.setText(getProfileResponse.getResponseData().getName());

    }

    @Override
    public void successfullyLogout() {
        Intent i=new Intent(SettingsActivity.this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onDeactivate(String data) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(SettingsActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, 800);
    }
}
