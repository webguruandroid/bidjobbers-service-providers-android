package com.bidjobbers.bidjobberspro.features.OnTimeScreen.TreeStrubWorkFragment;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface TreeStrubWorkFragmentMvpPresenter <V extends TreeStrubWorkFragmentMvpView> extends BaseFragmentMvpPresenter<V> {

    void onGetDataFromApi();
}
