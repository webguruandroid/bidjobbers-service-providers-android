package com.bidjobbers.bidjobberspro.features.dashbord.ui.share;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.Allpages.AllPagesRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Allpages.AllPagesResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class TearmsandConditionFragmentPresenter<V extends TearmsandConditionFragmentMvpView>
        extends BaseFragmentPresenter<V>
        implements TearmsandConditionFragmentMvpPresenter<V> {

    @Inject
    public TearmsandConditionFragmentPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }


    @Override
    public void getAllPages(String sug) {
        if(getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();

            AllPagesRequest allPagesRequest = new AllPagesRequest(  getDataManager().getLocalValue(),sug);

            getCompositeDisposable().add(
                    getDataManager().getAllPages("application/json",allPagesRequest)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeWith(new DisposableSingleObserver<AllPagesResponse>() {
                                @Override
                                public void onSuccess(AllPagesResponse testResponse) {
                                    Log.v("onSuccess", testResponse.getResponseCode() + "");
                                        getMvpView().hideLoading();
                                    if(testResponse.getResponseCode()==1) {

                                        getMvpView().allTasksName(testResponse);
                                    }
                                    else if(testResponse.getResponseCode()==401)
                                    {

                                        getMvpView().onError(testResponse.getResponseText());
                                        getMvpView().onDeactivate(testResponse.getResponseText());

                                    }
                                    else
                                    {
                                        getMvpView().onError(testResponse.getResponseText());
                                    }

                                }

                                @Override
                                public void onError(Throwable e) {

                                    getMvpView().hideLoading();
                                    Log.e("ERROR", "onError: " + e.getMessage());

                                }

                            }));
        }
        else
        {
            getMvpView().showAlert("No internet Connection");
        }

    }
}
