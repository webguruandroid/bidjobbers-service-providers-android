package com.bidjobbers.bidjobberspro.features.OnTimeScreen.TreeStrubWorkFragment;

import java.util.ArrayList;

public class TreeStrubWorkObject {


   public ArrayList<TreeClass> list=new ArrayList<>();


    public TreeStrubWorkObject(ArrayList<TreeClass> list) {
        this.list = list;
    }

    public ArrayList<TreeClass> getList() {
        return list;
    }

    public void setList(ArrayList<TreeClass> list) {
        this.list = list;
    }

    public static class TreeClass
    {
        String id;
        String price;

        public TreeClass(String id, String price) {
            this.id = id;
            this.price = price;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        @Override
        public String toString() {
            return "TreeClass{" +
                    "id='" + id + '\'' +
                    ", price='" + price + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "TreeStrubWorkObject{" +
                "list=" + list +
                '}';
    }
}
