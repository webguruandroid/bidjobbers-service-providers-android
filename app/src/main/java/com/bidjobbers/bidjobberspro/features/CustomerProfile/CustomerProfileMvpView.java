package com.bidjobbers.bidjobberspro.features.CustomerProfile;

import com.bidjobbers.bidjobberspro.data.network.models.Archived.ArchiveDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Archived.ArchiveListResponse;
import com.bidjobbers.bidjobberspro.data.network.models.CacelledTask.CancelledTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.CompletedTask.CompletedTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Customer.CustomerDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Recivetask.ReciveTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ScheduleTask.ScheduletaskResponse;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

import java.util.List;

public interface CustomerProfileMvpView extends MvpView {

    void onSuccessfullyGetCustomer(ArchiveDetailsResponse customerDetailsResponse);

}
