package com.bidjobbers.bidjobberspro.features.testfragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class LawnAdapter extends RecyclerView.Adapter<LawnAdapter.ViewHolder> {

    private ArrayList<LawnBean> listdata;
    public  LawnListener lawnListener;
    public Context mContext;

    public void setLawnListener( LawnListener lawnListener)
    {
        this.lawnListener = lawnListener;
    }

    public LawnAdapter(ArrayList<LawnBean> listdata) {
        this.listdata = listdata;
    }

    public interface LawnListener{
        public void chengeCheck(int pos,boolean chk);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mContext = viewGroup.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View listItem= layoutInflater.inflate(R.layout.layout_item_subjobs, viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(listItem);

        return viewHolder;
    }





    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        if(!listdata.get(i).getCheck())
        {
            viewHolder.ll_tick.setVisibility(View.GONE);
        }
        else
        {
            viewHolder.ll_tick.setVisibility(View.VISIBLE);
        }
//        if(i==0) {
//            viewHolder.imageView.setBackgroundResource(R.drawable.lawn_aerate_a_lawn);
//        }
//        if(i==1) {
//            viewHolder.imageView.setBackgroundResource(R.drawable.lawn_mow_a_lawn);
//        }
//        if(i==2) {
//            viewHolder.imageView.setBackgroundResource(R.drawable.lawn_yard_waste_clippings_and_removal);
//        }
//        if(i==3) {
//            viewHolder.imageView.setBackgroundResource(R.drawable.tree_stump_remove);
//        }
//        if(i==4){
//            viewHolder.imageView.setBackgroundResource(R.drawable.tree_yard_waste_clippings_and_removal);
//        }
//
//        if(i==5) {
//            viewHolder.imageView.setBackgroundResource(R.drawable.trees_and_shrub_trim_and_remove);
//        }

        Glide.with(mContext)
                .load(listdata.get(i).getImage())
                // .placeholder(R.drawable.ic_launcher_background)
                .into(viewHolder.imageView);



        viewHolder.tv_desc.setText(listdata.get(i).getDesc());

        viewHolder.tv_titel.setText(listdata.get(i).getTitel());


    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder  {
        public ImageView imageView;
        public LinearLayout ll_tick;
        public TextView tv_desc; public TextView tv_titel;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.img_item);
            this.ll_tick = (LinearLayout) itemView.findViewById(R.id.ll_tick);
            this.tv_desc = (TextView) itemView.findViewById(R.id.tv_desc);
            this.tv_titel = (TextView) itemView.findViewById(R.id.tv_titel);

            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    lawnListener.chengeCheck(getAdapterPosition(),listdata.get(getAdapterPosition()).getCheck());

                }
            });

        }
    }
}
