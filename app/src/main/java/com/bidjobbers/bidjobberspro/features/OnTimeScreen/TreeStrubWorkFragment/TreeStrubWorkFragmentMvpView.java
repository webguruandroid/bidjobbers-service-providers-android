package com.bidjobbers.bidjobberspro.features.OnTimeScreen.TreeStrubWorkFragment;

import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface TreeStrubWorkFragmentMvpView extends BaseFragmentMvpView {

    void setDataFromApi(DataModelForFirstTimeQusAns.BusinessPriceLawnSerce businessPriceLawnSerce);
}
