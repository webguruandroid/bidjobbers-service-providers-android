package com.bidjobbers.bidjobberspro.features.GetOtpAfterForgetPassword;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.resentotp.ResendOtpRequest;
import com.bidjobbers.bidjobberspro.data.network.models.resentotp.ResendOtpResponse;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;
import com.bidjobbers.bidjobberspro.utils.CommonUtils;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class GetOptForgotPassPresenter  <V extends GetOtpForgotPassMvpView> extends BasePresenter<V> implements GetOtpForgotPassMvpPresenter<V>   {

    @Inject
    public GetOptForgotPassPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void onGetOtp(String email) {

        if(!CommonUtils.isEmailValid(email.toString()))
        {
            if(getDataManager().getLocalValue().trim().equals("fr")) {
                getMvpView().showMessage("Email id n'est pas valide");
            }
            else {
                getMvpView().showMessage("Email Id is not valid");
            }
            return;
        }

        String otp_for = "RP";

        ResendOtpRequest resendOtpRequest = new ResendOtpRequest(email.trim(),getDataManager().getLocalValue(),otp_for);
        getCompositeDisposable().add(
                getDataManager().getSentOtp(resendOtpRequest)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<ResendOtpResponse>()
                        {
                            @Override
                            public void onSuccess(ResendOtpResponse testResponse)
                            {
                                if(testResponse.getResponseCode()==1)
                                {
                                    //getMvpView().onOtpValid(false, testResponse.getResponseText(),getDataManager().getAccess_token());
                                    getMvpView().showMessage(testResponse.getResponseText());
                                    getMvpView().actionGetOtp();
                                }
                                else
                                {
                                    //getMvpView().onOtpValid(true, testResponse.getResponseText(),getDataManager().getAccess_token());
                                    getMvpView().showMessage(testResponse.getResponseText());
                                }
                            }
                            @Override
                            public void onError(Throwable e)
                            {
                                Log.e("ERROR", "onError: "+e.getMessage());
                            }

                        }));





    }
}
