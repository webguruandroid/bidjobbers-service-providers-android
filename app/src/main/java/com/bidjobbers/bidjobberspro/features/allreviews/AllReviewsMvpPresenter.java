package com.bidjobbers.bidjobberspro.features.allreviews;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface AllReviewsMvpPresenter <V extends AllReviewsMvpView> extends MvpPresenter<V> {

    void getReview();
}
