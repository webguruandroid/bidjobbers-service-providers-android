package com.bidjobbers.bidjobberspro.features.dashbord.ui.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.NotificationCount.NotificationCountResponse;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.features.BankDetails.BankDetailsActivity;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPager.ViewPagerTaskAdapter;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.TabBean;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

import javax.inject.Inject;


public class HomeFragment extends BaseFragment implements HomeFragmentMvpView {

    ArrayList<TabBean> tabBeans= new ArrayList<>();
    TabLayout sliding_tabs;
    ViewPager viewpager;
    ViewPagerTaskAdapter viewPagerTaskyAdapter;
  //  @BindView(R.id.rl_bank_info)
    RelativeLayout rl_bank_info;
    SharedPreferences mBank;
    private  String isBankinfo="";

    @Inject
    HomeFragmentPresenter<HomeFragmentMvpView> homeFragmentPresenter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        mBank=getActivity().getSharedPreferences("Bidjobbers_Bank", Context.MODE_PRIVATE);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }
        homeFragmentPresenter.onAttach(this);

        sliding_tabs = (TabLayout)root. findViewById(R.id.sliding_tabs);
        viewpager = (ViewPager)root. findViewById(R.id.viewpager);
        rl_bank_info=root.findViewById(R.id.rl_bank_info);
        sliding_tabs.setupWithViewPager(viewpager);
        viewpager.setOffscreenPageLimit(1);

        rl_bank_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(getActivity(), BankDetailsActivity.class);
                i.putExtra("Tag","Home");
                startActivity(i);
              //  getActivity().finish();
            }
        });


        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        homeFragmentPresenter.getAllTasks();
        homeFragmentPresenter.onGetNotificationCount();
        Log.e("DATA", mBank.getString("BankInfo",""));
        isBankinfo=mBank.getString("BankInfo","");
//        if(isBankinfo.equals("yes"))
////        {
////            rl_bank_info.setVisibility(View.GONE);
////        }
////        else
////        {
////            rl_bank_info.setVisibility(View.VISIBLE);
////        }
    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void allTasksName(ArrayList<TabBean> arrayList) {

        tabBeans.add(new TabBean("1",getResources().getString(R.string.tasks_tab_heading_received),"","",""));
        tabBeans.add(new TabBean("2",getResources().getString(R.string.tasks_tab_heading_scheduled),"","",""));
        tabBeans.add(new TabBean("3",getResources().getString(R.string.tasks_tab_heading_completed),"","",""));
        tabBeans.add(new TabBean("4",getResources().getString(R.string.tasks_tab_heading_cancelled),"","",""));
       // tabBeans = arrayList;
        viewPagerTaskyAdapter = new ViewPagerTaskAdapter(getChildFragmentManager(),tabBeans);
        viewpager.setAdapter(viewPagerTaskyAdapter);
        viewPagerTaskyAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSuccessfullyGetNotificationCount(NotificationCountResponse notificationCountResponse) {
        if(notificationCountResponse.getResponseCode()==1)
        {


            if(notificationCountResponse.getResponseData().getHasTransAccDetails().equals("1")) {

                rl_bank_info.setVisibility(View.GONE);
                SharedPreferences.Editor myEdit
                        = mBank.edit();

                myEdit.putString("BankInfo", "yes");
                myEdit.apply();
                myEdit.commit();
            }
            else
            {

                rl_bank_info.setVisibility(View.VISIBLE);
                SharedPreferences.Editor myEdit
                        = mBank.edit();

                myEdit.putString("BankInfo", "no");
                myEdit.apply();
                myEdit.commit();
            }

        }
        else
        {
            rl_bank_info.setVisibility(View.VISIBLE);
            SharedPreferences.Editor myEdit
                    = mBank.edit();

            myEdit.putString("BankInfo", "no");
            myEdit.apply();
            myEdit.commit();

        }
    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }
}