package com.bidjobbers.bidjobberspro.features.signup;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.register.RegisterResponse;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;


public class SignUpPresenter <V extends SignupMvpView> extends BasePresenter<V> implements SignUpMvpPresenter<V> {

    @Inject
    public SignUpPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onSignUpButtonWithOutImageClick(String companyName, String firstAndLastName, String emailAddress, String phoneNumber, String password) {


        RequestBody companyNameData = RequestBody.create(MediaType.parse("multipart/form-data"), companyName);
        RequestBody firstAndLastNameData = RequestBody.create(MediaType.parse("multipart/form-data"), firstAndLastName);
        RequestBody emailAddressData = RequestBody.create(MediaType.parse("multipart/form-data"), emailAddress);
        RequestBody phoneNumberData = RequestBody.create(MediaType.parse("multipart/form-data"), phoneNumber);
        RequestBody passwordData = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(password));
        RequestBody localData = RequestBody.create(MediaType.parse("multipart/form-data"), getDataManager().getLocalValue());
        RequestBody accept_terms = RequestBody.create(MediaType.parse("multipart/form-data"), "1");


        if(getMvpView().isNetworkConnected()) {

            getMvpView().showLoading();
            getCompositeDisposable().add(getDataManager().postPersonalUpdate(
                    companyNameData, firstAndLastNameData,
                    emailAddressData, phoneNumberData, passwordData, localData, accept_terms)
                    .subscribeOn(getSchedulerProvider().io())
                    .observeOn(getSchedulerProvider().ui())
                    .subscribeWith(new DisposableSingleObserver<RegisterResponse>() {

                        @Override
                        public void onSuccess(RegisterResponse testResponse) {

                            getMvpView().hideLoading();
                            if (testResponse.getResponseCode() == 0) {
                                getMvpView().actionSignUpButtonClick(testResponse.getResponseText(), true);
                            }else if(testResponse.getResponseCode()==401)
                            {
                                getMvpView().actionDeactivateAccount(testResponse);
                            }
                            else {
                                if (testResponse.getResponseCode() == -1) {
                                    getMvpView().actionSignUpButtonClick(testResponse.getResponseText(), true);
                                } else {
                                    if ((testResponse.getResponseData().getAccess_token() != null) || (!testResponse.getResponseData().getAccess_token().trim().isEmpty())) {
                                        getDataManager().setAccess_token(testResponse.getResponseData().getAccess_token());
                                        getDataManager().setIsQuestionFillup(testResponse.getResponseData().getHasBusinessDetails());
                                        getDataManager().setIsEmailVarified(testResponse.getResponseData().getIsEmailVerified());
                                        getDataManager().setCurrentUserEmail(emailAddress);
                                        Log.v("SUBMIT", "Access Token save Successfully");
                                        Log.v("SUBMIT", getDataManager().getAccess_token());
                                        getMvpView().actionSignUpButtonClick(testResponse.getResponseText(), false);

                                    }
                                    getMvpView().actionSignUpButtonClick(testResponse.getResponseText(), false);

                                }

                                //Shared prefrence value save access_token.
                            }

                        }

                        @Override
                        public void onError(Throwable e) {

                            getMvpView().hideLoading();
                            Log.e("ERROR", "onError: " + e.getMessage());
                        }

                    }));

        }
        else
        {
            getMvpView().showAlert("No internet Connection");
        }


    }

    @Override
    public void onSignUpButtonClick(String companyName, String firstAndLastName,
                                    String emailAddress, String phoneNumber, String password,
                                    MultipartBody.Part img_body) {



        RequestBody companyNameData = RequestBody.create(MediaType.parse("multipart/form-data"), companyName);
        RequestBody firstAndLastNameData = RequestBody.create(MediaType.parse("multipart/form-data"), firstAndLastName);
        RequestBody emailAddressData = RequestBody.create(MediaType.parse("multipart/form-data"), emailAddress);
        RequestBody phoneNumberData = RequestBody.create(MediaType.parse("multipart/form-data"), phoneNumber);
        RequestBody passwordData = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(password));
        RequestBody localData = RequestBody.create(MediaType.parse("multipart/form-data"),  getDataManager().getLocalValue());
        RequestBody accept_terms = RequestBody.create(MediaType.parse("multipart/form-data"), "1");

        if(getMvpView().isNetworkConnected()) {

        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().postPersonalUpdate( img_body,
                companyNameData,firstAndLastNameData,
                emailAddressData,phoneNumberData,passwordData,localData,accept_terms)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<RegisterResponse>()
                {

                    @Override
                    public void onSuccess(RegisterResponse testResponse)
                    {
                        getMvpView().hideLoading();

                       if(testResponse.getResponseCode()==0 )
                       {
                           getMvpView().actionSignUpButtonClick(testResponse.getResponseText(),true);
                       }
                       else if(testResponse.getResponseCode()==401)
                       {
                            getMvpView().onError(testResponse.getResponseText());
                            getMvpView().actionDeactivateAccount(testResponse);
                       }
                       else
                       {
                           if(testResponse.getResponseCode()==-1)
                           {
                               getMvpView().actionSignUpButtonClick(testResponse.getResponseText(),true);
                           }
                           else
                           {
                               if((testResponse.getResponseData().getAccess_token()!= null) ||(!testResponse.getResponseData().getAccess_token().trim().isEmpty())  ) {
                                   getDataManager().setAccess_token(testResponse.getResponseData().getAccess_token());
                                   getDataManager().setIsQuestionFillup(testResponse.getResponseData().getHasBusinessDetails());
                                   getDataManager().setIsEmailVarified(testResponse.getResponseData().getIsEmailVerified());
                                   getDataManager().setCurrentUserEmail(emailAddress);
                                   Log.v("SUBMIT","Access Token save Successfully");
                                   Log.v("SUBMIT", getDataManager().getAccess_token());


                                   getMvpView().actionSignUpButtonClick(testResponse.getResponseText(), false);

                               }
                                   getMvpView().actionSignUpButtonClick(testResponse.getResponseText(), false);

                           }

                           //Shared prefrence value save access_token.
                       }


                    }
                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));

        }
        else
        {
            getMvpView().showAlert("No internet Connection");
        }


    }


}
