package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.Cancelled;

import com.bidjobbers.bidjobberspro.data.network.models.CacelledTask.CancelledTaskResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface CancelledBodyMvpView extends BaseFragmentMvpView {
     void  allTasksName(CancelledTaskResponse arrayList);
     void onDeactivate(String data);
}
