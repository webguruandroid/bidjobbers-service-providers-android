package com.bidjobbers.bidjobberspro.features.GetOtpAfterForgetPassword;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.features.ForgotOtpVeryfy.VerifyOtpActivity;
import com.bidjobbers.bidjobberspro.features.LanguageChange.LanguageChangeActivity;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GetOtpForForgetPasswordActivity extends BaseActivity implements GetOtpForgotPassMvpView {


    @Inject
    GetOtpForgotPassMvpPresenter<GetOtpForgotPassMvpView> getOtpForgotPassMvpViewGetOtpForgotPassMvpPresenter;
    @BindView(R.id.ed_email_id)
    EditText ed_email_id;
    @BindView(R.id.ivChangeLanguage)
    ImageView ivChangeLanguage;
    @BindView(R.id.ivBack)
    ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_otp_for_forget_password);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);

        getOtpForgotPassMvpViewGetOtpForgotPassMvpPresenter.onAttach(this);

    }

    public void statted(View view)
    {

        getOtpForgotPassMvpViewGetOtpForgotPassMvpPresenter.onGetOtp(ed_email_id.getText().toString().trim());
    }

    @Override
    public void actionGetOtp() {

        Intent intent = new Intent(this, VerifyOtpActivity.class);
        intent.putExtra("emailId",ed_email_id.getText().toString().trim());

        Log.e("GetOtpActivity",ed_email_id.getText().toString().trim());

       // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    @OnClick({R.id.ivChangeLanguage,R.id.ivBack })
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.ivChangeLanguage:

                Intent i=new Intent(GetOtpForForgetPasswordActivity.this, LanguageChangeActivity.class);
                i.putExtra("tag","Update");
                startActivity(i);
                break;
            case R.id.ivBack:
                onBackPressed();
                break;
        }
    }



}
