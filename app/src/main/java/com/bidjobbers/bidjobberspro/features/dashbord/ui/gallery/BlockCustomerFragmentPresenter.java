package com.bidjobbers.bidjobberspro.features.dashbord.ui.gallery;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList.BlockCustomerListRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList.BlockCustomerListResponse;
import com.bidjobbers.bidjobberspro.data.network.models.UnBlockUser.UnBlockUserRequest;
import com.bidjobbers.bidjobberspro.data.network.models.UnBlockUser.UnBlockUserResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class BlockCustomerFragmentPresenter <V extends BlockCustomerFragmentMvpView>
        extends BaseFragmentPresenter<V>
        implements BlockCustomerFragmentMvpPresenter<V> {

    @Inject
    public BlockCustomerFragmentPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }




    @Override
    public void getBlockCustomer() {
        BlockCustomerListRequest blockCustomerListRequest=new BlockCustomerListRequest( getDataManager().getLocalValue(),"","");


        getCompositeDisposable().add(getDataManager().postBlockUserList("Bearer "+getDataManager().getAccess_token(),"application/json",blockCustomerListRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<BlockCustomerListResponse>() {
                    @Override
                    public void onSuccess(BlockCustomerListResponse reciveTaskResponse) {

                        getMvpView().hideLoading();
                        if(reciveTaskResponse.getResponseCode()==1) {
                            getMvpView().successfullyGetBlockCustomer(reciveTaskResponse);
                        }
                        else
                        {
                            getMvpView().successfullyGetBlockCustomer(reciveTaskResponse);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));
    }

    @Override
    public void onUnBlockUser(String customerId) {
        UnBlockUserRequest blockCustomerRequest=new UnBlockUserRequest( getDataManager().getLocalValue(),customerId);

        getCompositeDisposable().add(getDataManager().postUnBlockUser("Bearer "+getDataManager().getAccess_token(),"application/json",blockCustomerRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<UnBlockUserResponse>() {
                    @Override
                    public void onSuccess(UnBlockUserResponse unBlockUserResponse) {


                        getMvpView().hideLoading();
                        getMvpView().onError(unBlockUserResponse.getResponseText());
                        Log.e("Task",unBlockUserResponse.getResponseText());
                        getMvpView().onSuccessfullyUnBlockUser(unBlockUserResponse);

                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));
    }
}
