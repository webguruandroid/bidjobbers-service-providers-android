package com.bidjobbers.bidjobberspro.features.ScheduleYourJob.AvalableDateViewPagerHelper;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;

import java.util.ArrayList;

public class AvalableTimeListAdapter extends RecyclerView.Adapter<AvalableTimeListAdapter.ViewHolder> {


    ArrayList<AvalableDateBean> mValues;
    ArrayList<String> selectItem;
    Context mContext;
    public SelectItemListener mListener;


    public AvalableTimeListAdapter(Context context, ArrayList<AvalableDateBean> items,ArrayList<String> itemsTwo)
    {
        mValues = items;
        mContext = context;
        selectItem=itemsTwo;
    }


    public void setmListener(SelectItemListener mListener)
    {
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_avl_time_item_task, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int i) {



        holder.tv_time_slot.setText(mValues.get(i).getDate());
        if(mValues.get(i).isChaked().equals("1"))
        {

            holder.ll_previous.setVisibility(View.VISIBLE);
            holder.ll_select.setVisibility(View.GONE);
        }
        else if(mValues.get(i).isChaked().equals("2"))
        {
            holder.ll_select.setVisibility(View.VISIBLE);
            holder.ll_previous.setVisibility(View.GONE);
        }
        else
        {
            holder.ll_select.setVisibility(View.GONE);
            holder.ll_previous.setVisibility(View.GONE);
            holder.rl_body.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClick(i);
                }
            });
        }



    }

    public void checkPosition(int position)
    {
        for(int i=0;i<mValues.size();i++)
        {

            if(position==i){
                mValues.get(i).setChaked("2");
            }
            else
            {
                if(selectItem.get(i).equals("1"))
                {
                    mValues.get(i).setChaked("1");
                }
                else {
                    mValues.get(i).setChaked("0");
                }
            }

        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
         return mValues.size();
    }


    public interface SelectItemListener
    {
        public void onItemClick(int pos);
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_time_slot;
        RelativeLayout rl_body;
        LinearLayout ll_select,ll_previous;


        public ViewHolder(View view) {
            super(view);

            tv_time_slot=(TextView)view.findViewById(R.id.tv_time_slot);
            ll_select=(LinearLayout) view.findViewById(R.id.ll_select);
            rl_body=(RelativeLayout) view.findViewById(R.id.rl_body);
            ll_previous=(LinearLayout)view.findViewById(R.id.ll_previous);

        }


    }
}