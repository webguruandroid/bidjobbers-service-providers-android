package com.bidjobbers.bidjobberspro.features.OnTimeScreen;

import com.bidjobbers.bidjobberspro.features.OnTimeScreen.PricesFor.PriceForObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TreeStrubWorkFragment.TreeStrubWorkObject;
import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;

public interface FirstTimeAccountSettingsMvpPresenter <V extends FirstTimeAccountSettingsMvpView>   extends MvpPresenter<V>
{
   public void init();
   void savePropertyData(boolean isResidential,boolean isCommertoal,boolean hasLicence,String licenceNo);
   void doGetServiceDetails(String language);


   void submitData(String desc);
   void submitKindOfPropertyData(List<Integer> service_type);
   void submitBusinessInfo(String companyName,String yearofFound,String noofEmpId,String comapnyRegNo);
   void submitBusinessLocation(String businessaddress,String apartment,String city,String state,String country,String zipcode, String lat, String lng);

   void submitBusinessHours(ArrayList<String> dayId,
   ArrayList<String>startTimeId,
   ArrayList<String>startTime,
   ArrayList<String>endTimeId,
   ArrayList<String>endTime);


   void submitSocialLink(List<String> socialLinks,String webUrl);

   void submitPropertyTypeData(ArrayList<Integer> listNo,String isHaveLicense,String licenceNo);

   void submitAdvanceTypeData(String advanceValueId,String advanceValue,String advanceNameid,String advanceName,String noticeValueId,String noticeValue,String noticeNameId,String noticeName);

   void submitPriceForTypeData(ArrayList<PriceForObject.PriceBean> list);

   void submitPriceForTree(ArrayList<TreeStrubWorkObject.TreeClass>list);

   void submitImageData(MultipartBody.Part[] workImage,String isWorkImage);
}
