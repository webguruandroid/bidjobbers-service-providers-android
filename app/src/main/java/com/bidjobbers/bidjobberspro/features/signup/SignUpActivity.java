package com.bidjobbers.bidjobberspro.features.signup;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.register.RegisterResponse;
import com.bidjobbers.bidjobberspro.features.LanguageChange.LanguageChangeActivity;
import com.bidjobbers.bidjobberspro.features.TearmsandCondition.TearmsandConditionActivity;
import com.bidjobbers.bidjobberspro.features.VerifySignup.VerifySignupActivity;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;
import com.bidjobbers.bidjobberspro.utils.CommonUtils;
import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignUpActivity extends BaseActivity implements SignupMvpView {

    ScrollView sc_body;
    LinearLayout header;

    @Inject
    SignUpPresenter<SignupMvpView> testPresenter;

    @BindView(R.id.btn_signUp)
    Button btn_signUp;
    @BindView(R.id.iv_prc)
    CircleImageView iv_prc;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.input_houseAddress)
    EditText input_houseAddress;
    @BindView(R.id.input_first_last_name)
    EditText input_first_last_name;
    @BindView(R.id.input_email)
    EditText input_email;
    @BindView(R.id.input_phone)
    EditText input_phone;
    @BindView(R.id.input_pass)
    EditText input_pass;
    @BindView(R.id.edt_confrm_pass)
    EditText edt_confrm_pass;
    @BindView(R.id.cbTearmCondition)
    CheckBox cbTearmCondition;
    @BindView(R.id.ivChangeLanguage)
    ImageView ivChangeLanguage;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tv_TearmsCondition)
    TextView tv_TearmsCondition;

    LinearLayout ll_go_veryfy;
    String tag,lang;

    Dialog dialog;
    File upload_file_me;

    Context mContext;
    String currentLanguage = "";
    Locale myLocale;
    ArrayList<Image> imagesArrayList1=new ArrayList<>();

    public static boolean isValidEmail(CharSequence target) {
        return (! Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }


    public void setLocale(String localeName) {

//

        if (!localeName.equals(currentLanguage)) {

            Log.e("Language",localeName);

            myLocale = new Locale(localeName);
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();

            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);



        } else {
          //  Toast.makeText(this, "Language already selected!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        Log.e("Language","OnResume");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this);
        lang=prefs.getString("LOCAL_KEY","");
        setLocale(lang);
    }

    private void setToolbar()
    {

        setSupportActionBar(toolbar);
        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_left_arrow_white);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
//            case R.id.action_edit:
//                // Toast.makeText(getApplicationContext(), "Coming Soon", Toast.LENGTH_LONG).show();
//                gotoNext(EditProfileActivity.class,null);
//                return true;
            case android.R.id.home:
                // todo: goto back activity from here


                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);

        mContext = this;
        testPresenter.onAttach(this);
        setToolbar();
        dialog = new Dialog(mContext, R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_otp_send);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);


        ll_go_veryfy = dialog.findViewById(R.id.ll_go_veryfy);
        ll_go_veryfy.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                Intent intentHome = new Intent(mContext, VerifySignupActivity.class);
                intentHome.putExtra("emailAddress",input_email.getText().toString().trim());
                intentHome.putExtra("otp_for","EVAR");
                startActivity(intentHome);
            }
        });

        tv_TearmsCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentHome = new Intent(mContext, TearmsandConditionActivity.class);
                startActivity(intentHome);
            }
        });

        Log.e("Language","OnCreate");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this);
        lang=prefs.getString("LOCAL_KEY","");
        setLocale(lang);

    }

    @OnClick({R.id.btn_signUp,R.id.cv_take_imge,R.id.ivChangeLanguage,R.id.ivBack })
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signUp:
               /*input_houseAddress input_first_last_name input_email input_phone input_pass edt_confrm_pass */
                if(input_houseAddress.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(mContext, getResources().getString(R.string.PleaseEnterCompanyName), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(input_first_last_name.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(mContext, getResources().getString(R.string.PleaseEnterFirstandLastName), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(input_email.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(mContext, getResources().getString(R.string.PleaseenterEmailAddress), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(isValidEmail(input_email.getText().toString().trim()))
                {
                    Toast.makeText(mContext, getResources().getString(R.string.PleaseenterEmailAddress), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(input_phone.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(mContext, getResources().getString(R.string.PleaseEnterPhoneNumber), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(input_pass.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(mContext, getResources().getString(R.string.PleaseEnterPassword), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(edt_confrm_pass.getText().toString().trim().isEmpty())
                {
                    Toast.makeText(mContext, getResources().getString(R.string.PleaseEnterConfirmPassword), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!CommonUtils.isEmailValid(input_email.getText().toString())) {
                   // CommonUtils.isFocousEndOfEditText(edt_email);
                  //  edt_email.setError(Html.fromHtml("<font color='red'>Enter Valid Email Address</font>"));
                    Toast.makeText(mContext, getResources().getString(R.string.PleaseEnterValidEmailAddress), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!cbTearmCondition.isChecked())
                {
                    Toast.makeText(mContext, getResources().getString(R.string.PleaseCheckedTermsandConditions), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(input_phone.getText().toString().length() < 10)
                {

                    Toast.makeText(mContext, getResources().getString(R.string.PleaseEnterValidmobilenumbe), Toast.LENGTH_SHORT).show();
                    return;
                }

                if(input_pass.getText().toString().length() < 6)
                {

                    Toast.makeText(mContext, getResources().getString(R.string.Passwordmustbeixdigits), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(edt_confrm_pass.getText().toString().length() < 6)
                {

                    Toast.makeText(mContext, getResources().getString(R.string.Passwordmustbeixdigits), Toast.LENGTH_SHORT).show();
                    return;
                }

                if(edt_confrm_pass.getText().toString().trim().equals(input_pass.getText().toString().trim()))
                {

                }
                else
                {
                    Toast.makeText(mContext, getResources().getString(R.string.ChoosePasswordshouldbequal), Toast.LENGTH_SHORT).show();
                    return;
                }

                MultipartBody.Part img_body = null;


                if( upload_file_me != null  )
                {
                    RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), upload_file_me);
                    img_body = MultipartBody.Part.createFormData("profile_image", upload_file_me.getName(), reqFile);

                    testPresenter.onSignUpButtonClick( input_houseAddress.getText().toString().trim(),
                            input_first_last_name.getText().toString().trim(),
                            input_email.getText().toString().trim(),
                            input_phone.getText().toString().trim(),
                            input_pass.getText().toString().trim(),
                            img_body);
                }
                else
                {
                    testPresenter.onSignUpButtonWithOutImageClick(
                            input_houseAddress.getText().toString().trim(),
                            input_first_last_name.getText().toString().trim(),
                            input_email.getText().toString().trim(),
                            input_phone.getText().toString().trim(),
                            input_pass.getText().toString().trim());
                }

                //input_first_last_name input_email input_phone input_pass edt_confrm_pass

                break;
            case R.id.cv_take_imge:
                /*Intent intentHome = new Intent(mContext, VerifySignupActivity.class);
                startActivity(intentHome);*/
               // Toast.makeText(mContext, "YES", Toast.LENGTH_SHORT).show();

                ImagePicker.create(this)
                        .returnMode(ReturnMode.CAMERA_ONLY) // set whether pick and / or camera action should return immediate result or not.
                        .folderMode(true) // folder mode (false by default)
                        .toolbarFolderTitle("Folder") // folder selection title
                        .toolbarImageTitle("Tap to select") // image selection title
                        .toolbarArrowColor(Color.BLACK) // Toolbar 'up' arrow color
                        .includeVideo(false) // Show video on image picker
                        .multi() // multi mode (default mode)
                        .limit(1) // max images can be selected (99 by default)
                        .showCamera(true) // show camera or not (true by default)
                        .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
                        .origin(imagesArrayList1) // original selected images, used in multi mode
                        .exclude(imagesArrayList1) // exclude anything that in image.getPath()
                        .theme(R.style.ImagePickerTheme) // must inherit ef_BaseTheme. please refer to sample
                        .enableLog(false) // disabling log
                        .start(); // start image picker activity with request code

                break;

            case R.id.ivChangeLanguage:

                Intent i=new Intent(SignUpActivity.this, LanguageChangeActivity.class);
                i.putExtra("tag","Update");
                startActivity(i);
                break;
            case R.id.ivBack:
                onBackPressed();
                break;


        }
    }


    ArrayList<File> files_send_server = new ArrayList<>();
    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            List<Image> images = ImagePicker.getImages(data);

            /*
               for (int i = 0; i < images.size(); i++) {
                files_send_server.add(images.get(i));
            }*/

            // or get a single image only

            Image image = ImagePicker.getFirstImageOrNull(data);

            // iv_prc.setI

            Glide.with(mContext)
                    .load(images.get(0).getPath())
                   // .placeholder(R.drawable.ic_launcher_background)
                    .into(iv_prc);

            Log.e("File Size",images.get(0).getPath());


            File file = new File(images.get(0).getPath());

            String dir = file.getParent();

            File dirAsFile = file.getParentFile();

            upload_file_me = new File(images.get(0).getPath());

            Log.e("dirAsFile",dir);

        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void actionSignUpButtonClick(String data, boolean isError)
    {
        if(isError==true)
        {
            Toast.makeText(mContext, data, Toast.LENGTH_SHORT).show();
        }
        else
        {
            dialog.show();
        }

        //Toast.makeText(mContext, data, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void actionDeactivateAccount(RegisterResponse response) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, 800);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
