package com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessInfo;

import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface BusinessInfoFragmentMvpView extends BaseFragmentMvpView {

    void setDataFromApi(DataModelForFirstTimeQusAns.BusinessInfo businessInfo);
}
