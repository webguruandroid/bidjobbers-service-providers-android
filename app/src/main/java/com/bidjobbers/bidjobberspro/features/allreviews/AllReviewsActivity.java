package com.bidjobbers.bidjobberspro.features.allreviews;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.ReviewList.ReviewResponse;
import com.bidjobbers.bidjobberspro.features.DetailsReview.ReviewDetailsActivity;
import com.bidjobbers.bidjobberspro.features.profiledetails.ProfileHelper.AllReviewAdapter;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AllReviewsActivity extends BaseActivity implements AllReviewsMvpView{

    @BindView(R.id.rv_reviews)
    RecyclerView rv_reviews;

    @Inject
    AllReviewAdapter reviewAdapter;

    @BindView(R.id.iv_mnu)
    ImageView iv_mnu;
    Context mContext;
    @Inject
    AllReviewsPresenter<AllReviewsMvpView> allpresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_reviews);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);

        mContext = this;
        allpresenter.onAttach(this);
        allpresenter.getReview();



        rv_reviews.setLayoutManager(new LinearLayoutManager(this));
        rv_reviews.setAdapter(reviewAdapter);


        reviewAdapter.setAdapterListner(new AllReviewAdapter.ReviewListner() {
            @Override
            public void onItemClick(String Image,String Desc,String Name,String Date,String Time,String Rating) {
                Intent i =new Intent(getApplicationContext(), ReviewDetailsActivity.class);
                i.putExtra("image",Image);
                i.putExtra("desc",Desc);
                i.putExtra("name",Name);
                i.putExtra("date",Date);
                i.putExtra("time",Time);
                i.putExtra("rating",Rating);
                startActivity(i);
            }
        });


        iv_mnu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition( R.anim.slide_from_top,R.anim.slide_in_top);
            }
        });



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition( R.anim.slide_from_top,R.anim.slide_in_top);
    }

    @Override
    public void successfullyGetReview(ReviewResponse reviewResponse) {
        if(reviewResponse.getResponseCode()==1){

                reviewAdapter.loadList(reviewResponse.getResponseData().getReviewList());
                reviewAdapter.notifyDataSetChanged();

        }
        else
        {

        }
    }
}
