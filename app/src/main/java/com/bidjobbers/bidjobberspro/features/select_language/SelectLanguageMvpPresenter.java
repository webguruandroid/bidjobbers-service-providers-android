package com.bidjobbers.bidjobberspro.features.select_language;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface SelectLanguageMvpPresenter <V extends SelectLanguageMvpView> extends MvpPresenter<V> {

    void getSetLocal(String local);
    void setLanguage(String language);
}
