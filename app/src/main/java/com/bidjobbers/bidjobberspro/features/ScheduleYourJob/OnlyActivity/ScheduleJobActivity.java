package com.bidjobbers.bidjobberspro.features.ScheduleYourJob.OnlyActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.viewpager.widget.ViewPager;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.TimeSlot.TimeSlotResponse;
import com.bidjobbers.bidjobberspro.features.ScheduleYourJob.AvalableDateViewPager.ViewPagerAvalableDateAdapter;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.TabBean;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;
import com.bidjobbers.bidjobberspro.utils.Constants;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class ScheduleJobActivity extends BaseActivity implements ScheduleJobMvpView {

    ArrayList<TabBean> tabBeans= new ArrayList<>();
    ViewPagerAvalableDateAdapter viewPagerAvalableDateAdapter;
    TabLayout sliding_tabs;
    ViewPager viewpager;
    LinearLayout ll_done,ll_done_disable;
    ImageView iv_mnu;


    @Inject
    ScheduleJobPresenter<ScheduleJobMvpView> scheduleJobPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_job);
        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        scheduleJobPresenter.onAttach(this);
        scheduleJobPresenter.ongetSchedule();


        sliding_tabs = (TabLayout) findViewById(R.id.sliding_tabs);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        ll_done = (LinearLayout) findViewById(R.id.ll_done);
        iv_mnu = (ImageView) findViewById(R.id.iv_mnu);
        ll_done_disable=findViewById(R.id.ll_done_disable);

        handelViewPager();

        iv_mnu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.putExtra("Time", Constants.select_slot_time);
                intent.putExtra("Date",Constants.select_slot__date_id);
                intent.putExtra("StartDate",Constants.select_slot_date);
                intent.putExtra("TimeName",Constants.select_slot_time_name);
                intent.putExtra("DateName",Constants.select_slot_date_name);
                setResult(2,intent);
                finish();
            }
        });

        ll_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent=new Intent();
                intent.putExtra("Time", Constants.select_slot_time);
                intent.putExtra("Date",Constants.select_slot__date_id);
                intent.putExtra("StartDate",Constants.select_slot_date);
                intent.putExtra("TimeName",Constants.select_slot_time_name);
                intent.putExtra("DateName",Constants.select_slot_date_name);
                setResult(2,intent);
                finish();
            }
        });


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent=new Intent();
        intent.putExtra("Time", Constants.select_slot_time);
        intent.putExtra("Date",Constants.select_slot__date_id);
        intent.putExtra("StartDate",Constants.select_slot_date);
        intent.putExtra("TimeName",Constants.select_slot_time_name);
        intent.putExtra("DateName",Constants.select_slot_date_name);
        setResult(2,intent);
        finish();//fi


    }

    @Override
    public void onSuccessfullyGetTimeSlot(TimeSlotResponse.ResponseDataBean timeSlotResponse) {

        if(timeSlotResponse.getAvalableSlots().size()>0) {

            for (int i = 0; i < timeSlotResponse.getAvalableSlots().size(); i++) {

                TimeSlotResponse.ResponseDataBean.AvalableSlotsBean avalableSlotsBean = timeSlotResponse.getAvalableSlots().get(i);
                String[] words = avalableSlotsBean.getStart_date().split("-");
                tabBeans.add(new TabBean(avalableSlotsBean.getId(), avalableSlotsBean.getDayName(),avalableSlotsBean.getDayName()+"\n"+words[2],avalableSlotsBean.getStart_date(),avalableSlotsBean.getSlots().size()+""));

            }

            viewPagerAvalableDateAdapter = new ViewPagerAvalableDateAdapter(getSupportFragmentManager(), tabBeans);

            viewpager.setAdapter(viewPagerAvalableDateAdapter);
            sliding_tabs.setupWithViewPager(viewpager);

            viewPagerAvalableDateAdapter.notifyDataSetChanged();


        }



    }

    @Override
    public void onDeactivate(String data) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(ScheduleJobActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, 800);
    }


    private void handelViewPager()
    {
        Log.e("Schedule","Called");
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
                Log.e("Schedule",tabBeans.get(i).getTabStartDate());
                switch(i)
                {

                    case 0:
                        Constants.select_slot__date_id =tabBeans.get(0).getTabId();
                        Constants.select_slot_date=tabBeans.get(0).getTabStartDate();
                        Constants.select_slot_date_name=tabBeans.get(0).getTabName();
                        if(tabBeans.get(0).getTimeSlot().equals("0"))
                        {
                            ll_done_disable.setVisibility(View.VISIBLE);
                            ll_done.setVisibility(View.GONE);
                        }
                        else
                        {
                            ll_done_disable.setVisibility(View.GONE);
                            ll_done.setVisibility(View.VISIBLE);
                        }
                        break;

                    case 1:
                        Constants.select_slot__date_id =tabBeans.get(1).getTabId();
                        Constants.select_slot_date=tabBeans.get(1).getTabStartDate();
                        Constants.select_slot_date_name=tabBeans.get(1).getTabName();
                        if(tabBeans.get(1).getTimeSlot().equals("0"))
                        {
                            ll_done_disable.setVisibility(View.VISIBLE);
                            ll_done.setVisibility(View.GONE);
                        }
                        else
                        {
                            ll_done_disable.setVisibility(View.GONE);
                            ll_done.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 2:
                        Constants.select_slot__date_id =tabBeans.get(2).getTabId();
                        Constants.select_slot_date=tabBeans.get(2).getTabStartDate();
                        Constants.select_slot_date_name=tabBeans.get(2).getTabName();
                        if(tabBeans.get(2).getTimeSlot().equals("0"))
                        {
                            ll_done_disable.setVisibility(View.VISIBLE);
                            ll_done.setVisibility(View.GONE);
                        }
                        else
                        {
                            ll_done_disable.setVisibility(View.GONE);
                            ll_done.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 3:
                        Constants.select_slot__date_id =tabBeans.get(3).getTabId();
                        Constants.select_slot_date=tabBeans.get(3).getTabStartDate();
                        Constants.select_slot_date_name=tabBeans.get(3).getTabName();
                        if(tabBeans.get(3).getTimeSlot().equals("0"))
                        {
                            ll_done_disable.setVisibility(View.VISIBLE);
                            ll_done.setVisibility(View.GONE);
                        }
                        else
                        {
                            ll_done_disable.setVisibility(View.GONE);
                            ll_done.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 4:
                        Constants.select_slot__date_id =tabBeans.get(4).getTabId();
                        Constants.select_slot_date=tabBeans.get(4).getTabStartDate();
                        Constants.select_slot_date_name=tabBeans.get(4).getTabName();
                        if(tabBeans.get(4).getTimeSlot().equals("0"))
                        {
                            ll_done_disable.setVisibility(View.VISIBLE);
                            ll_done.setVisibility(View.GONE);
                        }
                        else
                        {
                            ll_done_disable.setVisibility(View.GONE);
                            ll_done.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 5:
                        Constants.select_slot__date_id =tabBeans.get(5).getTabId();
                        Constants.select_slot_date=tabBeans.get(5).getTabStartDate();
                        Constants.select_slot_date_name=tabBeans.get(5).getTabName();
                        if(tabBeans.get(5).getTimeSlot().equals("0"))
                        {
                            ll_done_disable.setVisibility(View.VISIBLE);
                            ll_done.setVisibility(View.GONE);
                        }
                        else
                        {
                            ll_done_disable.setVisibility(View.GONE);
                            ll_done.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 6:
                        Constants.select_slot__date_id =tabBeans.get(6).getTabId();
                        Constants.select_slot_date=tabBeans.get(6).getTabStartDate();
                        Constants.select_slot_date_name=tabBeans.get(6).getTabName();
                        if(tabBeans.get(6).getTimeSlot().equals("0"))
                        {
                            ll_done_disable.setVisibility(View.VISIBLE);
                            ll_done.setVisibility(View.GONE);
                        }
                        else
                        {
                            ll_done_disable.setVisibility(View.GONE);
                            ll_done.setVisibility(View.VISIBLE);
                        }
                        break;

                }
            }

            @Override
            public void onPageSelected(int i) {
                switch(i)
                {
                    case 0:
                        Constants.select_slot__date_id =tabBeans.get(0).getTabId();
                        Constants.select_slot_date=tabBeans.get(0).getTabStartDate();
                        Constants.select_slot_date_name=tabBeans.get(0).getTabName();
                        if(tabBeans.get(0).getTimeSlot().equals("0"))
                        {
                            ll_done_disable.setVisibility(View.VISIBLE);
                            ll_done.setVisibility(View.GONE);
                        }
                        else
                        {
                            ll_done_disable.setVisibility(View.GONE);
                            ll_done.setVisibility(View.VISIBLE);
                        }
                        break;

                    case 1:
                        Constants.select_slot__date_id =tabBeans.get(1).getTabId();
                        Constants.select_slot_date=tabBeans.get(1).getTabStartDate();
                        Constants.select_slot_date_name=tabBeans.get(1).getTabName();
                        if(tabBeans.get(1).getTimeSlot().equals("0"))
                        {
                            ll_done_disable.setVisibility(View.VISIBLE);
                            ll_done.setVisibility(View.GONE);
                        }
                        else
                        {
                            ll_done_disable.setVisibility(View.GONE);
                            ll_done.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 2:
                        Constants.select_slot__date_id =tabBeans.get(2).getTabId();
                        Constants.select_slot_date=tabBeans.get(2).getTabStartDate();
                        Constants.select_slot_date_name=tabBeans.get(2).getTabName();
                        if(tabBeans.get(2).getTimeSlot().equals("0"))
                        {
                            ll_done_disable.setVisibility(View.VISIBLE);
                            ll_done.setVisibility(View.GONE);
                        }
                        else
                        {
                            ll_done_disable.setVisibility(View.GONE);
                            ll_done.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 3:
                        Constants.select_slot__date_id =tabBeans.get(3).getTabId();
                        Constants.select_slot_date=tabBeans.get(3).getTabStartDate();
                        Constants.select_slot_date_name=tabBeans.get(3).getTabName();
                        if(tabBeans.get(3).getTimeSlot().equals("0"))
                        {
                            ll_done_disable.setVisibility(View.VISIBLE);
                            ll_done.setVisibility(View.GONE);
                        }
                        else
                        {
                            ll_done_disable.setVisibility(View.GONE);
                            ll_done.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 4:
                        Constants.select_slot__date_id =tabBeans.get(4).getTabId();
                        Constants.select_slot_date=tabBeans.get(4).getTabStartDate();
                        Constants.select_slot_date_name=tabBeans.get(4).getTabName();
                        if(tabBeans.get(4).getTimeSlot().equals("0"))
                        {
                            ll_done_disable.setVisibility(View.VISIBLE);
                            ll_done.setVisibility(View.GONE);
                        }
                        else
                        {
                            ll_done_disable.setVisibility(View.GONE);
                            ll_done.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 5:
                        Constants.select_slot__date_id =tabBeans.get(5).getTabId();
                        Constants.select_slot_date=tabBeans.get(5).getTabStartDate();
                        Constants.select_slot_date_name=tabBeans.get(5).getTabName();
                        if(tabBeans.get(5).getTimeSlot().equals("0"))
                        {
                            ll_done_disable.setVisibility(View.VISIBLE);
                            ll_done.setVisibility(View.GONE);
                        }
                        else
                        {
                            ll_done_disable.setVisibility(View.GONE);
                            ll_done.setVisibility(View.VISIBLE);
                        }
                        break;
                    case 6:
                        Constants.select_slot__date_id =tabBeans.get(6).getTabId();
                        Constants.select_slot_date=tabBeans.get(6).getTabStartDate();
                        Constants.select_slot_date_name=tabBeans.get(6).getTabName();
                        if(tabBeans.get(6).getTimeSlot().equals("0"))
                        {
                            ll_done_disable.setVisibility(View.VISIBLE);
                            ll_done.setVisibility(View.GONE);
                        }
                        else
                        {
                            ll_done_disable.setVisibility(View.GONE);
                            ll_done.setVisibility(View.VISIBLE);
                        }
                        break;

                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        Log.e("Schedule",Constants.select_slot_date);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
