package com.bidjobbers.bidjobberspro.features.OnTimeScreen.ImageDataFragment;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface ImagePickerFragmentMvpPresenter <V extends ImagePickerFragmentMvpView> extends BaseFragmentMvpPresenter<V> {
    void onGetDataFromApi();
}
