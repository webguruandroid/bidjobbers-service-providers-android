package com.bidjobbers.bidjobberspro.features.settings;

import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface SettingsMvpView  extends MvpView {

    void successfullyGetProfile(getProfileResponse getProfileResponse);
    void successfullyLogout();
    void onDeactivate(String data);
}
