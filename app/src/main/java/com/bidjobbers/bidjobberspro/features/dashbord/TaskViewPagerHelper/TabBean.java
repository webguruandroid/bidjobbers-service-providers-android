package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper;

public class TabBean {

    String tabId="";
    String tabName="";
    String tabNameWithDate="";
    String tabStartDate="";
    String timeSlot="";

    public TabBean(String tabId, String tabName, String tabNameWithDate, String tabStartDate,String timeSlot) {
        this.tabId = tabId;
        this.tabName = tabName;
        this.tabNameWithDate = tabNameWithDate;
        this.tabStartDate = tabStartDate;
        this.timeSlot=timeSlot;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getTabStartDate() {
        return tabStartDate;
    }

    public void setTabStartDate(String tabStartDate) {
        this.tabStartDate = tabStartDate;
    }

    public String getTabId() {
        return tabId;
    }

    public void setTabId(String tabId) {
        this.tabId = tabId;
    }

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }

    public String getTabNameWithDate() {
        return tabNameWithDate;
    }

    public void setTabNameWithDate(String tabNameWithDate) {
        this.tabNameWithDate = tabNameWithDate;
    }
}
