package com.bidjobbers.bidjobberspro.features.ForgotOtpVeryfy;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.features.resetpassword.ResetPasswordActivity;
import com.bidjobbers.bidjobberspro.features.select_language.SelectLanguageActivity;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerifyOtpActivity extends BaseActivity implements VerifyOtpMvpView,View.OnClickListener {

    @Inject
    VerifyOtpPresenter<VerifyOtpMvpView> verifyOtpPresenter;
    @BindView(R.id.input_houseAddress)
    EditText input_houseAddress;
    @BindView(R.id.ivChangeLanguage)
    ImageView ivChangeLanguage;
    @BindView(R.id.ivBack)
    ImageView ivBack;


    String otp_for="";
    String emailId="";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        verifyOtpPresenter.onAttach(this);

        //get value by intent

        Intent intent = getIntent();
        //otp_for =  intent.getStringExtra("otp_for");
        emailId =  intent.getStringExtra("emailId");

    }

    public void resetPassWord(View view)
    {
        verifyOtpPresenter.onGetStarted(   emailId,   input_houseAddress.getText().toString().trim() );
    }

    @Override
    public void actionGetStarted()
    {
        Intent intent = new Intent(this, ResetPasswordActivity.class);
        intent.putExtra("otp",input_houseAddress.getText().toString().trim());
        intent.putExtra("email",emailId);
        Log.e("VerifyOtpActivity",emailId);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    public void resentOtp(View view)
    {
        verifyOtpPresenter.actionResentOtp(emailId);
    }

    boolean doubleBackToExitPressedOnce = false;

//    @Override
//    public void onBackPressed() {
////        if (doubleBackToExitPressedOnce) {
////            finish();
////            return;
////        }
////        this.doubleBackToExitPressedOnce = true;
////        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
////        new Handler().postDelayed(new Runnable() {
////            @Override
////            public void run() {
////                doubleBackToExitPressedOnce=false;
////            }
////        }, 2000);}
//    }

    @OnClick({R.id.ivChangeLanguage,R.id.ivBack })
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.ivChangeLanguage:
              //  Toast.makeText(getApplicationContext(),"click language",Toast.LENGTH_SHORT).show();
                Intent i=new Intent(VerifyOtpActivity.this, SelectLanguageActivity.class);
                startActivity(i);
                break;
            case R.id.ivBack:
                onBackPressed();
                break;
        }
    }


}
