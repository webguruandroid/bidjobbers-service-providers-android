package com.bidjobbers.bidjobberspro.features.allreviews;

import com.bidjobbers.bidjobberspro.data.network.models.ReviewList.ReviewResponse;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface AllReviewsMvpView extends MvpView {

    void successfullyGetReview(ReviewResponse reviewResponse);

}
