package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.Cancelled;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface CancelledBodyMvpPresenter<V extends CancelledBodyMvpView> extends BaseFragmentMvpPresenter<V> {
     void getAllTasks(String currentpage);
}
