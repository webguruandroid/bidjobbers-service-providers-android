package com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessLocatiion;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.CityCountry.CityCountryResponse;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class BusinessLocationFragmentPresenter
        <V extends  BusinessLocationFragmentMvpView>
        extends BaseFragmentPresenter<V>
        implements   BusinessLocationFragmentMvpPresenter<V>{
    @Inject
    public BusinessLocationFragmentPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }


    @Override
    public void onGetDataFromApi() {
        Gson gson = new Gson();
        String json = getDataManager().getMyObject();
        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
        getMvpView().setDataFromApi(obj.businessLocation);
    }

    @Override
    public void onGetCityCountry() {
        Log.e("City","City");
        getCompositeDisposable().add(getDataManager().getCityCountry()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CityCountryResponse>() {
                    @Override
                    public void onSuccess(CityCountryResponse cityCountryResponse) {
                        Log.e("City",cityCountryResponse.getResponseText());
                        getMvpView().successfullygetCity(cityCountryResponse);

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }));
    }

    @Override
    public void onGetCity(String ciuntryId) {
        getCompositeDisposable().add(getDataManager().getCityCountry()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CityCountryResponse>() {
                    @Override
                    public void onSuccess(CityCountryResponse cityCountryResponse) {
                        Log.e("City",cityCountryResponse.getResponseText());
                        for(int i=0;i<cityCountryResponse.getResponseData().getMasterData().size();i++) {


                            if (cityCountryResponse.getResponseData().getMasterData().get(i).getCountry_id().equals(ciuntryId)) {


                                  getMvpView().successCity(cityCountryResponse.getResponseData().getMasterData().get(i).getCity());
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }));
    }
}
