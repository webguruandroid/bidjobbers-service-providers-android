package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.Recivetask.ReciveTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Recivetask.ReciveTaskResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class TaskItemBodyPresenter <V extends TaskItemBodyMvpView>
        extends BaseFragmentPresenter<V>
        implements TaskItemBodyMvpPresenter<V> {


    @Inject
    public TaskItemBodyPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }

    @Override
    public void getAllTasks(String currentpage) {
        if(getMvpView().isNetworkConnected()) {
        getMvpView().showLoading();
       // getMvpView().showMessage(getDataManager().getLocalValue());
        ReciveTaskRequest reciveTaskRequest=new ReciveTaskRequest(getDataManager().getLocalValue(),currentpage);


        getCompositeDisposable().add(getDataManager().getReciveTask("Bearer "+getDataManager().getAccess_token(),"application/json",reciveTaskRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ReciveTaskResponse>() {
                    @Override
                    public void onSuccess(ReciveTaskResponse reciveTaskResponse) {



                     //   getMvpView().showMessage(reciveTaskResponse.getResponseText());

                        getMvpView().hideLoading();
                        if(reciveTaskResponse.getResponseCode()==1) {
                            getMvpView().allTasksName(reciveTaskResponse);
                        }
                        else if(reciveTaskResponse.getResponseCode()==401)
                        {
                            getMvpView().onError(reciveTaskResponse.getResponseText());
                            getMvpView().onDeActive(reciveTaskResponse.getResponseText());
                        }
                        else
                        {
                            getMvpView().allTasksName(reciveTaskResponse);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));
        }
        else
        {
            getMvpView().hideLoading();
            getMvpView().showAlert("No internet Connection");
        }

    }
}
