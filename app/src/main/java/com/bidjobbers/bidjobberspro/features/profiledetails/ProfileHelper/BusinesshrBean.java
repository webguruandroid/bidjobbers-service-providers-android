package com.bidjobbers.bidjobberspro.features.profiledetails.ProfileHelper;

public class BusinesshrBean {
   String dayName;
   String startTime;
   String endTime;

    public BusinesshrBean() {

    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "BusinesshrBean{" +
                "dayName='" + dayName + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                '}';
    }
}
