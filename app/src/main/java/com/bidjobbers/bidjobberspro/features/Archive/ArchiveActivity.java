package com.bidjobbers.bidjobberspro.features.Archive;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList.BlockCustomerListResponse;
import com.bidjobbers.bidjobberspro.data.network.models.UnBlockUser.UnBlockUserResponse;
import com.bidjobbers.bidjobberspro.features.dashbord.ui.gallery.BlockListingAdapter;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;

import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArchiveActivity extends BaseActivity implements ArchiveMvpView {


    @BindView(R.id.rvBlockList)
    RecyclerView rvBlockList;

    @BindView(R.id.llContainer)
    LinearLayout llContainer;
    @BindView(R.id.rlEmpty)
    RelativeLayout rlEmpty;
    @BindView(R.id.iv_mnu)
    ImageView iv_mnu;

    BlockListingAdapter blockListingAdapter;
    @BindView(R.id.sp_block)
    Spinner sp_block;

    ArrayList<String>list=new ArrayList<>();
    @Inject
    ArchivePresenter<ArchiveMvpView> archivePresenter;
    Context mContext;
    String currentPage="1";
    ArrayList<BlockCustomerListResponse.ResponseDataBean.BlocklistBean> productItemList;
    private String selectedYear;
    private int yr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_archive);
        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        mContext = this;
        archivePresenter.onAttach(this);
        yr=Calendar.getInstance().get(Calendar.YEAR);
        Log.e("Year",yr+"");

        rvBlockList.setLayoutManager(new LinearLayoutManager(mContext));
        rvBlockList.setAdapter(blockListingAdapter);


        productItemList = new ArrayList<BlockCustomerListResponse.ResponseDataBean.BlocklistBean>();
        productItemList.clear();
        archivePresenter.getBlockCustomer(selectedYear,currentPage);
        blockListingAdapter = new BlockListingAdapter(productItemList,mContext,rvBlockList);
        rvBlockList.setAdapter(blockListingAdapter);

        list.add(getResources().getString(R.string.pastsix));
        list.add(yr-1+"");
        list.add(yr-2+"");
        list.add(yr-3+"");
        list.add(yr-4+"");
        list.add(yr-5+"");



        setSpinner();

        iv_mnu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
              //  overridePendingTransition( R.anim.slide_from_top,R.anim.slide_in_top);
            }
        });

    }


    private void setSpinner()
    {
//Past 6 months ; 2019 ; 2018 ; 2017 ; 2016 ; 2015
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        //set the view for the Drop down list
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the ArrayAdapter to the spinner
        sp_block.setAdapter(dataAdapter);
        sp_block.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==0){
                    selectedYear = "Past 6 months";
                }
                else {
                    selectedYear = list.get(i);
                }
                archivePresenter.getBlockCustomer(selectedYear,currentPage);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                selectedYear=list.get(0);
                archivePresenter.getBlockCustomer(selectedYear,currentPage);
            }

        });
    }

    @Override
    public void successfullyGetBlockCustomer(BlockCustomerListResponse blockCustomerListResponse) {



        if(blockCustomerListResponse.getResponseCode()==1)
        {

            llContainer.setVisibility(View.VISIBLE);
            rlEmpty.setVisibility(View.GONE);

            if(blockCustomerListResponse.getResponseData().getBlocklist().size()>0) {

               // blockListingAdapter.loadList(blockCustomerListResponse.getResponseData().getBlocklist());


                if(currentPage.equals("1")){

                    productItemList.clear();
                    productItemList.addAll(blockCustomerListResponse.getResponseData().getBlocklist());
                }else{

                    productItemList.remove(productItemList.size() - 1);
                    blockListingAdapter.notifyItemRemoved(productItemList.size());

                    productItemList.addAll(blockCustomerListResponse.getResponseData().getBlocklist());
                    //mAdapter.notifyDataSetChanged();
                }

                blockListingAdapter.notifyDataSetChanged();
                blockListingAdapter.setLoaded();
                currentPage =blockCustomerListResponse.getResponseData().getPagination().getNext_page();
                blockListingAdapter.setmListener(new BlockListingAdapter.AddButtonListener() {
                    @Override
                    public void onItemClick(int pos, final String taskId) {

                        try {
                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setTitle("Confirm Unblock");
                            builder.setMessage("Are you sure to unblock this customer for you?");
                            builder.setCancelable(false);
                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    archivePresenter.onUnBlockUser(taskId);
                                }
                            });
                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                            AlertDialog alert = builder.create();
                            alert.show();
                        } catch (Exception e){

                        }
                    }
                });

            }
        }
        else
        {

            llContainer.setVisibility(View.GONE);
            rlEmpty.setVisibility(View.VISIBLE);
            //!
        }

    }

    @Override
    public void onSuccessfullyUnBlockUser(UnBlockUserResponse unBlockUserResponse) {
        archivePresenter.getBlockCustomer(selectedYear,currentPage);
    }
}
