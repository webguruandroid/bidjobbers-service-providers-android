package com.bidjobbers.bidjobberspro.features.profiledetails.ProfileHelper;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.servicedetails.ServiceDetailsResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusinessHrAdapter extends RecyclerView.Adapter<BusinessHrAdapter.ViewHolder> {

    Context mContext;
    private final ArrayList<ServiceDetailsResponse.ResponseDataBean.BuisnessHoursDetailsBean.BusinessHrDataBean> mValues;



    public BusinessHrAdapter(ArrayList<ServiceDetailsResponse.ResponseDataBean.BuisnessHoursDetailsBean.BusinessHrDataBean> mValues) {
        this.mValues = mValues;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        mContext = viewGroup.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View listItem= layoutInflater.inflate(R.layout.layout_businesshr_item, viewGroup, false);

        BusinessHrAdapter.ViewHolder viewHolder = new BusinessHrAdapter.ViewHolder(listItem);

        return viewHolder;
    }

    public void loadList(List<ServiceDetailsResponse.ResponseDataBean.BuisnessHoursDetailsBean.BusinessHrDataBean> items) {
        mValues.clear();
        mValues.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        ServiceDetailsResponse.ResponseDataBean.BuisnessHoursDetailsBean.BusinessHrDataBean mDataBean = mValues.get(i);

        Log.e("Adapter",mDataBean.toString());
        if(mDataBean.getIsSelected().equals("1")) {
            viewHolder.llMain.setVisibility(View.VISIBLE);
            viewHolder.tvDayName.setText(mDataBean.getDayName() + " : ");
            viewHolder.tvStartTime.setText(mDataBean.getFrom_time() + " - ");
            viewHolder.tnEndTime.setText(mDataBean.getEnd_time());
        }


    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{


        @BindView(R.id.tvDayName)
        TextView tvDayName;
        @BindView(R.id.tvStartTime)
        TextView tvStartTime;
        @BindView(R.id.tnEndTime)
        TextView tnEndTime;
        @BindView(R.id.llMain)
        LinearLayout llMain;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }
    }


}
