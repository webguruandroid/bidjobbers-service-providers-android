package com.bidjobbers.bidjobberspro.features.OnTimeScreen.ImageDataFragment;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.ImageDataFragment.ImagePickerHelper.TakeImageAdapter;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;
import com.bidjobbers.bidjobberspro.utils.ImagePicker;
import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.model.Image;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ImagePickerFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ImagePickerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ImagePickerFragment extends BaseFragment
                                 implements BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.OnMultiImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate,View.OnClickListener ,ImagePickerFragmentMvpView{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    @BindView(R.id.llPhoto)
    LinearLayout llPhoto;
    @BindView(R.id.squareImageView)
    ImageView squareImageView;
    @BindView(R.id.squareImageViewOne)
    ImageView squareImageViewOne;
    @BindView(R.id.squareImageViewTwo)
    ImageView squareImageViewTwo;
    @BindView(R.id.squareImageViewThree)
    ImageView squareImageViewThree;
    @BindView(R.id.squareImageViewFour)
    ImageView squareImageViewFour;
    @BindView(R.id.chk_ys)
    RadioButton chk_ys;
    @BindView(R.id.chk_no)
    RadioButton chk_no;
    @BindView(R.id.ivCrossOne)
    ImageView ivCrossOne;
    @BindView(R.id.ivCrossTwo)
    ImageView ivCrossTwo;
    @BindView(R.id.ivCrossThree)
    ImageView ivCrossThree;
    @BindView(R.id.ivCrossFour)
    ImageView ivCrossFour;
    @BindView(R.id.ivCrossFive)
    ImageView ivCrossFive;


    private File upload_file;

    private final static int IMAGE_RESULT_ONE = 100;
    private final static int IMAGE_RESULT_TWO = 200;
    private final static int IMAGE_RESULT_THREE = 300;
    private final static int IMAGE_RESULT_FOUR = 400;
    private final static int IMAGE_RESULT_FIVE = 500;

    Uri picUri;

    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();

    private final static int ALL_PERMISSIONS_RESULT = 107;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    ArrayList<String> a= new ArrayList<String>();
    TakeImageAdapter adapter;
    ArrayList<Image> imagesArrayList1=new ArrayList<>();
    Context mContext;
    private static final int CAMERA_REQUEST = 1888;
    static int ivCameraIndex = 0;
    File upload_file_me_one=null,upload_file_me_two,upload_file_me_three,upload_file_me_four,upload_file_me_five;
    String isSelected;


    @Inject
    ImagePickerFragmentPresenter<ImagePickerFragmentMvpView> imagePickerFragmentPresenter;



    public ImagePickerFragment() {
        a.add("a");
        a.add("b");
        a.add("c");
        a.add("d");
        a.add("e");
    }


    // TODO: Rename and change types and number of parameters
    public static ImagePickerFragment newInstance(String param1, String param2) {
        ImagePickerFragment fragment = new ImagePickerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_image_picker, container, false);
        setUnBinder(ButterKnife.bind(this,rootView));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }

        mContext = getContext();
        imagePickerFragmentPresenter.onAttach(this);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = getContext();

        permissions.add(CAMERA);
        permissions.add(WRITE_EXTERNAL_STORAGE);
        permissions.add(READ_EXTERNAL_STORAGE);
        permissionsToRequest = findUnAskedPermissions(permissions);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (permissionsToRequest.size() > 0)
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }

        setChangeListner();
        imagePickerFragmentPresenter.onGetDataFromApi();

    }

    private void setChangeListner()
    {
        chk_ys.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                {
                    llPhoto.setVisibility(View.VISIBLE);
                    isSelected="1";
                }

            }
        });

        chk_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                {
                    llPhoto.setVisibility(View.GONE);
                    isSelected="0";
                }

            }
        });
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void loadImage(File imageFile, ImageView ivImage) {

    }

    @Override
    public void onMultiImageSelected(List<Uri> uriList, String tag) {

    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {

    }

    @OnClick({R.id.squareImageView,R.id.squareImageViewOne,R.id.squareImageViewTwo,R.id.squareImageViewThree,R.id.squareImageViewFour,R.id.ivCrossOne,R.id.ivCrossTwo,R.id.ivCrossThree,R.id.ivCrossFour,R.id.ivCrossFive})
    public void onClick(View v) {

        if(v.getId()==R.id.squareImageView)
        {
            startActivityForResult(getPickImageChooserIntent(), IMAGE_RESULT_ONE);
            ivCrossOne.setVisibility(View.VISIBLE);
        }
        else if(v.getId()==R.id.squareImageViewOne)
        {
            startActivityForResult(getPickImageChooserIntent(), IMAGE_RESULT_TWO);
            ivCrossTwo.setVisibility(View.VISIBLE);
        }
        else if(v.getId()==R.id.squareImageViewTwo)
        {

            startActivityForResult(getPickImageChooserIntent(), IMAGE_RESULT_THREE);
            ivCrossThree.setVisibility(View.VISIBLE);
        }
        else if(v.getId()==R.id.squareImageViewThree) {
            startActivityForResult(getPickImageChooserIntent(), IMAGE_RESULT_FOUR);
            ivCrossFour.setVisibility(View.VISIBLE);
        }
        else if(v.getId()==R.id.squareImageViewFour)
        {
            startActivityForResult(getPickImageChooserIntent(), IMAGE_RESULT_FIVE);
            ivCrossFive.setVisibility(View.VISIBLE);
        }

        else if(v.getId()==R.id.ivCrossOne)
        {
            squareImageView.setImageResource(R.drawable.image_placeholder);
            ivCrossOne.setVisibility(View.GONE);
        }
        else if(v.getId()==R.id.ivCrossTwo)
        {
            squareImageViewOne.setImageResource(R.drawable.image_placeholder);
            ivCrossTwo.setVisibility(View.GONE);
        }
        else if(v.getId()==R.id.ivCrossThree)
        {
            squareImageViewTwo.setImageResource(R.drawable.image_placeholder);
            ivCrossThree.setVisibility(View.GONE);
        }
        else if(v.getId()==R.id.ivCrossFour)
        {
            squareImageViewThree.setImageResource(R.drawable.image_placeholder);
            ivCrossFour.setVisibility(View.GONE);
        }
        else if(v.getId()==R.id.ivCrossFive)
        {
            squareImageViewFour.setImageResource(R.drawable.image_placeholder);
            ivCrossFive.setVisibility(View.GONE);
        }
    }

    @Override
    public void setDataFromApi(DataModelForFirstTimeQusAns.PhotoDescribeProject workPhoto) {
        Log.e("Photo",workPhoto.toString());
        if(workPhoto!=null) {
            if (workPhoto.getIsAddad().equals("1")) {
                chk_ys.setChecked(true);
                chk_no.setChecked(false);
            } else {
                chk_ys.setChecked(false);
                chk_no.setChecked(true);
                llPhoto.setVisibility(View.GONE);
            }

            if (!workPhoto.getImageOne().equals("")) {
                Glide.with(mContext)
                        .load(workPhoto.getImageUrl() + "/" + workPhoto.getImageOne())
                        //.placeholder(R.drawable.image_placeholder)
                        .into(squareImageView);
            }
            if (!workPhoto.getImageTwo().equals("")) {
                Glide.with(mContext)
                        .load(workPhoto.getImageUrl() + "/" + workPhoto.getImageTwo())
                        // .placeholder(R.drawable.image_placeholder)
                        .into(squareImageViewOne);
            }
            if (!workPhoto.getImageThree().equals("")) {
                Glide.with(mContext)
                        .load(workPhoto.getImageUrl() + "/" + workPhoto.getImageThree())
                        // .placeholder(R.drawable.image_placeholder)
                        .into(squareImageViewTwo);
            }
            if (!workPhoto.getImageFour().equals("")) {
                Glide.with(mContext)
                        .load(workPhoto.getImageUrl() + "/" + workPhoto.getImageFour())
                        //.placeholder(R.drawable.image_placeholder)
                        .into(squareImageViewThree);
            }
            if (!workPhoto.getImageFive().equals("")) {
                Glide.with(mContext)
                        .load(workPhoto.getImageUrl() + "/" + workPhoto.getImageFive())
                        // .placeholder(R.drawable.image_placeholder)
                        .into(squareImageViewFour);
            }

        }
    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class ImageTask extends AsyncTask<Object, Void, Bitmap> {

        int resultCode;
        Intent data;
        ProgressDialog pd;
        int targetFileFlag;
        ImageView targetView;

        public ImageTask(int resultCode,Intent data,int f) {

            this.resultCode = resultCode;
            this.data = data;
            this.targetFileFlag = f;


            pd =new ProgressDialog(mContext);
            pd.setCancelable(false);
            pd.setMessage("Loading");
        }

        @Override
        protected Bitmap doInBackground(Object... params) {
            Bitmap bitmap = ImagePicker.getImageFromResult(mContext, resultCode, data);

            if(targetFileFlag == CAMERA_REQUEST){
                upload_file = ImagePicker.getSavedFile(mContext, resultCode, data);
            }

            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if(result!=null) {
                // targetView.setImageBitmap(result);
                Uri yourUri = Uri.fromFile(upload_file);
              /*  targetView.setImageResource(0);
                targetView.setImageURI(yourUri);*/
                adapter.sendImage(yourUri,ivCameraIndex);
                adapter.notifyDataSetChanged();

                //Log.e(TAG, "onActivityResult: " + pa.getPath());
            }else {
                Toast.makeText(mContext, "Image saving problem", Toast.LENGTH_SHORT).show();
            }

            if(pd.isShowing()) {
                pd.dismiss();
            }
        }

        @Override
        protected void onPreExecute() {
            pd.show();
        }
    }

    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getActivity().getPackageManager();

        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getActivity().getExternalFilesDir("");
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "profile.png"));
        }
        return outputFileUri;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


            if (requestCode == IMAGE_RESULT_ONE) {

                String filePath = getImageFilePath(data);
                if (filePath != null) {
                    Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
                    squareImageView.setImageBitmap(selectedImage);
                    upload_file_me_one = new File(getImageFromFilePath(data));
                }


            }
            else if(requestCode == IMAGE_RESULT_TWO){
                String filePath = getImageFilePath(data);
                if (filePath != null) {
                    Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
                    squareImageViewOne.setImageBitmap(selectedImage);
                    upload_file_me_two = new File(getImageFromFilePath(data));
                }
            }
            else if(requestCode == IMAGE_RESULT_THREE){
                String filePath = getImageFilePath(data);
                if (filePath != null) {
                    Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
                    squareImageViewTwo.setImageBitmap(selectedImage);
                    upload_file_me_three = new File(getImageFromFilePath(data));
                }
            }
            else if(requestCode == IMAGE_RESULT_FOUR){
                String filePath = getImageFilePath(data);
                if (filePath != null) {
                    Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
                    squareImageViewThree.setImageBitmap(selectedImage);
                    upload_file_me_four = new File(getImageFromFilePath(data));
                }
            }
            else if(requestCode == IMAGE_RESULT_FIVE){
                String filePath = getImageFilePath(data);
                if (filePath != null) {
                    Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
                    squareImageViewFour.setImageBitmap(selectedImage);
                    upload_file_me_five = new File(getImageFromFilePath(data));
                }
            }





    }

    private String getImageFromFilePath(Intent data) {
        boolean isCamera = data == null || data.getData() == null;

        if (isCamera) return getCaptureImageOutputUri().getPath();
        else return getPathFromURI(data.getData());

    }

    public String getImageFilePath(Intent data) {
        return getImageFromFilePath(data);
    }
    private String getPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable("pic_uri", picUri);
    }


    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }
    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (getActivity().checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }


  public ImagePickerObject saveImagePicker()
  {
      int count =0;
      if(squareImageView.getDrawable()!=null) {
          BitmapDrawable drawable_one = (BitmapDrawable) squareImageView.getDrawable();
          Bitmap bitmap_One = drawable_one.getBitmap();
          upload_file_me_one = persistImage(bitmap_One, "one");

     }

      if(squareImageViewOne.getDrawable()!=null)
      {
          BitmapDrawable drawable_two = (BitmapDrawable) squareImageViewOne.getDrawable();
          Bitmap bitmap_second = drawable_two.getBitmap();
          upload_file_me_two=persistImage(bitmap_second,"two");
          Log.e("ImageOne",upload_file_me_two.toString());
      }

      if(squareImageViewTwo.getDrawable()!=null)
      {
          BitmapDrawable drawable_three = (BitmapDrawable) squareImageViewTwo.getDrawable();
          Bitmap bitmap_three = drawable_three.getBitmap();
          upload_file_me_three=persistImage(bitmap_three,"three");
      }

      if(squareImageViewThree.getDrawable()!=null)
      {
          BitmapDrawable drawable_four = (BitmapDrawable) squareImageViewThree.getDrawable();
          Bitmap bitmap_four = drawable_four.getBitmap();
          upload_file_me_four=persistImage(bitmap_four,"four");
      }

      if(squareImageViewFour.getDrawable()!=null)
      {
          BitmapDrawable drawable_five = (BitmapDrawable) squareImageViewFour.getDrawable();
          Bitmap bitmap_five = drawable_five.getBitmap();
          upload_file_me_five=persistImage(bitmap_five,"five");
      }

//      if (squareImageView.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.image_placeholder).getConstantState())&&squareImageViewOne.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.image_placeholder).getConstantState())&&squareImageViewTwo.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.image_placeholder).getConstantState())&&squareImageViewFour.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.image_placeholder).getConstantState())&&squareImageViewThree.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.image_placeholder).getConstantState()))
//      {
//          count=1;
//      }
   //   Log.e("Image",squareImageView.get+"-"+getResources().getDrawable(R.drawable.image_placeholder));
//      if (squareImageView.getDrawable().equals(getResources().getDrawable(R.drawable.image_placeholder))&&squareImageViewOne.getDrawable().equals(getResources().getDrawable(R.drawable.image_placeholder))&&squareImageViewTwo.getDrawable().equals(getResources().getDrawable(R.drawable.image_placeholder))&&squareImageViewFour.getDrawable().equals(getResources().getDrawable(R.drawable.image_placeholder))&&squareImageViewThree.getDrawable().equals(getResources().getDrawable(R.drawable.image_placeholder)))
//      {
//
//          count=1;
//      }
//      else
//      {
//          count=0;
//      }
//      Toast.makeText(getActivity(),count+"",Toast.LENGTH_SHORT).show();

      ImagePickerObject imagePickerObject=new ImagePickerObject(upload_file_me_one,upload_file_me_two,upload_file_me_three,upload_file_me_four,upload_file_me_five,isSelected,count+"");
     return imagePickerObject;
  }



    private  File persistImage(Bitmap bitmap, String name) {

        File filesDir = getActivity().getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");

//Convert bitmap to byte array
        Bitmap bitmaps = bitmap;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmaps.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imageFile);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageFile;
    }
}
