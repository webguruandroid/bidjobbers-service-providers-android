package com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessInfo;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;
import com.bidjobbers.bidjobberspro.utils.Validation.BusinessInfoResult;

import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BusinessInfoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BusinessInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BusinessInfoFragment extends BaseFragment implements BusinessInfoFragmentMvpView {

   // private OnFragmentInteractionListener mListener;
    @BindView(R.id.tv_dob)
    LinearLayout tv_dob;
    @BindView(R.id.input_yearoffoundation)
    TextView input_yearoffoundation ;
    @BindView(R.id.input_com_name)
    EditText input_com_name;
    @BindView(R.id.edt_no_of_emp)
    Spinner edt_no_of_emp;
    @BindView(R.id.edt_reg_no)
    EditText edt_reg_no;

    //input_houseAddress input_com_name edt_no_of_emp edt_reg_no

    Context mContext;
    LinearLayoutManager layoutManager ;
    Calendar calendarCurrent;
    int year, month, day;
    boolean isDataSet = false;
    boolean datePickerDialog_visible = false;
    ArrayList<String> spinnerArray = new ArrayList<String>();
    ArrayList<String>spinnerArrayId=new ArrayList<>();
    String spinnerSelectedId;
    int Position;

    @Inject
    BusinessInfoFragmentPresenter<BusinessInfoFragmentMvpView> businessInfoFragmentPresenter;

    public BusinessInfoFragment() {

    }


    // TODO: Rename and change types and number of parameters
    public static BusinessInfoFragment newInstance() {
        BusinessInfoFragment fragment = new BusinessInfoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
           /* mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);*/
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            String str = bundle.getString("FROMG");
            Log.e("FROMG", str);

        }
    }

    public void showDatePickerDialog() {
        int year, month, day;
        isDataSet = false;
        calendarCurrent = Calendar.getInstance();
        month = calendarCurrent.get(Calendar.MONTH);
        day =   calendarCurrent.get(Calendar.DAY_OF_MONTH);
        year =  calendarCurrent.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), myDateListener, year, month, day );
        datePickerDialog.setOnDismissListener(mOnDismissListener);
       // datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
        datePickerDialog_visible=true;  //indicate dialog is up
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {

                    showDate(arg1, arg2+1, arg3);
                }
            };

    private DialogInterface.OnDismissListener mOnDismissListener =
            new DialogInterface.OnDismissListener() {
                public void onDismiss(DialogInterface dialog) {
                    datePickerDialog_visible=false;  //indicate dialog is cancelled/gone
                    if (isDataSet) {  // [IF date was picked

                    } else {     input_yearoffoundation.setText("Year Founded");

                    }
                }
            };

    void showDate(int year, int month, int day) {
        input_yearoffoundation.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));
        isDataSet = true;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v= inflater.inflate(R.layout.fragment_business_info, container, false);
        setUnBinder(ButterKnife.bind(this,v));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }
        businessInfoFragmentPresenter.onAttach(this);
        businessInfoFragmentPresenter.onGetDataFromApi();
        getEmpId();

        return v;
    }


    public void getEmpId()
    {
        edt_no_of_emp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Position=position;
                switch (Position)
                {
                    case 0:spinnerSelectedId=spinnerArrayId.get(0);
                        break;
                    case 1:spinnerSelectedId=spinnerArrayId.get(1);
                        break;
                    case 2:spinnerSelectedId=spinnerArrayId.get(2);
                        break;
                    case 3:
                        spinnerSelectedId=spinnerArrayId.get(3);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        /*if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        tv_dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(), "ttt", Toast.LENGTH_SHORT).show();
                showDatePickerDialog();
            }
        });
        input_yearoffoundation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(), "ttt", Toast.LENGTH_SHORT).show();
                showDatePickerDialog();
            }
        });
    }

    @Override
    public void setDataFromApi(DataModelForFirstTimeQusAns.BusinessInfo businessInfo) {


        input_com_name.setText(businessInfo.getCompanyName());
        input_yearoffoundation.setText(businessInfo.getYearFounded());
        spinnerArray = new ArrayList<String>();
        spinnerArrayId=new ArrayList<>();

        spinnerSelectedId=businessInfo.getNoOfEmployeeId();
     //   Toast.makeText(getActivity(),spinnerSelectedId+"",Toast.LENGTH_SHORT).show();

        spinnerArray.add(getString(R.string.noofemp));
        spinnerArray.add(businessInfo.getNOEOneName());
        spinnerArray.add(businessInfo.getNOETwoName());
        spinnerArray.add(businessInfo.getNOEThreeName());

        spinnerArrayId.add("");
        spinnerArrayId.add(businessInfo.getNOEOneId());
        spinnerArrayId.add(businessInfo.getNOETwoId());
        spinnerArrayId.add(businessInfo.getNOEThreeId());


        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_spinner_item,
                        spinnerArray); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        edt_no_of_emp.setAdapter(spinnerArrayAdapter);

        for(int i=0;i<spinnerArrayId.size();i++)
        {
            if(businessInfo.getNoOfEmployeeId().equals(spinnerArrayId.get(i)))
            {
                edt_no_of_emp.setSelection(i);
            }
        }

        edt_reg_no.setText(businessInfo.getCompRegisterNumber());
    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public BusinessInfoResult getBusinessInfo(){

        String input_houseAddress_data="", input_com_name_data="", edt_no_of_emp_data="", edt_reg_no_data="";
        boolean is_error=false;
        String  err_text="";

        input_houseAddress_data =  input_yearoffoundation.getText().toString().trim();
        input_com_name_data =  input_com_name.getText().toString().trim();
        edt_no_of_emp_data = "";
        edt_reg_no_data =  edt_reg_no.getText().toString().trim();

        if(input_houseAddress_data.trim().isEmpty()||input_houseAddress_data.trim().equals("Year Founded"))
        {
            is_error = true;err_text="Enter year of foundation";
        }
        if(input_com_name_data.trim().isEmpty())
        {
            is_error = true;err_text="Enter company Name";
        }
//        if(edt_no_of_emp_data.trim().isEmpty())
//        {
//            is_error = true;err_text="Enter number of employee";
//        }
        if(edt_reg_no_data.trim().isEmpty())
        {
            is_error = true;err_text="Enter registration no";
        }

        BusinessInfoResult businessInfoResult = new BusinessInfoResult( is_error,  err_text,
                input_com_name_data,  edt_no_of_emp_data,  edt_reg_no_data,  input_houseAddress_data);

        return businessInfoResult;

    }

    public BusinessInfoObject setDataBusinessInfo()
    {

        Log.e("Business",edt_reg_no.getText().toString().trim());
        BusinessInfoObject object=new BusinessInfoObject(input_com_name.getText().toString().trim(),input_yearoffoundation.getText().toString().trim(),spinnerSelectedId,edt_reg_no.getText().toString().trim());
        return object;

    }



}
