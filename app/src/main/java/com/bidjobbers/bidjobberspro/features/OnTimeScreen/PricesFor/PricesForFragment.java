package com.bidjobbers.bidjobberspro.features.OnTimeScreen.PricesFor;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PricesForFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PricesForFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PricesForFragment extends BaseFragment implements PricesForFragmentMvpView {

    private OnFragmentInteractionListener mListener;

    @BindView(R.id.ll_lawn_mow_a_lawn1)
    LinearLayout ll_lawn_mow_a_lawn1;
    @BindView(R.id.ll_lawn_mow_a_lawn2)
    LinearLayout ll_lawn_mow_a_lawn2;
    @BindView(R.id.ll_lawn_mow_a_lawn3)
    LinearLayout ll_lawn_mow_a_lawn3;
    @BindView(R.id.ll_lawn_mow_a_lawn4)
    LinearLayout ll_lawn_mow_a_lawn4;
    @BindView(R.id.etSmall)
    EditText etSmall ;
    @BindView(R.id.etMedium)
    EditText etMedium;
    @BindView(R.id.etLarge)
    EditText etLarge;
    @BindView(R.id.etVLarge)
    EditText etVLarge;
    @BindView(R.id.ivSmall)
    ImageView ivSmall;
    @BindView(R.id.tvSmall)
    TextView tvSmall;
    @BindView(R.id.tvSmallDesc)
    TextView tvSmallDesc;
    @BindView(R.id.ivMedium)
    ImageView ivMedium;
    @BindView(R.id.tvMedium)
    TextView tvMedium;
    @BindView(R.id.tvMediumDesc)
    TextView tvMediumDesc;
    @BindView(R.id.ivLarge)
    ImageView ivLarge;
    @BindView(R.id.tvLarge)
    TextView tvLarge;
    @BindView(R.id.tvLargeDesc)
    TextView tvLargeDesc;
    @BindView(R.id.ivVLarge)
    ImageView ivVLarge;
    @BindView(R.id.tvVLarge)
    TextView tvVLarge;
    @BindView(R.id.tvVLargeDesc)
    TextView tvVLargeDesc;
    @BindView(R.id.ivUnsure)
    ImageView ivUnsure;
    @BindView(R.id.tvUnsure)
    TextView tvUnsure;
    @BindView(R.id.tvUnsureDesc)
    TextView tvUnsureDesc;
    @BindView(R.id.tv_heres_why)
    TextView tv_heres_why;

    String smallId,mediumId,largeId,vlargeId,unsureId;
    Dialog dialog;
    public ArrayList<PriceForObject.PriceBean> arrayList=new ArrayList<>();


    @Inject
    PricesForFragmentPresenter<PricesForFragmentMvpView> pricesForFragmentPresenter;


    public PricesForFragment() {
        // Required empty public constructor
    }

    public static PricesForFragment newInstance() {
        PricesForFragment fragment = new PricesForFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            String str = bundle.getString("FROMG");
            Log.e("FROMG", str);

        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



        ll_lawn_mow_a_lawn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                etSmall.requestFocus();
                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(etSmall, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        ll_lawn_mow_a_lawn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etMedium.requestFocus();
                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(etMedium, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        ll_lawn_mow_a_lawn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etLarge.requestFocus();
                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(etLarge, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        ll_lawn_mow_a_lawn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etVLarge.requestFocus();
                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(etVLarge, InputMethodManager.SHOW_IMPLICIT);
            }
        });


      /*  etSmall.setText("$");
        Selection.setSelection(etSmall.getText(), etSmall.getText().length());
        etSmall.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("$")){
                    etSmall.setText("$");
                    Selection.setSelection(etSmall.getText(), etSmall.getText().length());
                }

            }
        });


        etMedium.setText("$");
        Selection.setSelection(etMedium.getText(), etMedium.getText().length());
        etMedium.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("$")){
                    etMedium.setText("$");
                    Selection.setSelection(etMedium.getText(), etMedium.getText().length());
                }

            }
        });


        etLarge.setText("$");
        Selection.setSelection(etLarge.getText(), etLarge.getText().length());
        etLarge.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("$")){
                    etLarge.setText("$");
                    Selection.setSelection(etLarge.getText(), etLarge.getText().length());
                }

            }
        });

        etVLarge.setText("$");
        Selection.setSelection(etVLarge.getText(), etVLarge.getText().length());
        etVLarge.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("$")){
                    etVLarge.setText("$");
                    Selection.setSelection(etVLarge.getText(), etVLarge.getText().length());
                }

            }
        });*/


        tv_heres_why.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPriceDialog();
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_your_price, container, false);
        setUnBinder(ButterKnife.bind(this,v));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }
        pricesForFragmentPresenter.onAttach(this);
        pricesForFragmentPresenter.onGetDataFromApi();
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public void setDataFromApi(DataModelForFirstTimeQusAns.BusinessPriceLawnSerce businessPriceLawnSerce) {


        Log.e("Price",businessPriceLawnSerce.toString());
        Glide.with(getActivity())
                .load(businessPriceLawnSerce.getLsmallImage())
                // .placeholder(R.drawable.ic_launcher_background)
                .into(ivSmall);
        tvSmall.setText(businessPriceLawnSerce.getLsmallName());
        tvSmallDesc.setText(businessPriceLawnSerce.getLsmallDescription());
        smallId=businessPriceLawnSerce.getLsmallId();
        Log.e("Price",businessPriceLawnSerce.getLsmaillPrice());
        if(!businessPriceLawnSerce.getLsmaillPrice().equals("")) {
            etSmall.setText(businessPriceLawnSerce.getLsmaillPrice().substring(1));
        }
      //  etSmall.append(businessPriceLawnSerce.getLsmaillPrice());

        Glide.with(getActivity())
                .load(businessPriceLawnSerce.getLmedImage())
                // .placeholder(R.drawable.ic_launcher_background)
                .into(ivMedium);
        tvMedium.setText(businessPriceLawnSerce.getLmedName());
        tvMediumDesc.setText(businessPriceLawnSerce.getLmedDescription());
        mediumId=businessPriceLawnSerce.getLmedId();
        Log.e("Price",businessPriceLawnSerce.getLmedPrice());

        if(!businessPriceLawnSerce.getLmedPrice().equals("")) {
            etMedium.setText(businessPriceLawnSerce.getLmedPrice().substring(1));
        }



        Glide.with(getActivity())
                .load(businessPriceLawnSerce.getLlargeImage())
                // .placeholder(R.drawable.ic_launcher_background)
                .into(ivLarge);
        tvLarge.setText(businessPriceLawnSerce.getLlargeName());
        tvLargeDesc.setText(businessPriceLawnSerce.getLlargeDescription());
        largeId=businessPriceLawnSerce.getLlargeId();
        Log.e("Price",businessPriceLawnSerce.getLlargePrice());

        if(!businessPriceLawnSerce.getLlargePrice().equals("")) {
            etLarge.setText(businessPriceLawnSerce.getLlargePrice().substring(1));
        }


        Glide.with(getActivity())
                .load(businessPriceLawnSerce.getLveryLargeImage())
                // .placeholder(R.drawable.ic_launcher_background)
                .into(ivVLarge);
        tvVLarge.setText(businessPriceLawnSerce.getLveryLargeName());
        tvVLargeDesc.setText(businessPriceLawnSerce.getLveryLargeDescription());
        vlargeId=businessPriceLawnSerce.getLvryLargeId();
        Log.e("Price",businessPriceLawnSerce.getLvryLargePrice());
        if(!businessPriceLawnSerce.getLvryLargePrice().equals("")) {
            etVLarge.setText(businessPriceLawnSerce.getLvryLargePrice().substring(1));
        }


        Glide.with(getActivity())
                .load(businessPriceLawnSerce.getLunSureImage())
                // .placeholder(R.drawable.ic_launcher_background)
                .into(ivUnsure);
        tvUnsure.setText(businessPriceLawnSerce.getLunSureName());
        tvUnsureDesc.setText(businessPriceLawnSerce.getLunSureDescription());
        unsureId=businessPriceLawnSerce.getLunSureId();



    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public PriceForObject setDataBusinessInfo()
    {
        arrayList=new ArrayList<>();
        PriceForObject.PriceBean bean=new PriceForObject.PriceBean(smallId,"$"+etSmall.getText().toString().trim());
        PriceForObject.PriceBean beanOne=new PriceForObject.PriceBean(mediumId,"$"+etMedium.getText().toString().trim());
        PriceForObject.PriceBean beanTwo=new PriceForObject.PriceBean(largeId,"$"+etLarge.getText().toString().trim());
        PriceForObject.PriceBean beanThree=new PriceForObject.PriceBean(vlargeId,"$"+etVLarge.getText().toString().trim());
        PriceForObject.PriceBean beanFour =new PriceForObject.PriceBean(unsureId,"0");

      //  PriceForObject.PriceBean bean=new PriceForObject.PriceBean(smallId,"0");
     //   PriceForObject.PriceBean beanOne=new PriceForObject.PriceBean(mediumId,"0");
     //   PriceForObject.PriceBean beanTwo=new PriceForObject.PriceBean(largeId,"0");
     //   PriceForObject.PriceBean beanThree=new PriceForObject.PriceBean(vlargeId,"0");
     //   PriceForObject.PriceBean beanFour =new PriceForObject.PriceBean(unsureId,"0");




        arrayList.add(bean);
        arrayList.add(beanOne);
        arrayList.add(beanTwo);
        arrayList.add(beanThree);
        arrayList.add(beanFour);
        Log.e("Pricefor",""+arrayList.get(0)+arrayList.get(1)+arrayList.get(2)+arrayList.get(3)+arrayList.get(4));

        PriceForObject object=new PriceForObject(arrayList);
        return object;

    }

    private void openPriceDialog()
    {
        dialog = new Dialog(getActivity(), R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.layout_price_dialog);
        TextView txt=dialog.findViewById(R.id.txt);
        Button btn_ok=dialog.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txt.setText(Html.fromHtml("<ul>\n" +
                    "  <li>"+getResources().getString(R.string.price_two)+"</li>\n" +
                    "  <li>"+getResources().getString(R.string.price_three)+"</li>\n" +
                    "  <li>"+getResources().getString(R.string.price_four)+"</li>\n" +
                    "  <li>"+getResources().getString(R.string.price_five)+"</li>\n" +
                    "</ul>", Html.FROM_HTML_MODE_COMPACT));
        } else {
            txt.setText(Html.fromHtml("<ul>\n" +
                    "  <li>"+getResources().getString(R.string.price_two)+"</li>\n" +
                    "  <li>"+getResources().getString(R.string.price_three)+"</li>\n" +
                    "  <li>"+getResources().getString(R.string.price_four)+"</li>\n" +
                    "  <li>"+getResources().getString(R.string.price_five)+"</li>\n" +
                    "</ul>"));
        }


        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.show();
    }


}
