package com.bidjobbers.bidjobberspro.features.ForgotOtpVeryfy;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.resentotp.ResendOtpRequest;
import com.bidjobbers.bidjobberspro.data.network.models.resentotp.ResendOtpResponse;
import com.bidjobbers.bidjobberspro.data.network.models.verifyotp.VerifyOtpRequest;
import com.bidjobbers.bidjobberspro.data.network.models.verifyotp.VerifyOtpResponse;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.CommonUtils;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class VerifyOtpPresenter <V extends VerifyOtpMvpView> extends BasePresenter<V> implements VerifyOtpMvpPresenter<V> {

    @Inject
    public VerifyOtpPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void actionResentOtp(String email)
    {
        if(email.isEmpty())
        {
            if(getDataManager().getLocalValue().equals("en")) {
                getMvpView().showMessage("Enter Email Address");
            }
            else
            {
                getMvpView().showMessage("Entrer l'adresse e-mail");
            }
            return;
        }
        if(!CommonUtils.isEmailValid(email.toString()))
        {
            if(getDataManager().getLocalValue().equals("en")) {
                getMvpView().showMessage("Enter Valid Email Address");
            }
             else
                {
                    getMvpView().showMessage("Entrez une adresse e-mail valide");
                }
            return;
        }

        ResendOtpRequest resendOtpRequest = new ResendOtpRequest(email.trim(),getDataManager().getLocalValue(),"RP");
        getCompositeDisposable().add(
                getDataManager().getSentOtp(resendOtpRequest)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<ResendOtpResponse>()
                        {
                            @Override
                            public void onSuccess(ResendOtpResponse testResponse)
                            {
                                if(testResponse.getResponseCode()==1)
                                {
                                    //getMvpView().onOtpValid(false, testResponse.getResponseText(),getDataManager().getAccess_token());
                                    getMvpView().showMessage(testResponse.getResponseText());
                                }
                                else
                                {
                                    //getMvpView().onOtpValid(true, testResponse.getResponseText(),getDataManager().getAccess_token());
                                    getMvpView().showMessage(testResponse.getResponseText());
                                }
                            }
                            @Override
                            public void onError(Throwable e)
                            {
                                Log.e("ERROR", "onError: "+e.getMessage());
                            }

                        }));

    }
    @Override
    public void onGetStarted(String emailId, String otp) {

     //Api for


        VerifyOtpRequest verifyOtpRequest = new VerifyOtpRequest(otp,emailId,getDataManager().getLocalValue(),"RP");

        getCompositeDisposable().add(
                getDataManager().getVeryResponse(verifyOtpRequest)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<VerifyOtpResponse>()
                        {
                            @Override
                            public void onSuccess(VerifyOtpResponse testResponse)
                            {
                                if(testResponse.getResponseCode()==1)
                                {
                                    //getMvpView().onOtpValid(false, testResponse.getResponseText(),getDataManager().getAccess_token());
                                    getMvpView().showMessage(testResponse.getResponseText());
                                    getMvpView().actionGetStarted();

                                }
                                else
                                {
                                    //getMvpView().onOtpValid(true, testResponse.getResponseText(),getDataManager().getAccess_token());
                                    getMvpView().showMessage(testResponse.getResponseText());
                                }
                            }
                            @Override
                            public void onError(Throwable e)
                            {
                                Log.e("ERROR", "onError: "+e.getMessage());
                            }

                        }));



    }
}
