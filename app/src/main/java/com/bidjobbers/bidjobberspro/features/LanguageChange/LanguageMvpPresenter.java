package com.bidjobbers.bidjobberspro.features.LanguageChange;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface LanguageMvpPresenter  <V extends LanguageMvpView> extends MvpPresenter<V> {

    void getSetLocal(String local);
}
