package com.bidjobbers.bidjobberspro.features.dashbord.OnlyActivty;

import com.bidjobbers.bidjobberspro.data.network.models.DeviceToken.DeviceTokenResponse;
import com.bidjobbers.bidjobberspro.data.network.models.NotificationCount.NotificationCountResponse;
import com.bidjobbers.bidjobberspro.data.network.models.SetLocal.SetLocalResponse;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface DashbordMvpView extends MvpView {

    //public void  allTasksName(ArrayList<TabBean> arrayList);
    void onDeactivate(String data);
    void successfullySEtLocal(SetLocalResponse setLocalResponse);
    void successfullyGetProfile(getProfileResponse getProfileResponse);
    void successfullyLogout();
    void onSuccessfullyGetNotificationCount(NotificationCountResponse notificationCountResponse);
    void onSuccessfullyUpdateToken(DeviceTokenResponse response);
}
