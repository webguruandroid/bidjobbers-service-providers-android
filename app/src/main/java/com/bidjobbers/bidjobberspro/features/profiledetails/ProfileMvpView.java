package com.bidjobbers.bidjobberspro.features.profiledetails;

import com.bidjobbers.bidjobberspro.data.network.models.Checkpassword.CheckpasswordResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ReviewList.ReviewResponse;
import com.bidjobbers.bidjobberspro.data.network.models.UpdateProfile.UpdateProfileResponse;
import com.bidjobbers.bidjobberspro.data.network.models.servicedetails.ServiceDetailsResponse;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface ProfileMvpView extends MvpView {

/*    public void displayDialog();
    public void cloaseDialog();

    public void displayDialogTypeAddress();
    public void closeDialogTypeAddress();

    public void displayDialogWhereDataTake();
    public void closeDialogWhereDataTake();*/




    void onDeactivate(String data);
    void successfullyGetServiceDetails(ServiceDetailsResponse serviceDetailsResponse);
    void successfullySubmitPassword(CheckpasswordResponse checkpasswordResponse);

    public void displayAllReviews();
    public void backToPriviousScreen();
    public void btnSubmitAction();

    public void editSocial();
    public void editProf();
    public void editTypeOfPropertyWorkFor();
    public void editImageGalaryFragment();
    public void editCngTree();
    public void editOpenKindOfPropertyWorkOn();
    public void editOpenImageFragment();
    public void editBusinessInfo();
    public void editOpenBusinessLocation();
    public void editBusinessIntroDetails();
    public void editBusinessHours();
    public void editAdvanceBookingTime();


    public void loadTakeImageAdapter();
    public void loadReviewAdapter();

    public void allDialogLoad();
    public void allTagLoad();


    //API

    void successfullyUpdateProfile(UpdateProfileResponse updateProfileResponse);
    void successfullySaveKindOfPropertyData();
    void successfullySaveSocialLink();
    void successfullySaveBusinessInfo();
    void successfullySaveBusinessLocation();
    void successfullySavaData();
    void successfullySaveBusinessHour();
    void successfullySaveAdvanceBooking();
    void successfullyySavePropertyType();
    void successfullySavePriceForLawn();
    void successfullySavePriceForTree();
    void successfullySaveImage();

    void successfullyGetReview(ReviewResponse reviewResponse);


}
