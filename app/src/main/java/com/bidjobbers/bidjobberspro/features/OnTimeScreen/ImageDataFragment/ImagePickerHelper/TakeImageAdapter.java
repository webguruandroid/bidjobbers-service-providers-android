package com.bidjobbers.bidjobberspro.features.OnTimeScreen.ImageDataFragment.ImagePickerHelper;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;
import com.theophrast.ui.widget.SquareImageView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TakeImageAdapter extends RecyclerView.Adapter<TakeImageAdapter.ViewHolder>  {
    ArrayList<String> a;
    Context context;
    CellListener cellListener;
    List<PlaceWorkModel> picturesList;
    Uri photo;
    int pos=77;



    public TakeImageAdapter(ArrayList<String> itemsData, Context context) {
        this.a = itemsData;
        this.context = context;
    }

    public void setAdapterListener(CellListener mListener) {
        this.cellListener = mListener;
    }

    public void sendImage(Uri photo, int pos) {
        this.photo = photo;
        this.pos=pos;
    }


    public interface CellListener {
        public void onItemClick(int pos, ArrayList<String> items);
    }

    public void updateList(List<PlaceWorkModel> picturesList) {
        this.picturesList = picturesList;
        notifyDataSetChanged();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TakeImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_take_image, null);

        // create ViewHolder
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

            if(photo!=null){
                if(pos!=77 && position==pos )
                {

                   if(photo!=null){
                        Bitmap bitmap = null;
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), photo);
                            viewHolder.squareImageView.setImageBitmap(bitmap);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                }

            }
    }


    // Return the size of your itemsData (invoked by the layout manager)
    @Override
    public int getItemCount() {

        return a.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        SquareImageView squareImageView;
        public ImageView iv_product;
        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            squareImageView=(SquareImageView)itemLayoutView.findViewById(R.id.squareImageView);

            itemLayoutView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cellListener.onItemClick(getAdapterPosition(),a);
                }
            });
        }
    }
}