package com.bidjobbers.bidjobberspro.features.resetpasswordfromsettings;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.ChangeaPassword.ChangePasswordRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ChangeaPassword.ChangePasswordResponse;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ResetPasswordPresenter <V extends ResetPasswordMvpView> extends BasePresenter<V> implements ResetPasswordMvpPresenter<V> {


    @Inject
    public ResetPasswordPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void onChagnePassword(String oldpassword, String newpassword,String confirmPassword) {


        if(oldpassword.equals(""))
        {
            getMvpView().onError("Please Enter Oldpassword");
            return;
        }
        else if(newpassword.equals(""))
        {
            getMvpView().onError("Please Enter New password");
            return;
        }

        else if(confirmPassword.equals(""))
        {
            getMvpView().onError("Please Enter ConfirmPassword");
            return;
        }
        else if(oldpassword.equals(newpassword))
        {
            getMvpView().onError("Old and new password must be different");
            return;
        }
        else if(!confirmPassword.equals(newpassword))
        {
            getMvpView().onError("password not matched");
            return;
        }

        getMvpView().showLoading();

        ChangePasswordRequest introBusinessDetailsRequest=new ChangePasswordRequest(oldpassword,newpassword, getDataManager().getLocalValue());
        getCompositeDisposable().add(getDataManager().postChangePassword("Bearer "+getDataManager().getAccess_token(),"application/json",introBusinessDetailsRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ChangePasswordResponse>() {
                    @Override
                    public void onSuccess(ChangePasswordResponse introBusinessDetailsResponse) {

                        getMvpView().hideLoading();

                    getMvpView().onError(introBusinessDetailsResponse.getResponseText());

                    getMvpView().successfullChangePassword(introBusinessDetailsResponse);


                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));
    }
}
