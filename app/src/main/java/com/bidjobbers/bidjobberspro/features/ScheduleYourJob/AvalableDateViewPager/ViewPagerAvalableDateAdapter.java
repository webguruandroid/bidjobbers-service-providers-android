package com.bidjobbers.bidjobberspro.features.ScheduleYourJob.AvalableDateViewPager;

import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.bidjobbers.bidjobberspro.features.ScheduleYourJob.AvalableDateViewPagerHelper.AvalableDateBodyFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.TabBean;

import java.util.ArrayList;

public class ViewPagerAvalableDateAdapter extends FragmentStatePagerAdapter {


    ArrayList<TabBean> tabsArray= new ArrayList<>();

    public ViewPagerAvalableDateAdapter(FragmentManager fm, ArrayList<TabBean> tabsArray)
    {
        super(fm);
        this.tabsArray = tabsArray;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;

        switch (i) {

            case 0:

            fragment = new AvalableDateBodyFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("id", tabsArray.get(i).getTabId());
            bundle.putSerializable("name", tabsArray.get(i).getTabName());

            fragment.setArguments(bundle);
            break;
            case 1:
                fragment = new AvalableDateBodyFragment();
                Bundle bundles = new Bundle();
                bundles.putSerializable("id", tabsArray.get(i).getTabId());
                bundles.putSerializable("name", tabsArray.get(i).getTabName());
                fragment.setArguments(bundles);
                break;
            case 2:
                fragment = new AvalableDateBodyFragment();
                Bundle bundleOne = new Bundle();
                bundleOne.putSerializable("id", tabsArray.get(i).getTabId());
                bundleOne.putSerializable("name", tabsArray.get(i).getTabName());
                Log.e("Tabid", tabsArray.get(i).getTabId() + "-" + i + "-" + tabsArray.get(i).getTabName());

                fragment.setArguments(bundleOne);
                break;
            case 3:
                fragment = new AvalableDateBodyFragment();
                Bundle bundleTwo = new Bundle();
                bundleTwo.putSerializable("id", tabsArray.get(i).getTabId());
                bundleTwo.putSerializable("name", tabsArray.get(i).getTabName());
                Log.e("Tabid", tabsArray.get(i).getTabId() + "-" + i + "-" + tabsArray.get(i).getTabName());

                fragment.setArguments(bundleTwo);
                break;
            case 4:
                fragment = new AvalableDateBodyFragment();
                Bundle bundleThree = new Bundle();
                bundleThree.putSerializable("id", tabsArray.get(i).getTabId());
                bundleThree.putSerializable("name", tabsArray.get(i).getTabName());
                Log.e("Tabid", tabsArray.get(i).getTabId() + "-" + i + "-" + tabsArray.get(i).getTabName());

                fragment.setArguments(bundleThree);
                break;
            case 5:
                fragment = new AvalableDateBodyFragment();
                Bundle bundleFour = new Bundle();
                bundleFour.putSerializable("id", tabsArray.get(i).getTabId());
                bundleFour.putSerializable("name", tabsArray.get(i).getTabName());
                Log.e("Tabid", tabsArray.get(i).getTabId() + "-" + i + "-" + tabsArray.get(i).getTabName());

                fragment.setArguments(bundleFour);
                break;
            case 6:
                fragment = new AvalableDateBodyFragment();
                Bundle bundleFive = new Bundle();
                bundleFive.putSerializable("id", tabsArray.get(i).getTabId());
                bundleFive.putSerializable("name", tabsArray.get(i).getTabName());
                Log.e("Tabid", tabsArray.get(i).getTabId() + "-" + i + "-" + tabsArray.get(i).getTabName());

                fragment.setArguments(bundleFive);
                break;
            case 7:
                fragment = new AvalableDateBodyFragment();
                Bundle bundleSix = new Bundle();
                bundleSix.putSerializable("id", tabsArray.get(i).getTabId());
                bundleSix.putSerializable("name", tabsArray.get(i).getTabName());
                Log.e("Tabid", tabsArray.get(i).getTabId() + "-" + i + "-" + tabsArray.get(i).getTabName());
                fragment.setArguments(bundleSix);
                break;
        }



        return fragment;
    }

    @Override
    public int getCount() {
        return  tabsArray.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        title = tabsArray.get(position).getTabNameWithDate();
        return title;
    }


}
