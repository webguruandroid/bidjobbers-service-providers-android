package com.bidjobbers.bidjobberspro.features.dashbord.ui.gallery;

import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList.BlockCustomerListResponse;
import com.bidjobbers.bidjobberspro.data.network.models.UnBlockUser.UnBlockUserResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface BlockCustomerFragmentMvpView extends BaseFragmentMvpView {

    void successfullyGetBlockCustomer(BlockCustomerListResponse blockCustomerListResponse);

    void onSuccessfullyUnBlockUser(UnBlockUserResponse unBlockUserResponse);
}
