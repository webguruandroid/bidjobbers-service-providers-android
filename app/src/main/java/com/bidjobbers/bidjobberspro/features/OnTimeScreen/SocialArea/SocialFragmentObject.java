package com.bidjobbers.bidjobberspro.features.OnTimeScreen.SocialArea;

import java.util.List;

public class SocialFragmentObject {

    List<String> socialList;

    String webUrl;

    public SocialFragmentObject(List<String> socialList, String webUrl) {
        this.socialList = socialList;
        this.webUrl = webUrl;
    }

    public List<String> getSocialList() {
        return socialList;
    }

    public void setSocialList(List<String> socialList) {
        this.socialList = socialList;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }
}
