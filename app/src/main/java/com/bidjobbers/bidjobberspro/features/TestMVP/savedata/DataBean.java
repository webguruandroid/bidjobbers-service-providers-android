package com.bidjobbers.bidjobberspro.features.TestMVP.savedata;

public class DataBean {

    String id="";
    String data="";

    public DataBean(String id, String data) {
        this.id = id;
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
