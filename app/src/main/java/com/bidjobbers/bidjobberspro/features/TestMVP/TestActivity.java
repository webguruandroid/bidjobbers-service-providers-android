package com.bidjobbers.bidjobberspro.features.TestMVP;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.features.TestMVP.savedata.TestPersistData;
import com.bidjobbers.bidjobberspro.features.TestMVP.viewmodel.viewmodelclass.TestViewModelClass;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class TestActivity extends BaseActivity implements TestMvpView {

   /* @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.edt)
    EditText edt;*/

    @Inject
    TestPresenter<TestMvpView> testPresenter;

    TestViewModelClass testViewModelClass;
    static TestPersistData mTestPersistData;
    static int testCount = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       setUnBinder(ButterKnife.bind(this));


        /* getActivityComponent().inject(this);
        testPresenter.onAttach(this);
        setUp();

        if(mTestPersistData != null)
        {
            testPresenter.displayData(mTestPersistData.getDataBeanArrayList().get(0).getData());
            Toast.makeText(this, "Success"+ this.mTestPersistData.getDataBeanArrayList().get(0).getId(), Toast.LENGTH_SHORT).show();
            Log.e(TAG, "onTestResult: "+this.mTestPersistData.toString() );
        }
        */

    }

    protected void setUp()
    {

    }

   /* @OnClick({R.id.btn_submit})
    public void onClick(View v){
        switch (v.getId()){
            case R.id.btn_submit:
                testPresenter.actionTestButtonPress("1");
                break;
        }
    }*/

    @Override
    public void onTestButtonPress(String data) {
        Toast.makeText(this, ""+data, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTestResult(TestPersistData mTestPersistData)
    {

        testCount = 2;
        this.mTestPersistData = mTestPersistData;
        Log.e(TAG, "onTestResult: "+this.mTestPersistData.toString() );
        Toast.makeText(this, "Success"+ this.mTestPersistData.getDataBeanArrayList().get(0).getData(), Toast.LENGTH_SHORT).show();
        Toast.makeText(this, "Success"+ this.mTestPersistData.getDataBeanArrayList().size()+"", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onTestResultFail()
    {
        Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDisplayData(String Name)
    {
        Toast.makeText(this, Name, Toast.LENGTH_SHORT).show();
    }
}
