package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPager;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.Cancelled.CancelledBodyFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.Completed.CompletedBodyFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.ScheduledTask.ScheduledBodyFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.TabBean;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.TaskItemBodyFragment;

import java.util.ArrayList;

public class ViewPagerTaskAdapter extends FragmentStatePagerAdapter {


    ArrayList<TabBean> tabsArray= new ArrayList<>();

    public ViewPagerTaskAdapter(FragmentManager fm, ArrayList<TabBean> tabsArray)
    {
        super(fm);
        this.tabsArray = tabsArray;
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;
        //This will bind all items of page index(i)
      //  fragment = new ScheduledBodyFragment();


        switch (i)
        {
            case 0:
                fragment = new TaskItemBodyFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("id", tabsArray.get(i).getTabId());
                fragment.setArguments(bundle);
                break;
            case 1:
                fragment = new ScheduledBodyFragment();
                Bundle bundleOne = new Bundle();
                bundleOne.putSerializable("id", tabsArray.get(i).getTabId());
                fragment.setArguments(bundleOne);
                break;
            case 2:
                fragment = new CompletedBodyFragment();
                Bundle bundleTwo = new Bundle();
                bundleTwo.putSerializable("id", tabsArray.get(i).getTabId());
                fragment.setArguments(bundleTwo);
                break;
            case 3:
                fragment = new CancelledBodyFragment();
                Bundle bundles = new Bundle();
                bundles.putSerializable("id", tabsArray.get(i).getTabId());
                fragment.setArguments(bundles);
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return  tabsArray.size();
    }


    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        title = tabsArray.get(position).getTabName();
        return title;
    }


}
