package com.bidjobbers.bidjobberspro.features.CustomerDetails;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.BidCreate.BidCreateResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomer.BlockCustomerResponse;
import com.bidjobbers.bidjobberspro.data.network.models.CacelledTask.CancelledTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.CompletedTask.CompletedTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Customer.CustomerDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.EndTask.EndTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ReSchedule.RescheduleResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Recivetask.ReciveTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ScheduleTask.ScheduletaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.StartTask.StartTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.UnBlockUser.UnBlockUserResponse;
import com.bidjobbers.bidjobberspro.features.MapCustomerDetails.MapCustomerDetailsActivity;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.ImageDataFragment.ImagePickerHelper.TakeImageAdapterAC;
import com.bidjobbers.bidjobberspro.features.ScheduleYourJob.OnlyActivity.ScheduleJobActivity;
import com.bidjobbers.bidjobberspro.features.dashbord.Main2Activity;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;
import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import timber.log.Timber;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class CustomerDetailsActivity extends BaseActivity implements CustomerDetailsMvpView,View.OnClickListener {

    @Inject
    DataManager dataManager;
    @Inject
    TakeImageAdapterAC adapter;
    ArrayList<String> a = new ArrayList<String>();
    private final static int ALL_PERMISSIONS_RESULT = 107;
    private final static int IMAGE_RESULT_ONE = 100;
    Context mContext;

    @BindView(R.id.ll_hide_show_reschuedule_area)
    LinearLayout ll_hide_show_reschuedule_area;
    @BindView(R.id.ll_start)
    LinearLayout ll_start;
    @BindView(R.id.ll_end)
    LinearLayout ll_end;
    @BindView(R.id.ll_send_bid)
    LinearLayout ll_send_bid;
    @BindView(R.id.ll_map_open)
    LinearLayout ll_map_open;
    @BindView(R.id.ll_complete_area)
    LinearLayout ll_complete_area;
    @BindView(R.id.ll_cancel_area)
    LinearLayout ll_cancel_area;
    @BindView(R.id.ll_bid_area)
    LinearLayout ll_bid_area;
    @BindView(R.id.ll_reschuedule_area)
    LinearLayout ll_reschuedule_area;

    @BindView(R.id.iv_prc)
    ImageView iv_prc;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvPhoneNumber)
    TextView tvPhoneNumber;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvServiceRequired)
    TextView tvServiceRequired;
    @BindView(R.id.tvServiceSize)
    TextView tvServiceSize;
    @BindView(R.id.tvCustomerPresent)
    TextView tvCustomerPresent;
    @BindView(R.id.tvServiceTime)
    TextView tvServiceTime;
    @BindView(R.id.tvHeader)
    TextView tvHeader;
    @BindView(R.id.tvServiceType)
    TextView tvServiceType;
    @BindView(R.id.tvServiceNeed)
    TextView tvServiceNeed;
    @BindView(R.id.tvServiceAt)
    TextView tvServiceAt;
    @BindView(R.id.llSchedule)
    LinearLayout llSchedule;
    @BindView(R.id.tvSelectedSlot)
    TextView tvSelectedSlot;
    @BindView(R.id.tvBidPrice)
    EditText tvBidPrice;
    @BindView(R.id.tvJobScheduleOn)
    TextView tvJobScheduleOn;
    @BindView(R.id.tvJobAmount)
    TextView tvJobAmount;
    @BindView(R.id.llJobAmount)
    LinearLayout llJobAmount;
    @BindView(R.id.llScheduleOn)
    LinearLayout llScheduleOn;
    @BindView(R.id.ll_site_image)
    LinearLayout ll_site_image;
    @BindView(R.id.tvSiteImage)
    TextView tvSiteImage;
    @BindView(R.id.tvComment)
    TextView tvComment;
    @BindView(R.id.ivblocked)
    ImageView ivblocked;
    @BindView(R.id.ivunblocked)
    FrameLayout ivunblocked;
    @BindView(R.id.llRescheduleJobs)
    LinearLayout llRescheduleJobs;
    @BindView(R.id.rb_reschedule_no)
    RadioButton rb_reschedule_no;
    @BindView(R.id.rb_reschedule_ys)
    RadioButton rb_reschedule_ys;
    @BindView(R.id.etRescheduleComment)
    EditText etRescheduleComment;
    @BindView(R.id.ll_bottom_cart_area)
    RelativeLayout ll_bottom_cart_area;
    private final static int IMAGE_RESULT_TWO = 200;
    private final static int IMAGE_RESULT_THREE = 300;
    private final static int IMAGE_RESULT_FOUR = 400;
    private final static int IMAGE_RESULT_FIVE = 500;
    @Inject
    TakeImageAdapterAC adapterEnd;
    String jobStatus = "", taskId, bidId,Tag="Foreground", customerId;
    @BindView(R.id.tvRescheduleSlot)
    TextView tvRescheduleSlot;
    @BindView(R.id.ll_select_slot)
    LinearLayout ll_select_slot;
    @BindView(R.id.ll_end_area)
    LinearLayout ll_end_area;
    @BindView(R.id.radio_one)
    RadioGroup radio_one;
    @BindView(R.id.rb_yes_photo)
    RadioButton rb_yes_photo;
    @BindView(R.id.rb_no_photo)
     RadioButton rb_no_photo;
    @BindView(R.id.llPhoto)
      LinearLayout llPhoto;
    @BindView(R.id.etEndNote)
    EditText etEndNote;
    @BindView(R.id.squareImageView)
    ImageView squareImageView;
    @BindView(R.id.squareImageViewOne)
    ImageView squareImageViewOne;
    @BindView(R.id.squareImageViewTwo)
    ImageView squareImageViewTwo;
    @BindView(R.id.squareImageViewThree)
    ImageView squareImageViewThree;
    @BindView(R.id.squareImageViewFour)
    ImageView squareImageViewFour;
    @BindView(R.id.ivCrossOne)
    ImageView ivCrossOne;
    @BindView(R.id.ivCrossTwo)
    ImageView ivCrossTwo;
    @BindView(R.id.ivCrossThree)
    ImageView ivCrossThree;
    @BindView(R.id.ivCrossFour)
    ImageView ivCrossFour;
    @BindView(R.id.ivCrossFive)
    ImageView ivCrossFive;
    @BindView(R.id.tvJobScheduleOnText)
    TextView tvJobScheduleOnText;
    @BindView(R.id.tvimgOne)
      TextView tvimgOne;
    @BindView(R.id.ll_payment_status)
    LinearLayout ll_payment_status;
    @BindView(R.id.tvPaymentStatus)
    TextView tvPaymentStatus;


    String ImageOne, ImageTwo, ImageThree, ImageFour, ImageFive;
    String Time, Date, StartDate="", TimeName, DateName,Latitude,Longitude;
    @BindView(R.id.tvimgTwo)
    TextView tvimgTwo;
    @BindView(R.id.tvimgThree)
    TextView tvimgThree;
    @BindView(R.id.tvimgFour)
    TextView tvimgFour;
    @BindView(R.id.tvimgFive)
    TextView tvimgFive;
    @BindView(R.id.rv_end_photo)
    RecyclerView rv_end_photo;
    @BindView(R.id.tvEndNote)
    TextView tvEndNote;
    MultipartBody.Part[] taskImages=new MultipartBody.Part[5];
    ArrayList<String> arrayListPhoto = new ArrayList<String>();
    File upload_file_me_one,upload_file_me_two,upload_file_me_three,upload_file_me_four,upload_file_me_five;
   @BindView(R.id.servicerequestId)
    TextView servicerequestId;
   @BindView(R.id.ll_requested_on)
   LinearLayout ll_requested_on;
   @BindView(R.id.tvRequestedOn)
   TextView tvRequestedOn;
   @BindView(R.id.ll_sh)
   LinearLayout ll_sh;
    @BindView(R.id.iv_paid)
    ImageView iv_paid;
    @BindView(R.id.iv_jobdone)
    ImageView iv_jobdone;

    String currentPage="1";

    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    private ArrayList<String> taskIdSchedule;

    Dialog dialog, dialogWhereDataTake;
    ImageView iv_gal_img;
    @Inject
    CustomerDetailsPresenter<CustomerDetailsMvpView> customerDetailsPresenter;
    private Animation animShow, animHide;
    private File upload_file;

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }


    private void setEditText() {
        //tvBidPrice.setText("$");
        Selection.setSelection(tvBidPrice.getText(), tvBidPrice.getText().length());
        tvBidPrice.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().startsWith("$")) {
                    tvBidPrice.setText("$");
                    Selection.setSelection(tvBidPrice.getText(), tvBidPrice.getText().length());

                }

            }
        });
    }


    private void initAnimation() {
        animShow = AnimationUtils.loadAnimation(this, R.anim.view_show);
        animHide = AnimationUtils.loadAnimation(this, R.anim.view_hide);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_details);

       // ll_map_open = (LinearLayout) findViewById(R.id.ll_map_open);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        mContext = this;
        customerDetailsPresenter.onAttach(this);


        dialog = new Dialog(mContext, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_zoom_in_popup);
        iv_gal_img = dialog.findViewById(R.id.iv_gal_img);

        permissions.add(CAMERA);
        permissions.add(WRITE_EXTERNAL_STORAGE);
        permissions.add(READ_EXTERNAL_STORAGE);
        permissionsToRequest = findUnAskedPermissions(permissions);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (permissionsToRequest.size() > 0)
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }

        LinearLayoutManager layoutManagerMakeOverBrand = new LinearLayoutManager(this);
        layoutManagerMakeOverBrand.setOrientation(LinearLayoutManager.HORIZONTAL);
        rv_end_photo.setLayoutManager(layoutManagerMakeOverBrand);

        rv_end_photo.setAdapter(adapterEnd);

        taskImages[0]=null;
        taskImages[1]=null;
        taskImages[2]=null;
        taskImages[3]=null;
        taskImages[4]=null;

        ll_map_open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                            if(Latitude!=null&&Longitude!=null) {

                                Intent intent = new Intent(CustomerDetailsActivity.this, MapCustomerDetailsActivity.class);
                                intent.putExtra("Latitude", Latitude);
                                intent.putExtra("Longitude", Longitude);
                                startActivity(intent);
                            }

                    }
                }, 500);
            }
        });


        ll_complete_area = (LinearLayout) findViewById(R.id.ll_complete_area);
        ll_cancel_area = (LinearLayout) findViewById(R.id.ll_cancel_area);
        ll_bid_area = (LinearLayout) findViewById(R.id.ll_bid_area);
        ll_reschuedule_area = (LinearLayout) findViewById(R.id.ll_reschuedule_area);

        Intent intent = getIntent();
        jobStatus = intent.getStringExtra("status");
        taskId = intent.getStringExtra("taskId");
        bidId = intent.getStringExtra("bidId");
        Tag=intent.getStringExtra("Tag");

       // Toast.makeText(getApplicationContext(), jobStatus + "-" + taskId, Toast.LENGTH_SHORT).show();

        tvJobScheduleOnText.setText("Job Scheduled On:");

        if (jobStatus.equals("1")) //Received
        {
            customerDetailsPresenter.getAllTasks();
            ll_payment_status.setVisibility(View.GONE);

        } else if (jobStatus.equals("2")) // Scheduled
        {
            customerDetailsPresenter.getScheduleTask();
            ll_payment_status.setVisibility(View.GONE);
          //  Toast.makeText(getApplicationContext(),"-"+taskId,Toast.LENGTH_SHORT).show();

        } else if (jobStatus.equals("3")) // Pending
        {
            customerDetailsPresenter.getScheduleTask();
            ll_payment_status.setVisibility(View.GONE);

        } else if (jobStatus.equals("4")) //Complete
        {
           // customerDetailsPresenter.getCompletedTask();
             customerDetailsPresenter.onGetCustomer(taskId);
            ll_payment_status.setVisibility(View.VISIBLE);

        } else if (jobStatus.equals("6")) //Bid Sent
        {
            customerDetailsPresenter.getAllTasks();
            ll_payment_status.setVisibility(View.GONE);
        } else if (jobStatus.equals("5"))//Cancelled
        {
            customerDetailsPresenter.getCancelledTasks();
            ll_payment_status.setVisibility(View.GONE);
        } else if (jobStatus.equals("7"))//Reschedule
        {
            customerDetailsPresenter.getScheduleTask();
            tvJobScheduleOnText.setText("Job ReScheduled On:");
            ll_payment_status.setVisibility(View.GONE);

        } else if (jobStatus.equals("8"))// In progress
        {
            customerDetailsPresenter.getScheduleTask();
            ll_payment_status.setVisibility(View.GONE);
        }
        else if (jobStatus.equals("9"))//Bid For This Job
        {
            customerDetailsPresenter.onGetCustomer(taskId);
            ll_payment_status.setVisibility(View.GONE);
        }
        else if (jobStatus.equals("10"))//Bid For This Job
        {
            customerDetailsPresenter.onGetCustomer(taskId);
            ll_payment_status.setVisibility(View.GONE);
        }

        if (jobStatus.trim().equals("1")) {
            ll_bid_area.setVisibility(View.VISIBLE);
          //  ll_sh.setVisibility(View.VISIBLE);
            ivblocked.setVisibility(View.GONE);
            ivunblocked.setVisibility(View.GONE);
            ll_start.setVisibility(View.GONE);
        }
        if (jobStatus.trim().equals("9")) {
            ll_bid_area.setVisibility(View.VISIBLE);
           // ll_sh.setVisibility(View.VISIBLE);
            ivblocked.setVisibility(View.GONE);
            ivunblocked.setVisibility(View.GONE);
            ll_start.setVisibility(View.GONE);
        }
        if (jobStatus.trim().equals("10")) {
            ll_bid_area.setVisibility(View.VISIBLE);
          //  ll_sh.setVisibility(View.GONE);
            ivblocked.setVisibility(View.GONE);
            ivunblocked.setVisibility(View.GONE);
            ll_start.setVisibility(View.GONE);
        }
        if (jobStatus.trim().equals("2")) {

            ll_reschuedule_area.setVisibility(View.VISIBLE);
            ivblocked.setVisibility(View.GONE);
            ivunblocked.setVisibility(View.GONE);

        }
        if (jobStatus.trim().equals("7")) {
            ll_reschuedule_area.setAlpha(1.0f);
            ll_start.setEnabled(true);
            ll_bottom_cart_area.setAlpha(0.5f);
            rb_reschedule_ys.setClickable(false);
            rb_reschedule_no.setClickable(false);
            ll_reschuedule_area.setVisibility(View.VISIBLE);
            ivblocked.setVisibility(View.GONE);
            ivunblocked.setVisibility(View.GONE);
        }
        if (jobStatus.trim().equals("3")) {
            ll_reschuedule_area.setAlpha(0.5f);
            ll_start.setEnabled(false);
            rb_reschedule_ys.setClickable(false);
            rb_reschedule_no.setClickable(false);
            ll_reschuedule_area.setVisibility(View.VISIBLE);
            ivblocked.setVisibility(View.GONE);
            ivunblocked.setVisibility(View.GONE);
        }
        if (jobStatus.trim().equals("8")) {
            ll_reschuedule_area.setAlpha(0.5f);
            ll_start.setVisibility(View.GONE);
            ll_end_area.setVisibility(View.VISIBLE);
            ll_end.setVisibility(View.VISIBLE);
            rb_reschedule_ys.setClickable(false);
            rb_reschedule_no.setClickable(false);
            ll_reschuedule_area.setVisibility(View.VISIBLE);
            ivblocked.setVisibility(View.GONE);
            ivunblocked.setVisibility(View.GONE);
        }
        if (jobStatus.trim().equals("4")) {
            ll_complete_area.setVisibility(View.VISIBLE);
            //  ivblocked.setVisibility(View.VISIBLE);
            ll_start.setVisibility(View.GONE);
        }
        if (jobStatus.trim().equals("5")) {
            ll_cancel_area.setVisibility(View.VISIBLE);
            //   ivblocked.setVisibility(View.VISIBLE);
            ll_start.setVisibility(View.GONE);
        }

        if (jobStatus.trim().equals("6")) {
               ll_bid_area.setVisibility(View.VISIBLE);
            ll_start.setVisibility(View.GONE);
            ivblocked.setVisibility(View.GONE);
            ivunblocked.setVisibility(View.GONE);
        }


        ll_send_bid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle(getResources().getString(R.string.confirmation));
                    String [] word=tvSelectedSlot.getText().toString().trim().split(",");
                    builder.setMessage(getResources().getString(R.string.bidprice) + tvBidPrice.getText().toString().trim() +"$"+ "\n" + getResources().getString(R.string.jobscheduleonA) +word[0]+","+word[1]+ "\n" + getResources().getString(R.string.jobscheduleonC)+word[2]+" h" );
                    builder.setCancelable(false);
                    builder.setPositiveButton(getResources().getString(R.string.yesA), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            if (tvBidPrice.getText().toString().trim().equals("")||tvBidPrice.getText().toString().trim().equals("0")) {
                                Toast.makeText(CustomerDetailsActivity.this, getResources().getString(R.string.PleaseEnterPrice), Toast.LENGTH_SHORT).show();

                            } else {
                                customerDetailsPresenter.onPostBid(taskId, Date, Time, tvBidPrice.getText().toString().trim(), StartDate,customerId);
                            }
                        }
                    });
                    builder.setNegativeButton(getResources().getString(R.string.noA), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();
                } catch (Exception e) {
                    Timber.tag(TAG).e(e);
                }


            }
        });

        llRescheduleJobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle(getResources().getString(R.string.PleaseConfirm));
                    String [] word=tvSelectedSlot.getText().toString().trim().split(",");
                    //builder.setMessage(getResources().getString(R.string.bidprice) + tvBidPrice.getText().toString().trim() +"$"+ "\n" + getResources().getString(R.string.jobscheduleonA) +word[0]+","+word[1]+ "\n" + getResources().getString(R.string.jobscheduleonC)+word[2]+" h" );
                    builder.setMessage(getResources().getString(R.string.areyoyreschedule)+ "\n" + getResources().getString(R.string.jobscheduleonA) +word[0]+","+word[1]+ "\n" + getResources().getString(R.string.jobscheduleonC)+word[2]+" h");
                    builder.setCancelable(false);
                    builder.setPositiveButton(getResources().getString(R.string.yesA), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            if (etRescheduleComment.getText().toString().trim().equals("")) {
                                Toast.makeText(CustomerDetailsActivity.this, getResources().getString(R.string.PleaseEnterRemarks), Toast.LENGTH_SHORT).show();

                            }
                            else if(StartDate.equals(""))
                            { Toast.makeText(CustomerDetailsActivity.this, getResources().getString(R.string.PleaseEnterSlot), Toast.LENGTH_SHORT).show();}
                            else {
                                customerDetailsPresenter.onScheduleTask(bidId, taskId, Date, Time, StartDate, etRescheduleComment.getText().toString().trim());
                            }
                        }
                    });
                    builder.setNegativeButton(getResources().getString(R.string.noA), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();
                } catch (Exception e) {
                    Timber.tag(TAG).e(e);
                }

            }
        });


        ll_end = (LinearLayout) findViewById(R.id.ll_end);

        ll_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!checkCanStartTask()) {



                    try {
                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setTitle(getResources().getString(R.string.StartConfirmation));
                        builder.setMessage(getResources().getString(R.string.ReadyToStartJob));
                        builder.setCancelable(false);
                        builder.setPositiveButton(getResources().getString(R.string.yesA), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                customerDetailsPresenter.onStartTask(customerId, taskId, bidId, "en");

                            }
                        });
                        builder.setNegativeButton(getResources().getString(R.string.noA), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } catch (Exception e) {
                        Timber.tag(TAG).e(e);
                    }







                }
                else
                {
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.OneOfYouJobIsOngoing),Toast.LENGTH_LONG).show();
                }
            }
        });

        Log.e("iamgeId",squareImageView.getId()+"");
        Log.e("iamgeId",R.drawable.image_placeholder+"");

        ll_end.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int count =0;

              //  Toast.makeText(getApplicationContext(),squareImageView.getId()+"",Toast.LENGTH_SHORT).show();
                if(tvimgOne.getText().equals("1")) {
                    if (squareImageView.getDrawable() != null) {
                        BitmapDrawable drawable_one = (BitmapDrawable) squareImageView.getDrawable();
                        Bitmap bitmap_One = drawable_one.getBitmap();
                        upload_file_me_one = persistImage(bitmap_One, "one");

                    }
                }
                else
                {
                    upload_file_me_one=null;
                }
                if(tvimgTwo.getText().equals("1")) {
                if(squareImageViewOne.getDrawable()!=null)
                {
                    BitmapDrawable drawable_two = (BitmapDrawable) squareImageViewOne.getDrawable();
                    Bitmap bitmap_second = drawable_two.getBitmap();
                    upload_file_me_two=persistImage(bitmap_second,"two");
                    Log.e("ImageOne",upload_file_me_two.toString());
                }
                }
                else
                {
                    upload_file_me_two=null;
                }
                if(tvimgThree.getText().equals("1")) {
                if(squareImageViewTwo.getDrawable()!=null)
                {
                    BitmapDrawable drawable_three = (BitmapDrawable) squareImageViewTwo.getDrawable();
                    Bitmap bitmap_three = drawable_three.getBitmap();
                    upload_file_me_three=persistImage(bitmap_three,"three");
                }
                }
                else
                {
                    upload_file_me_three=null;
                }
                if(tvimgFour.getText().equals("1")) {
                if(squareImageViewThree.getDrawable()!=null)
                {
                    BitmapDrawable drawable_four = (BitmapDrawable) squareImageViewThree.getDrawable();
                    Bitmap bitmap_four = drawable_four.getBitmap();
                    upload_file_me_four=persistImage(bitmap_four,"four");
                }
               }
                else
              {
                  upload_file_me_four=null;
                 }
                if(tvimgFive.getText().equals("1")) {
                    if (squareImageViewFour.getDrawable() != null) {
                        BitmapDrawable drawable_five = (BitmapDrawable) squareImageViewFour.getDrawable();
                        Bitmap bitmap_five = drawable_five.getBitmap();
                        upload_file_me_five = persistImage(bitmap_five, "five");
                    }
                }
                else
                {
                    upload_file_me_five=null;
                }
                if(rb_yes_photo.isChecked()) {

                    if (upload_file_me_one != null) {
                        RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), upload_file_me_one);
                        taskImages[0] = MultipartBody.Part.createFormData("task_images[0]", upload_file_me_one.getName(), reqFile);
                    }
                    if (upload_file_me_two != null) {
                        RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), upload_file_me_two);
                        taskImages[1] = MultipartBody.Part.createFormData("task_images[1]", upload_file_me_two.getName(), reqFile);
                    }

                    if (upload_file_me_three != null) {
                        RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), upload_file_me_three);
                        taskImages[2] = MultipartBody.Part.createFormData("task_images[2]", upload_file_me_three.getName(), reqFile);
                    }
                    if (upload_file_me_four != null) {
                        RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), upload_file_me_four);
                        taskImages[3] = MultipartBody.Part.createFormData("task_images[3]", upload_file_me_four.getName(), reqFile);
                    }
                    if (upload_file_me_five != null) {
                        RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), upload_file_me_five);
                        taskImages[4] = MultipartBody.Part.createFormData("task_images[4]", upload_file_me_five.getName(), reqFile);
                    }
                }
              //  customerDetailsPresenter.onEndTask(customerId, taskId, bidId, "en",taskImages,etEndNote.getText().toString().trim());



                try {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle(getResources().getString(R.string.EndConfirmation));
                    builder.setMessage(getResources().getString(R.string.IsJobFinished));
                    builder.setCancelable(false);
                    builder.setPositiveButton(getResources().getString(R.string.yesA), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            customerDetailsPresenter.onEndTask(customerId, taskId, bidId, "en",taskImages,etEndNote.getText().toString().trim());

                        }
                    });
                    builder.setNegativeButton(getResources().getString(R.string.noA), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                } catch (Exception e) {
                    Timber.tag(TAG).e(e);
                }




            }
        });


        rb_reschedule_no.setChecked(true);
        rb_reschedule_ys.setChecked(false);
        rb_no_photo.setChecked(true);
        rb_yes_photo.setChecked(false);

        rb_reschedule_no.setOnCheckedChangeListener(new Radio_check());
        rb_reschedule_ys.setOnCheckedChangeListener(new Radio_check());
        rb_no_photo.setOnCheckedChangeListener(new Radio_check_One());
        rb_yes_photo.setOnCheckedChangeListener(new Radio_check_One());


        ll_hide_show_reschuedule_area.setVisibility(View.GONE);
        llPhoto.setVisibility(View.GONE);


        ivblocked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle(getResources().getString(R.string.ConfirmBlock));
                    builder.setMessage(getResources().getString(R.string.Areyousuretoblockthiscustome));
                    builder.setCancelable(false);
                    builder.setPositiveButton(getResources().getString(R.string.yesA), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            customerDetailsPresenter.onBlockUser(customerId);

                        }
                    });
                    builder.setNegativeButton(getResources().getString(R.string.noA), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                } catch (Exception e) {
                    Timber.tag(TAG).e(e);
                }


            }
        });

        ivunblocked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle(getResources().getString(R.string.confirmblock));
                    builder.setMessage(getResources().getString(R.string.unblocktest));
                    builder.setCancelable(false);
                    builder.setPositiveButton(getResources().getString(R.string.yesA), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            customerDetailsPresenter.onUnBlockUser(customerId);

                        }
                    });
                    builder.setNegativeButton(getResources().getString(R.string.noA), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                } catch (Exception e) {
                    Timber.tag(TAG).e(e);
                }


            }
        });

        initAnimation();
        taskIdSchedule=new ArrayList<>();

//        iv_prc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i=new Intent(CustomerDetailsActivity.this, CustomerProfileActivity.class);
//               i.putExtra("status",jobStatus);
//               i.putExtra("taskId",taskId);
//               i.putExtra("bidId",bidId);
//               startActivity(i);
//
//
//            }
//        });

      //  do {
            customerDetailsPresenter.getAllTasksSchedule(currentPage);

      //  }while (!currentPage.equals(""));

    }


    private boolean checkCanStartTask()
    {
        int tagss=0;
        Log.e("DataSize",taskIdSchedule.size()+"");
        for(int i=0;i<taskIdSchedule.size()-1;i++)
        {
            Log.e("DataSize",taskIdSchedule.size()+"");
            if(taskIdSchedule.get(i).equals("8"))
            {
                tagss=1;
            }
        }
        if(tagss==0)
        return false;
        else
            return true;
    }

    @Override
    public void onDeactivate(String data) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(CustomerDetailsActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, 800);
    }

    @Override
    public void allTasksName(List<ReciveTaskResponse.ResponseDataBean.TaskDataBean> arrayList) {


        a = new ArrayList<>();

        for (int i = 0; i < arrayList.size(); i++) {
            ReciveTaskResponse.ResponseDataBean.TaskDataBean taskDataBean = arrayList.get(i);
            if (taskId.equals(taskDataBean.getTask_id())) {
                servicerequestId.setText(taskDataBean.getService_request_id());
                customerId = taskDataBean.getCustomer_id();
                tvName.setText(taskDataBean.getCustomer_name());
                tvPhoneNumber.setVisibility(View.GONE);
                tvPhoneNumber.setText(taskDataBean.getCustomer_phone());
                tvAddress.setText(taskDataBean.getFull_address());
                tvServiceAt.setText(taskDataBean.getService_at());
                tvServiceRequired.setText(taskDataBean.getService_required());
                if (taskDataBean.getService_type_id().equals("1")) {
                    String text=taskDataBean.getServide_needed();
                    if(!taskDataBean.getGrass_size().equals(""))
                    {
                       text=text+", "+ taskDataBean.getGrass_size();
                    }

                    tvServiceNeed.setText(text);

                } else if(taskDataBean.getService_type_id().equals("2")) {
                    String text=taskDataBean.getServide_needed();
                    if(!taskDataBean.getHow_many_tree_remove().equals(""))
                    {
                        text=text+", "+ taskDataBean.getHow_many_tree_remove();
                    }
                    if(!taskDataBean.getHow_many_tree_trim().equals(""))
                    {
                        text=text+", "+ taskDataBean.getHow_many_tree_trim();

                    }
                    Log.e("CustomerDetails",text);
                    tvServiceNeed.setText(text);
                }
                else
                {
                    tvServiceNeed.setText(taskDataBean.getServide_needed()+", "+taskDataBean.getGrass_size());
                }

                tvServiceType.setText(taskDataBean.getService_type());
                tvServiceTime.setText(taskDataBean.getService_timing());
                tvComment.setText(taskDataBean.getOther_info());

                Latitude=taskDataBean.getLat();
                Longitude=taskDataBean.getLng();




                if (taskDataBean.isOn_site_present()) {
                    tvCustomerPresent.setText("Yes");
                } else {
                    tvCustomerPresent.setText("No");
                }

                if (taskDataBean.getService_type_id().equals("1")) {
                    tvServiceSize.setText(taskDataBean.getLawn_size());
                } else if(taskDataBean.getService_type_id().equals("2")) {
                    tvServiceSize.setText(taskDataBean.getGrass_size());
                }
                else
                {
                    tvServiceSize.setText(taskDataBean.getLawn_size());
                }
                Glide.with(getApplication()).load(taskDataBean.getCustomer_profile_image())
                        .into(iv_prc);
                if (taskDataBean.getCustomer_work_image().size() == 0) {
                    tvSiteImage.setVisibility(View.GONE);
                    ll_site_image.setVisibility(View.GONE);
                } else {
                    tvSiteImage.setVisibility(View.VISIBLE);
                    ll_site_image.setVisibility(View.VISIBLE);
                }
                if (taskDataBean.getCustomer_work_image().size() == 1) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    a.add(ImageOne);
                } else if (taskDataBean.getCustomer_work_image().size() == 2) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    ImageTwo = taskDataBean.getCustomer_work_image().get(1);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                } else if (taskDataBean.getCustomer_work_image().size() == 3) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    ImageTwo = taskDataBean.getCustomer_work_image().get(1);
                    ImageThree = taskDataBean.getCustomer_work_image().get(2);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                } else if (taskDataBean.getCustomer_work_image().size() == 4) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    ImageTwo = taskDataBean.getCustomer_work_image().get(1);
                    ImageThree = taskDataBean.getCustomer_work_image().get(2);
                    ImageFour = taskDataBean.getCustomer_work_image().get(3);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                } else if (taskDataBean.getCustomer_work_image().size() == 5) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    ImageTwo = taskDataBean.getCustomer_work_image().get(1);
                    ImageThree = taskDataBean.getCustomer_work_image().get(2);
                    ImageFour = taskDataBean.getCustomer_work_image().get(3);
                    ImageFive = taskDataBean.getCustomer_work_image().get(4);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                    a.add(ImageFive);
                }

               // adapter = new TakeImageAdapterAC(a, this);
                adapter.loadList(a);
                adapter.setAdapterListener(new TakeImageAdapterAC.CellListener() {
                    @Override
                    public void onItemClick(int pos, ArrayList<String> items) {
                        String str = "";
                        if (pos == 0) {
                            str = ImageOne;
                        }
                        if (pos == 1) {
                            str = ImageTwo;
                        }
                        if (pos == 2) {
                            str = ImageThree;
                        }
                        if (pos == 3) {
                            str = ImageFour;
                        }
                        if (pos == 4) {
                            str = ImageFive;
                        }

                        Glide.with(mContext).
                                load(str)
                                //.placeholder(R.drawable.image_placeholder)
                                .into(iv_gal_img);
                        dialog.show();
                    }
                });
                adapter.notifyDataSetChanged();
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_take_image);
                LinearLayoutManager layoutManagerMakeOverBrand = new LinearLayoutManager(this);
                layoutManagerMakeOverBrand.setOrientation(LinearLayoutManager.HORIZONTAL);


                recyclerView.setLayoutManager(layoutManagerMakeOverBrand);
                recyclerView.setAdapter(adapter);
                //  Toast.makeText(getApplicationContext(),taskDataBean.getStatus(),Toast.LENGTH_SHORT).show();

                if (taskDataBean.getStatus_id()==6) {
                    ll_requested_on.setVisibility(View.GONE);
                    llJobAmount.setVisibility(View.GONE);
                    llScheduleOn.setVisibility(View.GONE);
                    tvJobAmount.setText(taskDataBean.getPrice());
                    tvJobScheduleOn.setText(setDate(taskDataBean.getDate())+ "," + taskDataBean.getSlot());
                    tvBidPrice.setFocusable(false);
                    tvBidPrice.setEnabled(false);
                    int length = taskDataBean.getPrice().length();
                    tvBidPrice.setText(taskDataBean.getPrice().substring(1, length));
                    // Toast.makeText(getApplicationContext(),taskDataBean.getPrice().substring(1,length),Toast.LENGTH_SHORT).show();
                    llSchedule.setVisibility(View.GONE);
                    tvSelectedSlot.setVisibility(View.VISIBLE);
                    tvSelectedSlot.setText(  setDate(taskDataBean.getDate())+ "," +taskDataBean.getSlot() );
                    tvSelectedSlot.setEnabled(false);
                    ll_send_bid.setVisibility(View.GONE);
                } else if (taskDataBean.getStatus_id()==1) {
                    ll_requested_on.setVisibility(View.VISIBLE);
                    tvRequestedOn.setText(taskDataBean.getRequestedOn());
                    llJobAmount.setVisibility(View.GONE);
                    llScheduleOn.setVisibility(View.GONE);
                    tvBidPrice.setFocusable(true);
                    int length = taskDataBean.getPrice().length();
                    tvBidPrice.setText(taskDataBean.getPrice().substring(1, length));
                    // Toast.makeText(getApplicationContext(),taskDataBean.getPrice(),Toast.LENGTH_SHORT).show();
                    llSchedule.setVisibility(View.VISIBLE);
                    tvSelectedSlot.setVisibility(View.GONE);
                    tvSelectedSlot.setText("" + "," + "");
                    ll_send_bid.setVisibility(View.VISIBLE);



                }

                else if(taskDataBean.getStatus_id()==10)
                {
                    ll_requested_on.setVisibility(View.GONE);
                    llJobAmount.setVisibility(View.GONE);
                    llScheduleOn.setVisibility(View.GONE);
                    tvJobAmount.setText(taskDataBean.getPrice());
                    tvJobScheduleOn.setText(setDate(taskDataBean.getDate())+ "," + taskDataBean.getSlot());
                    tvBidPrice.setFocusable(false);
                    tvBidPrice.setEnabled(false);
                    int length = taskDataBean.getPrice().length();
                    tvBidPrice.setText(taskDataBean.getPrice().substring(1, length));
                    // Toast.makeText(getApplicationContext(),taskDataBean.getPrice().substring(1,length),Toast.LENGTH_SHORT).show();
                    llSchedule.setVisibility(View.GONE);
                    tvSelectedSlot.setVisibility(View.VISIBLE);
                    tvSelectedSlot.setText(  setDate(taskDataBean.getDate())+ "," +taskDataBean.getSlot() );
                    tvSelectedSlot.setEnabled(false);
                    ll_send_bid.setVisibility(View.GONE);
                }
                else if(taskDataBean.getStatus_id()==9)
                {
                    ll_requested_on.setVisibility(View.VISIBLE);
                    tvRequestedOn.setText(taskDataBean.getRequestedOn());
                    llJobAmount.setVisibility(View.GONE);
                    llScheduleOn.setVisibility(View.GONE);
                    tvBidPrice.setFocusable(true);
                    int length = taskDataBean.getPrice().length();
                    tvBidPrice.setText(taskDataBean.getPrice().substring(1, length));
                    // Toast.makeText(getApplicationContext(),taskDataBean.getPrice(),Toast.LENGTH_SHORT).show();
                    llSchedule.setVisibility(View.VISIBLE);
                    tvSelectedSlot.setVisibility(View.GONE);
                    tvSelectedSlot.setText("" + "," + "");
                    ll_send_bid.setVisibility(View.VISIBLE);
                }
            }
            else
            {
                customerDetailsPresenter.onGetCustomer(taskId);
            }
        }

    }

    @Override
    public void onSuccessfullyGetCustomer(CustomerDetailsResponse customerDetailsResponse) {
        a = new ArrayList<>();
        arrayListPhoto.clear();
        if(customerDetailsResponse.getResponseCode()==1) {
            if(jobStatus.equals("1")||jobStatus.equals("9")||jobStatus.equals("6")||jobStatus.equals("10")) {
                servicerequestId.setText(customerDetailsResponse.getResponseData().getTaskData().getService_request_id());
                customerId = customerDetailsResponse.getResponseData().getTaskData().getCustomerId();
                tvName.setText(customerDetailsResponse.getResponseData().getTaskData().getCustomerName());
                tvPhoneNumber.setVisibility(View.GONE);
                tvPhoneNumber.setText(customerDetailsResponse.getResponseData().getTaskData().getCustomerPhone());
                tvAddress.setText(customerDetailsResponse.getResponseData().getTaskData().getFullAddress());
                tvServiceAt.setText(customerDetailsResponse.getResponseData().getTaskData().getServiceAt());
                tvServiceRequired.setText(customerDetailsResponse.getResponseData().getTaskData().getServiceRequired());
                if (customerDetailsResponse.getResponseData().getTaskData().getService_type_id().equals("1")) {
                    String text = customerDetailsResponse.getResponseData().getTaskData().getServideNeeded();
                    if (!customerDetailsResponse.getResponseData().getTaskData().getGrassSize().equals("")) {
                        text = text + ", " + customerDetailsResponse.getResponseData().getTaskData().getGrassSize();
                    }

                    tvServiceNeed.setText(text);
                } else if (customerDetailsResponse.getResponseData().getTaskData().getService_type_id().equals("2")) {
                    String text = customerDetailsResponse.getResponseData().getTaskData().getServideNeeded();
                    if (!customerDetailsResponse.getResponseData().getTaskData().getHowManyTreeRemove().equals("")) {
                        text = text + ", " + customerDetailsResponse.getResponseData().getTaskData().getHowManyTreeRemove();
                    }
                    if (!customerDetailsResponse.getResponseData().getTaskData().getHowManyTreeTrim().equals("")) {
                        text = text + ", " + customerDetailsResponse.getResponseData().getTaskData().getHowManyTreeTrim();

                    }
                    Log.e("CustomerDetails", text);
                    tvServiceNeed.setText(text);
                } else {
                    tvServiceNeed.setText(customerDetailsResponse.getResponseData().getTaskData().getServideNeeded() + ", " + customerDetailsResponse.getResponseData().getTaskData().getGrassSize());
                }

                tvServiceType.setText(customerDetailsResponse.getResponseData().getTaskData().getServiceType());
                tvServiceTime.setText(customerDetailsResponse.getResponseData().getTaskData().getServiceTiming());
                tvComment.setText(customerDetailsResponse.getResponseData().getTaskData().getOtherInfo());

                Latitude = customerDetailsResponse.getResponseData().getTaskData().getLat();
                Longitude = customerDetailsResponse.getResponseData().getTaskData().getLng();


                if (customerDetailsResponse.getResponseData().getTaskData().getOnSitePresent()) {
                    tvCustomerPresent.setText("Yes");
                } else {
                    tvCustomerPresent.setText("No");
                }

                if (customerDetailsResponse.getResponseData().getTaskData().getService_type_id().equals("1")) {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getTaskData().getLawnSize());
                } else if (customerDetailsResponse.getResponseData().getTaskData().getService_type_id().equals("2")) {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getTaskData().getGrassSize());
                } else {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getTaskData().getLawnSize());
                }
                Glide.with(getApplication()).load(customerDetailsResponse.getResponseData().getTaskData().getCustomerProfileImage())
                        .into(iv_prc);
                if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 0) {
                    tvSiteImage.setVisibility(View.GONE);
                    ll_site_image.setVisibility(View.GONE);
                } else {
                    tvSiteImage.setVisibility(View.VISIBLE);
                    ll_site_image.setVisibility(View.VISIBLE);
                }
                if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 1) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    a.add(ImageOne);
                } else if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 2) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(1);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                } else if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 3) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(2);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                } else if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 4) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(2);
                    ImageFour = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(3);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                } else if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 5) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(2);
                    ImageFour = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(3);
                    ImageFive = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(4);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                    a.add(ImageFive);
                }

                // adapter = new TakeImageAdapterAC(a, this);
                adapter.loadList(a);
                adapter.setAdapterListener(new TakeImageAdapterAC.CellListener() {
                    @Override
                    public void onItemClick(int pos, ArrayList<String> items) {
                        String str = "";
                        if (pos == 0) {
                            str = ImageOne;
                        }
                        if (pos == 1) {
                            str = ImageTwo;
                        }
                        if (pos == 2) {
                            str = ImageThree;
                        }
                        if (pos == 3) {
                            str = ImageFour;
                        }
                        if (pos == 4) {
                            str = ImageFive;
                        }

                        Glide.with(mContext).
                                load(str)
                                //.placeholder(R.drawable.image_placeholder)
                                .into(iv_gal_img);
                        dialog.show();
                    }
                });
                adapter.notifyDataSetChanged();
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_take_image);
                LinearLayoutManager layoutManagerMakeOverBrand = new LinearLayoutManager(this);
                layoutManagerMakeOverBrand.setOrientation(LinearLayoutManager.HORIZONTAL);


                recyclerView.setLayoutManager(layoutManagerMakeOverBrand);
                recyclerView.setAdapter(adapter);
                //  Toast.makeText(getApplicationContext(),taskDataBean.getStatus(),Toast.LENGTH_SHORT).show();

                if (customerDetailsResponse.getResponseData().getTaskData().getStatusId()==6) {
                    ll_requested_on.setVisibility(View.GONE);

                    llJobAmount.setVisibility(View.GONE);
                    llScheduleOn.setVisibility(View.GONE);
                    tvJobAmount.setText(customerDetailsResponse.getResponseData().getTaskData().getPrice());
                    tvJobScheduleOn.setText(setDate(customerDetailsResponse.getResponseData().getTaskData().getDate()) + "," + customerDetailsResponse.getResponseData().getTaskData().getSlot());
                    tvBidPrice.setFocusable(false);
                    tvBidPrice.setEnabled(false);
                    int length = customerDetailsResponse.getResponseData().getTaskData().getPrice().length();
                    tvBidPrice.setText(customerDetailsResponse.getResponseData().getTaskData().getPrice().substring(1, length));
                    // Toast.makeText(getApplicationContext(),taskDataBean.getPrice().substring(1,length),Toast.LENGTH_SHORT).show();
                    llSchedule.setVisibility(View.GONE);
                    tvSelectedSlot.setVisibility(View.VISIBLE);
                    tvSelectedSlot.setText(setDate(customerDetailsResponse.getResponseData().getTaskData().getDate()) + "," + customerDetailsResponse.getResponseData().getTaskData().getSlot());
                    tvSelectedSlot.setEnabled(false);
                    ll_send_bid.setVisibility(View.GONE);
                } else if (customerDetailsResponse.getResponseData().getTaskData().getStatusId()==1) {

                    ll_requested_on.setVisibility(View.VISIBLE);
                    tvRequestedOn.setText(customerDetailsResponse.getResponseData().getTaskData().getRequestedOn());
                    llJobAmount.setVisibility(View.GONE);
                    llScheduleOn.setVisibility(View.GONE);
                    tvBidPrice.setFocusable(true);
                    int length = customerDetailsResponse.getResponseData().getTaskData().getPrice().length();
                    tvBidPrice.setText(customerDetailsResponse.getResponseData().getTaskData().getPrice().substring(1, length));
                    // Toast.makeText(getApplicationContext(),taskDataBean.getPrice(),Toast.LENGTH_SHORT).show();
                    llSchedule.setVisibility(View.VISIBLE);
                    tvSelectedSlot.setVisibility(View.GONE);
                    tvSelectedSlot.setText("" + "," + "");
                    ll_send_bid.setVisibility(View.VISIBLE);

                }
                else if(customerDetailsResponse.getResponseData().getTaskData().getStatusId()==10)
                {
                    llJobAmount.setVisibility(View.GONE);
                    llScheduleOn.setVisibility(View.GONE);
                    tvJobAmount.setText(customerDetailsResponse.getResponseData().getTaskData().getPrice());
                    tvJobScheduleOn.setText(setDate(customerDetailsResponse.getResponseData().getTaskData().getDate()) + "," + customerDetailsResponse.getResponseData().getTaskData().getSlot());
                    tvBidPrice.setFocusable(false);
                    tvBidPrice.setEnabled(false);
                    int length = customerDetailsResponse.getResponseData().getTaskData().getPrice().length();
                    tvBidPrice.setText(customerDetailsResponse.getResponseData().getTaskData().getPrice().substring(1, length));
                    // Toast.makeText(getApplicationContext(),taskDataBean.getPrice().substring(1,length),Toast.LENGTH_SHORT).show();
                    llSchedule.setVisibility(View.GONE);
                    tvSelectedSlot.setVisibility(View.VISIBLE);
                    tvSelectedSlot.setText(setDate(customerDetailsResponse.getResponseData().getTaskData().getDate()) + "," + customerDetailsResponse.getResponseData().getTaskData().getSlot());
                    tvSelectedSlot.setEnabled(false);
                    ll_send_bid.setVisibility(View.GONE);
                }
                else if(customerDetailsResponse.getResponseData().getTaskData().getStatusId()==9)
                {
                    ll_requested_on.setVisibility(View.VISIBLE);
                    tvRequestedOn.setText(customerDetailsResponse.getResponseData().getTaskData().getRequestedOn());
                    llJobAmount.setVisibility(View.GONE);
                    llScheduleOn.setVisibility(View.GONE);
                    tvBidPrice.setFocusable(true);
                    int length = customerDetailsResponse.getResponseData().getTaskData().getPrice().length();
                    tvBidPrice.setText(customerDetailsResponse.getResponseData().getTaskData().getPrice().substring(1, length));
                    // Toast.makeText(getApplicationContext(),taskDataBean.getPrice(),Toast.LENGTH_SHORT).show();
                    llSchedule.setVisibility(View.VISIBLE);
                    tvSelectedSlot.setVisibility(View.GONE);
                    tvSelectedSlot.setText("" + "," + "");
                    ll_send_bid.setVisibility(View.VISIBLE);
                }
            }
            if(jobStatus.equals("2")||jobStatus.equals("3")||jobStatus.equals("7")||jobStatus.equals("8"))
            {
                servicerequestId.setText(customerDetailsResponse.getResponseData().getTaskData().getService_request_id());
                customerId =  customerDetailsResponse.getResponseData().getTaskData().getCustomerId();
                tvSiteImage.setVisibility(View.VISIBLE);
                bidId= customerDetailsResponse.getResponseData().getTaskData().getBid_id();
                ll_requested_on.setVisibility(View.GONE);
                Latitude=customerDetailsResponse.getResponseData().getTaskData().getLat();
                Longitude=customerDetailsResponse.getResponseData().getTaskData().getLng();
                // tvHeader.setText(taskDataBean.getCustomer_name());
                tvName.setText(customerDetailsResponse.getResponseData().getTaskData().getCustomerName());
                tvPhoneNumber.setVisibility(View.VISIBLE);
                tvPhoneNumber.setText(customerDetailsResponse.getResponseData().getTaskData().getCustomerPhone());
                tvAddress.setText(customerDetailsResponse.getResponseData().getTaskData().getFullAddress());
                tvServiceAt.setText(customerDetailsResponse.getResponseData().getTaskData().getServiceAt());
                tvServiceRequired.setText(customerDetailsResponse.getResponseData().getTaskData().getServiceRequired());

                tvServiceType.setText(customerDetailsResponse.getResponseData().getTaskData().getServiceType());
                tvServiceTime.setText(customerDetailsResponse.getResponseData().getTaskData().getServiceTiming());
                tvJobAmount.setText(customerDetailsResponse.getResponseData().getTaskData().getPrice());
                tvJobScheduleOn.setText(setDate( customerDetailsResponse.getResponseData().getTaskData().getDate()) + "," + customerDetailsResponse.getResponseData().getTaskData().getSlot());
                tvComment.setText(customerDetailsResponse.getResponseData().getTaskData().getOtherInfo());
                tvSiteImage.setVisibility(View.VISIBLE);


                if (customerDetailsResponse.getResponseData().getTaskData().getService_type_id().equals("1")) {
                    String text=customerDetailsResponse.getResponseData().getTaskData().getServideNeeded();
                    if(!customerDetailsResponse.getResponseData().getTaskData().getGrassSize().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getTaskData().getGrassSize();
                    }

                    tvServiceNeed.setText(text);
                } else if(customerDetailsResponse.getResponseData().getTaskData().getService_type_id().equals("2")) {
                    String text=customerDetailsResponse.getResponseData().getTaskData().getServideNeeded();
                    if(!customerDetailsResponse.getResponseData().getTaskData().getHowManyTreeRemove().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getTaskData().getHowManyTreeRemove();
                    }
                    if(!customerDetailsResponse.getResponseData().getTaskData().getHowManyTreeRemove().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getTaskData().getHowManyTreeTrim();

                    }
                    Log.e("CustomerDetails",text);
                    tvServiceNeed.setText(text);
                }
                else
                {
                    tvServiceNeed.setText(customerDetailsResponse.getResponseData().getTaskData().getServideNeeded()+", "+customerDetailsResponse.getResponseData().getTaskData().getGrassSize());
                }

                llJobAmount.setVisibility(View.VISIBLE);
                llScheduleOn.setVisibility(View.VISIBLE);
                tvJobAmount.setText(customerDetailsResponse.getResponseData().getTaskData().getPrice());
                tvJobScheduleOn.setText(setDate( customerDetailsResponse.getResponseData().getTaskData().getDate()) + "," + customerDetailsResponse.getResponseData().getTaskData().getSlot());

                if (customerDetailsResponse.getResponseData().getTaskData().getOnSitePresent()) {
                    tvCustomerPresent.setText("Yes");
                } else {
                    tvCustomerPresent.setText("No");
                }

                if (customerDetailsResponse.getResponseData().getTaskData().getService_type_id().equals("1")) {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getTaskData().getLawnSize());
                } else if(customerDetailsResponse.getResponseData().getTaskData().getService_type_id().equals("2")) {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getTaskData().getGrassSize());
                }
                else
                {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getTaskData().getLawnSize());
                }
                Glide.with(getApplication()).load(customerDetailsResponse.getResponseData().getTaskData().getCustomerProfileImage())
                        .into(iv_prc);
                if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 0) {
                    tvSiteImage.setVisibility(View.GONE);
                    ll_site_image.setVisibility(View.GONE);
                } else {
                    tvSiteImage.setVisibility(View.VISIBLE);
                    ll_site_image.setVisibility(View.VISIBLE);
                }
                if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 1) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    a.add(ImageOne);
                } else if ( customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 2) {
                    ImageOne =customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(1);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                } else if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 3) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(1);
                    ImageThree =customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(2);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                } else if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 4) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    ImageTwo =customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(2);
                    ImageFour = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(3);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                } else if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 5) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(2);
                    ImageFour =customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(3);
                    ImageFive =customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(4);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                    a.add(ImageFive);
                }


                //  adapter = new TakeImageAdapterAC(a, this);
                adapter.loadList(a);
                adapter.setAdapterListener(new TakeImageAdapterAC.CellListener() {
                    @Override
                    public void onItemClick(int pos, ArrayList<String> items) {
                        String str = "";
                        if (pos == 0) {
                            str = ImageOne;
                        }
                        if (pos == 1) {
                            str = ImageTwo;
                        }
                        if (pos == 2) {
                            str = ImageThree;
                        }
                        if (pos == 3) {
                            str = ImageFour;
                        }
                        if (pos == 4) {
                            str = ImageFive;
                        }

                        Glide.with(mContext).
                                load(str)
                                // .placeholder(R.drawable.image_placeholder)
                                .into(iv_gal_img);
                        dialog.show();
                    }
                });
                adapter.notifyDataSetChanged();
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_take_image);
                LinearLayoutManager layoutManagerMakeOverBrand = new LinearLayoutManager(this);
                layoutManagerMakeOverBrand.setOrientation(LinearLayoutManager.HORIZONTAL);


                recyclerView.setLayoutManager(layoutManagerMakeOverBrand);
                recyclerView.setAdapter(adapter);
                if (customerDetailsResponse.getResponseData().getTaskData().getStatusId()==6) {
                    tvBidPrice.setFocusable(false);
                    tvBidPrice.setEnabled(false);
                    int length = customerDetailsResponse.getResponseData().getTaskData().getPrice().length();
                    tvBidPrice.setText(customerDetailsResponse.getResponseData().getTaskData().getPrice().substring(1, length));
                    // Toast.makeText(getApplicationContext(),taskDataBean.getPrice().substring(1,length),Toast.LENGTH_SHORT).show();
                    llSchedule.setVisibility(View.GONE);
                    tvSelectedSlot.setVisibility(View.VISIBLE);
                    tvSelectedSlot.setText(  setDate(customerDetailsResponse.getResponseData().getTaskData().getDate())+ ","  +customerDetailsResponse.getResponseData().getTaskData().getSlot());
                    tvSelectedSlot.setEnabled(false);
                    ll_send_bid.setVisibility(View.GONE);

                } else if (customerDetailsResponse.getResponseData().getTaskData().getStatusId()==1) {
                    tvBidPrice.setFocusable(true);
                    int length = customerDetailsResponse.getResponseData().getTaskData().getPrice().length();
                    tvBidPrice.setText(customerDetailsResponse.getResponseData().getTaskData().getPrice().substring(1, length));
                    // Toast.makeText(getApplicationContext(),taskDataBean.getPrice(),Toast.LENGTH_SHORT).show();
                    llSchedule.setVisibility(View.VISIBLE);
                    tvSelectedSlot.setVisibility(View.GONE);
                    tvSelectedSlot.setText("" + "," + "");
                    ll_send_bid.setVisibility(View.VISIBLE);

                }
            }
            if(jobStatus.equals("4"))
            {
               // Toast.makeText(getApplicationContext(),"enter",Toast.LENGTH_SHORT).show();
                servicerequestId.setText(customerDetailsResponse.getResponseData().getTaskData().getService_request_id());
                customerId =  customerDetailsResponse.getResponseData().getTaskData().getCustomerId();
                tvSiteImage.setVisibility(View.VISIBLE);
                bidId= customerDetailsResponse.getResponseData().getTaskData().getBid_id();
                ll_requested_on.setVisibility(View.GONE);
                Latitude= customerDetailsResponse.getResponseData().getTaskData().getLat();
                Longitude= customerDetailsResponse.getResponseData().getTaskData().getLng();
                // tvHeader.setText(taskDataBean.getCustomer_name());
                tvName.setText( customerDetailsResponse.getResponseData().getTaskData().getCustomerName());
                tvPhoneNumber.setVisibility(View.VISIBLE);
                tvPhoneNumber.setText( customerDetailsResponse.getResponseData().getTaskData().getCustomerPhone());
                tvAddress.setText( customerDetailsResponse.getResponseData().getTaskData().getFullAddress());
                tvServiceAt.setText( customerDetailsResponse.getResponseData().getTaskData().getServiceAt());
                tvServiceRequired.setText( customerDetailsResponse.getResponseData().getTaskData().getServiceRequired());

                tvServiceType.setText(customerDetailsResponse.getResponseData().getTaskData().getServiceType());
                tvServiceTime.setText(customerDetailsResponse.getResponseData().getTaskData().getServiceTiming());
                tvJobAmount.setText(customerDetailsResponse.getResponseData().getTaskData().getPrice());
                tvJobScheduleOn.setText(setDate( customerDetailsResponse.getResponseData().getTaskData().getDate()) + "," + customerDetailsResponse.getResponseData().getTaskData().getSlot());
                tvComment.setText(customerDetailsResponse.getResponseData().getTaskData().getOtherInfo());
                tvSiteImage.setVisibility(View.VISIBLE);

                llJobAmount.setVisibility(View.VISIBLE);
                llScheduleOn.setVisibility(View.VISIBLE);
                tvJobAmount.setText(customerDetailsResponse.getResponseData().getTaskData().getPrice());
                tvJobScheduleOn.setText(setDate( customerDetailsResponse.getResponseData().getTaskData().getDate()) + "," + customerDetailsResponse.getResponseData().getTaskData().getSlot());
                // Toast.makeText(getApplicationContext(),taskDataBean.getIs_block()+"",Toast.LENGTH_SHORT).show();
                if (customerDetailsResponse.getResponseData().getTaskData().getIs_block()) {
                    ivblocked.setVisibility(View.GONE);
                    ivunblocked.setVisibility(View.VISIBLE);
                } else {
                    ivblocked.setVisibility(View.VISIBLE);
                    ivunblocked.setVisibility(View.GONE);
                }
                if (customerDetailsResponse.getResponseData().getTaskData().getOnSitePresent()) {
                    tvCustomerPresent.setText("Yes");
                } else {
                    tvCustomerPresent.setText("No");
                }

                if (customerDetailsResponse.getResponseData().getTaskData().getService_type_id().equals("1")) {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getTaskData().getLawnSize());
                } else if(customerDetailsResponse.getResponseData().getTaskData().getService_type_id().equals("2")) {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getTaskData().getGrassSize());
                }
                else
                {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getTaskData().getLawnSize());
                }


                //payment Status

                if(customerDetailsResponse.getResponseData().getTaskData().getPayment_validation_status().equals(""))
                {
                    tvPaymentStatus.setText(getResources().getString(R.string.pending));

                    iv_jobdone.setVisibility(View.VISIBLE);
                    iv_paid.setVisibility(View.GONE);

                }
                else if(customerDetailsResponse.getResponseData().getTaskData().getPayment_validation_status().equals("11"))
                {
                    tvPaymentStatus.setText(getResources().getString(R.string.onHole));
                    iv_jobdone.setVisibility(View.VISIBLE);
                    iv_paid.setVisibility(View.GONE);

                }
                else if(customerDetailsResponse.getResponseData().getTaskData().getPayment_validation_status().equals("12"))
                {
                    tvPaymentStatus.setText(getResources().getString(R.string.paid));
                    iv_jobdone.setVisibility(View.VISIBLE);
                    iv_paid.setVisibility(View.GONE);

                }
                else if(customerDetailsResponse.getResponseData().getTaskData().getPayment_validation_status().equals("13"))
                {
                    tvPaymentStatus.setText(getResources().getString(R.string.paidbybidjobbers));
                    iv_jobdone.setVisibility(View.GONE);
                    iv_paid.setVisibility(View.VISIBLE);

                }

                if (customerDetailsResponse.getResponseData().getTaskData().getService_type_id().equals("1")) {
                    String text=customerDetailsResponse.getResponseData().getTaskData().getServideNeeded();
                    if(!customerDetailsResponse.getResponseData().getTaskData().getGrassSize().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getTaskData().getGrassSize();
                    }

                    tvServiceNeed.setText(text);
                } else if(customerDetailsResponse.getResponseData().getTaskData().getService_type_id().equals("2")) {
                    String text=customerDetailsResponse.getResponseData().getTaskData().getServideNeeded();
                    if(!customerDetailsResponse.getResponseData().getTaskData().getHowManyTreeRemove().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getTaskData().getHowManyTreeRemove();
                    }
                    if(!customerDetailsResponse.getResponseData().getTaskData().getHowManyTreeRemove().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getTaskData().getHowManyTreeRemove();

                    }
                    Log.e("CustomerDetails",text);
                    tvServiceNeed.setText(text);
                }
                else
                {
                    tvServiceNeed.setText(customerDetailsResponse.getResponseData().getTaskData().getServideNeeded()+", "+customerDetailsResponse.getResponseData().getTaskData().getGrassSize());
                }
                Glide.with(getApplication()).load(customerDetailsResponse.getResponseData().getTaskData().getCustomerProfileImage())
                        .into(iv_prc);
                if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 0) {
                    tvSiteImage.setVisibility(View.GONE);
                    ll_site_image.setVisibility(View.GONE);
                } else {
                    tvSiteImage.setVisibility(View.VISIBLE);
                    ll_site_image.setVisibility(View.VISIBLE);
                }
                if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 1) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    a.add(ImageOne);
                } else if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 2) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(1);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                } else if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 3) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(1);
                    ImageThree =customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(2);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                } else if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 4) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(2);
                    ImageFour = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(3);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                } else if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 5) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(1);
                    ImageThree =customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(2);
                    ImageFour = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(3);
                    ImageFive = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(4);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                    a.add(ImageFive);
                }


                // adapter = new TakeImageAdapterAC(a, this);
                adapter.loadList(a);
                adapter.setAdapterListener(new TakeImageAdapterAC.CellListener() {
                    @Override
                    public void onItemClick(int pos, ArrayList<String> items) {
                        String str = "";
                        if (pos == 0) {
                            str = ImageOne;
                        }
                        if (pos == 1) {
                            str = ImageTwo;
                        }
                        if (pos == 2) {
                            str = ImageThree;
                        }
                        if (pos == 3) {
                            str = ImageFour;
                        }
                        if (pos == 4) {
                            str = ImageFive;
                        }

                        Glide.with(mContext).
                                load(str)
                                //.placeholder(R.drawable.image_placeholder)
                                .into(iv_gal_img);
                        dialog.show();
                    }
                });
                adapter.notifyDataSetChanged();
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_take_image);
                LinearLayoutManager layoutManagerMakeOverBrand = new LinearLayoutManager(this);
                layoutManagerMakeOverBrand.setOrientation(LinearLayoutManager.HORIZONTAL);


                recyclerView.setLayoutManager(layoutManagerMakeOverBrand);
                recyclerView.setAdapter(adapter);

                tvEndNote.setText( customerDetailsResponse.getResponseData().getTaskData().getTask_end_notes());
                if( customerDetailsResponse.getResponseData().getTaskData().getTask_images().size()>0) {

                    for (int k = 0; k <  customerDetailsResponse.getResponseData().getTaskData().getTask_images().size(); k++) {
                        arrayListPhoto.add( customerDetailsResponse.getResponseData().getTaskData().getTask_image_url() + "/" +  customerDetailsResponse.getResponseData().getTaskData().getTask_images().get(k));
                    }


                    adapterEnd.loadList(arrayListPhoto);
                    rv_end_photo.setAdapter(adapterEnd);



                    adapterEnd.setAdapterListener(new TakeImageAdapterAC.CellListener() {
                        @Override
                        public void onItemClick(int pos, ArrayList<String> items) {
                            String str = "";
                            str= customerDetailsResponse.getResponseData().getTaskData().getTask_image_url() + "/" +  customerDetailsResponse.getResponseData().getTaskData().getTask_images().get(pos);
                            Log.e("COMPLETE",str);
                            Glide.with(mContext).
                                    load(str)
                                    //.placeholder(R.drawable.image_placeholder)
                                    .into(iv_gal_img);
                            dialog.show();
                        }
                    });
                }
            }
            if(jobStatus.equals("5"))
            {
                servicerequestId.setText(customerDetailsResponse.getResponseData().getTaskData().getService_request_id());
                customerId = customerDetailsResponse.getResponseData().getTaskData().getCustomerId();
                tvSiteImage.setVisibility(View.VISIBLE);
                bidId=customerDetailsResponse.getResponseData().getTaskData().getBid_id();
                ll_requested_on.setVisibility(View.GONE);
                Latitude=customerDetailsResponse.getResponseData().getTaskData().getLat();
                Longitude=customerDetailsResponse.getResponseData().getTaskData().getLng();
                // tvHeader.setText(taskDataBean.getCustomer_name());
                tvName.setText(customerDetailsResponse.getResponseData().getTaskData().getCustomerName());
                tvPhoneNumber.setVisibility(View.VISIBLE);
                tvPhoneNumber.setText(customerDetailsResponse.getResponseData().getTaskData().getCustomerPhone());
                tvAddress.setText(customerDetailsResponse.getResponseData().getTaskData().getFullAddress());
                tvServiceAt.setText(customerDetailsResponse.getResponseData().getTaskData().getServiceAt());
                tvServiceRequired.setText(customerDetailsResponse.getResponseData().getTaskData().getServiceRequired());
                tvServiceType.setText(customerDetailsResponse.getResponseData().getTaskData().getServiceType());
                tvServiceTime.setText(customerDetailsResponse.getResponseData().getTaskData().getServiceTiming());
                tvJobAmount.setText(customerDetailsResponse.getResponseData().getTaskData().getPrice());
                tvJobScheduleOn.setText(setDate( customerDetailsResponse.getResponseData().getTaskData().getDate()) + "," + customerDetailsResponse.getResponseData().getTaskData().getSlot());
                tvComment.setText(customerDetailsResponse.getResponseData().getTaskData().getOtherInfo());
                tvSiteImage.setVisibility(View.VISIBLE);

                llJobAmount.setVisibility(View.VISIBLE);
                llScheduleOn.setVisibility(View.VISIBLE);
                tvJobAmount.setText(customerDetailsResponse.getResponseData().getTaskData().getPrice());
                tvJobScheduleOn.setText(setDate( customerDetailsResponse.getResponseData().getTaskData().getDate()) + "," + customerDetailsResponse.getResponseData().getTaskData().getSlot());

                if (customerDetailsResponse.getResponseData().getTaskData().getOnSitePresent()) {
                    tvCustomerPresent.setText("Yes");
                } else {
                    tvCustomerPresent.setText("No");
                }

                if (customerDetailsResponse.getResponseData().getTaskData().getIs_block()) {
                    ivblocked.setVisibility(View.GONE);
                    ivunblocked.setVisibility(View.VISIBLE);
                } else {
                    ivblocked.setVisibility(View.VISIBLE);
                    ivunblocked.setVisibility(View.GONE);
                }

//
                if (customerDetailsResponse.getResponseData().getTaskData().getService_type_id().equals("1")) {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getTaskData().getLawnSize());
                } else if(customerDetailsResponse.getResponseData().getTaskData().getService_type_id().equals("2")) {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getTaskData().getGrassSize());
                }
                else
                {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getTaskData().getLawnSize());
                }


                if (customerDetailsResponse.getResponseData().getTaskData().getService_type_id().equals("1")) {
                    String text=customerDetailsResponse.getResponseData().getTaskData().getServideNeeded();
                    if(!customerDetailsResponse.getResponseData().getTaskData().getGrassSize().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getTaskData().getGrassSize();
                    }

                    tvServiceNeed.setText(text);
                } else if(customerDetailsResponse.getResponseData().getTaskData().getService_type_id().equals("2")) {
                    String text=customerDetailsResponse.getResponseData().getTaskData().getServideNeeded();
                    if(!customerDetailsResponse.getResponseData().getTaskData().getHowManyTreeRemove().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getTaskData().getHowManyTreeRemove();
                    }
                    if(!customerDetailsResponse.getResponseData().getTaskData().getHowManyTreeTrim().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getTaskData().getHowManyTreeTrim();

                    }
                    Log.e("CustomerDetails",text);
                    tvServiceNeed.setText(text);
                }
                else
                {
                    tvServiceNeed.setText(customerDetailsResponse.getResponseData().getTaskData().getServideNeeded()+", "+customerDetailsResponse.getResponseData().getTaskData().getGrassSize());
                }

                Glide.with(getApplication()).load(customerDetailsResponse.getResponseData().getTaskData().getCustomerProfileImage())
                        .into(iv_prc);
                if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 0) {
                    tvSiteImage.setVisibility(View.GONE);
                    ll_site_image.setVisibility(View.GONE);
                } else {
                    tvSiteImage.setVisibility(View.VISIBLE);
                    ll_site_image.setVisibility(View.VISIBLE);
                }

                //  Toast.makeText(getApplicationContext(), taskDataBean.getCustomer_work_image().size() + "", Toast.LENGTH_SHORT).show();
                if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 1) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    a.add(ImageOne);
                } else if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 2) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(1);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                } else if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 3) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(2);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                } else if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 4) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(2);
                    ImageFour = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(3);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);

                } else if (customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().size() == 5) {
                    ImageOne = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(2);
                    ImageFour = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(3);
                    ImageFive = customerDetailsResponse.getResponseData().getTaskData().getCustomerWorkImage().get(4);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                    a.add(ImageFive);
                }


                // adapter = new TakeImageAdapterAC(a, this);
                adapter.loadList(a);
                adapter.setAdapterListener(new TakeImageAdapterAC.CellListener() {
                    @Override
                    public void onItemClick(int pos, ArrayList<String> items) {
                        String str = "";
                        if (pos == 0) {
                            str = ImageOne;
                        }
                        if (pos == 1) {
                            str = ImageTwo;
                        }
                        if (pos == 2) {
                            str = ImageThree;
                        }
                        if (pos == 3) {
                            str = ImageFour;
                        }
                        if (pos == 4) {
                            str = ImageFive;
                        }

                        Glide.with(mContext).
                                load(str)
                                // .placeholder(R.drawable.image_placeholder)
                                .into(iv_gal_img);
                        dialog.show();
                    }
                });
                adapter.notifyDataSetChanged();
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_take_image);
                LinearLayoutManager layoutManagerMakeOverBrand = new LinearLayoutManager(this);
                layoutManagerMakeOverBrand.setOrientation(LinearLayoutManager.HORIZONTAL);


                recyclerView.setLayoutManager(layoutManagerMakeOverBrand);
                recyclerView.setAdapter(adapter);

            }
        }
        else
        {

        }
    }

    @Override
    public void allTasksNameSchdeule(ScheduletaskResponse arrayList) {
        if(arrayList.getResponseCode()==1) {
            if (arrayList.getResponseData().getTaskData().size() > 0) {


//                if(currentPage.equals("1")){
//
//                    productItemList.clear();
//                    productItemList.addAll(scheduletaskResponse.getResponseData().getTaskData());
//                }else{
//
//                    productItemList.remove(productItemList.size() - 1);
//                    taskListingAdapter.notifyItemRemoved(productItemList.size());
//
//                    productItemList.addAll(scheduletaskResponse.getResponseData().getTaskData());
//                    //mAdapter.notifyDataSetChanged();
//                }

                for(int i=0;i<arrayList.getResponseData().getTaskData().size()-1;i++)
                {
                    taskIdSchedule.add(arrayList.getResponseData().getTaskData().get(i).getStatus_id()+"");
                }
                currentPage = arrayList.getResponseData().getPagination().getNext_page();
                if(currentPage.equals(""))
                {

                }else
                {
                    customerDetailsPresenter.getAllTasksSchedule(currentPage);
                }

            } else {


            }


        }


    }

    @Override
    public void allScheduleTasksName(List<ScheduletaskResponse.ResponseDataBean.TaskDataBean> arrayList) {
        a = new ArrayList<>();
       // Toast.makeText(getApplicationContext(),taskId,Toast.LENGTH_SHORT).show();
        for (int i = 0; i < arrayList.size(); i++) {
            ScheduletaskResponse.ResponseDataBean.TaskDataBean taskDataBean = arrayList.get(i);
            if (taskId.equals(taskDataBean.getTask_id())) {

                servicerequestId.setText(taskDataBean.getService_request_id());
                ll_requested_on.setVisibility(View.GONE);
                customerId = taskDataBean.getCustomer_id();
                tvSiteImage.setVisibility(View.VISIBLE);
                bidId=taskDataBean.getBid_id();

                Latitude=taskDataBean.getLat();
                Longitude=taskDataBean.getLng();
                // tvHeader.setText(taskDataBean.getCustomer_name());
                tvName.setText(taskDataBean.getCustomer_name());
                tvPhoneNumber.setVisibility(View.VISIBLE);
                tvPhoneNumber.setText(taskDataBean.getCustomer_phone());
                tvAddress.setText(taskDataBean.getFull_address());
                tvServiceAt.setText(taskDataBean.getService_at());
                tvServiceRequired.setText(taskDataBean.getService_required());

                tvServiceType.setText(taskDataBean.getService_type());
                tvServiceTime.setText(taskDataBean.getService_timing());
                tvJobAmount.setText(taskDataBean.getPrice());
                tvJobScheduleOn.setText(setDate( taskDataBean.getDate()) + "," + taskDataBean.getSlot());
                tvComment.setText(taskDataBean.getOther_info());
                tvSiteImage.setVisibility(View.VISIBLE);


                if (taskDataBean.getService_type_id().equals("1")) {
                    String text=taskDataBean.getServide_needed();
                    if(!taskDataBean.getGrass_size().equals(""))
                    {
                        text=text+", "+ taskDataBean.getGrass_size();
                    }

                    tvServiceNeed.setText(text);
                } else if(taskDataBean.getService_type_id().equals("2")) {
                    String text=taskDataBean.getServide_needed();
                    if(!taskDataBean.getHow_many_tree_remove().equals(""))
                    {
                        text=text+", "+ taskDataBean.getHow_many_tree_remove();
                    }
                    if(!taskDataBean.getHow_many_tree_trim().equals(""))
                    {
                        text=text+", "+ taskDataBean.getHow_many_tree_trim();

                    }
                    Log.e("CustomerDetails",text);
                    tvServiceNeed.setText(text);
                }
                else
                {
                    tvServiceNeed.setText(taskDataBean.getServide_needed()+", "+taskDataBean.getGrass_size());
                }

                llJobAmount.setVisibility(View.VISIBLE);
                llScheduleOn.setVisibility(View.VISIBLE);
                tvJobAmount.setText(taskDataBean.getPrice());
                tvJobScheduleOn.setText(setDate( taskDataBean.getDate()) + "," + taskDataBean.getSlot());

                if (taskDataBean.isOn_site_present()) {
                    tvCustomerPresent.setText("Yes");
                } else {
                    tvCustomerPresent.setText("No");
                }

                if (taskDataBean.getService_type_id().equals("1")) {
                    tvServiceSize.setText(taskDataBean.getLawn_size());
                } else if(taskDataBean.getService_type_id().equals("2")) {
                    tvServiceSize.setText(taskDataBean.getGrass_size());
                }
                else
                {
                    tvServiceSize.setText(taskDataBean.getLawn_size());
                }
                Glide.with(getApplication()).load(taskDataBean.getCustomer_profile_image())
                        .into(iv_prc);
                if (taskDataBean.getCustomer_work_image().size() == 0) {
                    tvSiteImage.setVisibility(View.GONE);
                    ll_site_image.setVisibility(View.GONE);
                } else {
                    tvSiteImage.setVisibility(View.VISIBLE);
                    ll_site_image.setVisibility(View.VISIBLE);
                }
                if (taskDataBean.getCustomer_work_image().size() == 1) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    a.add(ImageOne);
                } else if (taskDataBean.getCustomer_work_image().size() == 2) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    ImageTwo = taskDataBean.getCustomer_work_image().get(1);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                } else if (taskDataBean.getCustomer_work_image().size() == 3) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    ImageTwo = taskDataBean.getCustomer_work_image().get(1);
                    ImageThree = taskDataBean.getCustomer_work_image().get(2);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                } else if (taskDataBean.getCustomer_work_image().size() == 4) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    ImageTwo = taskDataBean.getCustomer_work_image().get(1);
                    ImageThree = taskDataBean.getCustomer_work_image().get(2);
                    ImageFour = taskDataBean.getCustomer_work_image().get(3);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                } else if (taskDataBean.getCustomer_work_image().size() == 5) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    ImageTwo = taskDataBean.getCustomer_work_image().get(1);
                    ImageThree = taskDataBean.getCustomer_work_image().get(2);
                    ImageFour = taskDataBean.getCustomer_work_image().get(3);
                    ImageFive = taskDataBean.getCustomer_work_image().get(4);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                    a.add(ImageFive);
                }


              //  adapter = new TakeImageAdapterAC(a, this);
                adapter.loadList(a);
                adapter.setAdapterListener(new TakeImageAdapterAC.CellListener() {
                    @Override
                    public void onItemClick(int pos, ArrayList<String> items) {
                        String str = "";
                        if (pos == 0) {
                            str = ImageOne;
                        }
                        if (pos == 1) {
                            str = ImageTwo;
                        }
                        if (pos == 2) {
                            str = ImageThree;
                        }
                        if (pos == 3) {
                            str = ImageFour;
                        }
                        if (pos == 4) {
                            str = ImageFive;
                        }

                        Glide.with(mContext).
                                load(str)
                               // .placeholder(R.drawable.image_placeholder)
                                .into(iv_gal_img);
                        dialog.show();
                    }
                });
                adapter.notifyDataSetChanged();
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_take_image);
                LinearLayoutManager layoutManagerMakeOverBrand = new LinearLayoutManager(this);
                layoutManagerMakeOverBrand.setOrientation(LinearLayoutManager.HORIZONTAL);


                recyclerView.setLayoutManager(layoutManagerMakeOverBrand);
                recyclerView.setAdapter(adapter);
                if (taskDataBean.getStatus_id()==6) {
                    tvBidPrice.setFocusable(false);
                    tvBidPrice.setEnabled(false);
                    int length = taskDataBean.getPrice().length();
                    tvBidPrice.setText(taskDataBean.getPrice().substring(1, length));
                    // Toast.makeText(getApplicationContext(),taskDataBean.getPrice().substring(1,length),Toast.LENGTH_SHORT).show();
                    llSchedule.setVisibility(View.GONE);
                    tvSelectedSlot.setVisibility(View.VISIBLE);
                    tvSelectedSlot.setText(  setDate(taskDataBean.getDate())+ ","  +taskDataBean.getSlot());
                    tvSelectedSlot.setEnabled(false);
                    ll_send_bid.setVisibility(View.GONE);

                } else if (taskDataBean.getStatus_id()==1) {
                    tvBidPrice.setFocusable(true);
                    int length = taskDataBean.getPrice().length();
                    tvBidPrice.setText(taskDataBean.getPrice().substring(1, length));
                    // Toast.makeText(getApplicationContext(),taskDataBean.getPrice(),Toast.LENGTH_SHORT).show();
                    llSchedule.setVisibility(View.VISIBLE);
                    tvSelectedSlot.setVisibility(View.GONE);
                    tvSelectedSlot.setText("" + "," + "");
                    ll_send_bid.setVisibility(View.VISIBLE);

                }
            }
            else
            {
                customerDetailsPresenter.onGetCustomer(taskId);
            }
        }
    }

    @Override
    public void allCompletedTaskName(List<CompletedTaskResponse.ResponseDataBean.TaskDataBean> arrayList) {
        a = new ArrayList<>();
        arrayListPhoto.clear();
        for (int i = 0; i < arrayList.size(); i++) {
            CompletedTaskResponse.ResponseDataBean.TaskDataBean taskDataBean = arrayList.get(i);
            if (taskId.equals(taskDataBean.getTask_id())) {
                servicerequestId.setText(taskDataBean.getService_request_id());
                customerId = taskDataBean.getCustomer_id();
                tvSiteImage.setVisibility(View.VISIBLE);
                bidId=taskDataBean.getBid_id();
                ll_requested_on.setVisibility(View.GONE);
                Latitude=taskDataBean.getLat();
                Longitude=taskDataBean.getLng();
                // tvHeader.setText(taskDataBean.getCustomer_name());
                tvName.setText(taskDataBean.getCustomer_name());
                tvPhoneNumber.setVisibility(View.VISIBLE);
                tvPhoneNumber.setText(taskDataBean.getCustomer_phone());
                tvAddress.setText(taskDataBean.getFull_address());
                tvServiceAt.setText(taskDataBean.getService_at());
                tvServiceRequired.setText(taskDataBean.getService_required());

                tvServiceType.setText(taskDataBean.getService_type());
                tvServiceTime.setText(taskDataBean.getService_timing());
                tvJobAmount.setText(taskDataBean.getPrice());
                tvJobScheduleOn.setText(setDate( taskDataBean.getDate()) + "," + taskDataBean.getSlot());
                tvComment.setText(taskDataBean.getOther_info());
                tvSiteImage.setVisibility(View.GONE);

                llJobAmount.setVisibility(View.VISIBLE);
                llScheduleOn.setVisibility(View.VISIBLE);
                tvJobAmount.setText(taskDataBean.getPrice());
                tvJobScheduleOn.setText(setDate( taskDataBean.getDate()) + "," + taskDataBean.getSlot());
                // Toast.makeText(getApplicationContext(),taskDataBean.getIs_block()+"",Toast.LENGTH_SHORT).show();
                if (taskDataBean.getIs_block()) {
                    ivblocked.setVisibility(View.GONE);
                    ivunblocked.setVisibility(View.VISIBLE);
                } else {
                    ivblocked.setVisibility(View.VISIBLE);
                    ivunblocked.setVisibility(View.GONE);
                }
                if (taskDataBean.isOn_site_present()) {
                    tvCustomerPresent.setText("Yes");
                } else {
                    tvCustomerPresent.setText("No");
                }

                if (taskDataBean.getService_type_id().equals("1")) {
                    tvServiceSize.setText(taskDataBean.getLawn_size());
                } else if(taskDataBean.getService_type_id().equals("2")) {
                    tvServiceSize.setText(taskDataBean.getGrass_size());
                }
                else
                {
                    tvServiceSize.setText(taskDataBean.getLawn_size());
                }


                if (taskDataBean.getService_type_id().equals("1")) {
                    String text=taskDataBean.getServide_needed();
                    if(!taskDataBean.getGrass_size().equals(""))
                    {
                        text=text+", "+ taskDataBean.getGrass_size();
                    }

                    tvServiceNeed.setText(text);
                } else if(taskDataBean.getService_type_id().equals("2")) {
                    String text=taskDataBean.getServide_needed();
                    if(!taskDataBean.getHow_many_tree_remove().equals(""))
                    {
                        text=text+", "+ taskDataBean.getHow_many_tree_remove();
                    }
                    if(!taskDataBean.getHow_many_tree_trim().equals(""))
                    {
                        text=text+", "+ taskDataBean.getHow_many_tree_trim();

                    }
                    Log.e("CustomerDetails",text);
                    tvServiceNeed.setText(text);
                }
                else
                {
                    tvServiceNeed.setText(taskDataBean.getServide_needed()+", "+taskDataBean.getGrass_size());
                }
                Glide.with(getApplication()).load(taskDataBean.getCustomer_profile_image())
                        .into(iv_prc);
                if (taskDataBean.getCustomer_work_image().size() == 0) {
                    tvSiteImage.setVisibility(View.GONE);
                    ll_site_image.setVisibility(View.GONE);
                } else {
                    tvSiteImage.setVisibility(View.VISIBLE);
                    ll_site_image.setVisibility(View.VISIBLE);
                }
                if (taskDataBean.getCustomer_work_image().size() == 1) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    a.add(ImageOne);
                } else if (taskDataBean.getCustomer_work_image().size() == 2) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    ImageTwo = taskDataBean.getCustomer_work_image().get(1);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                } else if (taskDataBean.getCustomer_work_image().size() == 3) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    ImageTwo = taskDataBean.getCustomer_work_image().get(1);
                    ImageThree = taskDataBean.getCustomer_work_image().get(2);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                } else if (taskDataBean.getCustomer_work_image().size() == 4) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    ImageTwo = taskDataBean.getCustomer_work_image().get(1);
                    ImageThree = taskDataBean.getCustomer_work_image().get(2);
                    ImageFour = taskDataBean.getCustomer_work_image().get(3);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                } else if (taskDataBean.getCustomer_work_image().size() == 5) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    ImageTwo = taskDataBean.getCustomer_work_image().get(1);
                    ImageThree = taskDataBean.getCustomer_work_image().get(2);
                    ImageFour = taskDataBean.getCustomer_work_image().get(3);
                    ImageFive = taskDataBean.getCustomer_work_image().get(4);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                    a.add(ImageFive);
                }


               // adapter = new TakeImageAdapterAC(a, this);
                adapter.loadList(a);
                adapter.setAdapterListener(new TakeImageAdapterAC.CellListener() {
                    @Override
                    public void onItemClick(int pos, ArrayList<String> items) {
                        String str = "";
                        if (pos == 0) {
                            str = ImageOne;
                        }
                        if (pos == 1) {
                            str = ImageTwo;
                        }
                        if (pos == 2) {
                            str = ImageThree;
                        }
                        if (pos == 3) {
                            str = ImageFour;
                        }
                        if (pos == 4) {
                            str = ImageFive;
                        }

                        Glide.with(mContext).
                                load(str)
                                //.placeholder(R.drawable.image_placeholder)
                                .into(iv_gal_img);
                        dialog.show();
                    }
                });
                adapter.notifyDataSetChanged();
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_take_image);
                LinearLayoutManager layoutManagerMakeOverBrand = new LinearLayoutManager(this);
                layoutManagerMakeOverBrand.setOrientation(LinearLayoutManager.HORIZONTAL);


                recyclerView.setLayoutManager(layoutManagerMakeOverBrand);
                recyclerView.setAdapter(adapter);

                 tvEndNote.setText(taskDataBean.getTask_end_notes());
                if(taskDataBean.getTask_images().size()>0) {

                    for (int k = 0; k < taskDataBean.getTask_images().size(); k++) {
                        arrayListPhoto.add(taskDataBean.getTask_image_url() + "/" + taskDataBean.getTask_images().get(k));
                    }


                    adapterEnd.loadList(arrayListPhoto);
                    rv_end_photo.setAdapter(adapterEnd);



                    adapterEnd.setAdapterListener(new TakeImageAdapterAC.CellListener() {
                        @Override
                        public void onItemClick(int pos, ArrayList<String> items) {
                            String str = "";
                            str=taskDataBean.getTask_image_url() + "/" + taskDataBean.getTask_images().get(pos);
                            Log.e("COMPLETE",str);
                            Glide.with(mContext).
                                    load(str)
                                    //.placeholder(R.drawable.image_placeholder)
                                    .into(iv_gal_img);
                            dialog.show();
                        }
                    });
                }
            }
            else
            {
               // Toast.makeText(getApplicationContext(),"enter else",Toast.LENGTH_SHORT).show();
                customerDetailsPresenter.onGetCustomer(taskId);
            }
        }
    }

    @Override
    public void allCancelledTasksName(List<CancelledTaskResponse.ResponseDataBean.CancelledTaskBean> arrayList) {
        a = new ArrayList<>();
        for (int i = 0; i < arrayList.size(); i++) {
            CancelledTaskResponse.ResponseDataBean.CancelledTaskBean taskDataBean = arrayList.get(i);
            if (taskId.equals(taskDataBean.getTask_id())) {
                servicerequestId.setText(taskDataBean.getService_request_id());
                customerId = taskDataBean.getCustomer_id();
                tvSiteImage.setVisibility(View.VISIBLE);
                bidId=taskDataBean.getBid_id();
                ll_requested_on.setVisibility(View.GONE);
                Latitude=taskDataBean.getLat();
                Longitude=taskDataBean.getLng();
                // tvHeader.setText(taskDataBean.getCustomer_name());
                tvName.setText(taskDataBean.getCustomer_name());
                tvPhoneNumber.setVisibility(View.VISIBLE);
                tvPhoneNumber.setText(taskDataBean.getCustomer_phone());
                tvAddress.setText(taskDataBean.getFull_address());
                tvServiceAt.setText(taskDataBean.getService_at());
                tvServiceRequired.setText(taskDataBean.getService_required());
                tvServiceType.setText(taskDataBean.getService_type());
                tvServiceTime.setText(taskDataBean.getService_timing());
                tvJobAmount.setText(taskDataBean.getPrice());
                tvJobScheduleOn.setText(setDate( taskDataBean.getDate()) + "," + taskDataBean.getSlot());
                tvComment.setText(taskDataBean.getOther_info());
                tvSiteImage.setVisibility(View.VISIBLE);

                llJobAmount.setVisibility(View.VISIBLE);
                llScheduleOn.setVisibility(View.VISIBLE);
                tvJobAmount.setText(taskDataBean.getPrice());
                tvJobScheduleOn.setText(setDate( taskDataBean.getDate()) + "," + taskDataBean.getSlot());

                if (taskDataBean.isOn_site_present()) {
                    tvCustomerPresent.setText("Yes");
                } else {
                    tvCustomerPresent.setText("No");
                }

                if (taskDataBean.isIs_block()) {
                    ivblocked.setVisibility(View.GONE);
                    ivunblocked.setVisibility(View.VISIBLE);
                } else {
                    ivblocked.setVisibility(View.VISIBLE);
                    ivunblocked.setVisibility(View.GONE);
                }

//
                if (taskDataBean.getService_type_id().equals("1")) {
                    tvServiceSize.setText(taskDataBean.getLawn_size());
                } else if(taskDataBean.getService_type_id().equals("2")) {
                    tvServiceSize.setText(taskDataBean.getGrass_size());
                }
                else
                {
                    tvServiceSize.setText(taskDataBean.getLawn_size());
                }


                if (taskDataBean.getService_type_id().equals("1")) {
                    String text=taskDataBean.getServide_needed();
                    if(!taskDataBean.getGrass_size().equals(""))
                    {
                        text=text+", "+ taskDataBean.getGrass_size();
                    }

                    tvServiceNeed.setText(text);
                } else if(taskDataBean.getService_type_id().equals("2")) {
                    String text=taskDataBean.getServide_needed();
                    if(!taskDataBean.getHow_many_tree_remove().equals(""))
                    {
                        text=text+", "+ taskDataBean.getHow_many_tree_remove();
                    }
                    if(!taskDataBean.getHow_many_tree_trim().equals(""))
                    {
                        text=text+", "+ taskDataBean.getHow_many_tree_trim();

                    }
                    Log.e("CustomerDetails",text);
                    tvServiceNeed.setText(text);
                }
                else
                {
                    tvServiceNeed.setText(taskDataBean.getServide_needed()+", "+taskDataBean.getGrass_size());
                }

                Glide.with(getApplication()).load(taskDataBean.getCustomer_profile_image())
                        .into(iv_prc);
                if (taskDataBean.getCustomer_work_image().size() == 0) {
                    tvSiteImage.setVisibility(View.GONE);
                    ll_site_image.setVisibility(View.GONE);
                } else {
                    tvSiteImage.setVisibility(View.VISIBLE);
                    ll_site_image.setVisibility(View.VISIBLE);
                }

              //  Toast.makeText(getApplicationContext(), taskDataBean.getCustomer_work_image().size() + "", Toast.LENGTH_SHORT).show();
                if (taskDataBean.getCustomer_work_image().size() == 1) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    a.add(ImageOne);
                } else if (taskDataBean.getCustomer_work_image().size() == 2) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    ImageTwo = taskDataBean.getCustomer_work_image().get(1);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                } else if (taskDataBean.getCustomer_work_image().size() == 3) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    ImageTwo = taskDataBean.getCustomer_work_image().get(1);
                    ImageThree = taskDataBean.getCustomer_work_image().get(2);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                } else if (taskDataBean.getCustomer_work_image().size() == 4) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    ImageTwo = taskDataBean.getCustomer_work_image().get(1);
                    ImageThree = taskDataBean.getCustomer_work_image().get(2);
                    ImageFour = taskDataBean.getCustomer_work_image().get(3);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                } else if (taskDataBean.getCustomer_work_image().size() == 5) {
                    ImageOne = taskDataBean.getCustomer_work_image().get(0);
                    ImageTwo = taskDataBean.getCustomer_work_image().get(1);
                    ImageThree = taskDataBean.getCustomer_work_image().get(2);
                    ImageFour = taskDataBean.getCustomer_work_image().get(3);
                    ImageFive = taskDataBean.getCustomer_work_image().get(4);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                    a.add(ImageFive);
                }


               // adapter = new TakeImageAdapterAC(a, this);
                adapter.loadList(a);
                adapter.setAdapterListener(new TakeImageAdapterAC.CellListener() {
                    @Override
                    public void onItemClick(int pos, ArrayList<String> items) {
                        String str = "";
                        if (pos == 0) {
                            str = ImageOne;
                        }
                        if (pos == 1) {
                            str = ImageTwo;
                        }
                        if (pos == 2) {
                            str = ImageThree;
                        }
                        if (pos == 3) {
                            str = ImageFour;
                        }
                        if (pos == 4) {
                            str = ImageFive;
                        }

                        Glide.with(mContext).
                                load(str)
                               // .placeholder(R.drawable.image_placeholder)
                                .into(iv_gal_img);
                        dialog.show();
                    }
                });
                adapter.notifyDataSetChanged();
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_take_image);
                LinearLayoutManager layoutManagerMakeOverBrand = new LinearLayoutManager(this);
                layoutManagerMakeOverBrand.setOrientation(LinearLayoutManager.HORIZONTAL);


                recyclerView.setLayoutManager(layoutManagerMakeOverBrand);
                recyclerView.setAdapter(adapter);

            }
            else
            {
                customerDetailsPresenter.onGetCustomer(taskId);
            }
        }
    }

    @Override
    public void onSuccessfillyBid(BidCreateResponse bidCreateResponse) {

      //  Toast.makeText(getApplicationContext(), bidCreateResponse.getResponseText(), Toast.LENGTH_SHORT).show();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
              onBackPressed();
            }
        }, 1000);

    }

    @Override
    public void onSuccessfullyRescheduleTask(RescheduleResponse rescheduleResponse) {
        if(rescheduleResponse.getResponseCode()==1) {

        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle(getResources().getString(R.string.success));
            builder.setMessage(getResources().getString(R.string.waitingforcustomerapproval));
            builder.setCancelable(false);
            builder.setPositiveButton(getResources().getString(R.string.yesA), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();


                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        } catch (Exception e) {
            Timber.tag(TAG).e(e);
        }


        }
    }

    @Override
    public void onStartTask(StartTaskResponse startTaskResponse) {
        ll_reschuedule_area.setAlpha(0.5f);
        ll_end.setVisibility(View.VISIBLE);
        ll_start.setVisibility(View.GONE);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
               // finish();
               // overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                onBackPressed();
            }
        }, 200);
    }

    @Override
    public void onSuccessfullyBlockUser(BlockCustomerResponse blockCustomerResponse) {

        if (blockCustomerResponse.getResponseCode() == 1) {
            finish();
            ivblocked.setVisibility(View.GONE);
            ivunblocked.setVisibility(View.VISIBLE);
        } else {
            ivblocked.setVisibility(View.VISIBLE);
            ivunblocked.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSuccessfullyUnBlockUser(UnBlockUserResponse unBlockUserResponse) {

        if (unBlockUserResponse.getResponseCode() == 1) {
            finish();
            ivblocked.setVisibility(View.VISIBLE);
            ivunblocked.setVisibility(View.GONE);
        } else {
            ivblocked.setVisibility(View.GONE);
            ivunblocked.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onEndTask(EndTaskResponse endTaskResponse) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
              //  finish();
              //  overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                onBackPressed();
            }
        }, 200);
    }

    public void bck(View view) {
        if(Tag.equals("Foreground")) {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }
        else if(Tag.equals("Background"))
        {
            Intent i=new Intent(CustomerDetailsActivity.this, Main2Activity.class);
            startActivity(i);
            finish();
        }
        else
        {
            finish();
            overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }
    }

    public void goSchedule(View view) {


        Intent intent = new Intent(this, ScheduleJobActivity.class);
        startActivityForResult(intent, 2);
        // overridePendingTransition( R.anim.slide_in_up, R.anim.slide_out_up );
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        if(Tag!=null) {

            if (Tag.equals("Foreground")) {
                finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            } else if (Tag.equals("Background")) {
                Intent i = new Intent(CustomerDetailsActivity.this, Main2Activity.class);
                startActivity(i);
                finish();
            } else {
                finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            }
        }
    }

    @SuppressLint("ResourceType")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2) {
            if (data != null) {

                Log.e("Schedule",Date+"- "+DateName);
                Time = data.getStringExtra("Time");
                Date = data.getStringExtra("Date");
                TimeName = data.getStringExtra("TimeName");
                DateName = data.getStringExtra("DateName");
                StartDate = data.getStringExtra("StartDate");

             //   Toast.makeText(getApplicationContext(), Time + "-" + TimeName, Toast.LENGTH_SHORT).show();
                if (!Time.equals("") && !Date.equals("")) {
                    llSchedule.setVisibility(View.GONE);
                    tvSelectedSlot.setVisibility(View.VISIBLE);
                    tvSelectedSlot.setText(  setDate(StartDate)+ ","  +TimeName);
                    ll_send_bid.setVisibility(View.VISIBLE);
                    ll_select_slot.setVisibility(View.GONE);
                    tvRescheduleSlot.setVisibility(View.VISIBLE);
                    tvRescheduleSlot.setText(  setDate(StartDate)+ ","  +TimeName);
                } else {
                    ll_send_bid.setVisibility(View.GONE);
                }


            }

        }

        if (requestCode == IMAGE_RESULT_ONE) {

            String filePath = getImageFilePath(data);

            if (filePath != null) {
                Bitmap selectedImage = BitmapFactory.decodeFile(filePath);

                ExifInterface ei = null;
                try {
                    ei = new ExifInterface(filePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);

                Bitmap rotatedBitmap = null;
                switch(orientation) {

                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotatedBitmap = rotateImage(selectedImage, 90);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotatedBitmap = rotateImage(selectedImage, 180);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotatedBitmap = rotateImage(selectedImage, 270);
                        break;

                    case ExifInterface.ORIENTATION_NORMAL:
                    default:
                        rotatedBitmap = selectedImage;
                }
                squareImageView.setImageBitmap(rotatedBitmap);
                tvimgOne.setText("1");

                    upload_file_me_one = new File(getImageFromFilePath(data));

            }


        }
        else if(requestCode == IMAGE_RESULT_TWO){
            String filePath = getImageFilePath(data);
            if (filePath != null) {
                Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
                ExifInterface ei = null;
                try {
                    ei = new ExifInterface(filePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);

                Bitmap rotatedBitmap = null;
                switch(orientation) {

                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotatedBitmap = rotateImage(selectedImage, 90);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotatedBitmap = rotateImage(selectedImage, 180);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotatedBitmap = rotateImage(selectedImage, 270);
                        break;

                    case ExifInterface.ORIENTATION_NORMAL:
                    default:
                        rotatedBitmap = selectedImage;
                }
                squareImageViewOne.setImageBitmap(rotatedBitmap);
                tvimgTwo.setText("1");
                upload_file_me_two = new File(getImageFromFilePath(data));
            }
        }
        else if(requestCode == IMAGE_RESULT_THREE){
            String filePath = getImageFilePath(data);
            if (filePath != null) {
                Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
                ExifInterface ei = null;
                try {
                    ei = new ExifInterface(filePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);

                Bitmap rotatedBitmap = null;
                switch(orientation) {

                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotatedBitmap = rotateImage(selectedImage, 90);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotatedBitmap = rotateImage(selectedImage, 180);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotatedBitmap = rotateImage(selectedImage, 270);
                        break;

                    case ExifInterface.ORIENTATION_NORMAL:
                    default:
                        rotatedBitmap = selectedImage;
                }
                squareImageViewTwo.setImageBitmap(rotatedBitmap);
                tvimgThree.setText("1");
                upload_file_me_three = new File(getImageFromFilePath(data));
            }
        }
        else if(requestCode == IMAGE_RESULT_FOUR){
            String filePath = getImageFilePath(data);
            if (filePath != null) {
                Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
                ExifInterface ei = null;
                try {
                    ei = new ExifInterface(filePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);

                Bitmap rotatedBitmap = null;
                switch(orientation) {

                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotatedBitmap = rotateImage(selectedImage, 90);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotatedBitmap = rotateImage(selectedImage, 180);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotatedBitmap = rotateImage(selectedImage, 270);
                        break;

                    case ExifInterface.ORIENTATION_NORMAL:
                    default:
                        rotatedBitmap = selectedImage;
                }
                squareImageViewThree.setImageBitmap(rotatedBitmap);
                tvimgFour.setText("1");
                upload_file_me_four = new File(getImageFromFilePath(data));
            }
        }
        else if(requestCode == IMAGE_RESULT_FIVE){
            String filePath = getImageFilePath(data);
            if (filePath != null) {
                Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
                ExifInterface ei = null;
                try {
                    ei = new ExifInterface(filePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED);

                Bitmap rotatedBitmap = null;
                switch(orientation) {

                    case ExifInterface.ORIENTATION_ROTATE_90:
                        rotatedBitmap = rotateImage(selectedImage, 90);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_180:
                        rotatedBitmap = rotateImage(selectedImage, 180);
                        break;

                    case ExifInterface.ORIENTATION_ROTATE_270:
                        rotatedBitmap = rotateImage(selectedImage, 270);
                        break;

                    case ExifInterface.ORIENTATION_NORMAL:
                    default:
                        rotatedBitmap = selectedImage;
                }
                squareImageViewFour.setImageBitmap(rotatedBitmap);
                tvimgFive.setText("1");
                upload_file_me_five = new File(getImageFromFilePath(data));
            }
        }
    }

    @OnClick({R.id.squareImageView,R.id.squareImageViewOne,R.id.squareImageViewTwo,R.id.squareImageViewThree,R.id.squareImageViewFour,R.id.ivCrossOne,R.id.ivCrossTwo,R.id.ivCrossThree,R.id.ivCrossFour,R.id.ivCrossFive})
    public void onClick(View v) {

        if(v.getId()==R.id.squareImageView)
        {
            startActivityForResult(getPickImageChooserIntent(), IMAGE_RESULT_ONE);
            ivCrossOne.setVisibility(View.VISIBLE);
        }
        else if(v.getId()==R.id.squareImageViewOne)
        {
            startActivityForResult(getPickImageChooserIntent(), IMAGE_RESULT_TWO);
            ivCrossTwo.setVisibility(View.VISIBLE);
        }
        else if(v.getId()==R.id.squareImageViewTwo)
        {

            startActivityForResult(getPickImageChooserIntent(), IMAGE_RESULT_THREE);
            ivCrossThree.setVisibility(View.VISIBLE);
        }
        else if(v.getId()==R.id.squareImageViewThree) {
            startActivityForResult(getPickImageChooserIntent(), IMAGE_RESULT_FOUR);
            ivCrossFour.setVisibility(View.VISIBLE);
        }
        else if(v.getId()==R.id.squareImageViewFour)
        {
            startActivityForResult(getPickImageChooserIntent(), IMAGE_RESULT_FIVE);
            ivCrossFive.setVisibility(View.VISIBLE);
        }

        else if(v.getId()==R.id.ivCrossOne)
        {
            squareImageView.setImageResource(R.drawable.image_placeholder);
            ivCrossOne.setVisibility(View.GONE);
        }
        else if(v.getId()==R.id.ivCrossTwo)
        {
            squareImageViewOne.setImageResource(R.drawable.image_placeholder);
            ivCrossTwo.setVisibility(View.GONE);
        }
        else if(v.getId()==R.id.ivCrossThree)
        {
            squareImageViewTwo.setImageResource(R.drawable.image_placeholder);
            ivCrossThree.setVisibility(View.GONE);
        }
        else if(v.getId()==R.id.ivCrossFour)
        {
            squareImageViewThree.setImageResource(R.drawable.image_placeholder);
            ivCrossFour.setVisibility(View.GONE);
        }
        else if(v.getId()==R.id.ivCrossFive)
        {
            squareImageViewFour.setImageResource(R.drawable.image_placeholder);
            ivCrossFive.setVisibility(View.GONE);
        }
    }

    public Intent getPickImageChooserIntent() {

        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalFilesDir("");
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "profile.png"));
        }
        return outputFileUri;
    }

    public String setDate(String date)
    {
        String dayName="";

        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();

        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        fmt.setTimeZone(tz);
        java.util.Date date1 = null;
        try {
            date1 = fmt.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        cal.setTime(date1);


        switch (cal. get(Calendar. DAY_OF_WEEK))
        {
            case 1:

                if(dataManager.getLocalValue().equals("en"))
                dayName="Sunday";
                else if(dataManager.getLocalValue().equals("fr"))
                    dayName="dimanche";
                break;
            case 2:
                if(dataManager.getLocalValue().equals("en"))
                    dayName="Monday";
                else if(dataManager.getLocalValue().equals("fr"))
                    dayName="Lundi";
               // dayName="Monday";
                break;
            case 3:
                if(dataManager.getLocalValue().equals("en"))
                    dayName="Tuesday";
                else if(dataManager.getLocalValue().equals("fr"))
                    dayName="Mardi";
               // dayName="Tuesday";
                break;
            case 4:
                if(dataManager.getLocalValue().equals("en"))
                    dayName="Wednesday";
                else if(dataManager.getLocalValue().equals("fr"))
                    dayName="Mercredi";
               // dayName="Wednesday";
                break;
            case 5:
                if(dataManager.getLocalValue().equals("en"))
                    dayName="Thursday";
                else if(dataManager.getLocalValue().equals("fr"))
                    dayName="Jeudi";
               // dayName="Thursday";
                break;
            case 6:
                if(dataManager.getLocalValue().equals("en"))
                    dayName="Friday";
                else if(dataManager.getLocalValue().equals("fr"))
                    dayName="Jeudi";
               // dayName="Friday";
                break;
            case 7:
                if(dataManager.getLocalValue().equals("en"))
                    dayName="Saturday";
                else if(dataManager.getLocalValue().equals("fr"))
                    dayName="samedi";
               // dayName="Saturday";
                break;

        }

        String[]word=date.split("-");
        String finaldate="",month="";
        switch (word[1])
        {
            case "01":
                month="Jan";
                break;
            case "02":
                month="Feb";
                break;
            case "03":
                month="Mar";
                break;
            case "04":
                month="Apr";
                break;
            case "05":
                month="May";
                break;
            case "06":
                month="Jun";
                break;
            case "07":
                month="Jul";
                break;
            case "08":
                month="Aug";
                break;
            case "09":
                month="Sep";
                break;
            case "10":
                month="Oct";
                break;
            case "11":
                month="Nov";
                break;
            case "12":
                month="Dec";
                break;

        }
        finaldate=dayName+", "+word[2]+" "+month+" "+word[0];

        return finaldate;
    }

    private String getImageFromFilePath(Intent data) {
        boolean isCamera = data == null || data.getData() == null;

        if (isCamera) return getCaptureImageOutputUri().getPath();
        else return getPathFromURI(data.getData());

    }

    public String getImageFilePath(Intent data) {
        return getImageFromFilePath(data);
    }

    private String getPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private  File persistImage(Bitmap bitmap, String name) {

        File filesDir =getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");

//Convert bitmap to byte array
        Bitmap bitmaps = bitmap;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmaps.compress(Bitmap.CompressFormat.JPEG, 50 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imageFile);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imageFile;
    }

    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    public boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


                                                requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(CustomerDetailsActivity.this)
                .setMessage(message)
                .setPositiveButton(getResources().getString(R.string.yesA), okListener)
                .setNegativeButton(getResources().getString(R.string.noA), null)
                .create()
                .show();
    }

    class Radio_check implements CompoundButton.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (rb_reschedule_no.isChecked()) {
                ll_start.setVisibility(View.VISIBLE);
                ll_hide_show_reschuedule_area.startAnimation(animHide);
                ll_hide_show_reschuedule_area.setVisibility(View.GONE);
            } else if (rb_reschedule_ys.isChecked()) {
                ll_start.setVisibility(View.GONE);
                ll_hide_show_reschuedule_area.setVisibility(View.VISIBLE);
                ll_hide_show_reschuedule_area.startAnimation(animShow);

            }
            else if(rb_no_photo.isChecked())
            {
                llPhoto.startAnimation(animHide);
                llPhoto.setVisibility(View.GONE);

            }
            else if(rb_yes_photo.isChecked())
            {
                llPhoto.setVisibility(View.VISIBLE);
                llPhoto.startAnimation(animHide);

            }
        }
    }

    class Radio_check_One implements CompoundButton.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

             if(rb_no_photo.isChecked())
            {
                llPhoto.startAnimation(animHide);
                llPhoto.setVisibility(View.GONE);

            }
            else if(rb_yes_photo.isChecked())
            {
                llPhoto.setVisibility(View.VISIBLE);
                llPhoto.startAnimation(animShow);

            }
        }
    }
}
