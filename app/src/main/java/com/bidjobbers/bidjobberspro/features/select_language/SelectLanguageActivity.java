package com.bidjobbers.bidjobberspro.features.select_language;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.SetLocal.SetLocalResponse;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectLanguageActivity extends BaseActivity implements SelectLanguageMvpView,View.OnClickListener{

    //iv_french_tick , iv_english_tick
    //ll_english_tick, ll_french_tick
    //isEnglish is_french

    ImageView iv_french_tick , iv_english_tick;
    LinearLayout ll_english_tick, ll_french_tick;
    boolean isEnglish=false,is_french=false;
    Button btn_next;
    String tag,lang;
    @BindView(R.id.ivBack)
    ImageView ivBack;

    String currentLanguage = "";
    Locale myLocale;

    SharedPreferences sharedpreferences;

    @Inject
    SelectLanguagePresenter<SelectLanguageMvpView> languagePresenter;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_selection);
        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        languagePresenter.onAttach(this);

        iv_french_tick = (ImageView) findViewById(R.id.iv_french_tick);
        iv_english_tick =  (ImageView) findViewById(R.id.iv_english_tick);

        ll_english_tick =  (LinearLayout) findViewById(R.id.ll_english_tick);
        ll_french_tick =  (LinearLayout) findViewById(R.id.ll_french_tick);
        btn_next =  (Button) findViewById(R.id.btn_next);

        iv_french_tick.setBackgroundResource(R.drawable.ic_uncheck);
        iv_english_tick.setBackgroundResource(R.drawable.ic_uncheck);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SelectLanguageActivity.this);
        lang=prefs.getString("LOCAL_KEY","");

        if(lang.equals("en"))
        {
            iv_french_tick.setBackgroundResource(R.drawable.ic_uncheck);
            iv_english_tick.setBackgroundResource(R.drawable.ic_check);
        }
        else if(lang.equals("fr"))
        {
            iv_french_tick.setBackgroundResource(R.drawable.ic_check);
            iv_english_tick.setBackgroundResource(R.drawable.ic_uncheck);
        }
        else
        {
            iv_french_tick.setBackgroundResource(R.drawable.ic_uncheck);
            iv_english_tick.setBackgroundResource(R.drawable.ic_check);
        }

    //    Toast.makeText(getApplicationContext(),lang,Toast.LENGTH_SHORT).show();
// else if(lang.equals("fr"))

        btn_next.
        setOnClickListener(
            new View.OnClickListener()
            {
              @Override
              public void onClick(View v)
              {


               if(isEnglish)
               {
                   setLocale("en");
                   languagePresenter.getSetLocal("en");
                   SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SelectLanguageActivity.this);
                   prefs.edit().putString("LOCAL_KEY", "en").commit();
                  // languagePresenter.setLanguage("en");



               }
               else if (is_french)
               {
                   setLocale("fr");
                   languagePresenter.getSetLocal("fr");
                   SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SelectLanguageActivity.this);
                   prefs.edit().putString("LOCAL_KEY", "en").commit();
                 //  languagePresenter.setLanguage("fr");

               }
               else
               {
                   setLocale("en");
                   languagePresenter.getSetLocal("en");
                   SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SelectLanguageActivity.this);
                   prefs.edit().putString("LOCAL_KEY", "en").apply();
                 //  languagePresenter.setLanguage("en");
               }
              }
            }
           );



        ll_english_tick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    iv_french_tick.setBackgroundResource(R.drawable.ic_uncheck);
                    iv_english_tick.setBackgroundResource(R.drawable.ic_check);

                    isEnglish = true;
                    is_french = false;
            }
        });
        ll_french_tick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                    iv_french_tick.setBackgroundResource(R.drawable.ic_check);
                    iv_english_tick.setBackgroundResource(R.drawable.ic_uncheck);

                    isEnglish = false;
                    is_french = true;
            }
        });

        getIntentdata();
       // setLocale("en");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
              //  ll_english_tick.performClick();
            }
        }, 500);

    }



    private void getIntentdata()
    {
        tag=getIntent().getStringExtra("tag");
        if(tag.equals("Update"))
        {
            ivBack.setVisibility(View.VISIBLE);
            btn_next.setText(getResources().getString(R.string.SUBMIT));
        }
        else
        {
            ivBack.setVisibility(View.GONE);
            btn_next.setText(getResources().getString(R.string.NEXT));
        }
    }

    public void setLocale(String localeName) {

        dataManager.setLocalValue(lang);

        if (!localeName.equals(currentLanguage)) {

            Log.e("Language",localeName);

            myLocale = new Locale(localeName);
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();

            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);

            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run()
                {


//                    if(tag.equals("Update"))
//                    {
//                        onBackPressed();
//                    }
//                    else
//                    {
                        Intent i = new Intent(SelectLanguageActivity.this, LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                  //  }


                }
            }, 500);

        } else {
            Toast.makeText(this, "Language already selected!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void successfullySEtLocal(SetLocalResponse setLocalResponse) {

    }

    @Override
    public void getLanguage() {

    }

    @OnClick(R.id.ivBack)
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.ivBack:
               // onBackPressed();
                finish();
        }
    }
}
