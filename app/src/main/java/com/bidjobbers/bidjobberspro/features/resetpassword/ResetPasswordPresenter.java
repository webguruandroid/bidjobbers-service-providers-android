package com.bidjobbers.bidjobberspro.features.resetpassword;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.resetpassword.ResetPasswordRequest;
import com.bidjobbers.bidjobberspro.data.network.models.resetpassword.ResetPasswordResponse;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class ResetPasswordPresenter<V extends ResetPasswordMvpView> extends BasePresenter<V> implements ResetPasswordMvpPresenter<V> {

    @Inject
    public ResetPasswordPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable)
    {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void changePassWord(String oldPass, String newPass, String confPass, String email, String otp) {

       /* if(oldPass.trim().isEmpty())
        {
         getMvpView().showMessage("Old Password should not be empty");
         return;
        }*/
        if(newPass.trim().isEmpty())
        {
         getMvpView().showMessage("New Password should not be empty");
         return;
        }
        if(confPass.trim().isEmpty())
        {
         getMvpView().showMessage("Confirm Password should not be empty");
         return;
        }

        if(newPass.length() < 6)
        {
            getMvpView().showMessage("New Password length should be 6");
            return;
        }
        /*if(newPass.equals(oldPass))
        {
            getMvpView().showMessage("New Password and old Password can not same");
            return;
        }*/
        if(!newPass.equals(confPass))
        {
            getMvpView().showMessage("New Password and Confirm Password should be same");
            return;
        }
       //resetPassword

        ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest( email,  newPass,  otp,  getDataManager().getLocalValue());

        getCompositeDisposable().add(
                getDataManager().resetPassword(resetPasswordRequest)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<ResetPasswordResponse>()
                        {
                            @Override
                            public void onSuccess(ResetPasswordResponse testResponse)
                            {
                                Log.v("onSuccess",testResponse.getResponseCode()+"");
                                if(testResponse.getResponseCode()==1)
                                {
                                   // getDataManager().setAccess_token(testResponse.getResponseData().getAuthorization().getAccess_token());
                                    getMvpView().showMessage(testResponse.getResponseText());
                                    getMvpView().goToLoginScreen();
                                }
                                else if(testResponse.getResponseCode()==401)
                                {
                                    getMvpView().onError(testResponse.getResponseText());
                                    getMvpView().doDeActivate(testResponse.getResponseText());
                                }
                                else
                                {
                                    getMvpView().showMessage(testResponse.getResponseText());
                                }

                            }
                            @Override
                            public void onError(Throwable e) {

                                Log.e("ERROR", "onError: "+e.getMessage());

                            }

                        }));

    }
}
