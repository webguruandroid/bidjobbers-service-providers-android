package com.bidjobbers.bidjobberspro.features.resetpasswordfromsettings;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.ChangeaPassword.ChangePasswordResponse;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResetPasswordActivity extends BaseActivity implements ResetPasswordMvpView {


    @BindView(R.id.iv_mnu)
    ImageView iv_mnu;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.input_currentpass)
    EditText input_currentpass;
    @BindView(R.id.input_newpass)
    EditText input_newpass;
    @BindView(R.id.input_confpassword)
    EditText input_confpassword;


    @Inject
    ResetPasswordPresenter<ResetPasswordMvpView>presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password2);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);

        presenter.onAttach(this);


        iv_mnu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                presenter.onChagnePassword(input_currentpass.getText().toString().trim(),input_newpass.getText().toString().trim(),input_confpassword.getText().toString().trim());
            }
        });

    }


    @Override
    public void successfullChangePassword(ChangePasswordResponse changePasswordResponse) {
       // overridePendingTransition(R.anim.nothing, R.anim.slide_out);

        if(changePasswordResponse.getResponseCode()==1)
        {
            input_confpassword.setText("");
            input_currentpass.setText("");
            input_newpass.setText("");
        }
    }
}
