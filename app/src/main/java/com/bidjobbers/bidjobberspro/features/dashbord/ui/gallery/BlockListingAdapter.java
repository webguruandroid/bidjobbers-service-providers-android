package com.bidjobbers.bidjobberspro.features.dashbord.ui.gallery;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.Archived.ArchiveJobList;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList.BlockCustomerListResponse;
import com.bidjobbers.bidjobberspro.features.NewArchive.NewArchiveAdapter;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.OnLoadMoreListener;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BlockListingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    public AddButtonListener mListener;
    ArrayList<BlockCustomerListResponse.ResponseDataBean.BlocklistBean> mValues;
    Context mContext;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading;
    private int visibleThreshold = 3;
    private int lastVisibleItem, totalItemCount;
    private OnLoadMoreListener mOnLoadMoreListener;


    public BlockListingAdapter(ArrayList<BlockCustomerListResponse.ResponseDataBean.BlocklistBean> items , Context context, RecyclerView mRecyclerView)
    {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                Log.e("load",totalItemCount+""+lastVisibleItem);

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                        Log.e("load",totalItemCount+" "+lastVisibleItem+" "+visibleThreshold);
                    }
                    isLoading = true;
                }
            }
        });
        mValues = items;
        mContext = context;



    }

    public void loadList(List<BlockCustomerListResponse.ResponseDataBean.BlocklistBean> items) {
        Log.e("Task",items.size()+"");
//        mValues.clear();
//        mValues.addAll(items);
//        notifyDataSetChanged();
    }

    public void setLoaded(){
        this.isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return mValues.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
//        if (isLoading) {
//            return mValues.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
//        } else {
//            return VIEW_TYPE_ITEM;
//        }
    }

    public void setmListener(AddButtonListener mListener) {
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public  BlockListingAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_blocked_item, parent, false);
        return new BlockListingAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder,final int i) {

        if (holder instanceof BlockListingAdapter.ViewHolder) {
            BlockListingAdapter.ViewHolder mViewHolder = (BlockListingAdapter.ViewHolder) holder;

       final BlockCustomerListResponse.ResponseDataBean.BlocklistBean mDataBean = mValues.get(i);


            mViewHolder.tvStatus.setText("Blocked");
            mViewHolder.tvName.setText(mDataBean.getCustomer_name());
            mViewHolder.tvEmail.setText(mDataBean.getCustomer_email());
            mViewHolder.tvPhoneNumber.setText(mDataBean.getCustomer_phone());
        Glide.with(mContext).load(mDataBean.getCustomer_profile_image())
                .into(mViewHolder.iv_prc);
            mViewHolder.tvtaskId.setText(mDataBean.getCustomer_id());

            mViewHolder.llblocked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(i,mDataBean.getCustomer_id());
            }
        });

        }else if (holder instanceof BlockListingAdapter.LoadingViewHolder) {
            BlockListingAdapter.LoadingViewHolder loadingViewHolder = (BlockListingAdapter.LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {


        return mValues.size();
    }


    public interface AddButtonListener
    {
         void onItemClick(int pos, String taskId);

    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressbar)
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvName)
        TextView tvName;
        @BindView(R.id.tvPhoneNumber)
        TextView tvPhoneNumber;
        @BindView(R.id.tvEmail)
        TextView tvEmail;
        @BindView(R.id.tvStatus)
        TextView tvStatus;
        @BindView(R.id.tvtaskId)
        TextView tvtaskId;

        @BindView(R.id.iv_prc)
        ImageView iv_prc;
        @BindView(R.id.ll_main)
        LinearLayout ll_main;
        @BindView(R.id.llblocked)
        LinearLayout llblocked;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);

        }

    }
}