package com.bidjobbers.bidjobberspro.features.InApp;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.subscription.SubscriptionResponse;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InAppActivity extends BaseActivity implements View.OnClickListener,InAppMvpView,PurchasesUpdatedListener, SkuDetailsResponseListener {


    public static final String TAG = "InAppActivity";
    // static final String ITEM_SKU = "android.test.purchased";
    static final String ITEM_SKU = "basic_plan";
    public MutableLiveData<List<Purchase>> purchases = new MutableLiveData<>();
    public SingleLiveEvent<List<Purchase>> purchaseUpdateEvent = new SingleLiveEvent<>();
    public MutableLiveData<Map<String, SkuDetails>> skusWithSkuDetails = new MutableLiveData<>();
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.rv_plan)
    RecyclerView rv_plan;
    @Inject
    ScbscriptionAdapter adapter;
    Button inapp,inapp_second;
    List skuDetailsList;
    TextView tvBasicPlan,tvPremiumPlan;
    @Inject
    InAppPresenter<InAppMvpView> presenter;
    private BillingClient billingClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_app);
        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        presenter.onAttach(this);
        presenter.getSubscriptionList();
        setRvAdapter();

        billingClient = BillingClient.newBuilder(InAppActivity.this)
                .enablePendingPurchases().setListener(this).build();

        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    Log.e(TAG, "set up finish");
                    querySkuDetails();

                    //  Buyitem(ITEM_SKU);
                    // The BillingClient is ready. You can query purchases here.
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                Log.e(TAG, "set up finish");
            }
        });
//        inapp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                 // queryPurchases();
//                Buyitem("basic_plan");
//
//            }
//        });

//        inapp_second.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Buyitem("premium_plan");
//            }
//        });
    }

    public void setRvAdapter()
    {
        rv_plan.setLayoutManager(new LinearLayoutManager(this));
        rv_plan.setAdapter(adapter);
        adapter.setAdapterListner(new ScbscriptionAdapter.NotificationListner() {
            @Override
            public void onItemClick(String skuId) {
                Buyitem(skuId);
            }
        });
    }
    public void querySkuDetails() {
        Log.e(TAG, "querySkuDetails");

        List<String> skus = new ArrayList<>();
         skus.add("basic_plan");
         skus.add("premium_plan");

        SkuDetailsParams params = SkuDetailsParams.newBuilder()
                .setType(BillingClient.SkuType.SUBS)
                .setSkusList(skus)
                .build();

        Log.e(TAG, "querySkuDetailsAsync");
        billingClient.querySkuDetailsAsync(params, this);
    }


    public void queryPurchases() {
        if (!billingClient.isReady()) {
            Log.e(TAG, "queryPurchases: BillingClient is not ready");
        }

        Purchase.PurchasesResult result = billingClient.queryPurchases(BillingClient.SkuType.SUBS);
        Log.e(TAG, "queryPurchases: SUBS"+result.getPurchasesList());
        if (result == null) {
            Log.i(TAG, "queryPurchases: null purchase result");
            processPurchases(null);
        } else {
            if (result.getPurchasesList() == null) {
                Log.i(TAG, "queryPurchases: null purchase list");
                processPurchases(null);
            } else {
                processPurchases(result.getPurchasesList());
            }
        }
    }

    private boolean isUnchangedPurchaseList(List<Purchase> purchasesList) {
        // TODO: Optimize to avoid updates with identical data.
        return false;
    }

    private void processPurchases(List<Purchase> purchasesList) {
        if (purchasesList != null) {
            Log.e(TAG, "processPurchases: " + purchasesList.size() + " purchase(s)");
        } else {
            Log.e(TAG, "processPurchases: with no purchases");
        }
        if (isUnchangedPurchaseList(purchasesList)) {
            Log.e(TAG, "processPurchases: Purchase list has not changed");
            return;
        }
        purchaseUpdateEvent.postValue(purchasesList);
        purchases.postValue(purchasesList);
        if (purchasesList != null) {
            logAcknowledgementStatus(purchasesList);
        }
    }

    private void logAcknowledgementStatus(List<Purchase> purchasesList) {
        int ack_yes = 0;
        int ack_no = 0;
        for (Purchase purchase : purchasesList) {
            if (purchase.isAcknowledged()) {
                ack_yes++;
            } else {
                ack_no++;
            }
        }
        Log.e(TAG, "logAcknowledgementStatus: acknowledged=" + ack_yes +
                " unacknowledged=" + ack_no);
    }

    public void Buyitem(String ITEM_SKU) {

        Log.e(TAG, "Buyitem: ");
        skuDetailsList = new ArrayList<String>();
        skuDetailsList.add(ITEM_SKU);
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuDetailsList).setType(BillingClient.SkuType.SUBS);

        billingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
            @Override
            public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> skuDetailsList) {
                if (skuDetailsList.isEmpty()) {
                    Toast.makeText(InAppActivity.this, "Cant fetch sku detail", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    BillingFlowParams flowParams = BillingFlowParams.newBuilder().setSkuDetails((SkuDetails) skuDetailsList.get(0)).build();
                    Log.e(TAG, "onSkuDetailsResponse: " + flowParams);
                    BillingResult respCode = billingClient.launchBillingFlow(InAppActivity.this, flowParams);
                    Log.e("launchPurchaseFlow", "onSkuDetailsResponse: " + respCode);
                }

            }
        });
    }

    @Override
    public void onPurchasesUpdated(BillingResult responseCode, @Nullable List<Purchase> purchases) {
        Log.e(TAG, "onPurchasesUpdated: purchases " + purchases);
        //Log.e(TAG, "onPurchasesUpdated: purchases "+purchases.get(0).getSku()+" "+ITEM_SKU );
        if (responseCode.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED && purchases != null) {

            for (Purchase purchase : purchases) {


                Log.e(TAG, "onPurchasesUpdated: purchaseToken " + purchase);
                break;

            }
        } else if (responseCode.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
            // Handle an error caused by a user cancelling the purchase flow.

        } else {
            // Handle any other error codes.

            switch (responseCode.getResponseCode()) {
                case BillingClient.BillingResponseCode.FEATURE_NOT_SUPPORTED:
                    Toast.makeText(InAppActivity.this, "Requested feature is not supported by Play Store on the current device.", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    break;
                case BillingClient.BillingResponseCode.SERVICE_DISCONNECTED:
                    Toast.makeText(InAppActivity.this, "Play Store service is not connected now - potentially transient state.", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    break;
                case BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE:
                    Toast.makeText(InAppActivity.this, "Network connection is down", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    break;
                case BillingClient.BillingResponseCode.BILLING_UNAVAILABLE:
                    Toast.makeText(InAppActivity.this, "Billing API version is not supported for the type requested ", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    break;
                case BillingClient.BillingResponseCode.ITEM_UNAVAILABLE:
                    Toast.makeText(InAppActivity.this, "Requested product is not available for purchase", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    break;
                case BillingClient.BillingResponseCode.DEVELOPER_ERROR:
                    Toast.makeText(InAppActivity.this, "DEVELOPER_ERROR", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    break;
                case BillingClient.BillingResponseCode.ERROR:
                    Toast.makeText(InAppActivity.this, "Fatal error during the API action", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    break;
                case BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED:


                    break;
                case BillingClient.BillingResponseCode.ITEM_NOT_OWNED:
                   // Toast.makeText(InAppActivity.this, "Failure to consume since item is not owned", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                    break;

                default:
                    Toast.makeText(InAppActivity.this, "Success : " + responseCode, Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> skuDetailsList) {
        if (billingResult == null) {
            Log.wtf(TAG, "onSkuDetailsResponse: null BillingResult");
            return;
        }

        int responseCode = billingResult.getResponseCode();
        String debugMessage = billingResult.getDebugMessage();
        switch (responseCode) {
            case BillingClient.BillingResponseCode.OK:
                Log.e(TAG, "onSkuDetailsResponse: " + responseCode + " message-" + skuDetailsList.size());
                if (skuDetailsList == null) {
                    Log.e(TAG, "onSkuDetailsResponse: null SkuDetails list");
                    skusWithSkuDetails.postValue(Collections.<String, SkuDetails>emptyMap());
                } else {
                    Map<String, SkuDetails> newSkusDetailList = new HashMap<String, SkuDetails>();
                    for (SkuDetails skuDetails : skuDetailsList) {
                        newSkusDetailList.put(skuDetails.getSku(), skuDetails);
                        if(skuDetails.getSku().equals("basic_plan")) {
                           // tvBasicPlan.setText(skuDetails.getPrice() + "\n" + skuDetails.getSku() + "\n" + skuDetails.getSubscriptionPeriod() + "\n" + skuDetails.getDescription()+"\n"+skuDetails.getFreeTrialPeriod()+"\n"+skuDetails.getTitle()+"\n"+skuDetails.getType()+"\n"+skuDetails.getOriginalJson());
                            Log.e(TAG, "onSkuDetailsResponse: data " +skuDetails.getOriginalJson()+skuDetails.getSku()+skuDetails.getSubscriptionPeriod()+skuDetails.getDescription());
                        }
                        else if(skuDetails.getSku().equals("premium_plan"))
                        {
                          //  tvPremiumPlan.setText(skuDetails.getPrice() + "\n" + skuDetails.getSku() + "\n" + skuDetails.getSubscriptionPeriod() + "\n" + skuDetails.getDescription());
                        }
                        Log.e(TAG, "onSkuDetailsResponse: data " +skuDetails.getPrice()+skuDetails.getSku()+skuDetails.getSubscriptionPeriod()+skuDetails.getDescription());


//                        inapp
//                        BillingFlowParams flowParams = BillingFlowParams.newBuilder().setSkuDetails((SkuDetails) skuDetailsList.get(0)).build();
//                        Log.e(TAG, "onSkuDetailsResponse: " + flowParams);
//                        BillingResult respCode = billingClient.launchBillingFlow(InAppActivity.this, flowParams);
//                        Log.e(TAG, "onSkuDetailsResponse: count " + newSkusDetailList.size()+"--"+respCode.getResponseCode());
                    }
                    skusWithSkuDetails.postValue(newSkusDetailList);
                    Log.e(TAG, "onSkuDetailsResponse: count " + newSkusDetailList.size()+"--"+responseCode);
                }
                break;
            case BillingClient.BillingResponseCode.SERVICE_DISCONNECTED:
            case BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE:
            case BillingClient.BillingResponseCode.BILLING_UNAVAILABLE:
            case BillingClient.BillingResponseCode.ITEM_UNAVAILABLE:
            case BillingClient.BillingResponseCode.DEVELOPER_ERROR:
            case BillingClient.BillingResponseCode.ERROR:
                Log.e(TAG, "onSkuDetailsResponse: " + responseCode + " " + debugMessage);
                break;
            case BillingClient.BillingResponseCode.USER_CANCELED:
                Log.i(TAG, "onSkuDetailsResponse: " + responseCode + " " + debugMessage);
                break;
            // These response codes are not expected.
            case BillingClient.BillingResponseCode.FEATURE_NOT_SUPPORTED:
            case BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED:
            case BillingClient.BillingResponseCode.ITEM_NOT_OWNED:
            default:
                Log.wtf(TAG, "onSkuDetailsResponse: " + responseCode + " " + debugMessage);
        }
    }

    @OnClick(R.id.iv_back)
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.iv_back:
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void successfullyFetchInAppList(SubscriptionResponse subscriptionResponse) {


        if(subscriptionResponse.getResponseCode()==1)
        {
            adapter.loadList(subscriptionResponse.getResponseData().getSubscribe_package());
        }
    }
}
