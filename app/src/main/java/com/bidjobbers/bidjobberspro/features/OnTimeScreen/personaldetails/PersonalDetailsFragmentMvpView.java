package com.bidjobbers.bidjobberspro.features.OnTimeScreen.personaldetails;

import com.bidjobbers.bidjobberspro.data.network.models.UpdateProfile.UpdateProfileResponse;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface PersonalDetailsFragmentMvpView extends BaseFragmentMvpView {

    void onDeactivate(String data);
    void successfullyGetProfile(getProfileResponse getProfileResponse);
    void successfullyUpdateProfile(UpdateProfileResponse updateProfileResponse);
}
