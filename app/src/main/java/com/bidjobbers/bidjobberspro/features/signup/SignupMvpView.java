package com.bidjobbers.bidjobberspro.features.signup;

import com.bidjobbers.bidjobberspro.data.network.models.register.RegisterResponse;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface SignupMvpView extends MvpView {

     void actionSignUpButtonClick(String data,boolean isError);
     void actionDeactivateAccount(RegisterResponse response);

}
