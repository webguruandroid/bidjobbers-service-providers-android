package com.bidjobbers.bidjobberspro.features.OnTimeScreen.ImageDataFragment;

import java.io.File;

public class ImagePickerObject {

    File fileOne,fileTwo,fileThree,fileFour,fileFive;
    String isSelected,isEmpty;

    public ImagePickerObject(File fileOne, File fileTwo, File fileThree, File fileFour, File fileFive, String isSelected,String isEmpty) {
        this.fileOne = fileOne;
        this.fileTwo = fileTwo;
        this.fileThree = fileThree;
        this.fileFour = fileFour;
        this.fileFive = fileFive;
        this.isSelected = isSelected;
        this.isEmpty=isEmpty;
    }

    public String getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(String isSelected) {
        this.isSelected = isSelected;
    }

    public String getIsEmpty() {
        return isEmpty;
    }

    public void setIsEmpty(String isEmpty) {
        this.isEmpty = isEmpty;
    }

    public File getFileOne() {
        return fileOne;
    }

    public void setFileOne(File fileOne) {
        this.fileOne = fileOne;
    }

    public File getFileTwo() {
        return fileTwo;
    }

    public void setFileTwo(File fileTwo) {
        this.fileTwo = fileTwo;
    }

    public File getFileThree() {
        return fileThree;
    }

    public void setFileThree(File fileThree) {
        this.fileThree = fileThree;
    }

    public File getFileFour() {
        return fileFour;
    }

    public void setFileFour(File fileFour) {
        this.fileFour = fileFour;
    }

    public File getFileFive() {
        return fileFive;
    }

    public void setFileFive(File fileFive) {
        this.fileFive = fileFive;
    }

    public String getWork() {
        return isSelected;
    }

    public void setWork(String isSelected) {
        this.isSelected = isSelected;
    }
}
