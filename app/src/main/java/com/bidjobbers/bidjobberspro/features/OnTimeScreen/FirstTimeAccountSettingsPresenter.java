package com.bidjobbers.bidjobberspro.features.OnTimeScreen;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.Advance.AdvanceRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Advance.AdvanceResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BusinessInfo.BusinessInfoRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BusinessInfo.BusinessInfoResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BusinessLocation.BusinessLocationRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BusinessLocation.BusinessLocationResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ImagePicker.ImagePickerResponse;
import com.bidjobbers.bidjobberspro.data.network.models.IntroBusinessDetails.IntroBusinessDetailsRequest;
import com.bidjobbers.bidjobberspro.data.network.models.IntroBusinessDetails.IntroBusinessDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.PriceFor.PriceForRequest;
import com.bidjobbers.bidjobberspro.data.network.models.PriceFor.PriceForResponse;
import com.bidjobbers.bidjobberspro.data.network.models.PriceForTree.PriceForTreeResponse;
import com.bidjobbers.bidjobberspro.data.network.models.PriceForTree.PriceForTreeRquest;
import com.bidjobbers.bidjobberspro.data.network.models.PropertyType.PropertyTypeResponse;
import com.bidjobbers.bidjobberspro.data.network.models.PropertyType.PropertypeRequest;
import com.bidjobbers.bidjobberspro.data.network.models.SocialMedia.SocialMediaRequest;
import com.bidjobbers.bidjobberspro.data.network.models.SocialMedia.SocialMediaResponse;
import com.bidjobbers.bidjobberspro.data.network.models.businesshours.BusinessHoursRequest;
import com.bidjobbers.bidjobberspro.data.network.models.businesshours.BusinessHoursRespose;
import com.bidjobbers.bidjobberspro.data.network.models.kindofproperty.KindofPropertyRequest;
import com.bidjobbers.bidjobberspro.data.network.models.kindofproperty.KindofPropertyResponse;
import com.bidjobbers.bidjobberspro.data.network.models.servicedetails.ServiceDetailsRequest;
import com.bidjobbers.bidjobberspro.data.network.models.servicedetails.ServiceDetailsResponse;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.PricesFor.PriceForObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TreeStrubWorkFragment.TreeStrubWorkObject;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class FirstTimeAccountSettingsPresenter<V extends FirstTimeAccountSettingsMvpView>
                                             extends BasePresenter<V>
                                             implements FirstTimeAccountSettingsMvpPresenter<V>
{

    @Inject
    public FirstTimeAccountSettingsPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void init() {
       getMvpView().actionTesting();
    }

    @Override
    public void savePropertyData(boolean isResidential, boolean isCommertoal, boolean hasLicence, String licenceNo) {
        Gson gson = new Gson();
        String json = getDataManager().getMyObject();
        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
        obj.typeOfPropertyWorkOn.setCommercial(isCommertoal);
        obj.typeOfPropertyWorkOn.setResidential(isResidential);
        obj.typeOfPropertyWorkOn.setHaveALincence(hasLicence);
        obj.typeOfPropertyWorkOn.setLincenceNumber(licenceNo);



        String jsonString = gson.toJson(obj);
        getDataManager().setMyObject(jsonString);
        Log.e("ERROR",obj.getTypeOfPropertyWorkOn().toString());
    }

    @Override
    public void doGetServiceDetails(String language) {

        if(getMvpView().isNetworkConnected()) {
       // getMvpView().onError("Login to getService");
        ServiceDetailsRequest serviceDetailsRequest = new ServiceDetailsRequest(language);

       getCompositeDisposable().add(getDataManager().getServiceDetails("Bearer "+getDataManager().getAccess_token(),"application/json","application/json",serviceDetailsRequest)
       .subscribeOn(Schedulers.io())
       .observeOn(AndroidSchedulers.mainThread())
       .subscribeWith(new DisposableSingleObserver<ServiceDetailsResponse>() {
           @Override
           public void onSuccess(ServiceDetailsResponse serviceDetailsResponse) {

            if(serviceDetailsResponse.getResponseCode()==1)
            {
               // getMvpView().onError(serviceDetailsResponse.getResponseText());


                Gson gson = new Gson();
                String json = getDataManager().getMyObject();
                DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);


                getDataManager().setIsQuestionFillup(serviceDetailsResponse.getResponseData().getServiceProviderDetails().getHasBusinessDetails());

                // KinfoServiceProvider

                obj.kindOfServiceDoProvide.setLawnServiceId(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getParent_id());
                obj.kindOfServiceDoProvide.setLawnServiceName(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getParent_name());


                if(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getIs_selected().equals("0"))
                {
                    obj.kindOfServiceDoProvide.setLawnServiceSelected(false);
                }
                else
                {
                    obj.kindOfServiceDoProvide.setLawnServiceSelected(true);
                }
                obj.kindOfServiceDoProvide.setTreeCuttingId(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getParent_id());
                obj.kindOfServiceDoProvide.setTreeCuttingServiceName(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getParent_name());

                if(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getIs_selected().equals("0"))
                {
                    obj.kindOfServiceDoProvide.setTreeCuttingSelected(false);
                }
                else
                {
                    obj.kindOfServiceDoProvide.setTreeCuttingSelected(true);
                }

                obj.kindOfServiceDoProvide.setAreateALawnId(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getChildren().get(0).getId());
                obj.kindOfServiceDoProvide.setAreateALawnName(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getChildren().get(0).getName());
                obj.kindOfServiceDoProvide.setAreateALawnDescription(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getChildren().get(0).getDescription());
                obj.kindOfServiceDoProvide.setAreateALawnImage(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getChildren().get(0).getImage());


                if(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getChildren().get(0).getIs_selected().equals("0"))
                {
                    obj.kindOfServiceDoProvide.setAreateALawn(false);
                }
                else
                {
                    obj.kindOfServiceDoProvide.setAreateALawn(true);
                }

                obj.kindOfServiceDoProvide.setMowALownId(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getChildren().get(1).getId());
                obj.kindOfServiceDoProvide.setMowALawnName(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getChildren().get(1).getName());
                obj.kindOfServiceDoProvide.setMowALawnDescription(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getChildren().get(1).getDescription());
                obj.kindOfServiceDoProvide.setMowAMowALawnImage(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getChildren().get(1).getImage());

                if(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getChildren().get(1).getIs_selected().equals("0"))
                {
                    obj.kindOfServiceDoProvide.setIsmowALownSelected(false);
                }
                else
                {
                    obj.kindOfServiceDoProvide.setIsmowALownSelected(true);
                }



                obj.kindOfServiceDoProvide.setYardWasteId(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getChildren().get(2).getId());
                obj.kindOfServiceDoProvide.setYearALawnName(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getChildren().get(2).getName());
                obj.kindOfServiceDoProvide.setYearALawnDescription(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getChildren().get(2).getDescription());
                obj.kindOfServiceDoProvide.setYearALawnImage(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getChildren().get(2).getImage());

                if(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getChildren().get(2).getIs_selected().equals("0"))
                {
                    obj.kindOfServiceDoProvide.setIsyardWaste(false);
                }
                else
                {
                    obj.kindOfServiceDoProvide.setIsyardWaste(true);
                }


                obj.kindOfServiceDoProvide.setTreeStrubsId(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getChildren().get(0).getId());
                obj.kindOfServiceDoProvide.setTreeStrubsName(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getChildren().get(0).getName());
                obj.kindOfServiceDoProvide.setTreeStrubsDescription(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getChildren().get(0).getDescription());
                obj.kindOfServiceDoProvide.setTreeStrubsImage(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getChildren().get(0).getImage());

                if(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getChildren().get(0).getIs_selected().equals("0"))
                {
                    obj.kindOfServiceDoProvide.setTreeStrubsSelected(false);
                }
                else
                {
                    obj.kindOfServiceDoProvide.setTreeStrubsSelected(true);
                }


                obj.kindOfServiceDoProvide.setTreeStrumpsId(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getChildren().get(1).getId());
                obj.kindOfServiceDoProvide.setTreeStrumpsName(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getChildren().get(1).getName());
                obj.kindOfServiceDoProvide.setTreeStrumpsDescription(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getChildren().get(1).getDescription());
                obj.kindOfServiceDoProvide.setTreeStrumpsImage(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getChildren().get(1).getImage());

                if(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getChildren().get(1).getIs_selected().equals("0"))
                {
                    obj.kindOfServiceDoProvide.setTreeStrumps(false);
                }
                else
                {
                    obj.kindOfServiceDoProvide.setTreeStrumps(true);
                }

                obj.kindOfServiceDoProvide.setYardWasteTreeCuttingId(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getChildren().get(2).getId());
                obj.kindOfServiceDoProvide.setYardWasteTreeName(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getChildren().get(2).getName());
                obj.kindOfServiceDoProvide.setYardWasteTreeDescription(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getChildren().get(2).getDescription());
                obj.kindOfServiceDoProvide.setYardWasteTreeImage(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getChildren().get(2).getImage());

                if(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getChildren().get(1).getIs_selected().equals("2"))
                {
                    obj.kindOfServiceDoProvide.setIsyardWasteTreeCutting(false);
                }
                else
                {
                    obj.kindOfServiceDoProvide.setIsyardWasteTreeCutting(true);
                }


                // *******BusinessPriceLawnSerce

                obj.businessPriceLawnSerce.setLawnServceId(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getParent_id());
                obj.businessPriceLawnSerce.setLawnServiceName(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(0).getParent_name());
                obj.businessPriceLawnSerce.setTreeCuttingServiceId(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getParent_id());
                obj.businessPriceLawnSerce.setTreeCuttingServiceName(serviceDetailsResponse.getResponseData().getServiceTypeDetails().get(1).getParent_name());

                obj.businessPriceLawnSerce.setLsmallId(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(0).getId());
                obj.businessPriceLawnSerce.setLsmallName(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(0).getName());
                obj.businessPriceLawnSerce.setLsmallDescription(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(0).getShort_description());
                obj.businessPriceLawnSerce.setLsmallImage(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(0).getImage());
                obj.businessPriceLawnSerce.setLsmaillPrice(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(0).getPrice());


                obj.businessPriceLawnSerce.setLmedId(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(1).getId());
                obj.businessPriceLawnSerce.setLmedName(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(1).getName());
                obj.businessPriceLawnSerce.setLmedDescription(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(1).getShort_description());
                obj.businessPriceLawnSerce.setLmedImage(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(1).getImage());
                obj.businessPriceLawnSerce.setLmedPrice(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(1).getPrice());



                obj.businessPriceLawnSerce.setLlargeId(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(2).getId());
                obj.businessPriceLawnSerce.setLlargeName(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(2).getName());
                obj.businessPriceLawnSerce.setLlargeDescription(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(2).getShort_description());
                obj.businessPriceLawnSerce.setLlargeImage(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(2).getImage());
                obj.businessPriceLawnSerce.setLlargePrice(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(2).getPrice());


                obj.businessPriceLawnSerce.setLvryLargeId(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(3).getId());
                obj.businessPriceLawnSerce.setLveryLargeName(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(3).getName());
                obj.businessPriceLawnSerce.setLveryLargeDescription(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(3).getShort_description());
                obj.businessPriceLawnSerce.setLveryLargeImage(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(3).getImage());
                obj.businessPriceLawnSerce.setLvryLargePrice(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(3).getPrice());


                obj.businessPriceLawnSerce.setLunSureId(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(4).getId());
                obj.businessPriceLawnSerce.setLunSureName(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(4).getName());
                obj.businessPriceLawnSerce.setLunSureDescription(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(4).getShort_description());
                obj.businessPriceLawnSerce.setLunSureImage(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(4).getImage());
                obj.businessPriceLawnSerce.setLunSurePrice(serviceDetailsResponse.getResponseData().getLawnServicesDetails().get(4).getPrice());

                obj.businessPriceLawnSerce.setTOneTreeId(serviceDetailsResponse.getResponseData().getTreeServicesDetails().get(0).getId());
                obj.businessPriceLawnSerce.setTOneTreeName(serviceDetailsResponse.getResponseData().getTreeServicesDetails().get(0).getName());
                obj.businessPriceLawnSerce.setTOneTreePrice(serviceDetailsResponse.getResponseData().getTreeServicesDetails().get(0).getPrice());



                obj.businessPriceLawnSerce.setTtwoTothreeId(serviceDetailsResponse.getResponseData().getTreeServicesDetails().get(1).getId());
                obj.businessPriceLawnSerce.setTtwoTothreeName(serviceDetailsResponse.getResponseData().getTreeServicesDetails().get(1).getName());
                obj.businessPriceLawnSerce.setTtwoTothreePrice(serviceDetailsResponse.getResponseData().getTreeServicesDetails().get(1).getPrice());



                obj.businessPriceLawnSerce.setTfourTofiveId(serviceDetailsResponse.getResponseData().getTreeServicesDetails().get(2).getId());
                obj.businessPriceLawnSerce.setTfourTofiveName(serviceDetailsResponse.getResponseData().getTreeServicesDetails().get(2).getName());
                obj.businessPriceLawnSerce.setTfourTofivePrice(serviceDetailsResponse.getResponseData().getTreeServicesDetails().get(2).getPrice());




                obj.businessPriceLawnSerce.setTmoreFiveId(serviceDetailsResponse.getResponseData().getTreeServicesDetails().get(3).getId());
                obj.businessPriceLawnSerce.setTmoreFiveName(serviceDetailsResponse.getResponseData().getTreeServicesDetails().get(3).getName());
                obj.businessPriceLawnSerce.setTmoreFivePrice(serviceDetailsResponse.getResponseData().getTreeServicesDetails().get(3).getPrice());

                // **** TypeOfPropertyWorkOn




                obj.typeOfPropertyWorkOn.setCommercialId(serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getPropertyType().get(0).getId());
                obj.typeOfPropertyWorkOn.setCommercialName(serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getPropertyType().get(0).getName());
                obj.typeOfPropertyWorkOn.setResidentialId(serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getPropertyType().get(1).getId());
                obj.typeOfPropertyWorkOn.setResidentialName(serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getPropertyType().get(1).getName());
                obj.typeOfPropertyWorkOn.setLincenceNumber(serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getLicence_number());

                if(serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getPropertyType().get(0).getIs_selected().equals("1"))
                {
                   obj.typeOfPropertyWorkOn.setResidential(true);
                }
                else
                {
                    obj.typeOfPropertyWorkOn.setResidential(false);
                }
                if(serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getPropertyType().get(1).getIs_selected().equals("1"))
                {
                    obj.typeOfPropertyWorkOn.setCommercial(true);
                }
                else
                {
                    obj.typeOfPropertyWorkOn.setCommercial(false);
                }
                if(serviceDetailsResponse.getResponseData().getPropertyTypeDetails().getIs_having_licence().equals("1"))
                {
                   obj.typeOfPropertyWorkOn.setHaveALincence(true);
                }
                else
                {
                    obj.typeOfPropertyWorkOn.setHaveALincence(false);
                }


                // **** BusinessHoursRequest


                obj.businessHours.setSundayId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(0).getId());
                obj.businessHours.setSundayName(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(0).getDayName());
                obj.businessHours.setSundayFromTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(0).getFrom_time());
                obj.businessHours.setSundayFromTimeId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(0).getFrom_time_id());
                obj.businessHours.setSundayToTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(0).getEnd_time());
                obj.businessHours.setSundayToTimeId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(0).getEnd_time_id());

                if(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(0).getIsSelected().equals("1"))
                {
                    obj.businessHours.setSundaySelected(true);
                }
                else
                {
                    obj.businessHours.setSundaySelected(false);
                }

                obj.businessHours.setMondayId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(1).getId());
                obj.businessHours.setMondayName(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(1).getDayName());
                obj.businessHours.setMondayFromTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(1).getFrom_time());
                obj.businessHours.setMondayFromTimeId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(1).getFrom_time_id());
                obj.businessHours.setMondayToTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(1).getEnd_time());
                obj.businessHours.setMondayToTimeId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(1).getEnd_time_id());


                if(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(1).getIsSelected().equals("1"))
                {
                    obj.businessHours.setMondaySelected(true);
                }
                else
                {
                    obj.businessHours.setMondaySelected(false);
                }


                obj.businessHours.setTuesdayId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(2).getId());
                obj.businessHours.setTuesdayName(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(2).getDayName());
                obj.businessHours.setTuesdayFromTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(2).getFrom_time());
                obj.businessHours.setTuesdayFromTimeId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(2).getFrom_time_id());
                obj.businessHours.setTuesdayToTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(2).getEnd_time());
                obj.businessHours.setTuesdayToTimeId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(2).getEnd_time_id());



                if(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(2).getIsSelected().equals("1"))
                {
                    obj.businessHours.setTuesdaySelected(true);
                }
                else
                {
                    obj.businessHours.setTuesdaySelected(false);
                }


                obj.businessHours.setWednesdayId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(3).getId());
                obj.businessHours.setWednesdayName(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(3).getDayName());
                obj.businessHours.setWednesdayFromTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(3).getFrom_time());
                obj.businessHours.setWednesdayFromTimeId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(3).getFrom_time_id());
                obj.businessHours.setWednesdayToTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(3).getEnd_time());
                obj.businessHours.setWednesdayToTimeId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(3).getEnd_time_id());



                if(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(3).getIsSelected().equals("1"))
                {
                    obj.businessHours.setWednesdaySelected(true);
                }
                else
                {
                    obj.businessHours.setWednesdaySelected(false);
                }


                obj.businessHours.setThursdayId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(4).getId());
                obj.businessHours.setThrusdayName(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(4).getDayName());
                obj.businessHours.setThursdayFromTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(4).getFrom_time());
                obj.businessHours.setThursdayFromTimeId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(4).getFrom_time_id());
                obj.businessHours.setThrusdayToTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(4).getEnd_time());
                obj.businessHours.setThursdayToTimeId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(4).getEnd_time_id());


                if(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(4).getIsSelected().equals("1"))
                {
                    obj.businessHours.setThursdaySelected(true);
                }
                else
                {
                    obj.businessHours.setThursdaySelected(false);
                }



                obj.businessHours.setFridayId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(5).getId());
                obj.businessHours.setFridayName(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(5).getDayName());
                obj.businessHours.setFridayFromTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(5).getFrom_time());
                obj.businessHours.setFridayFromTimeId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(5).getFrom_time_id());
                obj.businessHours.setFridayToTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(5).getEnd_time());
                obj.businessHours.setFridayToTimeId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(5).getEnd_time_id());



                if(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(5).getIsSelected().equals("1"))
                {
                    obj.businessHours.setFridaySelected(true);
                }
                else
                {
                    obj.businessHours.setFridaySelected(false);
                }

                obj.businessHours.setSaturdayId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(6).getId());
                obj.businessHours.setSaturdayName(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(6).getDayName());
                obj.businessHours.setSaturdayFromTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(6).getFrom_time());
                obj.businessHours.setSaturdayFromTimeId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(6).getFrom_time_id());
                obj.businessHours.setSaturdayToTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(6).getEnd_time());
                obj.businessHours.setSaturdayToTimeId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(6).getEnd_time_id());



                if(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBusinessHrData().get(6).getIsSelected().equals("1"))
                {
                    obj.businessHours.setSaturdaySelected(true);
                }
                else
                {
                    obj.businessHours.setSaturdaySelected(false);
                }


                obj.businessHours.setOneHourId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(0).getId());
                obj.businessHours.setOneHourTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(0).getTime());


                obj.businessHours.setTwoHourId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(1).getId());
                obj.businessHours.setTwoHourTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(1).getTime());

                obj.businessHours.setThreeHourId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(2).getId());
                obj.businessHours.setThreeHourTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(2).getTime());

                obj.businessHours.setFourHourId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(3).getId());
                obj.businessHours.setFourHourTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(3).getTime());

                obj.businessHours.setFiveHourId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(4).getId());
                obj.businessHours.setFiveHourTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(4).getTime());


                obj.businessHours.setSixHourId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(5).getId());
                obj.businessHours.setSixHourTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(5).getTime());

                obj.businessHours.setSevenHourId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(6).getId());
                obj.businessHours.setSevenHourTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(6).getTime());

                obj.businessHours.setEightHourId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(7).getId());
                obj.businessHours.setEightHourTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(7).getTime());

                obj.businessHours.setNineHourId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(8).getId());
                obj.businessHours.setNineHourTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(8).getTime());

                obj.businessHours.setTenHourId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(9).getId());
                obj.businessHours.setTenHourTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(9).getTime());

                obj.businessHours.setElevenHourId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(10).getId());
                obj.businessHours.setElevenHourTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(10).getTime());

                obj.businessHours.setTwellveHourId(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(11).getId());
                obj.businessHours.setTwelveHourTime(serviceDetailsResponse.getResponseData().getBuisnessHoursDetails().getBuisnessHrDropdown().get(11).getTime());



                //BusinessInfo

                obj.businessInfo.setCompanyName(serviceDetailsResponse.getResponseData().getBuisnessInfo().getCompany_name());
                obj.businessInfo.setCompRegisterNumber(serviceDetailsResponse.getResponseData().getBuisnessInfo().getCompany_reg_no());
                obj.businessInfo.setYearFounded(serviceDetailsResponse.getResponseData().getBuisnessInfo().getFounding_year());
                obj.businessInfo.setNoOfEmployee(serviceDetailsResponse.getResponseData().getBuisnessInfo().getNo_of_employee());
                obj.businessInfo.setNoOfEmployeeId(serviceDetailsResponse.getResponseData().getBuisnessInfo().getEmployee_count_selected_id());
                obj.businessInfo.setNOEOneId(serviceDetailsResponse.getResponseData().getBuisnessInfo().getEmployeeDropdownData().get(0).getId()+"");
                obj.businessInfo.setNOEOneName(serviceDetailsResponse.getResponseData().getBuisnessInfo().getEmployeeDropdownData().get(0).getNo_of_employee()+"");
                obj.businessInfo.setNOETwoId(serviceDetailsResponse.getResponseData().getBuisnessInfo().getEmployeeDropdownData().get(1).getId()+"");
                obj.businessInfo.setNOETwoName(serviceDetailsResponse.getResponseData().getBuisnessInfo().getEmployeeDropdownData().get(1).getNo_of_employee()+"");
                obj.businessInfo.setNOEThreeId(serviceDetailsResponse.getResponseData().getBuisnessInfo().getEmployeeDropdownData().get(2).getId()+"");
                obj.businessInfo.setNOEThreeName(serviceDetailsResponse.getResponseData().getBuisnessInfo().getEmployeeDropdownData().get(2).getNo_of_employee()+"");


                //BusinessLocation

                obj.businessLocation.setBusinessAddress(serviceDetailsResponse.getResponseData().getBuisnessLocation().getBusiness_address());
                obj.businessLocation.setApartment(serviceDetailsResponse.getResponseData().getBuisnessLocation().getApartment());
                obj.businessLocation.setCity(serviceDetailsResponse.getResponseData().getBuisnessLocation().getCity());
                obj.businessLocation.setCountry(serviceDetailsResponse.getResponseData().getBuisnessLocation().getCountry());
                obj.businessLocation.setState(serviceDetailsResponse.getResponseData().getBuisnessLocation().getState());
                obj.businessLocation.setPinCode(serviceDetailsResponse.getResponseData().getBuisnessLocation().getZipcode());
                obj.businessLocation.setCity_Id(serviceDetailsResponse.getResponseData().getBuisnessLocation().getCity_id());
                obj.businessLocation.setCountry_Id(serviceDetailsResponse.getResponseData().getBuisnessLocation().getCountry_id());
                obj.businessLocation.setLatitude(serviceDetailsResponse.getResponseData().getBuisnessLocation().getLat());
                obj.businessLocation.setLongitude(serviceDetailsResponse.getResponseData().getBuisnessLocation().getLng());


                //IntroduceYourBusiness

                obj.introduceYourBusiness.setDetails(serviceDetailsResponse.getResponseData().getBuisnessDesc().getBusiness_intro_desc());

               //AdvanceTimeToBookAJob

                obj.advanceTimeToBookAJob.setCount1Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(0).getId());
                obj.advanceTimeToBookAJob.setCount1Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(0).getCount());
                obj.advanceTimeToBookAJob.setCount2Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(1).getId());
                obj.advanceTimeToBookAJob.setCount2Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(1).getCount());
                obj.advanceTimeToBookAJob.setCount3Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(2).getId());
                obj.advanceTimeToBookAJob.setCount3Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(2).getCount());
                obj.advanceTimeToBookAJob.setCount4Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(3).getId());
                obj.advanceTimeToBookAJob.setCount4Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(3).getCount());
                obj.advanceTimeToBookAJob.setCount5Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(4).getId());
                obj.advanceTimeToBookAJob.setCount5Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(4).getCount());
                obj.advanceTimeToBookAJob.setCount6Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(5).getId());
                obj.advanceTimeToBookAJob.setCount6Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(5).getCount());
                obj.advanceTimeToBookAJob.setCount7Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(6).getId());
                obj.advanceTimeToBookAJob.setCount7Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(6).getCount());
                obj.advanceTimeToBookAJob.setCount8Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(7).getId());
                obj.advanceTimeToBookAJob.setCount8Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(7).getCount());
                obj.advanceTimeToBookAJob.setCount9Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(8).getId());
                obj.advanceTimeToBookAJob.setCount9Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(8).getCount());
                obj.advanceTimeToBookAJob.setCount10Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(9).getId());
                obj.advanceTimeToBookAJob.setCount10Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(9).getCount());
                obj.advanceTimeToBookAJob.setCount11Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(10).getId());
                obj.advanceTimeToBookAJob.setCount11Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(10).getCount());
                obj.advanceTimeToBookAJob.setCount12Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(11).getId());
                obj.advanceTimeToBookAJob.setCount12Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(11).getCount());
                obj.advanceTimeToBookAJob.setCount13Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(12).getId());
                obj.advanceTimeToBookAJob.setCount13Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(12).getCount());
                obj.advanceTimeToBookAJob.setCount14Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(13).getId());
                obj.advanceTimeToBookAJob.setCount14Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(13).getCount());
                obj.advanceTimeToBookAJob.setCount15Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(14).getId());
                obj.advanceTimeToBookAJob.setCount15Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(14).getCount());
                obj.advanceTimeToBookAJob.setCount16Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(15).getId());
                obj.advanceTimeToBookAJob.setCount16Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(15).getCount());
                obj.advanceTimeToBookAJob.setCount17Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(16).getId());
                obj.advanceTimeToBookAJob.setCount17Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(16).getCount());
                obj.advanceTimeToBookAJob.setCount18Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(17).getId());
                obj.advanceTimeToBookAJob.setCount18Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(17).getCount());
                obj.advanceTimeToBookAJob.setCount19Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(18).getId());
                obj.advanceTimeToBookAJob.setCount19Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(18).getCount());
                obj.advanceTimeToBookAJob.setCount20Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(19).getId());
                obj.advanceTimeToBookAJob.setCount20Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(19).getCount());
                obj.advanceTimeToBookAJob.setCount21Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(20).getId());
                obj.advanceTimeToBookAJob.setCount21Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(20).getCount());
                obj.advanceTimeToBookAJob.setCount22Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(21).getId());
                obj.advanceTimeToBookAJob.setCount22Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(21).getCount());
                obj.advanceTimeToBookAJob.setCount23Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(22).getId());
                obj.advanceTimeToBookAJob.setCount23Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(22).getCount());
                obj.advanceTimeToBookAJob.setCount24Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(23).getId());
                obj.advanceTimeToBookAJob.setCount24Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(23).getCount());
                obj.advanceTimeToBookAJob.setCount25Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(24).getId());
                obj.advanceTimeToBookAJob.setCount25Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(24).getCount());
                obj.advanceTimeToBookAJob.setCount26Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(25).getId());
                obj.advanceTimeToBookAJob.setCount26Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(25).getCount());
                obj.advanceTimeToBookAJob.setCount27Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(26).getId());
                obj.advanceTimeToBookAJob.setCount27Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(26).getCount());
                obj.advanceTimeToBookAJob.setCount28Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(27).getId());
                obj.advanceTimeToBookAJob.setCount28Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(27).getCount());
                obj.advanceTimeToBookAJob.setCount29Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(28).getId());
                obj.advanceTimeToBookAJob.setCount29Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(28).getCount());
                obj.advanceTimeToBookAJob.setCount30Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(29).getId());
                obj.advanceTimeToBookAJob.setCount30Count(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrbuteCount().get(29).getCount());


                obj.advanceTimeToBookAJob.setHowFar1Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrunit_for_how_far().get(0).getId()+"");
                obj.advanceTimeToBookAJob.setHowFar1Unit(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrunit_for_how_far().get(0).getUnit());
                obj.advanceTimeToBookAJob.setHowFar2Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrunit_for_how_far().get(1).getId()+"");
                obj.advanceTimeToBookAJob.setHowFar2Unit(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrunit_for_how_far().get(1).getUnit());
                obj.advanceTimeToBookAJob.setHowFar3Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrunit_for_how_far().get(2).getId()+"");
                obj.advanceTimeToBookAJob.setHowFar3Unit(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrunit_for_how_far().get(2).getUnit());

                obj.advanceTimeToBookAJob.setNotice1Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrunit_for_notice_period().get(0).getId()+"");
                obj.advanceTimeToBookAJob.setNotice1Unite(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrunit_for_notice_period().get(0).getUnit());
                obj.advanceTimeToBookAJob.setNotice2Id(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrunit_for_notice_period().get(1).getId()+"");
                obj.advanceTimeToBookAJob.setNotice2Unite(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAttrunit_for_notice_period().get(1).getUnit());




               obj.advanceTimeToBookAJob.setFieldAdvanceValueId(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAdvanceValueId());
               obj.advanceTimeToBookAJob.setFieldAdvanceValue(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAdvanceValue());
               obj.advanceTimeToBookAJob.setFieldAdvanceNameId(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAdvanceNameid());
               obj.advanceTimeToBookAJob.setFieldAdvanceName(serviceDetailsResponse.getResponseData().getAdvanceBooking().getAdvanceName());
               obj.advanceTimeToBookAJob.setFieldNoticeValueId(serviceDetailsResponse.getResponseData().getAdvanceBooking().getNoticeValueId());
               obj.advanceTimeToBookAJob.setFieldNoticeValue(serviceDetailsResponse.getResponseData().getAdvanceBooking().getNoticeValue());
               obj.advanceTimeToBookAJob.setFieldNoticeNameId(serviceDetailsResponse.getResponseData().getAdvanceBooking().getNoticeNameId());
               obj.advanceTimeToBookAJob.setFieldNoticeName(serviceDetailsResponse.getResponseData().getAdvanceBooking().getNoticeName());



               //social media
                if(obj.socialMediaLink!=null) {

                    // Log.e("FirstTime",serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(0));

                    if (serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().size() == 1) {

                        obj.socialMediaLink.setSm_one(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(0));
                    } else if (serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().size() == 2) {
                        obj.socialMediaLink.setSm_one(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(0));
                        obj.socialMediaLink.setSm_two(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(1));
                    } else if (serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().size() == 3) {
                        obj.socialMediaLink.setSm_one(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(0));
                        obj.socialMediaLink.setSm_two(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(1));
                        obj.socialMediaLink.setSm_three(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(2));

                    } else if (serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().size() == 4) {
                        obj.socialMediaLink.setSm_one(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(0));
                        obj.socialMediaLink.setSm_two(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(1));
                        obj.socialMediaLink.setSm_three(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(2));
                        obj.socialMediaLink.setSm_four(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(3));
                    }
                    else if (serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().size() == 5) {
                        obj.socialMediaLink.setSm_one(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(0));
                        obj.socialMediaLink.setSm_two(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(1));
                        obj.socialMediaLink.setSm_three(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(2));
                        obj.socialMediaLink.setSm_four(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(3));
                        obj.socialMediaLink.setSm_five(serviceDetailsResponse.getResponseData().getSocialLink().getSocial_media_links().get(4));
                    }

                    if (!serviceDetailsResponse.getResponseData().getSocialLink().getWebsite_url().equals("")) {
                        obj.socialMediaLink.setWebsite_Url(serviceDetailsResponse.getResponseData().getSocialLink().getWebsite_url());
                    }

                    //*****WorkPhoto*****


                    if(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().size()>0)
                    {
                        obj.photoDescribeProject.setIsAddad(serviceDetailsResponse.getResponseData().getWorkPhoto().getIs_added());
                        obj.photoDescribeProject.setImageUrl(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage_url());
                        if(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().size()==1)
                        {
                            obj.photoDescribeProject.setImageOne(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().get(0));
                        }
                        else if(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().size()==2)
                        {
                            obj.photoDescribeProject.setImageOne(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().get(0));
                            obj.photoDescribeProject.setImageTwo(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().get(1));
                            obj.photoDescribeProject.setImageThree("");
                            obj.photoDescribeProject.setImageFour("");
                            obj.photoDescribeProject.setImageFive("");
                        }
                        else if(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().size()==3)
                        {
                            obj.photoDescribeProject.setImageOne(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().get(0));
                            obj.photoDescribeProject.setImageTwo(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().get(1));
                            obj.photoDescribeProject.setImageThree(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().get(2));

                        }
                        else if(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().size()==4)
                        {
                            obj.photoDescribeProject.setImageOne(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().get(0));
                            obj.photoDescribeProject.setImageTwo(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().get(1));
                            obj.photoDescribeProject.setImageThree(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().get(2));
                            obj.photoDescribeProject.setImageFour(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().get(3));
                        }
                        else if(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().size()==5)
                        {
                            obj.photoDescribeProject.setImageOne(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().get(0));
                            obj.photoDescribeProject.setImageTwo(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().get(1));
                            obj.photoDescribeProject.setImageThree(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().get(2));
                            obj.photoDescribeProject.setImageFour(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().get(3));
                            obj.photoDescribeProject.setImageFive(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage().get(4));
                        }
                    }
                    else
                    {
                        obj.photoDescribeProject.setIsAddad(serviceDetailsResponse.getResponseData().getWorkPhoto().getIs_added());
                        obj.photoDescribeProject.setImageUrl(serviceDetailsResponse.getResponseData().getWorkPhoto().getImage_url());

                        obj.photoDescribeProject.setImageOne("");
                        obj.photoDescribeProject.setImageTwo("");
                        obj.photoDescribeProject.setImageThree("");
                        obj.photoDescribeProject.setImageFour("");
                        obj.photoDescribeProject.setImageFive("");
                    }
                }

                if(serviceDetailsResponse.getResponseData().getIsTransAccountDetailsAvailable().equals("1"))
                {    obj.bankDetails.setBank_account_holder_name(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_account_holder_first_name());
                    obj.bankDetails.setBank_account_holder_last_name(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_account_holder_last_name());
                    obj.bankDetails.setTransaction_account_id(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getTransaction_account_id());
                    obj.bankDetails.setBank_account_number(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_account_number());
                    obj.bankDetails.setBank_country(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_country());
                    obj.bankDetails.setBank_currency(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_currency());
                    obj.bankDetails.setBank_iban(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_iban());
                    obj.bankDetails.setBank_name(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_name());
                    obj.bankDetails.setBank_routing_number(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_routing_number());
                    obj.bankDetails.setPaypal_currency(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getPaypal_currency());
                    obj.bankDetails.setPaypal_id(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getPaypal_id());
                    obj.bankDetails.setBank_swift(serviceDetailsResponse.getResponseData().getTransactionAccountDetails().getBank_swift());


                }
                else
                {
                    obj.bankDetails.setBank_account_holder_name("");
                    obj.bankDetails.setBank_account_holder_last_name("");
                    obj.bankDetails.setTransaction_account_id("");
                    obj.bankDetails.setBank_account_number("");
                    obj.bankDetails.setBank_country("");
                    obj.bankDetails.setBank_currency("");
                    obj.bankDetails.setBank_iban("");
                    obj.bankDetails.setBank_name("");
                    obj.bankDetails.setBank_routing_number("");
                    obj.bankDetails.setPaypal_currency("");
                    obj.bankDetails.setPaypal_id("");
                    obj.bankDetails.setBank_swift("");
                }

                String jsonString = gson.toJson(obj);
                getDataManager().setMyObject(jsonString);

                getMvpView().successfullyGetServiceDetails(serviceDetailsResponse);



            }
            else
            {
                getMvpView().showMessage(serviceDetailsResponse.getResponseText());
            }

           }

           @Override
           public void onError(Throwable e) {

           }
       }));

        }
        else
        {
            getMvpView().showAlert("No internet Connection");
        }


    }

    @Override
    public void submitData(final String desc) {
        IntroBusinessDetailsRequest introBusinessDetailsRequest=new IntroBusinessDetailsRequest(getDataManager().getLocalValue(),desc);
        getCompositeDisposable().add(getDataManager().postIntroBusinessDetailsResponse("Bearer "+getDataManager().getAccess_token(),"application/json",introBusinessDetailsRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<IntroBusinessDetailsResponse>() {
                    @Override
                    public void onSuccess(IntroBusinessDetailsResponse introBusinessDetailsResponse) {

                        Gson gson = new Gson();
                        String json = getDataManager().getMyObject();
                        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);



                        obj.introduceYourBusiness.setDetails(desc);
                        String jsonString = gson.toJson(obj);
                        getDataManager().setMyObject(jsonString);

                       // getMvpView().onError(introBusinessDetailsResponse.getResponseText());
                        getMvpView().successfullySavaData();

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }));
    }

    @Override
    public void submitKindOfPropertyData(List<Integer> service_type) {
        KindofPropertyRequest kindofPropertyRequest=new KindofPropertyRequest(getDataManager().getLocalValue(),service_type);
        getCompositeDisposable().add(getDataManager().postKindOfPropertyWorkOn("Bearer "+getDataManager().getAccess_token(),"application/json",kindofPropertyRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<KindofPropertyResponse>() {
                    @Override
                    public void onSuccess(KindofPropertyResponse introBusinessDetailsResponse) {
//
//                        Gson gson = new Gson();
//                        String json = getDataManager().getMyObject();
//                        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
//
//
//
//                        obj.introduceYourBusiness.setDetails(desc);
//                        String jsonString = gson.toJson(obj);
//                        getDataManager().setMyObject(jsonString);

                     //   getMvpView().onError(introBusinessDetailsResponse.getResponseText());
                        getMvpView().successfullySaveKindOfPropertyData();

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }));
    }

    @Override
    public void submitBusinessInfo(String companyName, String yearofFound, String noofEmpId, String comapnyRegNo) {
        BusinessInfoRequest businessInfoRequest=new BusinessInfoRequest(getDataManager().getLocalValue(),companyName,yearofFound,noofEmpId,comapnyRegNo);
        getCompositeDisposable().add(getDataManager().postBusinessInfo("Bearer "+getDataManager().getAccess_token(),"application/json",businessInfoRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<BusinessInfoResponse>() {
                    @Override
                    public void onSuccess(BusinessInfoResponse introBusinessDetailsResponse) {
//
//                        Gson gson = new Gson();
//                        String json = getDataManager().getMyObject();
//                        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
//
//
//
//                        obj.introduceYourBusiness.setDetails(desc);
//                        String jsonString = gson.toJson(obj);
//                        getDataManager().setMyObject(jsonString);

                      //  getMvpView().onError(introBusinessDetailsResponse.getResponseText());
                        getMvpView().successfullySaveBusinessInfo();

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }));
    }

    @Override
    public void submitBusinessLocation(String businessaddress, String apartment, String city, String state, String country, String zipcode, String lat, String lng) {
        BusinessLocationRequest businessLocationRequest=new BusinessLocationRequest(getDataManager().getLocalValue(),businessaddress,apartment,city,state,country,zipcode,lat,lng);
        getCompositeDisposable().add(getDataManager().postBusinessLocation("Bearer "+getDataManager().getAccess_token(),"application/json",businessLocationRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<BusinessLocationResponse>() {
                    @Override
                    public void onSuccess(BusinessLocationResponse introBusinessDetailsResponse) {
//
//                        Gson gson = new Gson();
//                        String json = getDataManager().getMyObject();
//                        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
//
//
//
//                        obj.introduceYourBusiness.setDetails(desc);
//                        String jsonString = gson.toJson(obj);
//                        getDataManager().setMyObject(jsonString);

                       // getMvpView().onError(introBusinessDetailsResponse.getResponseText());
                        getMvpView().successfullySaveBusinessLocation();

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }));
    }

    @Override
    public void submitBusinessHours(ArrayList<String> dayId, ArrayList<String> startTimeId, ArrayList<String> startTime, ArrayList<String> endTimeId, ArrayList<String> endTime) {


        List<BusinessHoursRequest.BusinessHrDataBean> list=new ArrayList<>();
        for(int i=0;i<dayId.size();i++)
        {

            BusinessHoursRequest.BusinessHrDataBean businessHrDataBean=new BusinessHoursRequest.BusinessHrDataBean();
            businessHrDataBean.setDay_id(dayId.get(i));
            businessHrDataBean.setStart_time_id(startTimeId.get(i));
            businessHrDataBean.setStart_time(startTime.get(i));
            businessHrDataBean.setEnd_time(endTime.get(i));
            businessHrDataBean.setEnd_time_id(endTimeId.get(i));

            list.add(businessHrDataBean);
        }


        BusinessHoursRequest  businessHoursRequest=new BusinessHoursRequest(getDataManager().getLocalValue(),list);

        getCompositeDisposable().add(getDataManager().postBusinessHours("Bearer "+getDataManager().getAccess_token(),"application/json",businessHoursRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<BusinessHoursRespose>() {
                    @Override
                    public void onSuccess(BusinessHoursRespose introBusinessDetailsResponse) {
//
//                        Gson gson = new Gson();
//                        String json = getDataManager().getMyObject();
//                        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
//
//
//
//                        obj.introduceYourBusiness.setDetails(desc);
//                        String jsonString = gson.toJson(obj);
//                        getDataManager().setMyObject(jsonString);

                       // getMvpView().onError(introBusinessDetailsResponse.getResponseText());
                        getMvpView().successfullySaveBusinessHour();

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }));
    }

    @Override
    public void submitSocialLink(List<String> socialLinks,String webUrl) {



        SocialMediaRequest socialMediaRequest=new SocialMediaRequest(getDataManager().getLocalValue(),webUrl,socialLinks);

        getCompositeDisposable().add(getDataManager().postSocialMediaLink("Bearer "+getDataManager().getAccess_token(),"application/json",socialMediaRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<SocialMediaResponse>() {
                    @Override
                    public void onSuccess(SocialMediaResponse introBusinessDetailsResponse) {
//
//                        Gson gson = new Gson();
//                        String json = getDataManager().getMyObject();
//                        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
//
//
//
//                        obj.introduceYourBusiness.setDetails(desc);
//                        String jsonString = gson.toJson(obj);
//                        getDataManager().setMyObject(jsonString);

                       // getMvpView().onError(introBusinessDetailsResponse.getResponseText());
                        getMvpView().successfullySaveSocialLink();

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }));
    }

    @Override
    public void submitPropertyTypeData(ArrayList<Integer> listNo, String isHaveLicense, String licenceNo) {
        PropertypeRequest propertypeRequest=new PropertypeRequest(getDataManager().getLocalValue(),isHaveLicense,licenceNo,listNo);

        getCompositeDisposable().add(getDataManager().postPropertyType("Bearer "+getDataManager().getAccess_token(),"application/json",propertypeRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<PropertyTypeResponse>() {
                    @Override
                    public void onSuccess(PropertyTypeResponse introBusinessDetailsResponse) {
//
//                        Gson gson = new Gson();
//                        String json = getDataManager().getMyObject();
//                        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
//
//
//
//                        obj.introduceYourBusiness.setDetails(desc);
//                        String jsonString = gson.toJson(obj);
//                        getDataManager().setMyObject(jsonString);

                        // getMvpView().onError(introBusinessDetailsResponse.getResponseText());
                        getMvpView().successfullyySavePropertyType();

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }));
    }

    @Override
    public void submitAdvanceTypeData(String advanceValueId, String advanceValue, String advanceNameid, String advanceName, String noticeValueId, String noticeValue, String noticeNameId, String noticeName) {

        AdvanceRequest advanceRequest=new AdvanceRequest(getDataManager().getLocalValue(),advanceValueId,advanceValue,advanceNameid,advanceName,noticeValueId,noticeValue,noticeNameId,noticeName);

        getCompositeDisposable().add(getDataManager().postAdvanceType("Bearer "+getDataManager().getAccess_token(),"application/json",advanceRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<AdvanceResponse>() {
                    @Override
                    public void onSuccess(AdvanceResponse introBusinessDetailsResponse) {
//
//                        Gson gson = new Gson();
//                        String json = getDataManager().getMyObject();
//                        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
//
//
//
//                        obj.introduceYourBusiness.setDetails(desc);
//                        String jsonString = gson.toJson(obj);
//                        getDataManager().setMyObject(jsonString);

                        // getMvpView().onError(introBusinessDetailsResponse.getResponseText());
                        getMvpView().successfullySaveAdvanceBooking();

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }));

    }

    @Override
    public void submitPriceForTypeData(ArrayList<PriceForObject.PriceBean> list) {

        ArrayList<PriceForRequest.LawnserviceBean> list1=new ArrayList<>();
        for(int i=0;i<list.size();i++)
        {
            PriceForRequest.LawnserviceBean treeserviceBean=new PriceForRequest.LawnserviceBean();
            treeserviceBean.setLawnSizeId(list.get(i).getTreeId());
            treeserviceBean.setLawnSizePrice(list.get(i).getTreePrice());
            list1.add(treeserviceBean);
        }

        PriceForRequest priceForRequest=new PriceForRequest(getDataManager().getLocalValue(),list1);

        getCompositeDisposable().add(getDataManager().postPriceForType("Bearer "+getDataManager().getAccess_token(),"application/json",priceForRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<PriceForResponse>() {
                    @Override
                    public void onSuccess(PriceForResponse introBusinessDetailsResponse) {
//
//                        Gson gson = new Gson();
//                        String json = getDataManager().getMyObject();
//                        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
//
//
//
//                        obj.introduceYourBusiness.setDetails(desc);
//                        String jsonString = gson.toJson(obj);
//                        getDataManager().setMyObject(jsonString);

                        // getMvpView().onError(introBusinessDetailsResponse.getResponseText());
                        getMvpView().successfullySavePriceForLawn();

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }));
    }

    @Override
    public void submitPriceForTree(ArrayList<TreeStrubWorkObject.TreeClass> list) {
        ArrayList<PriceForTreeRquest.TreeserviceBean> list1=new ArrayList<>();
        for(int i=0;i<list.size();i++)
        {
            PriceForTreeRquest.TreeserviceBean treeserviceBean=new PriceForTreeRquest.TreeserviceBean();
            treeserviceBean.setTreeCountId(list.get(i).getId());
            treeserviceBean.setTreeCountPrice(list.get(i).getPrice());
            list1.add(treeserviceBean);
        }

        PriceForTreeRquest priceForRequest=new PriceForTreeRquest(getDataManager().getLocalValue(),list1);

        getCompositeDisposable().add(getDataManager().postPriceTreeForType("Bearer "+getDataManager().getAccess_token(),"application/json",priceForRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<PriceForTreeResponse>() {
                    @Override
                    public void onSuccess(PriceForTreeResponse introBusinessDetailsResponse) {
//
//                        Gson gson = new Gson();
//                        String json = getDataManager().getMyObject();
//                        DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
//
//
//
//                        obj.introduceYourBusiness.setDetails(desc);
//                        String jsonString = gson.toJson(obj);
//                        getDataManager().setMyObject(jsonString);

                        // getMvpView().onError(introBusinessDetailsResponse.getResponseText());
                        getMvpView().successfullySavePriceForTree();

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                }));
    }

    @Override
    public void submitImageData(MultipartBody.Part[] workImage, String isWorkImage) {


        RequestBody isWorkImageS = RequestBody.create(MediaType.parse("multipart/form-data"), isWorkImage);
        RequestBody localData = RequestBody.create(MediaType.parse("multipart/form-data"),  getDataManager().getLocalValue());

        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().postWorkPhotoForType( "Bearer "+getDataManager().getAccess_token(),
               localData,isWorkImageS,workImage)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<ImagePickerResponse>()
                {

                    @Override
                    public void onSuccess(ImagePickerResponse testResponse)
                    {
                        getMvpView().hideLoading();

                        if(testResponse.getResponseCode()==1 )
                        {
                            getDataManager().setIsQuestionFillup("1");
                            getMvpView().successfullySaveImage();
                        }
                        else
                        {

                           getMvpView().onError(testResponse.getResponseText());

                        }

                    }
                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));



    }


}
