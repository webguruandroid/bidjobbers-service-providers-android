package com.bidjobbers.bidjobberspro.features.ScheduleYourJob.AvalableDateViewPagerHelper;

import com.bidjobbers.bidjobberspro.data.network.models.TimeSlot.TimeSlotResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface AvalableDateBodyFragmentMvpView extends BaseFragmentMvpView {
    void onSuccessfullyGetTimeSlot(TimeSlotResponse.ResponseDataBean timeSlotResponse);
}
