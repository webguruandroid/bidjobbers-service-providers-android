package com.bidjobbers.bidjobberspro.features.resetpasswordfromsettings;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface ResetPasswordMvpPresenter <V extends ResetPasswordMvpView> extends MvpPresenter<V> {

 void onChagnePassword(String oldpassword,String newpassword,String confirmPassword);
}
