package com.bidjobbers.bidjobberspro.features.dashbord.ui.send;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileRequest;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

public class SettingsFragmentPresenter <V extends SettingsFragmentMvpView>
        extends BaseFragmentPresenter<V>
        implements SettingsFragmentMvpPresenter<V> {

    @Inject
    public SettingsFragmentPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }

    @Override
    public void onSuccessfullGetProfile(String language) {


        getProfileRequest getProfileRequest1=new getProfileRequest(language);

       // getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().getProfileDetails("Bearer "+getDataManager().getAccess_token(),"application/json","application/json",getProfileRequest1)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<getProfileResponse>()
                {

                    @Override
                    public void onSuccess(getProfileResponse testResponse)
                    {

                       // getMvpView().hideLoading();
                        if(testResponse.getResponseCode()==0 )
                        {
                            getMvpView().successfullyGetProfile(testResponse);
                        }
                        else if(testResponse.getResponseCode()==401)
                        {
                            getMvpView().onError(testResponse.getResponseText());
                            getMvpView().onDeactivate(testResponse.getResponseText());

                        }
                        else
                        {

                            getMvpView().successfullyGetProfile(testResponse);


                            //Shared prefrence value save access_token.
                        }

                    }
                    @Override
                    public void onError(Throwable e) {

                       // getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));



    }


    @Override
    public void onLogoutClick() {

        getDataManager().destroyPref();
        getMvpView().successfullyLogout();
    }
}
