package com.bidjobbers.bidjobberspro.features.resetpassword;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface ResetPasswordMvpPresenter <V extends ResetPasswordMvpView> extends MvpPresenter<V> {

    public void changePassWord(String oldPass, String newPass, String confPass,String email, String otp);

}
