package com.bidjobbers.bidjobberspro.features.GetOtpAfterForgetPassword;

import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface GetOtpForgotPassMvpView extends MvpView {

    public void actionGetOtp();


}
