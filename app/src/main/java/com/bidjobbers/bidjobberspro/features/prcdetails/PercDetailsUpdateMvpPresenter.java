package com.bidjobbers.bidjobberspro.features.prcdetails;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

import okhttp3.MultipartBody;

public interface PercDetailsUpdateMvpPresenter<V extends PercDetailsUpdateMvpView> extends MvpPresenter<V> {

    void onSuccessfullGetProfile(String language);
    void onUpdateProfile( MultipartBody.Part img_body,String name,String phone,String local);
}
