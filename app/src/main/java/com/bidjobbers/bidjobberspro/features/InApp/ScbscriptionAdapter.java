package com.bidjobbers.bidjobberspro.features.InApp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.subscription.SubscriptionResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScbscriptionAdapter extends RecyclerView.Adapter<ScbscriptionAdapter.ViewHolder> {

    Context mContext;
    ArrayList<SubscriptionResponse.ResponseDataBean.SubscribePackageBean> mValues;
    int size;
    private ScbscriptionAdapter.NotificationListner listListner;
    public ScbscriptionAdapter(ArrayList<SubscriptionResponse.ResponseDataBean.SubscribePackageBean> item, int size) {

        this.mValues=item;
        this.size=size;
    }

    public void loadList(List<SubscriptionResponse.ResponseDataBean.SubscribePackageBean> items) {
        Log.e("Task",items.size()+"");
        mValues.clear();
        mValues.addAll(items);
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mContext = viewGroup.getContext();
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout_in_app, viewGroup, false);
        return new ViewHolder(view);
    }

    public void setAdapterListner(ScbscriptionAdapter.NotificationListner listner)
    {
        this.listListner=listner;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final SubscriptionResponse.ResponseDataBean.SubscribePackageBean mDataBean = mValues.get(i);
        viewHolder.tv_BillingPeriods.setText(mDataBean.getBilling_period());
        viewHolder.tv_freeTrial.setText(mDataBean.getFree_trial());
        viewHolder.tv_subs_name.setText(mDataBean.getName());
        viewHolder.tv_subs_price.setText(mDataBean.getAmount());
        viewHolder.tv_subs_desc.setText(mDataBean.getDescription());



       viewHolder.llContainer.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               listListner.onItemClick(mDataBean.getApple_plan_sku());
           }
       });
    }

    @Override
    public int getItemCount() {
        return mValues.size();

    }

    public interface NotificationListner{
        void onItemClick(String skuId);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.llContainer)
        LinearLayout llContainer;
        @BindView(R.id.tv_BillingPeriods)
        TextView tv_BillingPeriods;
        @BindView(R.id.tv_freeTrial)
        TextView tv_freeTrial;
        @BindView(R.id.tv_subs_name)
        TextView tv_subs_name;
        @BindView(R.id.tv_subs_price)
        TextView tv_subs_price;
        @BindView(R.id.tv_subs_desc)
        TextView tv_subs_desc;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);


        }
    }


}
