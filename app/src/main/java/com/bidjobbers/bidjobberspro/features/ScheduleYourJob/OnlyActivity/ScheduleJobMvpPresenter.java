package com.bidjobbers.bidjobberspro.features.ScheduleYourJob.OnlyActivity;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface ScheduleJobMvpPresenter <V extends ScheduleJobMvpView> extends MvpPresenter<V> {


    void ongetSchedule();
}
