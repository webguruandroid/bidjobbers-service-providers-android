package com.bidjobbers.bidjobberspro.features.ScheduleYourJob.AvalableDateViewPagerHelper;

public class AvalableDateBean {

    String date="";
    String id="";
    String isChaked = "";


    public AvalableDateBean(String date, String id, String isChaked) {
        this.date = date;
        this.id = id;
        this.isChaked = isChaked;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String isChaked() {
        return isChaked;
    }

    public void setChaked(String chaked) {
        isChaked = chaked;
    }
}
