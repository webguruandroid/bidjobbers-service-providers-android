package com.bidjobbers.bidjobberspro.features.ScheduleYourJob.OnlyActivity;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.TimeSlot.TimeSlotRequest;
import com.bidjobbers.bidjobberspro.data.network.models.TimeSlot.TimeSlotResponse;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

public class ScheduleJobPresenter <V extends ScheduleJobMvpView>
        extends BasePresenter<V>
        implements ScheduleJobMvpPresenter<V> {


    @Inject
    public ScheduleJobPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void ongetSchedule() {
        TimeSlotRequest timeSlotRequests=new TimeSlotRequest(getDataManager().getLocalValue());
       // getMvpView().onError(getDataManager().getLocalValue());

        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().postGetTimeSlot( "Bearer "+getDataManager().getAccess_token(),"application/json",timeSlotRequests)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<TimeSlotResponse>()
                {

                    @Override
                    public void onSuccess(TimeSlotResponse timeSlotResponse)
                    {
                        getMvpView().hideLoading();

                        if(timeSlotResponse.getResponseCode()==1 )
                        {
                            getMvpView().onSuccessfullyGetTimeSlot(timeSlotResponse.getResponseData());
                          //  getMvpView().onError(timeSlotResponse.getResponseText());
                        }
                        else if(timeSlotResponse.getResponseCode()==401)
                        {
                            getMvpView().onError(timeSlotResponse.getResponseText());
                            getMvpView().onDeactivate(timeSlotResponse.getResponseText());
                        }
                        else
                        {

                            getMvpView().onError(timeSlotResponse.getResponseText());
                            getMvpView().onSuccessfullyGetTimeSlot(timeSlotResponse.getResponseData());

                        }

                    }
                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));


    }
}
