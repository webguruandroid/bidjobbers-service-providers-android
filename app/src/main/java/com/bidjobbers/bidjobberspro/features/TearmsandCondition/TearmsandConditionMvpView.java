package com.bidjobbers.bidjobberspro.features.TearmsandCondition;

import com.bidjobbers.bidjobberspro.data.network.models.Allpages.AllPagesResponse;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface TearmsandConditionMvpView extends MvpView {

    void  allTasksName(AllPagesResponse response);
    void onDeactive(String data);
}
