package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.ScheduledTask;

import com.bidjobbers.bidjobberspro.data.network.models.ScheduleTask.ScheduletaskResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface ScheduledBodyMvpView extends BaseFragmentMvpView {
     void  allTasksName(ScheduletaskResponse arrayList);
     void onDeactivate(String data);
}
