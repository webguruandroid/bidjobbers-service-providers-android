package com.bidjobbers.bidjobberspro.features.OnTimeScreen.KindOfPropertyWorkOn;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface KindOfProPropertyWorkOnFragmentMvpPresenter<V extends KindOfProPropertyWorkOnFragmentMvpView> extends BaseFragmentMvpPresenter<V> {

    void onGetDataFromSP();
    void onGetDataFromApi();
}
