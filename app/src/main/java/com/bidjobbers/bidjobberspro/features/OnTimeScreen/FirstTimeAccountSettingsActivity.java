package com.bidjobbers.bidjobberspro.features.OnTimeScreen;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.servicedetails.ServiceDetailsResponse;
import com.bidjobbers.bidjobberspro.features.BankDetails.BankDetailsActivity;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.AdvanceBookingTime.AdvanceTimeForCustomerBookingFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.AdvanceBookingTime.AdvanceTimeForCustomerObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessInfo.BusinessInfoFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessInfo.BusinessInfoObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessLocatiion.BusinessLocationFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessLocatiion.BusinessLocationObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.ImageDataFragment.ImagePickerFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.ImageDataFragment.ImagePickerObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.IntroBusinessDetails.IntroBusinessDetailsFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.IntroBusinessDetails.IntroBusinessObject.IntroBusinessObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.IntroductionFragment.IntroFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.KindOfPropertyWorkOn.KindOfProPropertyWorkOnFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.KindOfPropertyWorkOn.kindofPropertyObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.PricesFor.PriceForObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.PricesFor.PricesForFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.SocialArea.SocialFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.SocialArea.SocialFragmentObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TreeStrubWorkFragment.TreeStrubWorkFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TreeStrubWorkFragment.TreeStrubWorkObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TypeOfPropertyWorkOn.TypeOfPropertyObject.TypeOfPropertyObject;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.TypeOfPropertyWorkOn.TypeOfPropertyWorkOnFragment;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.personaldetails.PersonalDetailsFragment;
import com.bidjobbers.bidjobberspro.features.dashbord.Main2Activity;
import com.bidjobbers.bidjobberspro.features.profiledetails.ProfileActivity;
import com.bidjobbers.bidjobberspro.features.profiledetails.ProfileMvpView;
import com.bidjobbers.bidjobberspro.features.profiledetails.ProfilePresenter;
import com.bidjobbers.bidjobberspro.features.testfragment.QuestionImageGalaryFragment;
import com.bidjobbers.bidjobberspro.features.testfragment.QuestionImageGalaryObject;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class FirstTimeAccountSettingsActivity extends BaseActivity implements FirstTimeAccountSettingsMvpView {

    @Inject
    FirstTimeAccountSettingsPresenter<FirstTimeAccountSettingsMvpView> firstTimeAccountSettingsPresenter;

    FragmentTransaction fragmentTransaction;
    FragmentManager fragmentManager;
    BaseFragment previousFragments;
    int fragmentNumber = 1;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.btn_bck)
    LinearLayout btn_bck;
    @BindView(R.id.btn_submit)
    LinearLayout btn_submit;
    @BindView(R.id.btn_next)
    LinearLayout btn_next;


    @Inject
    ProfilePresenter<ProfileMvpView> profilePresenter;
    @Inject
    KindOfProPropertyWorkOnFragment kindOfProPropertyWorkOnFragment;
    @Inject
    PersonalDetailsFragment personalDetailsFragment;
    @Inject
    PricesForFragment pricesForFragment;
    @Inject
    TreeStrubWorkFragment treeStrubWorkFragment;
    @Inject
    TypeOfPropertyWorkOnFragment typeOfPropertyWorkOnFragment;
    @Inject
    AdvanceTimeForCustomerBookingFragment advanceTimeForCustomerBookingFragment;
    @Inject
    BusinessInfoFragment businessInfoFragment;
    @Inject
    BusinessLocationFragment businessLocationFragment;
    @Inject
    IntroFragment introFragment;
    @Inject
    QuestionImageGalaryFragment questionImageGalaryFragment;
    @Inject
    IntroBusinessDetailsFragment introBusinessDetailsFragment;
    @Inject
    SocialFragment socialFragment;
    @Inject
     ImagePickerFragment imagePickerFragment;

    SharedPreferences mPrefs;
    Dialog dialog,dialogWhereDataTake,dialogTypeAddress;
    LinearLayout ll_search_address;
    LinearLayout ll_type_address;
    Context mContext;
    TextView tv_add;
    ProfileActivity.GetLocation mGetLocation;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE_DEST = 601;
    ArrayList<Integer>propertyId=new ArrayList<>();
    String haveLicence;
    MultipartBody.Part[] workImage=new MultipartBody.Part[5];

    boolean flagTree=false;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_time_account_settings);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        mContext=this;
        mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        firstTimeAccountSettingsPresenter.onAttach(this);
        firstTimeAccountSettingsPresenter.init();
        firstTimeAccountSettingsPresenter.doGetServiceDetails("en");

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        previousFragments =  IntroFragment.newInstance();

        fragmentTransaction.setCustomAnimations(R.anim.slide_out_right, R.anim.slide_in_right);
        fragmentTransaction.add(R.id.fragment_container, IntroFragment.newInstance()).addToBackStack("Fragment_Step1_AboutProject");
        fragmentTransaction.commit();

        btn_bck.setVisibility(View.GONE);

        progressBar.setProgress(5);

        dialog = new Dialog(mContext, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_zoom_in_popup);



        dialogTypeAddress = new Dialog(mContext, R.style.Theme_Dialog_popup);
        dialogTypeAddress.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTypeAddress.setContentView(R.layout.layout_type_address);


        dialogWhereDataTake = new Dialog(mContext, R.style.Theme_Dialog_popup);
        dialogWhereDataTake.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogWhereDataTake.setContentView(R.layout.layout_select_loc_aria);

        ll_search_address= (LinearLayout) dialogWhereDataTake.findViewById(R.id.ll_search_address);
        ll_search_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogWhereDataTake.dismiss();
                openAutoLocationFetch();
            }
        });
        ll_type_address= (LinearLayout) dialogWhereDataTake.findViewById(R.id.ll_type_address);
        ll_type_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogWhereDataTake.dismiss();
                dialogTypeAddress.show();
            }
        });

        workImage[0]=null;
        workImage[1]=null;
        workImage[2]=null;
        workImage[3]=null;
        workImage[4]=null;
    }


    public void openAutoLocationFetch()    {
        try {
            List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME,Place.Field.LAT_LNG);
            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE_DEST);

        } catch (Exception e){
            Log.e(TAG, "findAddressAuto: "+ e.getMessage());
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        firstTimeAccountSettingsPresenter.onDetach();
    }

    @Override
    public void actionTesting() {
       // Toast.makeText(this, "Test", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void actionGetPropertyData() {

    }

    @Override
    public void successfullyGetServiceDetails(ServiceDetailsResponse serviceDetailsResponse) {

    }

    @Override
    public void successfullySavaData() {

    }

    @Override
    public void successfullySaveKindOfPropertyData() {

    }

    @Override
    public void successfullySaveBusinessInfo() {

    }

    @Override
    public void successfullySaveBusinessLocation() {

    }

    @Override
    public void successfullySaveBusinessHour() {

    }

    @Override
    public void successfullySaveSocialLink() {

    }

    @Override
    public void successfullyySavePropertyType() {

    }

    @Override
    public void successfullySaveAdvanceBooking() {

    }

    @Override
    public void successfullySavePriceForLawn() {

    }

    @Override
    public void successfullySavePriceForTree() {

    }

    @Override
    public void successfullySaveImage() {

        Log.e("Data","enter Bank");
        Intent i=new Intent(this, BankDetailsActivity.class);
        i.putExtra("Tag","First");
        startActivity(i);
        finish();

//        startActivity(new Intent(this, Main2Activity.class));
//        overridePendingTransition(R.anim.enter, R.anim.exit);
//        finish();

    }


    //btn_next

    @OnClick({R.id.btn_next,R.id.btn_bck,R.id.btn_submit})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                //Toast.makeText(this, ""+fragmentNumber, Toast.LENGTH_SHORT).show();
                replaceFragment(previousFragments,fragmentNumber);
                break;

            case R.id.btn_bck:
               // Toast.makeText(this, "backFragment:"+this.fragmentNumber, Toast.LENGTH_SHORT).show();
                backFragment(fragmentNumber);
                break;


                case R.id.btn_submit:



                    ImagePickerObject imagePickerObject= imagePickerFragment.saveImagePicker();

                   if(imagePickerObject!=null) {
//                       if (imagePickerObject.getWork().equals("1")) {
//                           if (imagePickerObject.getFileOne() == null || imagePickerObject.getFileTwo() == null || imagePickerObject.getFileThree() == null || imagePickerObject.getFileFour() == null || imagePickerObject.getFileFive() == null) {
//                               Toast.makeText(getApplicationContext(), "Please Select Alteast one Image", Toast.LENGTH_SHORT).show();
//                               return;
//                           }
//                       }

                       if (imagePickerObject.getFileOne() != null) {
                           RequestBody reqFile = RequestBody.create(MediaType.parse("multipart/form-data"), imagePickerObject.getFileOne());
                           workImage[0] = MultipartBody.Part.createFormData("work_image[0]", imagePickerObject.getFileOne().getName(), reqFile);
                       }
                       if (imagePickerObject.getFileTwo() != null) {
                           RequestBody reqFileOne = RequestBody.create(MediaType.parse("multipart/form-data"), imagePickerObject.getFileTwo());
                           workImage[1] = MultipartBody.Part.createFormData("work_image[1]", imagePickerObject.getFileTwo().getName(), reqFileOne);
                       }
                       if (imagePickerObject.getFileThree() != null) {
                           RequestBody reqFileThree = RequestBody.create(MediaType.parse("multipart/form-data"), imagePickerObject.getFileThree());
                           workImage[2] = MultipartBody.Part.createFormData("work_image[2]", imagePickerObject.getFileThree().getName(), reqFileThree);
                       }
                       if (imagePickerObject.getFileFour() != null) {
                           RequestBody reqFileFour = RequestBody.create(MediaType.parse("multipart/form-data"), imagePickerObject.getFileFour());
                           workImage[3] = MultipartBody.Part.createFormData("work_image[3]", imagePickerObject.getFileFour().getName(), reqFileFour);
                       }
                       if (imagePickerObject.getFileFive() != null) {
                           RequestBody reqFileFive = RequestBody.create(MediaType.parse("multipart/form-data"), imagePickerObject.getFileFive());
                           workImage[4] = MultipartBody.Part.createFormData("work_image[4]", imagePickerObject.getFileFive().getName(), reqFileFive);
                       }

                       firstTimeAccountSettingsPresenter.submitImageData(workImage, imagePickerObject.getWork());
                   }
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    public void backFragment(int fragmentNumber){

      if(this.fragmentNumber!=1)
        {
            this.fragmentNumber=fragmentNumber-1;
            getSupportFragmentManager().popBackStack();
            btn_bck.setVisibility(View.VISIBLE);
            btn_submit.setVisibility(View.GONE);
            btn_next.setVisibility(View.VISIBLE);
        }
        else
        {
           if(this.fragmentNumber == 1)
             {
                 btn_bck.setVisibility(View.GONE);
                 btn_submit.setVisibility(View.GONE);
                 btn_next.setVisibility(View.VISIBLE);
             }
        }

        if(fragmentNumber == 1) {
            progressBar.setProgress(0);
        } if(fragmentNumber == 2) {
            progressBar.setProgress(15);
        }
        if(fragmentNumber == 3) {
            progressBar.setProgress(25);
        }
        if(fragmentNumber == 4) {
            progressBar.setProgress(35);
        }
        if(fragmentNumber == 5) {
            progressBar.setProgress(45);
        }
        if(fragmentNumber == 6) {
            progressBar.setProgress(55);
        }
        if(fragmentNumber == 7) {
            progressBar.setProgress(65);
        }
        if(fragmentNumber == 8) {
            progressBar.setProgress(75);
        }if(fragmentNumber == 9) {
            progressBar.setProgress(85);
        }
        if(fragmentNumber == 10) {
            progressBar.setProgress(85);
        }

    }


    public void replaceFragment(BaseFragment previousFragments, int fragmentNumber) {

        if(fragmentNumber == 1) {
            btn_bck.setVisibility(View.VISIBLE);
            btn_submit.setVisibility(View.GONE);
            btn_next.setVisibility(View.VISIBLE);
            previousFragments =  kindOfProPropertyWorkOnFragment;
            this.previousFragments=previousFragments;
            Bundle bundle = new Bundle();
            bundle.putSerializable("FROMG", "1");
            previousFragments.setArguments(bundle);

            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(
                    R.anim.exit_to_right, R.anim.exit_to_right_two,R.anim.enter_from_right, R.anim.enter_from_right);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.replace(R.id.fragment_container,  previousFragments,"SECOND_FRAGMENT");
            fragmentTransaction.commit();
            this.fragmentNumber=fragmentNumber+1;
            progressBar.setProgress(15);

        }
        if(fragmentNumber == 2) {
            btn_bck.setVisibility(View.VISIBLE);btn_submit.setVisibility(View.GONE); btn_next.setVisibility(View.VISIBLE);
            previousFragments =  businessInfoFragment;
            this.previousFragments=previousFragments;
            ArrayList<Integer> kindofId=new ArrayList<>();


            kindofPropertyObject kindofPropertyObject= kindOfProPropertyWorkOnFragment.setDataFunctionKindofProperty();


            Gson gson = new Gson();
            String json = mPrefs.getString("MyObject", "");
            DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);
            Log.e("FirstTime",obj.kindOfServiceDoProvide.toString());

            if(kindofPropertyObject.getLawnServiceId().equals("1"))
            {
               kindofId.add(Integer.parseInt(obj.kindOfServiceDoProvide.getLawnServiceId()));
            }
            if(kindofPropertyObject.getYardWasteId().equals("1"))
            {
                kindofId.add(Integer.parseInt(obj.kindOfServiceDoProvide.getYardWasteId()));
            }
            if(kindofPropertyObject.getMowALownId().equals("1"))
            {
                kindofId.add(Integer.parseInt(obj.kindOfServiceDoProvide.getMowALownId()));
            }
            if(kindofPropertyObject.getAreateALawnId().equals("1"))
            {
                kindofId.add(Integer.parseInt(obj.kindOfServiceDoProvide.getAreateALawnId()));
            }
            if(kindofPropertyObject.getTreeCuttingId().equals("1"))
            {
                kindofId.add(Integer.parseInt(obj.kindOfServiceDoProvide.getTreeCuttingId()));
            }
            if(kindofPropertyObject.getTreeStrubsId().equals("1"))
            {
                kindofId.add(Integer.parseInt(obj.kindOfServiceDoProvide.getTreeStrubsId()));
            }
            if(kindofPropertyObject.getTreeStrumpsId().equals("1"))
            {
                kindofId.add(Integer.parseInt(obj.kindOfServiceDoProvide.getTreeStrumpsId()));
            }
            if(kindofPropertyObject.getYardWasteTreeCuttingId().equals("1"))
            {
                kindofId.add(Integer.parseInt(obj.kindOfServiceDoProvide.getYardWasteTreeCuttingId()));
            }

            Log.e("FirstTime",kindofId.size()+"");


            if(kindofPropertyObject.getLawnServiceId().equals("0")&&kindofPropertyObject.getTreeCuttingId().equals("0"))
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleaseSelecOneService),Toast.LENGTH_SHORT).show();
            }
            else if(kindofPropertyObject.getLawnServiceId().equals("1")&&kindofPropertyObject.getYardWasteId().equals("0")&&kindofPropertyObject.getMowALownId().equals("0")&&kindofPropertyObject.getAreateALawnId().equals("0"))
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleaseSelectneLawnService),Toast.LENGTH_SHORT).show();
            }
            else if(kindofPropertyObject.getTreeCuttingId().equals("1")&&kindofPropertyObject.getTreeStrubsId().equals("0")&&kindofPropertyObject.getTreeStrumpsId().equals("0")&&kindofPropertyObject.getYardWasteTreeCuttingId().equals("0"))
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleaseSelectOneTreeCuttingService),Toast.LENGTH_SHORT).show();
            }
            else {
                firstTimeAccountSettingsPresenter.submitKindOfPropertyData(kindofId);

                Bundle bundle = new Bundle();
                bundle.putSerializable("FROMG", "1");
                previousFragments.setArguments(bundle);

                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(
                        R.anim.exit_to_right, R.anim.exit_to_right_two, R.anim.enter_from_right, R.anim.enter_from_right);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.fragment_container, previousFragments, "SECOND_FRAGMENT");
                fragmentTransaction.commit();
                this.fragmentNumber = fragmentNumber + 1;
                progressBar.setProgress(25);
            }
        }

        if(fragmentNumber == 3) {
            btn_bck.setVisibility(View.VISIBLE);btn_submit.setVisibility(View.GONE); btn_next.setVisibility(View.VISIBLE);
            // previousFragments =  BusinessLocationFragment.newInstance("FirstTime");
            previousFragments = socialFragment;
            this.previousFragments=previousFragments;

            BusinessInfoObject businessInfoObject=businessInfoFragment.setDataBusinessInfo();

            Log.e("FirstTimeAccount",businessInfoObject.toString());


            if(businessInfoObject.getCompanyName().equals(""))
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleaseEnterCompanyName),Toast.LENGTH_SHORT).show();
            }
            else if(businessInfoObject.getYearFounded().equals(""))
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleaseEnterYearofFound),Toast.LENGTH_SHORT).show();
            }
            else if(businessInfoObject.getNoOfEmployeeId().equals(""))
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleaseSelectNoofEmployee),Toast.LENGTH_SHORT).show();
            }
            else if(businessInfoObject.getCompRegisterNumber().equals(""))
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleaseEnterCompanyRegistrationNumber),Toast.LENGTH_SHORT).show();
            }
            else {

               firstTimeAccountSettingsPresenter.submitBusinessInfo(businessInfoObject.getCompanyName(),businessInfoObject.getYearFounded(),businessInfoObject.getNoOfEmployeeId(),businessInfoObject.getCompRegisterNumber());
                Bundle bundle = new Bundle();
                bundle.putSerializable("FROMG", "1");
                previousFragments.setArguments(bundle);
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(
                        R.anim.exit_to_right, R.anim.exit_to_right_two,
                        R.anim.enter_from_right, R.anim.enter_from_right);
                fragmentTransaction.replace(R.id.fragment_container, previousFragments).addToBackStack(null);
                fragmentTransaction.commit();
                this.fragmentNumber = fragmentNumber + 1;
                progressBar.setProgress(35);
            }
        }
        if(fragmentNumber == 4) {
            btn_bck.setVisibility(View.VISIBLE);btn_submit.setVisibility(View.GONE); btn_next.setVisibility(View.VISIBLE);
           // previousFragments =  BusinessLocationFragment.newInstance("FirstTime");
            previousFragments = businessLocationFragment;
            this.previousFragments=previousFragments;

            SocialFragmentObject socialFragmentObject=socialFragment.setDataSocialFragment();
            firstTimeAccountSettingsPresenter.submitSocialLink(socialFragmentObject.getSocialList(),socialFragmentObject.getWebUrl());



            Bundle bundle = new Bundle();
            bundle.putSerializable("FROMG", "1");
            previousFragments.setArguments(bundle);
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(
                    R.anim.exit_to_right, R.anim.exit_to_right_two,
                    R.anim.enter_from_right, R.anim.enter_from_right);
            fragmentTransaction.replace(R.id.fragment_container,  previousFragments).addToBackStack(null);
            fragmentTransaction.commit();
            this.fragmentNumber=fragmentNumber+1;
            progressBar.setProgress(35);
        }
        if(fragmentNumber == 5) {
            btn_bck.setVisibility(View.VISIBLE);btn_submit.setVisibility(View.GONE); btn_next.setVisibility(View.VISIBLE);
            previousFragments =  introBusinessDetailsFragment;
            this.previousFragments=previousFragments;



            Bundle bundle = new Bundle();
            bundle.putSerializable("FROMG", "1");
            previousFragments.setArguments(bundle);

            BusinessLocationObject businessLocationObject=businessLocationFragment.setDataFunctionBusinessLocation();


            if(businessLocationObject.getBusinessAddress().equals(""))
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleaseEnterBusinessAddress),Toast.LENGTH_SHORT).show();
            }
            else if(businessLocationObject.getApartment().equals(""))
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleaseEnterApartmentStreet),Toast.LENGTH_SHORT).show();
            }
            else if(businessLocationObject.getCity().equals("0")) {

                Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleaseSelectCity),Toast.LENGTH_SHORT).show();
            }
            else if(businessLocationObject.getCountry().equals(""))
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleasenterCountry),Toast.LENGTH_SHORT).show();
            }
            else if(businessLocationObject.getPinCode().equals(""))
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleaseEnterPinCode),Toast.LENGTH_SHORT).show();
            }
            else  if(businessLocationObject.getLat().equals(""))
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleaseChooseBusinessAddress),Toast.LENGTH_SHORT).show();
            }
            else  if(businessLocationObject.getLog().equals(""))
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleaseChooseBusinessAddress),Toast.LENGTH_SHORT).show();
            }
            else {
                firstTimeAccountSettingsPresenter.submitBusinessLocation(businessLocationObject.getBusinessAddress(), businessLocationObject.getApartment(), businessLocationObject.getCity(), businessLocationObject.getState(), businessLocationObject.getCountry(), businessLocationObject.getPinCode(), businessLocationObject.getLat(), businessLocationObject.getLog());

                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.exit_to_right, R.anim.exit_to_right_two,
                        R.anim.enter_from_right, R.anim.enter_from_right);
                fragmentTransaction.replace(R.id.fragment_container, previousFragments).addToBackStack(null);
                fragmentTransaction.commit();
                this.fragmentNumber = fragmentNumber + 1;
                progressBar.setProgress(45);
            }
        }
        if(fragmentNumber == 6) {
            btn_bck.setVisibility(View.VISIBLE);btn_submit.setVisibility(View.GONE); btn_next.setVisibility(View.VISIBLE);
            previousFragments =  questionImageGalaryFragment;
            this.previousFragments=previousFragments;


            Bundle bundle = new Bundle();
            bundle.putSerializable("FROMG", "1");
            previousFragments.setArguments(bundle);

            IntroBusinessObject introBusinessObject= introBusinessDetailsFragment.sendDataToActvity();


            if(introBusinessObject.getDes().equals("")) {

                Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseEnterDescription), Toast.LENGTH_SHORT).show();
            }
            else {
                //call API
                firstTimeAccountSettingsPresenter.submitData(introBusinessObject.getDes());


                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.exit_to_right, R.anim.exit_to_right_two,
                        R.anim.enter_from_right, R.anim.enter_from_right);
                fragmentTransaction.replace(R.id.fragment_container, previousFragments).addToBackStack(null);
                fragmentTransaction.commit();
                this.fragmentNumber = fragmentNumber + 1;
                progressBar.setProgress(55);
            }
        }
        if(fragmentNumber == 7) {
            btn_bck.setVisibility(View.VISIBLE);btn_submit.setVisibility(View.GONE); btn_next.setVisibility(View.VISIBLE);
            previousFragments =  advanceTimeForCustomerBookingFragment;
            this.previousFragments=previousFragments;




            Bundle bundle = new Bundle();
            bundle.putSerializable("FROMG", "1");
            previousFragments.setArguments(bundle);


            QuestionImageGalaryObject questionImageGalaryObject=questionImageGalaryFragment.setDataQuestionImageGalary();


            Log.e("FirstTime",questionImageGalaryObject.toString());
            if(questionImageGalaryObject.getDayId().size()==0&&questionImageGalaryObject.getStartTimeId().size()==0&&questionImageGalaryObject.getStartTime().size()==0&&questionImageGalaryObject.getEndTimeId().size()==0&&questionImageGalaryObject.getEndTime().size()==0)
            {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseSelectOneBusinessHourAtleast), Toast.LENGTH_SHORT).show();
            }
            else {
                firstTimeAccountSettingsPresenter.submitBusinessHours(questionImageGalaryObject.getDayId(), questionImageGalaryObject.getStartTimeId(), questionImageGalaryObject.getStartTime(), questionImageGalaryObject.getEndTimeId(), questionImageGalaryObject.getEndTime());


                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.exit_to_right, R.anim.exit_to_right_two,
                        R.anim.enter_from_right, R.anim.enter_from_right);
                fragmentTransaction.replace(R.id.fragment_container, previousFragments).addToBackStack(null);
                fragmentTransaction.commit();
                this.fragmentNumber = fragmentNumber + 1;
                progressBar.setProgress(65);
            }
        }
        if(fragmentNumber == 8) {
            btn_bck.setVisibility(View.VISIBLE);btn_submit.setVisibility(View.GONE); btn_next.setVisibility(View.VISIBLE);
            previousFragments =  typeOfPropertyWorkOnFragment;
            this.previousFragments=previousFragments;


            Bundle bundle = new Bundle();
            bundle.putSerializable("FROMG", "1");
            previousFragments.setArguments(bundle);



            AdvanceTimeForCustomerObject advanceTimeForCustomerObject=advanceTimeForCustomerBookingFragment.setAdvanceBooking();

            if(advanceTimeForCustomerObject.getAdvanceNameid().equals("0")&&advanceTimeForCustomerObject.getAdvanceValueId().equals("0")&&advanceTimeForCustomerObject.getNoticeNameId().equals("0")&&advanceTimeForCustomerObject.getNoticeValueId().equals("0"))
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleaseSelectAllValue),Toast.LENGTH_SHORT).show();
            }

            else {

                firstTimeAccountSettingsPresenter.submitAdvanceTypeData(advanceTimeForCustomerObject.getAdvanceValueId(),advanceTimeForCustomerObject.getAdvanceValue(),advanceTimeForCustomerObject.getAdvanceNameid(),advanceTimeForCustomerObject.getAdvanceName(),advanceTimeForCustomerObject.getNoticeValueId(),advanceTimeForCustomerObject.getNoticeValue(),advanceTimeForCustomerObject.getNoticeNameId(),advanceTimeForCustomerObject.getNoticeName());

                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.exit_to_right, R.anim.exit_to_right_two,
                        R.anim.enter_from_right, R.anim.enter_from_right);
                fragmentTransaction.replace(R.id.fragment_container, previousFragments).addToBackStack(null);
                fragmentTransaction.commit();
                this.fragmentNumber = fragmentNumber + 1;
                progressBar.setProgress(75);

            }
        }
        if(fragmentNumber == 9) {


            Gson gson = new Gson();
            String json = mPrefs.getString("MyObject", "");
            DataModelForFirstTimeQusAns obj = gson.fromJson(json, DataModelForFirstTimeQusAns.class);

            //firstTimeAccountSettingsPresenter.savePropertyData();
            propertyId=new ArrayList<>();
            TypeOfPropertyObject typeOfPropertyObject= typeOfPropertyWorkOnFragment.sendDataToActivity();


            if(typeOfPropertyObject.isCommertial()) {
                propertyId.add(Integer.parseInt(typeOfPropertyObject.getCommertialId()));
            }
            if(typeOfPropertyObject.isResidential()) {
                propertyId.add(Integer.parseInt(typeOfPropertyObject.getResidentialId()));
            }

            Log.e("Firsttime",typeOfPropertyObject.toString());

            if(typeOfPropertyObject.isHasLicence()==true)
            {
               haveLicence="1";
            }
            else
            {
                haveLicence="0";
            }

            if(typeOfPropertyObject.isCommertial()==false&&typeOfPropertyObject.isResidential()==false)
            {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleaseSelectOnePropertyType),Toast.LENGTH_SHORT).show();
            }

            else {


                firstTimeAccountSettingsPresenter.submitPropertyTypeData(propertyId,haveLicence,typeOfPropertyObject.getLicenceNo());

                btn_bck.setVisibility(View.VISIBLE);
                btn_submit.setVisibility(View.GONE);
                btn_next.setVisibility(View.VISIBLE);

                if(obj.kindOfServiceDoProvide.isLawnServiceSelected()==true &&obj.kindOfServiceDoProvide.isTreeCuttingSelected()==true){

                   previousFragments = pricesForFragment;
                    this.previousFragments = previousFragments;


                    Bundle bundle = new Bundle();
                    bundle.putSerializable("FROMG", "1");
                    previousFragments.setArguments(bundle);
                    flagTree=true;


                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.exit_to_right, R.anim.exit_to_right_two,
                            R.anim.enter_from_right, R.anim.enter_from_right);
                    fragmentTransaction.replace(R.id.fragment_container, previousFragments).addToBackStack(null);
                    fragmentTransaction.commit();
                    this.fragmentNumber = fragmentNumber + 1;
                    progressBar.setProgress(85);

                }
                else if(obj.kindOfServiceDoProvide.isTreeStrubsSelected()==true)
                {
                    previousFragments = treeStrubWorkFragment;
                    this.previousFragments = previousFragments;


                    Bundle bundle = new Bundle();
                    bundle.putSerializable("FROMG", "1");
                    previousFragments.setArguments(bundle);
                    flagTree=true;


                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.exit_to_right, R.anim.exit_to_right_two,
                            R.anim.enter_from_right, R.anim.enter_from_right);
                    fragmentTransaction.replace(R.id.fragment_container, previousFragments).addToBackStack(null);
                    fragmentTransaction.commit();
                    this.fragmentNumber = fragmentNumber + 2;
                    progressBar.setProgress(85);
                }
                else
                {
                    previousFragments = pricesForFragment;
                    this.previousFragments = previousFragments;


                    Bundle bundle = new Bundle();
                    bundle.putSerializable("FROMG", "1");
                    previousFragments.setArguments(bundle);
                    flagTree=false;

//
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.exit_to_right, R.anim.exit_to_right_two,
                            R.anim.enter_from_right, R.anim.enter_from_right);
                    fragmentTransaction.replace(R.id.fragment_container, previousFragments).addToBackStack(null);
                    fragmentTransaction.commit();
                    this.fragmentNumber = fragmentNumber + 2;
                    progressBar.setProgress(85);
                }

            }
        }
        if(fragmentNumber==10)
        {
            btn_bck.setVisibility(View.VISIBLE);
            btn_submit.setVisibility(View.GONE);
            btn_next.setVisibility(View.VISIBLE);
            previousFragments = treeStrubWorkFragment;
            this.previousFragments = previousFragments;

            PriceForObject object=pricesForFragment.setDataBusinessInfo();
            Log.e("FirstTime",object.toString());
            for(int i=0;i<object.list.size();i++)
            {
               if(object.list.get(i).getTreePrice().equals(""))
               {
                   Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleaseEnterPrice),Toast.LENGTH_SHORT).show();
                   return;
               }
            }

            firstTimeAccountSettingsPresenter.submitPriceForTypeData(object.list);


            Bundle bundle = new Bundle();
            bundle.putSerializable("FROMG", "1");
            previousFragments.setArguments(bundle);


            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.exit_to_right, R.anim.exit_to_right_two,
                    R.anim.enter_from_right, R.anim.enter_from_right);
            fragmentTransaction.replace(R.id.fragment_container, previousFragments).addToBackStack(null);
            fragmentTransaction.commit();
            this.fragmentNumber = fragmentNumber + 1;
            progressBar.setProgress(85);

        }
        if(fragmentNumber == 11) {


            btn_bck.setVisibility(View.VISIBLE);
            btn_submit.setVisibility(View.VISIBLE);
            btn_next.setVisibility(View.GONE);
            previousFragments =  imagePickerFragment;
            this.previousFragments=previousFragments;

            if(flagTree) {
                if (treeStrubWorkFragment != null) {

                    TreeStrubWorkObject treeStrubWorkObject = treeStrubWorkFragment.sendDataTreeStrubWork();

                    for (int i = 0; i < treeStrubWorkObject.list.size(); i++) {
                        if (treeStrubWorkObject.list.get(i).getPrice().equals("")) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.PleaseEnterPrice), Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }


                    firstTimeAccountSettingsPresenter.submitPriceForTree(treeStrubWorkObject.list);
                }
            }
            else
            {
                PriceForObject object=pricesForFragment.setDataBusinessInfo();
                Log.e("FirstTime",object.toString());
                for(int i=0;i<object.list.size();i++)
                {
                    if(object.list.get(i).getTreePrice().equals(""))
                    {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.PleaseEnterPrice),Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                firstTimeAccountSettingsPresenter.submitPriceForTypeData(object.list);
            }


            Bundle bundle = new Bundle();
            bundle.putSerializable("FROMG", "1");
            previousFragments.setArguments(bundle);



            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.exit_to_right, R.anim.exit_to_right_two,
                    R.anim.enter_from_right, R.anim.enter_from_right);
            fragmentTransaction.replace(R.id.fragment_container,  previousFragments).addToBackStack(null);
            fragmentTransaction.commit();
            this.fragmentNumber=fragmentNumber+1;
            progressBar.setProgress(100);
        }


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case PLACE_AUTOCOMPLETE_REQUEST_CODE_DEST:
                try{

                    if (resultCode == RESULT_OK) {
                        Place sourceplace = Autocomplete.getPlaceFromIntent(data);
                        String source = sourceplace.getName().toString();
                       // Toast.makeText(mContext, "Location:"+source, Toast.LENGTH_SHORT).show();
                        mGetLocation.getLocation(source);

                    }else if(resultCode == AutocompleteActivity.RESULT_ERROR){
                     //   Toast.makeText(mContext, "Error:"+"Happen", Toast.LENGTH_SHORT).show();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
        }


    }


    public void dialogOpen()
    {
        dialogWhereDataTake.show();
    }
}
