package com.bidjobbers.bidjobberspro.features.dashbord.OnlyActivty;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface DashbordMvpPresenter<V extends DashbordMvpView> extends MvpPresenter<V> {

   // public void getAllTasks();
    void onSuccessfullGetProfile(String language);
    void onLogoutClick();
    void onGetNotificationCount();
    void onUpdateDeviceToken(String deviceType,String deviceToken);
    void getSetLocal(String local);

}
