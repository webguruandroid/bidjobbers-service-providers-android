package com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessLocatiion;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.CityCountry.CityCountryResponse;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.features.Map.MapActivity;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;
import com.bidjobbers.bidjobberspro.utils.Validation.BusinessLocationResult;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BusinessLocationFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BusinessLocationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BusinessLocationFragment extends BaseFragment implements BusinessLocationFragmentMvpView{

    private static String whereFrom="";
    @BindView(R.id.ll_fetch_location)
    LinearLayout ll_fetch_location;
    @BindView(R.id.input__business_name)
    EditText input__business_name;
    @BindView(R.id.etApartment)
    EditText etApartment;
    @BindView(R.id.spCity)
    Spinner spCity;
    @BindView(R.id.etState)
    EditText etState;
    @BindView(R.id.spCountry)
    Spinner spCountry;
    @BindView(R.id.etPincode)
    EditText etPincode;
    @BindView(R.id.etCity)
    EditText etCity;
    @BindView(R.id.etCountry)
    EditText etCountry;
    @BindView(R.id.ivCountry)
    ImageView ivCountry;
    @BindView(R.id.ivCity)
     ImageView ivCity;
    @BindView(R.id.ll_city)
     LinearLayout ll_city;


    String str="";

    ArrayList<String>city=new ArrayList<>();
    ArrayList<String>country=new ArrayList<>();
    ArrayList<String>cityId=new ArrayList<>();
    ArrayList<String>countryId=new ArrayList<>();


    String selectedCountryId,selectedCityId,latitude,longitude;

    @Inject
    BusinessLocationFragmentPresenter<BusinessLocationFragmentMvpView> businessLocationFragmentPresenter;

    public BusinessLocationFragment() {

    }


    // TODO: Rename and change types and number of parameters
    public static BusinessLocationFragment newInstance(String from) {
        whereFrom = from;
        BusinessLocationFragment fragment = new BusinessLocationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {

             str = bundle.getString("FROMG");
            Log.e("FROMG", str);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v= inflater.inflate(R.layout.fragment_business_location, container, false);
        setUnBinder(ButterKnife.bind(this,v));
        ActivityComponent component = getActivityComponent();

        if (component != null) {
            component.inject(this);
        }

        businessLocationFragmentPresenter.onAttach(this);
        country=new ArrayList<>();
        countryId=new ArrayList<>();

        if(whereFrom.toString().equals("FirstTime"))
        {

        }
        else
        {

        }
        businessLocationFragmentPresenter.onGetCityCountry();
        businessLocationFragmentPresenter.onGetDataFromApi();



        spCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {


                selectedCountryId=countryId.get(position);

              //  Toast.makeText(getActivity(),selectedCountryId,Toast.LENGTH_SHORT).show();
                if(selectedCountryId.equals("0"))
                {
                    city=new ArrayList<>();
                    cityId=new ArrayList<>();

//                    city.add("Select City");
//                    cityId.add("0");
                    ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, city);
                    spCity.setAdapter(spinnerArrayAdapter);
                }
                else
                {
                    businessLocationFragmentPresenter.onGetCity(selectedCountryId);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                selectedCountryId="0";
            }

        });




        spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {


                selectedCityId=cityId.get(position);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                selectedCityId="0";
            }

        });


        ivCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spCountry.setVisibility(View.VISIBLE);
                spCountry.performClick();
                etCountry.setVisibility(View.GONE);
            }
        });

        ivCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spCity.setVisibility(View.VISIBLE);
                spCity.performClick();
                etCity.setVisibility(View.GONE);
            }
        });

        return v;
    }


    @OnClick
    ({ R.id.input__business_name})
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.input__business_name :

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                Intent intent = new Intent(getContext(), MapActivity.class);
                                startActivityForResult(intent,1);
                            }
                        }, 1000);




                break;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }


    public BusinessLocationResult getLoacationNameAndPinCodeWithValidation()
    {

        String pinNumberData= "";
        String locationData ="";
        boolean error_location = false;
        String error_text="";

      //  pinNumberData = pinNumber.getText().toString().trim();
      //  locationData = input_com_name.getText().toString().trim();


        if(pinNumberData.trim().equals(""))
        {
            error_location=true;
            error_text = "You must enter Pin Code";
        }

        if(locationData.trim().equals(""))
        {
            error_location=true;
            error_text = "You must give place name";
        }


        BusinessLocationResult businessLocationResult = new BusinessLocationResult( error_location,  error_text,  pinNumberData,  locationData);
        return  businessLocationResult;

    }

    @Override
    public void setDataFromApi(DataModelForFirstTimeQusAns.BusinessLocation businessLocation) {

        spCountry.setVisibility(View.VISIBLE);
     //   etCountry.setVisibility(View.VISIBLE);

        spCity.setVisibility(View.VISIBLE);
     //   etCity.setVisibility(View.VISIBLE);

        latitude=businessLocation.getLatitude();
        longitude=businessLocation.getLongitude();

        input__business_name.setText(businessLocation.getBusinessAddress());
        etApartment.setText(businessLocation.getApartment());
        etCity.setText(businessLocation.getCity());
        etState.setText(businessLocation.getState());
        etCountry.setText(businessLocation.getCountry());
        etPincode.setText(businessLocation.getPinCode());

        selectedCityId=businessLocation.getCity_Id();
        selectedCountryId=businessLocation.getCountry_Id();



    }

    @Override
    public void successfullygetCity(CityCountryResponse cityCountryResponse) {
        if(cityCountryResponse.getResponseCode()==1)
        {  // country.add("Select Country");
           // countryId.add("0");

            for (int i=0;i<cityCountryResponse.getResponseData().getMasterData().size();i++)
            {


                country.add(cityCountryResponse.getResponseData().getMasterData().get(i).getCountry_name());
                countryId.add(cityCountryResponse.getResponseData().getMasterData().get(i).getCountry_id());


            }

            ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item,
                    country);
            spCountry.setAdapter(spinnerArrayAdapter);
        }
    }

    @Override
    public void successCity(List<CityCountryResponse.ResponseDataBean.MasterDataBean.CityBean> list) {
        city=new ArrayList<>();
        cityId=new ArrayList<>();


        if(list.size()>0) {
            ll_city.setVisibility(View.VISIBLE);
            for (int i = 0; i < list.size(); i++) {
                city.add(list.get(i).getName());
                cityId.add(list.get(i).getId());
            }

            ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, city);
            spCity.setAdapter(spinnerArrayAdapter);
        }
        else
        {
            ll_city.setVisibility(View.GONE);
            city.add("No City Available");
            cityId.add("0");
            ArrayAdapter spinnerArrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, city);
            spCity.setAdapter(spinnerArrayAdapter);
        }
    }



    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public BusinessLocationObject setDataFunctionBusinessLocation()
    {

       // Toast.makeText(getActivity(),""+cityId,Toast.LENGTH_SHORT).show();
        Log.e("CityId",cityId+"");
        Log.e("DATA",latitude+"--"+longitude);
            BusinessLocationObject object = new BusinessLocationObject(input__business_name.getText().toString().trim(), etApartment.getText().toString().trim(), selectedCityId, etState.getText().toString().trim(), selectedCountryId, etPincode.getText().toString().trim(), latitude, longitude);

            return object;


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1)
        {
            if(resultCode == Activity.RESULT_OK){
                String regex = "[0-9]+";
                if(data.getStringExtra("result")!=null) {
                    String result = data.getStringExtra("result");
                    latitude = data.getStringExtra("latitude");
                    longitude = data.getStringExtra("longitude");
                    Log.e("DATA",latitude+"--"+longitude);
                    input__business_name.setText(result);
                    try {


                        String[] words = result.split("-");
                        String[] apartment = words[0].split(",");
                        etApartment.setText(apartment[0]);
                        etState.setText(words[2]);
                        if (words[4].matches(regex)) {
                            etPincode.setText(words[4]);
                        } else {
                            etPincode.setText("");
                        }
                    }
                    catch(Exception e)
                    {

                    }
                }
            }
        }
    }
}
