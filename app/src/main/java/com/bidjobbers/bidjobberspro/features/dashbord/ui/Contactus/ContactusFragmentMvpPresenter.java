package com.bidjobbers.bidjobberspro.features.dashbord.ui.Contactus;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface ContactusFragmentMvpPresenter<V extends ContactusFragmentMvpView> extends BaseFragmentMvpPresenter<V> {

    void onSuccessfullGetProfile(String language);
    void onContactUs(String name , String email , String reason , String msg);
}
