package com.bidjobbers.bidjobberspro.features.OnTimeScreen.KindOfPropertyWorkOn;

public class kindofPropertyObject {

    String lawnServiceId="";
    String mowALownId="";
    String areateALawnId="";
    String yardWasteId="";

    String treeCuttingId="";
    String treeStrubsId="";
    String treeStrumpsId="";
    String yardWasteTreeCuttingId="";

    public kindofPropertyObject(String lawnServiceId, String mowALownId, String areateALawnId, String yardWasteId, String treeCuttingId, String treeStrubsId, String treeStrumpsId, String yardWasteTreeCuttingId) {
        this.lawnServiceId = lawnServiceId;
        this.mowALownId = mowALownId;
        this.areateALawnId = areateALawnId;
        this.yardWasteId = yardWasteId;
        this.treeCuttingId = treeCuttingId;
        this.treeStrubsId = treeStrubsId;
        this.treeStrumpsId = treeStrumpsId;
        this.yardWasteTreeCuttingId = yardWasteTreeCuttingId;
    }

    public String getLawnServiceId() {
        return lawnServiceId;
    }

    public void setLawnServiceId(String lawnServiceId) {
        this.lawnServiceId = lawnServiceId;
    }

    public String getMowALownId() {
        return mowALownId;
    }

    public void setMowALownId(String mowALownId) {
        this.mowALownId = mowALownId;
    }

    public String getAreateALawnId() {
        return areateALawnId;
    }

    public void setAreateALawnId(String areateALawnId) {
        this.areateALawnId = areateALawnId;
    }

    public String getYardWasteId() {
        return yardWasteId;
    }

    public void setYardWasteId(String yardWasteId) {
        this.yardWasteId = yardWasteId;
    }

    public String getTreeCuttingId() {
        return treeCuttingId;
    }

    public void setTreeCuttingId(String treeCuttingId) {
        this.treeCuttingId = treeCuttingId;
    }

    public String getTreeStrubsId() {
        return treeStrubsId;
    }

    public void setTreeStrubsId(String treeStrubsId) {
        this.treeStrubsId = treeStrubsId;
    }

    public String getTreeStrumpsId() {
        return treeStrumpsId;
    }

    public void setTreeStrumpsId(String treeStrumpsId) {
        this.treeStrumpsId = treeStrumpsId;
    }

    public String getYardWasteTreeCuttingId() {
        return yardWasteTreeCuttingId;
    }

    public void setYardWasteTreeCuttingId(String yardWasteTreeCuttingId) {
        this.yardWasteTreeCuttingId = yardWasteTreeCuttingId;
    }


    @Override
    public String toString() {
        return "kindofPropertyObject{" +
                "lawnServiceId='" + lawnServiceId + '\'' +
                ", mowALownId='" + mowALownId + '\'' +
                ", areateALawnId='" + areateALawnId + '\'' +
                ", yardWasteId='" + yardWasteId + '\'' +
                ", treeCuttingId='" + treeCuttingId + '\'' +
                ", treeStrubsId='" + treeStrubsId + '\'' +
                ", treeStrumpsId='" + treeStrumpsId + '\'' +
                ", yardWasteTreeCuttingId='" + yardWasteTreeCuttingId + '\'' +
                '}';
    }
}
