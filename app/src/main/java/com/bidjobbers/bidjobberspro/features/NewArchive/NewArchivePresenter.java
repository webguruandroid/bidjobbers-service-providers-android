package com.bidjobbers.bidjobberspro.features.NewArchive;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.Archived.ArchiveListRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Archived.ArchiveListResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList.BlockCustomerListResponse;
import com.bidjobbers.bidjobberspro.data.network.models.NewArchive.NewArchiveList;
import com.bidjobbers.bidjobberspro.features.MapCustomerDetails.MapCustomerDeatilsMvpPresenter;
import com.bidjobbers.bidjobberspro.features.MapCustomerDetails.MapCustomerDetailsMvpView;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class NewArchivePresenter <V extends NewArchiveMvpView> extends BasePresenter<V> implements NewArchiveMvpPresenter<V> {
    @Inject
    public NewArchivePresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void getArchiveList(String timePeriod,String currentpag) {
        if(getMvpView().isNetworkConnected()) {
            getMvpView().showLoading();

            ArchiveListRequest archiveListRequest=new ArchiveListRequest(getDataManager().getLocalValue(),timePeriod,currentpag);

            getCompositeDisposable().add(getDataManager().getArchivedList("Bearer "+getDataManager().getAccess_token(),"application/json",archiveListRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<NewArchiveList>() {
                        @Override
                        public void onSuccess(NewArchiveList reciveTaskResponse) {

                            getMvpView().hideLoading();
                            if(reciveTaskResponse.getResponseCode()==1) {
                                getMvpView().successfullyGetArchiveCustomer(reciveTaskResponse);
                            }
                            else
                            {
                                getMvpView().successfullyGetArchiveCustomer(reciveTaskResponse);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            getMvpView().hideLoading();
                        }
                    }));

        }
        else
        {
            getMvpView().showAlert("No internet Connection");
        }
    }
}
