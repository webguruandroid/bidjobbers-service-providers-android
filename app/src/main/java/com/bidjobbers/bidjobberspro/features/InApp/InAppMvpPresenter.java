package com.bidjobbers.bidjobberspro.features.InApp;


import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface InAppMvpPresenter <V extends InAppMvpView> extends MvpPresenter<V> {
    void getSubscriptionList();
}
