package com.bidjobbers.bidjobberspro.features.CustomerProfile;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.Archived.ArchiveDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Archived.ArchiveListRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Archived.ArchiveListResponse;
import com.bidjobbers.bidjobberspro.data.network.models.CacelledTask.CancelledTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.CacelledTask.CancelledTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.CompletedTask.CompletedTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.CompletedTask.CompletedTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Customer.CustomerDetailsRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Customer.CustomerDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Recivetask.ReciveTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Recivetask.ReciveTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ScheduleTask.ScheduletaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ScheduleTask.ScheduletaskResponse;
import com.bidjobbers.bidjobberspro.features.CustomerDetails.CustomerDetailsMvpPresenter;
import com.bidjobbers.bidjobberspro.features.CustomerDetails.CustomerDetailsMvpView;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class CustomerProfilePresenter <V extends CustomerProfileMvpView>
        extends BasePresenter<V>
        implements CustomerProfileMvpPresenter<V> {
    @Inject
    public CustomerProfilePresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }









    @Override
    public void onGetCustomer(String taskid) {
        CustomerDetailsRequest customerDetailsRequest=new CustomerDetailsRequest(getDataManager().getLocalValue(),taskid);
        getCompositeDisposable().add(getDataManager().getArchiveDetails("Bearer "+getDataManager().getAccess_token(),"application/json",customerDetailsRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<ArchiveDetailsResponse>() {
                    @Override
                    public void onSuccess(ArchiveDetailsResponse CustomerDetailsResponse) {


                        getMvpView().hideLoading();
                        if(CustomerDetailsResponse.getResponseCode()==1) {

                            getMvpView().onSuccessfullyGetCustomer(CustomerDetailsResponse);
                        }
                        else
                        {
                            getMvpView().onSuccessfullyGetCustomer(CustomerDetailsResponse);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));
    }
}
