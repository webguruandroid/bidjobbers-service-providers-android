package com.bidjobbers.bidjobberspro.features.OnTimeScreen.AccountInformation;

import com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessInfo.BusinessInfoFragmentMvpView;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface AccountInformationFragmentMvpPresenter <V extends AccountInformationFragmentMvpView> extends BaseFragmentMvpPresenter<V> {
}
