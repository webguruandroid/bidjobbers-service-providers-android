package com.bidjobbers.bidjobberspro.features.testfragment;

public class LawnBean {
    String image;
    String titel="";
    String desc = "";
    String id = "";
    Boolean check = false;

    public LawnBean(String image, String titel, String desc, String id) {
        this.image = image;
        this.titel = titel;
        this.desc = desc;
        this.id = id;
    }


    public Boolean getCheck() {
        return check;
    }

    public void setCheck(Boolean check) {
        this.check = check;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
