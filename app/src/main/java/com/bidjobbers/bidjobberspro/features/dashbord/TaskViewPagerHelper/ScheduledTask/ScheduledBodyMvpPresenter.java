package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.ScheduledTask;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface ScheduledBodyMvpPresenter<V extends ScheduledBodyMvpView> extends BaseFragmentMvpPresenter<V> {
     void getAllTasks(String currentpage);
}
