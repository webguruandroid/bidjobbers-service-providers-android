package com.bidjobbers.bidjobberspro.features.testfragment;

import android.os.Bundle;
import android.os.Handler;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;

import javax.inject.Inject;

public class TestFragmentActivity extends BaseActivity implements TestFragmentActivityMvpView {

    @Inject
    TestFragmentActivityPresenter<TestFragmentActivityMvpView> testPresenter;
    @Inject
    QuestionImageGalaryFragment questionImageGalaryFragment;

    FragmentManager fragmentManager ;
    FragmentTransaction fragmentTransaction ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_fragment);
        getActivityComponent().inject(this);
        fragmentManager = getSupportFragmentManager();

        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container, questionImageGalaryFragment).addToBackStack("firstFragment");
        fragmentTransaction.commit();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.remove(questionImageGalaryFragment);

                fragmentTransaction.setCustomAnimations(R.anim.slide_out_right, R.anim.slide_in_right);
                fragmentTransaction.add(R.id.fragment_container, questionImageGalaryFragment).addToBackStack("lawnFragment");
                fragmentTransaction.commit();
                //Do something after 100ms
            }
        }, 5000);
    }
}
