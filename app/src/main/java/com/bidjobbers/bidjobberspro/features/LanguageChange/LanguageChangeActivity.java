package com.bidjobbers.bidjobberspro.features.LanguageChange;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.SetLocal.SetLocalResponse;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;

import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LanguageChangeActivity extends BaseActivity implements LanguageMvpView{


    ImageView iv_french_tick , iv_english_tick;
    LinearLayout ll_english_tick, ll_french_tick;
    boolean isEnglish=false,is_french=false;
    Button btn_next;
    String tag,lang;

    @BindView(R.id.iv_mnu)
    ImageView iv_mnu;

    String currentLanguage = "";
    Locale myLocale;

    SharedPreferences sharedpreferences;

    @Inject
    DataManager dataManager;

    @Inject
    LanguagePresenter<LanguageMvpView> languagePresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language_change);
        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        languagePresenter.onAttach(this);

        iv_french_tick = (ImageView) findViewById(R.id.iv_french_tick);
        iv_english_tick =  (ImageView) findViewById(R.id.iv_english_tick);

        ll_english_tick =  (LinearLayout) findViewById(R.id.ll_english_tick);
        ll_french_tick =  (LinearLayout) findViewById(R.id.ll_french_tick);
        btn_next =  (Button) findViewById(R.id.btn_next);

        iv_french_tick.setBackgroundResource(R.drawable.ic_uncheck);
        iv_english_tick.setBackgroundResource(R.drawable.ic_uncheck);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LanguageChangeActivity.this);
        lang=prefs.getString("LOCAL_KEY","");

        if(lang.equals("en"))
        {
            iv_french_tick.setBackgroundResource(R.drawable.ic_uncheck);
            iv_english_tick.setBackgroundResource(R.drawable.ic_check);
        }
        else if(lang.equals("fr"))
        {
            iv_french_tick.setBackgroundResource(R.drawable.ic_check);
            iv_english_tick.setBackgroundResource(R.drawable.ic_uncheck);
        }
        else
        {
            iv_french_tick.setBackgroundResource(R.drawable.ic_uncheck);
            iv_english_tick.setBackgroundResource(R.drawable.ic_check);
        }

        //    Toast.makeText(getApplicationContext(),lang,Toast.LENGTH_SHORT).show();

        btn_next.
                setOnClickListener(
                        new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {


                                if(isEnglish)
                                {
                                    setLocale("en");
                                    languagePresenter.getSetLocal("en");
                                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LanguageChangeActivity.this);
                                    prefs.edit().putString("LOCAL_KEY", "en").commit();




                                }
                                else if (is_french)
                                {
                                    setLocale("fr");
                                    languagePresenter.getSetLocal("fr");
                                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LanguageChangeActivity.this);
                                    prefs.edit().putString("LOCAL_KEY", "fr").commit();


                                }
                                else
                                {
                                    setLocale("en");
                                    languagePresenter.getSetLocal("en");
                                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LanguageChangeActivity.this);
                                    prefs.edit().putString("LOCAL_KEY", "en").commit();

                                }
                            }
                        }
                );

        ll_english_tick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_french_tick.setBackgroundResource(R.drawable.ic_uncheck);
                iv_english_tick.setBackgroundResource(R.drawable.ic_check);

                isEnglish = true;
                is_french = false;
            }
        });
        ll_french_tick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                iv_french_tick.setBackgroundResource(R.drawable.ic_check);
                iv_english_tick.setBackgroundResource(R.drawable.ic_uncheck);

                isEnglish = false;
                is_french = true;
            }
        });

        iv_mnu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                //  overridePendingTransition( R.anim.slide_from_top,R.anim.slide_in_top);
            }
        });

    }



    public void setLocale(String localeName) {


        dataManager.setLocalValue(lang);
        if (!localeName.equals(currentLanguage)) {

            Log.e("Language--Page",localeName);

            myLocale = new Locale(localeName);
            Resources res = getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();

            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);


            new Handler().postDelayed(new Runnable()
            {
                @Override
                public void run()
                {



                    Intent i = new Intent(LanguageChangeActivity.this, LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();



                }
            }, 500);




        } else {
            Toast.makeText(this, getResources().getString(R.string.Languaglreadyselected), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void successfullySEtLocal(SetLocalResponse setLocalResponse) {

    }
}
