package com.bidjobbers.bidjobberspro.features.TearmsandCondition;

import com.bidjobbers.bidjobberspro.features.allreviews.AllReviewsMvpView;
import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface TearmsandConditionMvpPresenter  <V extends TearmsandConditionMvpView> extends MvpPresenter<V> {
    void getAllPages(String sug);
}
