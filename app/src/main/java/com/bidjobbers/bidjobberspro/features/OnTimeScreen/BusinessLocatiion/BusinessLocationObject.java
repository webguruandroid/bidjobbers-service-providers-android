package com.bidjobbers.bidjobberspro.features.OnTimeScreen.BusinessLocatiion;

public class BusinessLocationObject {

    String businessAddress="";
    String apartment="";
    String city="";
    String state="";
    String country="";
    String pinCode="";
    String lat="";
    String log="";

    public BusinessLocationObject(String businessAddress, String apartment, String city, String state, String country, String pinCode, String lat, String log) {
        this.businessAddress = businessAddress;
        this.apartment = apartment;
        this.city = city;
        this.state = state;
        this.country = country;
        this.pinCode = pinCode;
        this.lat = lat;
        this.log = log;
    }

    @Override
    public String toString() {
        return "BusinessLocationObject{" +
                "businessAddress='" + businessAddress + '\'' +
                ", apartment='" + apartment + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", country='" + country + '\'' +
                ", pinCode='" + pinCode + '\'' +
                ", lat='" + lat + '\'' +
                ", log='" + log + '\'' +
                '}';
    }

    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }



}
