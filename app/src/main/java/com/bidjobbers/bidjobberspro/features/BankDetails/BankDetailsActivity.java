package com.bidjobbers.bidjobberspro.features.BankDetails;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.BankDetails.BankDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList.BlockCustomerListResponse;
import com.bidjobbers.bidjobberspro.features.Archive.ArchiveMvpView;
import com.bidjobbers.bidjobberspro.features.Archive.ArchivePresenter;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.FirstTimeAccountSettingsActivity;
import com.bidjobbers.bidjobberspro.features.dashbord.Main2Activity;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BankDetailsActivity extends BaseActivity implements BankDetailsMvpView ,View.OnClickListener{


    @BindView(R.id.ll_bank)
    LinearLayout ll_bank;
    @BindView(R.id.ll_paypal)
    LinearLayout ll_paypal;

    @BindView(R.id.cb_bank)
    CheckBox cb_bank;
    @BindView(R.id.cb_paypal)
    CheckBox cb_paypal;
    @BindView(R.id.ll_bank_content)
    LinearLayout ll_bank_content;
    @BindView(R.id.et_bank_name)
    EditText et_bank_name;
    @BindView(R.id.et_bank_holder_name)
    EditText et_bank_holder_name;
    @BindView(R.id.et_accout_number)
    EditText et_accout_number;
    @BindView(R.id.edt_routing_number)
    EditText edt_routing_number;
    @BindView(R.id.edt_bank_iban)
    EditText edt_bank_iban;
    @BindView(R.id.sp_bank_currency)
    Spinner sp_bank_currency;
    @BindView(R.id.edt_swift_no)
    EditText edt_swift_no;
    @BindView(R.id.sp_bank_country)
    EditText sp_bank_country;
    @BindView(R.id.ll_bank_country)
     LinearLayout ll_bank_country;
    @BindView(R.id.et_paypal_id)
    EditText et_paypal_id;
    @BindView(R.id.sp_paypal_currency)
    Spinner sp_paypal_currency;
    @BindView(R.id.ll_paypal_content)
    LinearLayout ll_paypal_content;
    @BindView(R.id.btn_next)
    LinearLayout btn_next;
    @BindView(R.id.tv_skip)
    TextView tv_skip;
    @BindView(R.id.iv_mnu)
    ImageView iv_mnu;
    @BindView(R.id.et_paypal_id_confirm)
    EditText et_paypal_id_confirm;
    @BindView(R.id.et_accout_number_retype)
    EditText et_accout_number_retype;
    @BindView(R.id.et_bank_holder_last_name)
    EditText et_bank_holder_last_name;
    int count = 0;
    CountryPicker picker;

    SharedPreferences mBank;
    Context mContext;
    @Inject
    BankDetailsPresenter<BankDetailsMvpView> bankDetailsPresenter;
    private ArrayList<String> currency;
    private String Country="", Currency,Paypal_Currency,tag,transcationId;
    private boolean isBank,isPaypal;

    public static String insertPeriodically(
            String text, String insert, int period)
    {
        StringBuilder builder = new StringBuilder(
                text.length() + insert.length() * (text.length()/period)+1);

        int index = 0;
        String prefix = "";
        while (index < text.length())
        {
            // Don't put the insert in the very first iteration.
            // This is easier than appending it *after* each substring
            builder.append(prefix);
            prefix = insert;
            builder.append(text.substring(index,
                    Math.min(index + period, text.length())));
            index += period;
        }
        return builder.toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_details);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        mContext = this;
        mBank=getSharedPreferences("Bidjobbers_Bank", Context.MODE_PRIVATE);
        bankDetailsPresenter.onAttach(this);
         picker = CountryPicker.newInstance("Select Country");



         getIntentData();
         setCountry();
         setCurrency();

        bankDetailsPresenter.onGetDataFromApi();
        setUpUI();
    }

    private void getIntentData()
    {
        tag=getIntent().getStringExtra("Tag");
        switch (tag)
        {
            case "Profile":
                tv_skip.setVisibility(View.GONE);
                break;
            case "First":
                tv_skip.setVisibility(View.VISIBLE);
                break;
            case "Home":
                tv_skip.setVisibility(View.GONE);
                break;
        }



    }

    private void setCountry()
    {

        sp_bank_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                Country=name;
                sp_bank_country.setText(name);
                picker.dismiss();
            }
        });
    }

    private void setCurrency()
    {
        currency=new ArrayList<>();
        currency.add("Select currency");
        currency.add("EUR");
        currency.add("USD");
        currency.add("ANG");

        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,currency);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_bank_currency.setAdapter(aa);
        sp_paypal_currency.setAdapter(aa);

        sp_bank_currency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Currency=currency.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Currency="USD";
            }
        });
        sp_paypal_currency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Paypal_Currency=currency.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Paypal_Currency="USD";
            }
        });


    }

    private void setUpUI()
    {
        ll_bank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isBank)
                cb_bank.setChecked(false);
                else
                    cb_bank.setChecked(true);
            }
        });
        ll_paypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isPaypal)
                    cb_paypal.setChecked(false);
                else
                    cb_paypal.setChecked(true);
            }
        });


        cb_bank.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    isBank=b;
                    ll_bank_content.setVisibility(View.VISIBLE);
                }
                else
                {
                    isBank=b;
                    ll_bank_content.setVisibility(View.GONE);
                }
            }
        });

        cb_paypal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    isPaypal=b;
                    ll_paypal_content.setVisibility(View.VISIBLE);
                }
                else
                {
                    isPaypal=b;
                    ll_paypal_content.setVisibility(View.GONE);
                }
            }
        });

        edt_bank_iban.addTextChangedListener(new TextWatcher() {
            private static final char space = ' ';

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                char space = ' ';

                if (s.length() > 0 && (s.length() % 5) == 0)
                {
                    char c = s.charAt(s.length() - 1);

                    if (Character.isDigit(c)||Character.isAlphabetic(c))
                    {
                        s.insert(s.length() - 1, String.valueOf(space));
                    }
                }
            }
        });

    }

    @OnClick({R.id.btn_next,R.id.tv_skip,R.id.iv_mnu})
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.btn_next:
                validation(isBank,isPaypal,et_bank_name.getText().toString().trim(),et_bank_holder_name.getText().toString().trim(),et_bank_holder_last_name.getText().toString().trim(),et_accout_number.getText().toString().trim(),
                        et_accout_number_retype.getText().toString().trim() ,edt_routing_number.getText().toString().trim(),Country,Currency,edt_swift_no.getText().toString().trim(),edt_bank_iban.getText().toString().trim(),et_paypal_id.getText().toString().trim()
                        ,et_paypal_id_confirm.getText().toString().trim()
                ,Paypal_Currency);
                break;
            case R.id.tv_skip:
                if(tag.equals("First"))
                {
                    Intent i=new Intent(this, Main2Activity.class);
                    startActivity(i);
                    finish();
                }
                break;
            case R.id.iv_mnu:
                onBackPressed();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void validation(Boolean isBank, Boolean isPaypal, String bankName, String accountHolderName,String accountHolderLastName, String bankAccountNumber,String bankaccountRe, String routingNumber, String bankCountry, String bankCurrency, String bankSwift, String bankIban, String paypalId, String paypalidRe,String paypalCurrency)
    {

        Log.e("BANK",isBank+"-"+isPaypal+"-"+bankName+"-"+accountHolderName+"-"+bankAccountNumber+"-"+routingNumber+"-"+bankCountry+"="+bankCurrency+"_"+bankSwift+"_"+bankIban+"-"+paypalId+"-"+paypalCurrency);

       if(isBank&&!isPaypal)
       {
           Log.e("BANK",isBank+"-"+isPaypal+"-"+bankName+"-"+accountHolderName+"-"+bankAccountNumber+"-"+routingNumber+"-"+bankCountry+"="+bankCurrency+"_"+bankSwift+"_"+bankIban+"-"+paypalId+"-"+paypalCurrency);

           paypalCurrency="";
           paypalId="";
           if(bankName.equals(""))
           {
               Toast.makeText(getApplicationContext(),"Please Enter Bank Name",Toast.LENGTH_SHORT).show();
               return;
           }
           else if(accountHolderName.equals(""))
           {
               Toast.makeText(getApplicationContext(),"Please Enter Account Holder First Name",Toast.LENGTH_SHORT).show();
               return;
           }
           else if(accountHolderLastName.equals(""))
           {
               Toast.makeText(getApplicationContext(),"Please Enter Account Holder Last Name",Toast.LENGTH_SHORT).show();
               return;
           }
           else if(bankAccountNumber.equals("")||bankAccountNumber.length()<6)
           {
               Toast.makeText(getApplicationContext(),"Please Valid Account Number",Toast.LENGTH_SHORT).show();
               return;
           }
//           else if(bankaccountRe.equals(""))
//           {
//               Toast.makeText(getApplicationContext(),"Please Re-Enter Account Number",Toast.LENGTH_SHORT).show();
//               return;
//           }
//           else if(!bankaccountRe.matches(bankAccountNumber))
//           {
//               Toast.makeText(getApplicationContext(),"Account Numbers not matches",Toast.LENGTH_SHORT).show();
//               return;
//           }

//           else if(routingNumber.equals(""))
//           {
//               Toast.makeText(getApplicationContext(),"Please Enter Routing Number",Toast.LENGTH_SHORT).show();
//               return;
//           }
           else if(bankCountry.equals(""))
           {
               Toast.makeText(getApplicationContext(),"Please Enter Bank Country",Toast.LENGTH_SHORT).show();
               return;
           }
           else if(bankCurrency.equals("Select currency"))
           {
               Toast.makeText(getApplicationContext(),"Please Enter Bank Currency",Toast.LENGTH_SHORT).show();
               return;
           }
           else if(bankSwift.equals(""))
           {
               Toast.makeText(getApplicationContext(),"Please Enter Bank Swift",Toast.LENGTH_SHORT).show();
               return;
           }
           else if(bankIban.equals(""))
           {
               Toast.makeText(getApplicationContext(),"Please Enter Bank Iban",Toast.LENGTH_SHORT).show();
               return;
           }

           else if(bankIban.length()<10)
           {
               Toast.makeText(getApplicationContext(),"IBAN number should be minimum ten character",Toast.LENGTH_SHORT).show();
               return;
           }


       }
       else if(isPaypal&&!isBank)
       {
           if(paypalId.equals("")||paypalId.length()<6)
           {
               Toast.makeText(getApplicationContext(),"Please Enter Valid Paypal Id",Toast.LENGTH_SHORT).show();
               return;
           }
//           if(paypalidRe.equals(""))
//           {
//               Toast.makeText(getApplicationContext(),"Please Re-Enter Paypal Id",Toast.LENGTH_SHORT).show();
//               return;
//           }
//           if(!paypalidRe.matches(paypalId))
//           {
//               Toast.makeText(getApplicationContext(),"PaypalId not matches",Toast.LENGTH_SHORT).show();
//               return;
//           }
           else if(paypalCurrency.equals("Select currency"))
           {
               Toast.makeText(getApplicationContext(),"Please Enter Paypal Currency",Toast.LENGTH_SHORT).show();
               return;
           }

           bankName="";
           accountHolderName="";
           bankAccountNumber="";
           routingNumber="";
           bankCountry="";
           bankCurrency="";
           bankSwift="";
           bankIban="";
       }

       else if(isBank && isPaypal)
       {
           if(bankName.equals(""))
           {
               Toast.makeText(getApplicationContext(),"Please Enter Bank Name",Toast.LENGTH_SHORT).show();
               return;
           }
           else if(accountHolderName.equals(""))
           {
               Toast.makeText(getApplicationContext(),"Please Enter Account Holder Name",Toast.LENGTH_SHORT).show();
               return;
           }
           else if(bankAccountNumber.equals("")||bankAccountNumber.length()<6)
           {
               Toast.makeText(getApplicationContext(),"Please Valid Account Number",Toast.LENGTH_SHORT).show();
               return;
           }
//           else if(bankaccountRe.equals(""))
//           {
//               Toast.makeText(getApplicationContext(),"Please Re-Enter Account Number",Toast.LENGTH_SHORT).show();
//               return;
//           }
//           else if(!bankaccountRe.matches(bankAccountNumber))
//           {
//               Toast.makeText(getApplicationContext(),"Account Numbers not matches",Toast.LENGTH_SHORT).show();
//               return;
//           }
//           else if(routingNumber.equals(""))
//           {
//               Toast.makeText(getApplicationContext(),"Please Enter Routing Number",Toast.LENGTH_SHORT).show();
//               return;.
//           }
           else if(bankCountry.equals(""))
           {
               Toast.makeText(getApplicationContext(),"Please Enter Bank Country",Toast.LENGTH_SHORT).show();
               return;
           }
           else if(bankCurrency.equals("Select currency"))
           {
               Toast.makeText(getApplicationContext(),"Please Enter Bank Currency",Toast.LENGTH_SHORT).show();
               return;
           }
           else if(bankSwift.equals(""))
           {
               Toast.makeText(getApplicationContext(),"Please Enter Bank Swift",Toast.LENGTH_SHORT).show();
               return;
           }
           else if(bankIban.equals(""))
           {
               Toast.makeText(getApplicationContext(),"Please Enter Bank Iban",Toast.LENGTH_SHORT).show();
               return;
           }
           else if(bankIban.length()<10)
           {
               Toast.makeText(getApplicationContext(),"IBAN number should be minimum ten character",Toast.LENGTH_SHORT).show();
               return;
           }


           else if(paypalId.equals("")||paypalId.length()<6)
           {
               Toast.makeText(getApplicationContext(),"Please Enter Valid Paypal Id",Toast.LENGTH_SHORT).show();
               return;
           }
//           if(paypalidRe.equals(""))
//           {
//               Toast.makeText(getApplicationContext(),"Please Re-Enter Paypal Id",Toast.LENGTH_SHORT).show();
//               return;
//           }
//           if(!paypalidRe.matches(paypalId))
//           {
//               Toast.makeText(getApplicationContext(),"PaypalId not matches",Toast.LENGTH_SHORT).show();
//               return;
//           }
           else if(paypalCurrency.equals("Select currency"))
           {
               Toast.makeText(getApplicationContext(),"Please Enter Paypal Currency",Toast.LENGTH_SHORT).show();
               return;
           }
       }


       else
       {
           Toast.makeText(getApplicationContext(),"Please Enter Bank Information",Toast.LENGTH_SHORT).show();
           return;
       }

       bankDetailsPresenter.submitBankDetails(bankName,accountHolderName,accountHolderLastName,bankAccountNumber,routingNumber,bankCountry,bankCurrency,bankSwift,bankIban,paypalId,paypalCurrency,transcationId);


    }

    @Override
    public void successfullyUpdateBankDetails(BankDetailsResponse bankDetailsResponse) {
        SharedPreferences.Editor myEdit
                = mBank.edit();

        myEdit.putString("BankInfo", "yes");
        myEdit.apply();
        myEdit.commit();

        if(tag.equals("First"))
        {
            Intent i=new Intent(this, Main2Activity.class);
            startActivity(i);
            finish();
        }

    }

    @Override
    public void setDataFromApi(DataModelForFirstTimeQusAns.BankDetails bankDetails) {


        if (mBank.getString("BankInfo","").equals("yes"))  {
            if (bankDetails.getBank_account_number()!=null&&!bankDetails.getBank_account_number().equals("")) {
                isBank = true;
                cb_bank.setChecked(true);
                ll_bank_content.setVisibility(View.VISIBLE);
                et_accout_number.setText(bankDetails.getBank_account_number());
                et_bank_holder_name.setText(bankDetails.getBank_account_holder_name());
                et_bank_holder_last_name.setText(bankDetails.getBank_account_holder_last_name());
                et_bank_name.setText(bankDetails.getBank_name());
                String a=bankDetails.getBank_iban().replaceAll(" ","");
                edt_bank_iban.setText(insertPeriodically(a," ",4));
                edt_routing_number.setText(bankDetails.getBank_routing_number());
                edt_swift_no.setText(bankDetails.getBank_swift());
                sp_bank_country.setText(bankDetails.getBank_country());
                Country = bankDetails.getBank_country();
                Currency = bankDetails.getBank_currency();

                for (int i = 0; i < currency.size(); i++) {
                    if (bankDetails.getBank_currency().equals(currency.get(i))) {
                        sp_bank_currency.setSelection(i);
                    }
                }

            }
            if (!bankDetails.getPaypal_id().trim().equals("")) {
                isPaypal = true;
                et_paypal_id.setText(bankDetails.getPaypal_id());
                Paypal_Currency = bankDetails.getPaypal_currency();

                cb_paypal.setChecked(true);
                ll_paypal_content.setVisibility(View.VISIBLE);
                for (int i = 0; i < currency.size(); i++) {
                    if (bankDetails.getPaypal_currency().equals(currency.get(i))) {
                        sp_paypal_currency.setSelection(i);
                    }
                }
            }

            if (bankDetails.getBank_account_number().trim().equals("") && bankDetails.getPaypal_id().trim().equals("")) {
                isPaypal = false;
                isBank = false;


            }
            transcationId = bankDetails.getTransaction_account_id();
           // Toast.makeText(getApplicationContext(), transcationId + "", Toast.LENGTH_SHORT).show();

        }
    }
}