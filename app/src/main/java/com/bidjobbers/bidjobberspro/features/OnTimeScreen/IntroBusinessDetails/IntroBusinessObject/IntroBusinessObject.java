package com.bidjobbers.bidjobberspro.features.OnTimeScreen.IntroBusinessDetails.IntroBusinessObject;

public class IntroBusinessObject {

    String des;

    public IntroBusinessObject(String des) {
        this.des = des;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }
}
