package com.bidjobbers.bidjobberspro.features.splash;

import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface SplashMvpView  extends MvpView {

    void onSuccessfullyDecideNavigation(String flag);
}
