package com.bidjobbers.bidjobberspro.features.testfragment;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface QuestionImageGalaryFragmentMvpPresenter <V extends QuestionImageGalaryFragmentMvpView> extends BaseFragmentMvpPresenter<V> {


    void onGetDataFromApi();


public void onTestPresenter();

    //=======All Apply button action ==================

    public void onApplyMonday();
    public void onApplyTuesday();
    public void onApplyWednesday();
    public void onApplyThree();
    public void onApplyFriday();
    public void onApplySaturday();
    public void onApplySunday();

    //=======All Edit button===========================

    public void onEditOne();
    public void onEditTwo();
    public void onEditThree();
    public void onEditFour();
    public void onEditFive();
    public void onEditSix();
    public void onEditSeven();

    //=======All Done button ==========================

    public void onDoneOne();
    public void onDoneTwo();
    public void onDoneThree();
    public void onDoneFour();
    public void onDoneFive();
    public void onDoneSix();
    public void onDoneSeven();


    //=======All Check box ============================

    public void onChkMonday(boolean flag);
    public void onChkTuesday(boolean flag);
    public void onChkWednesday(boolean flag);
    public void onChkThusday(boolean flag);
    public void onChkFriday(boolean flag);
    public void onChkSaturday(boolean flag);
    public void onChkSunday(boolean flag);


    //===========Spinner Select Action =================

    public void onSpin1(int pos);
    public void onSpinEnd1(int pos);

    public void onSpin2(int pos);
    public void onSpinEnd2(int pos);

    public void onSpin3(int pos);
    public void onSpinEnd3(int pos);

    public void onSpin4(int pos);
    public void onSpinEnd4(int pos);

    public void onSpin5(int pos);
    public void onSpinEnd5(int pos);

    public void onSpin6(int pos);
    public void onSpinEnd6(int pos);

    public void onSpin7(int pos);
    public void onSpinEnd7(int pos);


    //========initial api call method ===============

    public void onInit(String from );

}
