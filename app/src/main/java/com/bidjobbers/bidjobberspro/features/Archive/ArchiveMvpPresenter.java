package com.bidjobbers.bidjobberspro.features.Archive;

import com.bidjobbers.bidjobberspro.features.allreviews.AllReviewsMvpView;
import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface ArchiveMvpPresenter <V extends ArchiveMvpView> extends MvpPresenter<V> {

    void getBlockCustomer(String timePeriod,String current_page);
    void onUnBlockUser(String customerId);
}
