package com.bidjobbers.bidjobberspro.features.TestMVP;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.test.TestResponse;
import com.bidjobbers.bidjobberspro.features.TestMVP.savedata.DataBean;
import com.bidjobbers.bidjobberspro.features.TestMVP.savedata.TestPersistData;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class TestPresenter <V extends TestMvpView> extends BasePresenter<V> implements TestMvpPresenter<V>{
    @Inject
    public TestPresenter(
            DataManager dataManager, SchedulerProvider schedulerProvider,
            CompositeDisposable compositeDisposable)
    {
        super(dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void actionTestButtonPress(String data) {
        getCompositeDisposable().add(

        //getDataManager().getHomeData() -> Single
        //The diffrent types of Observable
        // Observable, Flowable, Single, Maybe, Completable

        //Single ->  emits only a single item

        // So According to observable the
        // Observer are -> Observer, SingleObserver,MaybeObserver,ComputableObserver

         getDataManager().getHomeData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<TestResponse>()
                {

                    @Override
                    public void onSuccess(TestResponse testResponse) {

                        ArrayList<DataBean> dataBeanArrayList = new ArrayList<>();
                        for (int r=0;r<testResponse.getResponseData().getData().size();r++) {

                            DataBean dataBean = new DataBean(testResponse.getResponseData().getData().get(r).getData(),
                                    testResponse.getResponseData().getData().get(r).getData());

                            dataBeanArrayList.add(dataBean);
                        }

                        TestPersistData testPersistData = new TestPersistData(dataBeanArrayList);
                        getMvpView().onTestResult(testPersistData);

                    }

                    @Override
                    public void onError(Throwable e) {

                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));
    }

    public void displayData(String Name)
    {
        getMvpView().onDisplayData(Name);

    }
}
