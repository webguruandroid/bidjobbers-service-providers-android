package com.bidjobbers.bidjobberspro.features.MapCustomerDetails;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.EasyPermissions;

public class MapCustomerDetailsActivity extends BaseActivity implements MapCustomerDetailsMvpView,
        OnMapReadyCallback,
        EasyPermissions.PermissionCallbacks, View.OnClickListener {


    public static final int RC_LOC_PERM = 101;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final int REQUEST_CHECK_SETTINGS = 1452;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    Context mContext;
    Marker mCurrLocationMarker;
    SupportMapFragment mapFragment;
    Bundle bundle=null;
    String search="1",Latitude,Longitude;
    Double lat=16.261576,lng=-61.519523;
    @BindView(R.id.tvHeader)
    TextView tvHeader;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_customer_details);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);

        bundle=getIntent().getExtras();
        if (bundle!=null){
            search=bundle.getString("search");
            Latitude=bundle.getString("Latitude");
            Longitude=bundle.getString("Longitude");
            if (Latitude.equals("0.0")&&Longitude.equals("0.0"))
            {
                lat=16.261576;
                lng=-61.519523;

            }
            else if(Latitude.equals("")&&Longitude.equals(""))
            {
                lat=16.261576;
                lng=-61.519523;
            }
            else
            {
                lat=Double.parseDouble(Latitude);
                lng=Double.parseDouble(Longitude);
            }

            tvHeader.setText(getResources().getString(R.string.customeraddress));



        }
        mContext = this;
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    @OnClick({R.id.iv_back})
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.iv_back:
                finish();
                break;

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setPadding(0,100,10,0);
        LatLng latLng = new LatLng(lat, lng);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        mMap.clear();
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        markerOptions.getPosition();
        mCurrLocationMarker = mMap.addMarker(markerOptions);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }
}
