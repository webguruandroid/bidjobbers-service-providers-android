package com.bidjobbers.bidjobberspro.features.VerifySignup;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.resentotp.ResendOtpRequest;
import com.bidjobbers.bidjobberspro.data.network.models.resentotp.ResendOtpResponse;
import com.bidjobbers.bidjobberspro.data.network.models.verifyotp.VerifyOtpRequest;
import com.bidjobbers.bidjobberspro.data.network.models.verifyotp.VerifyOtpResponse;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.CommonUtils;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class VerifySignupPresenter<V extends  VerifySignupMvpView>  extends BasePresenter<V> implements  VerifySignupMvpPresenter<V> {


    @Inject
    public VerifySignupPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void actionVerifyOtp(String otp, String emailId,String otp_for) {

      //  getMvpView().showMessage("otp:"+otp);
       // getMvpView().showMessage("emailId:"+emailId);
    //    getMvpView().showMessage("otp_for:"+otp_for);

        VerifyOtpRequest verifyOtpRequest = new VerifyOtpRequest(otp,emailId,getDataManager().getLocalValue(),otp_for);

        getCompositeDisposable().add(
                getDataManager().getVeryResponse(verifyOtpRequest)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<VerifyOtpResponse>()
                        {
                            @Override
                            public void onSuccess(VerifyOtpResponse testResponse)
                            {
                                if(testResponse.getResponseCode()==1)
                                {
                                    getDataManager().setIsEmailVarified("1");
                                    getDataManager().setIsQuestionFillup("0");
                                    getMvpView().onOtpValid(false, testResponse.getResponseText(),getDataManager().getAccess_token());
                                }
                                else if(testResponse.getResponseCode()==401)
                                {
                                    getMvpView().onError(testResponse.getResponseText());
                                    getMvpView().onDeactivate(testResponse.getResponseText());
                                }
                                else
                                {
                                    getMvpView().onOtpValid(true, testResponse.getResponseText(),getDataManager().getAccess_token());
                                }
                            }
                            @Override
                            public void onError(Throwable e)
                            {
                                Log.e("ERROR", "onError: "+e.getMessage());
                            }

                        }));
    }

    @Override
    public void actionResentOtp(String email, String otp_for, String locale)
    {
          if(email.isEmpty())
          {
              getMvpView().showMessage("Enter Email Address");
              return;
          }
          if(!CommonUtils.isEmailValid(email.toString()))
          {
              getMvpView().showMessage("Enter Valid Email Address");
              return;
          }

          ResendOtpRequest resendOtpRequest = new ResendOtpRequest(email.trim(),getDataManager().getLocalValue(),otp_for);
          getCompositeDisposable().add(
                getDataManager().getSentOtp(resendOtpRequest)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<ResendOtpResponse>()
                        {
                            @Override
                            public void onSuccess(ResendOtpResponse testResponse)
                            {
                                if(testResponse.getResponseCode()==1)
                                {
                                    //getMvpView().onOtpValid(false, testResponse.getResponseText(),getDataManager().getAccess_token());
                                    getDataManager().setIsEmailVarified("1");
                                    getDataManager().setIsQuestionFillup("0");
                                     getMvpView().showMessage(testResponse.getResponseText());
                                }
                                else if(testResponse.getResponseCode()==401)
                                {
                                    getMvpView().onError(testResponse.getResponseText());
                                    getMvpView().onDeactivate(testResponse.getResponseText());
                                }
                                else
                                {
                                    //getMvpView().onOtpValid(true, testResponse.getResponseText(),getDataManager().getAccess_token());
                                    getMvpView().showMessage(testResponse.getResponseText());
                                }
                            }
                            @Override
                            public void onError(Throwable e)
                            {
                                Log.e("ERROR", "onError: "+e.getMessage());
                            }

                        }));

    }
}
