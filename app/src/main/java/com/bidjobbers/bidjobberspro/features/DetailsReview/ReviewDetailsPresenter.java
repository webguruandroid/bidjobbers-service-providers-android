package com.bidjobbers.bidjobberspro.features.DetailsReview;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class ReviewDetailsPresenter  <V extends ReviewDetailsMvpView>
        extends BasePresenter<V>
        implements ReviewDetailsMvpPresenter<V> {

    @Inject
    public ReviewDetailsPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);
    }
}
