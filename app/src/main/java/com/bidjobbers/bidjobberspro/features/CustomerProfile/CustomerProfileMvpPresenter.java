package com.bidjobbers.bidjobberspro.features.CustomerProfile;

import com.bidjobbers.bidjobberspro.features.CustomerDetails.CustomerDetailsMvpView;
import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface CustomerProfileMvpPresenter <V extends CustomerProfileMvpView> extends MvpPresenter<V> {
    void onGetCustomer(String taskid);

}
