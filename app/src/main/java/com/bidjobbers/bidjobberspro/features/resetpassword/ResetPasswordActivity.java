package com.bidjobbers.bidjobberspro.features.resetpassword;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResetPasswordActivity extends BaseActivity implements ResetPasswordMvpView{



/*
    @BindView(R.id.input_oldpass)
    EditText input_oldpass;*/

    @BindView(R.id.input_newpass)
    EditText input_newpass;

    @BindView(R.id.input_houseAddress)
    EditText input_cofirmpass;

    @Inject
    ResetPasswordPresenter<ResetPasswordMvpView> resetPasswordPresenter;
    boolean doubleBackToExitPressedOnce = false;


    String emailid = "";
    String otp = "";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        resetPasswordPresenter.onAttach(this);

        Intent intent = getIntent();
        emailid = intent.getStringExtra("email");
         otp = intent.getStringExtra("otp");

        Log.e("emailidJ",emailid);


    }


    public void resetPassWord(View view)
    {

        resetPasswordPresenter.changePassWord("",
                input_newpass.getText().toString().trim(),
                input_cofirmpass.getText().toString().trim(),
                emailid,
                otp);

    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finish();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);}

    @Override
    public void goToLoginScreen() {


        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
        finish();


    }

    @Override
    public void doDeActivate(String data) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i = new Intent(ResetPasswordActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        }, 800);
    }
}
