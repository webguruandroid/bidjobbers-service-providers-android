package com.bidjobbers.bidjobberspro.features.OnTimeScreen.personaldetails;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

import okhttp3.MultipartBody;

public interface PersonalDetailsFragmentMvpPresenter <V extends PersonalDetailsFragmentMvpView> extends BaseFragmentMvpPresenter<V> {


    void onSuccessfullGetProfile(String language);
    void onUpdateProfile(MultipartBody.Part img_body, String name, String phone, String local);

}
