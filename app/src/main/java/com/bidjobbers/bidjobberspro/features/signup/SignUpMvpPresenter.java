package com.bidjobbers.bidjobberspro.features.signup;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

import okhttp3.MultipartBody;

public interface SignUpMvpPresenter <V extends SignupMvpView> extends MvpPresenter<V> {


    public void onSignUpButtonClick(String companyName, String firstAndLastName,
                                    String emailAddress, String phoneNumber, String password,
                                    MultipartBody.Part img_body);

    public void onSignUpButtonWithOutImageClick(String companyName, String firstAndLastName,
                                    String emailAddress, String phoneNumber, String password );



}
