package com.bidjobbers.bidjobberspro.features.TestMVP;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface TestMvpPresenter<V extends TestMvpView> extends MvpPresenter<V> {
    void actionTestButtonPress(String data);
}
