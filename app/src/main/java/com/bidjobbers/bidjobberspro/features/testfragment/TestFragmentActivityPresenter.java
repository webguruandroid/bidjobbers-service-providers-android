package com.bidjobbers.bidjobberspro.features.testfragment;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.shared.base.BasePresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class TestFragmentActivityPresenter  <V extends TestFragmentActivityMvpView> extends BasePresenter<V> implements TestFragmentActivityMvpPresenter<V>{

    @Inject
    public TestFragmentActivityPresenter(DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(dataManager, schedulerProvider, compositeDisposable);

    }


    @Override
    public void init(String from) {



    }
}
