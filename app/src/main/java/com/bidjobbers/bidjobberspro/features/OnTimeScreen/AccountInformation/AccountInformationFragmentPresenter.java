package com.bidjobbers.bidjobberspro.features.OnTimeScreen.AccountInformation;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import io.reactivex.disposables.CompositeDisposable;

public class AccountInformationFragmentPresenter<V extends AccountInformationFragmentMvpView>
        extends BaseFragmentPresenter<V>
        implements AccountInformationFragmentMvpPresenter<V> {

    public AccountInformationFragmentPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }
}
