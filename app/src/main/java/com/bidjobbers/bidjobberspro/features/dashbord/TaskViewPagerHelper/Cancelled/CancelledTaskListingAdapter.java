package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.Cancelled;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.CacelledTask.CancelledTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.CompletedTask.CompletedTaskResponse;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.Completed.CompletedTaskListingAdapter;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.OnLoadMoreListener;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CancelledTaskListingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    public AddButtonListener mListener;
    ArrayList<CancelledTaskResponse.ResponseDataBean.CancelledTaskBean> mValues;
    Context mContext;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private boolean isLoading;
    private int visibleThreshold = 3;
    private int lastVisibleItem, totalItemCount;
    private OnLoadMoreListener mOnLoadMoreListener;


    public CancelledTaskListingAdapter(ArrayList<CancelledTaskResponse.ResponseDataBean.CancelledTaskBean> items, Context context, RecyclerView mRecyclerView)
    {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                Log.e("load",totalItemCount+""+lastVisibleItem);

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                        Log.e("load",totalItemCount+" "+lastVisibleItem+" "+visibleThreshold);
                    }
                    isLoading = true;
                }
            }
        });
        mValues = items;
        mContext = context;

    }

    public void loadList(List<CancelledTaskResponse.ResponseDataBean.CancelledTaskBean> items) {
        Log.e("Task",items.size()+"");
//        mValues.clear();
//        mValues.addAll(items);
//        notifyDataSetChanged();
    }
    public void setLoaded(){
        this.isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return mValues.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
//        if (isLoading) {
//            return mValues.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
//        } else {
//            return VIEW_TYPE_ITEM;
//        }
    }


    public void setmListener(AddButtonListener mListener) {
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public  CancelledTaskListingAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_task, parent, false);
        return new CancelledTaskListingAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull  RecyclerView.ViewHolder holder,final int i) {


        if (holder instanceof CancelledTaskListingAdapter.ViewHolder) {
            CancelledTaskListingAdapter.ViewHolder mViewHolder = (CancelledTaskListingAdapter.ViewHolder) holder;
       final CancelledTaskResponse.ResponseDataBean.CancelledTaskBean mDataBean = mValues.get(i);
            mViewHolder.tvtaskId.setVisibility(View.VISIBLE);
        String currentString = mDataBean.getService_timing();
            mViewHolder.ll_requested_on.setVisibility(View.GONE);
        String[] separated = currentString.split(" ");
            mViewHolder.tvDate.setText(setDate(mDataBean.getDate()));
            mViewHolder.tvTime.setText(mDataBean.getSlot());
            mViewHolder.tvStatus.setTextColor(mContext.getResources().getColor(R.color.red));
            mViewHolder.tvStatus.setText(mDataBean.getStatus());
            mViewHolder.tvAmount.setText(mDataBean.getPrice());
            mViewHolder.tvService.setText(mDataBean.getServide_needed());
        Glide.with(mContext).load(mDataBean.getCustomer_profile_image())
                .into(mViewHolder.iv_prc);
            mViewHolder.tvtaskId.setText(" "+mDataBean.getService_request_id());

            mViewHolder.ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(i,mDataBean.getStatus_id()+"",mDataBean.getTask_id());
            }
        });

        }else if (holder instanceof CancelledTaskListingAdapter.LoadingViewHolder) {
            CancelledTaskListingAdapter.LoadingViewHolder loadingViewHolder = (CancelledTaskListingAdapter.LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {


        return mValues.size();
    }

    public String setDate(String date)
    {
        String dayName="";



        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = null;
        try {
            date1 = fmt.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);


        switch (cal. get(Calendar. DAY_OF_WEEK))
        {
            case 1:

                dayName="Sunday";
                break;
            case 2:

                dayName="Monday";
                break;
            case 3:

                dayName="Tuesday";
                break;
            case 4:

                dayName="Wednesday";
                break;
            case 5:

                dayName="Thursday";
                break;
            case 6:

                dayName="Friday";
                break;
            case 7:

                dayName="Saturday";
                break;

        }

        String[]word=date.split("-");
        String finaldate="",month="";
        switch (word[1])
        {
            case "01":
                month="Jan";
                break;
            case "02":
                month="Feb";
                break;
            case "03":
                month="Mar";
                break;
            case "04":
                month="Apr";
                break;
            case "05":
                month="May";
                break;
            case "06":
                month="Jun";
                break;
            case "07":
                month="Jul";
                break;
            case "08":
                month="Aug";
                break;
            case "09":
                month="Sep";
                break;
            case "10":
                month="Octr";
                break;
            case "11":
                month="Nov";
                break;
            case "12":
                month="Dec";
                break;

        }
        finaldate=dayName+", "+word[2]+" "+month+" "+word[0];

        return finaldate;
    }



    public interface AddButtonListener
    {
        void onItemClick(int pos,String StatusId, String taskId);

    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressbar)
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvTime)
        TextView tvTime;
        @BindView(R.id.tvAmount)
        TextView tvAmount;
        @BindView(R.id.tvStatus)
        TextView tvStatus;
        @BindView(R.id.servicerequestId)
        TextView tvtaskId;

        @BindView(R.id.tvService)
        TextView tvService;
        @BindView(R.id.iv_prc)
        ImageView iv_prc;
        @BindView(R.id.ll_main)
        LinearLayout ll_main;
        @BindView(R.id.ll_requested_on)
        LinearLayout ll_requested_on;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);

        }
    }
}