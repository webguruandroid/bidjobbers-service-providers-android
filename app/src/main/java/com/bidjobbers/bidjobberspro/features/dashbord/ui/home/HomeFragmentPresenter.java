package com.bidjobbers.bidjobberspro.features.dashbord.ui.home;

import android.util.Log;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.NotificationCount.NotificationCountRequest;
import com.bidjobbers.bidjobberspro.data.network.models.NotificationCount.NotificationCountResponse;
import com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.TabBean;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

public class HomeFragmentPresenter <V extends HomeFragmentMvpView>
        extends BaseFragmentPresenter<V>
        implements HomeFragmentMvpPresenter<V> {

    @Inject
    public HomeFragmentPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }

    @Override
    public void getAllTasks()
    {

        ArrayList<TabBean> tabBeans = new ArrayList<TabBean>();

//        for (int i = 1; i<=getDataManager().getTypesOfTabs().size();i++)
//        {
//           // tabBeans.add(new TabBean(String.valueOf(i),getDataManager().getTypesOfTabs().get(i-1),"","",""));
//            tabBeans.add(new TabBean(String.valueOf(i),"omr","","",""));
//        }



        getMvpView().allTasksName(tabBeans);
    }


    @Override
    public void onGetNotificationCount() {
        NotificationCountRequest notificationCountRequest=new NotificationCountRequest(getDataManager().getLocalValue());

        getMvpView().showLoading();
        getCompositeDisposable().add(getDataManager().getNotificationCount("Bearer "+getDataManager().getAccess_token(),"application/json",notificationCountRequest)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribeWith(new DisposableSingleObserver<NotificationCountResponse>()
                {

                    @Override
                    public void onSuccess(NotificationCountResponse notificationCountResponse)
                    {

                        getMvpView().hideLoading();
                        if(notificationCountResponse.getResponseCode()==1 )
                        {
                            getMvpView().onSuccessfullyGetNotificationCount(notificationCountResponse);
                        }
                        else
                        {

                            getMvpView().onSuccessfullyGetNotificationCount(notificationCountResponse);


                            //Shared prefrence value save access_token.
                        }

                    }
                    @Override
                    public void onError(Throwable e) {

                        getMvpView().hideLoading();
                        Log.e("ERROR", "onError: "+e.getMessage());
                    }

                }));
    }
}
