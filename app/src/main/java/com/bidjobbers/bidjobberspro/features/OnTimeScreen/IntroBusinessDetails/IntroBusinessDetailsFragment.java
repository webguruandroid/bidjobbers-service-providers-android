package com.bidjobbers.bidjobberspro.features.OnTimeScreen.IntroBusinessDetails;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;



import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.di.components.ActivityComponent;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.IntroBusinessDetails.IntroBusinessObject.IntroBusinessObject;
import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragment;
import com.bidjobbers.bidjobberspro.utils.Validation.BusinessDescBean;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link IntroBusinessDetailsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link IntroBusinessDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class IntroBusinessDetailsFragment extends BaseFragment implements IntroBusinessDetailsFragmentMvpView {


    @BindView(R.id.edt_desc)
    EditText edt_desc;

    @Inject
    IntroBusinessDetailsFragmentPresenter<IntroBusinessDetailsFragmentMvpView> introBusinessDetailsFragmentPresenter;

    public IntroBusinessDetailsFragment() {  // Required empty public constructor

    }

    // TODO: Rename and change types and number of parameters
    public static IntroBusinessDetailsFragment newInstance() {
        IntroBusinessDetailsFragment fragment = new IntroBusinessDetailsFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {

            String str = bundle.getString("FROMG");
            Log.e("FROMG", str);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_intro_business_details, container, false);

        setUnBinder(ButterKnife.bind(this,v));
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
        }

        introBusinessDetailsFragmentPresenter.onAttach(this);

        introBusinessDetailsFragmentPresenter.onGetDataFromApi();
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }



    public BusinessDescBean getDescData()
    {
        boolean error=false;String error_text="";
        String edt_desc_data = edt_desc.getText().toString().trim();

        if(edt_desc_data.length()<100)
        {
            error = true;
            error_text = "Short Description Should have min 100 characters";
        }
        if(edt_desc_data.isEmpty())
        {
            error = true;
            error_text = "Enter Short Description of your project";
        }
        BusinessDescBean  businessDescBean = new BusinessDescBean(error,error_text,edt_desc_data);
        return businessDescBean;

    }

    @Override
    public void setDataFromApi(DataModelForFirstTimeQusAns.IntroduceYourBusiness introduceYourBusiness) {
        edt_desc.setText(introduceYourBusiness.getDetails());
    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void showAlert(String message) {

    }

    @Override
    public void showInactiveUserAlert(String message) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    public IntroBusinessObject sendDataToActvity ()
    {


        IntroBusinessObject introduceYourBusiness=new IntroBusinessObject(edt_desc.getText().toString().trim());
        return introduceYourBusiness;

    }
}
