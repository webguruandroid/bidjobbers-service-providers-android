package com.bidjobbers.bidjobberspro.features.dashbord.ui.send;

import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface SettingsFragmentMvpView extends BaseFragmentMvpView {

    void successfullyGetProfile(getProfileResponse getProfileResponse);
    void successfullyLogout();
    void onDeactivate(String data);
}
