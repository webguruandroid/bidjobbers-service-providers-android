package com.bidjobbers.bidjobberspro.features.BankDetails;

import com.bidjobbers.bidjobberspro.features.Archive.ArchiveMvpView;
import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface BankDetailsMvpPresenter <V extends BankDetailsMvpView> extends MvpPresenter<V> {
    void onGetDataFromApi();
    void submitBankDetails(String bankName,String accountHolderName,String accountHolderLastName,String bankAccountNumber,String routingNumber,String bankCountry,String bankCurrency,String bankSwift,String bankIban,String paypalId,String paypalCurrency,String transaction_acoount_id);
}
