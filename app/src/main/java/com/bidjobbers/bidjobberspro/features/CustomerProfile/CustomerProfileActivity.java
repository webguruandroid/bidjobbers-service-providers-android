package com.bidjobbers.bidjobberspro.features.CustomerProfile;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.models.Archived.ArchiveDetailsResponse;
import com.bidjobbers.bidjobberspro.features.OnTimeScreen.ImageDataFragment.ImagePickerHelper.TakeImageAdapterAC;
import com.bidjobbers.bidjobberspro.shared.base.BaseActivity;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomerProfileActivity extends BaseActivity implements CustomerProfileMvpView  {

    @Inject
    TakeImageAdapterAC adapter;
    ArrayList<String> a = new ArrayList<String>();
    @Inject
    TakeImageAdapterAC adapterEnd;
    String jobStatus = "", taskId, bidId, customerId;
    Context mContext;



    @BindView(R.id.ll_hide_show_reschuedule_area)
    LinearLayout ll_hide_show_reschuedule_area;
    @BindView(R.id.ll_start)
    LinearLayout ll_start;
    @BindView(R.id.ll_end)
    LinearLayout ll_end;
    @BindView(R.id.ll_send_bid)
    LinearLayout ll_send_bid;
    @BindView(R.id.ll_map_open)
    LinearLayout ll_map_open;
    @BindView(R.id.ll_complete_area)
    LinearLayout ll_complete_area;
    @BindView(R.id.ll_cancel_area)
    LinearLayout ll_cancel_area;
    @BindView(R.id.ll_bid_area)
    LinearLayout ll_bid_area;
    @BindView(R.id.ll_reschuedule_area)
    LinearLayout ll_reschuedule_area;

    @BindView(R.id.iv_prc)
    ImageView iv_prc;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvPhoneNumber)
    TextView tvPhoneNumber;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.tvServiceRequired)
    TextView tvServiceRequired;
    @BindView(R.id.tvServiceSize)
    TextView tvServiceSize;
    @BindView(R.id.tvCustomerPresent)
    TextView tvCustomerPresent;
    @BindView(R.id.tvServiceTime)
    TextView tvServiceTime;
    @BindView(R.id.tvHeader)
    TextView tvHeader;
    @BindView(R.id.tvServiceType)
    TextView tvServiceType;
    @BindView(R.id.tvServiceNeed)
    TextView tvServiceNeed;
    @BindView(R.id.tvServiceAt)
    TextView tvServiceAt;
    @BindView(R.id.llSchedule)
    LinearLayout llSchedule;
    @BindView(R.id.tvSelectedSlot)
    TextView tvSelectedSlot;
    @BindView(R.id.tvBidPrice)
    EditText tvBidPrice;
    @BindView(R.id.tvJobScheduleOn)
    TextView tvJobScheduleOn;
    @BindView(R.id.tvJobAmount)
    TextView tvJobAmount;
    @BindView(R.id.llJobAmount)
    LinearLayout llJobAmount;
    @BindView(R.id.llScheduleOn)
    LinearLayout llScheduleOn;
    @BindView(R.id.tvSiteImage)
    TextView tvSiteImage;
    @BindView(R.id.tvComment)
    TextView tvComment;
    @BindView(R.id.ivblocked)
    ImageView ivblocked;
    @BindView(R.id.ivunblocked)
    FrameLayout ivunblocked;
    @BindView(R.id.llRescheduleJobs)
    LinearLayout llRescheduleJobs;
    @BindView(R.id.rb_reschedule_no)
    RadioButton rb_reschedule_no;
    @BindView(R.id.rb_reschedule_ys)
    RadioButton rb_reschedule_ys;
    @BindView(R.id.etRescheduleComment)
    EditText etRescheduleComment;
    @BindView(R.id.ll_bottom_cart_area)
    RelativeLayout ll_bottom_cart_area;
    @BindView(R.id.tvRescheduleSlot)
    TextView tvRescheduleSlot;
    @BindView(R.id.ll_select_slot)
    LinearLayout ll_select_slot;
    @BindView(R.id.ll_end_area)
    LinearLayout ll_end_area;
    @BindView(R.id.radio_one)
    RadioGroup radio_one;
    @BindView(R.id.rb_yes_photo)
    RadioButton rb_yes_photo;
    @BindView(R.id.rb_no_photo)
    RadioButton rb_no_photo;
    @BindView(R.id.llPhoto)
    LinearLayout llPhoto;
    @BindView(R.id.etEndNote)
    EditText etEndNote;

    @BindView(R.id.squareImageView)
    ImageView squareImageView;
    @BindView(R.id.squareImageViewOne)
    ImageView squareImageViewOne;
    @BindView(R.id.squareImageViewTwo)
    ImageView squareImageViewTwo;
    @BindView(R.id.squareImageViewThree)
    ImageView squareImageViewThree;
    @BindView(R.id.squareImageViewFour)
    ImageView squareImageViewFour;
    @BindView(R.id.ivCrossOne)
    ImageView ivCrossOne;
    @BindView(R.id.ivCrossTwo)
    ImageView ivCrossTwo;
    @BindView(R.id.ivCrossThree)
    ImageView ivCrossThree;
    @BindView(R.id.ivCrossFour)
    ImageView ivCrossFour;
    @BindView(R.id.ivCrossFive)
    ImageView ivCrossFive;
    @BindView(R.id.tvJobScheduleOnText)
    TextView tvJobScheduleOnText;

    @BindView(R.id.tvimgOne)
    TextView tvimgOne;
    @BindView(R.id.tvimgTwo)
    TextView tvimgTwo;
    @BindView(R.id.tvimgThree)
    TextView tvimgThree;
    @BindView(R.id.tvimgFour)
    TextView tvimgFour;
    @BindView(R.id.tvimgFive)
    TextView tvimgFive;
    @BindView(R.id.rv_end_photo)
    RecyclerView rv_end_photo;
    @BindView(R.id.tvEndNote)
    TextView tvEndNote;
    @BindView(R.id.servicerequestId)
    TextView servicerequestId;
     @BindView(R.id.iv_mnu)
     ImageView iv_mnu;
    @BindView(R.id.ll_payment_status)
    LinearLayout ll_payment_status;
    @BindView(R.id.tvPaymentStatus)
    TextView tvPaymentStatus;
    @BindView(R.id.iv_paid)
    ImageView iv_paid;
    @BindView(R.id.iv_jobdone)
    ImageView iv_jobdone;
    @BindView(R.id.ll_site_image)
    LinearLayout ll_site_image;


    ArrayList<String> arrayListPhoto = new ArrayList<String>();
    String ImageOne, ImageTwo, ImageThree, ImageFour, ImageFive;
    String Time, Date, StartDate="", TimeName, DateName,Latitude,Longitude;

    Dialog dialog, dialogWhereDataTake;
    ImageView iv_gal_img;

    @Inject
    CustomerProfilePresenter<CustomerProfileMvpView> customerDetailsPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_profile);

        setUnBinder(ButterKnife.bind(this));
        getActivityComponent().inject(this);
        mContext = this;
        customerDetailsPresenter.onAttach(this);

        Intent intent = getIntent();
        jobStatus = intent.getStringExtra("status");
        taskId = intent.getStringExtra("taskId");
        bidId = intent.getStringExtra("bidId");

        if (jobStatus.trim().equals("4")) {
            ll_complete_area.setVisibility(View.VISIBLE);
            //  ivblocked.setVisibility(View.VISIBLE);
            ll_start.setVisibility(View.GONE);
            ll_payment_status.setVisibility(View.VISIBLE);
        }
        if (jobStatus.trim().equals("5")) {
            ll_cancel_area.setVisibility(View.VISIBLE);
            //   ivblocked.setVisibility(View.VISIBLE);
            ll_start.setVisibility(View.GONE);
            ll_payment_status.setVisibility(View.GONE);
        }
     //   tvJobScheduleOnText.setText("Job Scheduled On:");

        customerDetailsPresenter.onGetCustomer(taskId);
        Log.e("ARCHIVRE",taskId);


        iv_mnu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


    }









    @Override
    public void onSuccessfullyGetCustomer(ArchiveDetailsResponse customerDetailsResponse) {
        a = new ArrayList<>();
        arrayListPhoto.clear();
        if(customerDetailsResponse.getResponseCode()==1) {


            if(jobStatus.equals("1")||jobStatus.equals("9")||jobStatus.equals("6")) {
                servicerequestId.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceRequestId());
                customerId = customerDetailsResponse.getResponseData().getJobDetails().getCustomerId();
                tvName.setText(customerDetailsResponse.getResponseData().getJobDetails().getCustomerName());
                tvPhoneNumber.setText(customerDetailsResponse.getResponseData().getJobDetails().getCustomerPhone());
                tvAddress.setText(customerDetailsResponse.getResponseData().getJobDetails().getFullAddress());
                tvServiceAt.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceAt());
                tvServiceRequired.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceRequired());
                if (customerDetailsResponse.getResponseData().getJobDetails().getServiceRequired().equals("Lawn Service")) {
                    String text = customerDetailsResponse.getResponseData().getJobDetails().getServideNeeded();
                    if (!customerDetailsResponse.getResponseData().getJobDetails().getGrassSize().equals("")) {
                        text = text + ", " + customerDetailsResponse.getResponseData().getJobDetails().getGrassSize();
                    }

                    tvServiceNeed.setText(text);
                } else if (customerDetailsResponse.getResponseData().getJobDetails().getServiceRequired().equals("Tree Cutting")) {
                    String text = customerDetailsResponse.getResponseData().getJobDetails().getServideNeeded();
                    if (!customerDetailsResponse.getResponseData().getJobDetails().getHowManyTreeRemove().equals("")) {
                        text = text + ", " + customerDetailsResponse.getResponseData().getJobDetails().getHowManyTreeRemove();
                    }
                    if (!customerDetailsResponse.getResponseData().getJobDetails().getHowManyTreeTrim().equals("")) {
                        text = text + ", " + customerDetailsResponse.getResponseData().getJobDetails().getHowManyTreeTrim();

                    }
                    Log.e("CustomerDetails", text);
                    tvServiceNeed.setText(text);
                } else {
                    tvServiceNeed.setText(customerDetailsResponse.getResponseData().getJobDetails().getServideNeeded() + ", " + customerDetailsResponse.getResponseData().getJobDetails().getGrassSize());
                }

                tvServiceType.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceType());
                tvServiceTime.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceTiming());
                tvComment.setText(customerDetailsResponse.getResponseData().getJobDetails().getOtherInfo());

                Latitude = customerDetailsResponse.getResponseData().getJobDetails().getLat();
                Longitude = customerDetailsResponse.getResponseData().getJobDetails().getLng();


                if (customerDetailsResponse.getResponseData().getJobDetails().getOnSitePresent()) {
                    tvCustomerPresent.setText("Yes");
                } else {
                    tvCustomerPresent.setText("No");
                }

                if (customerDetailsResponse.getResponseData().getJobDetails().getServiceRequired().equals("Lawn Service")) {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getJobDetails().getLawnSize());
                } else if (customerDetailsResponse.getResponseData().getJobDetails().getServiceRequired().equals("Tree Cutting")) {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getJobDetails().getGrassSize());
                } else {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getJobDetails().getLawnSize());
                }
                Glide.with(getApplication()).load(customerDetailsResponse.getResponseData().getJobDetails().getCustomerProfileImage())
                        .into(iv_prc);
                if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 0) {
                    tvSiteImage.setVisibility(View.GONE);
                    ll_site_image.setVisibility(View.GONE);

                } else {
                    tvSiteImage.setVisibility(View.VISIBLE);
                    ll_site_image.setVisibility(View.VISIBLE);
                }
                if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 1) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    a.add(ImageOne);
                } else if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 2) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(1);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                } else if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 3) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(2);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                } else if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 4) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(2);
                    ImageFour = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(3);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                } else if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 5) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(2);
                    ImageFour = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(3);
                    ImageFive = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(4);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                    a.add(ImageFive);
                }

                // adapter = new TakeImageAdapterAC(a, this);
                adapter.loadList(a);
                adapter.setAdapterListener(new TakeImageAdapterAC.CellListener() {
                    @Override
                    public void onItemClick(int pos, ArrayList<String> items) {
                        String str = "";
                        if (pos == 0) {
                            str = ImageOne;
                        }
                        if (pos == 1) {
                            str = ImageTwo;
                        }
                        if (pos == 2) {
                            str = ImageThree;
                        }
                        if (pos == 3) {
                            str = ImageFour;
                        }
                        if (pos == 4) {
                            str = ImageFive;
                        }

                        Glide.with(mContext).
                                load(str)
                                //.placeholder(R.drawable.image_placeholder)
                                .into(iv_gal_img);
                        dialog.show();
                    }
                });
                adapter.notifyDataSetChanged();
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_take_image);
                LinearLayoutManager layoutManagerMakeOverBrand = new LinearLayoutManager(this);
                layoutManagerMakeOverBrand.setOrientation(LinearLayoutManager.HORIZONTAL);


                recyclerView.setLayoutManager(layoutManagerMakeOverBrand);
                recyclerView.setAdapter(adapter);
                //  Toast.makeText(getApplicationContext(),taskDataBean.getStatus(),Toast.LENGTH_SHORT).show();

                if (customerDetailsResponse.getResponseData().getJobDetails().getStatus().equals("Bid Sent")) {
                    llJobAmount.setVisibility(View.GONE);
                    llScheduleOn.setVisibility(View.GONE);
                    tvJobAmount.setText(customerDetailsResponse.getResponseData().getJobDetails().getPrice());
                    tvJobScheduleOn.setText(setDate(customerDetailsResponse.getResponseData().getJobDetails().getDate()) + "," + customerDetailsResponse.getResponseData().getJobDetails().getSlot());
                    tvBidPrice.setFocusable(false);
                    tvBidPrice.setEnabled(false);
                    int length = customerDetailsResponse.getResponseData().getJobDetails().getPrice().length();
                    tvBidPrice.setText(customerDetailsResponse.getResponseData().getJobDetails().getPrice().substring(1, length));
                    // Toast.makeText(getApplicationContext(),taskDataBean.getPrice().substring(1,length),Toast.LENGTH_SHORT).show();
                    llSchedule.setVisibility(View.GONE);
                    tvSelectedSlot.setVisibility(View.VISIBLE);
                    tvSelectedSlot.setText(setDate(customerDetailsResponse.getResponseData().getJobDetails().getDate()) + "," + customerDetailsResponse.getResponseData().getJobDetails().getSlot());
                    tvSelectedSlot.setEnabled(false);
                    ll_send_bid.setVisibility(View.GONE);
                } else if (customerDetailsResponse.getResponseData().getJobDetails().getStatus().equals("Received Jobs")) {

                    llJobAmount.setVisibility(View.GONE);
                    llScheduleOn.setVisibility(View.GONE);
                    tvBidPrice.setFocusable(true);
                    int length = customerDetailsResponse.getResponseData().getJobDetails().getPrice().length();
                    tvBidPrice.setText(customerDetailsResponse.getResponseData().getJobDetails().getPrice().substring(1, length));
                    // Toast.makeText(getApplicationContext(),taskDataBean.getPrice(),Toast.LENGTH_SHORT).show();
                    llSchedule.setVisibility(View.VISIBLE);
                    tvSelectedSlot.setVisibility(View.GONE);
                    tvSelectedSlot.setText("" + "," + "");
                    ll_send_bid.setVisibility(View.VISIBLE);

                }
            }
            if(jobStatus.equals("2")||jobStatus.equals("3")||jobStatus.equals("7")||jobStatus.equals("8"))
            {
                servicerequestId.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceRequestId());
                customerId =  customerDetailsResponse.getResponseData().getJobDetails().getCustomerId();
                tvSiteImage.setVisibility(View.VISIBLE);
                bidId= customerDetailsResponse.getResponseData().getJobDetails().getBidId();

                Latitude=customerDetailsResponse.getResponseData().getJobDetails().getLat();
                Longitude=customerDetailsResponse.getResponseData().getJobDetails().getLng();
                // tvHeader.setText(taskDataBean.getCustomer_name());
                tvName.setText(customerDetailsResponse.getResponseData().getJobDetails().getCustomerName());
                tvPhoneNumber.setText(customerDetailsResponse.getResponseData().getJobDetails().getCustomerPhone());
                tvAddress.setText(customerDetailsResponse.getResponseData().getJobDetails().getFullAddress());
                tvServiceAt.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceAt());
                tvServiceRequired.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceRequired());

                tvServiceType.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceType());
                tvServiceTime.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceTiming());
                tvJobAmount.setText(customerDetailsResponse.getResponseData().getJobDetails().getPrice());
                tvJobScheduleOn.setText(setDate( customerDetailsResponse.getResponseData().getJobDetails().getDate()) + "," + customerDetailsResponse.getResponseData().getJobDetails().getSlot());
                tvComment.setText(customerDetailsResponse.getResponseData().getJobDetails().getOtherInfo());
                tvSiteImage.setVisibility(View.VISIBLE);


                if (customerDetailsResponse.getResponseData().getJobDetails().getServiceRequired().equals("Lawn Service")) {
                    String text=customerDetailsResponse.getResponseData().getJobDetails().getServideNeeded();
                    if(!customerDetailsResponse.getResponseData().getJobDetails().getGrassSize().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getJobDetails().getGrassSize();
                    }

                    tvServiceNeed.setText(text);
                } else if(customerDetailsResponse.getResponseData().getJobDetails().getServiceRequired().equals("Tree Cutting")) {
                    String text=customerDetailsResponse.getResponseData().getJobDetails().getServideNeeded();
                    if(!customerDetailsResponse.getResponseData().getJobDetails().getHowManyTreeRemove().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getJobDetails().getHowManyTreeRemove();
                    }
                    if(!customerDetailsResponse.getResponseData().getJobDetails().getHowManyTreeRemove().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getJobDetails().getHowManyTreeTrim();

                    }
                    Log.e("CustomerDetails",text);
                    tvServiceNeed.setText(text);
                }
                else
                {
                    tvServiceNeed.setText(customerDetailsResponse.getResponseData().getJobDetails().getServideNeeded()+", "+customerDetailsResponse.getResponseData().getJobDetails().getGrassSize());
                }

                llJobAmount.setVisibility(View.VISIBLE);
                llScheduleOn.setVisibility(View.VISIBLE);
                tvJobAmount.setText(customerDetailsResponse.getResponseData().getJobDetails().getPrice());
                tvJobScheduleOn.setText(setDate( customerDetailsResponse.getResponseData().getJobDetails().getDate()) + "," + customerDetailsResponse.getResponseData().getJobDetails().getSlot());

                if (customerDetailsResponse.getResponseData().getJobDetails().getOnSitePresent()) {
                    tvCustomerPresent.setText("Yes");
                } else {
                    tvCustomerPresent.setText("No");
                }

                if (customerDetailsResponse.getResponseData().getJobDetails().getServiceRequired().equals("Lawn Service")) {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getJobDetails().getLawnSize());
                } else if(customerDetailsResponse.getResponseData().getJobDetails().getServiceRequired().equals("Tree Cutting")) {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getJobDetails().getGrassSize());
                }
                else
                {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getJobDetails().getLawnSize());
                }
                Glide.with(getApplication()).load(customerDetailsResponse.getResponseData().getJobDetails().getCustomerProfileImage())
                        .into(iv_prc);
                if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 0) {
                    tvSiteImage.setVisibility(View.GONE);
                    ll_site_image.setVisibility(View.GONE);

                } else {
                    tvSiteImage.setVisibility(View.VISIBLE);
                    ll_site_image.setVisibility(View.VISIBLE);
                }
                if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 1) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    a.add(ImageOne);
                } else if ( customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 2) {
                    ImageOne =customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(1);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                } else if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 3) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(1);
                    ImageThree =customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(2);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                } else if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 4) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    ImageTwo =customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(2);
                    ImageFour = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(3);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                } else if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 5) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(2);
                    ImageFour =customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(3);
                    ImageFive =customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(4);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                    a.add(ImageFive);
                }


                //  adapter = new TakeImageAdapterAC(a, this);
                adapter.loadList(a);
                adapter.setAdapterListener(new TakeImageAdapterAC.CellListener() {
                    @Override
                    public void onItemClick(int pos, ArrayList<String> items) {
                        String str = "";
                        if (pos == 0) {
                            str = ImageOne;
                        }
                        if (pos == 1) {
                            str = ImageTwo;
                        }
                        if (pos == 2) {
                            str = ImageThree;
                        }
                        if (pos == 3) {
                            str = ImageFour;
                        }
                        if (pos == 4) {
                            str = ImageFive;
                        }

                        Glide.with(mContext).
                                load(str)
                                // .placeholder(R.drawable.image_placeholder)
                                .into(iv_gal_img);
                        dialog.show();
                    }
                });
                adapter.notifyDataSetChanged();
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_take_image);
                LinearLayoutManager layoutManagerMakeOverBrand = new LinearLayoutManager(this);
                layoutManagerMakeOverBrand.setOrientation(LinearLayoutManager.HORIZONTAL);


                recyclerView.setLayoutManager(layoutManagerMakeOverBrand);
                recyclerView.setAdapter(adapter);
                if (customerDetailsResponse.getResponseData().getJobDetails().getStatus().equals("Bid Sent")) {
                    tvBidPrice.setFocusable(false);
                    tvBidPrice.setEnabled(false);
                    int length = customerDetailsResponse.getResponseData().getJobDetails().getPrice().length();
                    tvBidPrice.setText(customerDetailsResponse.getResponseData().getJobDetails().getPrice().substring(1, length));
                    // Toast.makeText(getApplicationContext(),taskDataBean.getPrice().substring(1,length),Toast.LENGTH_SHORT).show();
                    llSchedule.setVisibility(View.GONE);
                    tvSelectedSlot.setVisibility(View.VISIBLE);
                    tvSelectedSlot.setText(  setDate(customerDetailsResponse.getResponseData().getJobDetails().getDate())+ ","  +customerDetailsResponse.getResponseData().getJobDetails().getSlot());
                    tvSelectedSlot.setEnabled(false);
                    ll_send_bid.setVisibility(View.GONE);

                } else if (customerDetailsResponse.getResponseData().getJobDetails().getStatus().equals("Received Jobs")) {
                    tvBidPrice.setFocusable(true);
                    int length = customerDetailsResponse.getResponseData().getJobDetails().getPrice().length();
                    tvBidPrice.setText(customerDetailsResponse.getResponseData().getJobDetails().getPrice().substring(1, length));
                    // Toast.makeText(getApplicationContext(),taskDataBean.getPrice(),Toast.LENGTH_SHORT).show();
                    llSchedule.setVisibility(View.VISIBLE);
                    tvSelectedSlot.setVisibility(View.GONE);
                    tvSelectedSlot.setText("" + "," + "");
                    ll_send_bid.setVisibility(View.VISIBLE);

                }
            }
            if(jobStatus.equals("4"))
            {
                Log.e("ARCHIVRE",taskId+"-"+jobStatus+customerDetailsResponse.getResponseData().getJobDetails().getServiceRequestId());
                servicerequestId.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceRequestId());
                customerId =  customerDetailsResponse.getResponseData().getJobDetails().getCustomerId();
                tvSiteImage.setVisibility(View.VISIBLE);
                bidId= customerDetailsResponse.getResponseData().getJobDetails().getBidId();

                Latitude= customerDetailsResponse.getResponseData().getJobDetails().getLat();
                Longitude= customerDetailsResponse.getResponseData().getJobDetails().getLng();
                // tvHeader.setText(taskDataBean.getCustomer_name());
                tvName.setText( customerDetailsResponse.getResponseData().getJobDetails().getCustomerName());
                tvPhoneNumber.setText( customerDetailsResponse.getResponseData().getJobDetails().getCustomerPhone());
                tvAddress.setText( customerDetailsResponse.getResponseData().getJobDetails().getFullAddress());
                tvServiceAt.setText( customerDetailsResponse.getResponseData().getJobDetails().getServiceAt());
                tvServiceRequired.setText( customerDetailsResponse.getResponseData().getJobDetails().getServiceRequired());

                tvServiceType.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceType());
                tvServiceTime.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceTiming());
                tvJobAmount.setText(customerDetailsResponse.getResponseData().getJobDetails().getPrice());
                tvJobScheduleOn.setText(setDate( customerDetailsResponse.getResponseData().getJobDetails().getDate()) + "," + customerDetailsResponse.getResponseData().getJobDetails().getSlot());
                tvComment.setText(customerDetailsResponse.getResponseData().getJobDetails().getOtherInfo());
                tvSiteImage.setVisibility(View.VISIBLE);

                llJobAmount.setVisibility(View.VISIBLE);
                llScheduleOn.setVisibility(View.VISIBLE);
                tvJobAmount.setText(customerDetailsResponse.getResponseData().getJobDetails().getPrice());
                tvJobScheduleOn.setText(setDate( customerDetailsResponse.getResponseData().getJobDetails().getDate()) + "," + customerDetailsResponse.getResponseData().getJobDetails().getSlot());
                // Toast.makeText(getApplicationContext(),taskDataBean.getIs_block()+"",Toast.LENGTH_SHORT).show();
                if (customerDetailsResponse.getResponseData().getJobDetails().getIsBlock()) {
                    ivblocked.setVisibility(View.GONE);
                    ivunblocked.setVisibility(View.GONE);
                } else {
                    ivblocked.setVisibility(View.GONE);
                    ivunblocked.setVisibility(View.GONE);
                }
                if (customerDetailsResponse.getResponseData().getJobDetails().getOnSitePresent()) {
                    tvCustomerPresent.setText("Yes");
                } else {
                    tvCustomerPresent.setText("No");
                }

                if (customerDetailsResponse.getResponseData().getJobDetails().getServiceTypeId().equals("1")) {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getJobDetails().getLawnSize());
                } else if(customerDetailsResponse.getResponseData().getJobDetails().getServiceTypeId().equals("2")) {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getJobDetails().getGrassSize());
                }
                else
                {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getJobDetails().getLawnSize());
                }



                if(customerDetailsResponse.getResponseData().getJobDetails().getPayment_validation_status().equals(""))
                {
                    tvPaymentStatus.setText(getResources().getString(R.string.pending));

                    iv_jobdone.setVisibility(View.VISIBLE);
                    iv_paid.setVisibility(View.GONE);

                }
                else if(customerDetailsResponse.getResponseData().getJobDetails().getPayment_validation_status().equals("11"))
                {
                    tvPaymentStatus.setText(getResources().getString(R.string.onHole));
                    iv_jobdone.setVisibility(View.VISIBLE);
                    iv_paid.setVisibility(View.GONE);
                }
                else if(customerDetailsResponse.getResponseData().getJobDetails().getPayment_validation_status().equals("12"))
                {
                    tvPaymentStatus.setText(getResources().getString(R.string.paid));
                    iv_jobdone.setVisibility(View.VISIBLE);
                    iv_paid.setVisibility(View.GONE);
                }
                else if(customerDetailsResponse.getResponseData().getJobDetails().getPayment_validation_status().equals("13"))
                {
                    tvPaymentStatus.setText(getResources().getString(R.string.paidbybidjobbers));
                    iv_jobdone.setVisibility(View.GONE);
                    iv_paid.setVisibility(View.VISIBLE);
                }

                if (customerDetailsResponse.getResponseData().getJobDetails().getServiceTypeId().equals("1")) {
                    String text=customerDetailsResponse.getResponseData().getJobDetails().getServideNeeded();
                    if(!customerDetailsResponse.getResponseData().getJobDetails().getGrassSize().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getJobDetails().getGrassSize();
                    }

                    tvServiceNeed.setText(text);
                } else if(customerDetailsResponse.getResponseData().getJobDetails().getServiceTypeId().equals("2")) {
                    String text=customerDetailsResponse.getResponseData().getJobDetails().getServideNeeded();
                    if(!customerDetailsResponse.getResponseData().getJobDetails().getHowManyTreeRemove().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getJobDetails().getHowManyTreeRemove();
                    }
                    if(!customerDetailsResponse.getResponseData().getJobDetails().getHowManyTreeRemove().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getJobDetails().getHowManyTreeRemove();

                    }
                    Log.e("CustomerDetails",text);
                    tvServiceNeed.setText(text);
                }
                else
                {
                    tvServiceNeed.setText(customerDetailsResponse.getResponseData().getJobDetails().getServideNeeded()+", "+customerDetailsResponse.getResponseData().getJobDetails().getGrassSize());
                }
                Glide.with(getApplication()).load(customerDetailsResponse.getResponseData().getJobDetails().getCustomerProfileImage())
                        .into(iv_prc);
                if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 0) {
                    tvSiteImage.setVisibility(View.GONE);
                    ll_site_image.setVisibility(View.GONE);
                } else {
                    tvSiteImage.setVisibility(View.VISIBLE);
                    ll_site_image.setVisibility(View.VISIBLE);
                }
                if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 1) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    a.add(ImageOne);
                } else if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 2) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(1);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                } else if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 3) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(1);
                    ImageThree =customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(2);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                } else if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 4) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(2);
                    ImageFour = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(3);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                } else if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 5) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(1);
                    ImageThree =customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(2);
                    ImageFour = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(3);
                    ImageFive = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(4);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                    a.add(ImageFive);
                }


                // adapter = new TakeImageAdapterAC(a, this);
                adapter.loadList(a);
                adapter.setAdapterListener(new TakeImageAdapterAC.CellListener() {
                    @Override
                    public void onItemClick(int pos, ArrayList<String> items) {
                        String str = "";
                        if (pos == 0) {
                            str = ImageOne;
                        }
                        if (pos == 1) {
                            str = ImageTwo;
                        }
                        if (pos == 2) {
                            str = ImageThree;
                        }
                        if (pos == 3) {
                            str = ImageFour;
                        }
                        if (pos == 4) {
                            str = ImageFive;
                        }

                        Glide.with(mContext).
                                load(str)
                                //.placeholder(R.drawable.image_placeholder)
                                .into(iv_gal_img);
                        dialog.show();
                    }
                });
                adapter.notifyDataSetChanged();
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_take_image);
                LinearLayoutManager layoutManagerMakeOverBrand = new LinearLayoutManager(this);
                layoutManagerMakeOverBrand.setOrientation(LinearLayoutManager.HORIZONTAL);


                recyclerView.setLayoutManager(layoutManagerMakeOverBrand);
                recyclerView.setAdapter(adapter);

                tvEndNote.setText( customerDetailsResponse.getResponseData().getJobDetails().getTaskEndNotes());
                if( customerDetailsResponse.getResponseData().getJobDetails().getTaskImages().size()>0) {

                    for (int k = 0; k <  customerDetailsResponse.getResponseData().getJobDetails().getTaskImages().size(); k++) {
                        arrayListPhoto.add( customerDetailsResponse.getResponseData().getJobDetails().getTaskImageUrl() + "/" +  customerDetailsResponse.getResponseData().getJobDetails().getTaskImages().get(k));
                    }


                    adapterEnd.loadList(arrayListPhoto);
                    rv_end_photo.setAdapter(adapterEnd);



                    adapterEnd.setAdapterListener(new TakeImageAdapterAC.CellListener() {
                        @Override
                        public void onItemClick(int pos, ArrayList<String> items) {
                            String str = "";
                            str= customerDetailsResponse.getResponseData().getJobDetails().getTaskImageUrl() + "/" +  customerDetailsResponse.getResponseData().getJobDetails().getTaskImages().get(pos);
                            Log.e("COMPLETE",str);
                            Glide.with(mContext).
                                    load(str)
                                    //.placeholder(R.drawable.image_placeholder)
                                    .into(iv_gal_img);
                            dialog.show();
                        }
                    });
                }
            }
            if(jobStatus.equals("5"))
            {
                servicerequestId.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceRequestId());
                customerId = customerDetailsResponse.getResponseData().getJobDetails().getCustomerId();
                tvSiteImage.setVisibility(View.VISIBLE);
                bidId=customerDetailsResponse.getResponseData().getJobDetails().getBidId();

                Latitude=customerDetailsResponse.getResponseData().getJobDetails().getLat();
                Longitude=customerDetailsResponse.getResponseData().getJobDetails().getLng();
                // tvHeader.setText(taskDataBean.getCustomer_name());
                tvName.setText(customerDetailsResponse.getResponseData().getJobDetails().getCustomerName());
                tvPhoneNumber.setText(customerDetailsResponse.getResponseData().getJobDetails().getCustomerPhone());
                tvAddress.setText(customerDetailsResponse.getResponseData().getJobDetails().getFullAddress());
                tvServiceAt.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceAt());
                tvServiceRequired.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceRequired());
                tvServiceType.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceType());
                tvServiceTime.setText(customerDetailsResponse.getResponseData().getJobDetails().getServiceTiming());
                tvJobAmount.setText(customerDetailsResponse.getResponseData().getJobDetails().getPrice());
                tvJobScheduleOn.setText(setDate( customerDetailsResponse.getResponseData().getJobDetails().getDate()) + "," + customerDetailsResponse.getResponseData().getJobDetails().getSlot());
                tvComment.setText(customerDetailsResponse.getResponseData().getJobDetails().getOtherInfo());
                tvSiteImage.setVisibility(View.VISIBLE);

                llJobAmount.setVisibility(View.VISIBLE);
                llScheduleOn.setVisibility(View.VISIBLE);
                tvJobAmount.setText(customerDetailsResponse.getResponseData().getJobDetails().getPrice());
                tvJobScheduleOn.setText(setDate( customerDetailsResponse.getResponseData().getJobDetails().getDate()) + "," + customerDetailsResponse.getResponseData().getJobDetails().getSlot());

                if (customerDetailsResponse.getResponseData().getJobDetails().getOnSitePresent()) {
                    tvCustomerPresent.setText("Yes");
                } else {
                    tvCustomerPresent.setText("No");
                }

                if (customerDetailsResponse.getResponseData().getJobDetails().getIsBlock()) {
                    ivblocked.setVisibility(View.GONE);
                    ivunblocked.setVisibility(View.VISIBLE);
                } else {
                    ivblocked.setVisibility(View.VISIBLE);
                    ivunblocked.setVisibility(View.GONE);
                }

//
                if (customerDetailsResponse.getResponseData().getJobDetails().getServiceTypeId().equals("1")) {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getJobDetails().getLawnSize());
                } else if(customerDetailsResponse.getResponseData().getJobDetails().getServiceTypeId().equals("2")) {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getJobDetails().getGrassSize());
                }
                else
                {
                    tvServiceSize.setText(customerDetailsResponse.getResponseData().getJobDetails().getLawnSize());
                }


                if (customerDetailsResponse.getResponseData().getJobDetails().getServiceTypeId().equals("1")) {
                    String text=customerDetailsResponse.getResponseData().getJobDetails().getServideNeeded();
                    if(!customerDetailsResponse.getResponseData().getJobDetails().getGrassSize().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getJobDetails().getGrassSize();
                    }

                    tvServiceNeed.setText(text);
                } else if(customerDetailsResponse.getResponseData().getJobDetails().getServiceTypeId().equals("2")) {
                    String text=customerDetailsResponse.getResponseData().getJobDetails().getServideNeeded();
                    if(!customerDetailsResponse.getResponseData().getJobDetails().getHowManyTreeRemove().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getJobDetails().getHowManyTreeRemove();
                    }
                    if(!customerDetailsResponse.getResponseData().getJobDetails().getHowManyTreeTrim().equals(""))
                    {
                        text=text+", "+ customerDetailsResponse.getResponseData().getJobDetails().getHowManyTreeTrim();

                    }
                    Log.e("CustomerDetails",text);
                    tvServiceNeed.setText(text);
                }
                else
                {
                    tvServiceNeed.setText(customerDetailsResponse.getResponseData().getJobDetails().getServideNeeded()+", "+customerDetailsResponse.getResponseData().getJobDetails().getGrassSize());
                }

                Glide.with(getApplication()).load(customerDetailsResponse.getResponseData().getJobDetails().getCustomerProfileImage())
                        .into(iv_prc);
                if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 0) {
                    tvSiteImage.setVisibility(View.GONE);
                    ll_site_image.setVisibility(View.GONE);
                } else {
                    tvSiteImage.setVisibility(View.VISIBLE);
                    ll_site_image.setVisibility(View.VISIBLE);
                }

                //  Toast.makeText(getApplicationContext(), taskDataBean.getCustomer_work_image().size() + "", Toast.LENGTH_SHORT).show();
                if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 1) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    a.add(ImageOne);
                } else if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 2) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(1);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                } else if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 3) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(2);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                } else if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 4) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(2);
                    ImageFour = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(3);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);

                } else if (customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().size() == 5) {
                    ImageOne = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(0);
                    ImageTwo = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(1);
                    ImageThree = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(2);
                    ImageFour = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(3);
                    ImageFive = customerDetailsResponse.getResponseData().getJobDetails().getCustomerWorkImage().get(4);
                    a.add(ImageOne);
                    a.add(ImageTwo);
                    a.add(ImageThree);
                    a.add(ImageFour);
                    a.add(ImageFive);
                }


                // adapter = new TakeImageAdapterAC(a, this);
                adapter.loadList(a);
                adapter.setAdapterListener(new TakeImageAdapterAC.CellListener() {
                    @Override
                    public void onItemClick(int pos, ArrayList<String> items) {
                        String str = "";
                        if (pos == 0) {
                            str = ImageOne;
                        }
                        if (pos == 1) {
                            str = ImageTwo;
                        }
                        if (pos == 2) {
                            str = ImageThree;
                        }
                        if (pos == 3) {
                            str = ImageFour;
                        }
                        if (pos == 4) {
                            str = ImageFive;
                        }

                        Glide.with(mContext).
                                load(str)
                                // .placeholder(R.drawable.image_placeholder)
                                .into(iv_gal_img);
                        dialog.show();
                    }
                });
                adapter.notifyDataSetChanged();
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_take_image);
                LinearLayoutManager layoutManagerMakeOverBrand = new LinearLayoutManager(this);
                layoutManagerMakeOverBrand.setOrientation(LinearLayoutManager.HORIZONTAL);


                recyclerView.setLayoutManager(layoutManagerMakeOverBrand);
                recyclerView.setAdapter(adapter);

            }
        }
        else
        {

        }
    }




    public String setDate(String date)
    {
        String dayName="";



        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date1 = null;
        try {
            date1 = fmt.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date1);


        switch (cal. get(Calendar. DAY_OF_WEEK))
        {
            case 1:

                dayName="Sunday";
                break;
            case 2:

                dayName="Monday";
                break;
            case 3:

                dayName="Tuesday";
                break;
            case 4:

                dayName="Wednesday";
                break;
            case 5:

                dayName="Thursday";
                break;
            case 6:

                dayName="Friday";
                break;
            case 7:

                dayName="Saturday";
                break;

        }

        String[]word=date.split("-");
        String finaldate="",month="";
        switch (word[1])
        {
            case "01":
                month="Jan";
                break;
            case "02":
                month="Feb";
                break;
            case "03":
                month="Mar";
                break;
            case "04":
                month="Apr";
                break;
            case "05":
                month="May";
                break;
            case "06":
                month="Jun";
                break;
            case "07":
                month="Jul";
                break;
            case "08":
                month="Aug";
                break;
            case "09":
                month="Sep";
                break;
            case "10":
                month="Octr";
                break;
            case "11":
                month="Nov";
                break;
            case "12":
                month="Dec";
                break;

        }
        finaldate=dayName+", "+word[2]+" "+month+" "+word[0];

        return finaldate;
    }

}
