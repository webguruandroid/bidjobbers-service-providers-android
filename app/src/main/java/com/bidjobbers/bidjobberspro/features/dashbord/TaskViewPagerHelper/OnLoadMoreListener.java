package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper;

public interface OnLoadMoreListener {
    void onLoadMore();
}
