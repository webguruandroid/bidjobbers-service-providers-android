package com.bidjobbers.bidjobberspro.features.OnTimeScreen.IntroductionFragment;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class IntroFragmentPresenter<V extends IntroFragmentMvpView>
        extends BaseFragmentPresenter<V>
        implements  IntroFragmentMvpPresenter<V> {

    @Inject
    public IntroFragmentPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }
}
