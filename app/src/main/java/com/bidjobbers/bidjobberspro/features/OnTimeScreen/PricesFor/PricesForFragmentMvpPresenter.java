package com.bidjobbers.bidjobberspro.features.OnTimeScreen.PricesFor;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface PricesForFragmentMvpPresenter <V extends PricesForFragmentMvpView> extends BaseFragmentMvpPresenter<V> {

    void onGetDataFromApi();


}
