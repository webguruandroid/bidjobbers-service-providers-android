package com.bidjobbers.bidjobberspro.features.NewArchive;

import com.bidjobbers.bidjobberspro.data.network.models.Archived.ArchiveListResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList.BlockCustomerListResponse;
import com.bidjobbers.bidjobberspro.data.network.models.NewArchive.NewArchiveList;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface NewArchiveMvpView extends MvpView {

    void successfullyGetArchiveCustomer(NewArchiveList archiveListResponse);
}
