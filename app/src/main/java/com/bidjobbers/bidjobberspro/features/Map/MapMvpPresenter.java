package com.bidjobbers.bidjobberspro.features.Map;

import com.bidjobbers.bidjobberspro.shared.base.MvpPresenter;

public interface MapMvpPresenter <V extends MapMvpView> extends MvpPresenter<V> {
}
