package com.bidjobbers.bidjobberspro.features.dashbord.ui.slideshow;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface NotificationFragmentMvpPresenter <V extends NotificationFragmentMvpView> extends BaseFragmentMvpPresenter<V> {

    void getNotificationList();
    void getJobStatus(String local,String taskId);
}
