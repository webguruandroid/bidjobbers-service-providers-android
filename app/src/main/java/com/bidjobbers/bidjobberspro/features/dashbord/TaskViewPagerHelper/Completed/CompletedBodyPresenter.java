package com.bidjobbers.bidjobberspro.features.dashbord.TaskViewPagerHelper.Completed;

import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.data.network.models.CompletedTask.CompletedTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.CompletedTask.CompletedTaskResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentPresenter;
import com.bidjobbers.bidjobberspro.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class CompletedBodyPresenter<V extends CompletedBodyMvpView>
        extends BaseFragmentPresenter<V>
        implements CompletedBodyMvpPresenter<V> {


    @Inject
    public CompletedBodyPresenter(DataManager mDataManager, SchedulerProvider mSchedulerProvider, CompositeDisposable mCompositeDisposable) {
        super(mDataManager, mSchedulerProvider, mCompositeDisposable);
    }

    @Override
    public void getAllTasks(String currentpage) {


        CompletedTaskRequest completedTaskRequest=new CompletedTaskRequest(getDataManager().getLocalValue(),currentpage);


        getCompositeDisposable().add(getDataManager().postCompleteTask("Bearer "+getDataManager().getAccess_token(),"application/json",completedTaskRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CompletedTaskResponse>() {
                    @Override
                    public void onSuccess(CompletedTaskResponse completedTaskResponse) {



                        getMvpView().hideLoading();
                        if(completedTaskResponse.getResponseCode()==1) {
                            getMvpView().allTasksName(completedTaskResponse);
                        }
                        else if(completedTaskResponse.getResponseCode()==401)
                        {
                            getMvpView().onError(completedTaskResponse.getResponseText());
                            getMvpView().onDeactivate(completedTaskResponse.getResponseText());
                        }
                        else
                        {
                            getMvpView().allTasksName(completedTaskResponse);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        getMvpView().hideLoading();
                    }
                }));

    }
}
