package com.bidjobbers.bidjobberspro.features.TestMVP;

import com.bidjobbers.bidjobberspro.features.TestMVP.savedata.TestPersistData;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface TestMvpView extends MvpView {
    void onTestButtonPress(String data);
    void onTestResult(TestPersistData mTestPersistData);
    void onTestResultFail();
    void onDisplayData(String Name);
}
