package com.bidjobbers.bidjobberspro.features.Archive;

import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList.BlockCustomerListResponse;
import com.bidjobbers.bidjobberspro.data.network.models.UnBlockUser.UnBlockUserResponse;
import com.bidjobbers.bidjobberspro.shared.base.MvpView;

public interface ArchiveMvpView extends MvpView {

    void successfullyGetBlockCustomer(BlockCustomerListResponse blockCustomerListResponse);

    void onSuccessfullyUnBlockUser(UnBlockUserResponse unBlockUserResponse);
}
