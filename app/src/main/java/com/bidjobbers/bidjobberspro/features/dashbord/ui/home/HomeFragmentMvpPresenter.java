package com.bidjobbers.bidjobberspro.features.dashbord.ui.home;

import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpPresenter;

public interface HomeFragmentMvpPresenter <V extends HomeFragmentMvpView> extends BaseFragmentMvpPresenter<V> {

    public void getAllTasks();
    void onGetNotificationCount();
}
