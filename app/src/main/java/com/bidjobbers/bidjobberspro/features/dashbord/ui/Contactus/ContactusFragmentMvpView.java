package com.bidjobbers.bidjobberspro.features.dashbord.ui.Contactus;

import com.bidjobbers.bidjobberspro.data.network.models.ContactUs.ContactUsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface ContactusFragmentMvpView extends BaseFragmentMvpView {
    void successfullyGetProfile(getProfileResponse getProfileResponse);
    void onDeactivate(String data);
    void successfullyContactUs(ContactUsResponse contactUsResponse);
}
