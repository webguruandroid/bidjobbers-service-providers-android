package com.bidjobbers.bidjobberspro.features.OnTimeScreen.SocialArea;

import com.bidjobbers.bidjobberspro.shared.DataModelForFirstTimeQusAns;
import com.bidjobbers.bidjobberspro.shared.base.fragment.BaseFragmentMvpView;

public interface SocialFragmentMvpView extends BaseFragmentMvpView {

    public void resultAddSocialLinkOne();

    public void resultAddSocialLinkTwo();

    public void resultAddSocialLinkThree();

    void setDataFromApi(DataModelForFirstTimeQusAns.SocialMediaLink socialMediaLink);

}
