package com.bidjobbers.bidjobberspro.utils.Validation;

public class BusinessLocationResult {

    boolean error = false;
    String error_text= "";

    String pinNumberData, locationData ="";

    public BusinessLocationResult(boolean error, String error_text, String pinNumberData, String locationData) {
        this.error = error;
        this.error_text = error_text;
        this.pinNumberData = pinNumberData;
        this.locationData = locationData;
    }

    public boolean isError() {
        return error;
    }

    public String getError_text() {
        return error_text;
    }

    public String getPinNumberData() {
        return pinNumberData;
    }

    public String getLocationData() {
        return locationData;
    }
}
