package com.bidjobbers.bidjobberspro.utils.Validation;

public class TypeOfPropertyResult {


    boolean error= false;
    String errorText="";
    boolean residentialFlag = false;
    boolean commercialFlag= false;
    boolean haveALincece= false;
    String licenceText="";

    public TypeOfPropertyResult(boolean error, String errorText, boolean residentialFlag, boolean commercialFlag, boolean haveALincece, String licenceText) {
        this.error = error;
        this.errorText = errorText;
        this.residentialFlag = residentialFlag;
        this.commercialFlag = commercialFlag;
        this.haveALincece = haveALincece;
        this.licenceText = licenceText;
    }


    public boolean isError() {
        return error;
    }

    public String getErrorText() {
        return errorText;
    }

    public boolean isResidentialFlag() {
        return residentialFlag;
    }

    public boolean isCommercialFlag() {
        return commercialFlag;
    }

    public boolean isHaveALincece() {
        return haveALincece;
    }

    public String getLicenceText() {
        return licenceText;
    }


    @Override
    public String toString() {
        return "TypeOfPropertyResult{" +
                "error=" + error +
                ", errorText='" + errorText + '\'' +
                ", residentialFlag=" + residentialFlag +
                ", commercialFlag=" + commercialFlag +
                ", haveALincece=" + haveALincece +
                ", licenceText='" + licenceText + '\'' +
                '}';
    }
}
