package com.bidjobbers.bidjobberspro.utils.Validation;

public class ReturnSocialFragmentResult {

    String errorText="";
    boolean error= false;

    String media_one="",media_two="",media_three="",media_four="",web_site="";


    public ReturnSocialFragmentResult(String errorText, boolean error, String media_one, String media_two, String media_three, String media_four, String web_site)
    {
        this.errorText = errorText;
        this.error = error;
        this.media_one = media_one;
        this.media_two = media_two;
        this.media_three = media_three;
        this.media_four = media_four;
        this.web_site = web_site;
    }

    public String getErrorText() {
        return errorText;
    }

    public boolean isError() {
        return error;
    }

    public String getMedia_one() {
        return media_one;
    }

    public String getMedia_two() {
        return media_two;
    }

    public String getMedia_three() {
        return media_three;
    }

    public String getMedia_four() {
        return media_four;
    }

    public String getWeb_site() {
        return web_site;
    }
}
