package com.bidjobbers.bidjobberspro.utils.Validation;

public class BusinessInfoResult {

    boolean error=false;
    String error_text="";
    String company_name="";
    String no_of_employee="";
    String compay_reg_number="";
    String year_foundation="";

    public BusinessInfoResult(boolean error, String error_text, String company_name, String no_of_employee, String compay_reg_number, String year_foundation) {
        this.error = error;
        this.error_text = error_text;
        this.company_name = company_name;
        this.no_of_employee = no_of_employee;
        this.compay_reg_number = compay_reg_number;
        this.year_foundation = year_foundation;
    }

    public boolean isError() {
        return error;
    }

    public String getError_text() {
        return error_text;
    }

    public String getCompany_name() {
        return company_name;
    }

    public String getNo_of_employee() {
        return no_of_employee;
    }

    public String getCompay_reg_number() {
        return compay_reg_number;
    }

    public String getYear_foundation() {
        return year_foundation;
    }
}
