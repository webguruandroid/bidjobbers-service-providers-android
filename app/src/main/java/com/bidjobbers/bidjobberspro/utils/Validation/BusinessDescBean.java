package com.bidjobbers.bidjobberspro.utils.Validation;

public class BusinessDescBean {


    boolean error=false;
    String error_text= "";
    String edt_dtls_data="";


    public BusinessDescBean(boolean error, String error_text, String edt_dtls_data) {
        this.error = error;
        this.error_text = error_text;
        this.edt_dtls_data = edt_dtls_data;
    }

    public boolean isError() {
        return error;
    }

    public String getError_text() {
        return error_text;
    }

    public String getEdt_dtls_data() {
        return edt_dtls_data;
    }

}
