package com.bidjobbers.bidjobberspro.utils.Validation;

public class BusinessHoursDetaisResult {

    boolean error = false;
    String error_text = "";

    boolean mondayFlag = false;
    String mondayTiming = "";

    boolean tuesdayFlag = false;
    String tuesdayTiming = "";

    boolean wednesdayFlag = false;
    String wednesdayTiming = "";

    boolean thursdayFlag = false;
    String thursdayTiming = "";

    boolean fridayFlag = false;
    String fridayTiming = "";

    boolean saturdayFlag = false;
    String saturdayTiming = "";

    boolean sundayFlag = false;
    String sundayTiming = "";


    public BusinessHoursDetaisResult(boolean error, String error_text, boolean mondayFlag, String mondayTiming, boolean tuesdayFlag, String tuesdayTiming, boolean wednesdayFlag, String wednesdayTiming, boolean thursdayFlag, String thursdayTiming, boolean fridayFlag, String fridayTiming, boolean saturdayFlag, String saturdayTiming, boolean sundayFlag, String sundayTiming) {
        this.error = error;
        this.error_text = error_text;
        this.mondayFlag = mondayFlag;
        this.mondayTiming = mondayTiming;
        this.tuesdayFlag = tuesdayFlag;
        this.tuesdayTiming = tuesdayTiming;
        this.wednesdayFlag = wednesdayFlag;
        this.wednesdayTiming = wednesdayTiming;
        this.thursdayFlag = thursdayFlag;
        this.thursdayTiming = thursdayTiming;
        this.fridayFlag = fridayFlag;
        this.fridayTiming = fridayTiming;
        this.saturdayFlag = saturdayFlag;
        this.saturdayTiming = saturdayTiming;
        this.sundayFlag = sundayFlag;
        this.sundayTiming = sundayTiming;
    }

    public boolean isError() {
        return error;
    }

    public String getError_text() {
        return error_text;
    }

    public boolean isMondayFlag() {
        return mondayFlag;
    }

    public String getMondayTiming() {
        return mondayTiming;
    }

    public boolean isTuesdayFlag() {
        return tuesdayFlag;
    }

    public String getTuesdayTiming() {
        return tuesdayTiming;
    }

    public boolean isWednesdayFlag() {
        return wednesdayFlag;
    }

    public String getWednesdayTiming() {
        return wednesdayTiming;
    }

    public boolean isThursdayFlag() {
        return thursdayFlag;
    }

    public String getThursdayTiming() {
        return thursdayTiming;
    }

    public boolean isFridayFlag() {
        return fridayFlag;
    }

    public String getFridayTiming() {
        return fridayTiming;
    }

    public boolean isSaturdayFlag() {
        return saturdayFlag;
    }

    public String getSaturdayTiming() {
        return saturdayTiming;
    }

    public boolean isSundayFlag() {
        return sundayFlag;
    }

    public String getSundayTiming() {
        return sundayTiming;
    }
}
