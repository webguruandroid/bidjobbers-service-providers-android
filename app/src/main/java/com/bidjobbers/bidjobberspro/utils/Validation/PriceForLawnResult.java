package com.bidjobbers.bidjobberspro.utils.Validation;

public class PriceForLawnResult {

    boolean error;
    String error_text;

    String small_price="";
    String medium_price="";
    String large_price="";
    String very_large_price="";

    public PriceForLawnResult(boolean error, String error_text, String small_price, String medium_price, String large_price, String very_large_price) {
        this.error = error;
        this.error_text = error_text;
        this.small_price = small_price;
        this.medium_price = medium_price;
        this.large_price = large_price;
        this.very_large_price = very_large_price;
    }

    public boolean isError() {
        return error;
    }

    public String getError_text() {
        return error_text;
    }

    public String getSmall_price() {
        return small_price;
    }

    public String getMedium_price() {
        return medium_price;
    }

    public String getLarge_price() {
        return large_price;
    }

    public String getVery_large_price() {
        return very_large_price;
    }

    @Override
    public String toString() {
        return "PriceForLawnResult{" +
                "error=" + error +
                ", error_text='" + error_text + '\'' +
                ", small_price='" + small_price + '\'' +
                ", medium_price='" + medium_price + '\'' +
                ", large_price='" + large_price + '\'' +
                ", very_large_price='" + very_large_price + '\'' +
                '}';
    }
}
