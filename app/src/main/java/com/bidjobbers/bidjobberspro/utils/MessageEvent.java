package com.bidjobbers.bidjobberspro.utils;

/*
Eventbus for sendinf data between component
 */
public class MessageEvent {
    public String mMessage;

    public MessageEvent(String message) {
        mMessage = message;
    }

    public String getMessage() {
        return mMessage;
    }
}
