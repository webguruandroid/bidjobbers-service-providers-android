package com.bidjobbers.bidjobberspro.utils.Fcm;

import android.annotation.TargetApi;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.DataManager;
import com.bidjobbers.bidjobberspro.features.CustomerDetails.CustomerDetailsActivity;
import com.bidjobbers.bidjobberspro.features.login.LoginActivity;
import com.bidjobbers.bidjobberspro.utils.MessageEvent;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

import javax.inject.Inject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Inject
    DataManager mDataManager;
    Bitmap imageBitmap;
    Uri defaultSoundUri;

    //Message{status_id=1, task_id=591, status=Received Jobs, type=taskcreated, message=A new task has been created}

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        try {
            Log.e("msgreceived" ,""+remoteMessage.getNotification()+"--"+remoteMessage.getData());
           // Log.e("msgreceived" ,"Message"+remoteMessage.getData());
            if(remoteMessage.getData()!=null)
            {
                Log.e("msgreceived" ,""+remoteMessage.getNotification());
                Log.e("msgreceived" ,"-"+remoteMessage.getNotification().getTitle());
                    Log.e("msgreceived" ,"-"+remoteMessage.getNotification().getBody());
                      //  Log.e("msgreceived" ,"Message"+remoteMessage.getData());
                        if(remoteMessage.getData().get("type").equals("deactivated"))
                        {
                            sendNotificationProduct( remoteMessage.getNotification().getBody(),remoteMessage.getData().get("type"),"","",remoteMessage.getNotification().getTitle());
                        }
                        else {
                            sendNotificationProduct(remoteMessage.getNotification().getBody(), remoteMessage.getData().get("type"), remoteMessage.getData().get("status_id"), remoteMessage.getData().get("task_id"), remoteMessage.getNotification().getTitle());
                        }

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }



    public void sendNotificationProduct(String message,String type,String statusid,String taskid,String status)
    {
        Intent intent=null;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {

            if(type.equalsIgnoreCase("deactivated")) {
                EventBus.getDefault().postSticky(new MessageEvent("deactivated"));
                Random random = new Random();
                int m = random.nextInt(9999 - 1000) + 1000;


                intent = new Intent(this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.logo);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, m, intent, PendingIntent.FLAG_ONE_SHOT);
                NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


                NotificationCompat.Builder mBuilder1 = new NotificationCompat.Builder(this, "Default")
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setContentTitle(status)
                        .setSmallIcon(R.mipmap.logo)
                        .setLargeIcon(largeIcon)
                        .setAutoCancel(true)
                        .setSound(sound)
                        .setContentText(message)
                        .setContentIntent(pendingIntent);


                manager.notify(m, mBuilder1.build());
                Log.e("msgreceived", "end");
            }
            else {
                Log.e("msgreceived", "less O");
                Log.e("msgreceived", type + message + statusid + taskid);
                Random random = new Random();
                int m = random.nextInt(9999 - 1000) + 1000;


                intent = new Intent(this, CustomerDetailsActivity.class);
                intent.putExtra("status", statusid);
                intent.putExtra("taskId", taskid);
                intent.putExtra("bidId", "");
                intent.putExtra("Tag", "Foreground");

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                Bitmap largeIcon = BitmapFactory.decodeResource(getResources(), R.mipmap.logo);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, m, intent, PendingIntent.FLAG_ONE_SHOT);
                NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


                NotificationCompat.Builder mBuilder1 = new NotificationCompat.Builder(this, "Default")
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setContentTitle(status)
                        .setSmallIcon(R.mipmap.logo)
                        .setLargeIcon(largeIcon)
                        .setAutoCancel(true)
                        .setSound(sound)
                        .setContentText(message)
                        .setContentIntent(pendingIntent);


                manager.notify(m, mBuilder1.build());
                Log.e("msgreceived", "end");
            }
        }
        else
        {

            if(type.equalsIgnoreCase("deactivated")) {
                EventBus.getDefault().postSticky(new MessageEvent("deactivated"));

                Random random = new Random();
                int m = random.nextInt(9999 - 1000) + 1000;


                intent= new Intent(this, CustomerDetailsActivity.class);
                intent.putExtra("status",statusid );
                intent.putExtra("taskId",taskid);
                intent.putExtra("bidId","");
                intent.putExtra("Tag","Foreground");





                //  RemoteViews remoteViews=new RemoteViews(getPackageName(),R.layout.custome_notification);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                Bitmap largeIcon = BitmapFactory.decodeResource(getResources(),R.mipmap.logo);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, m, intent, PendingIntent.FLAG_ONE_SHOT);
                String channelId = "Default";
                NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Log.e("msgreceived",type+message+status+statusid);

                createChannel(manager);
                NotificationCompat.Builder mBuilder1 = new NotificationCompat.Builder(this, "Default")
                        .setChannelId(channelId)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setContentTitle(status)
                        .setSmallIcon(R.mipmap.logo)
                        .setLargeIcon(largeIcon)
                        .setAutoCancel(true)
                        .setSound(sound)
                        .setContentText(message)
                        .setContentIntent(pendingIntent);

                manager.notify(m, mBuilder1.build());

            }

             Log.e("msgreceived","greater O");
            Random random = new Random();
            int m = random.nextInt(9999 - 1000) + 1000;


                intent= new Intent(this, CustomerDetailsActivity.class);
                intent.putExtra("status",statusid );
                intent.putExtra("taskId",taskid);
                intent.putExtra("bidId","");
            intent.putExtra("Tag","Foreground");





          //  RemoteViews remoteViews=new RemoteViews(getPackageName(),R.layout.custome_notification);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            Bitmap largeIcon = BitmapFactory.decodeResource(getResources(),R.mipmap.logo);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, m, intent, PendingIntent.FLAG_ONE_SHOT);
            String channelId = "Default";
            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Log.e("msgreceived",type+message+status+statusid);

            createChannel(manager);
            NotificationCompat.Builder mBuilder1 = new NotificationCompat.Builder(this, "Default")
                    .setChannelId(channelId)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setContentTitle(status)
                    .setSmallIcon(R.mipmap.logo)
                    .setLargeIcon(largeIcon)
                    .setAutoCancel(true)
                    .setSound(sound)
                    .setContentText(message)
                    .setContentIntent(pendingIntent);

            manager.notify(m, mBuilder1.build());

            Log.e("msgreceived","end");
        }


    }





    @TargetApi(26)
    private void createChannel(NotificationManager notificationManager) {

        int importance = NotificationManager.IMPORTANCE_DEFAULT;

        NotificationChannel mChannel = new NotificationChannel("Default", "Default channel", importance);

        mChannel.enableLights(true);
        mChannel.setLightColor(Color.BLUE);
        mChannel.setShowBadge(true);
        notificationManager.createNotificationChannel(mChannel);
    }


    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }






}
