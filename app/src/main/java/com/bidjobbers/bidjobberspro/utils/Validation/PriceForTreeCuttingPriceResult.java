package com.bidjobbers.bidjobberspro.utils.Validation;

public class PriceForTreeCuttingPriceResult {

    boolean error = false;
    String error_text="";
    String oneTree="";
    String twoTree="";
    String fourTree="";

    public PriceForTreeCuttingPriceResult(boolean error, String error_text, String oneTree, String twoTree, String fourTree)
    {
        this.error = error;
        this.error_text = error_text;
        this.oneTree = oneTree;
        this.twoTree = twoTree;
        this.fourTree = fourTree;
    }

    public boolean isError()
    {
        return error;
    }

    public String getError_text()
    {
        return error_text;
    }

    public String getOneTree()
    {
        return oneTree;
    }

    public String getTwoTree()
    {
        return twoTree;
    }

    public String getFourTree()
    {
        return fourTree;
    }
}
