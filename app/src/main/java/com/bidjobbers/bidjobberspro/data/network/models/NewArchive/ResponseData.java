
package com.bidjobbers.bidjobberspro.data.network.models.NewArchive;

import java.util.Collection;
import java.util.List;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class ResponseData {

    @SerializedName("archive_job_list")
    private List<ArchiveJobList> mArchiveJobList;
    @SerializedName("pagination")
    private Pagination mPagination;

    public Collection<? extends ArchiveJobList> getArchiveJobList() {
        return mArchiveJobList;
    }

    public void setArchiveJobList(List<ArchiveJobList> archiveJobList) {
        mArchiveJobList = archiveJobList;
    }

    public Pagination getPagination() {
        return mPagination;
    }

    public void setPagination(Pagination pagination) {
        mPagination = pagination;
    }

}
