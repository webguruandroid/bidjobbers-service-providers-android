package com.bidjobbers.bidjobberspro.data.network.models.IntroBusinessDetails;

public class IntroBusinessDetailsRequest {


    /**
     * locale : en
     * business_intro_desc : test new
     */

    private String locale;
    private String business_intro_desc;

    public IntroBusinessDetailsRequest(String locale, String business_intro_desc) {
        this.locale = locale;
        this.business_intro_desc = business_intro_desc;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getBusiness_intro_desc() {
        return business_intro_desc;
    }

    public void setBusiness_intro_desc(String business_intro_desc) {
        this.business_intro_desc = business_intro_desc;
    }
}
