
package com.bidjobbers.bidjobberspro.data.network.models.Archived;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class ArchiveListRequest {

    @SerializedName("locale")
    private String mLocale;
    @SerializedName("time_period")
    private String time_period;
    @SerializedName("current_page")
    private String current_page;


    public ArchiveListRequest(String mLocale, String time_period , String current_page) {
        this.mLocale = mLocale;
        this.time_period = time_period;
        this. current_page=current_page;
    }


    public String getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(String current_page) {
        this.current_page = current_page;
    }

    public String getTime_period() {
        return time_period;
    }

    public void setTime_period(String time_period) {
        this.time_period = time_period;
    }

    public String getLocale() {
        return mLocale;
    }

    public void setLocale(String locale) {
        mLocale = locale;
    }

}
