package com.bidjobbers.bidjobberspro.data.network.models.servicedetails;

import java.util.List;

public class ServiceDetailsResponse {


    /**
     * responseCode : 1
     * responseText : Data Found
     * responseData : {"serviceProviderDetails":{"name":"Payel Phurrr","phone":"0978464843","email":"payal@bidjobbers.com","image":"http://192.168.5.51/bidjobbers/public/jobber/profile_image/thumb/1575028474.jpg","rating":""},"propertyTypeDetails":{"propertyType":[{"id":"1","name":"Residential","is_selected":"1"},{"id":"2","name":"Commercial","is_selected":"0"}],"is_having_licence":"1","licence_number":"56789045"},"serviceTypeDetails":[{"parent_id":"1","parent_name":"Lawn Service","is_selected":"1","children":[{"id":"3","name":"Mow a Lawn","description":"Gardening Services","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_mow_a_lawn.png","is_selected":"1"},{"id":"4","name":"Aerate a Lawn","description":"Gardening Services","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_aerate_a_lawn.png","is_selected":"0"},{"id":"5","name":"Yard Waste","description":"Clippings & Removal","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_yard_waste_clippings_and_removal.png","is_selected":"0"}]},{"parent_id":"2","parent_name":"Tree Cutting","is_selected":"0","children":[{"id":"6","name":"Trees & Shrubs","description":"Trim or Remove","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/trees_and_shrub_trim_and_remove.png","is_selected":"0"},{"id":"7","name":"Tree Stumps","description":"Remove","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/tree_stump_remove.png","is_selected":"0"},{"id":"8","name":"Yard Waste","description":"Clippings & Removal","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/tree_yard_waste_clippings_and_removal.png","is_selected":"0"}]}],"workPhoto":{"is_added":"1","image_url":"http://192.168.5.51/bidjobbers/public/jobber/Lawn_service_image","image":["lawn_large.png","lawn_medium.png","lawn_small.png"]},"advanceBooking":{"attrbuteCount":[{"id":"1","count":"1"},{"id":"2","count":"2"},{"id":"3","count":"3"},{"id":"4","count":"4"},{"id":"5","count":"5"},{"id":"6","count":"6"},{"id":"7","count":"7"},{"id":"8","count":"8"},{"id":"9","count":"9"},{"id":"10","count":"10"},{"id":"11","count":"11"},{"id":"12","count":"12"},{"id":"13","count":"13"},{"id":"14","count":"14"},{"id":"15","count":"15"},{"id":"16","count":"16"},{"id":"17","count":"17"},{"id":"18","count":"18"},{"id":"19","count":"19"},{"id":"20","count":"20"},{"id":"21","count":"21"},{"id":"22","count":"22"},{"id":"23","count":"23"},{"id":"24","count":"24"},{"id":"25","count":"25"},{"id":"26","count":"26"},{"id":"27","count":"27"},{"id":"28","count":"28"},{"id":"29","count":"29"},{"id":"30","count":"30"}],"attrunit_for_how_far":[{"id":1,"unit":"Days"},{"id":2,"unit":"Weeks"},{"id":3,"unit":"Months"}],"attrunit_for_notice_period":[{"id":1,"unit":"Days"},{"id":4,"unit":"Hours"}],"advanceValueId":"30","advanceValue":"30","advanceNameid":"1","advanceName":"Days","noticeValueId":"30","noticeValue":"30","noticeNameId":"4","noticeName":"Hours"},"buisnessInfo":{"company_name":"test","founding_year":"01 May,1989","employee_count_selected_id":"1","no_of_employee":"0 to 5","company_reg_no":"123456","employeeDropdownData":[{"id":1,"no_of_employee":"0 to 5"},{"id":2,"no_of_employee":"5 to 10"},{"id":3,"no_of_employee":"more than 10"}]},"socialLink":{"social_media_links":["www.facebook.com","www.twitter.com"],"website_url":"https://www.google.com/"},"buisnessLocation":{"business_address":"kolkata","apartment":"EE-118","city":"kolkata","state":"west bengal","country":"India","zipcode":"123456"},"buisnessDesc":{"business_intro_desc":"test"},"lawnServicesDetails":[{"id":"1","name":"Small","short_description":"Less than 1000 sq ft","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_small.png","price":""},{"id":"2","name":"Medium","short_description":"1000 to 3000 sq ft","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_medium.png","price":""},{"id":"3","name":"Large","short_description":"3000 to 1,0000 sq ft","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_large.png","price":"$555"},{"id":"4","name":"Very Large","short_description":"More than 1,0000 sq ft","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_very_large.png","price":""},{"id":"5","name":"Unsure","short_description":"No Idea!","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_unsure.png","price":""}],"treeServicesDetails":[{"id":"1","name":"1 Tree","price":""},{"id":"2","name":"2-3 Tree","price":""},{"id":"3","name":"4-5 Tree","price":""},{"id":"4","name":"More than 5","price":""}],"buisnessHoursDetails":{"buisnessHrDropdown":[{"id":"1","time":"08:00"},{"id":"2","time":"09:00"},{"id":"3","time":"10:00"},{"id":"4","time":"11:00"},{"id":"5","time":"12:00"},{"id":"6","time":"13:00"},{"id":"7","time":"14:00"},{"id":"8","time":"15:00"},{"id":"9","time":"16:00"},{"id":"10","time":"17:00"},{"id":"11","time":"18:00"},{"id":"12","time":"19:00"},{"id":"13","time":"20:00"}],"businessHrData":[{"id":"1","dayName":"Sunday","from_time_id":"","from_time":"","end_time_id":"","end_time":"","isSelected":"0"},{"id":"2","dayName":"Monday","from_time_id":"1","from_time":"08:00","end_time_id":"2","end_time":"09:00","isSelected":"1"},{"id":"3","dayName":"Tuesday","from_time_id":"","from_time":"","end_time_id":"","end_time":"","isSelected":"0"},{"id":"4","dayName":"Wednesday","from_time_id":"","from_time":"","end_time_id":"","end_time":"","isSelected":"0"},{"id":"5","dayName":"Thursday","from_time_id":"","from_time":"","end_time_id":"","end_time":"","isSelected":"0"},{"id":"6","dayName":"Friday","from_time_id":"","from_time":"","end_time_id":"","end_time":"","isSelected":"0"},{"id":"7","dayName":"Saturday","from_time_id":"","from_time":"","end_time_id":"","end_time":"","isSelected":"0"}]}}
     */

    private int responseCode;
    private String responseText;
    private ResponseDataBean responseData;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        /**
         * serviceProviderDetails : {"name":"Payel Phurrr","phone":"0978464843","email":"payal@bidjobbers.com","image":"http://192.168.5.51/bidjobbers/public/jobber/profile_image/thumb/1575028474.jpg","rating":""}
         * propertyTypeDetails : {"propertyType":[{"id":"1","name":"Residential","is_selected":"1"},{"id":"2","name":"Commercial","is_selected":"0"}],"is_having_licence":"1","licence_number":"56789045"}
         * serviceTypeDetails : [{"parent_id":"1","parent_name":"Lawn Service","is_selected":"1","children":[{"id":"3","name":"Mow a Lawn","description":"Gardening Services","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_mow_a_lawn.png","is_selected":"1"},{"id":"4","name":"Aerate a Lawn","description":"Gardening Services","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_aerate_a_lawn.png","is_selected":"0"},{"id":"5","name":"Yard Waste","description":"Clippings & Removal","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_yard_waste_clippings_and_removal.png","is_selected":"0"}]},{"parent_id":"2","parent_name":"Tree Cutting","is_selected":"0","children":[{"id":"6","name":"Trees & Shrubs","description":"Trim or Remove","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/trees_and_shrub_trim_and_remove.png","is_selected":"0"},{"id":"7","name":"Tree Stumps","description":"Remove","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/tree_stump_remove.png","is_selected":"0"},{"id":"8","name":"Yard Waste","description":"Clippings & Removal","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/tree_yard_waste_clippings_and_removal.png","is_selected":"0"}]}]
         * workPhoto : {"is_added":"1","image_url":"http://192.168.5.51/bidjobbers/public/jobber/Lawn_service_image","image":["lawn_large.png","lawn_medium.png","lawn_small.png"]}
         * advanceBooking : {"attrbuteCount":[{"id":"1","count":"1"},{"id":"2","count":"2"},{"id":"3","count":"3"},{"id":"4","count":"4"},{"id":"5","count":"5"},{"id":"6","count":"6"},{"id":"7","count":"7"},{"id":"8","count":"8"},{"id":"9","count":"9"},{"id":"10","count":"10"},{"id":"11","count":"11"},{"id":"12","count":"12"},{"id":"13","count":"13"},{"id":"14","count":"14"},{"id":"15","count":"15"},{"id":"16","count":"16"},{"id":"17","count":"17"},{"id":"18","count":"18"},{"id":"19","count":"19"},{"id":"20","count":"20"},{"id":"21","count":"21"},{"id":"22","count":"22"},{"id":"23","count":"23"},{"id":"24","count":"24"},{"id":"25","count":"25"},{"id":"26","count":"26"},{"id":"27","count":"27"},{"id":"28","count":"28"},{"id":"29","count":"29"},{"id":"30","count":"30"}],"attrunit_for_how_far":[{"id":1,"unit":"Days"},{"id":2,"unit":"Weeks"},{"id":3,"unit":"Months"}],"attrunit_for_notice_period":[{"id":1,"unit":"Days"},{"id":4,"unit":"Hours"}],"advanceValueId":"30","advanceValue":"30","advanceNameid":"1","advanceName":"Days","noticeValueId":"30","noticeValue":"30","noticeNameId":"4","noticeName":"Hours"}
         * buisnessInfo : {"company_name":"test","founding_year":"01 May,1989","employee_count_selected_id":"1","no_of_employee":"0 to 5","company_reg_no":"123456","employeeDropdownData":[{"id":1,"no_of_employee":"0 to 5"},{"id":2,"no_of_employee":"5 to 10"},{"id":3,"no_of_employee":"more than 10"}]}
         * socialLink : {"social_media_links":["www.facebook.com","www.twitter.com"],"website_url":"https://www.google.com/"}
         * buisnessLocation : {"business_address":"kolkata","apartment":"EE-118","city":"kolkata","state":"west bengal","country":"India","zipcode":"123456"}
         * buisnessDesc : {"business_intro_desc":"test"}
         * lawnServicesDetails : [{"id":"1","name":"Small","short_description":"Less than 1000 sq ft","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_small.png","price":""},{"id":"2","name":"Medium","short_description":"1000 to 3000 sq ft","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_medium.png","price":""},{"id":"3","name":"Large","short_description":"3000 to 1,0000 sq ft","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_large.png","price":"$555"},{"id":"4","name":"Very Large","short_description":"More than 1,0000 sq ft","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_very_large.png","price":""},{"id":"5","name":"Unsure","short_description":"No Idea!","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_unsure.png","price":""}]
         * treeServicesDetails : [{"id":"1","name":"1 Tree","price":""},{"id":"2","name":"2-3 Tree","price":""},{"id":"3","name":"4-5 Tree","price":""},{"id":"4","name":"More than 5","price":""}]
         * buisnessHoursDetails : {"buisnessHrDropdown":[{"id":"1","time":"08:00"},{"id":"2","time":"09:00"},{"id":"3","time":"10:00"},{"id":"4","time":"11:00"},{"id":"5","time":"12:00"},{"id":"6","time":"13:00"},{"id":"7","time":"14:00"},{"id":"8","time":"15:00"},{"id":"9","time":"16:00"},{"id":"10","time":"17:00"},{"id":"11","time":"18:00"},{"id":"12","time":"19:00"},{"id":"13","time":"20:00"}],"businessHrData":[{"id":"1","dayName":"Sunday","from_time_id":"","from_time":"","end_time_id":"","end_time":"","isSelected":"0"},{"id":"2","dayName":"Monday","from_time_id":"1","from_time":"08:00","end_time_id":"2","end_time":"09:00","isSelected":"1"},{"id":"3","dayName":"Tuesday","from_time_id":"","from_time":"","end_time_id":"","end_time":"","isSelected":"0"},{"id":"4","dayName":"Wednesday","from_time_id":"","from_time":"","end_time_id":"","end_time":"","isSelected":"0"},{"id":"5","dayName":"Thursday","from_time_id":"","from_time":"","end_time_id":"","end_time":"","isSelected":"0"},{"id":"6","dayName":"Friday","from_time_id":"","from_time":"","end_time_id":"","end_time":"","isSelected":"0"},{"id":"7","dayName":"Saturday","from_time_id":"","from_time":"","end_time_id":"","end_time":"","isSelected":"0"}]}
         */

        private ServiceProviderDetailsBean serviceProviderDetails;
        private PropertyTypeDetailsBean propertyTypeDetails;
        private WorkPhotoBean workPhoto;
        private AdvanceBookingBean advanceBooking;
        private BuisnessInfoBean buisnessInfo;
        private SocialLinkBean socialLink;
        private BuisnessLocationBean buisnessLocation;
        private BuisnessDescBean buisnessDesc;
        private BuisnessHoursDetailsBean buisnessHoursDetails;
        private List<ServiceTypeDetailsBean> serviceTypeDetails;
        private List<LawnServicesDetailsBean> lawnServicesDetails;
        private List<TreeServicesDetailsBean> treeServicesDetails;
        private TransactionAccountDetails transactionAccountDetails;
        private String isTransAccountDetailsAvailable;


        public String getIsTransAccountDetailsAvailable() {
            return isTransAccountDetailsAvailable;
        }

        public void setIsTransAccountDetailsAvailable(String isTransAccountDetailsAvailable) {
            this.isTransAccountDetailsAvailable = isTransAccountDetailsAvailable;
        }

        public TransactionAccountDetails getTransactionAccountDetails() {
            return transactionAccountDetails;
        }

        public void setTransactionAccountDetails(TransactionAccountDetails transactionAccountDetails) {
            this.transactionAccountDetails = transactionAccountDetails;
        }

        public ServiceProviderDetailsBean getServiceProviderDetails() {
            return serviceProviderDetails;
        }

        public void setServiceProviderDetails(ServiceProviderDetailsBean serviceProviderDetails) {
            this.serviceProviderDetails = serviceProviderDetails;
        }

        public PropertyTypeDetailsBean getPropertyTypeDetails() {
            return propertyTypeDetails;
        }

        public void setPropertyTypeDetails(PropertyTypeDetailsBean propertyTypeDetails) {
            this.propertyTypeDetails = propertyTypeDetails;
        }

        public WorkPhotoBean getWorkPhoto() {
            return workPhoto;
        }

        public void setWorkPhoto(WorkPhotoBean workPhoto) {
            this.workPhoto = workPhoto;
        }

        public AdvanceBookingBean getAdvanceBooking() {
            return advanceBooking;
        }

        public void setAdvanceBooking(AdvanceBookingBean advanceBooking) {
            this.advanceBooking = advanceBooking;
        }

        public BuisnessInfoBean getBuisnessInfo() {
            return buisnessInfo;
        }

        public void setBuisnessInfo(BuisnessInfoBean buisnessInfo) {
            this.buisnessInfo = buisnessInfo;
        }

        public SocialLinkBean getSocialLink() {
            return socialLink;
        }

        public void setSocialLink(SocialLinkBean socialLink) {
            this.socialLink = socialLink;
        }

        public BuisnessLocationBean getBuisnessLocation() {
            return buisnessLocation;
        }

        public void setBuisnessLocation(BuisnessLocationBean buisnessLocation) {
            this.buisnessLocation = buisnessLocation;
        }

        public BuisnessDescBean getBuisnessDesc() {
            return buisnessDesc;
        }

        public void setBuisnessDesc(BuisnessDescBean buisnessDesc) {
            this.buisnessDesc = buisnessDesc;
        }

        public BuisnessHoursDetailsBean getBuisnessHoursDetails() {
            return buisnessHoursDetails;
        }

        public void setBuisnessHoursDetails(BuisnessHoursDetailsBean buisnessHoursDetails) {
            this.buisnessHoursDetails = buisnessHoursDetails;
        }

        public List<ServiceTypeDetailsBean> getServiceTypeDetails() {
            return serviceTypeDetails;
        }

        public void setServiceTypeDetails(List<ServiceTypeDetailsBean> serviceTypeDetails) {
            this.serviceTypeDetails = serviceTypeDetails;
        }

        public List<LawnServicesDetailsBean> getLawnServicesDetails() {
            return lawnServicesDetails;
        }

        public void setLawnServicesDetails(List<LawnServicesDetailsBean> lawnServicesDetails) {
            this.lawnServicesDetails = lawnServicesDetails;
        }

        public List<TreeServicesDetailsBean> getTreeServicesDetails() {
            return treeServicesDetails;
        }

        public void setTreeServicesDetails(List<TreeServicesDetailsBean> treeServicesDetails) {
            this.treeServicesDetails = treeServicesDetails;
        }

        public static class ServiceProviderDetailsBean {
            /**
             * name : Payel Phurrr
             * phone : 0978464843
             * email : payal@bidjobbers.com
             * image : http://192.168.5.51/bidjobbers/public/jobber/profile_image/thumb/1575028474.jpg
             * rating :
             */

            private String name;
            private String phone;
            private String email;
            private String image;
            private String rating;
            private String hasBusinessDetails;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getRating() {
                return rating;
            }

            public void setRating(String rating) {
                this.rating = rating;
            }

            public String getHasBusinessDetails() {
                return hasBusinessDetails;
            }

            public void setHasBusinessDetails(String hasBusinessDetails) {
                this.hasBusinessDetails = hasBusinessDetails;
            }
        }

        public static class PropertyTypeDetailsBean {
            /**
             * propertyType : [{"id":"1","name":"Residential","is_selected":"1"},{"id":"2","name":"Commercial","is_selected":"0"}]
             * is_having_licence : 1
             * licence_number : 56789045
             */

            private String is_having_licence;
            private String licence_number;
            private List<PropertyTypeBean> propertyType;

            public String getIs_having_licence() {
                return is_having_licence;
            }

            public void setIs_having_licence(String is_having_licence) {
                this.is_having_licence = is_having_licence;
            }

            public String getLicence_number() {
                return licence_number;
            }

            public void setLicence_number(String licence_number) {
                this.licence_number = licence_number;
            }

            public List<PropertyTypeBean> getPropertyType() {
                return propertyType;
            }

            public void setPropertyType(List<PropertyTypeBean> propertyType) {
                this.propertyType = propertyType;
            }

            public static class PropertyTypeBean {
                /**
                 * id : 1
                 * name : Residential
                 * is_selected : 1
                 */

                private String id;
                private String name;
                private String is_selected;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getIs_selected() {
                    return is_selected;
                }

                public void setIs_selected(String is_selected) {
                    this.is_selected = is_selected;
                }
            }
        }

        public static class WorkPhotoBean {
            /**
             * is_added : 1
             * image_url : http://192.168.5.51/bidjobbers/public/jobber/Lawn_service_image
             * image : ["lawn_large.png","lawn_medium.png","lawn_small.png"]
             */

            private String is_added;
            private String image_url;
            private List<String> image;

            public String getIs_added() {
                return is_added;
            }

            public void setIs_added(String is_added) {
                this.is_added = is_added;
            }

            public String getImage_url() {
                return image_url;
            }

            public void setImage_url(String image_url) {
                this.image_url = image_url;
            }

            public List<String> getImage() {
                return image;
            }

            public void setImage(List<String> image) {
                this.image = image;
            }
        }

        public static class AdvanceBookingBean {
            /**
             * attrbuteCount : [{"id":"1","count":"1"},{"id":"2","count":"2"},{"id":"3","count":"3"},{"id":"4","count":"4"},{"id":"5","count":"5"},{"id":"6","count":"6"},{"id":"7","count":"7"},{"id":"8","count":"8"},{"id":"9","count":"9"},{"id":"10","count":"10"},{"id":"11","count":"11"},{"id":"12","count":"12"},{"id":"13","count":"13"},{"id":"14","count":"14"},{"id":"15","count":"15"},{"id":"16","count":"16"},{"id":"17","count":"17"},{"id":"18","count":"18"},{"id":"19","count":"19"},{"id":"20","count":"20"},{"id":"21","count":"21"},{"id":"22","count":"22"},{"id":"23","count":"23"},{"id":"24","count":"24"},{"id":"25","count":"25"},{"id":"26","count":"26"},{"id":"27","count":"27"},{"id":"28","count":"28"},{"id":"29","count":"29"},{"id":"30","count":"30"}]
             * attrunit_for_how_far : [{"id":1,"unit":"Days"},{"id":2,"unit":"Weeks"},{"id":3,"unit":"Months"}]
             * attrunit_for_notice_period : [{"id":1,"unit":"Days"},{"id":4,"unit":"Hours"}]
             * advanceValueId : 30
             * advanceValue : 30
             * advanceNameid : 1
             * advanceName : Days
             * noticeValueId : 30
             * noticeValue : 30
             * noticeNameId : 4
             * noticeName : Hours
             */

            private String advanceValueId;
            private String advanceValue;
            private String advanceNameid;
            private String advanceName;
            private String noticeValueId;
            private String noticeValue;
            private String noticeNameId;
            private String noticeName;
            private List<AttrbuteCountBean> attrbuteCount;
            private List<AttrunitForHowFarBean> attrunit_for_how_far;
            private List<AttrunitForNoticePeriodBean> attrunit_for_notice_period;

            public String getAdvanceValueId() {
                return advanceValueId;
            }

            public void setAdvanceValueId(String advanceValueId) {
                this.advanceValueId = advanceValueId;
            }

            public String getAdvanceValue() {
                return advanceValue;
            }

            public void setAdvanceValue(String advanceValue) {
                this.advanceValue = advanceValue;
            }

            public String getAdvanceNameid() {
                return advanceNameid;
            }

            public void setAdvanceNameid(String advanceNameid) {
                this.advanceNameid = advanceNameid;
            }

            public String getAdvanceName() {
                return advanceName;
            }

            public void setAdvanceName(String advanceName) {
                this.advanceName = advanceName;
            }

            public String getNoticeValueId() {
                return noticeValueId;
            }

            public void setNoticeValueId(String noticeValueId) {
                this.noticeValueId = noticeValueId;
            }

            public String getNoticeValue() {
                return noticeValue;
            }

            public void setNoticeValue(String noticeValue) {
                this.noticeValue = noticeValue;
            }

            public String getNoticeNameId() {
                return noticeNameId;
            }

            public void setNoticeNameId(String noticeNameId) {
                this.noticeNameId = noticeNameId;
            }

            public String getNoticeName() {
                return noticeName;
            }

            public void setNoticeName(String noticeName) {
                this.noticeName = noticeName;
            }

            public List<AttrbuteCountBean> getAttrbuteCount() {
                return attrbuteCount;
            }

            public void setAttrbuteCount(List<AttrbuteCountBean> attrbuteCount) {
                this.attrbuteCount = attrbuteCount;
            }

            public List<AttrunitForHowFarBean> getAttrunit_for_how_far() {
                return attrunit_for_how_far;
            }

            public void setAttrunit_for_how_far(List<AttrunitForHowFarBean> attrunit_for_how_far) {
                this.attrunit_for_how_far = attrunit_for_how_far;
            }

            public List<AttrunitForNoticePeriodBean> getAttrunit_for_notice_period() {
                return attrunit_for_notice_period;
            }

            public void setAttrunit_for_notice_period(List<AttrunitForNoticePeriodBean> attrunit_for_notice_period) {
                this.attrunit_for_notice_period = attrunit_for_notice_period;
            }

            public static class AttrbuteCountBean {
                /**
                 * id : 1
                 * count : 1
                 */

                private String id;
                private String count;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getCount() {
                    return count;
                }

                public void setCount(String count) {
                    this.count = count;
                }
            }

            public static class AttrunitForHowFarBean {
                /**
                 * id : 1
                 * unit : Days
                 */

                private int id;
                private String unit;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getUnit() {
                    return unit;
                }

                public void setUnit(String unit) {
                    this.unit = unit;
                }
            }

            public static class AttrunitForNoticePeriodBean {
                /**
                 * id : 1
                 * unit : Days
                 */

                private int id;
                private String unit;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getUnit() {
                    return unit;
                }

                public void setUnit(String unit) {
                    this.unit = unit;
                }
            }
        }

        public static class BuisnessInfoBean {
            /**
             * company_name : test
             * founding_year : 01 May,1989
             * employee_count_selected_id : 1
             * no_of_employee : 0 to 5
             * company_reg_no : 123456
             * employeeDropdownData : [{"id":1,"no_of_employee":"0 to 5"},{"id":2,"no_of_employee":"5 to 10"},{"id":3,"no_of_employee":"more than 10"}]
             */

            private String company_name;
            private String founding_year;
            private String employee_count_selected_id;
            private String no_of_employee;
            private String company_reg_no;
            private List<EmployeeDropdownDataBean> employeeDropdownData;

            public String getCompany_name() {
                return company_name;
            }

            public void setCompany_name(String company_name) {
                this.company_name = company_name;
            }

            public String getFounding_year() {
                return founding_year;
            }

            public void setFounding_year(String founding_year) {
                this.founding_year = founding_year;
            }

            public String getEmployee_count_selected_id() {
                return employee_count_selected_id;
            }

            public void setEmployee_count_selected_id(String employee_count_selected_id) {
                this.employee_count_selected_id = employee_count_selected_id;
            }

            public String getNo_of_employee() {
                return no_of_employee;
            }

            public void setNo_of_employee(String no_of_employee) {
                this.no_of_employee = no_of_employee;
            }

            public String getCompany_reg_no() {
                return company_reg_no;
            }

            public void setCompany_reg_no(String company_reg_no) {
                this.company_reg_no = company_reg_no;
            }

            public List<EmployeeDropdownDataBean> getEmployeeDropdownData() {
                return employeeDropdownData;
            }

            public void setEmployeeDropdownData(List<EmployeeDropdownDataBean> employeeDropdownData) {
                this.employeeDropdownData = employeeDropdownData;
            }

            public static class EmployeeDropdownDataBean {
                /**
                 * id : 1
                 * no_of_employee : 0 to 5
                 */

                private int id;
                private String no_of_employee;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getNo_of_employee() {
                    return no_of_employee;
                }

                public void setNo_of_employee(String no_of_employee) {
                    this.no_of_employee = no_of_employee;
                }
            }
        }

        public static class SocialLinkBean {
            /**
             * social_media_links : ["www.facebook.com","www.twitter.com"]
             * website_url : https://www.google.com/
             */

            private String website_url;
            private List<String> social_media_links;

            public String getWebsite_url() {
                return website_url;
            }

            public void setWebsite_url(String website_url) {
                this.website_url = website_url;
            }

            public List<String> getSocial_media_links() {
                return social_media_links;
            }

            public void setSocial_media_links(List<String> social_media_links) {
                this.social_media_links = social_media_links;
            }
        }

        public static class BuisnessLocationBean {
            /**
             * business_address : kolkata
             * apartment : EE-118
             * city : kolkata
             * state : west bengal
             * country : India
             * zipcode : 123456
             */

            private String business_address;
            private String apartment;
            private String city;
            private String state;
            private String country;
            private String zipcode;
            private String city_id;
            private String country_id;
            private String lat;
            private String lng;

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLng() {
                return lng;
            }

            public void setLng(String lng) {
                this.lng = lng;
            }

            public String getCity_id() {
                return city_id;
            }

            public void setCity_id(String city_id) {
                this.city_id = city_id;
            }

            public String getCountry_id() {
                return country_id;
            }

            public void setCountry_id(String country_id) {
                this.country_id = country_id;
            }

            public String getBusiness_address() {
                return business_address;
            }

            public void setBusiness_address(String business_address) {
                this.business_address = business_address;
            }

            public String getApartment() {
                return apartment;
            }

            public void setApartment(String apartment) {
                this.apartment = apartment;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getZipcode() {
                return zipcode;
            }

            public void setZipcode(String zipcode) {
                this.zipcode = zipcode;
            }
        }

        public static class BuisnessDescBean {
            /**
             * business_intro_desc : test
             */

            private String business_intro_desc;

            public String getBusiness_intro_desc() {
                return business_intro_desc;
            }

            public void setBusiness_intro_desc(String business_intro_desc) {
                this.business_intro_desc = business_intro_desc;
            }
        }

        public static class BuisnessHoursDetailsBean {
            private List<BuisnessHrDropdownBean> buisnessHrDropdown;
            private List<BusinessHrDataBean> businessHrData;

            public List<BuisnessHrDropdownBean> getBuisnessHrDropdown() {
                return buisnessHrDropdown;
            }

            public void setBuisnessHrDropdown(List<BuisnessHrDropdownBean> buisnessHrDropdown) {
                this.buisnessHrDropdown = buisnessHrDropdown;
            }

            public List<BusinessHrDataBean> getBusinessHrData() {
                return businessHrData;
            }

            public void setBusinessHrData(List<BusinessHrDataBean> businessHrData) {
                this.businessHrData = businessHrData;
            }

            public static class BuisnessHrDropdownBean {
                /**
                 * id : 1
                 * time : 08:00
                 */

                private String id;
                private String time;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getTime() {
                    return time;
                }

                public void setTime(String time) {
                    this.time = time;
                }
            }

            public static class BusinessHrDataBean {
                /**
                 * id : 1
                 * dayName : Sunday
                 * from_time_id :
                 * from_time :
                 * end_time_id :
                 * end_time :
                 * isSelected : 0
                 */

                private String id;
                private String dayName;
                private String from_time_id;
                private String from_time;
                private String end_time_id;
                private String end_time;
                private String isSelected;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getDayName() {
                    return dayName;
                }

                public void setDayName(String dayName) {
                    this.dayName = dayName;
                }

                public String getFrom_time_id() {
                    return from_time_id;
                }

                public void setFrom_time_id(String from_time_id) {
                    this.from_time_id = from_time_id;
                }

                public String getFrom_time() {
                    return from_time;
                }

                public void setFrom_time(String from_time) {
                    this.from_time = from_time;
                }

                public String getEnd_time_id() {
                    return end_time_id;
                }

                public void setEnd_time_id(String end_time_id) {
                    this.end_time_id = end_time_id;
                }

                public String getEnd_time() {
                    return end_time;
                }

                public void setEnd_time(String end_time) {
                    this.end_time = end_time;
                }

                public String getIsSelected() {
                    return isSelected;
                }

                public void setIsSelected(String isSelected) {
                    this.isSelected = isSelected;
                }
            }
        }

        public static class ServiceTypeDetailsBean {
            /**
             * parent_id : 1
             * parent_name : Lawn Service
             * is_selected : 1
             * children : [{"id":"3","name":"Mow a Lawn","description":"Gardening Services","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_mow_a_lawn.png","is_selected":"1"},{"id":"4","name":"Aerate a Lawn","description":"Gardening Services","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_aerate_a_lawn.png","is_selected":"0"},{"id":"5","name":"Yard Waste","description":"Clippings & Removal","image":"http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_yard_waste_clippings_and_removal.png","is_selected":"0"}]
             */

            private String parent_id;
            private String parent_name;
            private String is_selected;
            private List<ChildrenBean> children;

            public String getParent_id() {
                return parent_id;
            }

            public void setParent_id(String parent_id) {
                this.parent_id = parent_id;
            }

            public String getParent_name() {
                return parent_name;
            }

            public void setParent_name(String parent_name) {
                this.parent_name = parent_name;
            }

            public String getIs_selected() {
                return is_selected;
            }

            public void setIs_selected(String is_selected) {
                this.is_selected = is_selected;
            }

            public List<ChildrenBean> getChildren() {
                return children;
            }

            public void setChildren(List<ChildrenBean> children) {
                this.children = children;
            }

            public static class ChildrenBean {
                /**
                 * id : 3
                 * name : Mow a Lawn
                 * description : Gardening Services
                 * image : http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_mow_a_lawn.png
                 * is_selected : 1
                 */

                private String id;
                private String name;
                private String description;
                private String image;
                private String is_selected;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public String getImage() {
                    return image;
                }

                public void setImage(String image) {
                    this.image = image;
                }

                public String getIs_selected() {
                    return is_selected;
                }

                public void setIs_selected(String is_selected) {
                    this.is_selected = is_selected;
                }
            }
        }

        public static class LawnServicesDetailsBean {
            /**
             * id : 1
             * name : Small
             * short_description : Less than 1000 sq ft
             * image : http://192.168.5.51/bidjobbers/public/jobber/service_type_image/lawn_small.png
             * price :
             */

            private String id;
            private String name;
            private String short_description;
            private String image;
            private String price;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getShort_description() {
                return short_description;
            }

            public void setShort_description(String short_description) {
                this.short_description = short_description;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }
        }

        public static class TreeServicesDetailsBean {
            /**
             * id : 1
             * name : 1 Tree
             * price :
             */

            private String id;
            private String name;
            private String price;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }
        }
        public static class TransactionAccountDetails {


            private String transaction_account_id;
            private String bank_name;
            private String bank_account_holder_first_name;
            private String bank_account_holder_last_name;
            private String bank_account_number;
            private String bank_routing_number;
            private String bank_country;
            private String bank_currency;
            private String bank_swift;
            private String bank_iban;
            private String paypal_id;
            private String paypal_currency;


            public String getTransaction_account_id() {
                return transaction_account_id;
            }

            public void setTransaction_account_id(String transaction_account_id) {
                this.transaction_account_id = transaction_account_id;
            }

            public String getBank_name() {
                return bank_name;
            }

            public void setBank_name(String bank_name) {
                this.bank_name = bank_name;
            }

            public String getBank_account_holder_first_name() {
                return bank_account_holder_first_name;
            }

            public void setBank_account_holder_first_name(String bank_account_holder_first_name) {
                this.bank_account_holder_first_name = bank_account_holder_first_name;
            }

            public String getBank_account_holder_last_name() {
                return bank_account_holder_last_name;
            }

            public void setBank_account_holder_last_name(String bank_account_holder_last_name) {
                this.bank_account_holder_last_name = bank_account_holder_last_name;
            }

            public String getBank_account_number() {
                return bank_account_number;
            }

            public void setBank_account_number(String bank_account_number) {
                this.bank_account_number = bank_account_number;
            }

            public String getBank_routing_number() {
                return bank_routing_number;
            }

            public void setBank_routing_number(String bank_routing_number) {
                this.bank_routing_number = bank_routing_number;
            }

            public String getBank_country() {
                return bank_country;
            }

            public void setBank_country(String bank_country) {
                this.bank_country = bank_country;
            }

            public String getBank_currency() {
                return bank_currency;
            }

            public void setBank_currency(String bank_currency) {
                this.bank_currency = bank_currency;
            }

            public String getBank_swift() {
                return bank_swift;
            }

            public void setBank_swift(String bank_swift) {
                this.bank_swift = bank_swift;
            }

            public String getBank_iban() {
                return bank_iban;
            }

            public void setBank_iban(String bank_iban) {
                this.bank_iban = bank_iban;
            }

            public String getPaypal_id() {
                return paypal_id;
            }

            public void setPaypal_id(String paypal_id) {
                this.paypal_id = paypal_id;
            }

            public String getPaypal_currency() {
                return paypal_currency;
            }

            public void setPaypal_currency(String paypal_currency) {
                this.paypal_currency = paypal_currency;
            }


            @Override
            public String toString() {
                return "TransactionAccountDetails{" +
                        "transaction_account_id='" + transaction_account_id + '\'' +
                        ", bank_name='" + bank_name + '\'' +
                        ", bank_account_holder_first_name='" + bank_account_holder_first_name + '\'' +
                        ", bank_account_holder_last_name='" + bank_account_holder_last_name + '\'' +
                        ", bank_account_number='" + bank_account_number + '\'' +
                        ", bank_routing_number='" + bank_routing_number + '\'' +
                        ", bank_country='" + bank_country + '\'' +
                        ", bank_currency='" + bank_currency + '\'' +
                        ", bank_swift='" + bank_swift + '\'' +
                        ", bank_iban='" + bank_iban + '\'' +
                        ", paypal_id='" + paypal_id + '\'' +
                        ", paypal_currency='" + paypal_currency + '\'' +
                        '}';
            }
        }



    }
}
