
package com.bidjobbers.bidjobberspro.data.network.models.Customer;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class ResponseData {

    @SerializedName("taskData")
    private TaskData mTaskData;

    public TaskData getTaskData() {
        return mTaskData;
    }

    public void setTaskData(TaskData taskData) {
        mTaskData = taskData;
    }

}
