package com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList;

public class BlockCustomerListRequest {


    /**
     * locale : en
     */

    private String locale;
    private String time_period;
    private String current_page;


    public BlockCustomerListRequest(String locale, String time_period, String current_page) {
        this.locale = locale;
        this.time_period = time_period;
        this.current_page = current_page;
    }

    public String getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(String current_page) {
        this.current_page = current_page;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getTime_period() {
        return time_period;
    }

    public void setTime_period(String time_period) {
        this.time_period = time_period;
    }
}
