package com.bidjobbers.bidjobberspro.data.network.models.UpdateProfile;

public class UpdateProfileResponse {


    /**
     * responseCode : 1
     * responseText : Data Updated Successfully.
     */

    private int responseCode;
    private String responseText;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }
}
