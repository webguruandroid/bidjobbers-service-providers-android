package com.bidjobbers.bidjobberspro.data.network.models.ContactUs;

public class ContactUsRequest {


    /**
     * name : Test User
     * email : tuser@gmail.com
     * reason : No Reason
     * message : Test Message
     * locale : en
     */

    private String name;
    private String email;
    private String reason;
    private String message;
    private String locale;

    public ContactUsRequest(String name, String email, String reason, String message, String locale) {
        this.name = name;
        this.email = email;
        this.reason = reason;
        this.message = message;
        this.locale = locale;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
