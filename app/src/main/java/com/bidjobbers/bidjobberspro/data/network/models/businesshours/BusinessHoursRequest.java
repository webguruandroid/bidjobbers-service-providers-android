package com.bidjobbers.bidjobberspro.data.network.models.businesshours;

import java.util.List;

//Not Need
public class BusinessHoursRequest {


    /**
     * businessHrData : [{"day_id":"1","start_time_id":"1","start_time":"08:00","end_time_id":"2","end_time":"09:00"},{"day_id":"2","start_time_id":"2","start_time":"09:00","end_time_id":"3","end_time":"10:00"},{"day_id":"7","start_time_id":"1","start_time":"08:00","end_time_id":"2","end_time":"09:00"}]
     * locale : en
     */

    private String locale;
    private List<BusinessHrDataBean> businessHrData;

    public BusinessHoursRequest(String locale, List<BusinessHrDataBean> businessHrData) {
        this.locale = locale;
        this.businessHrData = businessHrData;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public List<BusinessHrDataBean> getBusinessHrData() {
        return businessHrData;
    }

    public void setBusinessHrData(List<BusinessHrDataBean> businessHrData) {
        this.businessHrData = businessHrData;
    }

    public static class BusinessHrDataBean {
        /**
         * day_id : 1
         * start_time_id : 1
         * start_time : 08:00
         * end_time_id : 2
         * end_time : 09:00
         */

        private String day_id;
        private String start_time_id;
        private String start_time;
        private String end_time_id;
        private String end_time;

        public String getDay_id() {
            return day_id;
        }

        public void setDay_id(String day_id) {
            this.day_id = day_id;
        }

        public String getStart_time_id() {
            return start_time_id;
        }

        public void setStart_time_id(String start_time_id) {
            this.start_time_id = start_time_id;
        }

        public String getStart_time() {
            return start_time;
        }

        public void setStart_time(String start_time) {
            this.start_time = start_time;
        }

        public String getEnd_time_id() {
            return end_time_id;
        }

        public void setEnd_time_id(String end_time_id) {
            this.end_time_id = end_time_id;
        }

        public String getEnd_time() {
            return end_time;
        }

        public void setEnd_time(String end_time) {
            this.end_time = end_time;
        }
    }
}
