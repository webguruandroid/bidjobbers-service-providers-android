package com.bidjobbers.bidjobberspro.data.network.models.login;

public class LoginRequest {


    String email="";
    String password="";
    String locale="";
    String device_type="";
    String device_token="";

    public LoginRequest(String email, String password, String locale, String device_type, String device_token) {
        this.email = email;
        this.password = password;
        this.locale = locale;
        this.device_type = device_type;
        this.device_token = device_token;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
