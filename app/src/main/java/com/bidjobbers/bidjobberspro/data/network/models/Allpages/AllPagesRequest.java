package com.bidjobbers.bidjobberspro.data.network.models.Allpages;

public class AllPagesRequest {


    /**
     * locale : en
     * slug : terms-and-condition
     */

    private String locale;
    private String slug;

    public AllPagesRequest(String locale, String slug) {
        this.locale = locale;
        this.slug = slug;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
