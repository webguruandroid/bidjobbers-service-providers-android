package com.bidjobbers.bidjobberspro.data.network.models.Advance;

public class AdvanceRequest {


    /**
     * locale : en
     * advanceValueId : 30
     * advanceValue : 30
     * advanceNameid : 1
     * advanceName : Days
     * noticeValueId : 23
     * noticeValue : 23
     * noticeNameId : 4
     * noticeName : Hours
     */

    private String locale;
    private String advanceValueId;
    private String advanceValue;
    private String advanceNameid;
    private String advanceName;
    private String noticeValueId;
    private String noticeValue;
    private String noticeNameId;
    private String noticeName;


    public AdvanceRequest(String locale, String advanceValueId, String advanceValue, String advanceNameid, String advanceName, String noticeValueId, String noticeValue, String noticeNameId, String noticeName) {
        this.locale = locale;
        this.advanceValueId = advanceValueId;
        this.advanceValue = advanceValue;
        this.advanceNameid = advanceNameid;
        this.advanceName = advanceName;
        this.noticeValueId = noticeValueId;
        this.noticeValue = noticeValue;
        this.noticeNameId = noticeNameId;
        this.noticeName = noticeName;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getAdvanceValueId() {
        return advanceValueId;
    }

    public void setAdvanceValueId(String advanceValueId) {
        this.advanceValueId = advanceValueId;
    }

    public String getAdvanceValue() {
        return advanceValue;
    }

    public void setAdvanceValue(String advanceValue) {
        this.advanceValue = advanceValue;
    }

    public String getAdvanceNameid() {
        return advanceNameid;
    }

    public void setAdvanceNameid(String advanceNameid) {
        this.advanceNameid = advanceNameid;
    }

    public String getAdvanceName() {
        return advanceName;
    }

    public void setAdvanceName(String advanceName) {
        this.advanceName = advanceName;
    }

    public String getNoticeValueId() {
        return noticeValueId;
    }

    public void setNoticeValueId(String noticeValueId) {
        this.noticeValueId = noticeValueId;
    }

    public String getNoticeValue() {
        return noticeValue;
    }

    public void setNoticeValue(String noticeValue) {
        this.noticeValue = noticeValue;
    }

    public String getNoticeNameId() {
        return noticeNameId;
    }

    public void setNoticeNameId(String noticeNameId) {
        this.noticeNameId = noticeNameId;
    }

    public String getNoticeName() {
        return noticeName;
    }

    public void setNoticeName(String noticeName) {
        this.noticeName = noticeName;
    }
}
