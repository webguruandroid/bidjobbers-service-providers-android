package com.bidjobbers.bidjobberspro.data.network.models.PriceForTree;

import java.util.List;

public class PriceForTreeRquest {


    /**
     * treeservice : [{"treeCountId":"1","treeCountPrice":"$597"},{"treeCountId":"2","treeCountPrice":"$689"},{"treeCountId":"3","treeCountPrice":"$778"},{"treeCountId":"4","treeCountPrice":"$894"}]
     * locale : en
     */

    private String locale;
    private List<TreeserviceBean> treeservice;

    public PriceForTreeRquest(String locale, List<TreeserviceBean> treeservice) {
        this.locale = locale;
        this.treeservice = treeservice;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public List<TreeserviceBean> getTreeservice() {
        return treeservice;
    }

    public void setTreeservice(List<TreeserviceBean> treeservice) {
        this.treeservice = treeservice;
    }

    public static class TreeserviceBean {
        /**
         * treeCountId : 1
         * treeCountPrice : $597
         */

        private String treeCountId;
        private String treeCountPrice;

        public String getTreeCountId() {
            return treeCountId;
        }

        public void setTreeCountId(String treeCountId) {
            this.treeCountId = treeCountId;
        }

        public String getTreeCountPrice() {
            return treeCountPrice;
        }

        public void setTreeCountPrice(String treeCountPrice) {
            this.treeCountPrice = treeCountPrice;
        }
    }
}
