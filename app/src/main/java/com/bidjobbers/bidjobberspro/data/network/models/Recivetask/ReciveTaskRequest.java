package com.bidjobbers.bidjobberspro.data.network.models.Recivetask;

public class ReciveTaskRequest {

    private String current_page;

    /**
     * locale : en
     */



    private String locale;
    public ReciveTaskRequest(String locale, String current_page) {
        this.locale = locale;
        this.current_page = current_page;
    }

    public String getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(String current_page) {
        this.current_page = current_page;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
