package com.bidjobbers.bidjobberspro.data.network.models.ChangeaPassword;

public class ChangePasswordRequest {


    /**
     * old_password : nopass
     * new_password : nopass
     * locale : en
     */

    private String old_password;
    private String new_password;
    private String locale;

    public ChangePasswordRequest(String old_password, String new_password, String locale) {
        this.old_password = old_password;
        this.new_password = new_password;
        this.locale = locale;
    }

    public String getOld_password() {
        return old_password;
    }

    public void setOld_password(String old_password) {
        this.old_password = old_password;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
