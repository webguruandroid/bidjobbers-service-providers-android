package com.bidjobbers.bidjobberspro.data.network.models.PropertyType;

import java.util.List;

public class PropertypeRequest {


    /**
     * locale : en
     * property_type : [1,2]
     * is_licence_no : 1
     * licence_number : 45435453
     */

    private String locale;
    private String is_licence_no;
    private String licence_number;
    private List<Integer> property_type;


    public PropertypeRequest(String locale, String is_licence_no, String licence_number, List<Integer> property_type) {
        this.locale = locale;
        this.is_licence_no = is_licence_no;
        this.licence_number = licence_number;
        this.property_type = property_type;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getIs_licence_no() {
        return is_licence_no;
    }

    public void setIs_licence_no(String is_licence_no) {
        this.is_licence_no = is_licence_no;
    }

    public String getLicence_number() {
        return licence_number;
    }

    public void setLicence_number(String licence_number) {
        this.licence_number = licence_number;
    }

    public List<Integer> getProperty_type() {
        return property_type;
    }

    public void setProperty_type(List<Integer> property_type) {
        this.property_type = property_type;
    }
}
