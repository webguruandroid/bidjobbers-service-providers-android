package com.bidjobbers.bidjobberspro.data.network.models.BusinessInfo;

public class BusinessInfoRequest {


    /**
     * locale : en
     * company_name : webguru
     * founding_year : 01 May,1999
     * employee_count_selected_id : 3
     * company_reg_no : 1234678
     */

    private String locale;
    private String company_name;
    private String founding_year;
    private String employee_count_selected_id;
    private String company_reg_no;

    public BusinessInfoRequest(String locale, String company_name, String founding_year, String employee_count_selected_id, String company_reg_no) {
        this.locale = locale;
        this.company_name = company_name;
        this.founding_year = founding_year;
        this.employee_count_selected_id = employee_count_selected_id;
        this.company_reg_no = company_reg_no;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getFounding_year() {
        return founding_year;
    }

    public void setFounding_year(String founding_year) {
        this.founding_year = founding_year;
    }

    public String getEmployee_count_selected_id() {
        return employee_count_selected_id;
    }

    public void setEmployee_count_selected_id(String employee_count_selected_id) {
        this.employee_count_selected_id = employee_count_selected_id;
    }

    public String getCompany_reg_no() {
        return company_reg_no;
    }

    public void setCompany_reg_no(String company_reg_no) {
        this.company_reg_no = company_reg_no;
    }
}
