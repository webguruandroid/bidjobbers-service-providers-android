package com.bidjobbers.bidjobberspro.data.network.models.ReviewList;

public class ReviewRequest {


    /**
     * locale : en
     */

    private String locale;

    public ReviewRequest(String locale) {
        this.locale = locale;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
