
package com.bidjobbers.bidjobberspro.data.network.models.BankDetails;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class BankDetailsRequest {

    @SerializedName("bank_account_holder_first_name")
    private String mBankAccountHolderFirstName;
    @SerializedName("bank_account_holder_last_name")
    private String mBankAccountHolderLastName;

    @SerializedName("bank_account_number")
    private String mBankAccountNumber;
    @SerializedName("bank_country")
    private String mBankCountry;
    @SerializedName("bank_currency")
    private String mBankCurrency;
    @SerializedName("bank_iban")
    private String mBankIban;
    @SerializedName("bank_name")
    private String mBankName;

    @SerializedName("bank_swift")
    private String mBankSwift;
    @SerializedName("locale")
    private String mLocale;
    @SerializedName("paypal_currency")
    private String mPaypalCurrency;
    @SerializedName("paypal_id")
    private String mPaypalId;
    @SerializedName("transaction_acc_id")
    private String mTransactionAccId;



    public BankDetailsRequest(String mBankAccountHolderFirstName,String mBankAccountHolderLastName, String mBankAccountNumber, String mBankCountry, String mBankCurrency, String mBankIban, String mBankName,String mBankSwift, String mLocale, String mPaypalCurrency, String mPaypalId, String mTransactionAccId) {
        this.mBankAccountHolderFirstName = mBankAccountHolderFirstName;
        this.mBankAccountHolderLastName = mBankAccountHolderLastName;
        this.mBankAccountNumber = mBankAccountNumber;
        this.mBankCountry = mBankCountry;
        this.mBankCurrency = mBankCurrency;
        this.mBankIban = mBankIban;
        this.mBankName = mBankName;
        this.mBankSwift = mBankSwift;
        this.mLocale = mLocale;
        this.mPaypalCurrency = mPaypalCurrency;
        this.mPaypalId = mPaypalId;
        this.mTransactionAccId = mTransactionAccId;

    }


    public String getmBankAccountHolderFirstName() {
        return mBankAccountHolderFirstName;
    }

    public void setmBankAccountHolderFirstName(String mBankAccountHolderFirstName) {
        this.mBankAccountHolderFirstName = mBankAccountHolderFirstName;
    }

    public String getmBankAccountHolderLastName() {
        return mBankAccountHolderLastName;
    }

    public void setmBankAccountHolderLastName(String mBankAccountHolderLastName) {
        this.mBankAccountHolderLastName = mBankAccountHolderLastName;
    }

    public String getBankAccountNumber() {
        return mBankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        mBankAccountNumber = bankAccountNumber;
    }

    public String getBankCountry() {
        return mBankCountry;
    }

    public void setBankCountry(String bankCountry) {
        mBankCountry = bankCountry;
    }

    public String getBankCurrency() {
        return mBankCurrency;
    }

    public void setBankCurrency(String bankCurrency) {
        mBankCurrency = bankCurrency;
    }

    public String getBankIban() {
        return mBankIban;
    }

    public void setBankIban(String bankIban) {
        mBankIban = bankIban;
    }

    public String getBankName() {
        return mBankName;
    }

    public void setBankName(String bankName) {
        mBankName = bankName;
    }



    public String getBankSwift() {
        return mBankSwift;
    }

    public void setBankSwift(String bankSwift) {
        mBankSwift = bankSwift;
    }

    public String getLocale() {
        return mLocale;
    }

    public void setLocale(String locale) {
        mLocale = locale;
    }

    public String getPaypalCurrency() {
        return mPaypalCurrency;
    }

    public void setPaypalCurrency(String paypalCurrency) {
        mPaypalCurrency = paypalCurrency;
    }

    public String getPaypalId() {
        return mPaypalId;
    }

    public void setPaypalId(String paypalId) {
        mPaypalId = paypalId;
    }

    public String getTransactionAccId() {
        return mTransactionAccId;
    }

    public void setTransactionAccId(String transactionAccId) {
        mTransactionAccId = transactionAccId;
    }

}
