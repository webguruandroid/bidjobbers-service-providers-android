package com.bidjobbers.bidjobberspro.data.network.models.TimeSlot;

public class TimeSlotRequest {


    /**
     * locale : en
     */

    private String locale;

    public TimeSlotRequest(String locale) {
        this.locale = locale;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
