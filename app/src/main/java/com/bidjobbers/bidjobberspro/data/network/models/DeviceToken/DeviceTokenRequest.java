package com.bidjobbers.bidjobberspro.data.network.models.DeviceToken;

public class DeviceTokenRequest {
    private String locale;
    private String device_type;
    private String device_token;

    public DeviceTokenRequest(String locale, String device_type, String device_token) {
        this.locale = locale;
        this.device_type = device_type;
        this.device_token = device_token;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    @Override
    public String toString() {
        return "DeviceTokenRequest{" +
                "locale='" + locale + '\'' +
                ", device_type='" + device_type + '\'' +
                ", device_token='" + device_token + '\'' +
                '}';
    }
}
