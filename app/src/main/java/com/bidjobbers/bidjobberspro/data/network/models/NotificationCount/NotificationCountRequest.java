package com.bidjobbers.bidjobberspro.data.network.models.NotificationCount;

public class NotificationCountRequest {

    /**
     * locale : en
     */

    private String locale;

    public NotificationCountRequest(String locale) {
        this.locale = locale;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
