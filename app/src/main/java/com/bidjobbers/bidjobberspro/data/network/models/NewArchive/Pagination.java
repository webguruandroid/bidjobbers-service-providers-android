
package com.bidjobbers.bidjobberspro.data.network.models.NewArchive;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class Pagination {

    @SerializedName("current_page")
    private String mCurrentPage;
    @SerializedName("limit")
    private String mLimit;
    @SerializedName("next_page")
    private String mNextPage;
    @SerializedName("offset")
    private String mOffset;

    public String getCurrentPage() {
        return mCurrentPage;
    }

    public void setCurrentPage(String currentPage) {
        mCurrentPage = currentPage;
    }

    public String getLimit() {
        return mLimit;
    }

    public void setLimit(String limit) {
        mLimit = limit;
    }

    public String getNextPage() {
        return mNextPage;
    }

    public void setNextPage(String nextPage) {
        mNextPage = nextPage;
    }

    public String getOffset() {
        return mOffset;
    }

    public void setOffset(String offset) {
        mOffset = offset;
    }

}
