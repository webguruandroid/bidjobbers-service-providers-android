package com.bidjobbers.bidjobberspro.data.network.models.SocialMedia;

import java.util.List;

public class SocialMediaRequest {


    /**
     * locale : en
     * social_media_links : ["facebook.com","twitter.com"]
     * website_url : https://www.google.com/
     */

    private String locale;
    private String website_url;
    private List<String> social_media_links;

    public SocialMediaRequest(String locale, String website_url, List<String> social_media_links) {
        this.locale = locale;
        this.website_url = website_url;
        this.social_media_links = social_media_links;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getWebsite_url() {
        return website_url;
    }

    public void setWebsite_url(String website_url) {
        this.website_url = website_url;
    }

    public List<String> getSocial_media_links() {
        return social_media_links;
    }

    public void setSocial_media_links(List<String> social_media_links) {
        this.social_media_links = social_media_links;
    }
}
