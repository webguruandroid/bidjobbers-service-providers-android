package com.bidjobbers.bidjobberspro.data.network.models.EndTask;

public class EndTaskRequest {


    /**
     * customer_id : 13
     * task_id : 63
     * bid_id : 21
     * locale : en
     */

    private String customer_id;
    private String task_id;
    private String bid_id;
    private String locale;

    public EndTaskRequest(String customer_id, String task_id, String bid_id, String locale) {
        this.customer_id = customer_id;
        this.task_id = task_id;
        this.bid_id = bid_id;
        this.locale = locale;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getBid_id() {
        return bid_id;
    }

    public void setBid_id(String bid_id) {
        this.bid_id = bid_id;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
