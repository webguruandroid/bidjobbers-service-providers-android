package com.bidjobbers.bidjobberspro.data.network.models.register;

public class RegisterResponse {


    /**
     * responseCode : 1
     * responseText : Registration successful. Please check your mail to verify the OTP.
     * responseData : {"otp":"PrgUbn","token_type":"Bearer","expires_in":31622400,"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjRkMGNkYzU2ZDdhOTA5NzEyZTMyMzA3M2YyNjNlZDIxNjdiYzc3YWIyYmU2MWYyNzY5ODJmMzIzMWYxZWRmOTcyNDE2YTllZmJjYTUzY2EwIn0.eyJhdWQiOiI2IiwianRpIjoiNGQwY2RjNTZkN2E5MDk3MTJlMzIzMDczZjI2M2VkMjE2N2JjNzdhYjJiZTYxZjI3Njk4MmYzMjMxZjFlZGY5NzI0MTZhOWVmYmNhNTNjYTAiLCJpYXQiOjE1NzE3MzUzNDgsIm5iZiI6MTU3MTczNTM0OCwiZXhwIjoxNjAzMzU3NzQ4LCJzdWIiOiI2Iiwic2NvcGVzIjpbIioiXX0.R2DEYylYMsS4BuKYrosSg1URQSlUzGZN9zYfOyzDr_F7cNaD7gjVxQWbcbARGRG8VeCizyhk9nxiJL7qwaqsTvGE2k4Z7JTFlf2cBKfZBnnrCLxonB5O82wxwixfVY3rIoF5OWFfl2b2GNtUrot6qPHeg0mXH2GtJoRYf9DzGSQRxvnteAyEWfdE6Eq33PdAbtOYHpjiBiaRFr5Mo0WZnyKWMW0Jw9x2nL9sYLu46lkSucTeHHtIxSCobD46vnW68POdxzrc1fAQ4h6VoF3mQPeUFpqyfw9CgpfpLH6wKm37LYJe5K6RmsLHWayzk5r8xN8eud8hS36Rkt4T703NHob0_6wGtoSd8W26Mw7VF7vrJNY4nYwlCyKhKHZQIVT7T9GyFbCl3IEy4bUR3L01ueRIr8QdN6QqaTubbu3uOxMkunR1ul98oweYGQ8zV3oYc6VTUzwmHWpgRlRx2VnVPG80SCfYiIamjnrGK_OvIPy2uWHtEWWijMRkPmpy6am0oH2bjRS9jSYQj2tNDY9jf1ZCL_MYjXgdXfE9gOt7WwRXEm1p13VMQJ4ugBOmjO_VZfO1zB_CJDPW-4jL1X38mBbYxoGqhvaQfnTRY3xIqT5C0e2fi8rw0S-R8PSO7i7YP2-haXBCwQaL_c6KNdeeCu1zgfeNYK65EGeYJUJVtuA","refresh_token":"def502006762b2bac3616c3fe0869efd7d71da8553bfd1873eac5d041ff198867ef0f2ba86eebdcbb0e89e882f87533543670c6d0d1f3e5d50e15f23156b22baf7fd853facdd82bded04016d86824e6cc6aca847c6df689149b1bd38d15dd2df59957e1855caf1d18ee9c5a0ee058b7471c315cd67d303c3a072a9c236039b918afa848493db8152ae7dd6f13462c5440008c1ee292bada92caf43ea09380860d89a5c6894f8476cc58d982a847354e200ccf37af9cd2533c22e7fdbc6f02ddabb58d496b13bbcea0b9ca345bf6f4b9939093a13a8f6e5ffb36206ee4fd761f7ffce477971b918ef56711f2a09fdc8c850ef9c89d29151abbb235be1659d53ab0f6852c235cd8dffa2007123fcd89898d78988545eb3456bb56eb2455d5b99192e7a5862ef75f24f43ea455258318a8f6219b5df5689ca2927f2dc63e54366144fe093a24c9dccd16bd9333daf78caec17836016e595ce4316f679c746dff26a7ece776f"}
     */

    private int responseCode;
    private String responseText;
    private ResponseDataBean responseData;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        /**
         * otp : PrgUbn
         * token_type : Bearer
         * expires_in : 31622400
         * access_token : eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjRkMGNkYzU2ZDdhOTA5NzEyZTMyMzA3M2YyNjNlZDIxNjdiYzc3YWIyYmU2MWYyNzY5ODJmMzIzMWYxZWRmOTcyNDE2YTllZmJjYTUzY2EwIn0.eyJhdWQiOiI2IiwianRpIjoiNGQwY2RjNTZkN2E5MDk3MTJlMzIzMDczZjI2M2VkMjE2N2JjNzdhYjJiZTYxZjI3Njk4MmYzMjMxZjFlZGY5NzI0MTZhOWVmYmNhNTNjYTAiLCJpYXQiOjE1NzE3MzUzNDgsIm5iZiI6MTU3MTczNTM0OCwiZXhwIjoxNjAzMzU3NzQ4LCJzdWIiOiI2Iiwic2NvcGVzIjpbIioiXX0.R2DEYylYMsS4BuKYrosSg1URQSlUzGZN9zYfOyzDr_F7cNaD7gjVxQWbcbARGRG8VeCizyhk9nxiJL7qwaqsTvGE2k4Z7JTFlf2cBKfZBnnrCLxonB5O82wxwixfVY3rIoF5OWFfl2b2GNtUrot6qPHeg0mXH2GtJoRYf9DzGSQRxvnteAyEWfdE6Eq33PdAbtOYHpjiBiaRFr5Mo0WZnyKWMW0Jw9x2nL9sYLu46lkSucTeHHtIxSCobD46vnW68POdxzrc1fAQ4h6VoF3mQPeUFpqyfw9CgpfpLH6wKm37LYJe5K6RmsLHWayzk5r8xN8eud8hS36Rkt4T703NHob0_6wGtoSd8W26Mw7VF7vrJNY4nYwlCyKhKHZQIVT7T9GyFbCl3IEy4bUR3L01ueRIr8QdN6QqaTubbu3uOxMkunR1ul98oweYGQ8zV3oYc6VTUzwmHWpgRlRx2VnVPG80SCfYiIamjnrGK_OvIPy2uWHtEWWijMRkPmpy6am0oH2bjRS9jSYQj2tNDY9jf1ZCL_MYjXgdXfE9gOt7WwRXEm1p13VMQJ4ugBOmjO_VZfO1zB_CJDPW-4jL1X38mBbYxoGqhvaQfnTRY3xIqT5C0e2fi8rw0S-R8PSO7i7YP2-haXBCwQaL_c6KNdeeCu1zgfeNYK65EGeYJUJVtuA
         * refresh_token : def502006762b2bac3616c3fe0869efd7d71da8553bfd1873eac5d041ff198867ef0f2ba86eebdcbb0e89e882f87533543670c6d0d1f3e5d50e15f23156b22baf7fd853facdd82bded04016d86824e6cc6aca847c6df689149b1bd38d15dd2df59957e1855caf1d18ee9c5a0ee058b7471c315cd67d303c3a072a9c236039b918afa848493db8152ae7dd6f13462c5440008c1ee292bada92caf43ea09380860d89a5c6894f8476cc58d982a847354e200ccf37af9cd2533c22e7fdbc6f02ddabb58d496b13bbcea0b9ca345bf6f4b9939093a13a8f6e5ffb36206ee4fd761f7ffce477971b918ef56711f2a09fdc8c850ef9c89d29151abbb235be1659d53ab0f6852c235cd8dffa2007123fcd89898d78988545eb3456bb56eb2455d5b99192e7a5862ef75f24f43ea455258318a8f6219b5df5689ca2927f2dc63e54366144fe093a24c9dccd16bd9333daf78caec17836016e595ce4316f679c746dff26a7ece776f
         */

        private String otp;
        private String token_type;
        private int expires_in;
        private String access_token;
        private String refresh_token;
        private String isEmailVerified;
        private String hasBusinessDetails;

        public String getIsEmailVerified() {
            return isEmailVerified;
        }

        public void setIsEmailVerified(String isEmailVerified) {
            this.isEmailVerified = isEmailVerified;
        }

        public String getHasBusinessDetails() {
            return hasBusinessDetails;
        }

        public void setHasBusinessDetails(String hasBusinessDetails) {
            this.hasBusinessDetails = hasBusinessDetails;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getToken_type() {
            return token_type;
        }

        public void setToken_type(String token_type) {
            this.token_type = token_type;
        }

        public int getExpires_in() {
            return expires_in;
        }

        public void setExpires_in(int expires_in) {
            this.expires_in = expires_in;
        }

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }

        public String getRefresh_token() {
            return refresh_token;
        }

        public void setRefresh_token(String refresh_token) {
            this.refresh_token = refresh_token;
        }
    }
}
