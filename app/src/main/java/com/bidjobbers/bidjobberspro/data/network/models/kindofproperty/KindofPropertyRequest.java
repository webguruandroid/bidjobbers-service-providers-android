package com.bidjobbers.bidjobberspro.data.network.models.kindofproperty;

import java.util.List;

public class KindofPropertyRequest {


    /**
     * locale : en
     * service_type : [1,3]
     */

    private String locale;
    private List<Integer> service_type;


    public KindofPropertyRequest(String locale, List<Integer> service_type) {
        this.locale = locale;
        this.service_type = service_type;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public List<Integer> getService_type() {
        return service_type;
    }

    public void setService_type(List<Integer> service_type) {
        this.service_type = service_type;
    }
}
