package com.bidjobbers.bidjobberspro.data.network.models.Notification;

public class NotificationRequest {


    /**
     * locale : en
     */

    private String locale;

    public NotificationRequest(String locale) {
        this.locale = locale;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
