package com.bidjobbers.bidjobberspro.data.network.models.NotificationCount;

public class NotificationCountResponse {


    /**
     * responseCode : 1
     * responseText : Data found Successfully.
     * responseData : {"notificationCount":"0"}
     */

    private int responseCode;
    private String responseText;
    private ResponseDataBean responseData;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        /**
         * notificationCount : 0
         */

        private String notificationCount;
        private String hasTransAccDetails;

        public String getHasTransAccDetails() {
            return hasTransAccDetails;
        }

        public void setHasTransAccDetails(String hasTransAccDetails) {
            this.hasTransAccDetails = hasTransAccDetails;
        }

        public String getNotificationCount() {
            return notificationCount;
        }

        public void setNotificationCount(String notificationCount) {
            this.notificationCount = notificationCount;
        }
    }
}
