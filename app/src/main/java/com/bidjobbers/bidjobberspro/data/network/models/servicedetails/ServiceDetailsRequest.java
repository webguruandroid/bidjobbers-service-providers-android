package com.bidjobbers.bidjobberspro.data.network.models.servicedetails;

public class ServiceDetailsRequest {

    /**
     * locale : en
     */

    private String locale;

    public ServiceDetailsRequest(String locale) {
        this.locale = locale;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
