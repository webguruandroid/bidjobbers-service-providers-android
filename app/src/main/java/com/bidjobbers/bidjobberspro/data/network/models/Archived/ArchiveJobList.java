
package com.bidjobbers.bidjobberspro.data.network.models.Archived;

import java.util.List;

import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class ArchiveJobList {

    @SerializedName("bid_id")
    private String mBidId;
    @SerializedName("customer_email")
    private String mCustomerEmail;
    @SerializedName("customer_id")
    private String mCustomerId;
    @SerializedName("customer_name")
    private String mCustomerName;
    @SerializedName("customer_phone")
    private String mCustomerPhone;
    @SerializedName("customer_profile_image")
    private String mCustomerProfileImage;
    @SerializedName("customer_work_image")
    private List<String> mCustomerWorkImage;
    @SerializedName("date")
    private String mDate;
    @SerializedName("day")
    private String mDay;
    @SerializedName("full_address")
    private String mFullAddress;
    @SerializedName("grass_size")
    private String mGrassSize;
    @SerializedName("how_many_tree_remove")
    private String mHowManyTreeRemove;
    @SerializedName("how_many_tree_trim")
    private String mHowManyTreeTrim;
    @SerializedName("is_block")
    private Boolean mIsBlock;
    @SerializedName("lat")
    private String mLat;
    @SerializedName("lawn_size")
    private String mLawnSize;
    @SerializedName("lng")
    private String mLng;
    @SerializedName("on_site_present")
    private Boolean mOnSitePresent;
    @SerializedName("other_info")
    private String mOtherInfo;
    @SerializedName("price")
    private String mPrice;
    @SerializedName("service_at")
    private String mServiceAt;
    @SerializedName("service_request_id")
    private String mServiceRequestId;
    @SerializedName("service_required")
    private String mServiceRequired;
    @SerializedName("service_timing")
    private String mServiceTiming;
    @SerializedName("service_type")
    private String mServiceType;
    @SerializedName("servide_needed")
    private String mServideNeeded;
    @SerializedName("slot")
    private String mSlot;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("status_id")
    private int mStatusId;
    @SerializedName("task_end_notes")
    private String mTaskEndNotes;
    @SerializedName("task_id")
    private String mTaskId;
    @SerializedName("task_image_url")
    private String mTaskImageUrl;
    @SerializedName("task_images")
    private List<String> mTaskImages;

    public String getBidId() {
        return mBidId;
    }

    public void setBidId(String bidId) {
        mBidId = bidId;
    }

    public String getCustomerEmail() {
        return mCustomerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        mCustomerEmail = customerEmail;
    }

    public String getCustomerId() {
        return mCustomerId;
    }

    public void setCustomerId(String customerId) {
        mCustomerId = customerId;
    }

    public String getCustomerName() {
        return mCustomerName;
    }

    public void setCustomerName(String customerName) {
        mCustomerName = customerName;
    }

    public String getCustomerPhone() {
        return mCustomerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        mCustomerPhone = customerPhone;
    }

    public String getCustomerProfileImage() {
        return mCustomerProfileImage;
    }

    public void setCustomerProfileImage(String customerProfileImage) {
        mCustomerProfileImage = customerProfileImage;
    }

    public List<String> getCustomerWorkImage() {
        return mCustomerWorkImage;
    }

    public void setCustomerWorkImage(List<String> customerWorkImage) {
        mCustomerWorkImage = customerWorkImage;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDay() {
        return mDay;
    }

    public void setDay(String day) {
        mDay = day;
    }

    public String getFullAddress() {
        return mFullAddress;
    }

    public void setFullAddress(String fullAddress) {
        mFullAddress = fullAddress;
    }

    public String getGrassSize() {
        return mGrassSize;
    }

    public void setGrassSize(String grassSize) {
        mGrassSize = grassSize;
    }

    public String getHowManyTreeRemove() {
        return mHowManyTreeRemove;
    }

    public void setHowManyTreeRemove(String howManyTreeRemove) {
        mHowManyTreeRemove = howManyTreeRemove;
    }

    public String getHowManyTreeTrim() {
        return mHowManyTreeTrim;
    }

    public void setHowManyTreeTrim(String howManyTreeTrim) {
        mHowManyTreeTrim = howManyTreeTrim;
    }

    public Boolean getIsBlock() {
        return mIsBlock;
    }

    public void setIsBlock(Boolean isBlock) {
        mIsBlock = isBlock;
    }

    public String getLat() {
        return mLat;
    }

    public void setLat(String lat) {
        mLat = lat;
    }

    public String getLawnSize() {
        return mLawnSize;
    }

    public void setLawnSize(String lawnSize) {
        mLawnSize = lawnSize;
    }

    public String getLng() {
        return mLng;
    }

    public void setLng(String lng) {
        mLng = lng;
    }

    public Boolean getOnSitePresent() {
        return mOnSitePresent;
    }

    public void setOnSitePresent(Boolean onSitePresent) {
        mOnSitePresent = onSitePresent;
    }

    public String getOtherInfo() {
        return mOtherInfo;
    }

    public void setOtherInfo(String otherInfo) {
        mOtherInfo = otherInfo;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String price) {
        mPrice = price;
    }

    public String getServiceAt() {
        return mServiceAt;
    }

    public void setServiceAt(String serviceAt) {
        mServiceAt = serviceAt;
    }

    public String getServiceRequestId() {
        return mServiceRequestId;
    }

    public void setServiceRequestId(String serviceRequestId) {
        mServiceRequestId = serviceRequestId;
    }

    public String getServiceRequired() {
        return mServiceRequired;
    }

    public void setServiceRequired(String serviceRequired) {
        mServiceRequired = serviceRequired;
    }

    public String getServiceTiming() {
        return mServiceTiming;
    }

    public void setServiceTiming(String serviceTiming) {
        mServiceTiming = serviceTiming;
    }

    public String getServiceType() {
        return mServiceType;
    }

    public void setServiceType(String serviceType) {
        mServiceType = serviceType;
    }

    public String getServideNeeded() {
        return mServideNeeded;
    }

    public void setServideNeeded(String servideNeeded) {
        mServideNeeded = servideNeeded;
    }

    public String getSlot() {
        return mSlot;
    }

    public void setSlot(String slot) {
        mSlot = slot;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public int getStatusId() {
        return mStatusId;
    }

    public void setStatusId(int statusId) {
        mStatusId = statusId;
    }

    public String getTaskEndNotes() {
        return mTaskEndNotes;
    }

    public void setTaskEndNotes(String taskEndNotes) {
        mTaskEndNotes = taskEndNotes;
    }

    public String getTaskId() {
        return mTaskId;
    }

    public void setTaskId(String taskId) {
        mTaskId = taskId;
    }

    public String getTaskImageUrl() {
        return mTaskImageUrl;
    }

    public void setTaskImageUrl(String taskImageUrl) {
        mTaskImageUrl = taskImageUrl;
    }

    public List<String> getTaskImages() {
        return mTaskImages;
    }

    public void setTaskImages(List<String> taskImages) {
        mTaskImages = taskImages;
    }

}
