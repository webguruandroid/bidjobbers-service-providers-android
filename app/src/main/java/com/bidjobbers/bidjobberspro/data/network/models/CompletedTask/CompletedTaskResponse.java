package com.bidjobbers.bidjobberspro.data.network.models.CompletedTask;

import com.bidjobbers.bidjobberspro.data.network.models.Recivetask.ReciveTaskResponse;

import java.util.List;

public class CompletedTaskResponse {


    /**
     * responseCode : 1
     * responseText : Data found Successfully.
     * responseData : {"taskData":[{"task_id":"70","bid_id":"30","customer_id":"22","full_address":"Nicco Park","lat":"","lng":"","customer_name":"New Customer Jobber Pagla Customer","customer_phone":"0797876481","customer_email":"pagal@pagal.com","customer_profile_image":"http://192.168.5.51/bidjobbers/public/customer/profile_image/388160815.png","service_timing":"Saturday/Sunday","status_id":4,"status":"Completed Jobs","service_at":"Business","service_type":"Lawn Service","service_required":"Every 2 weeks","on_site_present":false,"customer_work_image":[],"servide_needed":"Mow a Lawn","grass_size":"Tall (Upto 12 inches)","lawn_size":"Large","how_many_tree_remove":"","how_many_tree_trim":"","other_info":"","price":"$40","date":"2020-02-05","slot":"11:00 - 12:00","day":"Wednesday"},{"task_id":"69","bid_id":"29","customer_id":"22","full_address":"Allen Street","lat":"","lng":"","customer_name":"New Customer Jobber Pagla Customer","customer_phone":"0797876481","customer_email":"pagal@pagal.com","customer_profile_image":"http://192.168.5.51/bidjobbers/public/customer/profile_image/388160815.png","service_timing":"Saturday/Sunday","status_id":4,"status":"Completed Jobs","service_at":"Business","service_type":"Lawn Service","service_required":"Once a week","on_site_present":true,"customer_work_image":[],"servide_needed":"Mow a Lawn","grass_size":"Tall (Upto 12 inches)","lawn_size":"Medium","how_many_tree_remove":"","how_many_tree_trim":"","other_info":"","price":"$30","date":"2020-02-04","slot":"11:00 - 12:00","day":"Tuesday"},{"task_id":"63","bid_id":"21","customer_id":"13","full_address":"Garia","lat":"","lng":"","customer_name":"S Paul","customer_phone":"9804218910","customer_email":"iamsp123.0@gmail.com","customer_profile_image":"http://192.168.5.51/bidjobbers/public/customer/profile_image/1576141341.png","service_timing":"Weekday Mornings","status_id":4,"status":"Completed Jobs","service_at":"Home","service_type":"Lawn Service","service_required":"Just once","on_site_present":false,"customer_work_image":[],"servide_needed":"Aerate a Lawn","grass_size":"Standard (Upto 6 inches)","lawn_size":"Small","how_many_tree_remove":"","how_many_tree_trim":"","other_info":"15012020\n0245","price":"$20","date":"2020-01-26","slot":"08:00 - 09:00","day":"Sunday"},{"task_id":"58","bid_id":"17","customer_id":"13","full_address":"Garia","lat":"","lng":"","customer_name":"S Paul","customer_phone":"9804218910","customer_email":"iamsp123.0@gmail.com","customer_profile_image":"http://192.168.5.51/bidjobbers/public/customer/profile_image/1576141341.png","service_timing":"Weekday Mornings","status_id":4,"status":"Completed Jobs","service_at":"Home","service_type":"Lawn Service","service_required":"Just once","on_site_present":false,"customer_work_image":[],"servide_needed":"Aerate a Lawn","grass_size":"Standard (Upto 6 inches)","lawn_size":"Small","how_many_tree_remove":"","how_many_tree_trim":"","other_info":"0625","price":"$20","date":"2020-01-15","slot":"09:00 - 10:00","day":"Wednesday"},{"task_id":"57","bid_id":"18","customer_id":"13","full_address":"Garia","lat":"","lng":"","customer_name":"S Paul","customer_phone":"9804218910","customer_email":"iamsp123.0@gmail.com","customer_profile_image":"http://192.168.5.51/bidjobbers/public/customer/profile_image/1576141341.png","service_timing":"Weekday Evenings","status_id":4,"status":"Completed Jobs","service_at":"Home","service_type":"Lawn Service","service_required":"Every two weeks","on_site_present":false,"customer_work_image":[],"servide_needed":"Aerate a Lawn","grass_size":"Standard (Upto 6 inches)","lawn_size":"Small","how_many_tree_remove":"","how_many_tree_trim":"","other_info":"aeration","price":"$20","date":"2020-02-04","slot":"10:00 - 11:00","day":"Tuesday"},{"task_id":"56","bid_id":"19","customer_id":"13","full_address":"Garia","lat":"","lng":"","customer_name":"S Paul","customer_phone":"9804218910","customer_email":"iamsp123.0@gmail.com","customer_profile_image":"http://192.168.5.51/bidjobbers/public/customer/profile_image/1576141341.png","service_timing":"Weekday Mornings","status_id":4,"status":"Completed Jobs","service_at":"Residence","service_type":"Lawn Service","service_required":"Once a month","on_site_present":true,"customer_work_image":["http://192.168.5.51/bidjobbers/public/customer/work_image/1579004574_FirstPic.png"],"servide_needed":"Aerate a Lawn","grass_size":"Tall (Upto 12 inches)","lawn_size":"Small","how_many_tree_remove":"","how_many_tree_trim":"","other_info":"aerate","price":"$20","date":"2020-01-19","slot":"09:00 - 10:00","day":"Sunday"},{"task_id":"54","bid_id":"20","customer_id":"13","full_address":"Garia","lat":"","lng":"","customer_name":"S Paul","customer_phone":"9804218910","customer_email":"iamsp123.0@gmail.com","customer_profile_image":"http://192.168.5.51/bidjobbers/public/customer/profile_image/1576141341.png","service_timing":"Weekday Mornings","status_id":4,"status":"Completed Jobs","service_at":"Home","service_type":"Lawn Service","service_required":"Just Once","on_site_present":true,"customer_work_image":[],"servide_needed":"Mow a Lawn","grass_size":"Standard (Upto 6 inches)","lawn_size":"Small","how_many_tree_remove":"","how_many_tree_trim":"","other_info":"","price":"$20","date":"2020-01-20","slot":"08:00 - 09:00","day":"Monday"},{"task_id":"31","bid_id":"24","customer_id":"1","full_address":"kolkata","lat":"","lng":"","customer_name":"test data","customer_phone":"9090909090","customer_email":"payal1@gmail.com","customer_profile_image":"http://192.168.5.51/bidjobbers/public/customer/profile_image/1575870380.png","service_timing":"Weekday mornings","status_id":4,"status":"Completed Jobs","service_at":"home","service_type":"Tree Cutting","service_required":"Just once","on_site_present":true,"customer_work_image":["http://192.168.5.51/bidjobbers/public/customer/work_image/1577170644_bike.png"],"servide_needed":"Tree Stumps","grass_size":"Standard(up to 6 in)","lawn_size":"Small","how_many_tree_remove":"","how_many_tree_trim":"","other_info":"new data","price":"$560","date":"2020-02-06","slot":"11:00 - 12:00","day":"Thursday"},{"task_id":"29","bid_id":"23","customer_id":"1","full_address":"kolkata","lat":"","lng":"","customer_name":"test data","customer_phone":"9090909090","customer_email":"payal1@gmail.com","customer_profile_image":"http://192.168.5.51/bidjobbers/public/customer/profile_image/1575870380.png","service_timing":"Weekday mornings","status_id":4,"status":"Completed Jobs","service_at":"home","service_type":"Tree Cutting","service_required":"Just once","on_site_present":true,"customer_work_image":[],"servide_needed":"Tree Stumps","grass_size":"Standard(up to 6 in)","lawn_size":"Small","how_many_tree_remove":"","how_many_tree_trim":"","other_info":"new data","price":"$581","date":"2020-02-03","slot":"08:00 - 09:00","day":"Monday"}]}
     */

    private int responseCode;
    private String responseText;
    private ResponseDataBean responseData;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {


        private pagination pagination;

        public pagination getPagination() {
            return pagination;
        }

        public void setPagination(pagination pagination) {
            this.pagination = pagination;
        }
        private List<TaskDataBean> taskData;

        public List<TaskDataBean> getTaskData() {
            return taskData;
        }

        public void setTaskData(List<TaskDataBean> taskData) {
            this.taskData = taskData;
        }


        public static class  pagination
        {
            private String current_page;
            private String next_page;

            public String getCurrent_page() {
                return current_page;
            }

            public void setCurrent_page(String current_page) {
                this.current_page = current_page;
            }

            public String getNext_page() {
                return next_page;
            }

            public void setNext_page(String next_page) {
                this.next_page = next_page;
            }
        }

        public static class TaskDataBean {
            /**
             * task_id : 70
             * bid_id : 30
             * customer_id : 22
             * full_address : Nicco Park
             * lat :
             * lng :
             * customer_name : New Customer Jobber Pagla Customer
             * customer_phone : 0797876481
             * customer_email : pagal@pagal.com
             * customer_profile_image : http://192.168.5.51/bidjobbers/public/customer/profile_image/388160815.png
             * service_timing : Saturday/Sunday
             * status_id : 4
             * status : Completed Jobs
             * service_at : Business
             * service_type : Lawn Service
             * service_required : Every 2 weeks
             * on_site_present : false
             * customer_work_image : []
             * servide_needed : Mow a Lawn
             * grass_size : Tall (Upto 12 inches)
             * lawn_size : Large
             * how_many_tree_remove :
             * how_many_tree_trim :
             * other_info :
             * price : $40
             * date : 2020-02-05
             * slot : 11:00 - 12:00
             * day : Wednesday
             */

            private String task_id;
            private String bid_id;
            private String customer_id;
            private String full_address;
            private String lat;
            private String lng;
            private String customer_name;
            private String customer_phone;
            private String customer_email;
            private String customer_profile_image;
            private String service_timing;
            private int status_id;
            private String status;
            private String service_at;
            private String service_type;
            private String service_required;
            private boolean on_site_present;
            private String servide_needed;
            private String grass_size;
            private String lawn_size;
            private String how_many_tree_remove;
            private String how_many_tree_trim;
            private String other_info;
            private String price;
            private String date;
            private String slot;
            private String day;
            private List<String> customer_work_image;
            private boolean is_block;
            private List<String> task_images;
            private String task_end_notes;
            private String task_image_url;
            private String service_request_id;
            private String service_type_id;

            public String getService_type_id() {
                return service_type_id;
            }

            public void setService_type_id(String service_type_id) {
                this.service_type_id = service_type_id;
            }

            public String getService_request_id() {
                return service_request_id;
            }

            public void setService_request_id(String service_request_id) {
                this.service_request_id = service_request_id;
            }


            public List<String> getTask_images() {
                return task_images;
            }

            public void setTask_images(List<String> task_images) {
                this.task_images = task_images;
            }

            public String getTask_end_notes() {
                return task_end_notes;
            }

            public void setTask_end_notes(String task_end_notes) {
                this.task_end_notes = task_end_notes;
            }

            public String getTask_image_url() {
                return task_image_url;
            }

            public void setTask_image_url(String task_image_url) {
                this.task_image_url = task_image_url;
            }

            public boolean getIs_block() {
                return is_block;
            }

            public void setIs_block(boolean is_block) {
                this.is_block = is_block;
            }

            public String getTask_id() {
                return task_id;
            }

            public void setTask_id(String task_id) {
                this.task_id = task_id;
            }

            public String getBid_id() {
                return bid_id;
            }

            public void setBid_id(String bid_id) {
                this.bid_id = bid_id;
            }

            public String getCustomer_id() {
                return customer_id;
            }

            public void setCustomer_id(String customer_id) {
                this.customer_id = customer_id;
            }

            public String getFull_address() {
                return full_address;
            }

            public void setFull_address(String full_address) {
                this.full_address = full_address;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }

            public String getLng() {
                return lng;
            }

            public void setLng(String lng) {
                this.lng = lng;
            }

            public String getCustomer_name() {
                return customer_name;
            }

            public void setCustomer_name(String customer_name) {
                this.customer_name = customer_name;
            }

            public String getCustomer_phone() {
                return customer_phone;
            }

            public void setCustomer_phone(String customer_phone) {
                this.customer_phone = customer_phone;
            }

            public String getCustomer_email() {
                return customer_email;
            }

            public void setCustomer_email(String customer_email) {
                this.customer_email = customer_email;
            }

            public String getCustomer_profile_image() {
                return customer_profile_image;
            }

            public void setCustomer_profile_image(String customer_profile_image) {
                this.customer_profile_image = customer_profile_image;
            }

            public String getService_timing() {
                return service_timing;
            }

            public void setService_timing(String service_timing) {
                this.service_timing = service_timing;
            }

            public int getStatus_id() {
                return status_id;
            }

            public void setStatus_id(int status_id) {
                this.status_id = status_id;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getService_at() {
                return service_at;
            }

            public void setService_at(String service_at) {
                this.service_at = service_at;
            }

            public String getService_type() {
                return service_type;
            }

            public void setService_type(String service_type) {
                this.service_type = service_type;
            }

            public String getService_required() {
                return service_required;
            }

            public void setService_required(String service_required) {
                this.service_required = service_required;
            }

            public boolean isOn_site_present() {
                return on_site_present;
            }

            public void setOn_site_present(boolean on_site_present) {
                this.on_site_present = on_site_present;
            }

            public String getServide_needed() {
                return servide_needed;
            }

            public void setServide_needed(String servide_needed) {
                this.servide_needed = servide_needed;
            }

            public String getGrass_size() {
                return grass_size;
            }

            public void setGrass_size(String grass_size) {
                this.grass_size = grass_size;
            }

            public String getLawn_size() {
                return lawn_size;
            }

            public void setLawn_size(String lawn_size) {
                this.lawn_size = lawn_size;
            }

            public String getHow_many_tree_remove() {
                return how_many_tree_remove;
            }

            public void setHow_many_tree_remove(String how_many_tree_remove) {
                this.how_many_tree_remove = how_many_tree_remove;
            }

            public String getHow_many_tree_trim() {
                return how_many_tree_trim;
            }

            public void setHow_many_tree_trim(String how_many_tree_trim) {
                this.how_many_tree_trim = how_many_tree_trim;
            }

            public String getOther_info() {
                return other_info;
            }

            public void setOther_info(String other_info) {
                this.other_info = other_info;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public String getSlot() {
                return slot;
            }

            public void setSlot(String slot) {
                this.slot = slot;
            }

            public String getDay() {
                return day;
            }

            public void setDay(String day) {
                this.day = day;
            }

            public List<String> getCustomer_work_image() {
                return customer_work_image;
            }

            public void setCustomer_work_image(List<String> customer_work_image) {
                this.customer_work_image = customer_work_image;
            }
        }
    }
}
