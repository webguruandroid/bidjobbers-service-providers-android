package com.bidjobbers.bidjobberspro.data.network.models.resentotp;

public class ResendOtpRequest {

    String email="";
    String locale="";
    String otp_for="";

    public ResendOtpRequest(String email, String locale,String otp_for) {
        this.email = email;
        this.otp_for = otp_for;
        this.locale = locale;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }


    public String getOtp_for() {
        return otp_for;
    }

    public void setOtp_for(String otp_for) {
        this.otp_for = otp_for;
    }
}
