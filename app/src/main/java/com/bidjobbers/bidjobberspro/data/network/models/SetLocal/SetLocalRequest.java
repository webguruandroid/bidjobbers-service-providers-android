package com.bidjobbers.bidjobberspro.data.network.models.SetLocal;

public class SetLocalRequest {

    /**
     * locale : en
     */

    private String locale;

    public SetLocalRequest(String locale) {
        this.locale = locale;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
