package com.bidjobbers.bidjobberspro.data.network.models.verifyotp;

public class VerifyOtpRequest {

  String otp ="";
  String email="";
  String locale="";
  String otp_for="";

    public VerifyOtpRequest(String otp, String email, String locale, String otp_for) {
        this.otp = otp;
        this.email = email;
        this.locale = locale;
        this.otp_for = otp_for;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getOtp_for() {
        return otp_for;
    }

    public void setOtp_for(String otp_for) {
        this.otp_for = otp_for;
    }
}
