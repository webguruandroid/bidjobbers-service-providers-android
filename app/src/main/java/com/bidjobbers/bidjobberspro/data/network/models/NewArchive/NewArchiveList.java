
package com.bidjobbers.bidjobberspro.data.network.models.NewArchive;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class NewArchiveList {

    @SerializedName("responseCode")
    private Long mResponseCode;
    @SerializedName("responseData")
    private ResponseData mResponseData;
    @SerializedName("responseText")
    private String mResponseText;

    public Long getResponseCode() {
        return mResponseCode;
    }

    public void setResponseCode(Long responseCode) {
        mResponseCode = responseCode;
    }

    public ResponseData getResponseData() {
        return mResponseData;
    }

    public void setResponseData(ResponseData responseData) {
        mResponseData = responseData;
    }

    public String getResponseText() {
        return mResponseText;
    }

    public void setResponseText(String responseText) {
        mResponseText = responseText;
    }

}
