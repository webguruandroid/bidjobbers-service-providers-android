
package com.bidjobbers.bidjobberspro.data.network.models.Archived;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class ResponseData {

    @SerializedName("jobDetails")
    private JobDetails mJobDetails;

    public JobDetails getJobDetails() {
        return mJobDetails;
    }

    public void setJobDetails(JobDetails jobDetails) {
        mJobDetails = jobDetails;
    }

}
