package com.bidjobbers.bidjobberspro.data.network.models.priceforlawnservice;

import java.util.List;

public class PriceForLawnServiceResponse {


    /**
     * responseCode : 1
     * responseText : Price for lawn Service listed sucessfully.
     * responseData : [{"text":"Small","desc":"Less than 1,000 sq.ft","id":"0","price":""},{"text":"Medium","desc":"1,000 to 3,000 sq.ft","id":"1","price":""},{"text":"Large","desc":"3,000 to 10,000 sq.ft","id":"2","price":""},{"text":"VeryLarge","desc":"More than 10,000 sq.ft","id":"3","price":""},{"text":"Unsure","desc":"No idea!","id":"4","price":""}]
     */

    private int responseCode;
    private String responseText;
    private List<ResponseDataBean> responseData;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public List<ResponseDataBean> getResponseData() {
        return responseData;
    }

    public void setResponseData(List<ResponseDataBean> responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        /**
         * text : Small
         * desc : Less than 1,000 sq.ft
         * id : 0
         * price :
         */

        private String text;
        private String desc;
        private String id;
        private String price;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }
    }
}
