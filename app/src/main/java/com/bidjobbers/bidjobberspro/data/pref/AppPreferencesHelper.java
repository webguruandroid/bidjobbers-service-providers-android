package com.bidjobbers.bidjobberspro.data.pref;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.bidjobbers.bidjobberspro.di.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_CURRENT_USER_ID = "PREF_KEY_CURRENT_USER_ID";
    private static final String PREF_KEY_CURRENT_FIRST_NAME = "PREF_KEY_CURRENT_FIRST_NAME";
    private static final String PREF_KEY_CURRENT_LAST_NAME = "PREF_KEY_CURRENT_LAST_NAME";
    private static final String PREF_KEY_CURRENT_USER_MOB = "PREF_KEY_CURRENT_MOB";
    private static final String PREF_KEY_CURRENT_USER_EMAIL = "PREF_KEY_CURRENT_USER_EMAIL";
    private static final String PREF_KEY_CURRENT_USER_GENDER= "PREF_KEY_CURRENT_USER_GENDER";
    private static final String PREF_KEY_CURRENT_USER_REGTYPE= "PREF_KEY_CURRENT_USER_RGTYPE";
    private static final String PREF_KEY_CURRENT_USER_PROFILE_PIC_URL= "PREF_KEY_CURRENT_USER_PROFILE_PIC_URL";
    public static final String KEY_SP_LAST_INTERACTION_TIME = "KEY_SP_LAST_INTERACTION_TIME";
    private static final String LOCAL_KEY = "LOCAL_KEY";
    private static final String MYOBJECT = "MyObject";
    private static final String PREF_KEY_PROJECT_ACCESS_TOKEN = "PREF_KEY_PROJECT_ACCESS_TOKEN";
    private static final String PREF_QUESTION_FILLUP = "PREF_KEY_PROJECT_ACCESS_Fillup";
    private static final String PREF_IS_EMAIL = "PREF_IS_EMAIL";




    private final SharedPreferences mPrefs;

    @Inject
    public AppPreferencesHelper(@ApplicationContext Context context) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public String getCurrentUserId() {
        String userId = mPrefs.getString(PREF_KEY_CURRENT_USER_ID, "");
        return userId;
    }

    @Override
    public void setCurrentUserId(String userId) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_ID, userId).apply();
    }

    @Override
    public String getAccess_token() {
        String access_token = mPrefs.getString(PREF_KEY_PROJECT_ACCESS_TOKEN, "");
        return access_token;
    }

    @Override
    public void setAccess_token(String access_token) {
        mPrefs.edit().putString(PREF_KEY_PROJECT_ACCESS_TOKEN, access_token).apply();
    }

    @Override
    public String getIsEmailVarified() {
        String emil_varified = mPrefs.getString(PREF_IS_EMAIL, "");
        return emil_varified;
    }

    @Override
    public void setIsEmailVarified(String emailVarified) {
        mPrefs.edit().putString(PREF_IS_EMAIL, emailVarified).apply();
    }

    @Override
    public String getIsQuestionFillup() {
        String question_fillup = mPrefs.getString(PREF_QUESTION_FILLUP, "");
        return question_fillup;

    }

    @Override
    public void setIsQuestionFillup(String questionfillup) {
        mPrefs.edit().putString(PREF_QUESTION_FILLUP, questionfillup).apply();
    }

    @Override
    public String getLocalValue() {

        String vaulue = mPrefs.getString(LOCAL_KEY, "");
        return vaulue;

    }

    @Override
    public void setLocalValue(String localValue) {
        mPrefs.edit().putString(LOCAL_KEY, localValue).apply();
    }

    @Override
    public String getCurrentFirstName() {
        return mPrefs.getString(PREF_KEY_CURRENT_FIRST_NAME, "");
    }

    @Override
    public void setCurrentFirstName(String firstName) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_FIRST_NAME, firstName).apply();
    }

    @Override
    public String getCurrentLastName() {
        return mPrefs.getString(PREF_KEY_CURRENT_LAST_NAME, "");
    }

    @Override
    public void setCurrentLastName(String lastName) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_LAST_NAME, lastName).apply();
    }

    @Override
    public String getCurrentMobileNumber() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_MOB, "");
    }

    @Override
    public void setCurrentMobileNumber(String mobile) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_MOB, mobile).apply();
    }

    @Override
    public String getCurrentUserEmail() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_EMAIL, "");
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_EMAIL, email).apply();
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_PROFILE_PIC_URL, "");
    }

    @Override
    public void setCurrentUserProfilePicUrl(String profilePicUrl) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_PROFILE_PIC_URL, profilePicUrl).apply();
    }

    @Override
    public void setCurrentUserGender(String gender) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_GENDER, gender).apply();
    }

    @Override
    public String getCurrentUserGender() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_GENDER, "");
    }

    @Override
    public long getLastIntaractionTimestamp() {
        return mPrefs.getLong(KEY_SP_LAST_INTERACTION_TIME, 0);
    }

    @Override
    public void setLastIntaractionTimestamp(long timeStamp) {
        mPrefs.edit().putLong(KEY_SP_LAST_INTERACTION_TIME, timeStamp).apply();
    }

    @Override
    public void setRegistrationType(String registrationType) {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_REGTYPE, registrationType).apply();
    }

    @Override
    public String getRegistrationType() {
        return mPrefs.getString(PREF_KEY_CURRENT_USER_REGTYPE, "");
    }

    @Override
    public void setMyObject(String stringValueOfMyObject) {
        mPrefs.edit().putString(MYOBJECT, stringValueOfMyObject).apply();
    }

    @Override
    public String getMyObject() {
        return mPrefs.getString(MYOBJECT, "");

    }

    @Override
    public void destroyPref()
    {
        mPrefs.edit().putString(PREF_KEY_CURRENT_USER_ID, "").apply();

      //  mPrefs.edit().clear().apply();
    }

    public static String getSharedPreferencesString(Context context, String key, String _default){
        SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, _default);
    }
}
