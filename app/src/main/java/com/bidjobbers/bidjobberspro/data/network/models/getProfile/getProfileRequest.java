package com.bidjobbers.bidjobberspro.data.network.models.getProfile;

public class getProfileRequest {


    /**
     * locale : en
     */

    private String locale;


    public getProfileRequest(String locale) {
        this.locale = locale;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
