package com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList;

import com.bidjobbers.bidjobberspro.data.network.models.Archived.ResponseData;

import java.util.List;

public class BlockCustomerListResponse {


    /**
     * responseCode : 1
     * responseText : Data found Successfully.
     * responseData : {"blocklist":[{"customer_id":"1","customer_name":"test data","customer_phone":"9090909090","customer_email":"payal1@gmail.com","customer_profile_image":"http://192.168.5.51/bidjobbers/public/customer/profile_image/1575870380.png"}]}
     */

    private int responseCode;
    private String responseText;
    private ResponseDataBean responseData;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        private List<BlocklistBean> blocklist;

        public List<BlocklistBean> getBlocklist() {
            return blocklist;
        }

        public void setBlocklist(List<BlocklistBean> blocklist) {
            this.blocklist = blocklist;
        }

        private pagination pagination;

        public pagination getPagination() {
            return pagination;
        }

        public void setPagination(pagination pagination) {
            this.pagination = pagination;
        }

        public static class  pagination
        {
            private String current_page;
            private String next_page;

            public String getCurrent_page() {
                return current_page;
            }

            public void setCurrent_page(String current_page) {
                this.current_page = current_page;
            }

            public String getNext_page() {
                return next_page;
            }

            public void setNext_page(String next_page) {
                this.next_page = next_page;
            }
        }

        public static class BlocklistBean {
            /**
             * customer_id : 1
             * customer_name : test data
             * customer_phone : 9090909090
             * customer_email : payal1@gmail.com
             * customer_profile_image : http://192.168.5.51/bidjobbers/public/customer/profile_image/1575870380.png
             */

            private String customer_id;
            private String customer_name;
            private String customer_phone;
            private String customer_email;
            private String customer_profile_image;

            public String getCustomer_id() {
                return customer_id;
            }

            public void setCustomer_id(String customer_id) {
                this.customer_id = customer_id;
            }

            public String getCustomer_name() {
                return customer_name;
            }

            public void setCustomer_name(String customer_name) {
                this.customer_name = customer_name;
            }

            public String getCustomer_phone() {
                return customer_phone;
            }

            public void setCustomer_phone(String customer_phone) {
                this.customer_phone = customer_phone;
            }

            public String getCustomer_email() {
                return customer_email;
            }

            public void setCustomer_email(String customer_email) {
                this.customer_email = customer_email;
            }

            public String getCustomer_profile_image() {
                return customer_profile_image;
            }

            public void setCustomer_profile_image(String customer_profile_image) {
                this.customer_profile_image = customer_profile_image;
            }
        }
    }
}
