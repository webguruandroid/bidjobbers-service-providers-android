
package com.bidjobbers.bidjobberspro.data.network.models.BankDetails;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class BankDetailsResponse {

    @SerializedName("responseCode")
    private Long mResponseCode;
    @SerializedName("responseText")
    private String mResponseText;

    public Long getResponseCode() {
        return mResponseCode;
    }

    public void setResponseCode(Long responseCode) {
        mResponseCode = responseCode;
    }

    public String getResponseText() {
        return mResponseText;
    }

    public void setResponseText(String responseText) {
        mResponseText = responseText;
    }

}
