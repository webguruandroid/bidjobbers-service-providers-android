package com.bidjobbers.bidjobberspro.data.network.models.TimeSlot;

import java.util.List;

public class TimeSlotResponse {


    /**
     * responseCode : 1
     * responseText : Data found Successfully.
     * responseData : {"avalableSlots":[{"id":"2","dayName":"Monday","slots":[{"slot_id":"6","slots":"09:00 - 10:00","jobberSlot":"1"}],"start_day":"0","start_date":"2020-01-06","is_working":"1"},{"id":"3","dayName":"Tuesday","slots":[{"slot_id":"14","slots":"10:00 - 11:00","jobberSlot":"0"},{"slot_id":"15","slots":"11:00 - 12:00","jobberSlot":"0"}],"start_day":"1","start_date":"2020-01-07","is_working":"1"},{"id":"4","dayName":"Wednesday","slots":[],"start_day":"2","start_date":"2020-01-08","is_working":"0"},{"id":"5","dayName":"Thursday","slots":[],"start_day":"3","start_date":"2020-01-09","is_working":"0"},{"id":"6","dayName":"Friday","slots":[],"start_day":"4","start_date":"2020-01-10","is_working":"0"},{"id":"7","dayName":"Saturday","slots":[],"start_day":"5","start_date":"2020-01-11","is_working":"0"},{"id":"1","dayName":"Sunday","slots":[{"slot_id":"7","slots":"08:00 - 09:00","jobberSlot":"1"},{"slot_id":"8","slots":"09:00 - 10:00","jobberSlot":"0"},{"slot_id":"9","slots":"10:00 - 11:00","jobberSlot":"0"},{"slot_id":"10","slots":"11:00 - 12:00","jobberSlot":"0"},{"slot_id":"11","slots":"12:00 - 13:00","jobberSlot":"0"}],"start_day":"6","start_date":"2020-01-12","is_working":"1"}]}
     */

    private int responseCode;
    private String responseText;
    private ResponseDataBean responseData;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        private List<AvalableSlotsBean> avalableSlots;

        public List<AvalableSlotsBean> getAvalableSlots() {
            return avalableSlots;
        }

        public void setAvalableSlots(List<AvalableSlotsBean> avalableSlots) {
            this.avalableSlots = avalableSlots;
        }

        public static class AvalableSlotsBean {
            /**
             * id : 2
             * dayName : Monday
             * slots : [{"slot_id":"6","slots":"09:00 - 10:00","jobberSlot":"1"}]
             * start_day : 0
             * start_date : 2020-01-06
             * is_working : 1
             */

            private String id;
            private String dayName;
            private String start_day;
            private String start_date;
            private String is_working;
            private List<SlotsBean> slots;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getDayName() {
                return dayName;
            }

            public void setDayName(String dayName) {
                this.dayName = dayName;
            }

            public String getStart_day() {
                return start_day;
            }

            public void setStart_day(String start_day) {
                this.start_day = start_day;
            }

            public String getStart_date() {
                return start_date;
            }

            public void setStart_date(String start_date) {
                this.start_date = start_date;
            }

            public String getIs_working() {
                return is_working;
            }

            public void setIs_working(String is_working) {
                this.is_working = is_working;
            }

            public List<SlotsBean> getSlots() {
                return slots;
            }

            public void setSlots(List<SlotsBean> slots) {
                this.slots = slots;
            }

            public static class SlotsBean {
                /**
                 * slot_id : 6
                 * slots : 09:00 - 10:00
                 * jobberSlot : 1
                 */

                private String slot_id;
                private String slots;
                private String jobberSlot;

                public String getSlot_id() {
                    return slot_id;
                }

                public void setSlot_id(String slot_id) {
                    this.slot_id = slot_id;
                }

                public String getSlots() {
                    return slots;
                }

                public void setSlots(String slots) {
                    this.slots = slots;
                }

                public String getJobberSlot() {
                    return jobberSlot;
                }

                public void setJobberSlot(String jobberSlot) {
                    this.jobberSlot = jobberSlot;
                }
            }
        }
    }
}
