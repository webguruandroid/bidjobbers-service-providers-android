package com.bidjobbers.bidjobberspro.data.network.models.BidCreate;

public class BidCreateRequest {


    /**
     * locale : en
     * task_id : 36
     * day_id : 1
     * slot_id : 7
     * price : $500
     * date : 2020-01-03
     */

    private String locale;
    private String task_id;
    private String day_id;
    private String slot_id;
    private String price;
    private String date;
    private String customer_id;

    public BidCreateRequest(String locale, String task_id, String day_id, String slot_id, String price, String date,String customer_id) {
        this.locale = locale;
        this.task_id = task_id;
        this.day_id = day_id;
        this.slot_id = slot_id;
        this.price = price;
        this.date = date;
        this.customer_id=customer_id;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getDay_id() {
        return day_id;
    }

    public void setDay_id(String day_id) {
        this.day_id = day_id;
    }

    public String getSlot_id() {
        return slot_id;
    }

    public void setSlot_id(String slot_id) {
        this.slot_id = slot_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
