package com.bidjobbers.bidjobberspro.data.network.models.CityCountry;

import java.util.List;

public class CityCountryResponse {


    /**
     * responseCode : 1
     * responseText : Data found Successfully.
     * responseData : {"masterData":[{"country_id":"1","country_name":"India","city":[{"id":"3","name":"Midnapur"},{"id":"2","name":"bengaluru"},{"id":"1","name":"Kolkata"}]}]}
     */

    private int responseCode;
    private String responseText;
    private ResponseDataBean responseData;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        private List<MasterDataBean> masterData;

        public List<MasterDataBean> getMasterData() {
            return masterData;
        }

        public void setMasterData(List<MasterDataBean> masterData) {
            this.masterData = masterData;
        }

        public static class MasterDataBean {
            /**
             * country_id : 1
             * country_name : India
             * city : [{"id":"3","name":"Midnapur"},{"id":"2","name":"bengaluru"},{"id":"1","name":"Kolkata"}]
             */

            private String country_id;
            private String country_name;
            private List<CityBean> city;

            public String getCountry_id() {
                return country_id;
            }

            public void setCountry_id(String country_id) {
                this.country_id = country_id;
            }

            public String getCountry_name() {
                return country_name;
            }

            public void setCountry_name(String country_name) {
                this.country_name = country_name;
            }

            public List<CityBean> getCity() {
                return city;
            }

            public void setCity(List<CityBean> city) {
                this.city = city;
            }

            public static class CityBean {
                /**
                 * id : 3
                 * name : Midnapur
                 */

                private String id;
                private String name;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }
            }
        }
    }
}
