package com.bidjobbers.bidjobberspro.data.network.models.verifyotp;

public class VerifyOtpResponse {

    /**
     * responseCode : 0
     * responseText : Otp does not match.
     */

    private int responseCode;
    private String responseText;

    public int getResponseCode()
    {
        return responseCode;
    }

    public void setResponseCode(int responseCode)
    {
        this.responseCode = responseCode;
    }

    public String getResponseText()
    {
        return responseText;
    }

    public void setResponseText(String responseText)
    {
        this.responseText = responseText;
    }


    /**
     * responseCode : 1
     * responseText : OTP Verified.
     * responseData : {"userID":"12"}
     */

}
