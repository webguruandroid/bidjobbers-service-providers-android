package com.bidjobbers.bidjobberspro.data.network;

import com.bidjobbers.bidjobberspro.data.network.models.Advance.AdvanceRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Advance.AdvanceResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Allpages.AllPagesRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Allpages.AllPagesResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Archived.ArchiveDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Archived.ArchiveListRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BankDetails.BankDetailsRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BankDetails.BankDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BidCreate.BidCreateRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BidCreate.BidCreateResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomer.BlockCustomerRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomer.BlockCustomerResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList.BlockCustomerListRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList.BlockCustomerListResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BusinessInfo.BusinessInfoRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BusinessInfo.BusinessInfoResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BusinessLocation.BusinessLocationRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BusinessLocation.BusinessLocationResponse;
import com.bidjobbers.bidjobberspro.data.network.models.CacelledTask.CancelledTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.CacelledTask.CancelledTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ChangeaPassword.ChangePasswordRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ChangeaPassword.ChangePasswordResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Checkpassword.CheckPasswordRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Checkpassword.CheckpasswordResponse;
import com.bidjobbers.bidjobberspro.data.network.models.CityCountry.CityCountryResponse;
import com.bidjobbers.bidjobberspro.data.network.models.CompletedTask.CompletedTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.CompletedTask.CompletedTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ContactUs.ContactUsRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ContactUs.ContactUsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Customer.CustomerDetailsRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Customer.CustomerDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.DeviceToken.DeviceTokenRequest;
import com.bidjobbers.bidjobberspro.data.network.models.DeviceToken.DeviceTokenResponse;
import com.bidjobbers.bidjobberspro.data.network.models.EndTask.EndTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ImagePicker.ImagePickerResponse;
import com.bidjobbers.bidjobberspro.data.network.models.IntroBusinessDetails.IntroBusinessDetailsRequest;
import com.bidjobbers.bidjobberspro.data.network.models.IntroBusinessDetails.IntroBusinessDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.JobStatus.JobStatusRequest;
import com.bidjobbers.bidjobberspro.data.network.models.JobStatus.JobStatusResponse;
import com.bidjobbers.bidjobberspro.data.network.models.NewArchive.NewArchiveList;
import com.bidjobbers.bidjobberspro.data.network.models.Notification.NotificationRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Notification.NotificationResponse;
import com.bidjobbers.bidjobberspro.data.network.models.NotificationCount.NotificationCountRequest;
import com.bidjobbers.bidjobberspro.data.network.models.NotificationCount.NotificationCountResponse;
import com.bidjobbers.bidjobberspro.data.network.models.PriceFor.PriceForRequest;
import com.bidjobbers.bidjobberspro.data.network.models.PriceFor.PriceForResponse;
import com.bidjobbers.bidjobberspro.data.network.models.PriceForTree.PriceForTreeResponse;
import com.bidjobbers.bidjobberspro.data.network.models.PriceForTree.PriceForTreeRquest;
import com.bidjobbers.bidjobberspro.data.network.models.PropertyType.PropertyTypeResponse;
import com.bidjobbers.bidjobberspro.data.network.models.PropertyType.PropertypeRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ReSchedule.RescheduleRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ReSchedule.RescheduleResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Recivetask.ReciveTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Recivetask.ReciveTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ReviewList.ReviewRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ReviewList.ReviewResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ScheduleTask.ScheduletaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ScheduleTask.ScheduletaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.SetLocal.SetLocalRequest;
import com.bidjobbers.bidjobberspro.data.network.models.SetLocal.SetLocalResponse;
import com.bidjobbers.bidjobberspro.data.network.models.SocialMedia.SocialMediaRequest;
import com.bidjobbers.bidjobberspro.data.network.models.SocialMedia.SocialMediaResponse;
import com.bidjobbers.bidjobberspro.data.network.models.StartTask.StartTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.StartTask.StartTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.TimeSlot.TimeSlotRequest;
import com.bidjobbers.bidjobberspro.data.network.models.TimeSlot.TimeSlotResponse;
import com.bidjobbers.bidjobberspro.data.network.models.UnBlockUser.UnBlockUserRequest;
import com.bidjobbers.bidjobberspro.data.network.models.UnBlockUser.UnBlockUserResponse;
import com.bidjobbers.bidjobberspro.data.network.models.UpdateProfile.UpdateProfileResponse;
import com.bidjobbers.bidjobberspro.data.network.models.businesshours.BusinessHoursRequest;
import com.bidjobbers.bidjobberspro.data.network.models.businesshours.BusinessHoursRespose;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileRequest;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.data.network.models.kindofproperty.KindofPropertyRequest;
import com.bidjobbers.bidjobberspro.data.network.models.kindofproperty.KindofPropertyResponse;
import com.bidjobbers.bidjobberspro.data.network.models.login.LoginRequest;
import com.bidjobbers.bidjobberspro.data.network.models.login.LoginResponce;
import com.bidjobbers.bidjobberspro.data.network.models.priceforlawnservice.PriceForLawnServiceResponse;
import com.bidjobbers.bidjobberspro.data.network.models.register.RegisterResponse;
import com.bidjobbers.bidjobberspro.data.network.models.resentotp.ResendOtpRequest;
import com.bidjobbers.bidjobberspro.data.network.models.resentotp.ResendOtpResponse;
import com.bidjobbers.bidjobberspro.data.network.models.resetpassword.ResetPasswordRequest;
import com.bidjobbers.bidjobberspro.data.network.models.resetpassword.ResetPasswordResponse;
import com.bidjobbers.bidjobberspro.data.network.models.servicedetails.ServiceDetailsRequest;
import com.bidjobbers.bidjobberspro.data.network.models.servicedetails.ServiceDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.subscription.SubscriptionRequest;
import com.bidjobbers.bidjobberspro.data.network.models.subscription.SubscriptionResponse;
import com.bidjobbers.bidjobberspro.data.network.models.test.TestResponse;
import com.bidjobbers.bidjobberspro.data.network.models.typeofpropertyresponse.TypeOfProperty;
import com.bidjobbers.bidjobberspro.data.network.models.typeofpropertyresponse.TypeOfPropertyRequest;
import com.bidjobbers.bidjobberspro.data.network.models.verifyotp.VerifyOtpRequest;
import com.bidjobbers.bidjobberspro.data.network.models.verifyotp.VerifyOtpResponse;

import java.util.ArrayList;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiHelper {

    /*@POST("send_otp")
    Call<OTPResponse> sendOTP(@Body SendOTPRequest req);

    @Multipart
    @POST("signup")
    Call<RegisterResponse> register(@Part MultipartBody.Part profile_pic,
                                    @Part("f_name") RequestBody f_name,
                                    @Part("l_name") RequestBody l_name,
                                    @Part("email") RequestBody email,
                                    @Part("mobile") RequestBody mobile,
                                    @Part("password") RequestBody password,
                                    @Part("device_type") RequestBody device_type,
                                    @Part("device_token") RequestBody device_token);*/

    @POST("5d2ebfe0340000550064d21e")
    Single<TestResponse> getHomeData();

    //Dashboard Tabs

    ArrayList<String> getTypesOfTabs();


    @POST("jobber/login")
    Single<LoginResponce> getLoginResponse(@Body LoginRequest req);




    @Multipart
    @POST("jobber/register")
    Single<RegisterResponse> postPersonalUpdate(
                                              @Part MultipartBody.Part profileImage,
                                              @Part("company_name") RequestBody company_name,
                                              @Part("name") RequestBody name,
                                              @Part("email") RequestBody email,
                                              @Part("phone") RequestBody phone,
                                              @Part("password") RequestBody password,
                                              @Part("locale") RequestBody locale,
                                              @Part("accept_terms")RequestBody accept_terms);


    @Multipart
    @POST("jobber/register")
    Single<RegisterResponse> postPersonalUpdate(

            @Part("company_name") RequestBody company_name,
            @Part("name") RequestBody name,
            @Part("email") RequestBody email,
            @Part("phone") RequestBody phone,
            @Part("password") RequestBody password,
            @Part("locale") RequestBody locale,
            @Part("accept_terms")RequestBody accept_terms);


    @POST("jobber/verify-otp")
    Single<VerifyOtpResponse> getVeryResponse(@Body VerifyOtpRequest req);

    @POST("jobber/send-otp")
    Single<ResendOtpResponse> getSentOtp(@Body ResendOtpRequest req);

    @POST("jobber/reset-password")
    Single<ResetPasswordResponse> resetPassword(@Body ResetPasswordRequest req);


    @POST("jobber/get-service-details")
    Single<ServiceDetailsResponse>getServiceDetails(@Header("Authorization") String authorization,
                                                    @Header("Accept") String accept,
                                                    @Header("Content-Type") String contentType,
                                                    @Body ServiceDetailsRequest serviceDetailsRequest);



    @POST("jobber/my-account")
    Single<getProfileResponse>getProfileDetails(@Header("Authorization") String authorization,
                                                @Header("Accept") String accept,
                                                @Header("Content-Type") String contentType,
                                                @Body getProfileRequest getProfileRequest);



    @Multipart
    @POST("jobber/update-account")
    Single<UpdateProfileResponse> postProfileUpdate(
            @Header("Authorization") String authorization,
            @Part("name") RequestBody name,
            @Part("phone") RequestBody phone,
            @Part("locale") RequestBody locale,
            @Part MultipartBody.Part profileImage
           );





    @POST("jobber/add-update-buisness-desc")
    Single<IntroBusinessDetailsResponse>postIntroBusinessDetailsResponse(@Header("Authorization") String authorization,
                                                                         @Header("Content-Type") String contentType,
                                                                         @Body IntroBusinessDetailsRequest introBusinessDetailsRequest);






   @POST("jobber/add-update-service-type")
   Single<KindofPropertyResponse>postKindOfPropertyWorkOn(@Header("Authorization") String authorization,
                                                          @Header("Content-Type") String contentType,
                                                          @Body KindofPropertyRequest kindofPropertyRequest);




   @POST("jobber/add-update-buisness-info")
   Single<BusinessInfoResponse>postBusinessInfo(@Header("Authorization") String authorization,
                                                @Header("Content-Type") String contentType,
                                                @Body BusinessInfoRequest businessInfoRequest);





    @POST("jobber/add-update-buisness-location")
    Single<BusinessLocationResponse>postBusinessLocation(@Header("Authorization") String authorization,
                                                         @Header("Content-Type") String contentType,
                                                         @Body BusinessLocationRequest businessLocationRequest);


    @POST("jobber/add-update-buisness_hr")
    Single<BusinessHoursRespose>postBusinessHours(@Header("Authorization") String authorization,
                                                      @Header("Content-Type") String contentType,
                                                      @Body BusinessHoursRequest businessHoursRequest);


    @POST("jobber/add-update-social-link")
    Single<SocialMediaResponse>postSocialMediaLink(@Header("Authorization") String authorization,
                                                   @Header("Content-Type") String contentType,
                                                   @Body SocialMediaRequest socialMediaRequest);



    @POST("jobber/add-update-property-type")
    Single<PropertyTypeResponse>postPropertyType(@Header("Authorization") String authorization,
                                                  @Header("Content-Type") String contentType,
                                                  @Body PropertypeRequest propertypeRequest);



    @POST("jobber/add-update-advance-booking")
    Single<AdvanceResponse>postAdvanceType(@Header("Authorization") String authorization,
                                            @Header("Content-Type") String contentType,
                                            @Body AdvanceRequest advanceRequest);




    @POST("jobber/add-update-lawn-service-details")
    Single<PriceForResponse>postPriceForType(@Header("Authorization") String authorization,
                                            @Header("Content-Type") String contentType,
                                            @Body PriceForRequest priceForRequest);






    @POST("jobber/add-update-tree-service-details")
    Single<PriceForTreeResponse>postPriceTreeForType(@Header("Authorization") String authorization,
                                                 @Header("Content-Type") String contentType,
                                                 @Body PriceForTreeRquest priceForTreeRquest);





    @Multipart
    @POST("jobber/add-update-project-image")
    Single<ImagePickerResponse>postWorkPhotoForType(@Header("Authorization") String authorization,
                                                    @Part("locale") RequestBody local,
                                                    @Part("is_work_image") RequestBody isworkimage,
                                                    @Part MultipartBody.Part[] surveyImage);




    @POST("jobber/get-received-task")
    Single<ReciveTaskResponse>getReciveTask(@Header("Authorization") String authorization,
                                            @Header("Content-Type") String contentType,
                                            @Body ReciveTaskRequest reciveTaskRequest);





    @POST("jobber/change-password")
    Single<ChangePasswordResponse>postChangePassword(@Header("Authorization") String authorization,
                                                @Header("Content-Type") String contentType,
                                                @Body ChangePasswordRequest changePasswordRequest);





    @POST("jobber/get-time-slot")
    Single<TimeSlotResponse>postGetTimeSlot(@Header("Authorization") String authorization,
                                            @Header("Content-Type") String contentType,
                                            @Body TimeSlotRequest timeSlotRequest);





    @POST("jobber/bid-create")
    Single<BidCreateResponse>postBid(@Header("Authorization") String authorization,
                                     @Header("Content-Type") String contentType,
                                     @Body BidCreateRequest bidCreateRequest);







    @POST("jobber/get-schedule-task")
    Single<ScheduletaskResponse>getScheduledTask(@Header("Authorization") String authorization,
                                              @Header("Content-Type") String contentType,
                                              @Body ScheduletaskRequest scheduletaskRequest);




    @POST("getpage")
    Single<AllPagesResponse>getAllPages(
            @Header("Content-Type") String contentType,
            @Body AllPagesRequest allPagesRequest);








    @POST("jobber/re-schedule-task")
    Single<RescheduleResponse> postRescheduleTask(@Header("Authorization") String authorization,
                                                  @Header("Content-Type") String contentType,
                                                  @Body RescheduleRequest allPagesRequest);




    @POST("jobber/complete-task")
    Single<CompletedTaskResponse> postCompleteTask(@Header("Authorization") String authorization,
                                                   @Header("Content-Type") String contentType,
                                                   @Body CompletedTaskRequest completedTaskRequest);


    @POST("jobber/cancelled-task")
    Single<CancelledTaskResponse>postCancelledTask(@Header("Authorization") String authorization,
                                                   @Header("Content-Type") String contentType,
                                                   @Body CancelledTaskRequest cancelledTaskRequest);


    @POST("jobber/start-task")
    Single<StartTaskResponse>postStartTask(@Header("Authorization") String authorization,
                                           @Header("Content-Type") String contentType,
                                           @Body StartTaskRequest startTaskRequest);




    @Multipart
    @POST("jobber/end-task")
    Single<EndTaskResponse>postEndTask(@Header("Authorization") String authorization,
                                       @Part("locale") RequestBody local,
                                       @Part("customer_id") RequestBody CustomerId,
                                       @Part("task_id") RequestBody taskId,
                                       @Part("bid_id") RequestBody bidId,
                                       @Part("task_end_notes")RequestBody task_end_notes,
                                       @Part MultipartBody.Part[] task_images);

    @POST("jobber/block-customer")
    Single<BlockCustomerResponse>postBlockUser(@Header("Authorization") String authorization,
                                               @Header("Content-Type") String contentType,
                                               @Body BlockCustomerRequest blockCustomerRequest);



    @POST("jobber/unblock-customer")
    Single<UnBlockUserResponse>postUnBlockUser(@Header("Authorization") String authorization,
                                               @Header("Content-Type") String contentType,
                                               @Body UnBlockUserRequest unBlockUserRequest);



    @POST("jobber/block-customer-list")
    Single<BlockCustomerListResponse>postBlockUserList(@Header("Authorization") String authorization,
                                                       @Header("Content-Type") String contentType,
                                                       @Body BlockCustomerListRequest blockCustomerListRequest);







     @POST("jobber/review-list")
     Single<ReviewResponse>getReviewList(@Header("Authorization") String authorization,
                                      @Header("Content-Type") String contentType,
                                      @Body ReviewRequest reviewRequest);




    @POST("jobber/notification-list")
    Single<NotificationResponse>getNotificationList(@Header("Authorization") String authorization,
                                              @Header("Content-Type") String contentType,
                                              @Body NotificationRequest notificationRequest);


    @POST("jobber/subscription-package-list")
    Single<SubscriptionResponse>getSubscription(@Header("Authorization") String authorization,
                                                @Body SubscriptionRequest subscriptionRequest);


    @POST("jobber/notification-count")
    Single<NotificationCountResponse>getNotificationCount(@Header("Authorization") String authorization,
                                                          @Header("Content-Type") String contentType,
                                                          @Body NotificationCountRequest notificationCountRequest);



    @POST("jobber/update-token")
    Single<DeviceTokenResponse>getDeviceToken(@Header("Authorization") String authorization,
                                                    @Header("Content-Type") String contentType,
                                                    @Body DeviceTokenRequest notificationCountRequest);


    @POST("jobber/get-job-status")
    Single<JobStatusResponse>getJobStatus(@Header("Authorization") String authorization,
                                         @Header("Content-Type") String contentType,
                                         @Body JobStatusRequest jobStatusRequest);


    @POST("jobber/get-customer")
    Single<CustomerDetailsResponse>getCustomerDetails(@Header("Authorization") String authorization,
                                                      @Header("Content-Type") String contentType,
                                                      @Body CustomerDetailsRequest customerDetailsRequest);
    @POST("jobber/archive-job-details")
    Single<ArchiveDetailsResponse>getArchiveDetails(@Header("Authorization") String authorization,
                                                    @Header("Content-Type") String contentType,
                                                    @Body CustomerDetailsRequest customerDetailsRequest);



    @POST("jobber/archive-job-list")
    Single<NewArchiveList>getArchivedList(@Header("Authorization") String authorization,
                                          @Header("Content-Type") String contentType,
                                          @Body ArchiveListRequest archiveListRequest);



    @POST("countryCity")
    Single<CityCountryResponse>getCityCountry();




    @POST("jobber/add-update-transaction-account-details")
    Single<BankDetailsResponse>submitBankDetails(@Header("Authorization") String authorization,
                                                 @Header("Content-Type") String contentType,
                                                 @Body BankDetailsRequest bankDetailsRequest);






    @POST("jobber/check-user-password")
    Single<CheckpasswordResponse>validatePassword(@Header("Authorization") String authorization,
                                                  @Header("Content-Type") String contentType,
                                                  @Body CheckPasswordRequest checkPasswordRequest);





    @POST("jobber/set-locale")
    Single<SetLocalResponse>setLocal(@Header("Authorization") String authorization,
                                    @Header("Content-Type") String contentType,
                                    @Body SetLocalRequest setLocalRequest);



    @POST("contact_form")
    Single<ContactUsResponse>postContactUs(@Header("Content-Type") String contentType,
                                           @Body ContactUsRequest contactUsRequest);


    @POST("http://www.mocky.io/v2/5dba87f03000008700028f4d")
    Single<TypeOfProperty> getTypesOfProperty(@Body TypeOfPropertyRequest req);

    @POST("http://www.mocky.io/v2/5dbbc93331000096544c0c89")
    Single<BusinessHoursRespose> getBusinessHours(@Body TypeOfPropertyRequest req);

    @POST("http://www.mocky.io/v2/5dbbdc7d310000825d4c0d0b")
    Single<PriceForLawnServiceResponse> getPriceForLawnService(@Body TypeOfPropertyRequest req);






}
