package com.bidjobbers.bidjobberspro.data.network.models.typeofpropertyresponse;

public class TypeOfPropertyRequest {

    String access_token="";

    public TypeOfPropertyRequest(String access_token) {
        this.access_token = access_token;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
}
