package com.bidjobbers.bidjobberspro.data.network.models.ReSchedule;

public class RescheduleRequest {


    /**
     * locale : en
     * bid_id : 21
     * task_id : 63
     * day_id : 1
     * slot_id : 7
     * date : 2020-01-26
     * rescheduletext : please update
     */

    private String locale;
    private String bid_id;
    private String task_id;
    private String day_id;
    private String slot_id;
    private String date;
    private String rescheduletext;

    public RescheduleRequest(String locale, String bid_id, String task_id, String day_id, String slot_id, String date, String rescheduletext) {
        this.locale = locale;
        this.bid_id = bid_id;
        this.task_id = task_id;
        this.day_id = day_id;
        this.slot_id = slot_id;
        this.date = date;
        this.rescheduletext = rescheduletext;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getBid_id() {
        return bid_id;
    }

    public void setBid_id(String bid_id) {
        this.bid_id = bid_id;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getDay_id() {
        return day_id;
    }

    public void setDay_id(String day_id) {
        this.day_id = day_id;
    }

    public String getSlot_id() {
        return slot_id;
    }

    public void setSlot_id(String slot_id) {
        this.slot_id = slot_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRescheduletext() {
        return rescheduletext;
    }

    public void setRescheduletext(String rescheduletext) {
        this.rescheduletext = rescheduletext;
    }
}
