package com.bidjobbers.bidjobberspro.data.network.models.PriceFor;

import java.util.List;

public class PriceForRequest {


    /**
     * lawnservice : [{"lawnSizeId":"1","lawnSizePrice":"$697"},{"lawnSizeId":"2","lawnSizePrice":"$789"},{"lawnSizeId":"3","lawnSizePrice":"$678"},{"lawnSizeId":"4","lawnSizePrice":"$884"},{"lawnSizeId":"5","lawnSizePrice":""}]
     * locale : en
     */

    private String locale;
    private List<LawnserviceBean> lawnservice;

    public PriceForRequest(String locale, List<LawnserviceBean> lawnservice) {
        this.locale = locale;
        this.lawnservice = lawnservice;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public List<LawnserviceBean> getLawnservice() {
        return lawnservice;
    }

    public void setLawnservice(List<LawnserviceBean> lawnservice) {
        this.lawnservice = lawnservice;
    }

    public static class LawnserviceBean {
        /**
         * lawnSizeId : 1
         * lawnSizePrice : $697
         */

        private String lawnSizeId;
        private String lawnSizePrice;

        public String getLawnSizeId() {
            return lawnSizeId;
        }

        public void setLawnSizeId(String lawnSizeId) {
            this.lawnSizeId = lawnSizeId;
        }

        public String getLawnSizePrice() {
            return lawnSizePrice;
        }

        public void setLawnSizePrice(String lawnSizePrice) {
            this.lawnSizePrice = lawnSizePrice;
        }
    }
}
