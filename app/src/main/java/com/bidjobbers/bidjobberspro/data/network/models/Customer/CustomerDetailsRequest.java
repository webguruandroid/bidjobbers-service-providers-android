
package com.bidjobbers.bidjobberspro.data.network.models.Customer;


import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class CustomerDetailsRequest {

    @SerializedName("locale")
    private String mLocale;
    @SerializedName("task_id")
    private String mTaskId;

    public CustomerDetailsRequest(String mLocale, String mTaskId) {
        this.mLocale = mLocale;
        this.mTaskId = mTaskId;
    }

    public String getLocale() {
        return mLocale;
    }

    public void setLocale(String locale) {
        mLocale = locale;
    }

    public String getTaskId() {
        return mTaskId;
    }

    public void setTaskId(String taskId) {
        mTaskId = taskId;
    }

}
