package com.bidjobbers.bidjobberspro.data.network.models.subscription;

import java.util.List;

public class SubscriptionResponse {

    /**
     * responseCode : 1
     * responseText : Data found Successfully.
     * responseData : {"subscribe_package":[{"id":4,"name":"Premium Plan","description":"This is a premium plan","planId":"premium_plan","amount":"200","billing_period":"Yearly","free_trial":"7 days"},{"id":3,"name":"Basic Plan","description":"This is a basic plan","planId":"basic_plan","amount":"100","billing_period":"Monthly","free_trial":"7 days"}]}
     */

    private int responseCode;
    private String responseText;
    private ResponseDataBean responseData;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        private List<SubscribePackageBean> subscribe_packages;

        public List<SubscribePackageBean> getSubscribe_package() {
            return subscribe_packages;
        }

        public void setSubscribe_package(List<SubscribePackageBean> subscribe_package) {
            this.subscribe_packages = subscribe_package;
        }

        public static class SubscribePackageBean {
            /**
             * id : 4
             * name : Premium Plan
             * description : This is a premium plan
             * planId : premium_plan
             * amount : 200
             * billing_period : Yearly
             * free_trial : 7 days
             */

            private int id;
            private String name;
            private String description;
            private String android_plan_sku;
            private String apple_plan_sku;
            private String amount;
            private String billing_period;
            private String free_trial;

            public String getAndroid_plan_sku() {
                return android_plan_sku;
            }

            public void setAndroid_plan_sku(String android_plan_sku) {
                this.android_plan_sku = android_plan_sku;
            }

            public String getApple_plan_sku() {
                return apple_plan_sku;
            }

            public void setApple_plan_sku(String apple_plan_sku) {
                this.apple_plan_sku = apple_plan_sku;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }



            public String getAmount() {
                return amount;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }

            public String getBilling_period() {
                return billing_period;
            }

            public void setBilling_period(String billing_period) {
                this.billing_period = billing_period;
            }

            public String getFree_trial() {
                return free_trial;
            }

            public void setFree_trial(String free_trial) {
                this.free_trial = free_trial;
            }
        }
    }
}
