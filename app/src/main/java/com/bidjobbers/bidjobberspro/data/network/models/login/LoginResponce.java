package com.bidjobbers.bidjobberspro.data.network.models.login;

public class LoginResponce {


    /**
     * responseCode : 1
     * responseText : Login successful.
     * responseData : {"authorization":{"token_type":"Bearer","expires_in":31622400,"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImVjZGU0ZjhiNzI4YzlkOWQzMzdmMDYyYTBhNTAwYzg0ZTgxNTU3ZmQ3MmExZjk5MzIxMzE1MDZmYzk3NjdkNzViYWY3YzE0ZjE0MTE0ZmMwIn0.eyJhdWQiOiI0NiIsImp0aSI6ImVjZGU0ZjhiNzI4YzlkOWQzMzdmMDYyYTBhNTAwYzg0ZTgxNTU3ZmQ3MmExZjk5MzIxMzE1MDZmYzk3NjdkNzViYWY3YzE0ZjE0MTE0ZmMwIiwiaWF0IjoxNTcxODI3MTI4LCJuYmYiOjE1NzE4MjcxMjgsImV4cCI6MTYwMzQ0OTUyOCwic3ViIjoiNDciLCJzY29wZXMiOlsiKiJdfQ.ADQqK1TtZIEo52yMOw_fLBiBtPmxlAzSRhDg-JmXniFZiSvhrAuDsAMtzGOq0m-kVH_vH_IcTVsJfZ4Rq50uzfU33ehUhaOd7EOEc2-ZfXIffBKu7PAzlPfoANOOjBCBjhVN8bYZa4JrI9WT0_8uxq8RsF3Cpq8_bKg4hrzZQwdu_6K6gvclLYOec3XHEQC6VFfvUIAT7ARtBS1xz9nw_cW_2i_n0HC6nrwIBejq1eZX1qOB13Zn7gTQaw9hoSqmSf_IVeXSaHk1hIQsSkfKviY0nhjDsaqKi0hPpuXn70ijOXiXYECyiFQe9V0UXQDXacc6SEV4st6KahHBYvBowQ9p2WxK-4My-oOhu-xF50Y_ECFFYUxAkIlfcgfNJ-QUpOxiISOkq1AWG_sDIzfW4kKvF4Zy01_I8yTk9dIXqBeuHVa57c0fBNk7HMHNdaU1dDAttZzn-7vbfKXGN0ZUlwFkI4rmkT1YqtNgXtdjHez_y5yRGKjJm4ZBk30dK1k49-kCyBrDvvf6YreaYf_NbQc2JPgv35QXE1GxJBTLCQ1ID7m6C30KEWufbfHR24MZRlzQ0YQyH5TiwYGUaOAUY6ho-I4wTIbPnjgQsZdFq1dQnSSs5u-NSfhMGf5IB6sw0_t5TCu7TJ6kRJQszsOmBeiyTb9IYCGgDE_q_vNnB7k","refresh_token":"def50200132eb25c0c07a207759bb19e8dac8fc2246c375d96c142e490280ad0f378f72065967d3798e72be5a250defd396cbddf2f834bec3b870755c63ffff9220225ebfe473282ceb1ea39f42949b77930d47f90d38ba1490525a9a51c69db0a9a5256272151f208dedc1ef1f61427ada93ff5f8975e6631dfbe84bbd425a3a94a67313bc60a4e349515d7007f5135642d92ca118b308e1bea0158d41045a9a4323d349d29370a57a53d27ef9b838d7f2d02866dcc57b38f759d40a45506d021d6d874cd61891833c7d789d6a4cd09c0c4244281d9fd12c4c29fd93e9b762f3c95a99b4e98e9a8e4564fbabc1780c38ea67c30ccbe5d1bbfd4725b65e91a9f1a9c4214241e3e5e056a0882851fd9fba57360607dfc39fcc6bd1e9485e0add99b3f889a31bacd033f46fa258aec9ab0762f62258ee2822e821782c60c4618e2edbd714562e2f4c4eb34a278e5bff96c9fc6f34771904adb0a09447c11b00b29588355df772c"},"isEmailVerified":"0","hasBusinessDetails":"0"}
     */

    private int responseCode;
    private String responseText;
    private ResponseDataBean responseData;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        /**
         * authorization : {"token_type":"Bearer","expires_in":31622400,"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImVjZGU0ZjhiNzI4YzlkOWQzMzdmMDYyYTBhNTAwYzg0ZTgxNTU3ZmQ3MmExZjk5MzIxMzE1MDZmYzk3NjdkNzViYWY3YzE0ZjE0MTE0ZmMwIn0.eyJhdWQiOiI0NiIsImp0aSI6ImVjZGU0ZjhiNzI4YzlkOWQzMzdmMDYyYTBhNTAwYzg0ZTgxNTU3ZmQ3MmExZjk5MzIxMzE1MDZmYzk3NjdkNzViYWY3YzE0ZjE0MTE0ZmMwIiwiaWF0IjoxNTcxODI3MTI4LCJuYmYiOjE1NzE4MjcxMjgsImV4cCI6MTYwMzQ0OTUyOCwic3ViIjoiNDciLCJzY29wZXMiOlsiKiJdfQ.ADQqK1TtZIEo52yMOw_fLBiBtPmxlAzSRhDg-JmXniFZiSvhrAuDsAMtzGOq0m-kVH_vH_IcTVsJfZ4Rq50uzfU33ehUhaOd7EOEc2-ZfXIffBKu7PAzlPfoANOOjBCBjhVN8bYZa4JrI9WT0_8uxq8RsF3Cpq8_bKg4hrzZQwdu_6K6gvclLYOec3XHEQC6VFfvUIAT7ARtBS1xz9nw_cW_2i_n0HC6nrwIBejq1eZX1qOB13Zn7gTQaw9hoSqmSf_IVeXSaHk1hIQsSkfKviY0nhjDsaqKi0hPpuXn70ijOXiXYECyiFQe9V0UXQDXacc6SEV4st6KahHBYvBowQ9p2WxK-4My-oOhu-xF50Y_ECFFYUxAkIlfcgfNJ-QUpOxiISOkq1AWG_sDIzfW4kKvF4Zy01_I8yTk9dIXqBeuHVa57c0fBNk7HMHNdaU1dDAttZzn-7vbfKXGN0ZUlwFkI4rmkT1YqtNgXtdjHez_y5yRGKjJm4ZBk30dK1k49-kCyBrDvvf6YreaYf_NbQc2JPgv35QXE1GxJBTLCQ1ID7m6C30KEWufbfHR24MZRlzQ0YQyH5TiwYGUaOAUY6ho-I4wTIbPnjgQsZdFq1dQnSSs5u-NSfhMGf5IB6sw0_t5TCu7TJ6kRJQszsOmBeiyTb9IYCGgDE_q_vNnB7k","refresh_token":"def50200132eb25c0c07a207759bb19e8dac8fc2246c375d96c142e490280ad0f378f72065967d3798e72be5a250defd396cbddf2f834bec3b870755c63ffff9220225ebfe473282ceb1ea39f42949b77930d47f90d38ba1490525a9a51c69db0a9a5256272151f208dedc1ef1f61427ada93ff5f8975e6631dfbe84bbd425a3a94a67313bc60a4e349515d7007f5135642d92ca118b308e1bea0158d41045a9a4323d349d29370a57a53d27ef9b838d7f2d02866dcc57b38f759d40a45506d021d6d874cd61891833c7d789d6a4cd09c0c4244281d9fd12c4c29fd93e9b762f3c95a99b4e98e9a8e4564fbabc1780c38ea67c30ccbe5d1bbfd4725b65e91a9f1a9c4214241e3e5e056a0882851fd9fba57360607dfc39fcc6bd1e9485e0add99b3f889a31bacd033f46fa258aec9ab0762f62258ee2822e821782c60c4618e2edbd714562e2f4c4eb34a278e5bff96c9fc6f34771904adb0a09447c11b00b29588355df772c"}
         * isEmailVerified : 0
         * hasBusinessDetails : 0
         */

        private AuthorizationBean authorization;
        private String isEmailVerified;
        private String hasBusinessDetails;

        public AuthorizationBean getAuthorization() {
            return authorization;
        }

        public void setAuthorization(AuthorizationBean authorization) {
            this.authorization = authorization;
        }

        public String getIsEmailVerified() {
            return isEmailVerified;
        }

        public void setIsEmailVerified(String isEmailVerified) {
            this.isEmailVerified = isEmailVerified;
        }

        public String getHasBusinessDetails() {
            return hasBusinessDetails;
        }

        public void setHasBusinessDetails(String hasBusinessDetails) {
            this.hasBusinessDetails = hasBusinessDetails;
        }

        public static class AuthorizationBean {
            /**
             * token_type : Bearer
             * expires_in : 31622400
             * access_token : eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImVjZGU0ZjhiNzI4YzlkOWQzMzdmMDYyYTBhNTAwYzg0ZTgxNTU3ZmQ3MmExZjk5MzIxMzE1MDZmYzk3NjdkNzViYWY3YzE0ZjE0MTE0ZmMwIn0.eyJhdWQiOiI0NiIsImp0aSI6ImVjZGU0ZjhiNzI4YzlkOWQzMzdmMDYyYTBhNTAwYzg0ZTgxNTU3ZmQ3MmExZjk5MzIxMzE1MDZmYzk3NjdkNzViYWY3YzE0ZjE0MTE0ZmMwIiwiaWF0IjoxNTcxODI3MTI4LCJuYmYiOjE1NzE4MjcxMjgsImV4cCI6MTYwMzQ0OTUyOCwic3ViIjoiNDciLCJzY29wZXMiOlsiKiJdfQ.ADQqK1TtZIEo52yMOw_fLBiBtPmxlAzSRhDg-JmXniFZiSvhrAuDsAMtzGOq0m-kVH_vH_IcTVsJfZ4Rq50uzfU33ehUhaOd7EOEc2-ZfXIffBKu7PAzlPfoANOOjBCBjhVN8bYZa4JrI9WT0_8uxq8RsF3Cpq8_bKg4hrzZQwdu_6K6gvclLYOec3XHEQC6VFfvUIAT7ARtBS1xz9nw_cW_2i_n0HC6nrwIBejq1eZX1qOB13Zn7gTQaw9hoSqmSf_IVeXSaHk1hIQsSkfKviY0nhjDsaqKi0hPpuXn70ijOXiXYECyiFQe9V0UXQDXacc6SEV4st6KahHBYvBowQ9p2WxK-4My-oOhu-xF50Y_ECFFYUxAkIlfcgfNJ-QUpOxiISOkq1AWG_sDIzfW4kKvF4Zy01_I8yTk9dIXqBeuHVa57c0fBNk7HMHNdaU1dDAttZzn-7vbfKXGN0ZUlwFkI4rmkT1YqtNgXtdjHez_y5yRGKjJm4ZBk30dK1k49-kCyBrDvvf6YreaYf_NbQc2JPgv35QXE1GxJBTLCQ1ID7m6C30KEWufbfHR24MZRlzQ0YQyH5TiwYGUaOAUY6ho-I4wTIbPnjgQsZdFq1dQnSSs5u-NSfhMGf5IB6sw0_t5TCu7TJ6kRJQszsOmBeiyTb9IYCGgDE_q_vNnB7k
             * refresh_token : def50200132eb25c0c07a207759bb19e8dac8fc2246c375d96c142e490280ad0f378f72065967d3798e72be5a250defd396cbddf2f834bec3b870755c63ffff9220225ebfe473282ceb1ea39f42949b77930d47f90d38ba1490525a9a51c69db0a9a5256272151f208dedc1ef1f61427ada93ff5f8975e6631dfbe84bbd425a3a94a67313bc60a4e349515d7007f5135642d92ca118b308e1bea0158d41045a9a4323d349d29370a57a53d27ef9b838d7f2d02866dcc57b38f759d40a45506d021d6d874cd61891833c7d789d6a4cd09c0c4244281d9fd12c4c29fd93e9b762f3c95a99b4e98e9a8e4564fbabc1780c38ea67c30ccbe5d1bbfd4725b65e91a9f1a9c4214241e3e5e056a0882851fd9fba57360607dfc39fcc6bd1e9485e0add99b3f889a31bacd033f46fa258aec9ab0762f62258ee2822e821782c60c4618e2edbd714562e2f4c4eb34a278e5bff96c9fc6f34771904adb0a09447c11b00b29588355df772c
             */

            private String token_type;
            private int expires_in;
            private String access_token;
            private String refresh_token;

            public String getToken_type() {
                return token_type;
            }

            public void setToken_type(String token_type) {
                this.token_type = token_type;
            }

            public int getExpires_in() {
                return expires_in;
            }

            public void setExpires_in(int expires_in) {
                this.expires_in = expires_in;
            }

            public String getAccess_token() {
                return access_token;
            }

            public void setAccess_token(String access_token) {
                this.access_token = access_token;
            }

            public String getRefresh_token() {
                return refresh_token;
            }

            public void setRefresh_token(String refresh_token) {
                this.refresh_token = refresh_token;
            }
        }
    }
}
