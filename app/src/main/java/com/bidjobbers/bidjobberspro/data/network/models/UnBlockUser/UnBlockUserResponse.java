package com.bidjobbers.bidjobberspro.data.network.models.UnBlockUser;

public class UnBlockUserResponse {


    /**
     * responseCode : 1
     * responseText : Data found Successfully.
     * is_block : false
     */

    private int responseCode;
    private String responseText;
    private boolean is_block;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public boolean isIs_block() {
        return is_block;
    }

    public void setIs_block(boolean is_block) {
        this.is_block = is_block;
    }
}
