package com.bidjobbers.bidjobberspro.data.network.models.ContactUs;

public class ContactUsResponse {

    /**
     * responseCode : 1
     * responseText : Your enquiry form submitted successfully. Contact you soon.
     */

    private int responseCode;
    private String responseText;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }
}
