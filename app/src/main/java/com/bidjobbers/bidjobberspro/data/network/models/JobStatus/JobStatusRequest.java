package com.bidjobbers.bidjobberspro.data.network.models.JobStatus;

public class JobStatusRequest {


    private String task_id;
    private String locale;


    public JobStatusRequest(String task_id, String locale) {
        this.task_id = task_id;
        this.locale = locale;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
