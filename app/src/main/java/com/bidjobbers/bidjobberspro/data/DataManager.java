package com.bidjobbers.bidjobberspro.data;
import com.bidjobbers.bidjobberspro.data.network.ApiHelper;
import com.bidjobbers.bidjobberspro.data.pref.PreferencesHelper;

public interface DataManager extends ApiHelper, PreferencesHelper {

    void logout();

}
