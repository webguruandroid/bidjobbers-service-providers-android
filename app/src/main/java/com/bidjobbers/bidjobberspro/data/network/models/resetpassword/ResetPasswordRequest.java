package com.bidjobbers.bidjobberspro.data.network.models.resetpassword;

public class ResetPasswordRequest {

    String email="";
    String password="";
    String otp="";
    String locale="";

    public ResetPasswordRequest(String email, String password, String otp, String locale) {
        this.email = email;
        this.password = password;
        this.otp = otp;
        this.locale = locale;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
