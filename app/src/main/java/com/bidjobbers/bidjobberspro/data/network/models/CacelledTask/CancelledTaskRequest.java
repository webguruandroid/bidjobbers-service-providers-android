package com.bidjobbers.bidjobberspro.data.network.models.CacelledTask;

public class CancelledTaskRequest {


    /**
     * locale : en
     */

    private String locale;
    private String current_page;

    public CancelledTaskRequest(String locale, String current_page) {
        this.locale = locale;
        this.current_page = current_page;
    }

    public String getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(String current_page) {
        this.current_page = current_page;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
