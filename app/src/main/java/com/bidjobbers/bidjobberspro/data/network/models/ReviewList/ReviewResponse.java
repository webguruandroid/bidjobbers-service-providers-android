package com.bidjobbers.bidjobberspro.data.network.models.ReviewList;

import java.util.List;

public class ReviewResponse {


    /**
     * responseCode : 1
     * responseText : Data found Successfully.
     * responseData : {"reviewList":[{"customer_id":"13","customer_name":"S Paul","customer_profile_image":"http://13.233.187.231/bidjobbers/public/customer/profile_image/1578049451.png","rating":"4.5","review":"test","review_date":"14 Feb, 2020","review_time":"16:09"},{"customer_id":"13","customer_name":"S Paul","customer_profile_image":"http://13.233.187.231/bidjobbers/public/customer/profile_image/1578049451.png","rating":"4","review":"test review","review_date":"14 Feb, 2020","review_time":"16:11"},{"customer_id":"22","customer_name":"vbnn ghjj","customer_profile_image":"http://13.233.187.231/bidjobbers/public/customer/profile_image/1581079620.jpeg","rating":"4.5","review":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.","review_date":"14 Feb, 2020","review_time":"16:13"},{"customer_id":"22","customer_name":"vbnn ghjj","customer_profile_image":"http://13.233.187.231/bidjobbers/public/customer/profile_image/1581079620.jpeg","rating":"4.5","review":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.","review_date":"14 Feb, 2020","review_time":"16:14"}]}
     */

    private int responseCode;
    private String responseText;
    private ResponseDataBean responseData;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        private List<ReviewListBean> reviewList;

        public List<ReviewListBean> getReviewList() {
            return reviewList;
        }

        public void setReviewList(List<ReviewListBean> reviewList) {
            this.reviewList = reviewList;
        }

        public static class ReviewListBean {
            /**
             * customer_id : 13
             * customer_name : S Paul
             * customer_profile_image : http://13.233.187.231/bidjobbers/public/customer/profile_image/1578049451.png
             * rating : 4.5
             * review : test
             * review_date : 14 Feb, 2020
             * review_time : 16:09
             */

            private String customer_id;
            private String customer_name;
            private String customer_profile_image;
            private String rating;
            private String review;
            private String review_date;
            private String review_time;

            public String getCustomer_id() {
                return customer_id;
            }

            public void setCustomer_id(String customer_id) {
                this.customer_id = customer_id;
            }

            public String getCustomer_name() {
                return customer_name;
            }

            public void setCustomer_name(String customer_name) {
                this.customer_name = customer_name;
            }

            public String getCustomer_profile_image() {
                return customer_profile_image;
            }

            public void setCustomer_profile_image(String customer_profile_image) {
                this.customer_profile_image = customer_profile_image;
            }

            public String getRating() {
                return rating;
            }

            public void setRating(String rating) {
                this.rating = rating;
            }

            public String getReview() {
                return review;
            }

            public void setReview(String review) {
                this.review = review;
            }

            public String getReview_date() {
                return review_date;
            }

            public void setReview_date(String review_date) {
                this.review_date = review_date;
            }

            public String getReview_time() {
                return review_time;
            }

            public void setReview_time(String review_time) {
                this.review_time = review_time;
            }
        }
    }
}
