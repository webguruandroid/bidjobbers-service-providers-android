package com.bidjobbers.bidjobberspro.data.network.models.BlockCustomer;

public class BlockCustomerRequest {


    /**
     * locale : en
     * customer_id : 22
     */

    private String locale;
    private String customer_id;

    public BlockCustomerRequest(String locale, String customer_id) {
        this.locale = locale;
        this.customer_id = customer_id;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }
}
