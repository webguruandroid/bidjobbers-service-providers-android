package com.bidjobbers.bidjobberspro.data.network.models.Notification;

import java.util.List;

public class NotificationResponse {


    /**
     * responseCode : 1
     * responseText : Data found Successfully.
     * responseData : {"notificationList":[{"notification_id":1,"task_id":"99","bid_id":"","status_id":1,"status":"Received Jobs","notification_date":"20 Feb, 2020","notification_time":"18:13","notification_message":"A new task has been created","service_type":"Tree Stumps","service_image":"http://192.168.5.51/bidjobbers/public/jobber/Lawn_service_image/tree_stump_remove.png","customer_id":"1","customer_name":"test data"},{"notification_id":4,"task_id":"99","bid_id":"39","status_id":2,"status":"Scheduled Jobs","notification_date":"21 Feb, 2020","notification_time":"11:58","notification_message":"Your request accepted successfully","service_type":"Tree Stumps","service_image":"http://192.168.5.51/bidjobbers/public/jobber/Lawn_service_image/tree_stump_remove.png","customer_id":"1","customer_name":"test data"},{"notification_id":5,"task_id":"100","bid_id":"","status_id":1,"status":"Received Jobs","notification_date":"21 Feb, 2020","notification_time":"12:30","notification_message":"A new task has been created","service_type":"Trees & Shrubs","service_image":"http://192.168.5.51/bidjobbers/public/jobber/Lawn_service_image/trees_and_shrub_trim_and_remove.png","customer_id":"13","customer_name":"S Paul"},{"notification_id":6,"task_id":"101","bid_id":"","status_id":1,"status":"Received Jobs","notification_date":"21 Feb, 2020","notification_time":"12:54","notification_message":"A new task has been created","service_type":"Trees & Shrubs","service_image":"http://192.168.5.51/bidjobbers/public/jobber/Lawn_service_image/trees_and_shrub_trim_and_remove.png","customer_id":"13","customer_name":"S Paul"},{"notification_id":7,"task_id":"102","bid_id":"","status_id":1,"status":"Received Jobs","notification_date":"21 Feb, 2020","notification_time":"13:04","notification_message":"A new task has been created","service_type":"Tree Stumps","service_image":"http://192.168.5.51/bidjobbers/public/jobber/Lawn_service_image/tree_stump_remove.png","customer_id":"13","customer_name":"S Paul"}]}
     */

    private int responseCode;
    private String responseText;
    private ResponseDataBean responseData;

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public ResponseDataBean getResponseData() {
        return responseData;
    }

    public void setResponseData(ResponseDataBean responseData) {
        this.responseData = responseData;
    }

    public static class ResponseDataBean {
        private List<NotificationListBean> notificationList;

        public List<NotificationListBean> getNotificationList() {
            return notificationList;
        }

        public void setNotificationList(List<NotificationListBean> notificationList) {
            this.notificationList = notificationList;
        }

        public static class NotificationListBean {
            /**
             * notification_id : 1
             * task_id : 99
             * bid_id :
             * status_id : 1
             * status : Received Jobs
             * notification_date : 20 Feb, 2020
             * notification_time : 18:13
             * notification_message : A new task has been created
             * service_type : Tree Stumps
             * service_image : http://192.168.5.51/bidjobbers/public/jobber/Lawn_service_image/tree_stump_remove.png
             * customer_id : 1
             * customer_name : test data
             */

            private int notification_id;
            private String task_id;
            private String bid_id;
            private int status_id;
            private String status;
            private String notification_date;
            private String notification_time;
            private String notification_message;
            private String service_type;
            private String service_image;
            private String customer_id;
            private String customer_name;

            public int getNotification_id() {
                return notification_id;
            }

            public void setNotification_id(int notification_id) {
                this.notification_id = notification_id;
            }

            public String getTask_id() {
                return task_id;
            }

            public void setTask_id(String task_id) {
                this.task_id = task_id;
            }

            public String getBid_id() {
                return bid_id;
            }

            public void setBid_id(String bid_id) {
                this.bid_id = bid_id;
            }

            public int getStatus_id() {
                return status_id;
            }

            public void setStatus_id(int status_id) {
                this.status_id = status_id;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getNotification_date() {
                return notification_date;
            }

            public void setNotification_date(String notification_date) {
                this.notification_date = notification_date;
            }

            public String getNotification_time() {
                return notification_time;
            }

            public void setNotification_time(String notification_time) {
                this.notification_time = notification_time;
            }

            public String getNotification_message() {
                return notification_message;
            }

            public void setNotification_message(String notification_message) {
                this.notification_message = notification_message;
            }

            public String getService_type() {
                return service_type;
            }

            public void setService_type(String service_type) {
                this.service_type = service_type;
            }

            public String getService_image() {
                return service_image;
            }

            public void setService_image(String service_image) {
                this.service_image = service_image;
            }

            public String getCustomer_id() {
                return customer_id;
            }

            public void setCustomer_id(String customer_id) {
                this.customer_id = customer_id;
            }

            public String getCustomer_name() {
                return customer_name;
            }

            public void setCustomer_name(String customer_name) {
                this.customer_name = customer_name;
            }
        }
    }
}
