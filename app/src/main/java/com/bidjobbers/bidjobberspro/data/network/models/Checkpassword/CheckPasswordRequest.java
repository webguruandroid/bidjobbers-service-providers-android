package com.bidjobbers.bidjobberspro.data.network.models.Checkpassword;

public class CheckPasswordRequest {

    String locale;
    String password;

    public CheckPasswordRequest(String locale, String password) {
        this.locale = locale;
        this.password = password;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
