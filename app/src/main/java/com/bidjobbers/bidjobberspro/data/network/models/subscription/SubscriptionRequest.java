package com.bidjobbers.bidjobberspro.data.network.models.subscription;

public class SubscriptionRequest {

    /**
     * locale : en
     */

    private String locale;

    public SubscriptionRequest(String locale) {
        this.locale = locale;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }
}
