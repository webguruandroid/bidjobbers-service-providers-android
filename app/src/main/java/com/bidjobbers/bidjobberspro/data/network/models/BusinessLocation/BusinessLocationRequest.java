package com.bidjobbers.bidjobberspro.data.network.models.BusinessLocation;

public class BusinessLocationRequest {


    /**
     * locale : en
     * business_address : y8 brainware,kolkata-700091
     * apartment : y8 brainware
     * city : kolkata
     * state : WB
     * country : India
     * zipcode : 700091
     * lat : 22.572840
     * lng : 88.435883
     */

    private String locale;
    private String business_address;
    private String apartment;
    private String city_id;
    private String state;
    private String country_id;
    private String zipcode;
    private String lat;
    private String lng;

    public BusinessLocationRequest(String locale, String business_address, String apartment, String city_id, String state, String country_id, String zipcode, String lat, String lng) {
        this.locale = locale;
        this.business_address = business_address;
        this.apartment = apartment;
        this.city_id = city_id;
        this.state = state;
        this.country_id = country_id;
        this.zipcode = zipcode;
        this.lat = lat;
        this.lng = lng;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getBusiness_address() {
        return business_address;
    }

    public void setBusiness_address(String business_address) {
        this.business_address = business_address;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getCity() {
        return city_id;
    }

    public void setCity(String city_id) {
        this.city_id = city_id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country_id;
    }

    public void setCountry(String country_id) {
        this.country_id = country_id;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLongX() {
        return lng;
    }

    public void setLongX(String longX) {
        this.lng = longX;
    }
}
