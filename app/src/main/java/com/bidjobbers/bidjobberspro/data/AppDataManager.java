package com.bidjobbers.bidjobberspro.data;

import android.content.Context;

import com.bidjobbers.bidjobberspro.R;
import com.bidjobbers.bidjobberspro.data.network.ApiHelper;
import com.bidjobbers.bidjobberspro.data.network.models.Advance.AdvanceRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Advance.AdvanceResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Allpages.AllPagesRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Allpages.AllPagesResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Archived.ArchiveDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Archived.ArchiveListRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BankDetails.BankDetailsRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BankDetails.BankDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BidCreate.BidCreateRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BidCreate.BidCreateResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomer.BlockCustomerRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomer.BlockCustomerResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList.BlockCustomerListRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BlockCustomerList.BlockCustomerListResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BusinessInfo.BusinessInfoRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BusinessInfo.BusinessInfoResponse;
import com.bidjobbers.bidjobberspro.data.network.models.BusinessLocation.BusinessLocationRequest;
import com.bidjobbers.bidjobberspro.data.network.models.BusinessLocation.BusinessLocationResponse;
import com.bidjobbers.bidjobberspro.data.network.models.CacelledTask.CancelledTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.CacelledTask.CancelledTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ChangeaPassword.ChangePasswordRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ChangeaPassword.ChangePasswordResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Checkpassword.CheckPasswordRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Checkpassword.CheckpasswordResponse;
import com.bidjobbers.bidjobberspro.data.network.models.CityCountry.CityCountryResponse;
import com.bidjobbers.bidjobberspro.data.network.models.CompletedTask.CompletedTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.CompletedTask.CompletedTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ContactUs.ContactUsRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ContactUs.ContactUsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Customer.CustomerDetailsRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Customer.CustomerDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.DeviceToken.DeviceTokenRequest;
import com.bidjobbers.bidjobberspro.data.network.models.DeviceToken.DeviceTokenResponse;
import com.bidjobbers.bidjobberspro.data.network.models.EndTask.EndTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ImagePicker.ImagePickerResponse;
import com.bidjobbers.bidjobberspro.data.network.models.IntroBusinessDetails.IntroBusinessDetailsRequest;
import com.bidjobbers.bidjobberspro.data.network.models.IntroBusinessDetails.IntroBusinessDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.JobStatus.JobStatusRequest;
import com.bidjobbers.bidjobberspro.data.network.models.JobStatus.JobStatusResponse;
import com.bidjobbers.bidjobberspro.data.network.models.NewArchive.NewArchiveList;
import com.bidjobbers.bidjobberspro.data.network.models.Notification.NotificationRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Notification.NotificationResponse;
import com.bidjobbers.bidjobberspro.data.network.models.NotificationCount.NotificationCountRequest;
import com.bidjobbers.bidjobberspro.data.network.models.NotificationCount.NotificationCountResponse;
import com.bidjobbers.bidjobberspro.data.network.models.PriceFor.PriceForRequest;
import com.bidjobbers.bidjobberspro.data.network.models.PriceFor.PriceForResponse;
import com.bidjobbers.bidjobberspro.data.network.models.PriceForTree.PriceForTreeResponse;
import com.bidjobbers.bidjobberspro.data.network.models.PriceForTree.PriceForTreeRquest;
import com.bidjobbers.bidjobberspro.data.network.models.PropertyType.PropertyTypeResponse;
import com.bidjobbers.bidjobberspro.data.network.models.PropertyType.PropertypeRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ReSchedule.RescheduleRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ReSchedule.RescheduleResponse;
import com.bidjobbers.bidjobberspro.data.network.models.Recivetask.ReciveTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.Recivetask.ReciveTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ReviewList.ReviewRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ReviewList.ReviewResponse;
import com.bidjobbers.bidjobberspro.data.network.models.ScheduleTask.ScheduletaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.ScheduleTask.ScheduletaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.SetLocal.SetLocalRequest;
import com.bidjobbers.bidjobberspro.data.network.models.SetLocal.SetLocalResponse;
import com.bidjobbers.bidjobberspro.data.network.models.SocialMedia.SocialMediaRequest;
import com.bidjobbers.bidjobberspro.data.network.models.SocialMedia.SocialMediaResponse;
import com.bidjobbers.bidjobberspro.data.network.models.StartTask.StartTaskRequest;
import com.bidjobbers.bidjobberspro.data.network.models.StartTask.StartTaskResponse;
import com.bidjobbers.bidjobberspro.data.network.models.TimeSlot.TimeSlotRequest;
import com.bidjobbers.bidjobberspro.data.network.models.TimeSlot.TimeSlotResponse;
import com.bidjobbers.bidjobberspro.data.network.models.UnBlockUser.UnBlockUserRequest;
import com.bidjobbers.bidjobberspro.data.network.models.UnBlockUser.UnBlockUserResponse;
import com.bidjobbers.bidjobberspro.data.network.models.UpdateProfile.UpdateProfileResponse;
import com.bidjobbers.bidjobberspro.data.network.models.businesshours.BusinessHoursRequest;
import com.bidjobbers.bidjobberspro.data.network.models.businesshours.BusinessHoursRespose;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileRequest;
import com.bidjobbers.bidjobberspro.data.network.models.getProfile.getProfileResponse;
import com.bidjobbers.bidjobberspro.data.network.models.kindofproperty.KindofPropertyRequest;
import com.bidjobbers.bidjobberspro.data.network.models.kindofproperty.KindofPropertyResponse;
import com.bidjobbers.bidjobberspro.data.network.models.login.LoginRequest;
import com.bidjobbers.bidjobberspro.data.network.models.login.LoginResponce;
import com.bidjobbers.bidjobberspro.data.network.models.priceforlawnservice.PriceForLawnServiceResponse;
import com.bidjobbers.bidjobberspro.data.network.models.register.RegisterResponse;
import com.bidjobbers.bidjobberspro.data.network.models.resentotp.ResendOtpRequest;
import com.bidjobbers.bidjobberspro.data.network.models.resentotp.ResendOtpResponse;
import com.bidjobbers.bidjobberspro.data.network.models.resetpassword.ResetPasswordRequest;
import com.bidjobbers.bidjobberspro.data.network.models.resetpassword.ResetPasswordResponse;
import com.bidjobbers.bidjobberspro.data.network.models.servicedetails.ServiceDetailsRequest;
import com.bidjobbers.bidjobberspro.data.network.models.servicedetails.ServiceDetailsResponse;
import com.bidjobbers.bidjobberspro.data.network.models.subscription.SubscriptionRequest;
import com.bidjobbers.bidjobberspro.data.network.models.subscription.SubscriptionResponse;
import com.bidjobbers.bidjobberspro.data.network.models.test.TestResponse;
import com.bidjobbers.bidjobberspro.data.network.models.typeofpropertyresponse.TypeOfProperty;
import com.bidjobbers.bidjobberspro.data.network.models.typeofpropertyresponse.TypeOfPropertyRequest;
import com.bidjobbers.bidjobberspro.data.network.models.verifyotp.VerifyOtpRequest;
import com.bidjobbers.bidjobberspro.data.network.models.verifyotp.VerifyOtpResponse;
import com.bidjobbers.bidjobberspro.data.pref.PreferencesHelper;
import com.bidjobbers.bidjobberspro.di.ApplicationContext;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AppDataManager implements DataManager {

    ApiHelper mApiHelper;
    PreferencesHelper mPreferencesHelper;
    Context mContext;

    @Inject
    public AppDataManager(ApiHelper mApiHelper, PreferencesHelper mPreferencesHelper, @ApplicationContext Context mContext) {
        this.mApiHelper = mApiHelper;
        this.mPreferencesHelper = mPreferencesHelper;
        this.mContext = mContext;
    }

    @Override
    public void logout() {
        destroyPref();
    }


    @Override
    public String getCurrentUserId() {
        return mPreferencesHelper.getCurrentUserId();
    }

    @Override
    public void setCurrentUserId(String userId) {
        mPreferencesHelper.setCurrentUserId(userId);
    }

    @Override
    public String getAccess_token() {
        return mPreferencesHelper.getAccess_token();
    }

    @Override
    public void setAccess_token(String access_token) {
        mPreferencesHelper.setAccess_token(access_token);
    }

    @Override
    public String getIsEmailVarified() {
        return mPreferencesHelper.getIsEmailVarified();
    }

    @Override
    public void setIsEmailVarified(String emailVarified) {
        mPreferencesHelper.setIsEmailVarified(emailVarified);
    }

    @Override
    public String getIsQuestionFillup() {
        return mPreferencesHelper.getIsQuestionFillup();
    }

    @Override
    public void setIsQuestionFillup(String questionfillup) {
        mPreferencesHelper.setIsQuestionFillup(questionfillup);
    }

    @Override
    public String getLocalValue() {
        return mPreferencesHelper.getLocalValue();
    }

    @Override
    public void setLocalValue(String localValue) {
        mPreferencesHelper.setLocalValue(localValue);
    }

    @Override
    public String getCurrentUserEmail() {
        return mPreferencesHelper.getCurrentUserEmail();
    }

    @Override
    public void setCurrentUserEmail(String email) {
        mPreferencesHelper.setCurrentUserEmail(email);
    }

    @Override
    public String getCurrentUserProfilePicUrl() {
        return mPreferencesHelper.getCurrentUserProfilePicUrl();
    }

    @Override
    public void setCurrentUserProfilePicUrl(String profilePicUrl) {
        mPreferencesHelper.setCurrentUserProfilePicUrl(profilePicUrl);
    }

    @Override
    public String getCurrentFirstName() {
        return mPreferencesHelper.getCurrentFirstName();
    }

    @Override
    public void setCurrentFirstName(String firstName) {
        mPreferencesHelper.setCurrentFirstName(firstName);
    }

    @Override
    public String getCurrentLastName() {
        return mPreferencesHelper.getCurrentLastName();
    }

    @Override
    public void setCurrentLastName(String lastName) {
        mPreferencesHelper.setCurrentLastName(lastName);
    }

    @Override
    public void setCurrentUserGender(String gender) {
        mPreferencesHelper.setCurrentUserGender(gender);
    }

    @Override
    public String getCurrentUserGender() {
        return mPreferencesHelper.getCurrentUserGender();
    }

    @Override
    public String getCurrentMobileNumber() {
        return mPreferencesHelper.getCurrentMobileNumber();
    }

    @Override
    public void setCurrentMobileNumber(String mobileNumber) {
        mPreferencesHelper.setCurrentMobileNumber(mobileNumber);
    }

    @Override
    public void setLastIntaractionTimestamp(long timeStamp) {
        mPreferencesHelper.setLastIntaractionTimestamp(timeStamp);
    }

    @Override
    public long getLastIntaractionTimestamp() {
        return mPreferencesHelper.getLastIntaractionTimestamp();
    }

    @Override
    public void setRegistrationType(String registrationType) {
        mPreferencesHelper.setRegistrationType(registrationType);
    }

    @Override
    public String getRegistrationType() {
        return  mPreferencesHelper.getRegistrationType();
    }

    @Override
    public void setMyObject(String stringValueOfMyObject) {
        mPreferencesHelper.setMyObject(stringValueOfMyObject);
    }

    @Override
    public String getMyObject() {
         return  mPreferencesHelper.getMyObject();
    }

    @Override
    public void destroyPref() {

        mPreferencesHelper.destroyPref();
    }

    @Override
    public Single<TestResponse> getHomeData() {
        return mApiHelper.getHomeData();
    }

    @Override
    public ArrayList<String> getTypesOfTabs() {

        ArrayList<String> taskTabs= new ArrayList<String>();

        taskTabs.add(mContext.getResources().getString(R.string.tasks_tab_heading_received));
        taskTabs.add(mContext.getResources().getString(R.string.tasks_tab_heading_scheduled));
        taskTabs.add(mContext.getResources().getString(R.string.tasks_tab_heading_completed));
        taskTabs.add(mContext.getResources().getString(R.string.tasks_tab_heading_cancelled));


        return taskTabs;
    }

    @Override
    public Single<LoginResponce> getLoginResponse(LoginRequest req) {
        return mApiHelper.getLoginResponse(req);
    }

    @Override
    public Single<RegisterResponse> postPersonalUpdate(MultipartBody.Part profileImage, RequestBody company_name, RequestBody name, RequestBody email, RequestBody phone, RequestBody password, RequestBody locale, RequestBody accept_terms) {
        return mApiHelper.postPersonalUpdate( profileImage,  company_name,  name,  email,  phone,  password,  locale, accept_terms);
    }

    @Override
    public Single<RegisterResponse> postPersonalUpdate(RequestBody company_name, RequestBody name, RequestBody email, RequestBody phone, RequestBody password, RequestBody locale, RequestBody accept_terms) {
        return mApiHelper.postPersonalUpdate(company_name,name,email,phone,password,locale,accept_terms);
    }

    @Override
    public Single<VerifyOtpResponse> getVeryResponse(VerifyOtpRequest req) {
        return mApiHelper.getVeryResponse( req);
    }

    @Override
    public Single<ResendOtpResponse> getSentOtp(ResendOtpRequest req) {
        return mApiHelper.getSentOtp( req);
    }

    @Override
    public Single<ResetPasswordResponse> resetPassword(ResetPasswordRequest req) {
        return  mApiHelper.resetPassword( req);
    }

    @Override
    public Single<ServiceDetailsResponse> getServiceDetails(String authorization, String accept, String contentType, ServiceDetailsRequest serviceDetailsRequest) {
        return mApiHelper.getServiceDetails(authorization,accept,contentType,serviceDetailsRequest);
    }

    @Override
    public Single<getProfileResponse> getProfileDetails(String authorization, String accept, String contentType, getProfileRequest getProfileRequest) {
        return mApiHelper.getProfileDetails(authorization,accept,contentType,getProfileRequest);
    }

    @Override
    public Single<UpdateProfileResponse> postProfileUpdate(String authorization,  RequestBody name, RequestBody phone, RequestBody locale,MultipartBody.Part profileImage) {
        return mApiHelper.postProfileUpdate(authorization,name,phone,locale,profileImage);
    }

    @Override
    public Single<IntroBusinessDetailsResponse> postIntroBusinessDetailsResponse(String authorization, String contentType, IntroBusinessDetailsRequest introBusinessDetailsRequest) {
        return mApiHelper.postIntroBusinessDetailsResponse(authorization,contentType,introBusinessDetailsRequest);
    }

    @Override
    public Single<KindofPropertyResponse> postKindOfPropertyWorkOn(String authorization, String contentType, KindofPropertyRequest kindofPropertyRequest) {
        return mApiHelper.postKindOfPropertyWorkOn(authorization,contentType,kindofPropertyRequest);
    }

    @Override
    public Single<BusinessInfoResponse> postBusinessInfo(String authorization, String contentType, BusinessInfoRequest businessInfoRequest) {
        return mApiHelper.postBusinessInfo(authorization,contentType,businessInfoRequest);
    }

    @Override
    public Single<BusinessLocationResponse> postBusinessLocation(String authorization, String contentType, BusinessLocationRequest businessLocationRequest) {
        return mApiHelper.postBusinessLocation(authorization,contentType,businessLocationRequest);
    }

    @Override
    public Single<BusinessHoursRespose> postBusinessHours(String authorization, String contentType, BusinessHoursRequest businessHoursRequest) {
        return mApiHelper.postBusinessHours(authorization,contentType,businessHoursRequest);
    }

    @Override
    public Single<SocialMediaResponse> postSocialMediaLink(String authorization, String contentType, SocialMediaRequest socialMediaRequest) {
        return mApiHelper.postSocialMediaLink(authorization,contentType,socialMediaRequest);
    }

    @Override
    public Single<PropertyTypeResponse> postPropertyType(String authorization, String contentType, PropertypeRequest propertypeRequest) {
        return mApiHelper.postPropertyType(authorization,contentType,propertypeRequest);
    }

    @Override
    public Single<AdvanceResponse> postAdvanceType(String authorization, String contentType, AdvanceRequest advanceRequest) {
        return mApiHelper.postAdvanceType(authorization,contentType,advanceRequest);
    }

    @Override
    public Single<PriceForResponse> postPriceForType(String authorization, String contentType, PriceForRequest priceForRequest) {
        return mApiHelper.postPriceForType(authorization,contentType,priceForRequest);
    }

    @Override
    public Single<PriceForTreeResponse> postPriceTreeForType(String authorization, String contentType, PriceForTreeRquest priceForTreeRquest) {
        return mApiHelper.postPriceTreeForType(authorization,contentType,priceForTreeRquest);
    }

    @Override
    public Single<ImagePickerResponse> postWorkPhotoForType(String authorization, RequestBody local, RequestBody isworkimage, MultipartBody.Part[] surveyImage) {
        return mApiHelper.postWorkPhotoForType(authorization,local,isworkimage,surveyImage);
    }

    @Override
    public Single<ReciveTaskResponse> getReciveTask(String authorization, String contentType, ReciveTaskRequest reciveTaskRequest) {
        return mApiHelper.getReciveTask(authorization,contentType,reciveTaskRequest);
    }

    @Override
    public Single<ChangePasswordResponse> postChangePassword(String authorization, String contentType, ChangePasswordRequest changePasswordRequest) {
        return mApiHelper.postChangePassword(authorization,contentType,changePasswordRequest);
    }

    @Override
    public Single<TimeSlotResponse> postGetTimeSlot(String authorization, String contentType, TimeSlotRequest timeSlotRequest) {
        return mApiHelper.postGetTimeSlot(authorization,contentType,timeSlotRequest);
    }

    @Override
    public Single<BidCreateResponse> postBid(String authorization, String contentType, BidCreateRequest bidCreateRequest) {
        return mApiHelper.postBid(authorization,contentType,bidCreateRequest);
    }

    @Override
    public Single<ScheduletaskResponse> getScheduledTask(String authorization, String contentType, ScheduletaskRequest scheduletaskRequest) {
        return mApiHelper.getScheduledTask(authorization,contentType,scheduletaskRequest);
    }

    @Override
    public Single<AllPagesResponse> getAllPages( String contentType, AllPagesRequest allPagesRequest) {
        return mApiHelper.getAllPages(contentType,allPagesRequest);
    }

    @Override
    public Single<RescheduleResponse> postRescheduleTask(String authorization, String contentType, RescheduleRequest allPagesRequest) {
        return mApiHelper.postRescheduleTask(authorization,contentType,allPagesRequest);
    }

    @Override
    public Single<CompletedTaskResponse> postCompleteTask(String authorization, String contentType, CompletedTaskRequest completedTaskRequest) {
        return mApiHelper.postCompleteTask(authorization,contentType,completedTaskRequest);
    }

    @Override
    public Single<CancelledTaskResponse> postCancelledTask(String authorization, String contentType, CancelledTaskRequest cancelledTaskRequest) {
        return mApiHelper.postCancelledTask(authorization,contentType,cancelledTaskRequest);
    }

    @Override
    public Single<StartTaskResponse> postStartTask(String authorization, String contentType, StartTaskRequest startTaskRequest) {
        return mApiHelper.postStartTask(authorization,contentType,startTaskRequest);
    }

    @Override
    public Single<EndTaskResponse> postEndTask(String authorization,  RequestBody local, RequestBody CustomerId, RequestBody taskId, RequestBody bidId, RequestBody task_end_notes, MultipartBody.Part[] task_images) {
        return mApiHelper.postEndTask(authorization,local,CustomerId,taskId,bidId,task_end_notes,task_images);
    }


    @Override
    public Single<BlockCustomerResponse> postBlockUser(String authorization, String contentType, BlockCustomerRequest blockCustomerRequest) {
        return mApiHelper.postBlockUser(authorization,contentType,blockCustomerRequest);
    }

    @Override
    public Single<UnBlockUserResponse> postUnBlockUser(String authorization, String contentType, UnBlockUserRequest unBlockUserRequest) {
        return mApiHelper.postUnBlockUser(authorization,contentType,unBlockUserRequest);
    }

    @Override
    public Single<BlockCustomerListResponse> postBlockUserList(String authorization, String contentType, BlockCustomerListRequest blockCustomerListRequest) {
        return mApiHelper.postBlockUserList(authorization,contentType,blockCustomerListRequest);
    }

    @Override
    public Single<ReviewResponse> getReviewList(String authorization, String contentType, ReviewRequest reviewRequest) {
        return mApiHelper.getReviewList(authorization,contentType,reviewRequest);
    }

    @Override
    public Single<NotificationResponse> getNotificationList(String authorization, String contentType, NotificationRequest notificationRequest) {
        return mApiHelper.getNotificationList(authorization,contentType,notificationRequest);
    }

    @Override
    public Single<SubscriptionResponse> getSubscription(String authorization,SubscriptionRequest subscriptionRequest) {
        return mApiHelper.getSubscription(authorization,subscriptionRequest);
    }


    @Override
    public Single<NotificationCountResponse> getNotificationCount(String authorization, String contentType, NotificationCountRequest notificationCountRequest) {
        return mApiHelper.getNotificationCount(authorization,contentType,notificationCountRequest);
    }

    @Override
    public Single<DeviceTokenResponse> getDeviceToken(String authorization, String contentType, DeviceTokenRequest notificationCountRequest) {
        return mApiHelper.getDeviceToken(authorization,contentType,notificationCountRequest);
    }

    @Override
    public Single<JobStatusResponse> getJobStatus(String authorization, String contentType, JobStatusRequest jobStatusRequest) {
        return mApiHelper.getJobStatus(authorization,contentType,jobStatusRequest);
    }

    @Override
    public Single<CustomerDetailsResponse> getCustomerDetails(String authorization, String contentType, CustomerDetailsRequest customerDetailsRequest) {
        return mApiHelper.getCustomerDetails(authorization,contentType,customerDetailsRequest);
    }

    @Override
    public Single<ArchiveDetailsResponse> getArchiveDetails(String authorization, String contentType, CustomerDetailsRequest customerDetailsRequest) {
        return mApiHelper.getArchiveDetails(authorization,contentType,customerDetailsRequest);
    }

    @Override
    public Single<NewArchiveList> getArchivedList(String authorization, String contentType, ArchiveListRequest archiveListRequest) {
        return mApiHelper.getArchivedList(authorization,contentType,archiveListRequest);
    }

    @Override
    public Single<CityCountryResponse> getCityCountry() {
        return mApiHelper.getCityCountry();
    }

    @Override
    public Single<BankDetailsResponse> submitBankDetails(String authorization, String contentType, BankDetailsRequest bankDetailsRequest) {
        return mApiHelper.submitBankDetails(authorization,contentType,bankDetailsRequest);
    }

    @Override
    public Single<CheckpasswordResponse> validatePassword(String authorization, String contentType, CheckPasswordRequest checkPasswordRequest) {
        return mApiHelper.validatePassword(authorization,contentType,checkPasswordRequest);
    }

    @Override
    public Single<SetLocalResponse> setLocal(String authorization, String contentType,SetLocalRequest setLocalRequest) {
        return mApiHelper.setLocal(authorization,contentType,setLocalRequest);
    }

    @Override
    public Single<ContactUsResponse> postContactUs(String contentType, ContactUsRequest contactUsRequest) {
        return mApiHelper.postContactUs(contentType,contactUsRequest);
    }


    @Override
    public Single<TypeOfProperty> getTypesOfProperty(TypeOfPropertyRequest req) {
        return mApiHelper.getTypesOfProperty( req);
    }

    @Override
    public Single<BusinessHoursRespose> getBusinessHours(TypeOfPropertyRequest req) {
        return mApiHelper.getBusinessHours( req);
    }

    @Override
    public Single<PriceForLawnServiceResponse> getPriceForLawnService(TypeOfPropertyRequest req) {
        return mApiHelper.getPriceForLawnService(req);
    }


}

